// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })


import GocPageObject from "../../pageobjects/GocPageObject";

function getActualizarButton() {
    return cy.get(".x-grid-row-editor-buttons[id^='roweditorbuttons']:visible").contains("Actualizar");
}

Cypress.Commands.add('firmarYCerrar', (reunionesPage)=>{
    let gocPage = new GocPageObject(cy);
    reunionesPage.rellenarAcuerdos();
    reunionesPage.getSelectorResponsable().click();
    gocPage.getLoadingIndicator().should("not.be.visible");
    reunionesPage.getSelectorResponsable().type("{enter}", {force: true});
    reunionesPage.getBotonFirmarYCerrar().click();
    gocPage.getLoadingIndicator().should("not.be.visible");
})

Cypress.Commands.add('validForm', () => {
    getActualizarButton().should("have.attr", "aria-disabled", "false");
    cy.get("div.x-grid-row-editor-errors").should("have.attr", "aria-hidden", "true");
});

Cypress.Commands.add('invalidForm', () => {
    getActualizarButton().should("have.attr", "aria-disabled", "true");
    cy.get("div.x-grid-row-editor-errors:visible").should("have.attr", "aria-hidden", "false");
});

const _ = Cypress._;

Cypress.Commands.add('login', (overrides = {}) => {
    cy.clearCookies();

    Cypress.log({
        name: 'login'
    });

    cy.visit('/goc?lang=es')
        .then((resp) => {
            cy.contains("a", "desconectarse");
        });
});

Cypress.Commands.add('loginAs', (defaultUserId, defaultUserName) => {
    cy.clearCookie('JSESSIONID');

    Cypress.log({
        name: 'loginAs'
    });

    cy.request({url: '/goc', headers: {defaultUserId: defaultUserId, defaultUserName: defaultUserName}})
        .its("status").should("be.equal", 200);
});

Cypress.Commands.add('dataCy', (value) => {
    return cy.get(`[data-cy=${value}]`);
});

Cypress.Commands.add('upload_file', (fileName, fileType = ' ', selector) => {
    cy.get(selector).then(subject => {
        cy.fixture(fileName, 'base64')
            .then(Cypress.Blob.base64StringToBlob)
            .then(blob => {
                const el = subject[0]
                const testFile = new File([blob], fileName, {type: fileType})
                const dataTransfer = new DataTransfer()
                dataTransfer.items.add(testFile)
                el.files = dataTransfer.files
                el.dispatchEvent(new Event('change', {bubbles: true}))
            })
    })
});


