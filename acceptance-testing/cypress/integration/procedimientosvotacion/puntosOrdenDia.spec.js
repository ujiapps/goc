import OrdenDiaPageObject from "../../../pageobjects/OrdenDiaPageObject";
import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";

describe("Procedimientos votación a nivel de puntos del orden del día", () => {
    let reunionesPage;
    let ordenDiaPage;
    let namePuntoEnContra;

    before(() => {
        const name = Math.random().toString(36).substr(2, 5);

        cy.login();

        reunionesPage = new ReunionesPageObject(cy);
        ordenDiaPage = new OrdenDiaPageObject(cy);

        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");

        reunionesPage.add();

        reunionesPage.getTelematicaCheck().should("be.checked");
        reunionesPage.getVotacionCheck().check({force: true});
        reunionesPage.fill(name, name);
        reunionesPage.save();

        reunionesPage.select(name);
        reunionesPage.getBotonPuntosOrdenDia().click();
    });

    beforeEach(() => {
        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
    });

    it("Crear un punto orden día con tipo de procedimiento ordinario", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getProcedimientoOrdinario().should('be.checked');
        ordenDiaPage.cancel();
    });

    it("Crear un punto orden día con tipo de procedimiento abreviado", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoAbreviado().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getProcedimientoAbreviado().should('be.checked');
        ordenDiaPage.cancel();
    });

    it("Editar un punto orden día con tipo de procedimiento ordinario a abreviado", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");

        ordenDiaPage.getProcedimientoOrdinario().should('be.checked');
        ordenDiaPage.getProcedimientoAbreviado().click();
        ordenDiaPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);

        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getProcedimientoAbreviado().should('be.checked');

        ordenDiaPage.cancel();
    });

    it("Editar un punto orden día con tipo de procedimiento abreviado a ordinario", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoAbreviado().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");

        ordenDiaPage.getProcedimientoAbreviado().should('be.checked');
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);

        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getProcedimientoOrdinario().should('be.checked');

        ordenDiaPage.cancel();
    });

    it("Crear un punto orden día con recuento de voto de tipo mayoría simple", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.getMayoriaSimple().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getMayoriaSimple().should('be.checked');
        ordenDiaPage.cancel();
    });

    it("Crear un punto orden día con recuento de voto de tipo mayoría absoluta de los presentes", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.getMayoriaAbsolutaPresentes().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getMayoriaAbsolutaPresentes().should('be.checked');
        ordenDiaPage.cancel();
    });

    it("Crear un punto orden día con recuento de voto de tipo mayoría absoluta del órgano", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.getMayoriaAbsolutaOrgano().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getMayoriaAbsolutaOrgano().should('be.checked');
        ordenDiaPage.cancel();
    });

    it("Editar un punto orden día con recuento de voto de tipo mayoría simple a mayoría absoluta del órgano ", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.getMayoriaSimple().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);

        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getMayoriaSimple().should('be.checked');
        ordenDiaPage.getMayoriaAbsolutaOrgano().click();
        ordenDiaPage.getMayoriaSimple().should('be.not.checked');

        ordenDiaPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");

        ordenDiaPage.getMayoriaSimple().should('be.not.checked');
        ordenDiaPage.getMayoriaAbsolutaOrgano().should('be.checked');
        ordenDiaPage.cancel();
    });

    it("Debe crear un punto del orden del día no votable sin mayoría ni recuentos", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");

        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.getVotacionIndefinida().click();

        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);

        ordenDiaPage.getVotacionIndefinida().should('be.checked');
        ordenDiaPage.cancel();
    });

    it("Debe editar un punto que no requiera votación a votación abierta, abreviada y con mayoría simple", () => {
        const name = Math.random().toString(36).substr(2, 5);

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");

        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.getVotacionIndefinida().click();

        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);

        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoAbreviado().click();
        ordenDiaPage.getMayoriaSimple().click();

        ordenDiaPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(name);

        ordenDiaPage.getVotacionAbierta().should('be.checked');
        ordenDiaPage.getProcedimientoAbreviado().should('be.checked');
        ordenDiaPage.getMayoriaSimple().should('be.checked');
        ordenDiaPage.cancel();
    });

    it("Debe poder habilitar/deshabilitar la opcion de votar en contra en un punto al añadirlo", () => {
        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");

        namePuntoEnContra = Math.random().toString(36).substr(2, 5);
        ordenDiaPage.fill(namePuntoEnContra, "Punto sin voto en contra.");

        ordenDiaPage.getVotarEnContraCheck().click();

        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(namePuntoEnContra);

        ordenDiaPage.getVotarEnContraCheck().should("not.be.checked");

        ordenDiaPage.cancel();
    });


    it("Debe poder habilitar/deshabilitar la opcion de votar en contra en un punto al editarlo", () => {
        ordenDiaPage.edit(namePuntoEnContra);

        ordenDiaPage.getVotarEnContraCheck().click()

        ordenDiaPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        ordenDiaPage.edit(namePuntoEnContra);
        ordenDiaPage.getVotarEnContraCheck().should("be.checked");

        ordenDiaPage.cancel();
    });
});

