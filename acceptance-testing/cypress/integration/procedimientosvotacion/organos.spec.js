import OrganosPageObject from "../../../pageobjects/OrganosPageObject";

describe("Procedimientos votación", () => {
    let organosPage;
    let randName = "Procedimiento votacion -" + Math.random().toString(36).substr(2, 5);

    beforeEach(() => {
        cy.login();
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");

        organosPage = new OrganosPageObject(cy);
    })

    it('Crear un órgano con el procedimiento de votación abreviado', () => {
        organosPage.open();
        organosPage.getLoadMask().should("not.be.visible");

        organosPage.add();
        cy.invalidForm();
        organosPage.fill(randName, "Procedimiento abreviado", "test", "abreviado");
        cy.validForm();
        organosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it('Crear un órgano con el procedimiento de votación ordinario', () => {
        organosPage.open();
        organosPage.getLoadMask().should("not.be.visible");

        organosPage.add();
        cy.invalidForm();
        organosPage.fill(randName, "Procedimiento abreviado", "test", "ordinario");
        cy.validForm();
        organosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it('Editar un órgano con el procedimiento de votación abreviado a ordinario', () => {
        organosPage.open();
        organosPage.getLoadMask().should("not.be.visible");

        organosPage.add();
        cy.invalidForm();
        organosPage.fill(randName, "Procedimiento abreviado", "test", "abreviado");
        cy.validForm();
        organosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        organosPage.edit(randName);
        organosPage.clear();
        organosPage.fill(randName, "Procedimiento abreviado", "test", "ordinario");
        organosPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it('Editar un órgano con el procedimiento de votación ordinario a abreviado', () => {
        organosPage.open();
        organosPage.getLoadMask().should("not.be.visible");

        organosPage.add();
        cy.invalidForm();
        organosPage.fill(randName, "Procedimiento abreviado", "test", "ordinario");
        cy.validForm();
        organosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        organosPage.edit(randName);
        organosPage.clear();
        organosPage.fill(randName, "Procedimiento abreviado", "test", "abreviado");
        organosPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });
});