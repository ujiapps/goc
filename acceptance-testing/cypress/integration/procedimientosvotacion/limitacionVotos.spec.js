import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";

context("Limitación de votos", () => {
    let votacionesPage;
    let reunionId = 4000;
    let puntoAbiertoId = 4001;
    let puntoSecretoId = 4002;

    const waitUntilSendVoto = () => cy.wait(["@votar"]).its("status").should("be.equal", 204);
    const waitUntilUpdateVoto = () => cy.wait(["@actualizar"]).its("status").should("be.equal", 204);

    before(() => {
        votacionesPage = new VotacionesPageObject(cy);
        votacionesPage.open(reunionId);
    });

    beforeEach(() => {
        cy.server();
        cy.route("PUT", "/goc/rest/**").as("actualizar");
        cy.route("POST", "/goc/rest/**").as("votar");
    });

    it("Si la votación es abierta, un usuario no puede votar dos veces", () => {
        votacionesPage.getBotonAFavorPunto(puntoAbiertoId).click();
        waitUntilSendVoto();

        votacionesPage.getBotonAFavorPunto(puntoAbiertoId).should('be.disabled');
        votacionesPage.getBotonEnContraPunto(puntoAbiertoId).click();
        waitUntilUpdateVoto();
    });

    it("Si la votación es secreta, un usuario no puede votar dos veces", () => {
        votacionesPage.getBotonAFavorPunto(puntoSecretoId).click();
        waitUntilSendVoto();

        votacionesPage.getBotonAFavorPunto(puntoSecretoId).should('be.disabled');
        votacionesPage.getBotonEnContraPunto(puntoSecretoId).should('be.disabled');
        votacionesPage.getBotonAbstencionPunto(puntoSecretoId).should('be.disabled');
    });
});