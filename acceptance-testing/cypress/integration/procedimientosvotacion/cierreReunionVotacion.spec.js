import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";
import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";
import GocPageObject from "../../../pageobjects/GocPageObject";

context("Cierre reunión y de votación", () => {
    let reunionesPage;
    let reunionName = "Cierre votación";
    let reunionId = 9000;
    let puntoOrdinarioVotacionAbiertaId = 9002;
    let puntoAbreviadoVotacionAbiertaId = 9003;

    beforeEach(() => {
        cy.server();

        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("GET", "/goc/rest/**").as("get");
    });

    function waitUntilUpdate() {
        cy.wait(["@update"]).its("status").should("be.equal", 204);
    }

    function waitUntilUpdateError() {
        cy.wait(["@update"]).its("status").should("be.equal", 500);
    }

    function waitUntilReloadGrid() {
        cy.wait(["@get"]).its("status").should("be.equal", 200);
    }

    it("Al cerrar una reunión, si la votación es ordinaria o abreviada, se ha de cerrar la votación", () => {
        cy.login();

        reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
        reunionesPage.select(reunionName);
        reunionesPage.selectAccion("Cerrar acta");
        waitUntilReloadGrid();

        cy.firmarYCerrar(reunionesPage);

        reunionesPage.getLoadingIndicator().should("not.be.visible");

        waitUntilUpdate();
        waitUntilReloadGrid();
        reunionesPage.getLoadMask().should("not.be.visible");

        let votacionesPage = new VotacionesPageObject(cy);
        votacionesPage.open(reunionId);

        votacionesPage.getEstadoCerradaVotacionPunto(puntoOrdinarioVotacionAbiertaId).should('exist');
        votacionesPage.getBotonEnContraPunto(puntoOrdinarioVotacionAbiertaId).should('not.exist');
        votacionesPage.getBotonAbrirVotacionPunto(puntoOrdinarioVotacionAbiertaId).should('exist');
        votacionesPage.getBotonCerrarVotacionPunto(puntoOrdinarioVotacionAbiertaId).should('not.exist');

        votacionesPage.getEstadoCerradaVotacionPunto(puntoAbreviadoVotacionAbiertaId).should('exist');
        votacionesPage.getBotonEnContraPunto(puntoAbreviadoVotacionAbiertaId).should('not.exist');
    });
});