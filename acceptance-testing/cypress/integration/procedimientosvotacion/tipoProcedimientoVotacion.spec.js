import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";

import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";

context("Tipo de procedimiento de votacion y fechas de fin de la votacion.", () => {
    let votacionesPage;
    let reunionesPage;
    let reunionId = 13002;
    let reunionName = "Reunion con fecha fin anterior"
    let puntoAbreviado = 13003;
    let puntoOrdinario = 13004;


    beforeEach(() =>{
        cy.server();
        cy.loginAs(1, "Secretario");

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");
        cy.route("PUT", "/goc/rest/votos/cierra/**").as("cerrarVotacion");

        reunionesPage = new ReunionesPageObject(cy);
        votacionesPage = new VotacionesPageObject(cy);
    })

    it("No se debe poder votar un punto abreviado si la reunión tiene fecha de finalización pasada.", ()=>{
        votacionesPage.open(reunionId);
        votacionesPage.getEstadoCerradaVotacionPunto(puntoAbreviado).should("be.visible");
        votacionesPage.getBotonCerrarVotacionPunto(puntoAbreviado).should("not.exist");
        votacionesPage.getBotonAFavorPunto(puntoAbreviado).should("not.exist");
    })

    it("No se debe poder votar un punto ordinario si la reunión tiene fecha de finalización pasada.", ()=>{
        votacionesPage.open(reunionId);
        votacionesPage.getEstadoCerradaVotacionPunto(puntoOrdinario).should("be.visible");
        votacionesPage.getBotonCerrarVotacionPunto(puntoOrdinario).should("not.exist");
        votacionesPage.getBotonAFavorPunto(puntoOrdinario).should("not.exist");
    })

    it("No se puede volver a abrir la votacion de un punto de orden del dia abreviado si se ha cerrado.", ()=>{
        cy.login();
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
        reunionesPage.edit(reunionName);
        reunionesPage.fillFechaFinPosterior();
        reunionesPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);

        reunionesPage.getLoadingIndicator().should("not.be.visible");
        cy.loginAs(1, "Secretario");
        votacionesPage.open(reunionId);
        votacionesPage.getBotonCerrarVotacionPunto(puntoAbreviado).click();
        cy.wait(["@cerrarVotacion"]).its("status").should("be.equal", 200);
        votacionesPage.getBotonAbrirVotacionPunto(puntoAbreviado).should("not.exist");
        votacionesPage.getEstadoCerradaVotacionPunto(puntoAbreviado).should("be.visible");
    })
});