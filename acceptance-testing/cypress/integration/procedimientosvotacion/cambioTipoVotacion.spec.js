import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../../pageobjects/OrdenDiaPageObject";

context("Cambio del tipo de votación en la reunión", () => {
    let reunionesPage;
    let reunionName = "Cambio del tipo de votación en la reunión";
    let puntoVotacionSecretaSinVotosSeraAbiertaName = "POD con votación secreta sin votos será abierta";
    let puntoVotacionSecretaSinVotosSeraNoNecesitaVotacionName = "POD con votación secreta sin votos no necesitará votación";
    let puntoVotacionAbiertaConVotos = "POD con votación abierta con votos";
    let ordenDiaPage;

    before(() => {
        cy.login();

        reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
        reunionesPage.select(reunionName);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage = new OrdenDiaPageObject(cy);
    });

    beforeEach(() => {
        cy.server();

        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("GET", "/goc/rest/**").as("get");
    });

    afterEach(() => {
        ordenDiaPage.getLoadMask().should("not.be.visible");
        ordenDiaPage.cancel();
    });

    function waitUntilUpdate() {
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    }

    function waitUntilUpdateError() {
        cy.wait(["@update"]).its("status").should("be.equal", 500);
    }

    function waitUntilReloadGrid() {
        cy.wait(["@get"]).its("status").should("be.equal", 200);
    }

    it("Si no tiene votos, se ha de poder cambiar el tipo de votación de un punto del orden del día (POD), de votación secreta a votación abierta", () => {
        ordenDiaPage.edit(puntoVotacionSecretaSinVotosSeraAbiertaName);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.save();

        waitUntilUpdate();
        waitUntilReloadGrid();

        ordenDiaPage.edit(puntoVotacionSecretaSinVotosSeraAbiertaName);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().should('be.checked');
    });

    it("Si no tiene votos, se ha de poder cambiar el tipo de votación de un POD, de votación secreta a sin votación", () => {
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.edit(puntoVotacionSecretaSinVotosSeraNoNecesitaVotacionName);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionIndefinida().click();
        ordenDiaPage.save();

        waitUntilUpdate();
        waitUntilReloadGrid();

        ordenDiaPage.edit(puntoVotacionSecretaSinVotosSeraNoNecesitaVotacionName);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionIndefinida().should('be.checked');
    });

    it("Si tiene votos, no se ha de poder cambiar el tipo de votación de un POD. Se ha de mostrar un error controlado", () => {
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.edit(puntoVotacionAbiertaConVotos);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionSecreta().click();
        ordenDiaPage.save();

        waitUntilUpdateError();
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`El punto del orden del día tiene votos emitidos`);
        })
    });
});