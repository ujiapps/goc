import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";
import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";
import ResultadosVotacionPageObject from "../../../pageobjects/ResultadosVotacionPageObject";

context("Emisión de votos dobles por parte del presidente", () => {
    let reunionVotoDobleId = 17000;
    let puntoVotableDobleId = 17004;
    let reunionNoVotoDobleId = 17005;
    let puntoVotableNoDobleId = 17009;
    let reunionPage;
    let votacionPage;
    let resultadosPage;

    before(() => {
        cy.loginAs(2, "Presidente");
        reunionPage = new ReunionesPageObject(cy);
        votacionPage = new VotacionesPageObject(cy)
        votacionPage.open(reunionVotoDobleId);
        resultadosPage = new ResultadosVotacionPageObject(cy);
    });

    beforeEach(() => {
        cy.server();
        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("post");
    })

    it("El voto de un presidente en una votación contara doble si la opcion esta habilitada en el organo de la reunion", () => {
        votacionPage.open(reunionVotoDobleId);
        votacionPage.getBotonEnContraPunto(puntoVotableDobleId).click();
        cy.wait(["@post"]).its("status").should("be.equal", 204);
        resultadosPage.open(reunionVotoDobleId);
        resultadosPage.getVotosEnContra(puntoVotableDobleId).should('contain', 2);

    })

    it("El voto de un presidente en una votación no contara doble si la opcion no esta habilitada en el organo de la reunion", () => {
        votacionPage.open(reunionNoVotoDobleId);
        votacionPage.getBotonEnContraPunto(puntoVotableNoDobleId).click();
        cy.wait(["@post"]).its("status").should("be.equal", 204);
        resultadosPage.open(reunionNoVotoDobleId);
        resultadosPage.getVotosEnContra(puntoVotableNoDobleId).should('not.contain', 2);
    })
});