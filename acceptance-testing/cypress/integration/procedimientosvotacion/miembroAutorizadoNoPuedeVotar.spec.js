import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";

context("Miembro autorizado no puede votar", () => {
    let votacionesPage;
    let reunionId = 6000;
    let puntoAbiertoId = 6001;
    let puntoSecretoId = 6002;

    const isWindowAlertMessageNoPermissions = () => {
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`No tienes permiso para emitir votos`);
        });
    };

    before(() => {
        cy.loginAs(2, "Autorizado");
        votacionesPage = new VotacionesPageObject(cy);
        votacionesPage.open(reunionId);
    });

    it("El autorizado no ha de poder votar ningún punto de una votación abierta. Mostrar error controlado", () => {
        votacionesPage.getOpcionVotacionPunto(puntoAbiertoId).click();
        isWindowAlertMessageNoPermissions();
    });

    it("El autorizado no ha de poder votar ningún punto de una votación secreto. Mostrar error controlado", () => {
        votacionesPage.getOpcionVotacionPunto(puntoSecretoId).click();
        isWindowAlertMessageNoPermissions();
    });
});