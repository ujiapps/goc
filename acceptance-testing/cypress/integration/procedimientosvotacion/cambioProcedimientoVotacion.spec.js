import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../../pageobjects/OrdenDiaPageObject";

context("Cambio del procedimiento de votación en la reunión", () => {
    let reunionesPage;
    let reunionName = "Cambio del tipo procedimiento en la reunión";
    let puntoVotacion = "POD sin votos";
    let puntoVotacionConVotos = "POD con votos";
    let ordenDiaPage;

    before(() => {
        cy.login();

        reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
        reunionesPage.select(reunionName);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage = new OrdenDiaPageObject(cy);
    });

    beforeEach(() => {
        cy.server();

        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("GET", "/goc/rest/**").as("get");
    });

    afterEach(() => {
        ordenDiaPage.getLoadMask().should("not.be.visible");
        ordenDiaPage.cancel();
    });

    function waitUntilUpdate() {
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    }

    function waitUntilUpdateError() {
        cy.wait(["@update"]).its("status").should("be.equal", 500);
    }

    function waitUntilReloadGrid() {
        cy.wait(["@get"]).its("status").should("be.equal", 200);
    }

    it("Si no tiene votos, se ha de poder cambiar el tipo de procedimiento de una votacion de un POD, de abreviada a ordinaria.", () => {
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.edit(puntoVotacion);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.save();

        waitUntilUpdate();
        waitUntilReloadGrid();

        ordenDiaPage.edit(puntoVotacion);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getProcedimientoOrdinario().should('be.checked');
    });

    it("Si tiene votos, no se ha de poder cambiar el tipo de procedimiento de una votacion en un POD. Se ha de mostrar un error controlado", () => {
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.edit(puntoVotacionConVotos);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.save();

        waitUntilUpdateError();
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`El punto del orden del día tiene votos emitidos`);
        })
    });
});