import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../../pageobjects/OrdenDiaPageObject";
import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";

context("Añadir un nuevo POD a una reunón convocada", () => {
    let reunionesPage;
    let reunionId = 2000;
    let reunionName = "Añadir un nuevo POD a una reunón convocada";
    let puntoAbreviadoEnReunionConvocada = "Punto abreviado en reunión convocada";
    let puntoOrdinarioEnReunionConvocada = "Punto ordinario en reunión convocada";
    let ordenDiaPage;

    beforeEach(() => {
        cy.server();

        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("GET", "/goc/rest/**").as("get");

        cy.login();

        reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
        reunionesPage.select(reunionName);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage = new OrdenDiaPageObject(cy);
    });

    function waitUntilSave() {
        cy.wait(["@save"]).its("status").should("be.equal", 200);
    }

    function waitUntilReloadGrid() {
        cy.wait(["@get"]).its("status").should("be.equal", 200);
    }

    it("Si se añade un nuevo POD con votación abreviada  a una reunión convocada, la votación para este punto se abrirá automáticamente", () => {
        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoAbreviado().click();
        ordenDiaPage.fill(puntoAbreviadoEnReunionConvocada, puntoAbreviadoEnReunionConvocada);
        ordenDiaPage.save();

        waitUntilSave();
        waitUntilReloadGrid();

        cy.get('@save').its('response.body.data.id').then(puntoId => {
            let votacionesPage = new VotacionesPageObject(cy);
            votacionesPage.open(reunionId);
            votacionesPage.getOpcionVotacionPunto(puntoId).should('be.visible');
        });
    });

    it("Si se añade un nuevo POD con votación ordinaria a una reunión convocada, la votación permanecerá cerrada", () => {
        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.getProcedimientoOrdinario().click();
        ordenDiaPage.fill(puntoOrdinarioEnReunionConvocada, puntoOrdinarioEnReunionConvocada);
        ordenDiaPage.save();

        waitUntilSave();
        waitUntilReloadGrid();

        cy.get('@save').its('response.body.data.id').then(puntoId => {
            let votacionesPage = new VotacionesPageObject(cy);
            votacionesPage.open(reunionId);
            votacionesPage.getOpcionVotacionPunto(puntoId).should('not.exist');
        });
    });
});