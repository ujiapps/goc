import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";

const waitUntilSendVoto = () => cy.wait(["@votar"]).its("status").should("be.equal", 204);
const waitUntilUpdateVoto = () => cy.wait(["@actualizar"]).its("status").should("be.equal", 204);

context("Cambio de voto en reunión que SÍ admite cambio de voto", () => {
    let votacionesPage;
    let reunionId = 5001;
    let puntoAbiertoId = 5001;
    let puntoSecretoId = 5002;

    before(() => {
        cy.loginAs(1, "Secretario");
        votacionesPage = new VotacionesPageObject(cy);
        votacionesPage.open(reunionId);
    });

    beforeEach(() => {
        cy.server();
        cy.route("PUT", "/goc/rest/**").as("actualizar");
        cy.route("POST", "/goc/rest/**").as("votar");
    });


    it("El usuario ha de poder cambiar el voto emitido de un punto abierto", () => {
        votacionesPage.getBotonAFavorPunto(puntoAbiertoId).click();
        waitUntilSendVoto();

        votacionesPage.getBotonEnContraPunto(puntoAbiertoId).click();
        waitUntilUpdateVoto();
    });

    it("El usuario ha de poder cambiar el voto emitido de un punto cerrado", () => {
        votacionesPage.getBotonAFavorPunto(puntoSecretoId).click();
        waitUntilSendVoto();

        votacionesPage.getBotonEnContraPunto(puntoSecretoId).click();
        waitUntilUpdateVoto();
    });
});

context("Cambio de voto en reunión que NO admite cambio de voto", () => {
    let votacionesPage;
    let reunionId = 5002;
    let puntoAbiertoId = 5003;

    before(() => {
        cy.loginAs(1, "Secretario");
        votacionesPage = new VotacionesPageObject(cy);
        votacionesPage.open(reunionId);
    });

    beforeEach(() => {
        cy.server();
        cy.route("PUT", "/goc/rest/**").as("actualizar");
        cy.route("POST", "/goc/rest/**").as("votar");
    });

    it("El usuario no puede cambiar el voto emitido de un punto abierto una vez ha votado", () => {
        votacionesPage.getBotonAFavorPunto(puntoAbiertoId).click();
        waitUntilSendVoto();

        votacionesPage.getBotonAFavorPunto(puntoAbiertoId).should('be.disabled');
        votacionesPage.getBotonEnContraPunto(puntoAbiertoId).should('be.disabled');
        votacionesPage.getBotonAbstencionPunto(puntoAbiertoId).should('be.disabled');
    });
});