import PublicacionesPageObject from "../../../pageobjects/PublicacionesPageObject";
import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";
import ResultadosVotacionPageObject from "../../../pageobjects/ResultadosVotacionPageObject";
import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";

const waitUntilSendVotoOk = () => cy.wait(["@votar"]).its("status").should("be.equal", 204);
const waitUntilSendVotoError = () => cy.wait(["@votar"]).its("status").should("be.equal", 500);
const waitUntilAssignSuplenteError = () => cy.wait(["@asignarSuplente"]).its("status").should("be.equal", 500);
const waitUntilDeleteSuplenteError = () => cy.wait(["@eliminarSuplente"]).its("status").should("be.equal", 500);

context("Suplencia de votación", () => {
    let publicacionesPage;
    let reunionPage;
    let reunionName = "Si un titular tiene suplente, el suplente podrá votar en su nombre";

    before(() => {
        reunionPage = new ReunionesPageObject(cy);
    })

    beforeEach(() => {
        cy.server();
        cy.route("PUT", "/goc/rest/**").as("actualizar");
        cy.route("POST", "/goc/rest/**").as("votar");
        cy.route("POST", "/goc/rest/**/suplente").as("asignarSuplente");
        cy.route("DELETE", "/goc/rest/**/suplente").as("eliminarSuplente");
    });

    it("Los usuarios que indiquen que no irán e indiquen un suplente, tienen que seguir teniendo acceso a la reunión", () => {
        const reunionId = 12000;
        publicacionesPage = new PublicacionesPageObject(cy);
        publicacionesPage.open(reunionId);
    });

    it("Si un titular no va y selecciona un suplente, el titular no podrá votar", () => {
        const reunionId = 12001;
        const puntoAbreviadoId = 12002;
        const votacionesPage = new VotacionesPageObject(cy);

        votacionesPage.open(reunionId);
        votacionesPage.getBotonAFavorPunto(puntoAbreviadoId).click();
        waitUntilSendVotoError();

        cy.on('window:alert', (str) => {
            expect(str).to.equal(`No puede votar, ha delegado su voto`);
        });
    });

    it("Si un titular no va y selecciona un suplente, el suplente podrá votar en su nombre", () => {
        const reunionId = 12002;
        const puntoAbreviadoId = 12003;

        cy.loginAs(12000, 'suplente');

        const votacionesPage = new VotacionesPageObject(cy);
        votacionesPage.open(reunionId);
        votacionesPage.getBotonAFavorPunto(puntoAbreviadoId).click();
        waitUntilSendVotoOk();
    });

    it("Si un titular ha votado no puede elegir un suplente", () => {
        const reunionId = 12003;
        const titularId = 12005;

        cy.loginAs(1, 'un votante');

        const publicacionesPage = new PublicacionesPageObject(cy);
        publicacionesPage.open(reunionId);
        publicacionesPage.getBotonGestionarSuplente(titularId).click();
        publicacionesPage.getSuplenteModalQuery().type('nico');
        publicacionesPage.getBotonBuscarSuplenteModal().click();
        publicacionesPage.getSuplenteModal(88849).click();
        publicacionesPage.getBotonAnyadirSuplente().click();
        waitUntilAssignSuplenteError();

        cy.on('window:alert', (str) => {
            expect(str).to.equal(`El usuario ya ha emitido un voto`);
        });
    });

    it("Si el suplente ha votado no se puede eliminar la suplencia", () => {
        const reunionId = 12003;
        const titularId = 12007;

        cy.loginAs(3, 'miembro con suplente');

        const publicacionesPage = new PublicacionesPageObject(cy);
        publicacionesPage.open(reunionId);
        publicacionesPage.getBotonEliminarSuplente(titularId).click();
        waitUntilDeleteSuplenteError();

        cy.on('window:alert', (str) => {
            expect(str).to.equal(`No puede eliminar suplente si el suplente ya ha votado`);
        });
    });

    it("En los datos de la votación, si vota el suplente, han de salir sus datos (no los del titular)", () => {
        const reunionId = 12003;
        const puntoId = 12004;

        cy.loginAs(1, 'miembro con suplente');

        const resultadosVotacionPage = new ResultadosVotacionPageObject(cy);
        resultadosVotacionPage.open(reunionId);
        resultadosVotacionPage.getNombresVotante(puntoId).contains('suplente del miembro');
    });

    it("En una reunion no telematica el suplente debe poder votar", () => {
        const reunionId = 12002;
        const puntoAbreviadoId = 12003;

        cy.login();

        reunionPage.open()
        reunionPage.edit(reunionName);
        reunionPage.getTelematicaCheck().click({force:true});
        reunionPage.getTelematicaCheck().should("not.be.checked");
        reunionPage.save();
        cy.wait(["@actualizar"]).its("status").should("be.equal", 200);

        cy.loginAs(12000, 'suplente');

        const votacionesPage = new VotacionesPageObject(cy);
        votacionesPage.open(reunionId);
        votacionesPage.getBotonEnContraPunto(puntoAbreviadoId).click();
        cy.wait(["@actualizar"]).its("status").should("be.equal", 200);
    })
});