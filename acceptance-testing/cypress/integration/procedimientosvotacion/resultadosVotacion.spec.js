import ResultadosVotacionPageObject from "../../../pageobjects/ResultadosVotacionPageObject";

context("Resultados de la votación", () => {
    let reunionId = 3000;
    let puntoVotacionSecretaId = 3001;
    let puntoVotacionAbiertaId = 3002;

    it("Si la votación es secreta, en el recuento de votos aparecerán los nombres de los votantes pero no las opciones votadas", () => {
        let resultadosVotacionPageObject = new ResultadosVotacionPageObject(cy);
        resultadosVotacionPageObject.open(reunionId);
        resultadosVotacionPageObject.getResultadosDetalle(puntoVotacionSecretaId).click();
        resultadosVotacionPageObject.getNombresVotante(puntoVotacionSecretaId).should('be.visible');
        resultadosVotacionPageObject.getNombresVotante(puntoVotacionSecretaId).contains('un votante');
        resultadosVotacionPageObject.getResultadosAFavor(puntoVotacionSecretaId).should('not.exist');
    });

    it("Si la votación es abierta, en el recuento de votos apareceran los nombres de los votantes con la opcion que ha elegido cada unos de ellos", () => {
        let resultadosVotacionPageObject = new ResultadosVotacionPageObject(cy);
        resultadosVotacionPageObject.open(reunionId);
        resultadosVotacionPageObject.getResultadosDetalle(puntoVotacionAbiertaId).click();
        resultadosVotacionPageObject.getNombresVotante(puntoVotacionAbiertaId).should('be.visible');
        resultadosVotacionPageObject.getNombresVotante(puntoVotacionAbiertaId).contains('un votante');
        resultadosVotacionPageObject.getResultadosAFavor(puntoVotacionAbiertaId).should('exist');
    });
});