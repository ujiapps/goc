import PublicacionesPageObject from "../../../../pageobjects/PublicacionesPageObject";

context("Control de emisión de votos cuando la reunión está cerrada", () => {
    let publicacionPage;
    let reunionId = 8002;

    before(() => {
       publicacionPage = new PublicacionesPageObject(cy);
       publicacionPage.open(reunionId);
    });

    it("Si la reunión está cerrada, no se ha de poder votar", () => {
        publicacionPage.getBotonAccederVotacion(reunionId).should('not.exist');
    });
});