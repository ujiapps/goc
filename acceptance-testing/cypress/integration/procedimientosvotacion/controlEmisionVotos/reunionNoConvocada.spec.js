import PublicacionesPageObject from "../../../../pageobjects/PublicacionesPageObject";

context("Control de emisión de votos cuando la reunión NO está convocada", () => {
    let publicacionPage;
    let reunionId = 8000;

    before(() => {
        publicacionPage = new PublicacionesPageObject(cy);
        publicacionPage.open(reunionId);
    });

    it("Si la reunión no está convocada no se puede acceder a la votacion", () => {
        publicacionPage.getBotonAccederVotacion(reunionId).should('not.exist');
    });
});