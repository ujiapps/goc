import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";
import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";

context("Emisión de votos", () => {
    let reunionId = 10000;
    let reunionName = "Los puntos no votables no se pueden votar";
    let reunionPage;
    let puntoNoVotableId = 10001;
    let puntoVotableId = 10004;
    let puntoVotableSecretoId = 10005;
    let votacionPage;
    
    before(() => {
        cy.loginAs(1, "Secretario");
        reunionPage = new ReunionesPageObject(cy);
        votacionPage = new VotacionesPageObject(cy)
        votacionPage.open(reunionId);
    });

    beforeEach(() => {
        cy.server();
        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("PUT", "/goc/rest/**").as("update");
    })

    it("Los puntos no votables no se pueden votar", () => {
        votacionPage.getPuntoNoVotable(puntoNoVotableId).should('exist');
        votacionPage.getOpcionVotacionPunto(puntoNoVotableId).should('not.exist');
    });


    it("Al realizar una votación sobre un punto publico deberia aparece el mensaje de confirma.", () => {
        votacionPage.getBotonAFavorPunto(puntoVotableId).click();
        votacionPage.getTextoConfirmacionVoto(puntoVotableId).should('be.visible');
    });

    it("Al realizar una votación sobre un punto publico de una reunion no telematica deberia aparece el mensaje de confirma.", () => {
        cy.login();

        reunionPage.open()
        reunionPage.edit(reunionName);
        reunionPage.getTelematicaCheck().click({force:true});
        reunionPage.getTelematicaCheck().should("not.be.checked");
        reunionPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);

        cy.loginAs(1, "Secretario");
        votacionPage.open(reunionId);
        votacionPage.getBotonEnContraPunto(puntoVotableId).click();
        votacionPage.getTextoConfirmacionVoto(puntoVotableId).should('be.visible');
    });

    it('Al realizar una votacion sobre un punto secreto no deberia aparecer el mensaje de confimacion', () => {
        votacionPage.getBotonAFavorPunto(puntoVotableSecretoId).click();
        votacionPage.getTextoConfirmacionVoto(puntoVotableSecretoId).should('not.visible')
    });
});