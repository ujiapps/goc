import PublicacionesPageObject from "../../../pageobjects/PublicacionesPageObject";
import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";

context("Control de asistencia a la reunion y a la votación", () => {
    const reunionId = 5001;
    let puntoAbiertoId = 5001;

    let publicacionPage;
    let votacionPage;

    beforeEach(() => {
        cy.loginAs(1, "Secretario");
        cy.server();

        cy.route("GET", "/goc/rest/**/miembros/otros").as("getOtros");
        cy.route("POST", "/goc/rest/**/votos/?*").as("votar");
        cy.route("POST", "/goc/rest/**/delegadovoto").as("delegar");
        cy.route("DELETE", "/goc/rest/**/delegadovoto").as("borrarDelegacion");
    })

    before(() => {
        publicacionPage = new PublicacionesPageObject(cy);
        votacionPage = new VotacionesPageObject(cy);
    });

    it("No debes poder excusar la asistencia si ya has votado.", ()=>{
        votacionPage.open(reunionId);
        votacionPage.getBotonAFavorPunto(puntoAbiertoId).click({force:true});

        publicacionPage.open(reunionId);
        publicacionPage.getBotonNoAsistir().should("be.disabled");
    })
});