import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";

context("Cierre/apertura votación", () => {
    let reunionId = 7000;
    let puntoOrdinarioVotacionCerradaId = 7001;
    let puntoOrdinarioVotacionAbiertaId = 7002;
    let puntoAbreviadoVotacionCerradaId = 7003;
    let puntoAbreviadoVotacionAbiertaId = 7004;
    let votacionesPage;
    let rectorId = 3;

    const waitUntilVotacionAbierta = () => cy.wait(["@abrirVotacion"]).its("status").should("be.equal", 204);
    const waitUntilVotacionCerrada = () => cy.wait(["@cerrarVotacion"]).its("status").should("be.equal", 200);

    before(() => {
        votacionesPage = new VotacionesPageObject(cy);
        votacionesPage.open(reunionId);
    });

    beforeEach(() => {
        cy.server();

        cy.route("PUT", "/goc/rest/votos/abre/**").as("abrirVotacion");
        cy.route("PUT", "/goc/rest/votos/cierra/**").as("cerrarVotacion");
    });

    it("El secretario ha de poder abrir la votación", () => {
        [puntoOrdinarioVotacionCerradaId, puntoAbreviadoVotacionCerradaId].forEach((id) => {
            votacionesPage.getBotonAbrirVotacionPunto(id).click();
            waitUntilVotacionAbierta();

            votacionesPage.getBotonAbrirVotacionPunto(id).should('not.exist');
            votacionesPage.getBotonCerrarVotacionPunto(id).should('exist');
        });
    });

    it("El secretario ha de poder cerrar la votación", () => {
         [puntoOrdinarioVotacionAbiertaId, puntoAbreviadoVotacionAbiertaId].forEach((id) => {
            votacionesPage.getBotonCerrarVotacionPunto(id).click();
            waitUntilVotacionCerrada();

            votacionesPage.getBotonCerrarVotacionPunto(id).should('not.exist');
            votacionesPage.getBotonAbrirVotacionPunto(id).should('exist');
        });
    });

    it("Un miembro no secretario no ha de poder abrir la votación ni cerrarla", () => {
        cy.loginAs(rectorId, 'Autorizado');

        votacionesPage.open(reunionId);
        votacionesPage.getBotonAbrirVotacionPunto(puntoOrdinarioVotacionCerradaId).should('not.exist');
        votacionesPage.getBotonCerrarVotacionPunto(puntoOrdinarioVotacionCerradaId).should('not.exist');
        votacionesPage.getBotonAbrirVotacionPunto(puntoAbreviadoVotacionCerradaId).should('not.exist');
        votacionesPage.getBotonCerrarVotacionPunto(puntoAbreviadoVotacionCerradaId).should('not.exist');
    });
});