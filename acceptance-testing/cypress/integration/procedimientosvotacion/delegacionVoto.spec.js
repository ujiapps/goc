import PublicacionesPageObject from "../../../pageobjects/PublicacionesPageObject";
import VotacionesPageObject from "../../../pageobjects/VotacionesPageObject";
import ResultadosVotacionPageObject from "../../../pageobjects/ResultadosVotacionPageObject";
import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";

const waitUntilAssignDelegacionError = () => cy.wait(["@delegar"]).its("status").should("be.equal", 500);
const waitUntilDeleteDelegacionError = () => cy.wait(["@borrarDelegacion"]).its("status").should("be.equal", 500);

context("Delegaciones de voto", () => {
    const reunionNoPermiteDelegacionId = 11000;
    const reunionPermiteDelegacionId = 11003;
    const reunionNoPermiteDelegarConVotos = 11004;
    const puntoIdReunionConDelegacion = 11007;

    let publicacionPage;
    let reunionPage;
    let votacionPage;

    beforeEach(() => {
        cy.loginAs(1, "Secretario");
        cy.server();

        cy.route("GET", "/goc/rest/**/miembros/otros").as("getOtros");
        cy.route("POST", "/goc/rest/**/votos/?*").as("votar");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("POST", "/goc/rest/**/delegadovoto").as("delegar");
        cy.route("DELETE", "/goc/rest/**/delegadovoto").as("borrarDelegacion");
    })

    before(() => {
        publicacionPage = new PublicacionesPageObject(cy);
        votacionPage = new VotacionesPageObject(cy);
        reunionPage = new ReunionesPageObject(cy);
    });

    it("Si la reunión no permite delegación de voto no se ha de poder delegar el voto", () => {
        const miembroId = 11001;
        publicacionPage.open(reunionNoPermiteDelegacionId);
        publicacionPage.getBotonDelegarVotacion(miembroId).should('not.exist');
    });

    it("Si el órgano no permite delegación múltiple de voto no se ha de poder delegar el voto en un miembro que ya se le ha delegado otro voto", () => {
        const miembroId = 11005;
        publicacionPage.open(reunionPermiteDelegacionId);
        publicacionPage.getBotonDelegarVotacion(miembroId).click();
        publicacionPage.seleccionaDelegado(2).click();
        publicacionPage.getBotonAnyadirDelegado().click();

        waitUntilAssignDelegacionError();
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`Ya está asignado como delegado en esta misma reunión`);
        });
    });

    it("Los usuarios que deleguen el voto, tienen que seguir teniendo acceso a la reunión", () => {
        const miembroId = 11007;
        cy.loginAs(3, "Delegador");
        publicacionPage.open(reunionPermiteDelegacionId);
        publicacionPage.getBotonBorrarDelegarVotacion(miembroId).should('exist');
        publicacionPage.getBotonBorrarDelegarVotacion(miembroId).should('be.enabled');
    });

    it("No se ha de poder delegar en caso de haber votado", () => {
        const miembroId = 11009;
        cy.loginAs(1, "unvotante");

        publicacionPage.open(reunionNoPermiteDelegarConVotos);
        publicacionPage.getBotonDelegarVotacion(miembroId).click();
        cy.wait(["@getOtros"]).its("status").should("be.equal", 200);
        publicacionPage.seleccionaDelegado(7).click();
        publicacionPage.getBotonAnyadirDelegado().click();

        waitUntilAssignDelegacionError();
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`El usuario ya ha emitido un voto`);
        });
    });

    it("No se ha de poder eliminar la delegación de voto si el delegado ha votado", () => {
        const miembroId = 11011;
        cy.loginAs(5, "miembrocondelegado");

        publicacionPage.open(reunionNoPermiteDelegarConVotos);
        publicacionPage.getBotonBorrarDelegarVotacion(miembroId).click();

        waitUntilDeleteDelegacionError();
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`El usuario ya ha emitido un voto`);
        });
    });

    it("No se ha de poder delegar en un miembro que no asiste", () => {
        const miembroId = 11009;
        cy.loginAs(1, "unvotante");

        publicacionPage.open(reunionNoPermiteDelegarConVotos);
        publicacionPage.getBotonDelegarVotacion(miembroId).click();
        cy.wait(["@getOtros"]).its("status").should("be.equal", 200);
        cy.get('@getOtros').its('response.body.data').then(miembros => {
            miembros.forEach((miembro) => {
                if (miembro.asistencia === "false") {
                    publicacionPage.seleccionaDelegado(miembro.miembroId).click();
                    publicacionPage.getBotonAnyadirDelegado().click();
                    waitUntilAssignDelegacionError();
                }
            });
        });
    });

    it("No se ha de poder delegar en caso de asistir", () => {
        const miembroId = 11006;
        cy.loginAs(2, "Asiste");
        publicacionPage.open(reunionPermiteDelegacionId);
        publicacionPage.getBotonDelegarVotacion(miembroId).should('exist');
        publicacionPage.getBotonDelegarVotacion(miembroId).should('not.be.enabled');
    });

    it("Si el órgano permite la delegación de voto se ha de poder delegar en caso de no asistir en algún miembro asistente de la reunión", () => {
        const miembroId = 11005;
        publicacionPage.open(reunionPermiteDelegacionId);
        publicacionPage.getBotonDelegarVotacion(miembroId).should('exist');
        publicacionPage.getBotonDelegarVotacion(miembroId).should('be.enabled');

        publicacionPage.getBotonDelegarVotacion(miembroId).click();
        cy.wait(["@getOtros"]).its("status").should("be.equal", 200);
        publicacionPage.seleccionaDelegado(4).click();
        publicacionPage.getBotonAnyadirDelegado().click();
        publicacionPage.getBotonBorrarDelegarVotacion(miembroId).should('be.enabled');
    });

    it("Si se ha delegado el voto, la persona que lo ha delegado no ha de poder votar", () => {
        cy.loginAs(3, "La persona que lo ha delegado");
        const votacionPage = new VotacionesPageObject(cy);
        votacionPage.open(reunionPermiteDelegacionId);
        votacionPage.getOpcionVotacionPunto(puntoIdReunionConDelegacion).should('not.exist');
    });

    it("Si se ha delegado el voto, el asistente al que se lo han delegado ha de seguir pudiendo poder votar", () => {
        cy.loginAs(5, 'delegado');
        const reunionId = 11030;
        const puntoAbiertoId = 11031;

        votacionPage.open(reunionId);
        votacionPage.getSelectorVotanteRepresentado().select('Votar en mi nombre');
        votacionPage.getBotonAFavorPunto(puntoAbiertoId).click();
        cy.wait(["@votar"]).its("status").should("be.equal", 204);
    });

    it("En los resultados de la votación si has votado como delegado debe indicar en representación de quién votas", () => {
        const reunionId = 11004;
        const puntoId = 11008;

        cy.loginAs(5, 'miembrocondelegado');

        const resultadosVotacionPage = new ResultadosVotacionPageObject(cy);
        resultadosVotacionPage.open(reunionId);
        resultadosVotacionPage.getNombresVotante(puntoId).contains('delegado (miembro con delegado)');
    });

    it("Si el miembro es delegado ha de poder ver el selector de votantes representados", () => {
        const reunionId = 11030;
        cy.loginAs(5, 'delegado');

        votacionPage.open(reunionId);
        votacionPage.getSelectorVotanteRepresentado().should('be.exist');
    });

    it("Si el miembro no es delegado no ha de poder ver el selector de votantes representados", () => {
        const reunionId = 11030;
        cy.loginAs(1, 'test');

        votacionPage.open(reunionId);
        votacionPage.getSelectorVotanteRepresentado().should('not.be.exist');
    });

    it("Al cambiar el votante representado se han de mostrar los votos emitidos del votante representado", () => {
        const reunionId = 11030;
        const puntoSecretoId = 11032;

        cy.loginAs(5, 'delegado');
        votacionPage.open(reunionId);
        votacionPage.getSelectorVotanteRepresentado().select('Miembro con delegado (id delegado: 5)');
        votacionPage.getBotonAFavorPunto(puntoSecretoId).click({force:true});
        cy.wait(["@votar"]).its("status").should("be.equal", 204);
    });

    it("Si se ha delegado el voto, el asistente al que se lo han delegado ha de seguir pudiendo poder votar", () => {
        const reunionId = 11030;
        const puntoSecretoId = 11032;

        cy.loginAs(5, 'delegado');
        votacionPage.open(reunionId);
        votacionPage.getSelectorVotanteRepresentado().select('Votar en mi nombre');
        votacionPage.getBotonAFavorPunto(puntoSecretoId).click({force:true});
        cy.wait(["@votar"]).its("status").should("be.equal", 204);
    });

    it("Si se ha delegado el voto en una reunion no telematica, el asistente al que se lo han delegado ha de seguir pudiendo poder votar", () => {
        const reunionId = 11030;
        const reunionName = "Se ha de poder votar por el miembro representado"
        const puntoSecretoId = 11031;

        cy.login();

        reunionPage.open()
        reunionPage.edit(reunionName);
        reunionPage.getTelematicaCheck().click({force:true});
        reunionPage.getTelematicaCheck().should("not.be.checked");
        reunionPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);

        cy.loginAs(5, 'delegado');
        votacionPage.open(reunionId);
        votacionPage.getSelectorVotanteRepresentado().select('Votar en mi nombre');
        votacionPage.getBotonAbstencionPunto(puntoSecretoId).click({force: true});
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });
});