import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../pageobjects/OrdenDiaPageObject";
import FormUploadPageObject from "../../pageobjects/FormUploadPageObject";

context("Tipo de votacion acorde al que tiene el organo.", () => {
    let reunionesPage;
    let ordenDiaPage;
    let formUploadPage;
    let reunionAbreviada = "Reunión con organo (Abreviado)";
    let reunionOrdinaria = "Reunión con organo (Ordinario)"
    let reunionSinOrgano = "Reunión sin organo";

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        reunionesPage = new ReunionesPageObject(cy);
        ordenDiaPage = new OrdenDiaPageObject(cy);
        formUploadPage = new FormUploadPageObject(cy)

        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
    });

    afterEach(() => {
        ordenDiaPage.goBack();
    })

    it("Con un órgano con tipo de votación abreviado, cuando se crea un punto de orden del dia debe marcarse por defecto el tipo de votacion abreviada", () => {
        reunionesPage.select(reunionAbreviada);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.add();
        ordenDiaPage.getProcedimientoAbreviado().should("be.enabled");
        ordenDiaPage.getProcedimientoAbreviado().should("be.checked");
        ordenDiaPage.getBotonCancelar().click();

    });

    it("Con un órgano con tipo de votación ordinario, cuando se crea un punto de orden del dia debe marcarse por defecto el tipo de votacion ordinario", () => {
        reunionesPage.select(reunionOrdinaria);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.add();
        ordenDiaPage.getProcedimientoOrdinario().should("be.enabled");
        ordenDiaPage.getProcedimientoOrdinario().should("be.checked");
        ordenDiaPage.getBotonCancelar().click();
    });

    it("Cuando una reunion no tiene asignado un organo, no debe marcarse por defecto ningun tipo de votación.", () => {
        reunionesPage.select(reunionSinOrgano);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.add();
        ordenDiaPage.getProcedimientoOrdinario().should("be.enabled");
        ordenDiaPage.getProcedimientoOrdinario().should("not.be.checked");
        ordenDiaPage.getProcedimientoAbreviado().should("be.enabled");
        ordenDiaPage.getProcedimientoAbreviado().should("not.be.checked");
        ordenDiaPage.getBotonCancelar().click();
    });
});