import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../../pageobjects/OrdenDiaPageObject"
import GocPageObject from "../../../pageobjects/GocPageObject";
import FormUploadPageObject from "../../../pageobjects/FormUploadPageObject";
import FormPageObject from "../../../pageobjects/FormPageObject";

context("Reuniones, Grid acceso externos", () => {
    let reunionesPage;
    let reunionName = "Reunión abierta";
    let ordenDiaPage;
    let gocPage;
    let fileName = 'text.txt';
    let fileType = 'multipart/form-data';
    let formUploadPage;
    let formPage;

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("GET", "/goc/rest/**/checktoclose*").as("check");
        cy.route("GET", "/goc/rest/**/acuerdos*").as("acuerdos");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        reunionesPage = new ReunionesPageObject(cy);
        ordenDiaPage = new OrdenDiaPageObject(cy);
        gocPage = new GocPageObject(cy);
        formPage = new FormPageObject(cy);
        formUploadPage = new FormUploadPageObject(cy);

        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
    });

    it("Filtra reuniones por tipo de órgano", () => {
        reunionesPage.find(reunionName).should("be.visible");

        reunionesPage.filterByTipo("Órgano externo");
        reunionesPage.find(reunionName).should("not.exist");

        reunionesPage.disableFilterByTipo();
        reunionesPage.find(reunionName).should("be.visible");
    });

    it("Filtra reuniones por órgano", () => {
        reunionesPage.find(reunionName).should("be.visible");

        reunionesPage.filterByOrgano("Dirección general");
        reunionesPage.find(reunionName).should("not.exist");

        reunionesPage.disableFilterByOrgano();
        reunionesPage.find(reunionName).should("be.visible");
    });

    it("Debe subir un fichero y editar su descripción", ()=>{
        reunionesPage.select(reunionName);
        reunionesPage.openDocumentacionAdjunta();
        cy.upload_file(fileName, fileType, "input[name^='documento']");
        formUploadPage.typeDescriptionFieldParaSubida("test file");
        formUploadPage.clickUpload("documento");
        formUploadPage.closeForm();
    });

    it("Debe aparecer un responsable de acta por defecto si en la reunion hay un miembro que ostenta ese cargo", () =>{
        reunionesPage.add();

        reunionesPage.addOrgano();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.selectOrgano('Junta de gobierno');

        reunionesPage.fill("prueba", "prueba");
        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        reunionesPage.select("prueba");
        reunionesPage.selectAccion("Cerrar acta");
        cy.wait(["@check"]).its("status").should("be.equal", 200);
        cy.wait(["@acuerdos"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        cy.wait(1000);

        reunionesPage.getSelectorResponsable().click();
        gocPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.getFocusedElement().should("exist");
        reunionesPage.getFocusedElement().should("contain", "Secretario (Secretario)");
        reunionesPage.getBotonCancelar().click();
    })

    it("Cierra una reunión", () => {
        reunionesPage.select(reunionName);
        reunionesPage.selectAccion("Cerrar acta");
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        cy.firmarYCerrar(reunionesPage);

        cy.wait(["@update"]).its("status").should("be.equal", 204);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        reunionesPage.getLoadMask().should("not.be.visible");
        reunionesPage.find(reunionName).should("not.exist");
    });

    it("Debe duplicar una reunión", () => {
        reunionesPage.select("Mi reunión");
        reunionesPage.selectAccion("Duplicar la reunión");
        reunionesPage.clear();
        reunionesPage.fill("Duplicada","Duplicada alt");
        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        reunionesPage.find("Duplicada").should("be.visible")
    })
});
