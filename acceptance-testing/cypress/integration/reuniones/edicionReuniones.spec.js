import ReunionesPageObject from "../../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../../pageobjects/OrdenDiaPageObject"
import GocPageObject from "../../../pageobjects/GocPageObject";
import FormUploadPageObject from "../../../pageobjects/FormUploadPageObject";
import FormPageObject from "../../../pageobjects/FormPageObject";

context("Reuniones, Grid acceso externos", () => {
    let reunionesPage;
    let random = Math.random().toString(36).substr(2, 5);
    let randName = "Reunion Test " + random.toString();
    let ordenDiaPage;
    let gocPage;
    let formUploadPage;
    let formPage;

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        reunionesPage = new ReunionesPageObject(cy);
        ordenDiaPage = new OrdenDiaPageObject(cy);
        gocPage = new GocPageObject(cy);
        formPage = new FormPageObject(cy);
        formUploadPage = new FormUploadPageObject(cy);

        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
    });

    it("Añade una reunión", () => {
        reunionesPage.add();

        reunionesPage.addOrgano();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.selectOrgano('Dirección general');

        reunionesPage.fill(randName, randName);
        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

    });

    it("Edita una reunión", () => {
        reunionesPage.edit(randName);
        reunionesPage.clear();
        reunionesPage.fill(randName, "Asunto alternativo" + Math.random(), randName);
        reunionesPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe eliminar un órgano de una reunión", () => {
        reunionesPage.edit(randName);

        //Si el titulo automatico esta activado hace fallar el test, por que el asunto se modifica a {organo} al eliminarlo de la reunión
        reunionesPage.deleteOrgano('Dirección general');
        reunionesPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe eliminar una reunión", () => {
        reunionesPage.select(randName);
        reunionesPage.delete();

        cy.wait(["@delete"]).its("status").should("be.equal", 200);
    });
});
