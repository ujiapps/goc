import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../pageobjects/OrdenDiaPageObject"
import FormUploadPageObject from "../../pageobjects/FormUploadPageObject";

context("Puntos orden día", () => {
    let reunionesPage;
    let reunionName = "Mi reunión";
    let random = Math.random().toString(36).substr(2, 5);
    let randName = "Punto orden día Test " + random.toString();
    let puntoOrdenDiaName = "Punto orden día ficheros";
    let ordenDiaPage;
    let fileName = 'text.txt';
    let fileType = 'multipart/form-data';
    let formUploadPage;


    before(() => {
        cy.login();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        reunionesPage = new ReunionesPageObject(cy);
        ordenDiaPage = new OrdenDiaPageObject(cy);
        formUploadPage = new FormUploadPageObject(cy)
    });

    it("Debe añadir un punto de orden del día",() => {
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");

        reunionesPage.select(reunionName);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.add();
        ordenDiaPage.fill(randName, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe editar un punto de orden del día", () => {
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.select(randName);
        ordenDiaPage.edit(randName);
        cy.get("[id^='formOrdenDia'").contains("Edición").should("be.visible");
        ordenDiaPage.clear();
        ordenDiaPage.fill(randName + "EDITADO", "Título alternativo " + Math.random());
        ordenDiaPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe añadir un segundo punto de orden del día", () => {
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.add();
        ordenDiaPage.fill("new", "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe modificar el orden de los puntos del orden del día de una reunión", () =>{
        ordenDiaPage.getLoadMask().should("not.be.visible");
        ordenDiaPage.upOrden(puntoOrdenDiaName);
        ordenDiaPage.getLoadMask().should("not.be.visible");
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    function selectPuntoDeOrdenDiaByReunion() {
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
        reunionesPage.select(reunionName);
        ordenDiaPage.select(puntoOrdenDiaName);
    }

    function uploadTestFile(tipo) {
        cy.upload_file(fileName, fileType, "input[name^='documento']");
        formUploadPage.typeDescriptionFieldParaSubida("test file");
        formUploadPage.getSelectorTipo().focus().type("{downarrow}", {force: true}).type("{enter}", {force: true});
        formUploadPage.clickUpload(tipo);
        formUploadPage.dblclickDescriptionField();
        formUploadPage.typeDescriptionFieldDeFicheroSubido("Editado");
        formUploadPage.closeForm();
    }

    it("Debe subir un fichero y editar su descripción", ()=>{
        selectPuntoDeOrdenDiaByReunion();
        ordenDiaPage.openDocumentacionAdjunta();
        uploadTestFile("documento");
    });

    it("Debe eliminar un punto de orden del día de una reunión", () => {
        ordenDiaPage.select(randName);
        ordenDiaPage.delete();

        cy.wait(["@delete"]).its("status").should("be.equal", 200);
    });
});
