import PublicacionesPageObject from "../../pageobjects/PublicacionesPageObject";


context("Acta provisional visible si los organos tienen la opcion activa.", () => {
    let publicacionesPage;
    let reunionSinActaProvisional = 24002;
    let reunionActaProvisional = 22003;
    let url = "http://localhost:8080/goc/rest/actas/"+reunionSinActaProvisional

    before(() => {
        cy.loginAs(1, "Secretario");


        publicacionesPage = new PublicacionesPageObject(cy);
        publicacionesPage.open(reunionSinActaProvisional);
    });

    beforeEach(() => {
        cy.server();
    });

    it("No se debe mostrar la opcion de ver el acta provisional si hay un organo en la reunion que no tiene la opcion activada", ()=>{
        publicacionesPage.open(reunionSinActaProvisional);
        publicacionesPage.getBotonActaProvisional().should("not.exist");
    });

    it("Se debe mostrar la opcion de ver el acta provisional si todos los organos de la reunion tienen la opcion activada", ()=>{
        publicacionesPage.open(reunionActaProvisional);
        publicacionesPage.getBotonActaProvisional().should("be.visible");
    });

    it("No se debe acceder al acta manualmente", () => {
        cy.request({
            method: 'GET',
            failOnStatusCode: false,
            url: url,
        }).then(response =>{
            expect(response.status).to.be.eq(500)
        })
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`El acta provisional está deshabilitada`);
        });
    })

});