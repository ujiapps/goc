import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../pageobjects/OrdenDiaPageObject";

context("Votaciones", () => {
    let reunionesPage;
    let ordenDiaPage;
    let randomName;

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        cy.server();
        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");
        cy.route("GET", "/goc/rest/reuniones/*/invitados**").as("getInvitados");

        reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
        ordenDiaPage = new OrdenDiaPageObject(cy);
        randomName = Math.random().toString(36).substr(2, 5);
    });

    it("Si la reunión no es telemática las opciones de votación deben salir habilitadas", () => {
        reunionesPage.add();

        reunionesPage.getVotacionCheck().should("be.enabled");
        reunionesPage.getTelematicaCheck().should("be.checked");
        reunionesPage.getTelematicaCheck().click();
        reunionesPage.getTelematicaCheck().should("not.be.checked");

        reunionesPage.getVotacionCheck().should("be.enabled");
        reunionesPage.getAdmiteCambioVotoCheck().should("be.enabled");
        reunionesPage.getCheckResultadosCerrarReunion().should("be.enabled");
        reunionesPage.getCheckResultadosCerrarReunion().should("be.checked");

        reunionesPage.getBotonCancelar().click();
    });

    it("Si la reunión no admite votación el resto de opciones de votación deben salir deshabilitadas", () => {
        reunionesPage.add();

        reunionesPage.getVotacionCheck().should("be.checked");
        reunionesPage.getAdmiteCambioVotoCheck().should("be.checked");
        reunionesPage.getAdmiteCambioVotoCheck().should("be.enabled");

        reunionesPage.getVotacionCheck().click();
        reunionesPage.getVotacionCheck().should("not.be.checked");
        reunionesPage.getAdmiteCambioVotoCheck().should("be.disabled");
        reunionesPage.getAdmiteCambioVotoCheck().should("not.be.checked");
        reunionesPage.getCheckResultadosCerrarReunion().should("be.disabled");
        reunionesPage.getCheckResultadosFechaFin().should("be.disabled");
        reunionesPage.getCheckResultadosFechaFin().should("not.be.checked");

        reunionesPage.getBotonCancelar().click();
    });

    it("Crear una reunión telemática sin votación", () => {
        reunionesPage.add();

        reunionesPage.getVotacionCheck().should("be.checked");
        reunionesPage.getVotacionCheck().click();
        reunionesPage.fill("Telematica sin votacion", "Telematica sin votacion");
        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        reunionesPage.edit("Telematica sin votacion");
        reunionesPage.getVotacionCheck().should("be.enabled");
        reunionesPage.getVotacionCheck().should("not.be.checked");
        reunionesPage.getBotonCancelar().click();
    });

    it("Editar la reunión telemática y permitir la votación", () => {
        reunionesPage.edit("Telematica sin votacion");
        reunionesPage.getVotacionCheck().click();
        reunionesPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);

        reunionesPage.edit("Telematica sin votacion");
        reunionesPage.getVotacionCheck().should("be.enabled");
        reunionesPage.getVotacionCheck().should("be.checked");
        reunionesPage.getBotonCancelar().click();
    });

    it("Crear una reunión telemática que no admita cambio de voto", () => {
        reunionesPage.add();

        reunionesPage.getAdmiteCambioVotoCheck().should("be.checked");
        reunionesPage.getAdmiteCambioVotoCheck().click();
        reunionesPage.fill(randomName, randomName);
        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        reunionesPage.edit(randomName);
        reunionesPage.getAdmiteCambioVotoCheck().should("be.enabled");
        reunionesPage.getAdmiteCambioVotoCheck().should("not.be.checked");
        reunionesPage.getBotonCancelar().click();
    });

    it("Crear una reunión telemática que admita cambio de voto", () => {
        reunionesPage.add();

        reunionesPage.getAdmiteCambioVotoCheck().should("be.checked");
        reunionesPage.fill(randomName, randomName);
        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        reunionesPage.edit(randomName);
        reunionesPage.getAdmiteCambioVotoCheck().should("be.enabled");
        reunionesPage.getAdmiteCambioVotoCheck().should("be.checked");
        reunionesPage.getBotonCancelar().click();
    });

    it("Crear un punto orden día con votacion abierta", () => {
        reunionesPage.add();

        const name = Math.random().toString(36).substr(2, 5);

        reunionesPage.getTelematicaCheck().should("be.checked");
        reunionesPage.getVotacionCheck().check();
        reunionesPage.fill(name, name);
        reunionesPage.save();

        reunionesPage.select(name);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.fill(name, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        ordenDiaPage.edit(name);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().should('be.checked');
        ordenDiaPage.cancel();
        ordenDiaPage.goBack()
    });

    it("Crear un punto orden día con votación secreta", () => {
        reunionesPage.add();

        reunionesPage.getTelematicaCheck().should("be.checked");
        reunionesPage.getVotacionCheck().check();
        reunionesPage.fill(randomName, randomName);
        reunionesPage.save();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");

        reunionesPage.select(randomName);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionSecreta().click();
        ordenDiaPage.fill(randomName, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(randomName);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionSecreta().should('be.checked');
        ordenDiaPage.cancel();
        ordenDiaPage.goBack()
    });

    it("Crear un punto orden día con votación indefinida", () => {
        reunionesPage.add();

        reunionesPage.getTelematicaCheck().should("be.checked");
        reunionesPage.getVotacionCheck().check();
        reunionesPage.fill(randomName, randomName);
        reunionesPage.save();

        reunionesPage.select(randomName);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionIndefinida().click();
        ordenDiaPage.fill(randomName, "Título alternativo");
        ordenDiaPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(randomName);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionIndefinida().should('be.checked');
        ordenDiaPage.cancel();
        ordenDiaPage.goBack()
    });

    it("Editar un punto orden día con votación abierta", () => {
        reunionesPage.add();

        reunionesPage.getTelematicaCheck().should("be.checked");
        reunionesPage.getVotacionCheck().check();
        reunionesPage.fill(randomName, randomName);
        reunionesPage.save();

        reunionesPage.select(randomName);
        reunionesPage.getBotonPuntosOrdenDia().click();

        ordenDiaPage.add();
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().click();
        ordenDiaPage.fill(randomName, "Título alternativo");
        ordenDiaPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(randomName);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionSecreta().click();
        ordenDiaPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);

        cy.wait(["@get"]).its("status").should("be.equal", 200);
        ordenDiaPage.edit(randomName);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.getVotacionAbierta().should('not.be.checked');
        ordenDiaPage.getVotacionSecreta().should('be.checked');
        ordenDiaPage.cancel();
        ordenDiaPage.goBack()
    });

    it("Crear una reunión con visualización de resultados al cerrar la reunion", () => {
        reunionesPage.add();

        reunionesPage.getVotacionCheck().should("be.checked");
        reunionesPage.fill(randomName, randomName);
        reunionesPage.getCheckResultadosCerrarReunion().click();

        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        reunionesPage.edit(randomName);
        reunionesPage.getCheckResultadosCerrarReunion().should("be.checked");
        reunionesPage.getBotonCancelar().click();
    });

    it("Crear una reunión con visualización de resultados al alcanzar fecha fin", () => {
        reunionesPage.add();

        reunionesPage.getVotacionCheck().should("be.checked");
        reunionesPage.fill(randomName, randomName);
        reunionesPage.getCheckResultadosFechaFin().should("be.not.checked");
        reunionesPage.fillFechaFinPosterior();

        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        reunionesPage.edit(randomName);
        reunionesPage.getCheckResultadosFechaFin().should("be.not.checked");
        reunionesPage.getBotonCancelar().click();
    });
});
