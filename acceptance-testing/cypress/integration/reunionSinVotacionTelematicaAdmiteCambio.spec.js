import OrganosPageObject from "../../pageobjects/OrganosPageObject"
import MiembrosPageObject from "../../pageobjects/MiembrosPageObject"
import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../pageobjects/OrdenDiaPageObject";
import GocPageObject from "../../pageobjects/GocPageObject";

context("Reunion sin votacion telematica admite cambio a votacion telematica.", () => {
    let organosPage;
    let miembrosPage;
    let reunionesPage;
    let ordenDiaPage;
    var gocPage;
    let randName = 'Reunión no telematica'

    beforeEach(() => {
        cy.login();
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        organosPage = new OrganosPageObject(cy);
        miembrosPage = new MiembrosPageObject(cy);
        reunionesPage = new ReunionesPageObject(cy);
        ordenDiaPage = new OrdenDiaPageObject(cy);
        gocPage = new GocPageObject(cy);

        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
    });

    it("Cuando se crea una reunion sin votación telematica y se crea un punto de orden del dia debe poderse cambiar la votación a telematica.",() => {

        reunionesPage.edit(randName);
        reunionesPage.getTelematicaCheck().click();
        reunionesPage.getTelematicaCheck().should("be.checked");
        reunionesPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });
});