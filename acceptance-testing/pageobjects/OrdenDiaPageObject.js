import TabPageObject from "./TabPageObject"

class OrdenDiaPageObject extends TabPageObject {
    constructor(wrapper) {
        super("", wrapper);
        this.wrapper = wrapper;
        this.ordenDiaId = "ordenDia";
    }

    select(id) {
        super._select(this.ordenDiaId, id);
    }

    add() {
        super._add(this.ordenDiaId);
    }

    edit(id) {
        this.select(id)
        super._edit(this.ordenDiaId);
        this.getFormTitle().contains(id);
    }

    delete() {
        return super._delete(this.ordenDiaId);
    }

    getFormTitle() {
        return this.wrapper.get("div[id^='formOrdenDia'][id$='header-title']");
    }

    getLoadMask() {
        return super._getLoadMask(this.ordenDiaId);
    }

    cancel() {
        this.getBotonCancelar().click();
    }

    goBack() {
        this._getGrid(this.ordenDiaId).contains('Volver').click({force: true});
    }

    clear() {
        this.wrapper.get("textarea[name='titulo']").clear();
        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this.wrapper.get("textarea[name='tituloAlternativo']").clear();
            }
        });
    }

    fill(titulo, tituloAlternativo) {
        this.wrapper.get("textarea[name='titulo']").type(titulo);
        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this.wrapper.get("textarea[name='tituloAlternativo']").type(tituloAlternativo);
            }
        });
    }

    save() {
        this.wrapper.get("div[id^='formOrdenDia']").contains("Guardar").click();
    }

    upOrden(punto) {
        this._getGrid(this.ordenDiaId).contains("td.x-grid-cell", punto).parent().find("[data-qtip='Subir']").click();
    }

    openDocumentacionAdjunta() {
        this.wrapper.get("a[id^='button']:visible").contains("Documentación").click();
    }

    getOpcionPublica() {
        return this.wrapper.get("input[id^='radioVotacionPublica']");
    }

    getOpcionPrivada() {
        return this.wrapper.get("input[id^='radioVotacionPrivada']");
    }

    getVotacionAbierta() {
        return this.wrapper.get("input[id^='checkVotacionAbiertaTrue']")
    }

    getVotacionSecreta() {
        return this.wrapper.get("input[id^='checkVotacionAbiertaFalse']")
    }

    getVotacionIndefinida() {
        return this.wrapper.get("input[id^='checkVotacionAbiertaNull']")
    }

    getBotonCancelar() {
        return this.wrapper.get("div[id^='formOrdenDia']").contains("Cancelar");
    }

    getProcedimientoOrdinario() {
        return this.wrapper.get("input[id^='checkProcedimientoOrdinario']");
    }

    getProcedimientoAbreviado() {
        return this.wrapper.get("input[id^='checkProcedimientoAbreviado']");
    }

    getMayoriaSimple() {
        return this.wrapper.get("input[id^='checkMayoriaSimple']");
    }

    getMayoriaAbsolutaPresentes() {
        return this.wrapper.get("input[id^='checkMayoriaAbsolutaPresentes']");
    }

    getMayoriaAbsolutaOrgano() {
        return this.wrapper.get("input[id^='checkMayoriaAbsolutaOrgano']");
    }

    getVotarEnContraCheck(){
        return this.wrapper.get("input[name^='votableEnContra']")
    }

}

export default OrdenDiaPageObject;