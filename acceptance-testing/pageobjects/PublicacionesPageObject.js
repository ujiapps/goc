import PublicPageObject from "./PublicPageObject";

class ReunionesPageObject extends PublicPageObject {
    constructor(wrapper) {
        super(wrapper);
        this.wrapper = wrapper;
    }

    open(reunionId) {
        this.wrapper.visit(`/goc/rest/publicacion/reuniones/${reunionId}`);
    }

    getEnlaceVotar() {
        return this.wrapper.contains("a", "Votar electrónicamente");
    }

    getBotonAsistir() {
        return this.wrapper.get("button[name='confirmar']");
    }

    getBotonNoAsistir() {
        return this.wrapper.get("button[name='denegar']");
    }

    getJusticificacionTextArea() {
        return this.wrapper.get("textarea[name='motivo-ausencia']:visible");
    }

    getContadorCaracteres(){
        return this.wrapper.get("[data-cy='contador'");
    }

    getBotonEnviarJustificacion() {
        return this.wrapper.get("button[name='confirmar-ausencia']");
    }

    getComentarioTextArea() {
        return this.wrapper.get("div.nuevo-comentario-punto > textarea:visible,div.nuevo-comentario > textarea:visible");
    }

    getBotonEnviarComentario() {
        return this.wrapper.get("button.post-comentario-punto:visible,button.post-comentario:visible");
    }

    getBotonAnyadirComentario() {
        return this.wrapper.get("button.add-comentario");
    }

    getBotonAnyadirComentarioEnPunto(punto) {
        return this._getPuntosOrdenDia(punto).find("input.button[value='Añadir comentario']");
    }

    getBotonOcultarComentarios() {
        return this.wrapper.get("a.enlace-ocultar-comentarios");
    }

    getBotonVerComentarios() {
        return this.wrapper.get("a.enlace-ver-comentarios");
    }

    getListaComentarios() {
        return this.wrapper.contains("strong", "Comentarios:").parent();
    }

    getBotonEliminarComentario() {
        return this.wrapper.get("[data-cy='borrarComentario']").last();
    }

    getBotonDelegarVotacion(miembroId) {
        return this.wrapper.get("[data-cy='delegar_voto-" + miembroId + "']");
    }

    getBotonBorrarDelegarVotacion(miembroId) {
        return this.wrapper.get("[data-cy='borrar_delegar_voto-" + miembroId + "']");
    }

    seleccionaDelegado(personaId) {
        return this.wrapper.get("input[type='radio'][name='persona'][value='" + personaId + "']");
    }

    getBotonAnyadirDelegado() {
        return this.wrapper.get(".button[name='add-delegado-voto']");
    }

    _getUltimoComentarioPunto(punto) {
        return this._getPuntosOrdenDia(punto).find("div.comentario:not([data-comentario-padre-id]):visible,div.comentario[data-comentario-padre-id='null']:visible").last();
    }

    getBotonResponderUltimoComentario(punto) {
        return this._getUltimoComentarioPunto(punto).find("a.enlace-responder-comentario");
    }

    getBotonVerRespuestasUltimoComentario(punto) {
        return this._getUltimoComentarioPunto(punto).find("a.enlace-ver-respuestas");
    }

    getBotonOcultarRespuestasUltimoComentario(punto) {
        return this._getUltimoComentarioPunto(punto).find("a.enlace-ocultar-respuestas");
    }

    getListaRespuestasUltimoComentario(punto) {
        return this._getUltimoComentarioPunto(punto).next().find("div.comentario");
    }

    getBotonAccederVotacion(reunionId) {
        return this.wrapper.dataCy(`boton-acceder-votacion-${reunionId}`);
    }

    getBotonResultadoVotacion(reunionId) {
        return this.wrapper.dataCy(`boton-acceder-resultado-votacion-${reunionId}`);
    }

    getBotonGestionarSuplente(miembroId) {
        return this.wrapper.dataCy(`gestionar_suplente-${miembroId}`);
    }

    getBotonEliminarSuplente(miembroId) {
        return this.wrapper.dataCy(`borrar_suplente-${miembroId}`);
    }

    getSuplenteModalQuery() {
        return this.wrapper.get("input[name='query-persona']");
    }

    getBotonBuscarSuplenteModal() {
        return this.wrapper.get("button[name='busca-persona']");
    }

    getSuplenteModal(personaId) {
        return this.wrapper.get(`input[name='persona'][value='${personaId}']`);
    }

    getBotonAnyadirSuplente() {
        return this.wrapper.get("button[name='add-suplente']");
    }

    getBotonActaProvisional(){
        return this.wrapper.contains("a", "Borrador del acta");
    }
}

export default ReunionesPageObject;