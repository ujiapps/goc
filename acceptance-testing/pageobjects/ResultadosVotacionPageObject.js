import PublicPageObject from "./PublicPageObject";

class ResultadosVotacionPageObject extends PublicPageObject {
    constructor(wrapper) {
        super(wrapper);
        this.wrapper = wrapper;
    }

    open(reunionId) {
        this.wrapper.visit(`/goc/rest/votos/resultado/votacion/${reunionId}`);
    }

    getNombresVotante(puntoId) {
        return this.wrapper.dataCy(`votantes_punto-${puntoId}`);
    }

    getResultadosAFavor(puntoId) {
        return this.wrapper.dataCy(`resultado_a_favor_punto-${puntoId}`);
    }

    getResultadosDetalle(puntoVotacionAbiertaId) {
        return this.wrapper.dataCy(`boton_resultados_detalle-${puntoVotacionAbiertaId}`)
    }

    getVotosEnContra(puntoId){
        return this.wrapper.dataCy(`votos-en-contra-${puntoId}`)
    }
}

export default ResultadosVotacionPageObject;