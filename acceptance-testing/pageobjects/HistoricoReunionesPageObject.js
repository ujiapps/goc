import TabPageObject from "./TabPageObject";

class HistoricoReunionesPageObject extends TabPageObject {

    constructor(wrapper) {
        super("Histórico de reuniones", wrapper);
        this.wrapper = wrapper;
        this.historicoReunionId = "historicoReunion";
    }

    getLoadMask() {
        return super._getLoadMask(this.historicoReunionId);
    }

    find(id) {
        return super._find(this.historicoReunionId, id);
    }

    filterByAsunto(asunto) {
        this.wrapper.get("input[name^=searchReunion]").type(asunto);
    }

    clearSearchAsuntoField() {
        this.wrapper.get("input[name^=searchReunion]").clear();
    }

    filterByTipo(tipo) {
        this.wrapper.get("input[id^=comboReunionTipoOrgano]").click();
        this.wrapper.get("div[id^=comboReunionTipoOrgano][data-ref=listWrap]").contains("li", tipo).click();
    }

    disableFilterByTipo() {
        this.wrapper.get("div.x-form-clear-trigger[id^=comboReunionTipoOrgano]").click({force: true});
    }

    filterByOrgano(organo) {
        this.wrapper.get("input[id^=comboOrgano]").click();
        this.wrapper.get("div[id^=comboOrgano][data-ref=listWrap]").contains("li", organo).click();
    }

    disableFilterByOrgano() {
        this.wrapper.get("div.x-form-clear-trigger[id^=comboOrgano]").click({force: true});
    }

    select(codigo) {
        super._select(this.historicoReunionId, codigo);
    }

    fillMotivoReapertura() {
        this.wrapper.get("textarea:visible").type("reunión reabierta con cypress");
        this.wrapper.get("a[id^=button]").contains("Reabrir").parent().parent().click();
    }

}

export default HistoricoReunionesPageObject;