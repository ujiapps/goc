import PublicPageObject from "./PublicPageObject";

class VotacionesPageObject extends PublicPageObject {
    constructor(wrapper) {
        super(wrapper);
        this.wrapper = wrapper;
    }

    open(reunionId) {
        this.wrapper.visit(`/goc/rest/publicacion/reuniones/puntos/${reunionId}`);
    }

    getOpcionVotacionPunto(puntoId) {
        return this.wrapper.dataCy(`opciones_votacion_punto-${puntoId}`);
    }

    getTextoConfirmacionVoto(puntoId){
        return this.wrapper.dataCy(`texto_confirmacion_votacion_realizada-${puntoId}`);
    }

    getEstadoCerradaVotacionPunto(puntoId) {
        return this.wrapper.dataCy(`votacion_cerrada_punto-${puntoId}`);
    }

    getEstadoAbiertaVotacionPunto(puntoId) {
        return this.wrapper.dataCy(`votacion_abierta_punto-${puntoId}`);
    }

    getBotonAbrirVotacionPunto(puntoId) {
        return this.wrapper.dataCy(`abrir_votacion_punto-${puntoId}`);
    }

    getBotonCerrarVotacionPunto(puntoId) {
        return this.wrapper.dataCy(`cerrar_votacion_punto-${puntoId}`);
    }

    getBotonAFavorPunto(puntoId) {
        return this.wrapper.dataCy(`boton_favor_votacion_punto-${puntoId}`);
    }

    getBotonEnContraPunto(puntoId) {
        return this.wrapper.dataCy(`boton_contra_votacion_punto-${puntoId}`);
    }

    getBotonAbstencionPunto(puntoId) {
        return this.wrapper.dataCy(`boton_blanco_votacion_punto-${puntoId}`);
    }

    getPuntoNoVotable(puntoId) {
        return this.wrapper.dataCy(`punto-no-votable-${puntoId}`);
    }

    getSelectorVotanteRepresentado() {
        return this.wrapper.dataCy('selector-votante');
    }
    
    getGuardarVotacionPuntoSecreto(puntoId) {
		return this.wrapper.dataCy(`guardar_votacion_punto_secreto-${puntoId}`);
	}
}

export default VotacionesPageObject;