import FormPageObject from "./FormPageObject";

class FormUploadPageObject extends FormPageObject{
    constructor(wrapper) {
        super(wrapper);
    }

    typeDescriptionFieldParaSubida(text) {
        this.wrapper.get("input[placeholder^='Descripción'][type='text']").type(text);
    }

    typeDescriptionFieldDeFicheroSubido(text){
        this.wrapper.get("input[type='text'][name='descripcion'][aria-labelledby^='gridcolum']").type(text);
    }

    clickUpload(tipo){
        this.wrapper.get("a[id^='button']").contains("Subir " + tipo ).click();
    }

    dblclickDescriptionField(){
        this.wrapper.get("div[class^='x-grid']").contains("test file").click().dblclick();
    }

    closeForm(){
        this.wrapper.get(".x-tool-close:visible").click();
    }

    getSelectorTipo() {
        return this.wrapper.get("input[id^='tipoAdjuntoSubida-']")
    }
}
export default FormUploadPageObject;