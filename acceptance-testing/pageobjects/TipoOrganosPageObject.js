import TabPageObject from "./TabPageObject"

class TipoOrganosPageObject extends TabPageObject {
    constructor(wrapper) {
        super("Tipos de órganos", wrapper);
        this.wrapper = wrapper;
        this.tipoOrganoId = "tipoOrgano";
    }

    select(id) {
        return super._select(this.tipoOrganoId, id);
    }

    add() {
        return super._add(this.tipoOrganoId);
    }

    edit() {
        return super._edit(this.tipoOrganoId);
    }

    delete() {
        return super._delete(this.tipoOrganoId);
    }

    getLoadMask() {
        return super._getLoadMask(this.tipoOrganoId);
    }

    clear() {
        this._getInput("codigo").clear().type(" ").clear();
        this._getInput("nombre").clear();

        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("nombreAlternativo").clear();
            }
        });
    }

    fill(codigo, nombre, nombreAlternativo) {
        this._getInput("codigo").type(codigo);
        this._getInput("nombre").type(nombre);

        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("nombreAlternativo").type(nombreAlternativo);
            }
        });
    }
}

export default TipoOrganosPageObject;