class FormPageObject  {
    constructor(wrapper) {
        this.wrapper = wrapper;
    }

    closeForm(){
        this.wrapper.get(".x-tool-close:visible").click();
    }
}
export default FormPageObject;