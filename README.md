# Gestión de órganos colegiados

Gestión de órganos colegiados (GOC) es una aplicación pensada para convocar y gestionar las reuniones de los órganos colegiados de las 
universidades. Algunos ejemplos de usuarios de esta aplicación son el Claustro, Consejo de Gobierno, Consejo de Dirección, Consejo Social, 
Consejo de Estudiantes, Juntas de Centro, entre otras. Esta herramienta cubre todo el proceso de organización de una reunión, desde su
convocatoria hasta el cierre del acta. De esta forma, la aplicación facilita los trámites y posibilita, desde una misma plataforma, 
convocar una reunión, añadir puntos del orden del día, adjuntar documentación y enviar la convocatoria a los miembros de un órgano
colegiado para que reciban la información de la reunión y confirmen su asistencia. Adicionalmente, una vez realizada la reunión, permite
incluir el resultado de los acuerdos y deliberaciones y, seguidamente, cerrar el acta, validarla, firmarla digitalmente y archivarla para
que quede guardada en un histórico. 

Para más detalles, puedes consultar el manual de uso [en esta dirección](https://universitatjaumei.atlassian.net/l/c/gev6LCf0).

 
# Arquitectura y tecnologías utilizadas

Como resume el documento correspondiente al Marco tecnológico para el desarrollo de aplicaciones en la Universitat Jaume I, la 
aplicación del GOC se ha desarrollado en Java 8 siguiendo el siguiente esquema de separación de responsabilidades:

- **Interfaz de usuario**. Desarrollada en ExtJS 6, un framework rico de desarrollo de aplicaciones en JavaScript.
- **Aplicación web**. Conjunto de servicios REST que intercambian datos en formato JSON con la aplicación cliente. Desarrollada con Jersey, Spring y JPA, permite implementar la lógica de negocio correspondiente a los distintos procesos analizados en el documento funcional del proyecto.
- **Backend de datos**. Aunque se ha utilizado Oracle para la persistencia de datos, gracias a JPA sería factible utilizar cualquier sistema de gestión de base de datos relacional.

## Requisitos

Para compilar y ejecutar el proyecto necesitaremos:

- **Cliente de Git**. En Linux/Mac hay habitualmente paquetes de git para instalar, pero si usamos Windows, una buena opción puede ser TortoiseGit.
- **Java JDK 8**. Ojo!! Necesitamos JDK para compilar, no JRE. Disponible aquí.
- **Apache Maven**. Necesario para la compilación y empaquetado del código. Disponible aquí.
- **Apache Tomcat**. Únicamente para el despliegue de pre o de producción. Para desarrollo usaremos jetty desde Maven, por lo que no hace falta nada más.

# Descarga y compilación del proyecto

GOC es una aplicación web Java, por lo que para generar un empaquetado de tipo WAR que podamos desplegar en un servidor de aplicaciones deberemos realizar los pasos descritos a continuación.

Actualmente, se utiliza un esquema de *feature branching* directamente sobre la rama *master*.

Para construir el proyecto, nos bajamos el código y nos posicionamos en el directorio raíz del proyecto y ejecutamos Maven:

```bash
cd goc
export MAVEN_OPTS="-Dgoc.home=<dir donde está el app.properties>"
mvn clean package
```

El resultado de la compilación, en este caso `goc.war`, podrá encontrarse en el directorio `goc-base/target` de la ruta actual.

Para poder ejecutar y hacer pruebas en desarrollo, podemos arrancarlo mediante el plugin de Jetty que el proyecto tiene incorporado:

```bash
cd goc-base
export MAVEN_OPTS="-Dgoc.home=<dir donde está el app.properties>"
mvn -U clean jetty:run
```

# Documentación de los servicios de integración

En la propia distribución de GOC se incluye una implementación básica de los servicios consumidos 
por la misma, con el objetivo de poder ser probada sin la integración de los servicios reales.

El proyecto está configurado con `swagger`, ofreciendo la documentación de los servicios en la siguiente 
dirección:

```
https://ujiapps.uji.es/goc/rest/swagger.json
```

# Estructura de base de datos

GOC utiliza `flyway` como mecanismo de gestión de las migraciones de BD. Para descargar e instalar `flyway` desde aquí:

```
https://flywaydb.org/download/community
```

Una vez instalado `flyway`, es necesario ejecutar el siguiente comando para inicializar el esquema de BD:

```bash
$ flyway -driver=oracle.jdbc.OracleDriver -url=jdbc:oracle:thin:@<host>:<port>:<sid> \
         -user=<username> -password=<password> \
         -locations=filesystem:<path to goc>/goc/goc-base/src/main/resources/database/migrations migrate
```

# Descripción del fichero principal de configuración

```
# Base de datos

uji.db.driverClass=oracle.jdbc.OracleDriver
uji.db.jdbcUrl=jdbc:oracle:thin:@XXXX:1521:XXXX
uji.db.username=XXXX
uji.db.password=XXXX
uji.db.databaseId=ORACLE
uji.db.dialect=org.hibernate.dialect.Oracle10gDialect
uji.db.preferredTestQuery=select 1 from dual

# Autenticación: SAML

goc.saml.keystore.path=/etc/uji/goc/samlKeystore.jks
goc.saml.keystore.keyName=apollo
goc.saml.keystore.keyPassword=XXXX
goc.saml.idp.path=/etc/uji/goc/idp.xml
goc.saml.metadata.username=uid
goc.saml.host=ujiapps.uji.es
goc.saml.port=443
goc.saml.protocol=https
goc.saml.app=goc
goc.saml.defaultUserId=9792
goc.saml.defaultUserName=borillo
goc.saml.includeServerPortInRequestURL=false

# Endpoints externos

goc.external.authToken=XXXX
goc.external.cuentasEndpoint=http://localhost:8143/goc-services/rest/external/cuentas
goc.external.organosEndpoint=http://localhost:8143/goc-services/rest/external/organos
goc.external.miembrosEndpoint=http://localhost:8143/goc-services/rest/external/organos/{organoId}/miembros
goc.external.notificacionesEndpoint=http://localhost:8143/goc-services/rest/external/notificaciones
goc.external.personasEndpoint=http://localhost:8143/goc-services/rest/external/personas
goc.external.firmasEndpoint=http://localhost:8143/goc-services/rest/external/firmas
goc.external.config.menus=http://localhost:8143/goc-services/rest/external/config/menus
goc.external.publicarAcuerdosEndpoint=http://localhost:8143/goc-services/rest/external/reuniones/{reunionId}/publicaracuerdos

# Envío de correo

uji.smtp.host=email-smtp.eu-west-1.amazonaws.com
uji.smtp.starttls.enable=true
uji.smtp.auth=true
uji.smtp.port=465
uji.smtp.socketFactory.class=javax.net.ssl.SSLSocketFactory
uji.smtp.username=XXXX
uji.smtp.password=XXXX
uji.smtp.defaultSender=e-ujier@uji.es

# Personalización

goc.logo=http://static.uji.es/img/commons/m-uji.png
goc.logoPublic=http://static.uji.es/templates/uji2016_plantillas/static/images/logo.png
goc.logoDocumentos=http://static.uji.es/img/commons/uji.jpg
goc.customCSS=
goc.publicUrl=http://ujiapps.uji.es

goc.nombreInstitucion=Universitat Jaume I
goc.ciudadInstitucion=Castell\u00f3 de la Plana

goc.log.path=/var/log/uji
goc.charset=utf-8

goc.mainLanguage=ca
goc.mainLanguageDescription=Valenci\u00e0
goc.alternativeLanguage=
goc.alternativeLanguageDescription=

# Oficios

goc.menu.oficios=false

# Publicación parcial de acuerdos

goc.reunion.publicarAcuerdos=false

# Manual

goc.enlaceManual=https://jira.uji.es/confluence/pages/viewpage.action?pageId=43680320

# goc-services

uji.deploy.defaultUserName=borillo
uji.deploy.defaultUserId=9792
uji.deploy.returnScheme=https
uji.deploy.returnHost=ujiapps.uji.es
uji.deploy.returnPort=443

goc.reunion.basePathDocumentacion=/tmp

goc.votaciones.enabled=false
```

# Integración con el SSO SAML

GOC permite conectar con sistemas de SSO basados en SAML gracias al uso de `Spring Security`. Para ello se debe de contar con los siguientes elementos:

- El fichero `idp.xml` que nos proporcionará nuestro SSO.
- Los metadatos generados por nuestra aplicación. Estos los genera el propio GOC a partir de la configuración establecida. Podemos acceder a ellos ejecutando el siguiente comando:

```bash
curl https://<host>/goc/saml/metadata
```

# Despliegue de producción en Tomcat

Ejemplo de parámetros de configuración de tomcat que podemos definir en el fichero `$TOMCAT_HOME/bin/setenv.sh`:
```
export LC_ALL="es_ES.UTF-8"
export LANG="es_ES.UTF-8"
export JAVA_OPTS="-Dgoc.home=<ruta al dir del app.properties> -Dgoc.logs=<dir de logs> -Dserver.use-forward-headers=true -Dserver.tomcat.protocol-header=x-forwarded-proto -Dserver.tomcat.remote-ip-header=x-forwarded-for -Xmx512M -Xms64M -Dfile.encoding=UTF-8 -XX:+CMSClassUnloadingEnabled -XX:CompressedClassSpaceSize=100M "
export CATALINA_PID=$CATALINA_BASE/tomcat.pid
export CATALINA_OPTS="-Djava.awt.headless=true -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9641 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"
```

# Autenticación de los servicios externos integrados en GOC

GOC accederá a cada uno de los servicios configurados en el `app.properties` enviando la cabecera HTTP `X-UJI-AuthToken` con el valor que 
establezcamos en la propiedad `goc.external.authToken`.

# Gestión de contribuciones externas

## Envío de propuestas de nueva funcionalidad

Aunque la decisión final de aceptar o evolucionar nuevas funcionalidades propuestas será tomada siempre por las universidades que comparten la gobernanza de este proyecto, cualquier entidad externa o usuario de GOC podrá realizar propuestas de nueva funcionalidad siempre que estén convenientemente descritas y argumentadas.

El procedimiento para remitir las propuesta se gestionará a través de la página pública del proyecto en Bitbucket. Accediendo al apartado issues, se insertará una nueva incidencia marcada con la etiqueta “proposal” y con todos los detalles necesarios documentados en el campo descripción.

Una vez recibida la propuesta, será discutida en Bitbucket con la participación de los representantes de las universidades miembro antes de que su alcance e implicaciones queden totalmente definidas. Finalmente, esta propuesta se aceptará o denegará.

## Implementación de nuevas funcionalidades

Si se acepta, esta nueva funcionalidad deberá analizarse y ser dividida en el conjunto total de historias de usuario necesarias para su implementación. 

La creación, mantenimiento y evolución de estas historias de usuario se realizará en el backlog que las universidades miembro comparten en JIRA.

Cada historia de usuario deberá pasar, igual que todas las actuales, por una fase de testeo funcional antes de ser aprobada. Esto quiere decir que el código necesario para que esta historia de usuario se pueda implementar deberá ser proporcionado por la persona que la ha propuesto o, en su defecto, por la empresa contratada para su implementación. 

La implementación de la nueva funcionalidad deberá realizarse por la persona o empresa que contribuye y el código deberá subirse al repositorio para ser validado.

El mecanismo de entrega de las modificaciones de código asociadas a cada historia de usuario será el de pull request, siendo este el mecanismo habitual en los proyectos abiertos. El pull request se enviará a una rama nueva que tendrá el ID de la issue interna en JIRA si se conoce o una breve descripción de la funcionalidad si es algo no recogido en la gestión actual del proyecto. El nombre de la rama deberá seguir la estructura "feature/<id de jira>" o "feature/<resumen funcionalidad>".


