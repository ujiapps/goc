package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class MiembroNoPuedeSerSuplenteException extends CoreDataBaseException {
    public MiembroNoPuedeSerSuplenteException() {
        super("Un miembro no puede actuar como suplente");
    }

    public MiembroNoPuedeSerSuplenteException(String message) {
        super(message);
    }
}
