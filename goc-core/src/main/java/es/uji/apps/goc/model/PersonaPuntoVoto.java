package es.uji.apps.goc.model;


import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.AsistenciaNoConfirmadaException;
import es.uji.apps.goc.exceptions.VotoDelegadoException;
import es.uji.apps.goc.exceptions.VotoException;
import es.uji.apps.goc.model.punto.PuntoVotable;
import es.uji.apps.goc.model.punto.PuntoVotableActualizar;
import org.apache.commons.lang3.Validate;

import java.util.UUID;

public class PersonaPuntoVoto extends Votante {
    private UUID id;
    private boolean votoDoble;

    public PersonaPuntoVoto(Persona persona, PuntoVotable punto, OrganoReunionMiembro organoReunionMiembro, boolean votoDoble)
            throws VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        super(persona, punto, organoReunionMiembro);
        this.votoDoble = votoDoble;
    }

    public PersonaPuntoVoto(UUID id, Persona persona, PuntoVotableActualizar puntoAActualizar, OrganoReunionMiembro organoReunionMiembro, boolean votoDoble)
            throws VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        this(persona, puntoAActualizar, organoReunionMiembro, votoDoble);
        Validate.notNull(id);
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getVoto() {
        return this.getPunto().getVoto();
    }

    public void setVoto(TipoVotoEnum voto) {
        this.getPunto().setVoto(voto);
    }

    public boolean isVotoDoble() {
        return votoDoble;
    }

    public void setVotoDoble(boolean votoDoble) {
        this.votoDoble = votoDoble;
    }
}
