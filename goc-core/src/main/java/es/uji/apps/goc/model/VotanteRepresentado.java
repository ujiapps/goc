package es.uji.apps.goc.model;

import org.apache.commons.lang3.Validate;

public class VotanteRepresentado {
    private Long miembroId;
    private String nombre;

    public VotanteRepresentado(Long miembroId, String nombre) {
        validateParams(miembroId, nombre);
        this.miembroId = miembroId;
        this.nombre = nombre;
    }

    public Long getMiembroId() {
        return miembroId;
    }

    public String getNombre() {
        return nombre;
    }

    private void validateParams(Long miembroId, String nombre) {
        Validate.notNull(miembroId);
        Validate.notNull(nombre);
    }
}
