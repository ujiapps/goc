package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import es.uji.apps.goc.dto.PuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.dto.QPuntoOrdenDiaAcuerdo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class PuntoOrdenDiaAcuerdoDAO extends BaseDAODatabaseImpl {
    private QPuntoOrdenDiaAcuerdo qPuntoOrdenDiaAcuerdo = QPuntoOrdenDiaAcuerdo.puntoOrdenDiaAcuerdo;

    public Long getNumeroAcuerdosPorPuntoOrdenDia(Long puntoOrdenDiaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPuntoOrdenDiaAcuerdo).where(qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(puntoOrdenDiaId).and(qPuntoOrdenDiaAcuerdo.activo.isTrue())).count();
    }

    public Long getUltimoAcuerdo(Long puntoOrdenDiaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPuntoOrdenDiaAcuerdo)
                .where(qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(puntoOrdenDiaId))
                .orderBy(qPuntoOrdenDiaAcuerdo.orden.asc()).
                singleResult(qPuntoOrdenDiaAcuerdo.orden);
    }

    public PuntoOrdenDiaAcuerdo getAcuerdoAnterior(Long puntoOrdenDiaId, Long orden) {
        BooleanExpression be = qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(puntoOrdenDiaId).and(qPuntoOrdenDiaAcuerdo.orden.lt(orden));
        JPAQuery query = new JPAQuery(entityManager);

        Tuple tuple = query.from(qPuntoOrdenDiaAcuerdo).
                where(be).
                orderBy(qPuntoOrdenDiaAcuerdo.orden.desc()).
                singleResult(qPuntoOrdenDiaAcuerdo.id, qPuntoOrdenDiaAcuerdo.orden);

        if (tuple == null) {
            return null;
        }

        PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo = new PuntoOrdenDiaAcuerdo();
        puntoOrdenDiaAcuerdo.setId(tuple.get(qPuntoOrdenDiaAcuerdo.id));
        puntoOrdenDiaAcuerdo.setId(tuple.get(qPuntoOrdenDiaAcuerdo.orden));

        return puntoOrdenDiaAcuerdo;
    }

    public PuntoOrdenDiaAcuerdo getAcuerdoPosterior(Long puntoOrdenDiaId, Long orden) {
        BooleanExpression be = qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(puntoOrdenDiaId).and(qPuntoOrdenDiaAcuerdo.orden.gt(orden));
        JPAQuery query = new JPAQuery(entityManager);

        Tuple tuple = query.from(qPuntoOrdenDiaAcuerdo).
                where(be).
                orderBy(qPuntoOrdenDiaAcuerdo.orden.asc()).
                singleResult(qPuntoOrdenDiaAcuerdo.id, qPuntoOrdenDiaAcuerdo.orden);

        if (tuple == null) {
            return null;
        }

        PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo = new PuntoOrdenDiaAcuerdo();
        puntoOrdenDiaAcuerdo.setId(tuple.get(qPuntoOrdenDiaAcuerdo.id));
        puntoOrdenDiaAcuerdo.setId(tuple.get(qPuntoOrdenDiaAcuerdo.orden));

        return puntoOrdenDiaAcuerdo;
    }

    public void cambiarOrdenAcuerdo(Long documentoId, Long orden) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDiaAcuerdo);
        update.set(qPuntoOrdenDiaAcuerdo.orden, orden).where(qPuntoOrdenDiaAcuerdo.id.eq(documentoId));
        update.execute();
    }

    @Transactional
    public void updateMotivoEdicion(Long documentoId, String motivoEdicion, Long connectedUserId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDiaAcuerdo);
        update.set(qPuntoOrdenDiaAcuerdo.motivoEdicion, motivoEdicion).
                set(qPuntoOrdenDiaAcuerdo.fechaEdicion, new Date()).
                set(qPuntoOrdenDiaAcuerdo.editorId, connectedUserId).
                where(qPuntoOrdenDiaAcuerdo.id.eq(documentoId));
        update.execute();
    }

    @Transactional
    public void disable(Long documentoId, Long connectedUserId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDiaAcuerdo);
        update.set(qPuntoOrdenDiaAcuerdo.activo, false).
                set(qPuntoOrdenDiaAcuerdo.fechaEdicion, new Date()).
                set(qPuntoOrdenDiaAcuerdo.editorId, connectedUserId).
                where(qPuntoOrdenDiaAcuerdo.id.eq(documentoId));
        update.execute();
    }

    public PuntoOrdenDiaAcuerdo getAcuerdoById(Long acuerdoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDiaAcuerdo).where(qPuntoOrdenDiaAcuerdo.id.eq(acuerdoId));

        List<PuntoOrdenDiaAcuerdo> resultado = query.list(qPuntoOrdenDiaAcuerdo);

        if (resultado.size() != 1) {
            return null;
        }

        return resultado.get(0);
    }

    public PuntoOrdenDiaAcuerdo getDatosAcuerdoById(Long documentoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDiaAcuerdo).where(qPuntoOrdenDiaAcuerdo.id.eq(documentoId));

        Tuple tupla = query.singleResult(qPuntoOrdenDiaAcuerdo.id, qPuntoOrdenDiaAcuerdo.orden, qPuntoOrdenDiaAcuerdo.creadorId);

        PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo = new PuntoOrdenDiaAcuerdo();
        puntoOrdenDiaAcuerdo.setId(tupla.get(qPuntoOrdenDiaAcuerdo.id));
        puntoOrdenDiaAcuerdo.setOrden(tupla.get(qPuntoOrdenDiaAcuerdo.orden));
        puntoOrdenDiaAcuerdo.setCreadorId(tupla.get(qPuntoOrdenDiaAcuerdo.creadorId));

        return puntoOrdenDiaAcuerdo;
    }

    public List<PuntoOrdenDiaAcuerdo> getAcuerdosActivosByPuntoOrdenDiaId(Long puntoOrdenDiaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDiaAcuerdo)
                .where(qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(puntoOrdenDiaId)
                        .and(qPuntoOrdenDiaAcuerdo.activo.isTrue()))
                .orderBy(qPuntoOrdenDiaAcuerdo.orden.asc());

        return query.list(qPuntoOrdenDiaAcuerdo);
    }

    public List<PuntoOrdenDiaAcuerdo> getDatosAcuerdosByPuntoOrdenDiaId(Long puntoOrdenDiaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDiaAcuerdo)
                .where(qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(puntoOrdenDiaId)
                        .and(qPuntoOrdenDiaAcuerdo.activo.isTrue()))
                .orderBy(qPuntoOrdenDiaAcuerdo.orden.asc());

        List<Tuple> tuplas = query.list(qPuntoOrdenDiaAcuerdo.creadorId, qPuntoOrdenDiaAcuerdo.descripcion,
                qPuntoOrdenDiaAcuerdo.descripcionAlternativa, qPuntoOrdenDiaAcuerdo.fechaAdicion, qPuntoOrdenDiaAcuerdo.id,
                qPuntoOrdenDiaAcuerdo.mimeType, qPuntoOrdenDiaAcuerdo.nombreFichero, qPuntoOrdenDiaAcuerdo.publico,
                qPuntoOrdenDiaAcuerdo.orden, qPuntoOrdenDiaAcuerdo.hash, qPuntoOrdenDiaAcuerdo.editorId, qPuntoOrdenDiaAcuerdo.fechaEdicion,
                qPuntoOrdenDiaAcuerdo.motivoEdicion, qPuntoOrdenDiaAcuerdo.activo);

        List<PuntoOrdenDiaAcuerdo> documentos = new ArrayList<>();

        for (Tuple tupla : tuplas) {
            PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo = new PuntoOrdenDiaAcuerdo();

            puntoOrdenDiaAcuerdo.setId(tupla.get(qPuntoOrdenDiaAcuerdo.id));
            puntoOrdenDiaAcuerdo.setCreadorId(tupla.get(qPuntoOrdenDiaAcuerdo.creadorId));
            puntoOrdenDiaAcuerdo.setDescripcion(tupla.get(qPuntoOrdenDiaAcuerdo.descripcion));
            puntoOrdenDiaAcuerdo.setDescripcionAlternativa(tupla.get(qPuntoOrdenDiaAcuerdo.descripcionAlternativa));
            puntoOrdenDiaAcuerdo.setFechaAdicion(tupla.get(qPuntoOrdenDiaAcuerdo.fechaAdicion));
            puntoOrdenDiaAcuerdo.setMimeType(tupla.get(qPuntoOrdenDiaAcuerdo.mimeType));
            puntoOrdenDiaAcuerdo.setNombreFichero(tupla.get(qPuntoOrdenDiaAcuerdo.nombreFichero));
            puntoOrdenDiaAcuerdo.setPublico(tupla.get(qPuntoOrdenDiaAcuerdo.publico));
            puntoOrdenDiaAcuerdo.setOrden(tupla.get(qPuntoOrdenDiaAcuerdo.orden));
            puntoOrdenDiaAcuerdo.setHash(tupla.get(qPuntoOrdenDiaAcuerdo.hash));
            puntoOrdenDiaAcuerdo.setEditorId(tupla.get(qPuntoOrdenDiaAcuerdo.editorId));
            puntoOrdenDiaAcuerdo.setFechaAdicion(tupla.get(qPuntoOrdenDiaAcuerdo.fechaEdicion));
            puntoOrdenDiaAcuerdo.setMotivoEdicion(tupla.get(qPuntoOrdenDiaAcuerdo.motivoEdicion));
            puntoOrdenDiaAcuerdo.setActivo(tupla.get(qPuntoOrdenDiaAcuerdo.activo));

            documentos.add(puntoOrdenDiaAcuerdo);
        }

        return documentos;
    }

    @Transactional
    public void updatePrivacidadAcuerdosPunto(Long puntoOrdenDiaId, Boolean publico) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDiaAcuerdo);

        update.set(qPuntoOrdenDiaAcuerdo.publico, publico).where(qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(puntoOrdenDiaId)).execute();
    }
}