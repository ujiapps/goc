package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoPuedeEliminarSuplenteSiYaSeHaVotadoException extends CoreBaseException
{
    public NoPuedeEliminarSuplenteSiYaSeHaVotadoException() {
        super("No puede eliminar suplente si el suplente ya ha votado");
    }
}
