package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ReunionNoAdmiteComentariosException extends CoreBaseException
{
    public ReunionNoAdmiteComentariosException(String applang)
    {
        super(applang);
    }

    public ReunionNoAdmiteComentariosException() {
        super("La reunión no admite comentarios");
    }
}
