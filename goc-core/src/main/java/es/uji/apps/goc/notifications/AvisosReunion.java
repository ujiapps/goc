package es.uji.apps.goc.notifications;

import es.uji.apps.goc.DateUtils;
import es.uji.apps.goc.charset.ResourceBundleUTF8;
import es.uji.apps.goc.dao.*;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.exceptions.NotificacionesException;
import es.uji.apps.goc.exceptions.ReunionNoDisponibleException;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.templates.HTMLTemplate;
import es.uji.apps.goc.templates.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component
public class AvisosReunion {
    @Value("${goc.mainLanguage}")
    public String mainLanguage;
    @Value("${goc.alternativeLanguage}")
    public String alternativeLanguage;
    @Value("${goc.templates.path:classpath:templates/}")
    public String templatesPath;
    private OrganoReunionMiembroDAO organoReunionMiembroDAO;
    private NotificacionesDAO notificacionesDAO;
    private ReunionDAO reunionDAO;
    private OrganoAutorizadoDAO organoAutorizadoDAO;
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;
    private OrganoDAO organoDAO;
    @Value("${goc.publicUrl}")
    private String publicUrl;
    @Value("${uji.smtp.defaultSender}")
    private String defaultSender;
    @Value("${goc.charset}")
    private String charset;
    @Value("${goc.email.templates:false}")
    private Boolean emailTemplates;

    @Value("${goc.email.correosBilingues:false}")
    private Boolean correosBilingue;


    @Autowired
    public AvisosReunion(ReunionDAO reunionDAO, OrganoReunionMiembroDAO organoReunionMiembroDAO,
                         NotificacionesDAO notificacionesDAO, OrganoAutorizadoDAO organoAutorizadoDAO, PuntoOrdenDiaDAO puntoOrdenDiaDAO,
                         OrganoDAO organoDAO) {
        this.reunionDAO = reunionDAO;
        this.organoReunionMiembroDAO = organoReunionMiembroDAO;
        this.notificacionesDAO = notificacionesDAO;
        this.organoAutorizadoDAO = organoAutorizadoDAO;
        this.puntoOrdenDiaDAO = puntoOrdenDiaDAO;
        this.organoDAO = organoDAO;
    }

    private static String obtenerMailAsistente(OrganoReunionMiembro asistente) {
        return (asistente.getSuplenteEmail() != null) ? asistente.getSuplenteEmail() : asistente.getEmail();
    }

    @Transactional
    public void enviaAvisoAltaSuplente(Long reunionId, String emailSuplente, String miembroNombre, String miembroCargo)
            throws Exception {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        Date fechaActual = new Date();
        if (reunion.getFecha().compareTo(fechaActual) > 0) {
            if (emailSuplente == null || emailSuplente.isEmpty()) return;

            String textoAux = getTextoAuxiliar(miembroNombre, miembroCargo, "mail.reunion.suplenteDesignado", resourceBundle);

            String asunto = "[GOC]";

            asunto += " " + getTextoAsuntoReunion(resourceBundle, "mail.reunion.suplenteEnReunion", reunion);

            if (reunion.getNumeroSesion() != null) {
                asunto += " n. " + reunion.getNumeroSesion();
            }

            SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

            buildAndSendMessageWithExtraText(reunion, emailSuplente, asunto, textoAux);
        }
    }

    public String getTextoAsuntoReunion(ResourceBundleUTF8 resourceBundle, String key, Reunion reunion) {
        return resourceBundle.getString(key) + " " + getNombreOrganos(reunion);
    }

    public String getTextoAuxiliar(String miembroNombre, String miembroCargo, String key, ResourceBundleUTF8 resourceBundle) {
        return resourceBundle.getString(key) + " " + miembroNombre + " (" + miembroCargo + ")";
    }

    @Transactional
    public void enviaAvisoBajaSuplente(Long reunionId, String emailSuplente, String miembroNombre, String miembroCargo)
            throws Exception {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        Date fechaActual = new Date();
        if (reunion.getFecha().compareTo(fechaActual) > 0) {
            if (emailSuplente == null || emailSuplente.isEmpty()) return;

            String textoAux = getTextoAuxiliar(miembroNombre, miembroCargo, "mail.reunion.suplenteBaja", resourceBundle);

            String asunto = "[GOC]";

            asunto += " " + getTextoAsuntoReunion(resourceBundle, "mail.reunion.suplenteBajaEnReunion", reunion);

            if (reunion.getNumeroSesion() != null) {
                asunto += " n. " + reunion.getNumeroSesion();
            }

            SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

            buildAndSendMessageWithExtraText(reunion, emailSuplente, asunto, textoAux);
        }
    }

    @Transactional
    public void enviaAvisoDelegacionVoto(Long reunionId, String emailDelegadoVoto, String miembroNombre,
                                         String miembroCargo, boolean isAvisoDelegacionAlta) throws Exception {
        String keyTextoAux;
        String keyAsuntoReunion;

        if (isAvisoDelegacionAlta) {
            keyTextoAux = "mail.reunion.delegacionVoto";
            keyAsuntoReunion = "mail.reunion.delegacionVotoEnReunion";
        } else {
            keyTextoAux = "mail.reunion.delegacionVotoBaja";
            keyAsuntoReunion = "mail.reunion.delegacionVotoBajaEnReunion";
        }

        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (emailDelegadoVoto == null || emailDelegadoVoto.isEmpty()) return;

        String textoAux = getTextoAuxiliar(miembroNombre, miembroCargo, keyTextoAux, resourceBundle);

        String asunto = "[GOC]";

        asunto += " " + getTextoAsuntoReunion(resourceBundle, keyAsuntoReunion, reunion);

        if (reunion.getNumeroSesion() != null) {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

        buildAndSendMessageWithExtraText(reunion, emailDelegadoVoto, asunto, textoAux);
    }

    @Transactional
    public void enviaAvisonuevaReunionNuevosInvitados(Reunion reunion, List<String> reunionInvitadosAñadidosPosteriormente) throws Exception {
        Boolean avisoPrimeraReunion = reunion.getAvisoPrimeraReunion();
        reunion.setAvisoPrimeraReunion(false);

        crearTextoYEnviarConvocatoria(reunion, "", "", reunionInvitadosAñadidosPosteriormente, Collections.emptyList(), false, "", "");

        reunion.setAvisoPrimeraReunion(avisoPrimeraReunion);
    }

    @Transactional
    public void enviaAvisoNuevaReunion(Reunion reunion, String textoConvocatoria, String textoConvocatoriaAlternativo, String textoInformar, String textoInformarAlternativo) throws Exception {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        List<String> miembros = getMiembros(reunion, false);
        List<String> autorizados = getAutorizados(reunion);
        List<OrganoReunion> organosReunionByReunionId = reunionDAO.getOrganosReunionByReunionId(reunion.getId());
        Set<OrganoReunion> organoReunionSet = organosReunionByReunionId.stream().collect(Collectors.toSet());
        reunion.setReunionOrganos(organoReunionSet);


        crearTextoYEnviarConvocatoria(reunion, textoConvocatoria, textoConvocatoriaAlternativo, miembros, autorizados, false, textoInformar, textoInformarAlternativo);

    }

    @Transactional
    public void enviaBorrador(Reunion reunion, List<String> emails) throws Exception {
        crearTextoYEnviarConvocatoria(reunion, "", "", emails, null, true, "", "");
    }

    public void crearTextoYEnviarConvocatoria(Reunion reunion, String textoConvocatoria, String textoConvocatoriaAlternativo, List<String> miembros, List<String> autorizados, Boolean isBorrador, String textoInformar, String textoInformarAlternativo) throws Exception {

        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        String asunto = "[GOC]";

        if (isBorrador) {
            asunto = getAsuntoReunionBorrador(reunion, resourceBundle, asunto);
        } else {
            asunto = getAsuntoReunionNuevaORectificada(reunion, resourceBundle, asunto);
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());


        buildAndSendMessage(reunion, null, miembros, autorizados, asunto, textoConvocatoria, textoConvocatoriaAlternativo, true, true, isBorrador, textoInformar, textoInformarAlternativo);

    }

    public String getAsuntoReunionNuevaORectificada(Reunion reunion, ResourceBundleUTF8 resourceBundle, String asunto) {
        if (reunion.getAvisoPrimeraReunion()) {
            asunto += " [" + resourceBundle.getString("mail.reunion.rectificada") + "]";
        }

        asunto += " " + resourceBundle.getString("mail.reunion.convocatoria") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null) {
            asunto += " n. " + reunion.getNumeroSesion();
        }
        return asunto;
    }

    public String getAsuntoReunionBorrador(Reunion reunion, ResourceBundleUTF8 resourceBundle, String asunto) {

        asunto += " [" + resourceBundle.getString("mail.reunion.asuntoBorrador") + "]";
        asunto += " " + resourceBundle.getString("mail.reunion.convocatoria") + " " + getNombreOrganos(reunion);

        return asunto;
    }

    private String getNombreOrganos(Reunion reunion) {
        List<String> nombreOrganos = new ArrayList<>();

        for (OrganoReunion organo : reunion.getReunionOrganos()) {
            nombreOrganos.add(organo.getOrganoNombre());
        }

        return StringUtils.join(nombreOrganos, ", ");
    }

    private String getNombreAltOrganos(Reunion reunion) {
        List<String> nombreAltOrganos = new ArrayList<>();

        for (OrganoReunion organo : reunion.getReunionOrganos()) {
            nombreAltOrganos.add(organo.getOrganoNombreAlternativo());
        }

        return StringUtils.join(nombreAltOrganos, ", ");
    }

    public Boolean enviaAvisoReunionProxima(Reunion reunion) throws Exception {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        List<String> miembros = getMiembros(reunion, true);
        List<String> autorizados = getAutorizados(reunion);

        if (miembros == null || miembros.size() == 0) {
            return false;
        }

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        buildAndSendMessage(reunion, null, miembros, autorizados,
                "[GOC] " + getAsuntoEnviaAvisoReunionProxima(reunion, resourceBundle, df, DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage)), "", "", false, false, false, "", "");


        return true;
    }

    public String getAsuntoEnviaAvisoReunionProxima(Reunion reunion, ResourceBundleUTF8 resourceBundle, DateFormat df, String diaSemanaTexto) {
        return resourceBundle.getString("mail.reunion.recordatorio") + ": " + reunion.getAsunto() + " " + diaSemanaTexto + " (" + df.format(reunion.getFecha()) + ")";
    }

    public void enviaAvisoNuevoComentarioAMiembrosInvitadosAutorizadosOrganosReunion(Reunion reunion, ReunionComentario reunionComentario)
            throws Exception {
        if (reunion.getEmailsEnNuevosComentarios()) {
            ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
            ResourceBundleUTF8 resourceBundleEs = new ResourceBundleUTF8("i18nEmails", alternativeLanguage);
            Mensaje mensaje = new Mensaje();
            mensaje.setReunionId(reunion.getId());
            mensaje.setAsunto("[GOC] " + getTexto(resourceBundle, "mail.reunion.nuevoComentario") + " [" + reunion.getAsunto() + "]");

            StringBuilder cuerpo = new StringBuilder();

            if (emailTemplates) {
                Template template = new HTMLTemplate("email-nuevo-comentario-" + mainLanguage, templatesPath);

                if (correosBilingue) {
                    if (alternativeLanguage != null && !alternativeLanguage.isEmpty()) {
                        template = new HTMLTemplate("email-nuevo-comentario", templatesPath);
                    }
                }
                template.put("reunion", reunion);
                template.put("creadorNombre", reunionComentario.getCreadorNombre());
                template.put("comentario", reunionComentario.getComentario());
                template.put("publicUrl", publicUrl);
                template.put("mainLanguage", mainLanguage);

                mensaje.setCuerpo(new String(template.process()));
            } else {

                cuerpo.append("<h2>" + reunion.getAsunto() + "</h2>");
                cuerpo.append("<div><strong>" + reunionComentario.getCreadorNombre() + "</strong>" + " " + getTexto(resourceBundle, "mail.reunion.anadioNuevoComentario") + "</a></div>");
                cuerpo.append("<br/><br/>");

                cuerpo.append(reunionComentario.getComentario());

                cuerpo.append("<div>" + getTexto(resourceBundle, "mail.reunion.masInfo") + "<a href=\"" + publicUrl
                        + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "\">" + publicUrl + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "</a></div>");

                if (correosBilingue) {
                    if (alternativeLanguage != null && !alternativeLanguage.isEmpty()) {
                        cuerpo.append("<hr/>");
                        cuerpo.append("<h2>" + reunion.getAsuntoAlternativo() + "</h2>");
                        cuerpo.append("<div><strong>" + reunionComentario.getCreadorNombre() + "</strong>" + " " + getTexto(resourceBundleEs, "mail.reunion.anadioNuevoComentario") + "</div>");
                        cuerpo.append("<br/><br/>");

                        cuerpo.append(reunionComentario.getComentario());

                        cuerpo.append("<div>" + getTexto(resourceBundleEs, "mail.reunion.masInfo") + "<a href=\"" + publicUrl
                                + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "?lang=" + alternativeLanguage + "\">" + publicUrl + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "</div>");
                    }
                }

                mensaje.setCuerpo(cuerpo.toString());
            }

            mensaje.setDestinos(getMiembros(reunion, false));
            mensaje.setAutorizados(getAutorizados(reunion));
            mensaje.setReplyTo(defaultSender);
            mensaje.setFrom(defaultSender);

            notificacionesDAO.enviaNotificacion(mensaje);
        }
    }

    public void enviarAvisoCambioCondicionesDeVotoEnPOD(Reunion reunion, PuntoOrdenDia puntoOrdenDia, boolean procedimientoVotoHaCambiado, boolean tipoVotoHaCambiado, boolean votableEnContraHaCambiado, boolean tipoRecuentoVotosHaCambiado, boolean tipoVotacionHaCambiado)
            throws NotificacionesException, MiembrosExternosException, ReunionNoDisponibleException {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);

        Mensaje mensaje = new Mensaje();
        mensaje.setReunionId(reunion.getId());
        mensaje.setAsunto("[GOC] " + getTexto(resourceBundle, "mail.reunion.cambioParametrosVoto") + " [" + reunion.getAsunto() + "]");

        StringBuilder cuerpo = new StringBuilder();

        cuerpo.append("<h2>" + reunion.getAsunto() + "</h2>");
        cuerpo.append("<div>" + getTexto(resourceBundle, "mail.reunion.punto") + ": " + "<strong>" + puntoOrdenDia.getTitulo() + "</strong></div>");
        cuerpo.append("<br/>");

        cuerpo.append(getTexto(resourceBundle, "mail.reunion.cambiosEn") + ": <ul>");
        if (procedimientoVotoHaCambiado)
            cuerpo.append("<li>" + getTexto(resourceBundle, "mail.reunion.procedimientoVotoCambiado") + "</li>");
        if (tipoVotoHaCambiado)
            cuerpo.append("<li>" + getTexto(resourceBundle, "mail.reunion.tipoVotoHaCambiado") + "</li>");
        if (votableEnContraHaCambiado)
            cuerpo.append("<li>" + getTexto(resourceBundle, "mail.reunion.votableEnContraHaCambiado") + "</li>");
        if (tipoRecuentoVotosHaCambiado)
            cuerpo.append("<li>" + getTexto(resourceBundle, "mail.reunion.tipoRecuentoVotosHaCambiado") + "</li>");
        if (tipoVotacionHaCambiado)
            cuerpo.append("<li>" + getTexto(resourceBundle, "mail.reunion.tipoVotacionHaCambiado") + "</li>");
        cuerpo.append("</ul>");

        cuerpo.append("<div>" + getTexto(resourceBundle, "mail.reunion.masInfo") + "<a href=\"" + publicUrl
                + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "\">" + publicUrl + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "</div>");
        mensaje.setCuerpo(cuerpo.toString());

        mensaje.setDestinos(getMiembros(reunion, true));
        mensaje.setAutorizados(getAutorizados(reunion));
        mensaje.setReplyTo(defaultSender);
        mensaje.setFrom(defaultSender);

        notificacionesDAO.enviaNotificacion(mensaje);
    }

    public String getTexto(ResourceBundleUTF8 resourceBundle, String s) {
        return resourceBundle.getString(s);
    }

    private String getReplyTo(Reunion reunion) {
        Set<OrganoReunion> reunionOrganos = reunion.getReunionOrganos();
        if (reunionOrganos.size() <= 1) {
            String emailOrgano = getEmailOrganoConvocante(reunionOrganos);
            if (emailOrgano != null) {
                return emailOrgano;
            }
            if (defaultSender != null) {
                return defaultSender;
            }
        }
        return reunion.getCreadorEmail();
    }

    public void enviarAvisoNuevoComentarioPuntoOrdenDiaAsistentesAutorizados(Reunion reunion,
                                                                             PuntoOrdenDiaComentario puntoOrdenDiaComentario, List<OrganoReunionMiembro> asistentes) throws Exception {
        if (reunion.getEmailsEnNuevosComentarios()) {
            ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
            ResourceBundleUTF8 resourceBundleEs = new ResourceBundleUTF8("i18nEmails", alternativeLanguage);
            Mensaje mensaje = new Mensaje();
            mensaje.setReunionId(reunion.getId());

            mensaje.setAsunto("[GOC] " + getTexto(resourceBundle, "mail.reunion.nuevoComentario") + " [" + reunion.getAsunto() + "]");

            StringBuilder cuerpo = new StringBuilder();

            if (emailTemplates) {
                Template template = new HTMLTemplate("email-nuevo-comentario-punto-" + mainLanguage, templatesPath);

                if (correosBilingue) {
                    if (alternativeLanguage != null && !alternativeLanguage.isEmpty()) {
                        template = new HTMLTemplate("email-nuevo-comentario-punto", templatesPath);
                    }
                }
                template.put("reunion", reunion);
                template.put("tituloPunto", puntoOrdenDiaComentario.getPuntoOrdenDia().getTitulo());
                template.put("tituloPuntoAlternativo", puntoOrdenDiaComentario.getPuntoOrdenDia().getTituloAlternativo());
                template.put("creadorNombre", puntoOrdenDiaComentario.getCreadorNombre());
                template.put("comentario", puntoOrdenDiaComentario.getComentario());
                template.put("publicUrl", publicUrl);
                template.put("mainLanguage", mainLanguage);

                mensaje.setCuerpo(new String(template.process()));
            } else {
                cuerpo.append("<h2>" + reunion.getAsunto() + "</h2>");
                cuerpo.append("<span> " + getTexto(resourceBundle, "mail.reunion.comentarioPunto") + ": </span>");
                cuerpo.append("<h3>" + puntoOrdenDiaComentario.getPuntoOrdenDia().getTitulo() + "</h3>");
                cuerpo.append("<div><strong>" + puntoOrdenDiaComentario.getCreadorNombre() + "</strong>" + " " + getTexto(resourceBundle, "mail.reunion.comentario") + ".</div>");
                cuerpo.append("<br/><br/>");
                cuerpo.append(puntoOrdenDiaComentario.getComentario());

                cuerpo.append("<div>" + getTexto(resourceBundle, "mail.reunion.masInfo") + "<a href=\"" + publicUrl
                        + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "\">" + publicUrl + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "</a></div>");

                if (correosBilingue) {
                    if (alternativeLanguage != null && !alternativeLanguage.isEmpty()) {
                        cuerpo.append("<hr/>");
                        cuerpo.append("<h2>" + reunion.getAsuntoAlternativo() + "</h2>");
                        cuerpo.append("<span> " + getTexto(resourceBundleEs, "mail.reunion.comentarioPunto") + ": </span>");
                        cuerpo.append("<h3>" + puntoOrdenDiaComentario.getPuntoOrdenDia().getTituloAlternativo() + "</h3>");
                        cuerpo.append("<div><strong>" + puntoOrdenDiaComentario.getCreadorNombre() + "</strong>" + " " + getTexto(resourceBundleEs, "mail.reunion.comentario") + ".</div>");
                        cuerpo.append("<br/><br/>");
                        cuerpo.append(puntoOrdenDiaComentario.getComentario());
                        cuerpo.append("<div>" + getTexto(resourceBundleEs, "mail.reunion.masInfo") + "<a href=\"" + publicUrl
                                + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "?lang=" + alternativeLanguage + "\">" + publicUrl + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "</a></div>");
                    }
                }
                mensaje.setCuerpo(cuerpo.toString());
            }

            mensaje.setDestinos(getMiembros(reunion, false));
            mensaje.setAutorizados(getAutorizados(reunion));
            mensaje.setReplyTo(getReplyTo(reunion));
            mensaje.setFrom(defaultSender);

            notificacionesDAO.enviaNotificacion(mensaje);
        }
    }

    private void buildAndSendMessageWithExtraText(Reunion reunion, String miembro, String asunto, String textoAux) throws Exception {
        List<String> miembros = Collections.singletonList(miembro);
        buildAndSendMessageWithExtraText(reunion, miembros, asunto, textoAux);
    }

    private void buildAndSendMessageWithExtraText(Reunion reunion, List<String> miembros, String asunto, String textoAux)
            throws Exception {
        Mensaje mensaje = new Mensaje();
        mensaje.setReunionId(reunion.getId());
        mensaje.setAsunto(asunto);

        List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados = puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateado(reunion.getId());

        if (emailTemplates) {
            Template template = getTemplateReunionEmail(reunion, puntosOrdenDiaOrdenados);
            template.put("textoAux", textoAux);
            mensaje.setCuerpo(new String(template.process()));
        } else {
            ReunionFormatter formatter = new ReunionFormatter(reunion, puntosOrdenDiaOrdenados);

            mensaje.setCuerpo(formatter.format(publicUrl, textoAux, null, null, true, true, mainLanguage, false, null, null));

        }
        mensaje.setFrom(defaultSender);
        mensaje.setReplyTo(getReplyTo(reunion));
        mensaje.setDestinos(miembros);

        notificacionesDAO.enviaNotificacion(mensaje);
    }

    private void buildAndSendMessage(Reunion reunion, List<Organo> organos, List<String> miembros, List<String> autorizados, String asunto,
                                     String textoConvocatoria, String textoConvocatoriaAlternativo, Boolean enviarOrdenDia, Boolean enviarUrlEnlace, Boolean isBorrador, String textoInformar, String textoInformarAlternativo

    ) throws Exception {
        Mensaje mensaje = new Mensaje();
        mensaje.setReunionId(reunion.getId());

        mensaje.setAsunto(asunto);

        List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados = puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateado(reunion.getId());

        if (emailTemplates) {
            Template template = getTemplateReunionEmail(reunion, puntosOrdenDiaOrdenados);

            if (correosBilingue) {
                if (alternativeLanguage != null && !alternativeLanguage.isEmpty()) {
                    template = new HTMLTemplate("email-envio-convocatoria", templatesPath);
                    if (puntosOrdenDiaOrdenados.size() == 1) {
                        PuntoOrdenDiaMultinivel puntoUnico = puntosOrdenDiaOrdenados.get(0);
                        template.put("puntoUnico", puntoUnico);
                    }
                }
            }
            template.put("reunion", reunion);
            template.put("puntosOrdenDiaOrdenados", puntosOrdenDiaOrdenados);
            template.put("publicUrl", publicUrl);
            template.put("mainLanguage", mainLanguage);

            String diaSemanaTextoSegundaConvocatoria = null;
            String diaSemanaTextoSegundaConvocatoriaEs = null;
            if (reunion.getFechaSegundaConvocatoria() != null) {
                diaSemanaTextoSegundaConvocatoria = DateUtils.getDiaSemanaTexto(reunion.getFechaSegundaConvocatoria(), "ca");
                diaSemanaTextoSegundaConvocatoriaEs = DateUtils.getDiaSemanaTexto(reunion.getFechaSegundaConvocatoria(), "es");
            }

            template.put("diaSemanaFechaPrimeraConvocatoria", DateUtils.getDiaSemanaTexto(reunion.getFecha(), "ca"));
            template.put("diaSemanaFechaPrimeraConvocatoriaEs", DateUtils.getDiaSemanaTexto(reunion.getFecha(), "es"));
            template.put("diaSemanaFechaSegundaConvocatoria", diaSemanaTextoSegundaConvocatoria);
            template.put("diaSemanaFechaSegundaConvocatoriaEs", diaSemanaTextoSegundaConvocatoriaEs);


            //Template template = getTemplateReunionEmail(reunion, puntosOrdenDiaOrdenados);
            template.put("textoConvocatoria", textoConvocatoria);
            template.put("textoConvocatoriaAlternativo", textoConvocatoriaAlternativo);
            template.put("mostrarOrdenDia", enviarOrdenDia);
            template.put("enviarUrlEnlace", enviarUrlEnlace);
            template.put("textoInformar", textoInformar);
            template.put("textoInformarAlternativo", textoInformarAlternativo);

            template.put("isBorrador", isBorrador);

            mensaje.setCuerpo(new String(template.process()));
        } else {
            ReunionFormatter formatter = new ReunionFormatter(reunion, puntosOrdenDiaOrdenados);

            String mainBody = formatter.format(publicUrl, null, textoConvocatoria, textoConvocatoriaAlternativo, enviarOrdenDia, enviarUrlEnlace, mainLanguage, isBorrador, textoInformar, textoInformarAlternativo);

            if (correosBilingue) {
                if (alternativeLanguage != null && !alternativeLanguage.isEmpty()) {
                    String alternativeBody = formatter.formatAlternativeLanguage(publicUrl, null, textoConvocatoria, enviarOrdenDia, enviarUrlEnlace, alternativeLanguage, textoInformar, textoInformarAlternativo);
                    mainBody += alternativeBody;
                }
            }

            mensaje.setCuerpo(mainBody);

        }

        String sender = defaultSender;
        if (organos != null && organos.size() == 1) {
            OrganoParametro organoParametro = organoDAO.getOrganoParametro(organos.get(0).getId(), organos.get(0).isExterno());
            if (organoParametro.getEmail() != null) {
                sender = organoParametro.getEmail();
            }
        }

        mensaje.setFrom(sender);
        mensaje.setReplyTo(getReplyTo(reunion));
        mensaje.setDestinos(miembros);
        mensaje.setAutorizados(autorizados);

        notificacionesDAO.enviaNotificacion(mensaje);
    }

    private void buildAndSendMessage(Reunion reunion, List<String> miembros, List<String> autorizados, String asunto,
                                     String textoConvocatoria, String textoConvocatoriaAlternativo, Boolean isBorrador, String textoInformar, String textoInformarAlternativo) throws Exception {
        buildAndSendMessage(reunion, miembros, autorizados, asunto, textoConvocatoria, textoConvocatoriaAlternativo, true, textoInformar, textoInformarAlternativo);


    }

    private Template getTemplateReunionEmail(
            Reunion reunion,
            List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados
    ) {
        Template template = new HTMLTemplate("email-envio-convocatoria-" + mainLanguage, templatesPath);
        template.put("reunion", reunion);
        template.put("puntosOrdenDiaOrdenados", puntosOrdenDiaOrdenados);
        template.put("charset", charset);
        template.put("publicUrl", publicUrl);

        numeracionMultinivel(puntosOrdenDiaOrdenados, null);

        PuntoOrdenDiaMultinivel puntoUnico = null;
        if (puntosOrdenDiaOrdenados.size() == 1) {
            puntoUnico = puntosOrdenDiaOrdenados.get(0);
            template.put("puntoUnico", puntoUnico);
        }

        String diaSemanaTextoSegundaConvocatoria = null;
        if (reunion.getFechaSegundaConvocatoria() != null) {
            diaSemanaTextoSegundaConvocatoria = DateUtils.getDiaSemanaTexto(reunion.getFechaSegundaConvocatoria(), "ca");
        }

        template.put("diaSemanaFechaPrimeraConvocatoria", DateUtils.getDiaSemanaTexto(reunion.getFecha(), "ca"));
        template.put("diaSemanaFechaSegundaConvocatoria", diaSemanaTextoSegundaConvocatoria);

        return template;
    }

    public void numeracionMultinivel(List<PuntoOrdenDiaMultinivel> puntos, String nivelPadre) {
        int i = 1;
        for (PuntoOrdenDiaMultinivel puntoMultinivel : puntos) {
            String multiNivel = nivelPadre != null ? String.format("%s.%s", nivelPadre, i) : String.valueOf(i);
            puntoMultinivel.setNumeracionMultinivel(multiNivel);
            List<PuntoOrdenDiaMultinivel> puntosInferiores = puntoMultinivel.getPuntosInferioresAsOrderedList();
            if (puntosInferiores != null) {
                List<PuntoOrdenDiaMultinivel> subpuntos = new ArrayList<PuntoOrdenDiaMultinivel>(puntosInferiores);
                subpuntos.sort(Comparator.comparing(punto -> punto.getOrden()));
                numeracionMultinivel(subpuntos, multiNivel);
            }
            i++;

        }
    }

    private String getEmailOrganoConvocante(Set<OrganoReunion> reunionOrganos) {
        OrganoReunion organoConvocante = reunionOrganos.iterator().next();
        OrganoParametro organoParametro =
                organoDAO.getOrganoParametro(organoConvocante.getOrganoId(), organoConvocante.isExterno());
        if (organoParametro != null && organoParametro.getEmail() != null && !organoParametro.getEmail().isEmpty()) {
            return organoParametro.getEmail();
        }

        return null;
    }

    private List<String> getAutorizados(Reunion reunion) {
        List<OrganoAutorizado> autorizados = organoAutorizadoDAO.getAutorizadosByReunionId(reunion.getId());

        return autorizados.stream()
                .filter(a -> a.getPersonaEmail() != null)
                .map(a -> a.getPersonaEmail())
                .collect(toList());
    }

    public List<String> getMiembros(Reunion reunion, Boolean confirmados)
            throws ReunionNoDisponibleException, MiembrosExternosException {
        List<OrganoReunionMiembro> listaAsistentesReunion;

        if (confirmados) {
            listaAsistentesReunion = organoReunionMiembroDAO.getAsistentesByReunionId(reunion.getId());
        } else {
            listaAsistentesReunion = organoReunionMiembroDAO.getMiembrosByReunionId(reunion.getId());
        }

        List<Persona> invitados = reunionDAO.getInvitadosByReunionId(reunion.getId());

        List<String> emailMiembros =
                listaAsistentesReunion.stream().map(AvisosReunion::obtenerMailAsistente).collect(toList());

        List<String> emailInvitados = invitados.stream().map(invitado -> invitado.getEmail()).collect(toList());

        emailMiembros.addAll(emailInvitados);

        return emailMiembros.stream().distinct().collect(toList());
    }

    public void enviarAvisoAnulacionReunion(Mensaje mensaje) throws NotificacionesException {
        notificacionesDAO.enviaNotificacion(mensaje);
    }

    private Template getTemplateAnulacionReunionEmail(
            Reunion reunion
    ) {
        Template template = new HTMLTemplate("email-envio-anulacion-" + mainLanguage, templatesPath);

        if (correosBilingue) {
            if (alternativeLanguage != null && !alternativeLanguage.isEmpty()) {
                template = new HTMLTemplate("email-envio-anulacion", templatesPath);
            }
        }
        template.put("reunion", reunion);
        template.put("mainLanguage", mainLanguage);

        String diaSemanaTextoSegundaConvocatoria = null;
        String diaSemanaTextoSegundaConvocatoriaEs = null;
        if (reunion.getFechaSegundaConvocatoria() != null) {
            diaSemanaTextoSegundaConvocatoria = DateUtils.getDiaSemanaTexto(reunion.getFechaSegundaConvocatoria(), "ca");
            diaSemanaTextoSegundaConvocatoriaEs = DateUtils.getDiaSemanaTexto(reunion.getFechaSegundaConvocatoria(), "es");
        }

        template.put("diaSemanaFechaPrimeraConvocatoria", DateUtils.getDiaSemanaTexto(reunion.getFecha(), "ca"));
        template.put("diaSemanaFechaPrimeraConvocatoriaEs", DateUtils.getDiaSemanaTexto(reunion.getFecha(), "es"));
        template.put("diaSemanaFechaSegundaConvocatoria", diaSemanaTextoSegundaConvocatoria);
        template.put("diaSemanaFechaSegundaConvocatoriaEs", diaSemanaTextoSegundaConvocatoriaEs);

        return template;
    }

    public Mensaje getMensajeAvisoAnulacion(Reunion reunion) throws Exception {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        List<String> miembros = getMiembros(reunion, false);
        List<String> autorizados = getAutorizados(reunion);

        String asunto = "[GOC] " + getTexto(resourceBundle, "mail.reunion.anulacion");

        asunto += " " + getTexto(resourceBundle, "mail.reunion.convocatoria") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null) {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

        Mensaje mensaje = new Mensaje();
        mensaje.setReunionId(reunion.getId());
        mensaje.setAsunto(asunto);

        Template template = getTemplateAnulacionReunionEmail(reunion);
        mensaje.setCuerpo(new String(template.process()));
        mensaje.setFrom(defaultSender);
        mensaje.setReplyTo(getReplyTo(reunion));
        mensaje.setDestinos(miembros);
        mensaje.setAutorizados(autorizados);

        return mensaje;
    }

    @Transactional
    public void enviaEmailTextoLibre(Reunion reunion, String cuerpoEmail) throws Exception {
        List<String> miembros = getMiembros(reunion, false);
        List<String> autorizados = getAutorizados(reunion);

        String asunto = "[GOC]";

        asunto += " comunicat " + getNombreOrganos(reunion);
        asunto += " en relació a la reunió " + reunion.getAsunto();

        Mensaje mensaje = new Mensaje();
        mensaje.setReunionId(reunion.getId());
        mensaje.setAsunto(asunto);

        mensaje.setCuerpo(cuerpoEmail.replaceAll("\n", "<br>"));
        mensaje.setFrom(defaultSender);
        mensaje.setReplyTo(getReplyTo(reunion));
        mensaje.setDestinos(miembros);
        mensaje.setAutorizados(autorizados);

        notificacionesDAO.enviaNotificacion(mensaje);
    }

    public void informarOrgano(
            Reunion reunion,
            List<Organo> organos,
            List<Miembro> miembrosAInformar,
            Boolean enviarOrdenDia,
            String textoInformar,
            String textoInformarAlternativo
    ) throws Exception {
        List<String> miembros = miembrosAInformar.stream().map(m -> m.getEmail()).collect(toList());

        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);

        String asunto = "[GOC] ";

        asunto += resourceBundle.getString("mail.reunion.informacionConvocatoria") + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null) {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());


        buildAndSendMessage(reunion, organos, miembros, null, asunto, null, null, enviarOrdenDia, false, false, textoInformar, textoInformarAlternativo);
    }

    @Transactional
    public void notificarPersonasQueYahabianDelegado(List<String> mailPersonasANotificar, Long reunionId) throws Exception {
        Reunion reunionById = reunionDAO.getReunionById(reunionId);

        String asunto = "[GOC]";

        asunto += " Baixa de delegació de vot en reunió " + getNombreOrganos(reunionById);

        String textoAux =
                "La delegació de vot d'aquesta reunió ha sigut donada de baixa, ja no es permet la delegació de vot a la reunió.";

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunionById.getFecha(), "ca") + " " + dataFormatter.format(reunionById.getFecha());

        buildAndSendMessageWithExtraText(reunionById, mailPersonasANotificar, asunto, textoAux);
    }

    public void enviarMailAMiembroPorDelegadoEliminado(Long reunionId, OrganoReunionMiembro miembroAfectado) throws Exception {
        Reunion reunionById = reunionDAO.getReunionById(reunionId);

        String asunto = "[GOC]";

        asunto += " Delegació de vot eliminada en " + getNombreOrganos(reunionById);

        String textoAux =
                "La persona en qui vau delegar el vot per a esta reunió no pot assistir. S'elimina la vostra delegació perquè pugueu seleccionar a una altra persona.";

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunionById.getFecha(), "ca") + " " + dataFormatter.format(reunionById.getFecha());

        buildAndSendMessageWithExtraText(reunionById, miembroAfectado.getEmail(), asunto, textoAux);
    }
}
