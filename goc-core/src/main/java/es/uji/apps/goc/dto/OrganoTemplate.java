package es.uji.apps.goc.dto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.enums.CodigoCargoEnum;

public class OrganoTemplate
{

    private String id;
    private String nombre;
    private Boolean inactivo;

    private Long tipoOrganoId;
    private String tipoNombre;
    private String tipoCodigo;

    private List<MiembroTemplate> asistentes;
    private List<MiembroTemplate> ausentes;
    private List<MiembroTemplate> ausentesJustificanAusencia;
    private Boolean actaProvisionalActiva;
    private Boolean permiteAbstencionVoto;
    private Boolean verAsistentes;    
    private Boolean verDelegaciones;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getTipoOrganoId()
    {
        return tipoOrganoId;
    }

    public void setTipoOrganoId(Long tipoOrganoId)
    {
        this.tipoOrganoId = tipoOrganoId;
    }

    public String getTipoNombre()
    {
        return tipoNombre;
    }

    public void setTipoNombre(String tipoNombre)
    {
        this.tipoNombre = tipoNombre;
    }

    public String getTipoCodigo()
    {
        return tipoCodigo;
    }

    public void setTipoCodigo(String tipoCodigo)
    {
        this.tipoCodigo = tipoCodigo;
    }

    public OrganoTemplate()
    {
    }

    public OrganoTemplate(String id)
    {
        this.id = id;
    }
    
    

    /*public List<MiembroTemplate> getAsistentesHojasFirmas()
    {
        return orderMiembrosYSuplentes(asistentes);
    }*/
    
    public List<MiembroTemplate> getAsistentesHojasFirmas() {
    	return asistentes;
    }
    
    public List<MiembroTemplate> getAsistentes()
    {
    	return orderMiembros(asistentes);
    }

    public void setAsistentes(List<MiembroTemplate> asistentes)
    {
        this.asistentes = asistentes;
    }

    public Boolean getInactivo()
    {
        return inactivo;
    }

    public void setInactivo(Boolean inactivo)
    {
        this.inactivo = inactivo;
    }

    public List<MiembroTemplate> getAusentes()
    {
        return orderMiembros(ausentes);
    }

    public void setAusentes(List<MiembroTemplate> ausentes)
    {
        this.ausentes = ausentes;
    }

    public List<MiembroTemplate> getAusentesJustificanAusencia() {
		return ausentesJustificanAusencia;
	}

	public void setAusentesJustificanAusencia(List<MiembroTemplate> ausentesJustificanAusencia) {
		this.ausentesJustificanAusencia = ausentesJustificanAusencia;
	}

	private List<MiembroTemplate> orderMiembros(List<MiembroTemplate> miembros)
    {
        if (miembros == null) return null;

        List<MiembroTemplate> miembrosOrdenados = new ArrayList<>();
        
        miembros = miembros.stream().sorted(Comparator.comparing(MiembroTemplate::getNombreLimpio)).collect(Collectors.toList());

        miembrosOrdenados.addAll(miembros.stream().filter(m -> m.getCargo().getCodigo().equalsIgnoreCase(CodigoCargoEnum.PRESIDENTE.codigo)).collect(Collectors.toList()));
        miembrosOrdenados.addAll(miembros.stream().filter(m -> m.getCargo().getCodigo().equalsIgnoreCase(CodigoCargoEnum.SECRETARIO.codigo)).collect(Collectors.toList()));
        miembrosOrdenados.addAll(miembros.stream().filter(m -> ! (m.getCargo().getCodigo().equalsIgnoreCase(CodigoCargoEnum.SECRETARIO.codigo) || (m.getCargo().getCodigo().equalsIgnoreCase(CodigoCargoEnum.PRESIDENTE.codigo)))).collect(Collectors.toList()));

        return miembrosOrdenados;
    }

    public void setActaProvisionalActiva(Boolean actaProvisionalActiva)
    {
        this.actaProvisionalActiva = actaProvisionalActiva;
    }

    public Boolean getActaProvisionalActiva()
    {
        return actaProvisionalActiva;
    }

    public Boolean getPermiteAbstencionVoto()
    {
        return permiteAbstencionVoto;
    }

    public void setPermiteAbstencionVoto(Boolean permiteAbstencionVoto)
    {
        this.permiteAbstencionVoto = permiteAbstencionVoto;
    }

	public Boolean getVerAsistentes() {
		return verAsistentes;
	}

	public void setVerAsistentes(Boolean verAsistentes) {
		this.verAsistentes = verAsistentes;
	}

    public Boolean getVerDelegaciones() {
    	return verDelegaciones;
    }
    
    public void setVerDelegaciones(Boolean verDelegaciones) {
    	this.verDelegaciones = verDelegaciones;
    }

}
