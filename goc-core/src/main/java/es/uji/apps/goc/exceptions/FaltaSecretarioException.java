package es.uji.apps.goc.exceptions;

public class FaltaSecretarioException extends Exception {
	
    public FaltaSecretarioException()
    {
        super("appI18N.common.faltaSecretario");
    }
    
    public FaltaSecretarioException(String message)
    {
        super(message);
    }
}