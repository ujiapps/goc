package es.uji.apps.goc.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "GOC_VW_REUNIONES_PERMISOS")
public class ReunionPermiso {
    @Id
    private Long id;

    private String asunto;

    @Column(name = "ASUNTO_ALT")
    private String asuntoAlternativo;

    private Date fecha;

    private Boolean completada;

    private Boolean asistente;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "PERSONA_NOMBRE")
    private String personaNombre;

    @Column(name = "URL_ACTA")
    private String urlActa;

    @Column(name = "URL_ACTA_ALT")
    private String urlActaAlternativa;

    @Column(name = "URL_ASISTENCIA")
    private String urlAsistencia;

    @Column(name = "URL_ASISTENCIA_ALT")
    private String urlAsistenciaAlternativa;

    @Column(name = "ENLACE_ACTA_PROVISIONAL_ACTIVO")
    private Boolean enlaceActaProvisionalActivo;

    @Column(name = "AVISO_PRIMERA_REUNION")
    private Boolean avisoPrimeraReunion;

    @Column(name = "ORGANO_ID")
    private Long organoId;

    @Column(name = "ORGANO_EXTERNO")
    private Boolean organoExterno;

    @Column(name = "ORGANO_NOMBRE")
    private String organoNombre;

    @Column(name = "ORGANO_NOMBRE_ALT")
    private String organoNombreAlt;

    @Column(name = "RESPONSABLE_VOTO_ID")
    private Long responsableVotoId;

    @Column(name = "RESPONSABLE_VOTO_NOM")
    private String responsableVotoNom;

    @Column(name = "VER_REUNION_SIN_CONVOCAR")
    private Boolean verReunionSinConvocar;

    public ReunionPermiso() {
    }

    public ReunionPermiso(Long reunionId) {
        this.id = reunionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getAsuntoAlternativo() {
        return asuntoAlternativo;
    }

    public void setAsuntoAlternativo(String asuntoAlternativo) {
        this.asuntoAlternativo = asuntoAlternativo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getCompletada() {
        return completada;
    }

    public void setCompletada(Boolean completada) {
        this.completada = completada;
    }

    public Boolean getAsistente() {
        return asistente;
    }

    public void setAsistente(Boolean asistente) {
        this.asistente = asistente;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getPersonaNombre() {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre) {
        this.personaNombre = personaNombre;
    }

    public String getUrlActa() {
        return urlActa;
    }

    public void setUrlActa(String urlActa) {
        this.urlActa = urlActa;
    }

    public String getUrlActaAlternativa() {
        return urlActaAlternativa;
    }

    public void setUrlActaAlternativa(String urlActaAlternativa) {
        this.urlActaAlternativa = urlActaAlternativa;
    }

    public String getUrlAsistencia() {
        return urlAsistencia;
    }

    public void setUrlAsistencia(String urlAsistencia) {
        this.urlAsistencia = urlAsistencia;
    }

    public String getUrlAsistenciaAlternativa() {
        return urlAsistenciaAlternativa;
    }

    public void setUrlAsistenciaAlternativa(String urlAsistenciaAlternativa) {
        this.urlAsistenciaAlternativa = urlAsistenciaAlternativa;
    }

    public Boolean getEnlaceActaProvisionalActivo() {
        return enlaceActaProvisionalActivo;
    }

    public void setEnlaceActaProvisionalActivo(Boolean enlaceActaProvisionalActivo) {
        this.enlaceActaProvisionalActivo = enlaceActaProvisionalActivo;
    }

    public Boolean getAvisoPrimeraReunion() {
        return avisoPrimeraReunion;
    }

    public void setAvisoPrimeraReunion(Boolean avisoPrimeraReunion) {
        this.avisoPrimeraReunion = avisoPrimeraReunion;
    }

    public String getOrganoNombre() {
        return organoNombre;
    }

    public void setOrganoNombre(String organoNombre) {
        this.organoNombre = organoNombre;
    }

    public String getOrganoNombreAlt() {
        return organoNombreAlt;
    }

    public void setOrganoNombreAlt(String organoNombreAlt) {
        this.organoNombreAlt = organoNombreAlt;
    }

    public Long getResponsableVotoId() {
        return responsableVotoId;
    }

    public void setResponsableVotoId(Long responsableVotoId) {
        this.responsableVotoId = responsableVotoId;
    }

    public String getResponsableVotoNom() {
        return responsableVotoNom;
    }

    public void setResponsableVotoNom(String responsableVotoNom) {
        this.responsableVotoNom = responsableVotoNom;
    }

    public Boolean getVerReunionSinConvocar() {
        return verReunionSinConvocar;
    }

    public void setVerReunionSinConvocar(Boolean verReunionSinConvocar) {
        this.verReunionSinConvocar = verReunionSinConvocar;
    }

    public Long getOrganoId() {
        return organoId;
    }

    public void setOrganoId(Long organoId) {
        this.organoId = organoId;
    }

    public Boolean getOrganoExterno() {
        return organoExterno;
    }

    public void setOrganoExterno(Boolean organoExterno) {
        this.organoExterno = organoExterno;
    }
}
