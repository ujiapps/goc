package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class SuplenteYaAsignadoAnteriormente extends CoreDataBaseException
{
    public SuplenteYaAsignadoAnteriormente()
    {
        super("Ya está asignado como suplente en esta misma reunión");
    }

    public SuplenteYaAsignadoAnteriormente(String message)
    {
        super(message);
    }
}
