package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.OrganoReunionMiembro;

public class ReunionAcuerdos {

    private String acuerdos;

    private OrganoReunionMiembro responsableActa;

    public String getAcuerdos() {
        return acuerdos;
    }

    public void setAcuerdos(String acuerdos) {
        this.acuerdos = acuerdos;
    }

    public OrganoReunionMiembro getResponsableActa() {
        return responsableActa;
    }

    public void setResponsableActa(OrganoReunionMiembro responsableActa) {
        this.responsableActa = responsableActa;
    }
}
