package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoPuedeAsignarSuplenteSiYaHaVotadoException extends CoreBaseException
{
    public NoPuedeAsignarSuplenteSiYaHaVotadoException() {
        super("No puede asignar suplente si ya ha votado");
    }
}
