package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class RecuentoVotoInvalidoParaPuntoNoVotableException extends CoreBaseException
{
    public RecuentoVotoInvalidoParaPuntoNoVotableException(String tipoRecuentoVoto) {
        super("El tipo de recuento de voto '" + tipoRecuentoVoto + "'  no es válido para un punto no votable");
    }
}
