package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class VotoDelegadoException extends CoreBaseException
{
    public VotoDelegadoException() {
        super("No puede votar, ha delegado su voto");
    }
}
