package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class MiembroNoPerteneceReunionException extends CoreBaseException
{
    public MiembroNoPerteneceReunionException() {
        super("Este miembro no pertenece a la reunión");
    }
}
