package es.uji.apps.goc.model;

public class ResponsableFirma
{
    private String id;
    private String nombre;
    private String email;
    private String organo;
    private String organoAlternativo;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOrgano()
    {
        return organo;
    }

    public void setOrgano(String organo)
    {
        this.organo = organo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getOrganoAlternativo()
    {
        return organoAlternativo;
    }

    public void setOrganoAlternativo(String organoAlternativo)
    {
        this.organoAlternativo = organoAlternativo;
    }
}
