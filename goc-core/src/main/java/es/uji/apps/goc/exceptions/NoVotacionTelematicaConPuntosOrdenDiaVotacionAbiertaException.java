package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException extends CoreBaseException
{
    public NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException() {
        super("La reunión no tiene votacion telemática y tiene puntos orden día con votación abierta no nula");
    }
}
