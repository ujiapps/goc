package es.uji.apps.goc.dto;

import com.google.common.base.Strings;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.enums.TipoRecuentoVotoEnum;
import es.uji.apps.goc.enums.TipoVotacionEnum;
import es.uji.apps.goc.exceptions.ProcedimientoVotacionInvalidoParaPuntoNoVotableException;
import es.uji.apps.goc.exceptions.RecuentoVotoInvalidoParaPuntoNoVotableException;
import es.uji.apps.goc.exceptions.TipoRecuentoVotoNoExisteException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "GOC_REUNIONES_PUNTOS_ORDEN_DIA")
public class PuntoOrdenDia extends PuntoOrdenDiaBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String titulo;

    @Column(name = "TITULO_ALT")
    private String tituloAlternativo;

    private String descripcion;

    @Column(name = "DESCRIPCION_ALT")
    private String descripcionAlternativa;

    private Long orden;

    private String acuerdos;

    @Column(name = "ACUERDOS_ALT")
    private String acuerdosAlternativos;

    private Boolean publico;

    private String deliberaciones;

    @Column(name = "DELIBERACIONES_ALT")
    private String deliberacionesAlternativas;

    @Column(name = "URL_ACTA")
    private String urlActa;

    @Column(name = "URL_ACTA_ALT")
    private String urlActaAlternativa;

    @Column(name = "URL_ACTA_ANTERIOR")
    private String urlActaAnterior;

    @Column(name = "URL_ACTA_ANTERIOR_ALT")
    private String urlActaAnteriorAlt;

    @Column(name = "EDITADO_EN_REAPERTURA")
    private Boolean editado;

    @Column(name = "VOTABLE_EN_CONTRA")
    private Boolean votableEnContra;

    @Column(name = "TIPO_VOTACION")
    private String tipoVotacion;

    @OneToMany(mappedBy = "puntoOrdenDia", cascade = CascadeType.REMOVE)
    private Set<PuntoOrdenDiaDocumento> puntoOrdenDiaDocumentos;

    @OneToMany(mappedBy = "puntoOrdenDia", cascade = CascadeType.REMOVE)
    private Set<PuntoOrdenDiaAcuerdo> puntoOrdenDiaAcuerdos;

    @OneToMany(mappedBy = "puntoOrdenDia", cascade = CascadeType.ALL)
    private Set<PuntoOrdenDiaDescriptor> puntoOrdenDiaDescriptores;

    @ManyToOne
    @JoinColumn(name = "ID_PUNTO_SUPERIOR", referencedColumnName = "id")
    private PuntoOrdenDia puntoSuperior;

    @OneToMany(mappedBy = "puntoSuperior", cascade = CascadeType.REMOVE)
    private Set<PuntoOrdenDia> puntosInferiores;

    @Column(name = "TIPO_VOTO")
    private Boolean tipoVoto;

    @Column(name = "FECHA_INICIO_VOTACION")
    private Date fechaInicioVotacion;

    @Column(name = "FECHA_FIN_VOTACION")
    private Date fechaFinVotacion;

    @OneToMany(mappedBy = "puntoOrdenDia")
    private Set<PersonaPuntoVotoDTO> personasPuntoVoto;

    @OneToMany(mappedBy = "puntoOrdenDia")
    private Set<PuntoVotoPrivadoDTO> puntoVotoPrivado;

    @OneToMany(mappedBy = "puntoOrdenDia")
    private Set<VotantePrivadoDTO> votantePuntoVotoPrivado;

    @Column(name = "VOTACION_ABIERTA")
    private Boolean votacionAbierta;

    @Column(name = "TIPO_RECUENTO_VOTO")
    private String tipoRecuentoVoto;

    @Column(name = "VOTACION_CERRADA_MANUALMENTE")
    private Boolean votacionCerradaManualmente;

    public PuntoOrdenDia() {
    }

    public PuntoOrdenDia(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }

    public Reunion getReunion() {
        return reunion;
    }

    public void setReunion(Reunion reunion) {
        this.reunion = reunion;
    }

    public Set<PuntoOrdenDiaDocumento> getPuntoOrdenDiaDocumentos() {
        return puntoOrdenDiaDocumentos;
    }

    public void setPuntoOrdenDiaDocumentos(Set<PuntoOrdenDiaDocumento> puntoOrdenDiaDocumentos) {
        this.puntoOrdenDiaDocumentos = puntoOrdenDiaDocumentos;
    }

    public String getAcuerdos() {
        return acuerdos;
    }

    public void setAcuerdos(String acuerdos) {
        this.acuerdos = acuerdos;
    }

    public String getDeliberaciones() {
        return deliberaciones;
    }

    public void setDeliberaciones(String deliberaciones) {
        this.deliberaciones = deliberaciones;
    }

    public Boolean isPublico() {
        return publico;
    }

    public String getTituloAlternativo() {
        return tituloAlternativo;
    }

    public void setTituloAlternativo(String tituloAlternativo) {
        this.tituloAlternativo = tituloAlternativo;
    }

    public String getDescripcionAlternativa() {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa) {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public String getAcuerdosAlternativos() {
        return acuerdosAlternativos;
    }

    public void setAcuerdosAlternativos(String acuerdosAlternativos) {
        this.acuerdosAlternativos = acuerdosAlternativos;
    }

    public String getDeliberacionesAlternativas() {
        return deliberacionesAlternativas;
    }

    public void setDeliberacionesAlternativas(String deliberacionesAlternativas) {
        this.deliberacionesAlternativas = deliberacionesAlternativas;
    }

    public Set<PuntoOrdenDiaDescriptor> getPuntoOrdenDiaDescriptores() {
        return puntoOrdenDiaDescriptores;
    }

    public void setPuntoOrdenDiaDescriptores(Set<PuntoOrdenDiaDescriptor> puntoOrdenDiaDescriptores) {
        this.puntoOrdenDiaDescriptores = puntoOrdenDiaDescriptores;
    }

    public Set<PuntoOrdenDiaAcuerdo> getPuntoOrdenDiaAcuerdos() {
        return puntoOrdenDiaAcuerdos;
    }

    public void setPuntoOrdenDiaAcuerdos(Set<PuntoOrdenDiaAcuerdo> puntoOrdenDiaAcuerdos) {
        this.puntoOrdenDiaAcuerdos = puntoOrdenDiaAcuerdos;
    }

    public String getUrlActa() {
        return urlActa;
    }

    public void setUrlActa(String urlActa) {
        this.urlActa = urlActa;
    }

    public String getUrlActaAlternativa() {
        return urlActaAlternativa;
    }

    public void setUrlActaAlternativa(String urlActaAlternativa) {
        this.urlActaAlternativa = urlActaAlternativa;
    }

    public PuntoOrdenDia getPuntoSuperior() {
        return puntoSuperior;
    }

    public void setPuntoSuperior(PuntoOrdenDia puntoSuperior) {
        this.puntoSuperior = puntoSuperior;
    }

    public Set<PuntoOrdenDia> getPuntosInferiores() {
        return puntosInferiores;
    }

    public void setPuntosInferiores(Set<PuntoOrdenDia> puntosInferiores) {
        this.puntosInferiores = puntosInferiores;
    }

    public String getUrlActaAnterior() {
        return urlActaAnterior;
    }

    public void setUrlActaAnterior(String urlActaAnterior) {
        this.urlActaAnterior = urlActaAnterior;
    }

    public String getUrlActaAnteriorAlt() {
        return urlActaAnteriorAlt;
    }

    public void setUrlActaAnteriorAlt(String urlActaAnteriorAlt) {
        this.urlActaAnteriorAlt = urlActaAnteriorAlt;
    }

    public Boolean getEditado() {
        return editado;
    }

    public void setEditado(Boolean editado) {
        this.editado = editado;
    }

    public Boolean isEditado() {
        return this.editado != null && this.editado == true;
    }

    public void cleanBeforeDuplicate() {
        this.setAcuerdos(null);
        this.setAcuerdosAlternativos(null);
        this.setDeliberaciones(null);
        this.setId(null);
        this.setDeliberacionesAlternativas(null);
        this.setPuntoOrdenDiaDocumentos(null);
        this.setPuntoOrdenDiaAcuerdos(null);
        this.setUrlActa(null);
        this.setUrlActaAlternativa(null);
        this.setPersonasPuntoVoto(null);
        this.setPuntoVotoPrivado(null);
        this.setVotantePuntoVotoPrivado(null);
        this.setVotacionCerradaManualmente(false);
    }

    public Boolean getPublico() {
        return publico;
    }

    public void setPublico(Boolean publico) {
        this.publico = publico;
    }

    public Boolean getTipoVoto() {
        return tipoVoto;
    }

    public void setTipoVoto(Boolean tipoVoto) {
        this.tipoVoto = tipoVoto;
    }

    public Boolean isTipoVoto() {
        return tipoVoto;
    }

    public void setTipoProcedimientoVotacion(String tipoProcedimientoVotacion) {
        if (Strings.isNullOrEmpty(tipoProcedimientoVotacion)) {
            super.tipoProcedimientoVotacion = TipoProcedimientoVotacionEnum.NO_VOTABLE.name();
        } else {
            super.tipoProcedimientoVotacion = tipoProcedimientoVotacion;
        }
    }

    public Date getFechaInicioVotacion() {
        return fechaInicioVotacion;
    }

    public void setFechaInicioVotacion(Date fechaInicioVotacion) {
        this.fechaInicioVotacion = fechaInicioVotacion;
    }

    public Date getFechaFinVotacion() {
        return fechaFinVotacion;
    }

    public void setFechaFinVotacion(Date fechaFinVotacion) {
        this.fechaFinVotacion = fechaFinVotacion;
    }

    public Set<PersonaPuntoVotoDTO> getPersonasPuntoVoto() {
        return personasPuntoVoto;
    }

    public void setPersonasPuntoVoto(Set<PersonaPuntoVotoDTO> personasPuntoVoto) {
        this.personasPuntoVoto = personasPuntoVoto;
    }

    public Set<PuntoVotoPrivadoDTO> getPuntoVotoPrivado() {
        return puntoVotoPrivado;
    }

    public void setPuntoVotoPrivado(Set<PuntoVotoPrivadoDTO> puntoVotoPrivado) {
        this.puntoVotoPrivado = puntoVotoPrivado;
    }

    public Set<VotantePrivadoDTO> getVotantePuntoVotoPrivado() {
        return votantePuntoVotoPrivado;
    }

    public void setVotantePuntoVotoPrivado(Set<VotantePrivadoDTO> votantePuntoVotoPrivado) {
        this.votantePuntoVotoPrivado = votantePuntoVotoPrivado;
    }

    public String getTipoRecuentoVoto() {
        return tipoRecuentoVoto;
    }

    public void setTipoRecuentoVoto(String tipoRecuentoVoto) {
        if (Strings.isNullOrEmpty(tipoRecuentoVoto)) {
            this.tipoRecuentoVoto = TipoRecuentoVotoEnum.NO_VOTABLE.name();
        } else {
            this.tipoRecuentoVoto = tipoRecuentoVoto;
        }
    }

    public Boolean getVotacionAbierta() {
        return votacionAbierta;
    }

    public void setVotacionAbierta(Boolean votacionAbierta) {
        this.votacionAbierta = votacionAbierta;
    }

    public void checkValidezTipoRecuento() throws TipoRecuentoVotoNoExisteException {
        if (reunion.isVotacionTelematica()) {
            if (!TipoRecuentoVotoEnum.checkRecuentoVoto(tipoRecuentoVoto))
                throw new TipoRecuentoVotoNoExisteException();
        }
    }

    public void checkValidezAmbitoVotacion()
            throws RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        if (getTipoVoto() == null) {
            if (tipoRecuentoVoto != null && !TipoRecuentoVotoEnum.NO_VOTABLE.name().equals(tipoRecuentoVoto)) {
                throw new RecuentoVotoInvalidoParaPuntoNoVotableException(tipoRecuentoVoto);
            }

            if (tipoProcedimientoVotacion != null && !TipoProcedimientoVotacionEnum.NO_VOTABLE.name().equals(tipoProcedimientoVotacion)) {
                throw new ProcedimientoVotacionInvalidoParaPuntoNoVotableException(tipoProcedimientoVotacion);
            }
        }
    }

    public Boolean getVotableEnContra() {
        return votableEnContra;
    }

    public void setVotableEnContra(Boolean votableEnContra) {
        this.votableEnContra = votableEnContra;
    }

    public String getTipoVotacion() {
        return tipoVotacion;
    }

    public void setTipoVotacion(String tipoVotacion) {
        if (this.tipoVoto != null) {
            if (Strings.isNullOrEmpty(tipoVotacion)) {
                this.tipoVotacion = TipoVotacionEnum.TELEMATICA.name();
            } else {
                this.tipoVotacion = tipoVotacion;
            }
        } else {
            this.tipoVotacion = null;
        }
    }

    public Boolean getVotacionCerradaManualmente() {
        return votacionCerradaManualmente;
    }

    public void setVotacionCerradaManualmente(Boolean votacionCerradaManualmente) {
        this.votacionCerradaManualmente = votacionCerradaManualmente;
    }
}