package es.uji.apps.goc.charset;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleUTF8
{
    private static final String CHARSET = "UTF-8";
    private static final String DEFAULT_RESOURCE_BUNDLE_CHARSET = "ISO-8859-1";
    private ResourceBundle resourceBundle;

    public ResourceBundleUTF8(String file, String locale)
    {
        this.resourceBundle = ResourceBundle.getBundle(file, Locale.forLanguageTag(locale));
    }

    public String getString(String key)
    {
        try
        {
            return new String(this.resourceBundle.getString(key).getBytes(DEFAULT_RESOURCE_BUNDLE_CHARSET), CHARSET);
        } catch (UnsupportedEncodingException e)
        {
            System.out.println("Charset no soportado: " + CHARSET);
        }

        return "";
    }
}
