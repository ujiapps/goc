package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class PuntoNoVotableEnContraException extends CoreBaseException {
    public PuntoNoVotableEnContraException() {
        super("Este punto de orden del dia no permite votar en contra.");
    }
}
