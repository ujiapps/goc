package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class CargoNoSecretarioNoPuedeAbrirVotacionException extends CoreBaseException
{
    public CargoNoSecretarioNoPuedeAbrirVotacionException(){
        super("Solo los miembros con cargo secretario pueden abrir una votación");
    }
}
