package es.uji.apps.goc.dto;

import java.util.Date;

public interface Documentable
{
    Long getId();
    String getHash();
    String getDescripcion();
    String getMimeType();
    String getNombreFichero();
    Long getOrden();
    Date getFechaAdicion();
    byte[] getDatos();
    Long getCreadorId();
    PuntoOrdenDia getPuntoOrdenDia();
    String getDescripcionAlternativa();
    Boolean getPublico();
    Long getEditorId();
    Date getFechaEdicion();
    String getMotivoEdicion();
    Boolean getActivo();
}
