package es.uji.apps.goc.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "GOC_P_ORDEN_DIA_COMENTARIOS")
public class PuntoOrdenDiaComentario implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PUNTO_ORDEN_DIA_ID")
    private PuntoOrdenDia puntoOrdenDia;

    @Column(name="FECHA")
    private Date fecha;

    @Column(name="CREADOR_ID")
    private Long creadorId;

    @Column(name="CREADOR_NOMBRE")
    private String creadorNombre;

    @Column(name="COMENTARIO")
    private String comentario;

    @ManyToOne
    @JoinColumn(name = "COMENTARIO_PADRE_ID", referencedColumnName = "id")
    private PuntoOrdenDiaComentario comentarioPadre;

    @Transient
    private Set<PuntoOrdenDiaComentario> respuestas;

    public PuntoOrdenDiaComentario()
    {
    }

    public PuntoOrdenDiaComentario(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public PuntoOrdenDia getPuntoOrdenDia()
    {
        return puntoOrdenDia;
    }

    public void setPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia)
    {
        this.puntoOrdenDia = puntoOrdenDia;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getCreadorId()
    {
        return creadorId;
    }

    public void setCreadorId(Long creadorId)
    {
        this.creadorId = creadorId;
    }

    public String getCreadorNombre()
    {
        return creadorNombre;
    }

    public void setCreadorNombre(String creadorNombre)
    {
        this.creadorNombre = creadorNombre;
    }

    public PuntoOrdenDiaComentario getComentarioPadre()
    {
        return comentarioPadre;
    }

    public void setComentarioPadre(PuntoOrdenDiaComentario comentarioPadre)
    {
        this.comentarioPadre = comentarioPadre;
    }

    public Set<PuntoOrdenDiaComentario> getRespuestas()
    {
        return respuestas;
    }

    public void setRespuestas(Set<PuntoOrdenDiaComentario> respuestas)
    {
        this.respuestas = respuestas;
    }
}
