package es.uji.apps.goc.exceptions;

public class FaltaPresidenteException extends Exception {
	
    public FaltaPresidenteException()
    {
        super("appI18N.common.faltaPresidente");
    }
    
    public FaltaPresidenteException(String message)
    {
        super(message);
    }
}