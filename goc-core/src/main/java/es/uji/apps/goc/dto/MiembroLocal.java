package es.uji.apps.goc.dto;

import es.uji.apps.goc.enums.CodigoCargoEnum;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "GOC_MIEMBROS")
public class MiembroLocal implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    private String nombre;

    private String email;

    @ManyToOne
    @JoinColumn(name = "ORGANO_ID")
    private OrganoLocal organo;

    @ManyToOne
    @JoinColumn(name = "CARGO_ID")
    private Cargo cargo;

    @Column(name = "NATO")
    private boolean nato;

    public MiembroLocal() {}

    public MiembroLocal(Long id) {
        this.id = id;
    }

    public MiembroLocal(Long id, String nombre, String email, OrganoLocal organo) {
        this(id);
        this.nombre = nombre;
        this.email = email;
        this.organo = organo;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public OrganoLocal getOrgano()
    {
        return organo;
    }

    public void setOrgano(OrganoLocal organo)
    {
        this.organo = organo;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Cargo getCargo()
    {
        return cargo;
    }

    public void setCargo(Cargo cargo)
    {
        this.cargo = cargo;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public boolean getNato() {
        return nato;
    }

    public boolean isNato() {
        return getNato();
    }

    public void setNato(boolean nato) {
        this.nato = nato;
    }

    public Boolean isSecretario() {
        if (cargo == null || cargo.getCodigo() == null) return false;

        return CodigoCargoEnum.SECRETARIO.codigo.equals(cargo.getCodigo());
    }

    public Boolean isPresidente() {
        if (cargo == null || cargo.getCodigo() == null) return false;

        return CodigoCargoEnum.PRESIDENTE.codigo.equals(cargo.getCodigo());
    }
}