package es.uji.apps.goc.exceptions;

public class FaltaPresidenteYSecretarioException extends Exception {
	
    public FaltaPresidenteYSecretarioException()
    {
        super("appI18N.common.faltaPresidenteYSecretario");
    }
    
    public FaltaPresidenteYSecretarioException(String message)
    {
        super(message);
    }
}