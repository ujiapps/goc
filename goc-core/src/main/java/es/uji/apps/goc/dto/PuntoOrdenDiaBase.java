package es.uji.apps.goc.dto;

import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public class PuntoOrdenDiaBase
{

    @ManyToOne
    @JoinColumn(name = "REUNION_ID")
    protected Reunion reunion;

    @Column(name = "PROCEDIMIENTO_VOTACION")
    protected String tipoProcedimientoVotacion;

    public Boolean isVotacionAbrible()
    {
        if (reunion == null) return false;

        return !reunion.isCompletada() && (reunion.getFechaFinVotacion() == null || !new Date().after(
                reunion.getFechaFinVotacion()));
    }

    public Reunion getReunion()
    {
        return reunion;
    }

    public void setReunion(Reunion reunion)
    {
        this.reunion = reunion;
    }

    public String getTipoProcedimientoVotacion()
    {
        return tipoProcedimientoVotacion;
    }

    public void setTipoProcedimientoVotacion(String tipoProcedimientoVotacion)
    {
        this.tipoProcedimientoVotacion = tipoProcedimientoVotacion;
    }
}
