package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class VotoException extends CoreBaseException {
    public VotoException() {
        super("Error en el voto");
    }
}
