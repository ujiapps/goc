package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class PuntoNoVotableException extends CoreBaseException
{
    public PuntoNoVotableException() {
        super("El punto no es votable");
    }
}
