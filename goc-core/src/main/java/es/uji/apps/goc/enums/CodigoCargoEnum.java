package es.uji.apps.goc.enums;

public enum CodigoCargoEnum {
    SECRETARIO("SE"),
    PRESIDENTE("PR"),
    VOCAL("VO"),
    GERENTE("GE"),
    RECTOR("RE");

    public final String codigo;

    CodigoCargoEnum(String codigo) {
        this.codigo = codigo;
    }
}
