package es.uji.apps.goc.model;

public class OrganosPermisosWrapper {
    private Long id;
    private Boolean externo;
    private String nombre;
    private String nombreAlt;

    public OrganosPermisosWrapper(Long id, Boolean externo) {
        this.id = id;
        this.externo = externo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreAlt() {
        return nombreAlt;
    }

    public void setNombreAlt(String nombreAlt) {
        this.nombreAlt = nombreAlt;
    }

    public Boolean isExterno() {
        return externo;
    }

    public void setExterno(Boolean externo) {
        this.externo = externo;
    }
}

