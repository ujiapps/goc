package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class AsistenciaNoConfirmadaException extends CoreBaseException
{
    public AsistenciaNoConfirmadaException() {
        super("Debe confirmar la asistencia");
    }
}
