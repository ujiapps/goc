package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.exceptions.AsistenciaNoConfirmadaException;
import es.uji.apps.goc.exceptions.VotoDelegadoException;
import es.uji.apps.goc.exceptions.VotoException;
import es.uji.apps.goc.model.punto.PuntoVotable;
import org.apache.commons.lang3.Validate;

public abstract class Votante {
    private Persona persona;

    private OrganoReunionMiembro organoReunionMiembro;

    private PuntoVotable punto;

    public Votante(Persona persona, PuntoVotable punto, OrganoReunionMiembro organoReunionMiembro)
            throws VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        Validate.notNull(persona);
        Validate.notNull(punto);
        Validate.notNull(organoReunionMiembro);

        if (organoReunionMiembro.getSuplenteId() != null) {
            try {
                Validate.isTrue(
                        persona.getId().equals(organoReunionMiembro.getSuplenteId()));
            } catch (IllegalArgumentException e) {
                throw new VotoDelegadoException();
            }
        } else {
            if (organoReunionMiembro.getAsistencia() == null || !organoReunionMiembro.getAsistencia()) {
                if (organoReunionMiembro.getDelegadoVotoId() != null) {
                    try {
                        Validate.isTrue(
                                persona.getId().equals(organoReunionMiembro.getDelegadoVotoId()));
                    } catch (IllegalArgumentException e) {
                        throw new VotoDelegadoException();
                    }
                } else {
                    throw new AsistenciaNoConfirmadaException();
                }
            } else {
                Validate.isTrue(persona.getId().toString().equals(organoReunionMiembro.getMiembroId()));
            }
        }

        this.persona = persona;
        this.punto = punto;
        this.organoReunionMiembro = organoReunionMiembro;
    }

    public Persona getPersona() {
        return persona;
    }

    protected PuntoVotable getPunto() {
        return punto;
    }

    public OrganoReunionMiembro getOrganoReunionMiembro() {
        return organoReunionMiembro;
    }

    public Long getPuntoAVotarId() {
        return punto.getPuntoOrdenDia().getId();
    }

    public void checkVotacionAbierta() {
        this.getPunto().checkVotacionAbierta();
    }
}
