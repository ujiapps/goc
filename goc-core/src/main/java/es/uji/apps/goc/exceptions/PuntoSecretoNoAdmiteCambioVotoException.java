package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class PuntoSecretoNoAdmiteCambioVotoException extends CoreBaseException
{

    public PuntoSecretoNoAdmiteCambioVotoException() {
        super("Los puntos con tipo de voto secreto no admiten cambio de voto");
    }
}
