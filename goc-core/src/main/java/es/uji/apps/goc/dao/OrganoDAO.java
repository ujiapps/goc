package es.uji.apps.goc.dao;

import com.google.common.base.Strings;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.StringExpression;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoParametro;
import es.uji.apps.goc.dto.OrganoParametroPK;
import es.uji.apps.goc.dto.QOrganoAutorizado;
import es.uji.apps.goc.dto.QOrganoLocal;
import es.uji.apps.goc.dto.QOrganoParametro;
import es.uji.apps.goc.dto.QOrganoReunion;
import es.uji.apps.goc.dto.QOrganoReunionMiembro;
import es.uji.apps.goc.dto.QPuntoOrdenDia;
import es.uji.apps.goc.dto.QReunion;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.TipoOrgano;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OrganoDAO extends BaseDAODatabaseImpl
{
    private QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;
    private QOrganoAutorizado qOrganoAutorizado = QOrganoAutorizado.organoAutorizado;
    private QReunion qReunion = QReunion.reunion;
    private QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;
    private QOrganoParametro qOrganoParametro = QOrganoParametro.organoParametro;
    private QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    private QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;

    @Value("${goc.reunion.mostrarPublicarAcuerdos:false}")
    public boolean mostrarPublicarAcuerdos;

    @Value("${goc.votacion.permiteAbstencionVoto:true}")
    public boolean permiteAbstencionVoto;

    @Value("${goc.votacion.presidenteVotoDoble:false}")
    public boolean parametroPresidenteVotoDoble;

    public List<Organo> getOrganosByUserId(Long connectedUserId, String searchString)
    {
        JPAQuery query = new JPAQuery(entityManager);
        
        query = query.from(qOrganoLocal);
        query = query.where(qOrganoLocal.creadorId.eq(connectedUserId));
        
        if (searchString != null && !searchString.isEmpty()) {
			
        	// Limpiar cadena campo nombre BBDD
        	StringExpression nombreLimpio = Expressions.stringTemplate("function('translate', {0}, 'ÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöü', 'AEIOUAEIOUAEIOUAEIOUAEIOUAEIOU')", qOrganoLocal.nombre);
        	
        	query = query.where(nombreLimpio.containsIgnoreCase(searchString));
        
        }

        List<OrganoLocal> organos = query.orderBy(qOrganoLocal.fechaCreacion.desc()).list(qOrganoLocal);

        return organosLocalesToOrganos(organos);
    }

    public List<Organo> getOrganosByAutorizadoId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoLocal> organos = query.from(qOrganoLocal, qOrganoAutorizado)
                .where(qOrganoAutorizado.organoExterno.eq(false)
                        .and(qOrganoAutorizado.organoId.eq(qOrganoLocal.id.stringValue()))
                        .and(qOrganoAutorizado.personaId.eq(connectedUserId)))
                .orderBy(qOrganoLocal.fechaCreacion.desc())
                .list(qOrganoLocal);

        return organosLocalesToOrganos(organos);
    }

    public List<Organo> getOrganosExternosByAutorizadoId(Long connectedUserId, List<Organo> todosOrganosExternos) {
        JPAQuery query = new JPAQuery(entityManager);

        List<String> organos =  query.from(qOrganoAutorizado)
                        .where(qOrganoAutorizado.personaId.eq(connectedUserId))
                .list(qOrganoAutorizado.organoId);

        List<Organo> externosByAutorizado = new ArrayList<>();
        for(Organo organoExterno: todosOrganosExternos){
            if(organos.contains(organoExterno.getId())){
                externosByAutorizado.add(organoExterno);
            }
        }

        return externosByAutorizado;
    }

    private List<Organo> organosLocalesToOrganos(List<OrganoLocal> organosDTO)
    {
        List<Organo> organos = new ArrayList<>();

        for (OrganoLocal organoLocalDTO : organosDTO)
        {
            Organo organo = organoLocalToOrgano(organoLocalDTO);
            organos.add(organo);
        }

        return organos;
    }

    private Organo organoLocalToOrgano(OrganoLocal organoLocalDTO)
    {
        Organo organo = new Organo();

        organo.setId(organoLocalDTO.getId().toString());
        organo.setNombre(organoLocalDTO.getNombre());
        organo.setNombreAlternativo(organoLocalDTO.getNombreAlternativo());
        organo.setInactivo(organoLocalDTO.isInactivo());
        organo.setCreadorId(organoLocalDTO.getCreadorId());
        organo.setFechaCreacion(organoLocalDTO.getFechaCreacion());
        OrganoParametro organoParametro = this.getOrganoParametro(organo.getId(), organo.isExterno());
        Boolean convocarSinOrdenDia = false;
        Boolean actaProvisionalActiva = true;
        Boolean delegacionVotoMultiple = true;
        Boolean verAsistencia = true;
        Boolean presidenteVotoDoble = parametroPresidenteVotoDoble;
        String emailOrgano = null;
        organo.setTipoProcedimientoVotacionEnum(TipoProcedimientoVotacionEnum.ORDINARIO);

        organo.setPermiteAbstencionVoto(permiteAbstencionVoto);

        if(organoParametro != null)
        {
            if(organoParametro.getConvocarSinOrdenDia() != null)
                convocarSinOrdenDia = organoParametro.getConvocarSinOrdenDia();

            if(organoParametro.getActaProvisionalActiva() != null)
                actaProvisionalActiva = organoParametro.getActaProvisionalActiva();

            if(organoParametro.getDelegacionVotoMultiple() != null)
                delegacionVotoMultiple = organoParametro.getDelegacionVotoMultiple();

            if(organoParametro.getEmail() != null)
                emailOrgano = organoParametro.getEmail();

            if (!Strings.isNullOrEmpty(organoParametro.getTipoProcedimientoVotacion())) {
                organo.setTipoProcedimientoVotacionEnum(
                    TipoProcedimientoVotacionEnum.valueOf(organoParametro.getTipoProcedimientoVotacion()));
            }

            if(organoParametro.getPresidenteVotoDoble() != null)
                presidenteVotoDoble = organoParametro.getPresidenteVotoDoble();

            if(organoParametro.getPermiteAbstencionVoto() != null)
                organo.setPermiteAbstencionVoto(organoParametro.getPermiteAbstencionVoto());
            
            if(organoParametro.getVerAsistencia() != null) {
            	verAsistencia = organoParametro.getVerAsistencia();
            }

            if(organoParametro.getVerDelegaciones() != null)
            	organo.setVerDelegaciones(organoParametro.getVerDelegaciones());
        }
        organo.setConvocarSinOrdenDia(convocarSinOrdenDia);
        organo.setActaProvisionalActiva(actaProvisionalActiva);
        organo.setDelegacionVotoMultiple(delegacionVotoMultiple);
        organo.setPresidenteVotoDoble(presidenteVotoDoble);
        organo.setEmail(emailOrgano);
        organo.setVerAsistencia(verAsistencia);
        TipoOrgano tipoOrgano = new TipoOrgano(organoLocalDTO.getTipoOrganoLocal().getId());
        organo.setTipoOrgano(tipoOrgano);

        return organo;
    }

    public Organo getOrganoById(Long organoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoLocal> organos = query.from(qOrganoLocal).where(qOrganoLocal.id.eq(organoId)).list(qOrganoLocal);

        if (organos.size() == 0)
        {
            return null;
        }

        return organoLocalToOrgano(organos.get(0));
    }

    @Transactional
    public Organo insertOrgano(Organo organo, Long connectedUserId)
    {
        OrganoLocal organoLocal = organoLocalDesdeOrgano(organo);
        organoLocal.setCreadorId(connectedUserId);
        organoLocal.setFechaCreacion(new Date());
        organoLocal = this.insert(organoLocal);

        insertOrUpdateParametros(organoLocal.getId().toString(), false, organo.getConvocarSinOrdenDia(), organo.getEmail(),
                organo.getActaProvisionalActiva(), organo.getDelegacionVotoMultiple(), organo.getTipoProcedimientoVotacionName(),
                organo.getPresidenteVotoDoble(), organo.getPermiteAbstencionVoto(),organo.getVerAsistencia(),organo.getVerDelegaciones());
        return organoLocalToOrgano(organoLocal);
    }

    @Transactional
    public Organo updateOrgano(Organo organo)
    {
        OrganoLocal organoLocal = organoLocalDesdeOrgano(organo);
        organoLocal = this.update(organoLocal);

        return organoLocalToOrgano(organoLocal);
    }

    private OrganoLocal organoLocalDesdeOrgano(Organo organo)
    {
        OrganoLocal organoLocal = new OrganoLocal();

        if (organo.getId() != null)
        {
            organoLocal.setId(Long.parseLong(organo.getId()));
        }

        organoLocal.setInactivo(organo.isInactivo());
        organoLocal.setNombre(organo.getNombre());
        organoLocal.setNombreAlternativo(organo.getNombreAlternativo());
        organoLocal.setCreadorId(organo.getCreadorId());
        organoLocal.setFechaCreacion(organo.getFechaCreacion());

        if (organo.getTipoOrgano() != null)
        {
            TipoOrganoLocal tipoOrganoLocal = new TipoOrganoLocal(organo.getTipoOrgano().getId());
            organoLocal.setTipoOrganoLocal(tipoOrganoLocal);
        }

        return organoLocal;
    }

    public List<Tuple> getOrganosConReunionesPublicas(Long tipoOrganoId, Integer anyo)
    {
        BooleanExpression beWhere = null;
        if (anyo != null)
        {
            beWhere = qReunion.fecha.year().eq(anyo);
        }
        JPAQuery query = new JPAQuery(entityManager);

        if(!mostrarPublicarAcuerdos)
        {
            return query.from(qOrganoReunion)
                .join(qOrganoReunion.reunion, qReunion)
                .where(qOrganoReunion.tipoOrganoId.eq(tipoOrganoId)
                    .and(qReunion.publica.isTrue())
                    .and(qReunion.completada.isTrue())
                    .and(beWhere))
                .distinct()
                .list(qOrganoReunion.organoId, qOrganoReunion.organoNombre, qOrganoReunion.organoNombreAlternativo);
        } else {
            return query.from(qOrganoReunion)
                .join(qOrganoReunion.reunion, qReunion)
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .where(qOrganoReunion.tipoOrganoId.eq(tipoOrganoId)
                    .and(qReunion.publica.isTrue())
                    .and(
                        qReunion.completada.isTrue()
                        .or(qPuntoOrdenDia.urlActa.isNotNull())
                    )
                    .and(beWhere))
                .distinct()
                .list(qOrganoReunion.organoId, qOrganoReunion.organoNombre, qOrganoReunion.organoNombreAlternativo);
        }
    }

    public List<Organo> getOrganosByAdmin(boolean soloActivos, String searchString)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query = query.from(qOrganoLocal);
        
        if (searchString != null && !searchString.isEmpty()) {
			
        	// Limpiar cadena campo nombre BBDD
        	StringExpression nombreLimpio = Expressions.stringTemplate("function('translate', {0}, 'ÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöü', 'AEIOUAEIOUAEIOUAEIOUAEIOUAEIOU')", qOrganoLocal.nombre);
        	
        	query = query.where(nombreLimpio.containsIgnoreCase(searchString));
        
        }

        if(soloActivos)
        {
            query = query.where(qOrganoLocal.inactivo.eq(false).or(qOrganoLocal.inactivo.isNull()));
        }

        List<OrganoLocal> organos = query.orderBy(qOrganoLocal.fechaCreacion.desc()).list(qOrganoLocal);

        return organosLocalesToOrganos(organos);
    }

    public OrganoParametro getOrganoParametro(String organoId, Boolean ordinario)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoParametro)
            .where(qOrganoParametro.organoParametroPK.organoId.eq(organoId)
                .and(qOrganoParametro.organoParametroPK.ordinario.eq(ordinario))
            )
            .uniqueResult(qOrganoParametro);
    }

    @Transactional
    public void insertOrUpdateParametros(
        String organoId,
        Boolean externo,
        Boolean convocarSinOrdenDia,
        String email,
        Boolean actaProvisionalActiva,
        Boolean delegacionVotoMultiple,
        String tipoProcedimientoVotacion,
        Boolean presidenteVotoDoble,
        Boolean permiteAbstencionVoto,
        Boolean verAsistentes,
        Boolean verDelegaciones
    )
    {
        OrganoParametro organoParametro = new OrganoParametro();
        OrganoParametroPK organoParametroPK = new OrganoParametroPK(organoId, externo);
        organoParametro.setOrganoParametroPK(organoParametroPK);
        organoParametro.setConvocarSinOrdenDia(convocarSinOrdenDia);
        organoParametro.setEmail(email);
        organoParametro.setActaProvisionalActiva(actaProvisionalActiva);
        organoParametro.setDelegacionVotoMultiple(delegacionVotoMultiple);
        organoParametro.setTipoProcedimientoVotacion(tipoProcedimientoVotacion);
        organoParametro.setPresidenteVotoDoble(presidenteVotoDoble);
        organoParametro.setPermiteAbstencionVoto(permiteAbstencionVoto);
        organoParametro.setVerAsistencia(verAsistentes);
        organoParametro.setVerDelegaciones(verDelegaciones);

        entityManager.merge(organoParametro);
    }

    public boolean getOrganoAdmiteVotoMultipleByDelegadoVotoId(Long organoId, Boolean ordinario)
    {
        JPAQuery query = new JPAQuery(entityManager);

        Boolean admiteDelegacionVotoMultiple = query.from(qOrganoReunionMiembro).
            join(qOrganoParametro).on(qOrganoParametro.organoParametroPK.organoId.eq(qOrganoReunionMiembro.organoId)).
                on(qOrganoParametro.organoParametroPK.ordinario.eq(qOrganoReunionMiembro.organoExterno))
            .where(qOrganoReunionMiembro.organoId.eq(organoId.toString()).and(qOrganoReunionMiembro.organoExterno.eq(ordinario)))
            .singleResult(qOrganoParametro.delegacionVotoMultiple);
        return admiteDelegacionVotoMultiple != null ? admiteDelegacionVotoMultiple : true;
    }

    public List<String> getTipoProcedimientoByReunionId(Long reunionId){
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunion)
                .join(qReunion.reunionOrganos, qOrganoReunion)
                .join(qOrganoParametro)
                .on(qOrganoParametro.organoParametroPK.organoId.eq(qOrganoReunion.organoId))
                .where(qReunion.id.eq(reunionId))
                .list(qOrganoParametro.tipoProcedimientoVotacion);
    }
}
