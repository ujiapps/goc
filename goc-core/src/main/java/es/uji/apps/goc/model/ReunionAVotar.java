package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.NoEsTelematicaExceptionConVotacion;
import es.uji.apps.goc.exceptions.NoVotacionTelematicaException;
import es.uji.apps.goc.exceptions.ReunionNoAbiertaException;
import es.uji.apps.goc.exceptions.VotacionNoAbiertaException;
import org.apache.commons.lang3.Validate;

import java.util.Date;

public class ReunionAVotar
{
    private Long reunionId;

    private boolean admiteCambioVoto;

    public ReunionAVotar(Reunion reunion)
        throws NoVotacionTelematicaException, ReunionNoAbiertaException,
        VotacionNoAbiertaException {
        Validate.notNull(reunion);

        if (!reunion.getVotacionTelematica()) throw new NoVotacionTelematicaException();
        if (!reunion.isAbierta()) throw new ReunionNoAbiertaException();
        if(reunion.getFechaFinVotacion()!= null && reunion.getFechaFinVotacion().before(new Date()))
            throw new VotacionNoAbiertaException();

        admiteCambioVoto = reunion.isAdmiteCambioVoto();

        this.reunionId = reunion.getId();
    }

    public Long getReunionId() {
        return reunionId;
    }

    public boolean isAdmiteCambioVoto() {
        return admiteCambioVoto;
    }
}
