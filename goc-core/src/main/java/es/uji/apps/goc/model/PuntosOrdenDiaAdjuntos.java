package es.uji.apps.goc.model;

import java.util.Date;

import javax.persistence.Column;

import es.uji.apps.goc.dto.Documentable;
import es.uji.apps.goc.dto.PuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.enums.PuntosOrdenDiaTiposEnum;

public class PuntosOrdenDiaAdjuntos {
    private Long id;
    private String nombre;
    private String descripcion;
    private String descripcionAlternativa;
    private PuntosOrdenDiaTiposEnum tipo;
    private Boolean mostrarEnFicha;
    private Boolean publico;
    private Long editorId;
    private Date fechaEdicion;
    private String motivoEdicion;
    private Boolean activo;
    private int orden;

    public PuntosOrdenDiaAdjuntos() {

    }

    private PuntosOrdenDiaAdjuntos(Documentable documentable) {
        this.id = documentable.getId();
        this.nombre = documentable.getNombreFichero();
        this.descripcion = documentable.getDescripcion();
        this.descripcionAlternativa = documentable.getDescripcionAlternativa();
        this.publico = documentable.getPublico();
        this.editorId = documentable.getEditorId();
        this.fechaEdicion = documentable.getFechaEdicion();
        this.motivoEdicion = documentable.getMotivoEdicion();
        this.activo = documentable.getActivo();
    }

    public PuntosOrdenDiaAdjuntos(PuntoOrdenDiaAcuerdo acuerdo) {
        this((Documentable) acuerdo);
        this.tipo = PuntosOrdenDiaTiposEnum.ACUERDO;
    }

    public PuntosOrdenDiaAdjuntos (PuntoOrdenDiaDocumento adjunto){
        this((Documentable) adjunto);
        this.tipo = PuntosOrdenDiaTiposEnum.ADJUNTO;
        this.mostrarEnFicha = adjunto.getMostrarEnFicha();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PuntosOrdenDiaTiposEnum getTipo() {
        return tipo;
    }

    public void setTipo(PuntosOrdenDiaTiposEnum tipo) {
        this.tipo = tipo;
    }

    public Boolean getMostrarEnFicha() {
        return mostrarEnFicha;
    }

    public void setMostrarEnFicha(Boolean mostrarEnFicha) {
        this.mostrarEnFicha = mostrarEnFicha;
    }

    public Boolean getPublico() {
        return publico;
    }

    public void setPublico(Boolean publico) {
        this.publico = publico;
    }

    public Long getEditorId() {
		return editorId;
	}

	public void setEditorId(Long editorId) {
		this.editorId = editorId;
	}

	public Date getFechaEdicion() {
		return fechaEdicion;
	}

	public void setFechaEdicion(Date fechaEdicion) {
		this.fechaEdicion = fechaEdicion;
	}

	public String getMotivoEdicion() {
		return motivoEdicion;
	}

	public void setMotivoEdicion(String motivoEdicion) {
		this.motivoEdicion = motivoEdicion;
	}
	
	public Boolean isActivo() {
		return activo;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean isPublico() {
        return this.publico;
    }
    public Boolean isMostrarEnFicha() {
        return this.mostrarEnFicha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcionAlternativa() {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa) {
        this.descripcionAlternativa = descripcionAlternativa;
    }

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}
}
