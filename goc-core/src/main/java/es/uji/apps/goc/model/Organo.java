package es.uji.apps.goc.model;

import java.util.Date;
import java.util.Set;

import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.commons.rest.StringUtils;

public class Organo
{
    private String id;

    private String nombre;

    private String nombreAlternativo;

    private Set<OrganoReunion> organoReuniones;

    private Set<Miembro> miembros;

    private TipoOrgano tipoOrgano;

    private Boolean externo;

    private Boolean inactivo;

    private Long creadorId;

    private Date fechaCreacion;

    private Boolean convocarSinOrdenDia;

    private String email;

    private Boolean actaProvisionalActiva;

    private Boolean delegacionVotoMultiple;

    private TipoProcedimientoVotacionEnum tipoProcedimientoVotacionEnum;

    private Boolean presidenteVotoDoble;

    private Boolean permiteAbstencionVoto;
    
    private Boolean verAsistencia;

    private Boolean verDelegaciones;   

	public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getNombreLimpio()
    {
        if (nombre == null) return null;

        return StringUtils.limpiaAcentos(nombre.trim());
    }
    
    public String getNombreAlternativoLimpio()
    {
        if (nombreAlternativo == null) return null;

        return StringUtils.limpiaAcentos(nombreAlternativo.trim());
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public TipoOrgano getTipoOrgano()
    {
        return tipoOrgano;
    }

    public void setTipoOrgano(TipoOrgano tipoOrgano)
    {
        this.tipoOrgano = tipoOrgano;
    }

    public Organo() {
        this.externo = false;
    }

    public Organo(String id, String nombre, String nombreAlternativo, TipoOrgano tipoOrgano) {
        this.id = id;
        this.nombre = nombre;
        this.nombreAlternativo = nombreAlternativo;
        this.tipoOrgano = tipoOrgano;
        this.externo = true;
    }

    public Organo(
            String id,
            String nombre,
            String nombreAlternativo,
            TipoOrgano tipoOrgano,
            TipoProcedimientoVotacionEnum tipoProcedimientoVotacionEnum
    ) {
        this(id , nombre, nombreAlternativo, tipoOrgano);
        this.tipoProcedimientoVotacionEnum = tipoProcedimientoVotacionEnum;
    }

    public Organo(String id) {
        this.id = id;
    }

    public Organo(String id, String nombre, String nombreAlternativo, TipoOrgano tipoOrgano, Boolean externo) {
        this.id = id;
        this.nombre = nombre;
        this.nombreAlternativo = nombreAlternativo;
        this.externo = externo;
        this.tipoOrgano = tipoOrgano;
    }

    public Boolean isExterno()
    {
    	return (externo == null) ? false : externo;
    }

    public void setExterno(Boolean externo)
    {
        this.externo = externo;
    }

    public Set<OrganoReunion> getOrganoReuniones()
    {
        return organoReuniones;
    }

    public void setOrganoReuniones(Set<OrganoReunion> organoReuniones)
    {
        this.organoReuniones = organoReuniones;
    }

    public Set<Miembro> getMiembros() {
        return miembros;
    }

    public void setMiembros(Set<Miembro> miembros) {
        this.miembros = miembros;
    }

    public Long getCreadorId() {
        return creadorId;
    }

    public void setCreadorId(Long creadorId) {
        this.creadorId = creadorId;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean isInactivo() {
        return inactivo;
    }

    public void setInactivo(Boolean inactivo) {
        this.inactivo = inactivo;
    }

    public String getNombreAlternativo()
    {
        return nombreAlternativo;
    }

    public void setNombreAlternativo(String nombreAlternativo)
    {
        this.nombreAlternativo = nombreAlternativo;
    }

    public Boolean getConvocarSinOrdenDia()
    {
        return convocarSinOrdenDia;
    }

    public void setConvocarSinOrdenDia(Boolean convocarSinOrdenDia)
    {
        this.convocarSinOrdenDia = convocarSinOrdenDia;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Boolean getActaProvisionalActiva()
    {
        return actaProvisionalActiva;
    }

    public void setActaProvisionalActiva(Boolean actaProvisionalActiva)
    {
        this.actaProvisionalActiva = actaProvisionalActiva;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Organo organo = (Organo) o;

        if (!id.equals(organo.id)) return false;
        return nombre.equals(organo.nombre);
    }

    @Override
    public int hashCode()
    {
        int result = id.hashCode();
        result = 31 * result + nombre.hashCode();
        return result;
    }

    public Boolean getDelegacionVotoMultiple()
    {
        return delegacionVotoMultiple;
    }

    public void setDelegacionVotoMultiple(Boolean delegacionVotoMultiple)
    {
        this.delegacionVotoMultiple = delegacionVotoMultiple;
    }

    public TipoProcedimientoVotacionEnum getTipoProcedimientoVotacionEnum() {
        return tipoProcedimientoVotacionEnum;
    }

    public String getTipoProcedimientoVotacionName() {
        return tipoProcedimientoVotacionEnum != null ? tipoProcedimientoVotacionEnum.name() : null;
    }

    public void setTipoProcedimientoVotacionEnum(TipoProcedimientoVotacionEnum tipoProcedimientoVotacionEnum) {
        this.tipoProcedimientoVotacionEnum = tipoProcedimientoVotacionEnum;
    }

    public Boolean getPresidenteVotoDoble() {
        return presidenteVotoDoble;
    }

    public void setPresidenteVotoDoble(Boolean presidenteVotoDoble) {
        this.presidenteVotoDoble = presidenteVotoDoble;
    }

    public Boolean getPermiteAbstencionVoto() {
        return permiteAbstencionVoto;
    }

    public void setPermiteAbstencionVoto(Boolean permiteAbstencionVoto) {
        this.permiteAbstencionVoto = permiteAbstencionVoto;
    }

    public Boolean isPermiteAbstencionVoto() {
        return getPermiteAbstencionVoto();
    }
    
    public Boolean getVerAsistencia() {
		return verAsistencia;
	}

	public void setVerAsistencia(Boolean verAsistencia) {
		this.verAsistencia = verAsistencia;
	}
	
	public Boolean getVerDelegaciones() {
		return verDelegaciones;
	}

	public void setVerDelegaciones(Boolean verDelegaciones) {
		this.verDelegaciones = verDelegaciones;
	}
	
	public Boolean isVerDelegaciones() {
		return verDelegaciones;
	}
}
