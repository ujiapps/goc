package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class DelegadoVotoYaAsignadoAnteriormente extends CoreDataBaseException
{
    public DelegadoVotoYaAsignadoAnteriormente()
    {
        super("Ya está asignado como delegado en esta misma reunión");
    }

    public DelegadoVotoYaAsignadoAnteriormente(String message)
    {
        super(message);
    }
}
