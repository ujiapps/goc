package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ReunionNoAbiertaException extends CoreBaseException
{

    public ReunionNoAbiertaException() {
        super("La reunión no está abierta");
    }
}
