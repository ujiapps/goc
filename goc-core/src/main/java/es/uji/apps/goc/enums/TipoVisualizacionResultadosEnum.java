package es.uji.apps.goc.enums;

public enum TipoVisualizacionResultadosEnum {
    AL_FINALIZAR_REUNION(true),
    AL_ALCANZAR_FECHA_FIN_VOTACION(false);

    private final boolean tipo;

    TipoVisualizacionResultadosEnum(boolean tipo){
        this.tipo = tipo;
    }

    public boolean getTipo() {
        return tipo;
    }
}
