package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class PuntoDelDiaConAcuerdosException extends CoreDataBaseException
{
    public PuntoDelDiaConAcuerdosException()
    {
        super("El punto del día no se puede borrar si tiene acuerdos u otros registros asociados (Votaciones, ...)");
    }
}
