package es.uji.apps.goc.enums;

import java.util.HashSet;
import java.util.Set;

public enum TipoVotoEnum
{
    FAVOR, CONTRA, BLANCO;
    
    public static boolean checkVoto(String voto) {
        Set<String> tiposVoto = new HashSet<>();
        tiposVoto.add(TipoVotoEnum.FAVOR.name());
        tiposVoto.add(TipoVotoEnum.CONTRA.name());
        tiposVoto.add(TipoVotoEnum.BLANCO.name());

        return tiposVoto.contains(voto);
    }
}
