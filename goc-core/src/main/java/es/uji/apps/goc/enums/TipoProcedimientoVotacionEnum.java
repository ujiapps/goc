package es.uji.apps.goc.enums;

public enum TipoProcedimientoVotacionEnum
{
    ABREVIADO, ORDINARIO, NO_VOTABLE
}
