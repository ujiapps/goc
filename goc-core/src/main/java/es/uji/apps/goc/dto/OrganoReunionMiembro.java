package es.uji.apps.goc.dto;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import es.uji.commons.rest.StringUtils;

@Entity
@Table(name = "GOC_ORGANOS_REUNIONES_MIEMBROS")
public class OrganoReunionMiembro implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private static final String PRESIDENTE = "PR";
    private static final String SECRETARIO = "SE";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    private String email;

    private Boolean asistencia;

    @Column(name = "ORGANO_ID")
    private String organoId;

    @Column(name = "ORGANO_EXTERNO")
    private Boolean organoExterno;

    @Column(name = "REUNION_ID")
    private Long reunionId;

    @Column(name = "MIEMBRO_ID")
    private String miembroId;

    @Column(name = "CARGO_ID")
    private String cargoId;

    @Column(name = "CARGO_CODIGO")
    private String cargoCodigo;

    @Column(name = "CARGO_NOMBRE")
    private String cargoNombre;

    @Column(name = "CARGO_NOMBRE_ALT")
    private String cargoNombreAlternativo;

    @Column(name = "SUPLENTE_ID")
    private Long suplenteId;

    @Column(name = "SUPLENTE_NOMBRE")
    private String suplenteNombre;

    @Column(name = "SUPLENTE_EMAIL")
    private String suplenteEmail;

    @Column(name = "ASISTENCIA_CONFIRMADA")
    private Boolean asistenciaConfirmada;

    @Column(name = "DELEGADO_VOTO_ID")
    private Long delegadoVotoId;

    @Column(name = "DELEGADO_VOTO_NOMBRE")
    private String delegadoVotoNombre;

    @Column(name = "DELEGADO_VOTO_EMAIL")
    private String delegadoVotoEmail;

    @Column(name = "URL_ASISTENCIA")
    private String urlAsistencia;

    @Column(name = "URL_ASISTENCIA_ALT")
    private String urlAsistenciaAlternativa;

    private String condicion;

    @Column(name = "CONDICION_ALT")
    private String condicionAlternativa;

    @Column(name = "JUSTIFICA_AUSENCIA")
    private Boolean justificaAusencia;

    @ManyToOne
    @JoinColumn(name = "ORGANO_REUNION_ID")
    private OrganoReunion organoReunion;

    @Column(name = "RESPONSABLE_ACTA")
    private Boolean responsableActa;

    @Column(name = "CARGO_SUPLENTE")
    private Boolean cargoSuplente;
    
    @Column(name = "MOTIVO_AUSENCIA", length = 500)
    private String motivoAusencia;

    private Boolean nato;

    @OneToMany(mappedBy = "organoReunionMiembro")
    private Set<PersonaPuntoVotoDTO> personasPuntoVoto;

    @OneToMany(mappedBy = "organoReunionMiembro")
    private Set<VotantePrivadoDTO> votantesPrivados;

    public OrganoReunionMiembro()
    {
    }

    public OrganoReunionMiembro(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }
    
    public String getNombreLimpio()
    {
        if (nombre == null) return null;

        return StringUtils.limpiaAcentos(nombre).trim();
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public OrganoReunion getOrganoReunion()
    {
        return organoReunion;
    }

    public void setOrganoReunion(OrganoReunion organoReunion)
    {
        this.organoReunion = organoReunion;
    }

    public Boolean isAsistencia()
    {
        return asistencia;
    }

    public void setAsistencia(Boolean asistencia)
    {
        this.asistencia = asistencia;
    }

    public Boolean getAsistencia()
    {
        return asistencia;
    }

    public String getOrganoId()
    {
        return organoId;
    }

    public void setOrganoId(String organoId)
    {
        this.organoId = organoId;
    }

    public Long getReunionId()
    {
        return reunionId;
    }

    public void setReunionId(Long reunionId)
    {
        this.reunionId = reunionId;
    }

    public Boolean getOrganoExterno()
    {
        return organoExterno;
    }

    public void setOrganoExterno(Boolean organoExterno)
    {
        this.organoExterno = organoExterno;
    }

    public String getMiembroId()
    {
        return miembroId;
    }

    public void setMiembroId(String miembroId)
    {
        this.miembroId = miembroId;
    }

    public String getCargoId()
    {
        return cargoId;
    }

    public void setCargoId(String cargoId)
    {
        this.cargoId = cargoId;
    }

    public String getCargoNombre()
    {
        return cargoNombre;
    }

    public void setCargoNombre(String cargoNombre)
    {
        this.cargoNombre = cargoNombre;
    }

    public Long getSuplenteId()
    {
        return suplenteId;
    }

    public void setSuplenteId(Long suplenteId)
    {
        this.suplenteId = suplenteId;
    }

    public String getSuplenteNombre()
    {
        return suplenteNombre;
    }

    public void setSuplenteNombre(String suplenteNombre)
    {
        this.suplenteNombre = suplenteNombre;
    }

    public Boolean isAsistenciaConfirmada()
    {
        return asistenciaConfirmada != null ? asistenciaConfirmada : true;
    }

    public Boolean getAsistenciaConfirmada()
    {
        return asistenciaConfirmada != null ? asistenciaConfirmada : true;
    }

    public void setAsistenciaConfirmada(Boolean asistenciaConfirmada)
    {
        this.asistenciaConfirmada = asistenciaConfirmada;
    }

    public Boolean isHaConfirmado()
    {
        return asistenciaConfirmada;
    }

    public String getSuplenteEmail()
    {
        return suplenteEmail;
    }

    public void setSuplenteEmail(String suplenteEmail)
    {
        this.suplenteEmail = suplenteEmail;
    }

    public String getCargoNombreAlternativo()
    {
        return cargoNombreAlternativo;
    }

    public void setCargoNombreAlternativo(String cargoNombreAlternativo)
    {
        this.cargoNombreAlternativo = cargoNombreAlternativo;
    }

    public Long getDelegadoVotoId()
    {
        return delegadoVotoId;
    }

    public void setDelegadoVotoId(Long delegadoVotoId)
    {
        this.delegadoVotoId = delegadoVotoId;
    }

    public String getDelegadoVotoNombre()
    {
        return delegadoVotoNombre;
    }

    public void setDelegadoVotoNombre(String delegadoVotoNombre)
    {
        this.delegadoVotoNombre = delegadoVotoNombre;
    }

    public String getDelegadoVotoEmail()
    {
        return delegadoVotoEmail;
    }

    public void setDelegadoVotoEmail(String delegadoVotoEmail)
    {
        this.delegadoVotoEmail = delegadoVotoEmail;
    }

    public String getCargoCodigo()
    {
        return cargoCodigo;
    }

    public void setCargoCodigo(String cargoCodigo)
    {
        this.cargoCodigo = cargoCodigo;
    }

    public String getUrlAsistencia()
    {
        return urlAsistencia;
    }

    public void setUrlAsistencia(String urlAsistencia)
    {
        this.urlAsistencia = urlAsistencia;
    }

    public String getUrlAsistenciaAlternativa()
    {
        return urlAsistenciaAlternativa;
    }

    public void setUrlAsistenciaAlternativa(String urlAsistenciaAlternativa)
    {
        this.urlAsistenciaAlternativa = urlAsistenciaAlternativa;
    }

    public String getCondicion()
    {
        return condicion;
    }

    public void setCondicion(String condicion)
    {
        this.condicion = condicion;
    }

    public String getCondicionAlternativa()
    {
        return condicionAlternativa;
    }

    public void setCondicionAlternativa(String condicionAlternativa)
    {
        this.condicionAlternativa = condicionAlternativa;
    }

    public Boolean getJustificaAusencia()
    {
        return justificaAusencia != null ? justificaAusencia : false;
    }

    public Boolean isJustificaAusencia() {
        return justificaAusencia != null ? justificaAusencia : false;
    }

    public void setJustificaAusencia(Boolean justificaAsistencia)
    {
        this.justificaAusencia = justificaAsistencia;
    }

    public Boolean getResponsableActa()
    {
        return responsableActa;
    }

    public void setResponsableActa(Boolean responsableActa)
    {
        this.responsableActa = responsableActa;
    }

    public Boolean isResponsableActa()
    {
        return this.responsableActa;
    }

    public Boolean getCargoSuplente()
    {
        return cargoSuplente;
    }

    public Boolean isCargoSuplente()
    {
        return cargoSuplente;
    }

    public void setCargoSuplente(Boolean cargoSuplente)
    {
        this.cargoSuplente = cargoSuplente;
    }
    
    public String getMotivoAusencia()
    {
        return motivoAusencia;
    }

    public void setMotivoAusencia(String motivoAusencia)
    {
        this.motivoAusencia = motivoAusencia;
    }

    public Set<PersonaPuntoVotoDTO> getPersonasPuntoVoto() {
        return personasPuntoVoto;
    }

    public void setPersonasPuntoVoto(Set<PersonaPuntoVotoDTO> personasPuntoVoto) {
        this.personasPuntoVoto = personasPuntoVoto;
    }

    public Set<VotantePrivadoDTO> getVotantesPrivados() {
        return votantesPrivados;
    }

    public void setVotantesPrivados(Set<VotantePrivadoDTO> votantesPrivados) {
        this.votantesPrivados = votantesPrivados;
    }

    public Boolean getNato()
    {
        return nato;
    }

    public void setNato(Boolean nato)
    {
        this.nato = nato;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganoReunionMiembro that = (OrganoReunionMiembro) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode()
    {
        return id.hashCode();
    }
    
    public boolean isPresidente()
    {
        return this.getCargoCodigo() != null && this.getCargoCodigo().equalsIgnoreCase(PRESIDENTE);
    }
    
    public boolean isSecretario()
    {
        return this.getCargoCodigo() != null && this.getCargoCodigo().equalsIgnoreCase(SECRETARIO);
    }
}
