package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class MiembroHaEmitidoVotosException extends CoreBaseException
{
    public MiembroHaEmitidoVotosException() {
        super("El miembro ya ha emitido votos, no se puede establecer, modificar o eliminar suplencias ni delegaciones de voto");
    }
}
