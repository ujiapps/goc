package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.VotanteRepresentado;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OrganoReunionMiembroDAO extends BaseDAODatabaseImpl {
    private QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;
    private QOrganoReunionInvitado qOrganoReunionInvitado = QOrganoReunionInvitado.organoReunionInvitado;
    private QOrganoAutorizado qOrganoAutorizado = QOrganoAutorizado.organoAutorizado;
    private QReunion qReunion = QReunion.reunion;
    private QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
    private QPersonaPuntoVotoDTO qPersonaPuntoVoto = QPersonaPuntoVotoDTO.personaPuntoVotoDTO;

    public List<OrganoReunionMiembro> getOrganoReunionMiembroByOrganoReunionId(Long organoReunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.organoReunion.id.eq(organoReunionId))
                .list(qOrganoReunionMiembro);
    }

    public OrganoReunionMiembro getMiembroById(Long miembroId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoReunionMiembro> resultado = query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.id.eq(miembroId))
                .list(qOrganoReunionMiembro);

        if (resultado.size() == 0) {
            return null;
        }

        return resultado.get(0);
    }

    public List<OrganoReunionMiembro> getMiembroReunionByOrganoAndReunionId(String organoId, Boolean externo,
                                                                            Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.organoExterno.eq(externo)
                        .and(qOrganoReunionMiembro.organoId.eq(organoId)
                                .and(qOrganoReunionMiembro.reunionId.eq(reunionId))))
                .list(qOrganoReunionMiembro);
    }

    public List<OrganoReunionMiembro> getAsistenteReunionByOrganoAndReunionId(String organoId, Boolean externo,
                                                                              Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.organoExterno.eq(externo)
                        .and(qOrganoReunionMiembro.organoId.eq(organoId)
                                .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                                .and((qOrganoReunionMiembro.asistencia.eq(true)).or(qOrganoReunionMiembro.suplenteId.isNotNull()))
                        ))
                .list(qOrganoReunionMiembro);
    }

    @Transactional
    public void updateAsistenteReunionByEmail(Long reunionId, String organoId, Boolean externo, String asistenteEmail,
                                              Boolean asistencia, Long suplenteId, String suplenteNombre, String suplenteEmail, Boolean guardarAsistencia,
                                              Boolean justificaAusencia, Long delegadoVotoId, String delegadoVotoNombre, String delegadoVotoEmail) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        if (guardarAsistencia) {
            update.set(qOrganoReunionMiembro.asistencia, asistencia)
                    .set(qOrganoReunionMiembro.asistenciaConfirmada, asistencia);
        }

        update.set(qOrganoReunionMiembro.suplenteId, suplenteId)
                .set(qOrganoReunionMiembro.suplenteNombre, suplenteNombre)
                .set(qOrganoReunionMiembro.suplenteEmail, suplenteEmail)
                .set(qOrganoReunionMiembro.justificaAusencia, justificaAusencia)
                .set(qOrganoReunionMiembro.delegadoVotoId, delegadoVotoId)
                .set(qOrganoReunionMiembro.delegadoVotoNombre, delegadoVotoNombre)
                .set(qOrganoReunionMiembro.delegadoVotoEmail, delegadoVotoEmail)
                .where(qOrganoReunionMiembro.email.eq(asistenteEmail)
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId)
                                .and(qOrganoReunionMiembro.organoId.eq(organoId)
                                        .and(qOrganoReunionMiembro.organoExterno.eq(externo)))));
        update.execute();
    }

    public List<OrganoReunionMiembro> getMiembroByAsistenteIdOrSuplenteId(Long reunionId, Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro).leftJoin(qOrganoReunionMiembro.personasPuntoVoto, qPersonaPuntoVoto).fetch()
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                        .and(qOrganoReunionMiembro.suplenteId.eq(connectedUserId)
                                .or(qOrganoReunionMiembro.miembroId.eq(connectedUserId.toString()))))
                .list(qOrganoReunionMiembro);
    }

    public List<OrganoReunionMiembro> getAsistentesByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                        .and(qOrganoReunionMiembro.asistencia.eq(true)))
                .list(qOrganoReunionMiembro);
    }

    public List<OrganoReunionMiembro> getMiembrosByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId))
                .list(qOrganoReunionMiembro);
    }

    public OrganoReunionMiembro getByReunionAndOrganoAndEmail(Long reunionId, String organoId, Boolean externo,
                                                              String email) {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoReunionMiembro> miembros = query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.email.eq(email)
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId)
                                .and(qOrganoReunionMiembro.organoId.eq(organoId)
                                        .and(qOrganoReunionMiembro.organoExterno.eq(externo)))))
                .list(qOrganoReunionMiembro);

        if (miembros.size() == 0) return null;

        return miembros.get(0);
    }

    @Transactional
    public void deleteByOrganoReunionIdPersonaIdAndCargoId(Long organoReunionId, String personaId, String cargoId) {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qOrganoReunionMiembro);

        deleteClause.where(qOrganoReunionMiembro.organoReunion.id.eq(organoReunionId)
                        .and(qOrganoReunionMiembro.miembroId.eq(personaId).and(qOrganoReunionMiembro.cargoId.eq(cargoId))))
                .execute();
    }

    public void ByOrganoReunionIdPersonaIdAndCargoId(Long organoReunionId, String personaId, String cargoId,
                                                     String nombre, String email, Cargo cargo) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.set(qOrganoReunionMiembro.nombre, nombre).
                set(qOrganoReunionMiembro.email, email).
                set(qOrganoReunionMiembro.cargoId, cargo.getId().toString()).
                set(qOrganoReunionMiembro.cargoCodigo, cargo.getCodigo()).
                set(qOrganoReunionMiembro.cargoNombre, cargo.getNombre()).
                set(qOrganoReunionMiembro.cargoNombreAlternativo, cargo.getNombreAlternativo()).
                where(qOrganoReunionMiembro.organoReunion.id.eq(organoReunionId)
                        .and(qOrganoReunionMiembro.miembroId.eq(personaId)
                                .and(qOrganoReunionMiembro.cargoId.eq(cargoId))));

        update.execute();
    }

    @Transactional
    public void eliminarDelegacionesExistentes(Long reunionId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.setNull(qOrganoReunionMiembro.delegadoVotoId).
                setNull(qOrganoReunionMiembro.delegadoVotoNombre).
                setNull(qOrganoReunionMiembro.delegadoVotoEmail).
                where(qOrganoReunionMiembro.reunionId.eq(reunionId));
        update.execute();
    }

    @Transactional
    public List<OrganoReunionMiembro> getMailPersonasVotoDelegado(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId).and(qOrganoReunionMiembro.delegadoVotoId.isNotNull())).list(qOrganoReunionMiembro);

    }

    @Transactional
    public List<String> getMailPersonaseceptorasVotoDelegado(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId).and(qOrganoReunionMiembro.delegadoVotoId.isNotNull())).list(qOrganoReunionMiembro.delegadoVotoEmail);

    }

    public List<OrganoReunionMiembro> getNombrePersonasVotoDelegado(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId).and(qOrganoReunionMiembro.delegadoVotoId.isNotNull())).list(qOrganoReunionMiembro);
    }

    public boolean haSidoAsignadoDelegadoEnReunion(Long delegadoVotoId, Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro).where(qOrganoReunionMiembro.delegadoVotoId.eq(delegadoVotoId).and(qOrganoReunionMiembro.reunionId.eq(reunionId))).exists();
    }

    public OrganoReunionMiembro getOrganoReunionMiembroByPersonaAndReunionId(Persona persona, Long reunionId) {
        return new JPAQuery(entityManager).from(qReunion)
                .join(qOrganoReunionMiembro).on(qOrganoReunionMiembro.reunionId.eq(qReunion.id))
                .leftJoin(qMiembroLocal).on(qMiembroLocal.personaId.stringValue().eq(qOrganoReunionMiembro.miembroId).or(qMiembroLocal.id.eq(qOrganoReunionMiembro.suplenteId)))
                .where(((qOrganoReunionMiembro.email.eq(persona.getEmail()).or(qOrganoReunionMiembro.suplenteEmail.eq(persona.getEmail())))
                        .or(qMiembroLocal.isNotNull().and(qMiembroLocal.personaId.eq(persona.getId()))))
                        .and(qReunion.id.eq(reunionId))
                ).singleResult(qOrganoReunionMiembro);
    }

    public List<VotanteRepresentado> getVotantesRepresentadosByReunionIdAndPersonaId(Long reunionId, Long personaId) {
        List<VotanteRepresentado> votanteRepresentados = new ArrayList<>();
        JPAQuery query = new JPAQuery(entityManager);

        List<Tuple> list =
                query.from(qOrganoReunionMiembro)
                        .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                                .and(qOrganoReunionMiembro.delegadoVotoId.eq(personaId)))
                        .list(qOrganoReunionMiembro.id.castToNum(Long.class), qOrganoReunionMiembro.nombre);

        for (Tuple tuple : list) {
            votanteRepresentados.add(new VotanteRepresentado(tuple.get(0, Long.class), tuple.get(1, String.class)));
        }

        return votanteRepresentados;
    }

    @Transactional
    public void deleteOrganoReunionMiembro(OrganoReunionMiembro organoReunionMiembro) {
        entityManager.remove(entityManager.contains(organoReunionMiembro) ? organoReunionMiembro : entityManager.merge(organoReunionMiembro));
    }

    public Long vecesSuplenteEnReunion(Long suplenteId, Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro).where(qOrganoReunionMiembro.suplenteId.eq(suplenteId).and(qOrganoReunionMiembro.reunionId.eq(reunionId))).count();
    }

    public Boolean soyDelegadoEnReunion(Long reunionId, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return (query).from(qOrganoReunionMiembro).where(qOrganoReunionMiembro.delegadoVotoId.eq(personaId).
                        and(qOrganoReunionMiembro.reunionId.eq(reunionId))).
                exists();
    }

    public List<OrganoReunionMiembro> getMiembrosDeLosQueSoyDelegado(Long reunionId, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro).where(qOrganoReunionMiembro.delegadoVotoId.eq(personaId).
                        and(qOrganoReunionMiembro.reunionId.eq(reunionId))).
                list(qOrganoReunionMiembro);
    }
}
