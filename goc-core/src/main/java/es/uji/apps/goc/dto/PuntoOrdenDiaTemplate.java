package es.uji.apps.goc.dto;

import java.util.List;

import es.uji.apps.goc.model.ComentarioPuntoOrdenDia;

public class PuntoOrdenDiaTemplate
{
    private Long id;

    private String titulo;

    private String descripcion;

    private Long orden;

    private String acuerdos;

    private String deliberaciones;

    private boolean publico;

    private List<DocumentoTemplate> documentos;

    private List<DocumentoTemplate> documentosAcuerdos;

    private List<DescriptorTemplate> descriptores;

    private String urlActa;

    private String urlActaAnterior;

    private List<PuntoOrdenDiaTemplate> subpuntos;

    private List<ComentarioPuntoOrdenDia> comentarios;

    private String voto;

    private Boolean abrible;

    private Boolean tipoVoto;

    private Long votanteId;

    private String tipoRecuentoVoto;

    private String tipoProcedimientoVotacion;

    private Boolean votacionAbierta;

    private Boolean votableEnContra;

    private Boolean permiteAbstencionVoto;
    
    private String numeracionMultinivel;
    
    private Long recuentoVotos;
    
    private List<PersonaPuntoVotoDTO> votosPunto;
    
    private PuntoSecretoVotosDTO votosPuntoSecreto;
    
    private String tipoVotacion;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getAcuerdos()
    {
        return acuerdos;
    }

    public void setAcuerdos(String acuerdos)
    {
        this.acuerdos = acuerdos;
    }

    public String getDeliberaciones()
    {
        return deliberaciones;
    }

    public void setDeliberaciones(String deliberaciones)
    {
        this.deliberaciones = deliberaciones;
    }

    public List<DocumentoTemplate> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoTemplate> documentos) {
        this.documentos = documentos;
    }

    public boolean isPublico() {
        return publico;
    }

    public void setPublico(boolean publico) {
        this.publico = publico;
    }

    public List<DocumentoTemplate> getDocumentosAcuerdos()
    {
        return documentosAcuerdos;
    }

    public void setDocumentosAcuerdos(List<DocumentoTemplate> documentosAcuerdos)
    {
        this.documentosAcuerdos = documentosAcuerdos;
    }

    public List<DescriptorTemplate> getDescriptores()
    {
        return descriptores;
    }

    public void setDescriptores(List<DescriptorTemplate> descriptores)
    {
        this.descriptores = descriptores;
    }

    public String getUrlActa()
    {
        return urlActa;
    }

    public void setUrlActa(String urlActa)
    {
        this.urlActa = urlActa;
    }

    public List<PuntoOrdenDiaTemplate> getSubpuntos()
    {
        return subpuntos;
    }

    public void setSubpuntos(List<PuntoOrdenDiaTemplate> subpuntos)
    {
        this.subpuntos = subpuntos;
    }

    public List<ComentarioPuntoOrdenDia> getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(List<ComentarioPuntoOrdenDia> comentarios)
    {
        this.comentarios = comentarios;
    }

    public String getUrlActaAnterior()
    {
        return urlActaAnterior;
    }

    public void setUrlActaAnterior(String urlActaAnterior)
    {
        this.urlActaAnterior = urlActaAnterior;
    }

    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) {
        this.voto = voto;
    }

    public Boolean getTipoVoto() {
        return tipoVoto;
    }

    public void setTipoVoto(Boolean tipoVoto) {
        this.tipoVoto = tipoVoto;
    }

    public Long getVotanteId() {
        return votanteId;
    }

    public void setVotanteId(Long votanteId) {
        this.votanteId = votanteId;
    }

    public String getTipoRecuentoVoto() {
        return tipoRecuentoVoto;
    }

    public void setTipoRecuentoVoto(String tipoRecuentoVoto) {
        this.tipoRecuentoVoto = tipoRecuentoVoto;
    }

    public String getTipoProcedimientoVotacion() {
        return tipoProcedimientoVotacion;
    }

    public void setTipoProcedimientoVotacion(String tipoProcedimientoVotacion) {
        this.tipoProcedimientoVotacion = tipoProcedimientoVotacion;
    }

    public Boolean isVotacionAbierta() {
        return votacionAbierta;
    }

    public void setVotacionAbierta(Boolean votacionAbierta) {
        this.votacionAbierta = votacionAbierta;
    }

    public Boolean getAbrible() {
        return abrible;
    }

    public void setAbrible(Boolean abrible) {
        this.abrible = abrible;
    }

    public Boolean getVotableEnContra() {
        return votableEnContra;
    }

    public void setVotableEnContra(Boolean votableEnContra) {
        this.votableEnContra = votableEnContra;
    }

    public Boolean isPermiteAbstencionVoto()
    {
        return permiteAbstencionVoto;
    }

    public Boolean getPermiteAbstencionVoto()
    {
        return permiteAbstencionVoto;
    }

    public void setPermiteAbstencionVoto(Boolean permiteAbstencionVoto)
    {
        this.permiteAbstencionVoto = permiteAbstencionVoto;
    }

	public String getNumeracionMultinivel() {
		return numeracionMultinivel;
	}

	public void setNumeracionMultinivel(String numeracionMultinivel) {
		this.numeracionMultinivel = numeracionMultinivel;
	}

	public Long getRecuentoVotos() {
		return recuentoVotos;
	}

	public void setRecuentoVotos(Long recuentoVotos) {
		this.recuentoVotos = recuentoVotos;
	}

	public List<PersonaPuntoVotoDTO> getVotosPunto() {
		return votosPunto;
	}

	public void setVotosPunto(List<PersonaPuntoVotoDTO> votosPunto) {
		this.votosPunto = votosPunto;
	}

	public PuntoSecretoVotosDTO getVotosPuntoSecreto() {
		return votosPuntoSecreto;
	}

	public void setVotosPuntoSecreto(PuntoSecretoVotosDTO votosPuntoSecreto) {
		this.votosPuntoSecreto = votosPuntoSecreto;
	}

	public String getTipoVotacion() {
		return tipoVotacion;
	}

	public void setTipoVotacion(String tipoVotacion) {
		this.tipoVotacion = tipoVotacion;
	}
}
