package es.uji.apps.goc.dto;

import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.List;

public class PuntoOrdenDiaRecuentoVotosDTO
{
    private PuntoOrdenDia puntoOrdenDia;

    private Long votosFavor;
    private Long votosContra;
    private Long votosBlanco;
    private Long recuentoVotos;

    private boolean aprobado;

    private List<VotanteVoto> votantes;
    private List<PuntoOrdenDiaRecuentoVotosDTO> subpuntos;

    public PuntoOrdenDiaRecuentoVotosDTO(
            PuntoOrdenDia puntoOrdenDia,
            Long votosFavor,
            Long votosContra,
            Long votosBlanco,
            Long recuentoVotos,
            boolean aprobado,
            List<VotanteVoto> votantes
    ) {
        Validate.notNull(puntoOrdenDia);
        Validate.notNull(votosFavor);
        Validate.notNull(votosContra);
        Validate.notNull(votosBlanco);
        Validate.notNull(recuentoVotos);
        Validate.isTrue(votosFavor >= 0);
        Validate.isTrue(votosContra >= 0);
        Validate.isTrue(votosBlanco >= 0);
        Validate.isTrue(recuentoVotos >= 0);
        Validate.notNull(votantes);

        this.puntoOrdenDia = puntoOrdenDia;
        this.votosFavor = votosFavor;
        this.votosContra = votosContra;
        this.votosBlanco = votosBlanco;
        this.recuentoVotos = recuentoVotos;
        this.aprobado = aprobado;
        this.votantes = votantes;
        this.subpuntos = new ArrayList<>();
    }

    public PuntoOrdenDia getPuntoOrdenDia() {
        return puntoOrdenDia;
    }

    public Long getVotosFavor() {
        return votosFavor;
    }

    public Long getVotosContra() {
        return votosContra;
    }

    public Long getVotosBlanco() {
        return votosBlanco;
    }

    public Long getRecuentoVotos() {
        return recuentoVotos;
    }

    public boolean isAprobado() {
        return aprobado;
    }

    public List<VotanteVoto> getVotantesNombre() {
        return votantes;
    }

    public void setVotantesNombre(List<VotanteVoto> votantesExtra){
        votantes.addAll(votantesExtra);
    }

    public void addSubpunto(PuntoOrdenDiaRecuentoVotosDTO subpunto) {
        Validate.notNull(subpunto);
        subpuntos.add(subpunto);
    }

    public void addVotosBlanco(Long votosExtra) {
        votosBlanco += votosExtra;
        recuentoVotos += votosExtra;
    }

    public void setSubpuntos(List<PuntoOrdenDiaRecuentoVotosDTO> subpuntos) {
        this.subpuntos = subpuntos;
    }

    public List<PuntoOrdenDiaRecuentoVotosDTO> getSubpuntos() {
        return subpuntos;
    }
}
