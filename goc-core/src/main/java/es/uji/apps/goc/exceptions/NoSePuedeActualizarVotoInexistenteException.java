package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoSePuedeActualizarVotoInexistenteException extends CoreBaseException
{
    public NoSePuedeActualizarVotoInexistenteException() {
        super("El punto que se está intentando actualizar no existe");
    }
}
