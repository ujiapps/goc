package es.uji.apps.goc.dto;

import org.hibernate.annotations.Type;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_VW_VOTOS")
public class VotoDTO
{
    @Id
    @Type(type = "uuid-char")
    private UUID id;

    @Column(name = "PUNTO_ORDEN_DIA_ID")
    private Long puntoOrdenDiaId;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "ORGANO_REUNION_MIEMBRO_ID")
    private Long organoReunionMiembroId;

    @Column(name = "VOTO_PUBLICO")
    private Boolean votoPublico;

    @Column(name = "VOTO")
    private String voto;

    @Column(name="VOTO_DOBLE")
    private Boolean isVotoDoble;

    public VotoDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getPuntoOrdenDiaId() {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(Long puntoOrdenDiaId) {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getOrganoReunionMiembroId() {
        return organoReunionMiembroId;
    }

    public void setOrganoReunionMiembroId(Long organoReunionMiembroId) {
        this.organoReunionMiembroId = organoReunionMiembroId;
    }

    public Boolean getVotoPublico() {
        return votoPublico;
    }

    public void setVotoPublico(Boolean votoPublico) {
        this.votoPublico = votoPublico;
    }

    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) {
        this.voto = voto;
    }

    public Boolean isVotoDoble() {
        return isVotoDoble;
    }

    public void setVotoDoble(Boolean votoDoble) {
        isVotoDoble = votoDoble;
    }
}
