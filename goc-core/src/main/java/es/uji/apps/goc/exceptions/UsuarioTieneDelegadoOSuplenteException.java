package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class UsuarioTieneDelegadoOSuplenteException extends CoreDataBaseException {
    public UsuarioTieneDelegadoOSuplenteException() {
        super("La persona seleccionada ha delegado el voto o ha elegido un suplente");
    }
}
