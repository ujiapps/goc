package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TipoVotoNoExisteException extends CoreBaseException
{
    public TipoVotoNoExisteException() {
        super("No existe ese tipo de voto");
    }
}
