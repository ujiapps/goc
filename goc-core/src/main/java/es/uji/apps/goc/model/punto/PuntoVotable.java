package es.uji.apps.goc.model.punto;

import es.uji.apps.goc.exceptions.PuntoNoVotableEnContraException;
import org.apache.commons.lang3.Validate;

import java.util.Date;

import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.enums.TipoVotacionEnum;
import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.PuntoNoVotableException;
import es.uji.apps.goc.exceptions.TipoVotoNoExisteException;
import es.uji.apps.goc.model.ReunionAVotar;

public class PuntoVotable
{
    protected PuntoOrdenDia puntoOrdenDia;

    protected ReunionAVotar reunionAVotar;

    protected TipoVotoEnum voto;

    public PuntoVotable(PuntoOrdenDia puntoOrdenDia, ReunionAVotar reunionAVotar, String voto)
            throws TipoVotoNoExisteException, PuntoNoVotableException, PuntoNoVotableEnContraException {
        Validate.notNull(puntoOrdenDia);

        if (puntoOrdenDia.getTipoVoto() == null) {
            throw new PuntoNoVotableException();
        }

        Validate.notNull(reunionAVotar);
        Validate.notNull(voto);

        if (!TipoVotoEnum.checkVoto(voto))
            throw new TipoVotoNoExisteException();

        if(TipoVotoEnum.CONTRA.name().equals(voto) && puntoOrdenDia.getVotableEnContra()!= null &&
                !puntoOrdenDia.getVotableEnContra()){
            throw new PuntoNoVotableEnContraException();
        }

        this.voto = TipoVotoEnum.valueOf(voto);
        this.puntoOrdenDia = puntoOrdenDia;
        checkVotacionAbierta();
        this.reunionAVotar = reunionAVotar;
    }

    public PuntoOrdenDia getPuntoOrdenDia() {
        return puntoOrdenDia;
    }

    public ReunionAVotar getReunionAVotar() {
        return reunionAVotar;
    }

    public String getVoto() {
        return voto.name();
    }

    public void setVoto(TipoVotoEnum voto) {
        Validate.notNull(voto);
        this.voto = voto;
    }

    private void checkVotacionAbiertaAbreviado() {
        Date now = new Date();

        Validate.notNull(puntoOrdenDia.getFechaInicioVotacion());
        Validate.isTrue(puntoOrdenDia.getFechaInicioVotacion().before(now) || puntoOrdenDia.getFechaInicioVotacion().equals(now));
        Validate.isTrue(
                puntoOrdenDia.getFechaFinVotacion() == null ||
                        puntoOrdenDia.getFechaFinVotacion().after(now)
        );

        Validate.isTrue(puntoOrdenDia.getVotacionAbierta() != null && puntoOrdenDia.getVotacionAbierta());
    }

    private void checkVotacionAbiertaOrdinario() {
        Validate.isTrue(puntoOrdenDia.getVotacionAbierta());
    }

    public void checkVotacionAbierta() {
    	if(!puntoOrdenDia.getTipoVotacion().equals(TipoVotacionEnum.PRESENCIAL.toString())) {
	        if (puntoOrdenDia.getTipoProcedimientoVotacion().equals(TipoProcedimientoVotacionEnum.ABREVIADO.toString())) {
	            checkVotacionAbiertaAbreviado();
	        } else if (puntoOrdenDia.getTipoProcedimientoVotacion().equals(TipoProcedimientoVotacionEnum.ORDINARIO.toString())) {
	            checkVotacionAbiertaOrdinario();
	        }
    	}
    }
}
