package es.uji.apps.goc.dto;

import com.google.common.base.Strings;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;

@Entity
@Table(name = "GOC_ORGANOS_PARAMETROS")
public class OrganoParametro
{

    @EmbeddedId
    OrganoParametroPK organoParametroPK;

    @Column(name = "CONVOCAR_SIN_ORDEN_DIA")
    private Boolean convocarSinOrdenDia;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ACTA_PROVISIONAL_ACTIVA")
    private Boolean actaProvisionalActiva;

    @Column(name = "DELEGACION_VOTO_MULTIPLE")
    private Boolean delegacionVotoMultiple;

    @Column(name = "TIPO_PROCEDIMIENTO_VOTACION")
    private String tipoProcedimientoVotacion = TipoProcedimientoVotacionEnum.ORDINARIO.name();

    @Column(name = "PRESIDENTE_VOTO_DOBLE")
    private Boolean presidenteVotoDoble;

    @Column(name = "PERMITE_ABSTENCION_VOTO")
    private Boolean permiteAbstencionVoto;
    
    @Column(name = "VER_DELEGACIONES")
    private Boolean verDelegaciones;

    @Column(name = "VER_ASISTENCIA")
    private Boolean verAsistencia;
    
   

	public OrganoParametroPK getOrganoParametroPK()
    {
        return organoParametroPK;
    }

    public void setOrganoParametroPK(OrganoParametroPK organoParametroPK)
    {
        this.organoParametroPK = organoParametroPK;
    }

    public Boolean getConvocarSinOrdenDia()
    {
        return convocarSinOrdenDia;
    }

    public void setConvocarSinOrdenDia(Boolean convocarSinOrdenDia)
    {
        this.convocarSinOrdenDia = convocarSinOrdenDia;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Boolean getActaProvisionalActiva()
    {
        return actaProvisionalActiva;
    }

    public void setActaProvisionalActiva(Boolean actaProvisionalActiva)
    {
        this.actaProvisionalActiva = actaProvisionalActiva;
    }

    public Boolean getDelegacionVotoMultiple()
    {
        return delegacionVotoMultiple;
    }

    public void setDelegacionVotoMultiple(Boolean delegacionVotoMultiple)
    {
        this.delegacionVotoMultiple = delegacionVotoMultiple;
    }

    public Boolean isDelegacionVotoMultiple()
    {
        return delegacionVotoMultiple;
    }

    public String getTipoProcedimientoVotacion() {
        return tipoProcedimientoVotacion;
    }

    public void setTipoProcedimientoVotacion(String tipoProcedimientoVotacion) {
        if (!Strings.isNullOrEmpty(tipoProcedimientoVotacion)) {
            this.tipoProcedimientoVotacion = tipoProcedimientoVotacion;
        }
    }

    public Boolean getPresidenteVotoDoble() {
        return presidenteVotoDoble != null && presidenteVotoDoble;
    }

    public void setPresidenteVotoDoble(Boolean presidenteVotoDoble) {
        this.presidenteVotoDoble = presidenteVotoDoble != null && presidenteVotoDoble;
    }

    public Boolean getPermiteAbstencionVoto() {
        return permiteAbstencionVoto != null && permiteAbstencionVoto;
    }

    public void setPermiteAbstencionVoto(Boolean permiteAbstencionVoto) {
        this.permiteAbstencionVoto = permiteAbstencionVoto != null && permiteAbstencionVoto;
    }
    
    public Boolean getVerAsistencia() {
		return verAsistencia;
	}

	public void setVerAsistencia(Boolean verAsistencia) {
		this.verAsistencia = verAsistencia;
	}

    public Boolean getVerDelegaciones() {
    	return verDelegaciones;
    }
    
    public void setVerDelegaciones(Boolean verDelegaciones) {
    	this.verDelegaciones = verDelegaciones;
    }
    
    public Boolean isVerDelegaciones(Boolean verDelegaciones) {
    	return verDelegaciones;
    }
}
