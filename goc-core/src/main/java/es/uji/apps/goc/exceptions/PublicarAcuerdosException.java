package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class PublicarAcuerdosException extends CoreBaseException
{
    public PublicarAcuerdosException() {
        super("No s'han pogut publicar els acords");
    }
}
