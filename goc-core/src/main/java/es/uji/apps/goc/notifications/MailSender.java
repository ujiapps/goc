package es.uji.apps.goc.notifications;

import com.sun.istack.ByteArrayDataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@Component
public class MailSender
{
    private static Logger log = Logger.getLogger(MailSender.class);

    private String smtpHost;
    private Boolean smtpStartTLS;
    private Boolean smtpAuthRequired;
    private Integer smtpPort;
    private String smtpUsername;
    private String smtpPassword;
    private String defaultSender;
    private boolean smtpEnabled;

    @Autowired
    public MailSender(
            @Value("${uji.smtp.host}") String smtpHost,
            @Value("${uji.smtp.starttls.enable}") String smtpStartTLS,
            @Value("${uji.smtp.auth}") String smtpAuthRequired,
            @Value("${uji.smtp.port}") String smtpPort,
            @Value("${uji.smtp.username}") String smtpUsername,
            @Value("${uji.smtp.password}") String smtpPassword,
            @Value("${uji.smtp.defaultSender}") String defaultSender,
            @Value("${goc.smtp.enabled:true}") boolean smtpEnabled)
    {
        this.smtpHost = smtpHost;
        this.smtpStartTLS = Boolean.parseBoolean(smtpStartTLS);
        this.smtpAuthRequired = Boolean.parseBoolean(smtpAuthRequired);
        this.smtpPort = Integer.parseInt(smtpPort);
        this.smtpUsername = smtpUsername;
        this.smtpPassword = smtpPassword;
        this.defaultSender = defaultSender;
        this.smtpEnabled = smtpEnabled;
    }

    public void send(Mensaje message) throws CanNotSendException
    {
        if (message.getDestinos().size() == 0) {
            return;
        }

        try
        {
            Session session = getMailSession();

            Message mimeMessage = new MimeMessage(session);
            defineMessageHeaders(mimeMessage);
            setSubject(message, mimeMessage);
            setRecipients(message, mimeMessage);
            setMessageContent(message, mimeMessage);
            if(smtpEnabled)
                Transport.send(mimeMessage);
            else{
                String tmpdir = System.getProperty("java.io.tmpdir");
                SimpleDateFormat sdf = new SimpleDateFormat("_ddMMyy_hhmmss_SSS");
                String fecha = sdf.format(new Date());
                mimeMessage.writeTo(new FileOutputStream(Paths.get(tmpdir, "email" + fecha + ".eml").toString()));
            }
        }
        catch (Exception e)
        {
            throw new CanNotSendException(e);
        }
    }

    private Authenticator getAuthenticator()
    {
        if (smtpUsername == null || smtpPassword == null)
        {
            return null;
        }

        return new Authenticator()
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(smtpUsername, smtpPassword);
            }
        };
    }

    private Properties getEmailProperties()
    {
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.starttls.enable", smtpStartTLS);
        props.put("mail.smtp.auth", smtpAuthRequired);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.ssl.trust", "email-smtp.eu-west-1.amazonaws.com");

        return props;
    }

    private Session getMailSession()
    {
        return Session.getInstance(getEmailProperties(), getAuthenticator());
    }

    private void defineMessageHeaders(Message message) throws MessagingException
    {
        message.addHeader("Auto-Submitted", "auto-generated");
        message.addHeader("Content-Type", "text/plain; charset=UTF-8");
    }

    private void setSubject(Mensaje mensaje, Message message) throws MessagingException
    {
        message.setSubject(mensaje.getAsunto());
    }

    private void setRecipients(Mensaje mensaje, Message message) throws MessagingException
    {
    	String sender = mensaje.getFrom() != null ? mensaje.getFrom() : defaultSender;
        message.setFrom(new InternetAddress(sender));

        if (mensaje.getReplyTo() != null && !mensaje.getReplyTo().isEmpty())
        {
            message.setReplyTo(new Address[]{getAddress(mensaje.getReplyTo())});
        }

        for (String destino : mensaje.getDestinos())
        {
            Message.RecipientType recipientType = Message.RecipientType.TO;
            message.addRecipient(recipientType, getAddress(destino));
        }
    }

    private Address getAddress(String address) throws AddressException
    {
        if (address == null || address.equals(""))
        {
            return new InternetAddress();
        }

        return new InternetAddress(address.trim());
    }

    private void setMessageContent(Mensaje mensaje, Message message) throws MessagingException
    {
        MimeMultipart multipart = new MimeMultipart();

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(mensaje.getCuerpo(), mensaje.getContentType());
        multipart.addBodyPart(messageBodyPart);

        if(mensaje.getAdjuntos() != null)
        {
            for (ArchivoAdjunto archivoAdjunto : mensaje.getAdjuntos())
            {

                messageBodyPart = new MimeBodyPart();
                byte[] contentDecoded = Base64.getDecoder().decode(archivoAdjunto.getContent());
                ByteArrayDataSource byteArrayDataSource =
                    new ByteArrayDataSource(contentDecoded, archivoAdjunto.getcontentType());
                messageBodyPart.setDataHandler(new DataHandler(byteArrayDataSource));
                messageBodyPart.setFileName(archivoAdjunto.getFilename());
                multipart.addBodyPart(messageBodyPart);
            }
        }

        message.setContent(multipart);
    }
}