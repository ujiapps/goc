package es.uji.apps.goc.dto;

import es.uji.apps.goc.enums.TipoVisualizacionResultadosEnum;
import org.apache.commons.lang3.Validate;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class PuntoOrdenDiaRecuentoVotosTemplate
{
    private Long id;
    private String titulo;
    private String tituloAlternativo;

    private String descripcion;
    private String descripcionAlternativa;

    private Boolean tipoVoto;
    private String tipoVotacion;

    private Long votosFavor;
    private Long votosContra;
    private Long votosBlanco;
    private Long recuentoVotos;
    private Boolean votableEnContra;

    private String tipoRecuentoVoto;

    private boolean aprobado;

    private List<VotanteVoto> votantesNombre;

    private List<PuntoOrdenDiaRecuentoVotosTemplate> subpuntos;

    private Boolean mostrarResultados;

    public PuntoOrdenDiaRecuentoVotosTemplate(PuntoOrdenDiaRecuentoVotosDTO puntoOrdenDiaRecuentoVotosDTO) {
        Validate.notNull(puntoOrdenDiaRecuentoVotosDTO);

        this.id = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getId();
        this.titulo = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getTitulo();
        this.tituloAlternativo = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getTituloAlternativo();
        this.descripcion = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getDescripcion();
        this.descripcionAlternativa = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getDescripcionAlternativa();
        this.votosFavor = puntoOrdenDiaRecuentoVotosDTO.getVotosFavor();
        this.votosContra = puntoOrdenDiaRecuentoVotosDTO.getVotosContra();
        this.votosBlanco = puntoOrdenDiaRecuentoVotosDTO.getVotosBlanco();
        this.recuentoVotos = puntoOrdenDiaRecuentoVotosDTO.getRecuentoVotos();
        this.tipoVoto = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getTipoVoto();
        this.tipoVotacion = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getTipoVotacion();
        this.tipoRecuentoVoto = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getTipoRecuentoVoto();
        this.aprobado = puntoOrdenDiaRecuentoVotosDTO.isAprobado();
        this.votantesNombre = puntoOrdenDiaRecuentoVotosDTO.getVotantesNombre();
        this.votableEnContra = puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getVotableEnContra();
        this.mostrarResultados = calculaSiSePuedenMostrarLosResultados(puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia());

        initializeSubpuntos(puntoOrdenDiaRecuentoVotosDTO.getSubpuntos());
    }

    private Boolean calculaSiSePuedenMostrarLosResultados(PuntoOrdenDia puntoOrdenDia)
    {
        Reunion reunion = puntoOrdenDia.getReunion();

        if (puntoOrdenDia.getTipoVoto() == null || !reunion.getVotacionTelematica()) return false;

        if (puntoOrdenDia.getVotacionCerradaManualmente()) return true;

        return reunion.getTipoVisualizacionResultados() == TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION ? reunion.isCompletada() : reunion.getFechaFinVotacion()!= null && reunion.getFechaFinVotacion()
                .before(new Date());
    }

    public Long getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getTituloAlternativo() {
        return tituloAlternativo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getDescripcionAlternativa() {
        return descripcionAlternativa;
    }

    public Long getVotosFavor() {
        return votosFavor;
    }

    public Long getVotosContra() {
        return votosContra;
    }

    public Long getVotosBlanco() {
        return votosBlanco;
    }

    public Boolean getTipoVoto() {
        return tipoVoto;
    }

	public String getTipoVotacion() {
		return tipoVotacion;
	}

	public Long getRecuentoVotos() {
        return recuentoVotos;
    }

    public String getTipoRecuentoVoto() {
        return tipoRecuentoVoto;
    }

    public boolean isAprobado() {
        return aprobado;
    }

    public List<VotanteVoto> getVotantesNombre() {
        return votantesNombre;
    }

    public List<PuntoOrdenDiaRecuentoVotosTemplate> getSubpuntos() {
        return subpuntos;
    }

    private void initializeSubpuntos(List<PuntoOrdenDiaRecuentoVotosDTO> puntoOrdenDiaRecuentoVotosDTOS) {
        this.subpuntos =
                puntoOrdenDiaRecuentoVotosDTOS
                        .stream()
                        .map(PuntoOrdenDiaRecuentoVotosTemplate::new)
                        .collect(Collectors.toList());
    }

    public Boolean isVotableEnContra() {
        return votableEnContra;
    }

    public void setVotableEnContra(boolean votableEnContra) {
        this.votableEnContra = votableEnContra;
    }

    public Boolean getMostrarResultados()
    {
        return mostrarResultados;
    }

    public void setMostrarResultados(Boolean mostrarResultados)
    {
        this.mostrarResultados = mostrarResultados;
    }

    public void isMostrarResultados(Boolean mostrarResultados)
    {
        this.mostrarResultados = mostrarResultados;
    }
}
