package es.uji.apps.goc.dao;

import org.springframework.stereotype.Repository;

import es.uji.apps.goc.dto.QModificacionDocumento;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ModificacionDocumentoDAO extends BaseDAODatabaseImpl
{
    private QModificacionDocumento qModificacionDocumento = QModificacionDocumento.modificacionDocumento;
}