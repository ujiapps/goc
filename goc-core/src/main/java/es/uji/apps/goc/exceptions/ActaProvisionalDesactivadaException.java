package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ActaProvisionalDesactivadaException extends CoreBaseException
{
    public ActaProvisionalDesactivadaException()
    {
        super("El acta provisional está deshabilitada");
    }
}
