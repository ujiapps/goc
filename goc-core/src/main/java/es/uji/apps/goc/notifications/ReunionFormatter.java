package es.uji.apps.goc.notifications;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import es.uji.apps.goc.charset.ResourceBundleUTF8;
import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.Reunion;
import org.springframework.beans.factory.annotation.Value;

public class ReunionFormatter
{
    private final SimpleDateFormat formatter;
    private Reunion reunion;
    private List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados;

    public ReunionFormatter(Reunion reunion, List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados)
    {
        this.formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        this.reunion = reunion;
        this.puntosOrdenDiaOrdenados = puntosOrdenDiaOrdenados;
    }


    public String format(String publicUrl, String textoAux, String textoConvocatoria,String textoConvocatoriaAlternativo, boolean ordenDia, boolean url, String mainLanguage, Boolean isBorrador,String textoInformar,String textoInformarAlternativo)

    {

        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        StringBuffer content = new StringBuffer();

        content.append("<h2>" + reunion.getAsunto() + "</h2>");

        if (textoConvocatoria != null)
        {
            content.append("<div><pre>" + textoConvocatoria.replaceAll("\n", "<br>") + "</pre></div><br/>");
        }
        
        if(textoConvocatoriaAlternativo!=null) {
            content.append("<div><pre>" + textoConvocatoriaAlternativo.replaceAll("\n", "<br>") + "</pre></div><br/>");
        }
        
        if (textoAux != null)
        {
            content.append("<div>" + textoAux + "</div><br/>");
        }
        
        if(textoInformar != null) {
        	content.append("<div><pre>" + textoInformar.replaceAll("\n", "<br>") + "</pre></div><br/>");
        }

        if (reunion.getNumeroSesion() != null)
        {
            content.append(
                "<div><strong>" + resourceBundle.getString("mail.reunion.numeroSesion") + ": " + " </strong><span>" + reunion.getNumeroSesion() + "</span></div>");
        }

        if (reunion.getDescripcion() != null && !reunion.getDescripcion().isEmpty())
        {
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.descripcion") + ": " + "</strong><span>" + reunion.getDescripcion().replaceAll("\n", "<br>") + "</span></div>");
        }

        if (reunion.getFechaSegundaConvocatoria() == null)
        {
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.fechaYHora") + ": " + "</strong>" + formatter.format(reunion.getFecha()) + "</div>");
        }
        else
        {
            content.append(
                "<div><strong>"+ resourceBundle.getString("mail.reunion.primeraConvocatoria") + ": " +" </strong>" + formatter.format(reunion.getFecha()) + "</div>");
            content.append("<div><strong>"+ resourceBundle.getString("mail.reunion.segundaConvocatoria") + ":" + " </strong>" + formatter.format(
                reunion.getFechaSegundaConvocatoria()) + "</div>");
        }

        if (reunion.getUbicacion() != null && !reunion.getUbicacion().isEmpty())
        {
            content.append("<div><strong>"+ resourceBundle.getString("mail.reunion.ubicacion") + ": " + "</strong>" + reunion.getUbicacion() + "</div>");
        }

        if (reunion.getDuracion() != null && reunion.getDuracion() > 0)
        {
            content.append("<div><strong>"+ resourceBundle.getString("mail.reunion.duracion") + ": " + " </strong>" + reunion.getDuracion() + " "+ resourceBundle.getString("mail.reunion.minutos")+"</div>");
        }

        if (puntosOrdenDiaOrdenados != null && !puntosOrdenDiaOrdenados.isEmpty() && ordenDia)
        {
            content.append("<h4>"+ resourceBundle.getString("mail.reunion.ordenDia") +"</h4>");

            if(puntosOrdenDiaOrdenados.size()>1) {
                content.append(createListaPuntosOrdenDia(new TreeSet(puntosOrdenDiaOrdenados),true, null));
            } else {
                PuntoOrdenDiaMultinivel puntoUnico = puntosOrdenDiaOrdenados.get(0);
                content.append(createPuntoUnico(puntoUnico));
            }
        }

        if(url)
        {
            content.append("<div>"+ resourceBundle.getString("mail.reunion.masInfo") +  "<a href=\"" + publicUrl
                + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "\">" + publicUrl + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "</a></div>");
        }
        
        if(isBorrador) {
        	content.append("<div>"+resourceBundle.getString("mail.reunion.esBorrador")+"</div>");
//   		content.append("<div>"+"Si"+"<a href=\"/goc/rest/reuniones/"+reunion.getId()+"/enviarconvocatoria"+"\">"+"</div>");
        }

        return content.toString();
    }
    
    public String formatAlternativeLanguage(String publicUrl, String textoAux, String textoConvocatoria, boolean ordenDia, boolean url, String alternativeLanguage, String textoInformar,String textoInformarAlternativo)
    {
    	ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", alternativeLanguage);
        StringBuffer content = new StringBuffer();

        content.append("<br/><hr/>");
        
        content.append("<h2>" + reunion.getAsuntoAlternativo() + "</h2>");
		
        if (textoConvocatoria != null)
        {
            content.append("<div><pre>" + textoConvocatoria.replaceAll("\n", "<br>") + "</pre></div><br/>");
        }

        if (textoAux != null)
        {
            content.append("<div>" + textoAux + "</div><br/>");
        }
        
        if(textoInformarAlternativo != null) {
        	content.append("<div><pre>" + textoInformarAlternativo.replaceAll("\n", "<br>") + "</pre></div><br/>");
        }

        if (reunion.getNumeroSesion() != null)
        {
            content.append(
                "<div><strong>" + resourceBundle.getString("mail.reunion.numeroSesion") + ": " + " </strong><span>" + reunion.getNumeroSesion() + "</span></div>");
        }

        if (reunion.getDescripcion() != null && !reunion.getDescripcion().isEmpty())
        {
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.descripcion") + ": " + "</strong><span>" + reunion.getDescripcion().replaceAll("\n", "<br>") + "</span></div>");
        }

        if (reunion.getFechaSegundaConvocatoria() == null)
        {
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.fechaYHora") + ": " + "</strong>" + formatter.format(reunion.getFecha()) + "</div>");
        }
        else
        {
            content.append(
                "<div><strong>"+ resourceBundle.getString("mail.reunion.primeraConvocatoria") + ": " +" </strong>" + formatter.format(reunion.getFecha()) + "</div>");
            content.append("<div><strong>"+ resourceBundle.getString("mail.reunion.segundaConvocatoria") + ":" + " </strong>" + formatter.format(
                reunion.getFechaSegundaConvocatoria()) + "</div>");
        }

        if (reunion.getUbicacion() != null && !reunion.getUbicacion().isEmpty())
        {
            content.append("<div><strong>"+ resourceBundle.getString("mail.reunion.ubicacion") + ": " + "</strong>" + reunion.getUbicacion() + "</div>");
        }

        if (reunion.getDuracion() != null && reunion.getDuracion() > 0)
        {
            content.append("<div><strong>"+ resourceBundle.getString("mail.reunion.duracion") + ": " + " </strong>" + reunion.getDuracion() + " "+ resourceBundle.getString("mail.reunion.minutos")+"</div>");
        }

        if (puntosOrdenDiaOrdenados != null && !puntosOrdenDiaOrdenados.isEmpty() && ordenDia)
        {
            content.append("<h4>"+ resourceBundle.getString("mail.reunion.ordenDia") +"</h4>");
            content.append(createListaPuntosOrdenDia(new TreeSet(puntosOrdenDiaOrdenados), false,null));
        }

        if(url)
        {
            content.append("<div>"+ resourceBundle.getString("mail.reunion.masInfo") +  "<a href=\"" + publicUrl
                + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "?lang="+alternativeLanguage+ "\">" + publicUrl + "/goc/rest/publicacion/reuniones/" + reunion.getId() + "</div>");
        }
        
        return content.toString();
    }
    
    public String formatAlternativeLanguage(String publicUrl, String textoAux, String textoConvocatoria, String alternativeLanguage, String textoInformar, String textoInformarAlternativo)
    {
        return formatAlternativeLanguage(publicUrl, textoAux, textoConvocatoria, true, true, alternativeLanguage, textoInformar, textoInformarAlternativo);
    }

    private String createListaPuntosOrdenDia(Set<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados, Boolean isMainLanguage,String nivelPadre)
    {
        StringBuilder puntosContent = new StringBuilder("<ul>");
        int i = 1;
        for(PuntoOrdenDiaMultinivel puntoOrdenDia : puntosOrdenDiaOrdenados)
        {
        	String puntoTitulo = isMainLanguage ? puntoOrdenDia.getTitulo() : puntoOrdenDia.getTituloAlternativo();
        	 String multinivel = nivelPadre != null ? String.format("%s.%s", nivelPadre, i) : String.valueOf(i);
            puntosContent.append("<li>" + multinivel+". "+puntoTitulo + "</li>");
            if(puntoOrdenDia.getPuntosInferiores() != null && !puntoOrdenDia.getPuntosInferiores().isEmpty())
            {
                puntosContent.append(createListaPuntosOrdenDia(puntoOrdenDia.getPuntosInferiores(), isMainLanguage,multinivel));
            }
            i++;
        }
        puntosContent.append("</ul>");

        return puntosContent.toString();
    }
 private String createPuntoUnico(PuntoOrdenDiaMultinivel puntoUnico) {
    	
    	StringBuilder cuerpo = new StringBuilder("<div>");
    	cuerpo.append("Punt Únic.  "+puntoUnico.getTitulo());
    	if(puntoUnico.getPuntosInferiores()!=null) {
    	cuerpo.append(createListaPuntosOrdenDia(puntoUnico.getPuntosInferiores(),true, null));
    	}
    	cuerpo.append("</div>");
    	return cuerpo.toString();
    }
}
