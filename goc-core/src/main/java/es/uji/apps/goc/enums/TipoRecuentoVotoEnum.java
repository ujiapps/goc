package es.uji.apps.goc.enums;

import java.util.HashSet;
import java.util.Set;

public enum TipoRecuentoVotoEnum
{
    MAYORIA_SIMPLE, MAYORIA_ABSOLUTA_PRESENTES, MAYORIA_ABSOLUTA_ORGANO,MAYORIA_DOS_TERCIOS, NO_VOTABLE;

    public static boolean checkRecuentoVoto(String voto) {
        Set<String> tiposRecuentoVotos = new HashSet<>();
        tiposRecuentoVotos.add(TipoRecuentoVotoEnum.MAYORIA_SIMPLE.name());
        tiposRecuentoVotos.add(TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_PRESENTES.name());
        tiposRecuentoVotos.add(TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_ORGANO.name());
        tiposRecuentoVotos.add(TipoRecuentoVotoEnum.MAYORIA_DOS_TERCIOS.name());
        tiposRecuentoVotos.add(TipoRecuentoVotoEnum.NO_VOTABLE.name());

        return tiposRecuentoVotos.contains(voto);
    }
}
