package es.uji.apps.goc.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.enums.CodigoCargoEnum;
import es.uji.apps.goc.model.Comentario;
import es.uji.apps.goc.model.ResponsableFirma;

public class ReunionTemplate implements Serializable
{
    private Long id;

    private String asunto;

    private Date fecha;

    private Date fechaSegundaConvocatoria;

    private Long numeroSesion;

    private String descripcion;

    private Long duracion;

    private String ubicacion;

    private String acuerdos;

    private Boolean publica;

    private Boolean telematica;

    private Boolean completada;

    private Boolean admiteSuplencia;

    private Boolean admiteDelegacionVoto;

    private Boolean admiteComentarios;

    private Boolean emailsEnNuevosComentarios;

    private String telematicaDescripcion;

    private String creadorNombre;

    private String creadorEmail;

    private Long creadorId;

    private String urlGrabacion;

    private Boolean comoAsistente;

    private String responsableActa;

    private String cargoResponsableActa;

    private ResponsableFirma firmante;

    private List<OrganoTemplate> organos;

    private List<Comentario> comentarios;

    private List<PuntoOrdenDiaTemplate> puntosOrdenDia;

    private List<DocumentoTemplate> documentos;

    private List<InvitadoTemplate> invitados;

    private String urlActa;
    
    private String urlActaAntesReapertura;
    
    private String urlActaAlternativaAntesReapertura;

    private String horaFinalizacion;

    private Long convocatoriaComienzo;

    private Boolean reabierta;

    private Boolean votacionTelematica;

    private Boolean admiteCambioVoto;

    private boolean isAbierta;

    private Boolean mostrarResultados;

    private Date fechaFinVotacion;
    
    private Boolean verDeliberaciones;
    
	private Boolean verAcuerdos;
	




    public ReunionTemplate()
    {
    }

    public ReunionTemplate(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getAsunto()
    {
        return asunto;
    }

    public void setAsunto(String asunto)
    {
        this.asunto = asunto;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getNumeroSesion()
    {
        return numeroSesion;
    }

    public void setNumeroSesion(Long numeroSesion)
    {
        this.numeroSesion = numeroSesion;
    }

    public Long getDuracion()
    {
        return duracion;
    }

    public void setDuracion(Long duracion)
    {
        this.duracion = duracion;
    }

    public String getUbicacion()
    {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion)
    {
        this.ubicacion = ubicacion;
    }

    public String getAcuerdos()
    {
        return acuerdos;
    }

    public void setAcuerdos(String acuerdos)
    {
        this.acuerdos = acuerdos;
    }

    public List<PuntoOrdenDiaTemplate> getPuntosOrdenDia()
    {
        return puntosOrdenDia;
    }

    public void setPuntosOrdenDia(List<PuntoOrdenDiaTemplate> puntosOrdenDia)
    {
        this.puntosOrdenDia = puntosOrdenDia;
    }

    public List<OrganoTemplate> getOrganos()
    {
        return organos;
    }

    public void setOrganos(List<OrganoTemplate> organos)
    {
        this.organos = organos;
    }

    public List<DocumentoTemplate> getDocumentos()
    {
        return documentos;
    }

    public void setDocumentos(List<DocumentoTemplate> documentos)
    {
        this.documentos = documentos;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public List<Comentario> getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios)
    {
        this.comentarios = comentarios;
    }

    public String getUrlGrabacion()
    {
        return urlGrabacion;
    }

    public void setUrlGrabacion(String urlGrabacion)
    {
        this.urlGrabacion = urlGrabacion;
    }

    public String getResponsableActa()
    {
        return responsableActa;
    }

    public void setResponsableActa(String responsableActa)
    {
        this.responsableActa = responsableActa;
    }

    public Boolean isTelematica()
    {
        return telematica;
    }

    public void setTelematica(Boolean telematica)
    {
        this.telematica = telematica;
    }

    public String getTelematicaDescripcion()
    {
        return telematicaDescripcion;
    }

    public void setTelematicaDescripcion(String telematicaDescripcion)
    {
        this.telematicaDescripcion = telematicaDescripcion;
    }


    public String getCreadorNombre()
    {
        return creadorNombre;
    }

    public void setCreadorNombre(String creadorNombre)
    {
        this.creadorNombre = creadorNombre;
    }

    public String getCreadorEmail()
    {
        return creadorEmail;
    }

    public void setCreadorEmail(String creadorEmail)
    {
        this.creadorEmail = creadorEmail;
    }

    public Boolean isCompletada()
    {
        return completada;
    }

    public void setCompletada(Boolean completada)
    {
        this.completada = completada;
    }

    public Boolean isAdmiteSuplencia()
    {
        return admiteSuplencia;
    }

    public void setAdmiteSuplencia(Boolean admiteSuplencia)
    {
        this.admiteSuplencia = admiteSuplencia;
    }

    public Boolean isComoAsistente()
    {
        return comoAsistente;
    }

    public void setComoAsistente(Boolean comoAsistente)
    {
        this.comoAsistente = comoAsistente;
    }

    public void setAdmiteComentarios(Boolean admiteComentarios)
    {
        this.admiteComentarios = admiteComentarios;
    }

    public Boolean isAdmiteComentarios()
    {
        return admiteComentarios;
    }

    public Date getFechaSegundaConvocatoria()
    {
        return fechaSegundaConvocatoria;
    }

    public void setFechaSegundaConvocatoria(Date fechaSegundaConvocatoria)
    {
        this.fechaSegundaConvocatoria = fechaSegundaConvocatoria;
    }

    public String getCargoResponsableActa()
    {
        return cargoResponsableActa;
    }

    public void setCargoResponsableActa(String cargoResponsableActa)
    {
        this.cargoResponsableActa = cargoResponsableActa;
    }

    public Long getCreadorId()
    {
        return creadorId;
    }

    public void setCreadorId(Long creadorId)
    {
        this.creadorId = creadorId;
    }

    public List<InvitadoTemplate> getInvitados()
    {
        return ordenInvitados(invitados);
    }

    public void setInvitados(List<InvitadoTemplate> invitados)
    {
        this.invitados = invitados;
    }
    
    private List<InvitadoTemplate> ordenInvitados(List<InvitadoTemplate> invitados)
    {
        if (invitados == null) return null;

        List<InvitadoTemplate> invitadosOrdenados = new ArrayList<>();
        
    	invitadosOrdenados.addAll(invitados.stream().sorted(Comparator.comparing(InvitadoTemplate::getNombreLimpio)).collect(Collectors.toList()));

        return invitadosOrdenados;
    }

    public Boolean getAdmiteDelegacionVoto()
    {
        return admiteDelegacionVoto;
    }

    public void setAdmiteDelegacionVoto(Boolean admiteDelegacionVoto)
    {
        this.admiteDelegacionVoto = admiteDelegacionVoto;
    }

    public String getUrlActa()
    {
        return urlActa;
    }

    public void setUrlActa(String urlActa)
    {
        this.urlActa = urlActa;
    }
    
    public String getUrlActaAntesReapertura() {
		return urlActaAntesReapertura;
	}
    
	public void setUrlActaAntesReapertura(String urlActaAntesReapertura) {
		this.urlActaAntesReapertura = urlActaAntesReapertura;
	}
	
	public String getUrlActaAlternativaAntesReapertura() {
		return urlActaAlternativaAntesReapertura;
	}
	
	public void setUrlActaAlternativaAntesReapertura(String urlActaAlternativaAntesReapertura) {
		this.urlActaAlternativaAntesReapertura = urlActaAlternativaAntesReapertura;
	}

    public Boolean isPublica()
    {
        return publica;
    }

    public void setPublica(Boolean publica)
    {
        this.publica = publica;
    }

    public ResponsableFirma getFirmante()
    {
        return firmante;
    }

    public void setFirmante(ResponsableFirma firmante)
    {
        this.firmante = firmante;
    }

    public String getHoraFinalizacion()
    {
        return horaFinalizacion;
    }

    public void setHoraFinalizacion(String horaFinalizacion)
    {
        this.horaFinalizacion = horaFinalizacion;
    }

    public Long getConvocatoriaComienzo()
    {
        return convocatoriaComienzo;
    }

    public void setConvocatoriaComienzo(Long convocatoriaComienzo)
    {
        this.convocatoriaComienzo = convocatoriaComienzo;
    }

    public Boolean getEmailsEnNuevosComentarios()
    {
        return emailsEnNuevosComentarios;
    }

    public void setEmailsEnNuevosComentarios(Boolean emailsEnNuevosComentarios)
    {
        this.emailsEnNuevosComentarios = emailsEnNuevosComentarios;
    }

    public Boolean getReabierta()
    {
        return reabierta;
    }

    public void setReabierta(Boolean reabierta)
    {
        this.reabierta = reabierta;
    }

    public Boolean getVotacionTelematica() {
        return votacionTelematica;
    }

    public void setVotacionTelematica(Boolean votacionTelematica) {
        this.votacionTelematica = votacionTelematica;
    }

    public Boolean getAdmiteCambioVoto() {
        return admiteCambioVoto;
    }

    public void setAdmiteCambioVoto(Boolean admiteCambioVoto) {
        this.admiteCambioVoto = admiteCambioVoto;
    }

    public boolean isAbierta() {
        return isAbierta;
    }

    public void setAbierta(boolean abierta) {
        isAbierta = abierta;
    }

    public Boolean getMostrarResultados() {
        return mostrarResultados == null ? false : mostrarResultados;
    }

    public void setMostrarResultados(Boolean mostrarResultados) {
        this.mostrarResultados = mostrarResultados == null ? false : mostrarResultados;
    }

    public Boolean isTipoVisualizacionResultados() {
        return getMostrarResultados();
    }

    public Date getFechaFinVotacion() {
        return fechaFinVotacion;
    }

    public void setFechaFinVotacion(Date fechaFinVotacion) {
        this.fechaFinVotacion = fechaFinVotacion;
    }
    
    public Boolean getVerDeliberaciones() {
		return verDeliberaciones;
	}

	public void setVerDeliberaciones(Boolean verDeliberaciones) {
		this.verDeliberaciones = verDeliberaciones;
	}
	
	 public Boolean isVerDeliberaciones() {
	   return verDeliberaciones;
	}

	public Boolean getVerAcuerdos() {
		return verAcuerdos;
	}

	public void setVerAcuerdos(Boolean verAcuerdos) {
		this.verAcuerdos = verAcuerdos;
	}
	
	public Boolean isVerAcuerdos() {
		return verAcuerdos;
	}
}
