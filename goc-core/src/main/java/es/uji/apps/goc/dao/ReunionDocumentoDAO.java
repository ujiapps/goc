package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import es.uji.apps.goc.dto.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ReunionDocumentoDAO extends BaseDAODatabaseImpl {
    private QReunionDocumento qReunionDocumento = QReunionDocumento.reunionDocumento;

    public List<ReunionDocumento> getDocumentosActivosByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qReunionDocumento)
                .where(qReunionDocumento.reunion.id.eq(reunionId)
                        .and(qReunionDocumento.activo.isTrue()))
                .orderBy(qReunionDocumento.orden.desc());

        return query.list(qReunionDocumento);
    }

    public List<ReunionDocumento> getDatosDocumentosByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qReunionDocumento)
                .where(qReunionDocumento.reunion.id.eq(reunionId)
                        .and(qReunionDocumento.activo.isTrue()))
                .orderBy(qReunionDocumento.orden.asc());

        List<Tuple> tuplas = query.list(qReunionDocumento.creadorId, qReunionDocumento.descripcion,
                qReunionDocumento.descripcionAlternativa, qReunionDocumento.fechaAdicion, qReunionDocumento.id,
                qReunionDocumento.mimeType, qReunionDocumento.nombreFichero, qReunionDocumento.hash,
                qReunionDocumento.orden, qReunionDocumento.editorId, qReunionDocumento.fechaAdicion,
                qReunionDocumento.motivoEdicion, qReunionDocumento.activo);

        List<ReunionDocumento> documentos = new ArrayList<>();

        for (Tuple tupla : tuplas) {
            ReunionDocumento reunionDocumento = new ReunionDocumento();

            reunionDocumento.setId(tupla.get(qReunionDocumento.id));
            reunionDocumento.setCreadorId(tupla.get(qReunionDocumento.creadorId));
            reunionDocumento.setDescripcion(tupla.get(qReunionDocumento.descripcion));
            reunionDocumento.setDescripcionAlternativa(tupla.get(qReunionDocumento.descripcionAlternativa));
            reunionDocumento.setFechaAdicion(tupla.get(qReunionDocumento.fechaAdicion));
            reunionDocumento.setMimeType(tupla.get(qReunionDocumento.mimeType));
            reunionDocumento.setNombreFichero(tupla.get(qReunionDocumento.nombreFichero));
            reunionDocumento.setHash(tupla.get(qReunionDocumento.hash));
            reunionDocumento.setOrden(tupla.get(qReunionDocumento.orden));
            reunionDocumento.setEditorId(tupla.get(qReunionDocumento.editorId));
            reunionDocumento.setFechaEdicion(tupla.get(qReunionDocumento.fechaEdicion));
            reunionDocumento.setMotivoEdicion(tupla.get(qReunionDocumento.motivoEdicion));
            reunionDocumento.setActivo(tupla.get(qReunionDocumento.activo));

            documentos.add(reunionDocumento);
        }

        return documentos;
    }

    public Long getOrdenUltimoDocumento(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunionDocumento)
                .where(qReunionDocumento.reunion.id.eq(reunionId))
                .orderBy(qReunionDocumento.orden.desc())
                .singleResult(qReunionDocumento.orden);
    }

    public ReunionDocumento getDocumentoAnterior(Long reunionId, Long orden) {
        BooleanExpression be = qReunionDocumento.reunion.id.eq(reunionId).and(qReunionDocumento.orden.lt(orden));
        JPAQuery query = new JPAQuery(entityManager);

        Tuple tuple = query.from(qReunionDocumento).
                where(be).
                orderBy(qReunionDocumento.orden.desc()).
                singleResult(qReunionDocumento.id, qReunionDocumento.orden);

        if (tuple == null) {
            return null;
        }

        ReunionDocumento documento = new ReunionDocumento();
        documento.setId(tuple.get(qReunionDocumento.id));
        documento.setOrden(tuple.get(qReunionDocumento.orden));

        return documento;
    }

    public ReunionDocumento getDocumentoPosterior(Long reunionId, Long orden) {
        BooleanExpression be = qReunionDocumento.reunion.id.eq(reunionId).and(qReunionDocumento.orden.gt(orden));
        JPAQuery query = new JPAQuery(entityManager);

        Tuple tuple = query.from(qReunionDocumento).
                where(be).
                orderBy(qReunionDocumento.orden.asc()).
                singleResult(qReunionDocumento.id, qReunionDocumento.orden);

        if (tuple == null) {
            return null;
        }

        ReunionDocumento documento = new ReunionDocumento();
        documento.setId(tuple.get(qReunionDocumento.id));
        documento.setOrden(tuple.get(qReunionDocumento.orden));

        return documento;
    }

    public ReunionDocumento getDocumentoById(Long documentoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qReunionDocumento).where(qReunionDocumento.id.eq(documentoId));

        List<ReunionDocumento> resultado = query.list(qReunionDocumento);

        if (resultado.size() != 1) {
            return null;
        }

        return resultado.get(0);
    }

    public ReunionDocumento getDatosDocumentoById(Long documentoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qReunionDocumento).where(qReunionDocumento.id.eq(documentoId));

        Tuple tupla = query.singleResult(qReunionDocumento.id, qReunionDocumento.orden);

        ReunionDocumento reunionDocumento = new ReunionDocumento();
        reunionDocumento.setId(tupla.get(qReunionDocumento.id));
        reunionDocumento.setOrden(tupla.get(qReunionDocumento.orden));

        return reunionDocumento;
    }

    @Transactional
    public void cambiarOrden(Long documentoId, Long orden) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionDocumento);
        update.set(qReunionDocumento.orden, orden).where(qReunionDocumento.id.eq(documentoId));
        update.execute();
    }

    @Transactional
    public void updateMotivoEdicion(Long documentoId, String motivoEdicion, Long connectedUserId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionDocumento);
        update.set(qReunionDocumento.motivoEdicion, motivoEdicion).
                set(qReunionDocumento.fechaEdicion, new Date()).
                set(qReunionDocumento.editorId, connectedUserId).
                where(qReunionDocumento.id.eq(documentoId));
        update.execute();
    }

    @Transactional
    public void disable(Long documentoId, Long connectedUserId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionDocumento);
        update.set(qReunionDocumento.editorId, connectedUserId).
                set(qReunionDocumento.fechaEdicion, new Date()).
                set(qReunionDocumento.activo, false).
                where(qReunionDocumento.id.eq(documentoId));
        update.execute();
    }

    public boolean tieneDocumentosReunionOPuntosOrdenDia(Long reunionId) {
        QPuntoOrdenDiaDocumento qPuntoOrdenDiaDocumento = QPuntoOrdenDiaDocumento.puntoOrdenDiaDocumento;
        QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
        QReunion qReunion = QReunion.reunion;
        JPAQuery query = new JPAQuery(entityManager);
        boolean reunionTieneDocumentos =
                query.from(qReunionDocumento).where(qReunionDocumento.reunion.id.eq(reunionId).and(qReunionDocumento.activo.isTrue())).exists();

        query = new JPAQuery(entityManager);
        boolean puntosTienenDocumentos = query.from(qPuntoOrdenDiaDocumento).join(qPuntoOrdenDiaDocumento.puntoOrdenDia, qPuntoOrdenDia)
                .join(qPuntoOrdenDia.reunion, qReunion).where(qReunion.id.eq(reunionId).and(qPuntoOrdenDiaDocumento.activo.isTrue())).exists();

        return reunionTieneDocumentos || puntosTienenDocumentos;
    }
}