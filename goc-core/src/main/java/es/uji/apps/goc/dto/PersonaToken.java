package es.uji.apps.goc.dto;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_PERSONA_TOKEN")
public class PersonaToken
{
    @EmbeddedId
    private PersonaTokenKey id;

    public PersonaTokenKey getId()
    {
        return id;
    }

    public void setId(PersonaTokenKey id)
    {
        this.id = id;
    }
}
