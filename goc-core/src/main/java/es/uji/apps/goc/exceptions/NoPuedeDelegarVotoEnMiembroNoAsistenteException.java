package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoPuedeDelegarVotoEnMiembroNoAsistenteException extends CoreBaseException
{

    public NoPuedeDelegarVotoEnMiembroNoAsistenteException() {
        super("No se puede delegar el voto en un miembro que no tiene confirmada la asistencia");
    }
}
