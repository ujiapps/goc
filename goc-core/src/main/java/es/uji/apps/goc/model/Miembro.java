package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.commons.rest.StringUtils;

public class Miembro
{
    private Long id;

    private Long personaId;

    private String nombre;

    private String email;
    private Organo organo;
    private Cargo cargo;

    private String condicion;
    private String condicionAlternativa;

    private Boolean responsableActa;

    private Boolean nato;

    public Miembro()
    {
    }

    public Miembro(Long id, String nombre, String email, Organo organo, Cargo cargo)
    {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.organo = organo;
        this.cargo = cargo;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }
    
    public String getNombreLimpio()
    {
        if (nombre == null) return null;

        return StringUtils.limpiaAcentos(nombre).trim();
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Organo getOrgano()
    {
        return organo;
    }

    public void setOrgano(Organo organo)
    {
        this.organo = organo;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Cargo getCargo()
    {
        return cargo;
    }

    public void setCargo(Cargo cargo)
    {
        this.cargo = cargo;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getCondicion()
    {
        return condicion;
    }

    public void setCondicion(String condicion)
    {
        this.condicion = condicion;
    }

    public String getCondicionAlternativa()
    {
        return condicionAlternativa;
    }

    public void setCondicionAlternativa(String condicionAlternativa)
    {
        this.condicionAlternativa = condicionAlternativa;
    }

    public Boolean getResponsableActa()
    {
        return responsableActa;
    }

    public void setResponsableActa(Boolean responsableActa)
    {
        this.responsableActa = responsableActa;
    }

    public Boolean isResponsableActa()
    {
        return responsableActa;
    }

    public OrganoReunionMiembro toOrganoReunionMiembro(OrganoReunion organoReunion)
    {
        OrganoReunionMiembro organoReunionMiembro = new OrganoReunionMiembro();
        organoReunionMiembro.setOrganoReunion(organoReunion);

        if (organoReunion.isExterno())
        {
            organoReunionMiembro.setOrganoExterno(true);
        }
        else
        {
            organoReunionMiembro.setOrganoExterno(false);
        }

        organoReunionMiembro.setNombre(this.getNombre());
        organoReunionMiembro.setEmail(this.getEmail());
        organoReunionMiembro.setAsistencia(true);
        organoReunionMiembro.setOrganoId(organoReunion.getOrganoId());
        organoReunionMiembro.setReunionId(organoReunion.getReunion().getId());
        organoReunionMiembro.setMiembroId(this.getPersonaId().toString());
        organoReunionMiembro.setCargoId(this.getCargo().getId());
        organoReunionMiembro.setCargoCodigo(this.getCargo().getCodigo());
        organoReunionMiembro.setCargoNombre(this.getCargo().getNombre());
        organoReunionMiembro.setCargoNombreAlternativo(this.getCargo().getNombreAlternativo());
        organoReunionMiembro.setCondicion(this.getCondicion());
        organoReunionMiembro.setCondicionAlternativa(this.getCondicionAlternativa());
        organoReunionMiembro.setResponsableActa(this.getCargo().getResponsableActa());
        organoReunionMiembro.setCargoSuplente(this.getCargo().getSuplente());
        organoReunionMiembro.setNato(this.getNato());

        return organoReunionMiembro;
    }

    public boolean getNato() {
        return nato != null ? nato : false;
    }

    public boolean isNato() {
        return getNato();
    }

    public void setNato(boolean nato) {
        this.nato = nato;
    }
}