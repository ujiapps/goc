package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.dto.QPuntoOrdenDiaDocumento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class PuntoOrdenDiaDocumentoDAO extends BaseDAODatabaseImpl {
    private QPuntoOrdenDiaDocumento qPuntoOrdenDiaDocumento = QPuntoOrdenDiaDocumento.puntoOrdenDiaDocumento;

    public Long getNumeroDocumentosPorPuntoOrdenDia(Long puntoOrdenDiaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPuntoOrdenDiaDocumento).where(qPuntoOrdenDiaDocumento.puntoOrdenDia.id.eq(puntoOrdenDiaId).and(qPuntoOrdenDiaDocumento.activo.isTrue())).count();
    }

    public Long getOrdenUltimoDocumento(Long puntoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPuntoOrdenDiaDocumento).
                where(qPuntoOrdenDiaDocumento.puntoOrdenDia.id.eq(puntoId)).
                orderBy(qPuntoOrdenDiaDocumento.orden.desc()).
                singleResult(qPuntoOrdenDiaDocumento.orden);
    }

    public PuntoOrdenDiaDocumento getDocumentoAnterior(Long puntoOrdenDiaId, Long orden) {
        BooleanExpression be = qPuntoOrdenDiaDocumento.puntoOrdenDia.id.eq(puntoOrdenDiaId).and(qPuntoOrdenDiaDocumento.orden.lt(orden));
        JPAQuery query = new JPAQuery(entityManager);

        Tuple tuple = query.from(qPuntoOrdenDiaDocumento).
                where(be).
                orderBy(qPuntoOrdenDiaDocumento.orden.desc()).
                singleResult(qPuntoOrdenDiaDocumento.id, qPuntoOrdenDiaDocumento.orden);

        if (tuple == null) {
            return null;
        }

        PuntoOrdenDiaDocumento puntoOrdenDiaDocumento = new PuntoOrdenDiaDocumento();
        puntoOrdenDiaDocumento.setId(tuple.get(qPuntoOrdenDiaDocumento.id));
        puntoOrdenDiaDocumento.setOrden(tuple.get(qPuntoOrdenDiaDocumento.orden));

        return puntoOrdenDiaDocumento;
    }

    public PuntoOrdenDiaDocumento getDocumentoPosterior(Long puntoOrdenDiaId, Long orden) {
        BooleanExpression be = qPuntoOrdenDiaDocumento.puntoOrdenDia.id.eq(puntoOrdenDiaId).and(qPuntoOrdenDiaDocumento.orden.gt(orden));
        JPAQuery query = new JPAQuery(entityManager);

        Tuple tuple = query.from(qPuntoOrdenDiaDocumento).
                where(be).
                orderBy(qPuntoOrdenDiaDocumento.orden.asc()).
                singleResult(qPuntoOrdenDiaDocumento.id, qPuntoOrdenDiaDocumento.orden);

        if (tuple == null) {
            return null;
        }

        PuntoOrdenDiaDocumento puntoOrdenDiaDocumento = new PuntoOrdenDiaDocumento();
        puntoOrdenDiaDocumento.setId(tuple.get(qPuntoOrdenDiaDocumento.id));
        puntoOrdenDiaDocumento.setOrden(tuple.get(qPuntoOrdenDiaDocumento.orden));

        return puntoOrdenDiaDocumento;
    }


    public void cambiarOrdenDocumento(Long documentoId, Long orden) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDiaDocumento);
        update.set(qPuntoOrdenDiaDocumento.orden, orden).where(qPuntoOrdenDiaDocumento.id.eq(documentoId));
        update.execute();
    }

    @Transactional
    public void updateMotivoEdicion(Long documentoId, String motivoEdicion, Long connectedUserId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDiaDocumento);
        update.set(qPuntoOrdenDiaDocumento.motivoEdicion, motivoEdicion).
                set(qPuntoOrdenDiaDocumento.fechaEdicion, new Date()).
                set(qPuntoOrdenDiaDocumento.editorId, connectedUserId).
                where(qPuntoOrdenDiaDocumento.id.eq(documentoId));
        update.execute();
    }

    @Transactional
    public void disable(Long documentoId, Long connectedUserId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDiaDocumento);
        update.set(qPuntoOrdenDiaDocumento.activo, false).
                set(qPuntoOrdenDiaDocumento.fechaEdicion, new Date()).
                set(qPuntoOrdenDiaDocumento.editorId, connectedUserId).
                where(qPuntoOrdenDiaDocumento.id.eq(documentoId));
        update.execute();
    }

    public PuntoOrdenDiaDocumento getDocumentoById(Long documentoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDiaDocumento).where(qPuntoOrdenDiaDocumento.id.eq(documentoId));

        List<PuntoOrdenDiaDocumento> resultado = query.list(qPuntoOrdenDiaDocumento);

        if (resultado.size() != 1) {
            return null;
        }

        return resultado.get(0);
    }

    public PuntoOrdenDiaDocumento getDatosDocumentoById(Long documentoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDiaDocumento).where(qPuntoOrdenDiaDocumento.id.eq(documentoId));

        Tuple tupla = query.singleResult(qPuntoOrdenDiaDocumento.id, qPuntoOrdenDiaDocumento.orden, qPuntoOrdenDiaDocumento.creadorId);

        PuntoOrdenDiaDocumento puntoOrdenDiaDocumento = new PuntoOrdenDiaDocumento();
        puntoOrdenDiaDocumento.setId(tupla.get(qPuntoOrdenDiaDocumento.id));
        puntoOrdenDiaDocumento.setOrden(tupla.get(qPuntoOrdenDiaDocumento.orden));
        puntoOrdenDiaDocumento.setCreadorId(tupla.get(qPuntoOrdenDiaDocumento.creadorId));

        return puntoOrdenDiaDocumento;
    }

    public List<PuntoOrdenDiaDocumento> getDocumentosActivosByPuntoOrdenDiaId(Long puntoOrdenDiaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDiaDocumento)
                .where(qPuntoOrdenDiaDocumento.puntoOrdenDia.id.eq(puntoOrdenDiaId)
                        .and(qPuntoOrdenDiaDocumento.activo.isTrue()))
                .orderBy(qPuntoOrdenDiaDocumento.orden.asc());

        return query.list(qPuntoOrdenDiaDocumento);
    }

    public List<PuntoOrdenDiaDocumento> getDatosDocumentosByPuntoOrdenDiaId(Long puntoOrdenDiaId, Boolean esParaFicha) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDiaDocumento);

        BooleanExpression where = qPuntoOrdenDiaDocumento.puntoOrdenDia.id.eq(puntoOrdenDiaId).
                and(qPuntoOrdenDiaDocumento.activo.isTrue());

        if (esParaFicha) {
            where = where.and(qPuntoOrdenDiaDocumento.mostrarEnFicha.isTrue());
        }

        query.where(where).orderBy(qPuntoOrdenDiaDocumento.orden.asc());

        List<Tuple> tuplas = query.list(qPuntoOrdenDiaDocumento.creadorId, qPuntoOrdenDiaDocumento.descripcion,
                qPuntoOrdenDiaDocumento.descripcionAlternativa, qPuntoOrdenDiaDocumento.fechaAdicion, qPuntoOrdenDiaDocumento.id,
                qPuntoOrdenDiaDocumento.mimeType, qPuntoOrdenDiaDocumento.nombreFichero, qPuntoOrdenDiaDocumento.publico, qPuntoOrdenDiaDocumento.orden,
                qPuntoOrdenDiaDocumento.hash, qPuntoOrdenDiaDocumento.editorId, qPuntoOrdenDiaDocumento.fechaEdicion,
                qPuntoOrdenDiaDocumento.motivoEdicion, qPuntoOrdenDiaDocumento.activo);

        List<PuntoOrdenDiaDocumento> documentos = new ArrayList<>();

        for (Tuple tupla : tuplas) {
            PuntoOrdenDiaDocumento puntoOrdenDiaDocumento = new PuntoOrdenDiaDocumento();

            puntoOrdenDiaDocumento.setId(tupla.get(qPuntoOrdenDiaDocumento.id));
            puntoOrdenDiaDocumento.setCreadorId(tupla.get(qPuntoOrdenDiaDocumento.creadorId));
            puntoOrdenDiaDocumento.setDescripcion(tupla.get(qPuntoOrdenDiaDocumento.descripcion));
            puntoOrdenDiaDocumento.setDescripcionAlternativa(tupla.get(qPuntoOrdenDiaDocumento.descripcionAlternativa));
            puntoOrdenDiaDocumento.setFechaAdicion(tupla.get(qPuntoOrdenDiaDocumento.fechaAdicion));
            puntoOrdenDiaDocumento.setMimeType(tupla.get(qPuntoOrdenDiaDocumento.mimeType));
            puntoOrdenDiaDocumento.setNombreFichero(tupla.get(qPuntoOrdenDiaDocumento.nombreFichero));
            puntoOrdenDiaDocumento.setPublico(tupla.get(qPuntoOrdenDiaDocumento.publico));
            puntoOrdenDiaDocumento.setOrden(tupla.get(qPuntoOrdenDiaDocumento.orden));
            puntoOrdenDiaDocumento.setHash(tupla.get(qPuntoOrdenDiaDocumento.hash));
            puntoOrdenDiaDocumento.setEditorId(tupla.get(qPuntoOrdenDiaDocumento.editorId));
            puntoOrdenDiaDocumento.setFechaEdicion(tupla.get(qPuntoOrdenDiaDocumento.fechaEdicion));
            puntoOrdenDiaDocumento.setMotivoEdicion(tupla.get(qPuntoOrdenDiaDocumento.motivoEdicion));
            puntoOrdenDiaDocumento.setActivo(tupla.get(qPuntoOrdenDiaDocumento.activo));

            documentos.add(puntoOrdenDiaDocumento);
        }

        return documentos;
    }
}