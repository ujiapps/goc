package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoVotacionTelematicaException extends CoreBaseException
{
    public NoVotacionTelematicaException() {
        super("La reunión no tiene votacion telemática");
    }
}
