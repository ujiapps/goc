package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.exceptions.NoVotacionTelematicaException;
import es.uji.apps.goc.exceptions.PuntoDelDiaConAcuerdosException;
import es.uji.apps.goc.exceptions.ReunionNoAbiertaException;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Repository
public class PuntoOrdenDiaDAO extends BaseDAODatabaseImpl {
    private QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    private QPuntoOrdenDiaDocumento qPuntoOrdenDiaDocumento = QPuntoOrdenDiaDocumento.puntoOrdenDiaDocumento;
    private QPuntoOrdenDiaDescriptor qPuntoOrdenDiaDescriptor = QPuntoOrdenDiaDescriptor.puntoOrdenDiaDescriptor;
    private QPuntoOrdenDiaAcuerdo qPuntoOrdenDiaAcuerdo = QPuntoOrdenDiaAcuerdo.puntoOrdenDiaAcuerdo;
    private QPuntoOrdenDiaMultinivel qPuntoOrdenDiaMultinivel = QPuntoOrdenDiaMultinivel.puntoOrdenDiaMultinivel;

    public List<PuntoOrdenDia> getPuntosByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDia).where(qPuntoOrdenDia.reunion.id.eq(reunionId)).orderBy(qPuntoOrdenDia.orden.asc());

        return query.list(qPuntoOrdenDia);
    }

    public List<PuntoOrdenDiaMultinivel> getPuntosMultinivelByReunionIdFormateado(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        query
                .from(qPuntoOrdenDiaMultinivel)
                .where(qPuntoOrdenDiaMultinivel.reunion.id.eq(reunionId));

        return sort(query.orderBy(qPuntoOrdenDiaMultinivel.orden.asc()).list(qPuntoOrdenDiaMultinivel));
    }

    private List<PuntoOrdenDiaMultinivel> sort(List<PuntoOrdenDiaMultinivel> puntoOrdenDiaMultinivels) {
        List<PuntoOrdenDiaMultinivel> puntoOrdenDiaMultinivelsCombinados = juntarPuntosOrdenDiaDocumentosYAcuerdos(puntoOrdenDiaMultinivels);
        List<PuntoOrdenDiaMultinivel> puntosOrdenados = puntoOrdenDiaMultinivelsCombinados.stream().sorted().collect(toList());
        Map<Long, List<PuntoOrdenDiaMultinivel>> puntosOrdenadosYAgrupadosPorPadreId =
                puntosOrdenados.stream().sorted().collect(groupingBy(punto -> (punto.getPuntoSuperiorId())));

        for (PuntoOrdenDiaMultinivel punto : puntosOrdenados) {
            if (puntosOrdenadosYAgrupadosPorPadreId.get(punto.getId()) != null)
                punto.setPuntosInferiores(mapToSet(puntosOrdenadosYAgrupadosPorPadreId.get(punto.getId()).stream().sorted().collect(toList())));
        }

        return puntosOrdenados.stream().filter(punto -> punto.getPuntoSuperior() == null || (punto.getPuntoSuperior() != null && punto.getPuntoSuperior().getId() == null)).collect(toList());
    }

    private List<PuntoOrdenDiaMultinivel> juntarPuntosOrdenDiaDocumentosYAcuerdos(List<PuntoOrdenDiaMultinivel> puntoOrdenDiaMultinivels) {
        ArrayList<PuntoOrdenDiaMultinivel> puntosYaCombinados = new ArrayList<>();
        Map<Long, List<PuntoOrdenDiaMultinivel>> gruposPorId = puntoOrdenDiaMultinivels.stream().collect(groupingBy(punto -> punto.getId()));

        for (Long idPunto : gruposPorId.keySet()) {
            Boolean firstTime = true;
            PuntoOrdenDiaMultinivel puntoYaCombinado = null;
            for (PuntoOrdenDiaMultinivel puntoDocumentosYAcuerdos : gruposPorId.get(idPunto)) {
                if (firstTime) {
                    firstTime = false;
                    puntoYaCombinado = puntoDocumentosYAcuerdos;
                } else {
                    if (puntoDocumentosYAcuerdos.getPuntoOrdenDiaAcuerdos() != null) {
                        addPuntoAcuerdos(puntoYaCombinado, puntoDocumentosYAcuerdos);
                    }
                    if (puntoDocumentosYAcuerdos.getPuntoOrdenDiaDocumentos() != null) {
                        addPuntoDocumentos(puntoYaCombinado, puntoDocumentosYAcuerdos);
                    }
                }
            }
            puntosYaCombinados.add(puntoYaCombinado);
        }
        return puntosYaCombinados;
    }

    private void addPuntoAcuerdos(PuntoOrdenDiaMultinivel puntoYaCombinado, PuntoOrdenDiaMultinivel puntoDocumentosYAcuerdos) {
        if (puntoYaCombinado.getPuntoOrdenDiaAcuerdos() == null) {
            Set<PuntoOrdenDiaAcuerdo> acuerdos = new HashSet<>();
            puntoYaCombinado.setPuntoOrdenDiaAcuerdos(acuerdos);
            puntoYaCombinado.getPuntoOrdenDiaAcuerdos().addAll(puntoDocumentosYAcuerdos.getPuntoOrdenDiaAcuerdos());
        } else {
            puntoYaCombinado.getPuntoOrdenDiaAcuerdos().addAll(puntoDocumentosYAcuerdos.getPuntoOrdenDiaAcuerdos());
        }
    }

    private void addPuntoDocumentos(PuntoOrdenDiaMultinivel puntoYaCombinado, PuntoOrdenDiaMultinivel puntoDocumentosYAcuerdos) {
        if (puntoYaCombinado.getPuntoOrdenDiaDocumentos() == null) {
            Set<PuntoOrdenDiaDocumento> documentos = new HashSet<>();
            puntoYaCombinado.setPuntoOrdenDiaDocumentos(documentos);
            puntoYaCombinado.getPuntoOrdenDiaDocumentos().addAll(puntoDocumentosYAcuerdos.getPuntoOrdenDiaDocumentos());
        } else {
            puntoYaCombinado.getPuntoOrdenDiaDocumentos().addAll(puntoDocumentosYAcuerdos.getPuntoOrdenDiaDocumentos());
        }
    }

    private Set<PuntoOrdenDiaMultinivel> mapToSet(List<PuntoOrdenDiaMultinivel> puntoOrdenDiaMultinivels) {
        Set<PuntoOrdenDiaMultinivel> puntosInferiores = new LinkedHashSet<>();
        for (PuntoOrdenDiaMultinivel puntoOrden : puntoOrdenDiaMultinivels) {
            puntosInferiores.add(puntoOrden);
        }
        return puntosInferiores;
    }

    public PuntoOrdenDia getPuntoOrdenDiaById(Long puntoOrdenDiaId) {
        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia =
                query.from(qPuntoOrdenDia).where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId)).list(qPuntoOrdenDia);

        if (puntosOrdenDia.size() == 0) {
            return null;
        }

        return puntosOrdenDia.get(0);

    }

    public PuntoOrdenDia getSiguientePuntoOrdenDiaByOrdenYNivel(Long reunionId, Long orden, PuntoOrdenDia padre) {
        BooleanExpression be = qPuntoOrdenDia.reunion.id.eq(reunionId).and(qPuntoOrdenDia.orden.gt(orden));

        be = (padre == null) ?
                be.and(qPuntoOrdenDia.puntoSuperior.isNull()) :
                be.and(qPuntoOrdenDia.puntoSuperior.id.eq(padre.getId()));

        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia = query.from(qPuntoOrdenDia)
                .where(be)
                .orderBy(qPuntoOrdenDia.orden.asc())
                .list(qPuntoOrdenDia);

        if (puntosOrdenDia.size() == 0) {
            return null;
        }

        return puntosOrdenDia.get(0);
    }

    public PuntoOrdenDia getAnteriorPuntoOrdenDiaByOrdenYNivel(Long reunionId, Long orden, PuntoOrdenDia padre) {
        BooleanExpression be = qPuntoOrdenDia.reunion.id.eq(reunionId).and(qPuntoOrdenDia.orden.lt(orden));

        be = (padre == null) ?
                be.and(qPuntoOrdenDia.puntoSuperior.isNull()) :
                be.and(qPuntoOrdenDia.puntoSuperior.id.eq(padre.getId()));

        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia = query.from(qPuntoOrdenDia)
                .where(be)
                .orderBy(qPuntoOrdenDia.orden.desc())
                .list(qPuntoOrdenDia);

        if (puntosOrdenDia.size() == 0) {
            return null;
        }
        return puntosOrdenDia.get(0);
    }

    @Transactional
    public void actualizaOrden(Long puntoOrdenDiaId, Long orden) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        update.set(qPuntoOrdenDia.orden, orden).where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId));
        update.execute();
    }

    public PuntoOrdenDia getUltimoPuntoOrdenDiaByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia = query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId))
                .orderBy(qPuntoOrdenDia.orden.desc())
                .list(qPuntoOrdenDia);

        if (puntosOrdenDia.size() == 0) {
            return null;
        }

        return puntosOrdenDia.get(0);
    }

    @Transactional(rollbackFor = PuntoDelDiaConAcuerdosException.class)
    public void deleteByPuntoId(Long id) throws PuntoDelDiaConAcuerdosException {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDiaDocumento);
        deleteClause.where(qPuntoOrdenDiaDocumento.puntoOrdenDia.id.eq(id)).execute();

        deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDiaAcuerdo);
        deleteClause.where(qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(id)).execute();

        deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDiaDescriptor);
        deleteClause.where(qPuntoOrdenDiaDescriptor.puntoOrdenDia.id.eq(id)).execute();

        deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDia);
        try {
            deleteClause.where(qPuntoOrdenDia.id.eq(id)).execute();
        } catch (PersistenceException e) {
            throw new PuntoDelDiaConAcuerdosException();
        }
    }

    @Transactional
    public void incrementaOrdenPuntosSiguientesConMismoPadre(
            Long reunionId,
            Long puntoOrdenDiaAMoverId,
            Long orden
    ) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        updateClause
                .set(qPuntoOrdenDia.orden, qPuntoOrdenDia.orden.add(10L))
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId)
                        .and(qPuntoOrdenDia.orden.goe(orden))
                        .and(qPuntoOrdenDia.id.ne(puntoOrdenDiaAMoverId)))
                .execute();
    }

    public PuntoOrdenDia detach(PuntoOrdenDia puntoOrdenDia) {
        entityManager.detach(puntoOrdenDia);
        puntoOrdenDia.setId(null);
        return puntoOrdenDia;
    }

    @Transactional
    public void iniciaFechaVotacionPuntosAbreviados(Reunion reunion)
            throws NoVotacionTelematicaException {
        if (!reunion.isVotacionTelematica()) throw new NoVotacionTelematicaException();

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        updateClause.set(qPuntoOrdenDia.fechaInicioVotacion, reunion.getAvisoPrimeraReunionFecha())
                .set(qPuntoOrdenDia.votacionAbierta, true)
                .where(qPuntoOrdenDia.reunion.id.eq(reunion.getId())
                        .and(qPuntoOrdenDia.tipoProcedimientoVotacion.eq(TipoProcedimientoVotacionEnum.ABREVIADO.name())))
                .execute();

    }

    @Transactional
    public void actualizadoFechaFinVotacionPuntos(Reunion reunion)
            throws NoVotacionTelematicaException {
        if (!reunion.isVotacionTelematica()) throw new NoVotacionTelematicaException();

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        updateClause.set(qPuntoOrdenDia.fechaFinVotacion, reunion.getFechaFinVotacion())
                .where(qPuntoOrdenDia.reunion.id.eq(reunion.getId()).and(qPuntoOrdenDia.fechaFinVotacion.isNotNull()))
                .execute();

    }

    @Transactional
    public void eliminaFechaFinVotacionPuntos(Reunion reunion)
            throws NoVotacionTelematicaException {
        if (!reunion.isVotacionTelematica()) throw new NoVotacionTelematicaException();

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        updateClause.setNull(qPuntoOrdenDia.fechaFinVotacion)
                .where(qPuntoOrdenDia.reunion.id.eq(reunion.getId()).and(qPuntoOrdenDia.fechaFinVotacion.isNotNull()))
                .execute();

    }

    @Transactional
    public void cierraFechaVotacionPuntos(Reunion reunion)
            throws ReunionNoAbiertaException {
        if (!reunion.isAbierta()) throw new ReunionNoAbiertaException();

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        updateClause
                .set(qPuntoOrdenDia.fechaFinVotacion, new Date())
                .set(qPuntoOrdenDia.votacionAbierta, false)
                .where(qPuntoOrdenDia.reunion.id.eq(reunion.getId()))
                .execute();

    }

    public Long getReunionIdByPuntoId(Long puntoId) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDia).where(qPuntoOrdenDia.id.eq(puntoId)).uniqueResult(qPuntoOrdenDia.reunion.id);
    }

    public List<Long> getPuntosIdByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId))
                .list(qPuntoOrdenDia.id);
    }

    public List<Long> getPuntosPadreIdsByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId)
                        .and(qPuntoOrdenDia.puntoSuperior.isNull())
                )
                .list(qPuntoOrdenDia.id);
    }
}