package es.uji.apps.goc.dto;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import java.io.UnsupportedEncodingException;

@MappedSuperclass
@Component
public class BaseAdjuntos {
    @Column(name = "HASH_STRING")
    private String hash;

    @Lob()
    private byte[] datos;


    @Column(name="NOMBRE_FICHERO")
    private String nombreFichero;

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = encodeStringToUTF(nombreFichero);
    }

    public String encodeStringToUTF(String texto)
    {
        String textoEncoded;
        try {
            textoEncoded = new String(texto.getBytes("iso-8859-1"), "UTF-8");
        } catch(UnsupportedEncodingException e) {
            textoEncoded = texto;
        }
        return textoEncoded;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public byte[] getDatos() {
        return datos;
    }

    public void setDatos(byte[] datos) {
        this.datos = datos;
    }

}