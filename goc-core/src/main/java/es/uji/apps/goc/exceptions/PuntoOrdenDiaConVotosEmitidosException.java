package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class PuntoOrdenDiaConVotosEmitidosException extends CoreDataBaseException
{
    public PuntoOrdenDiaConVotosEmitidosException()
    {
        super("El punto del orden del día tiene votos emitidos");
    }
}
