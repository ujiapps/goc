package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class UsuarioYaHaVotadoSoloSePermiteActualizacionException extends CoreBaseException
{
    public UsuarioYaHaVotadoSoloSePermiteActualizacionException() {
        super("El usuario ya ha realizado un voto en este punto y solo se permite actualizarlo");
    }
}
