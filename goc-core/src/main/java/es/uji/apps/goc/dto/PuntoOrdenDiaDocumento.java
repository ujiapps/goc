package es.uji.apps.goc.dto;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "GOC_P_ORDEN_DIA_DOCUMENTOS")
public class PuntoOrdenDiaDocumento extends BaseAdjuntos implements Serializable, Documentable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String descripcion;

    @Column(name = "DESCRIPCION_ALT")
    private String descripcionAlternativa;

    @Column(name="MIME_TYPE")
    private String mimeType;

    @Column(name="FECHA_ADICION")
    private Date fechaAdicion;

    @Column(name="CREADOR_ID")
    private Long creadorId;

    private Boolean publico;

    @Column(name="MOSTRAR_EN_FICHA")
    private Boolean mostrarEnFicha;
    
    private Long orden;

    @ManyToOne
    @JoinColumn(name = "PUNTO_ID")
    private PuntoOrdenDia puntoOrdenDia;
    
    @Column(name="EDITOR_ID")
    private Long editorId;
    
    @Column (name="FECHA_EDICION")
    private Date fechaEdicion;    
    
    @Column(name="MOTIVO_EDICION")
    private String motivoEdicion;
    
    @Column(name="ACTIVO")
    private Boolean activo;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }


    public Date getFechaAdicion()
    {
        return fechaAdicion;
    }

    public void setFechaAdicion(Date fechaAdicion)
    {
        this.fechaAdicion = fechaAdicion;
    }

    public Long getCreadorId() {
        return creadorId;
    }

    public void setCreadorId(Long creadorId) {
        this.creadorId = creadorId;
    }

    public PuntoOrdenDia getPuntoOrdenDia() {
        return puntoOrdenDia;
    }

    public void setPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia) {
        this.puntoOrdenDia = puntoOrdenDia;
    }

    public Long getEditorId() {
		return editorId;
	}

	public void setEditorId(Long editorId) {
		this.editorId = editorId;
	}

	public Date getFechaEdicion() {
		return fechaEdicion;
	}

	public void setFechaEdicion(Date fechaEdicion) {
		this.fechaEdicion = fechaEdicion;
	}

	public String getMotivoEdicion() {
		return motivoEdicion;
	}

	public void setMotivoEdicion(String motivoEdicion) {
		this.motivoEdicion = motivoEdicion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getDescripcionAlternativa()
    {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa)
    {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public Boolean getPublico()
    {
        return publico;
    }

    public void setPublico(Boolean publico)
    {
        this.publico = publico;
    }

    public Boolean getMostrarEnFicha() {
        return mostrarEnFicha;
    }

    public void setMostrarEnFicha(Boolean mostrarEnFicha) {
        this.mostrarEnFicha = mostrarEnFicha;
    }

    public Boolean isMostrarEnFicha(){
        return this.mostrarEnFicha;
    }

	public Long getOrden() {
		return orden;
	}

	public void setOrden(Long orden) {
		this.orden = orden;
	}
}
