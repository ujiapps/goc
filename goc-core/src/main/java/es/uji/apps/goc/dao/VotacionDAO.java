package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.goc.builder.PersonaPuntoVotoDTOBuilder;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.CodigoCargoEnum;
import es.uji.apps.goc.enums.TipoRecuentoVotoEnum;
import es.uji.apps.goc.enums.TipoVotacionEnum;
import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.TipoRecuentoVotoNoExisteException;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.PersonaPuntoVoto;
import es.uji.apps.goc.model.PuntoVotoPrivado;
import es.uji.apps.goc.model.VotantePrivado;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class VotacionDAO extends BaseDAODatabaseImpl {
    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private OrganoReunionMiembroDAO organoReunionMiembroDAO;

    private QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;
    private QReunion qReunion = QReunion.reunion;
    private QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    private QPersonaPuntoVotoDTO qPersonaPuntoVoto = QPersonaPuntoVotoDTO.personaPuntoVotoDTO;
    private QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
    private QVotoDTO qVotoDTO = QVotoDTO.votoDTO;
    private QPuntoSecretoVotosDTO qPuntoSecretoVotosDTO = QPuntoSecretoVotosDTO.puntoSecretoVotosDTO;
    private JPAUpdateClause updateClause;

    public PersonaPuntoVotoDTO votarPunto(PersonaPuntoVoto personaPuntoVoto) {
        PersonaPuntoVotoDTO personaPuntoVotoDTO =
                new PersonaPuntoVotoDTOBuilder(personaPuntoVoto)
                        .build();

        return insert(personaPuntoVotoDTO);
    }

    @Transactional
    public PuntoVotoPrivadoDTO votarPuntoPrivado(PuntoVotoPrivado puntoVotoPrivado, VotantePrivado votantePrivado) {
        VotantePrivadoDTO votantePrivadoDTO = new VotantePrivadoDTO(votantePrivado);
        PuntoVotoPrivadoDTO puntoVotoPrivadoDTO = new PuntoVotoPrivadoDTO(puntoVotoPrivado);

        insert(votantePrivadoDTO);
        return insert(puntoVotoPrivadoDTO);
    }

    public Boolean miembroPuedeVotarPunto(Persona persona, Long puntoId) {
        JPAQuery query = new JPAQuery(entityManager);

        Date now = new Date();

        return query.from(qReunion)
                .join(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .join(qOrganoReunionMiembro).on(qOrganoReunionMiembro.reunionId.eq(qReunion.id))
                .leftJoin(qMiembroLocal).on(qMiembroLocal.id.stringValue().eq(qOrganoReunionMiembro.miembroId))
                .where((qOrganoReunionMiembro.email.eq(persona.getEmail())
                        .or(qMiembroLocal.isNotNull().and(qMiembroLocal.personaId.eq(persona.getId()))))
                        .and(qPuntoOrdenDia.id.eq(puntoId))
                        .and(qPuntoOrdenDia.fechaInicioVotacion.isNotNull().and(qPuntoOrdenDia.fechaInicioVotacion.loe(now))
                                .and(qPuntoOrdenDia.fechaFinVotacion.isNull()).or(qPuntoOrdenDia.fechaFinVotacion.goe(now)))
                ).exists();
    }

    public List<PuntoOrdenDiaTemplate> getPuntosOrdenDiaVotosTemplate(
            List<PuntoOrdenDiaTemplate> puntoOrdenDiaTemplates, Long personaId, Long reunionId, Long organoReunionMiembroId
    ) {
        List<Long> puntosIds = puntoOrdenDiaDAO.getPuntosIdByReunionId(reunionId);
        List<VotoDTO> votos = getVotosByMiembroAndPuntosIds(puntosIds, personaId, organoReunionMiembroId);

        setRecuentoVotosPuntosTemplate(puntoOrdenDiaTemplates);
        setListaVotosPuntosTemplate(puntoOrdenDiaTemplates);
        setSubpuntosOrdenDiaVotosTemplate(puntoOrdenDiaTemplates, votos);
        setVotosSecretosPuntosTemplate(puntoOrdenDiaTemplates);
        setVotosSecretosSubPuntosTemplate(puntoOrdenDiaTemplates);

        return puntoOrdenDiaTemplates;
    }

    private void setRecuentoVotosPuntosTemplate(List<PuntoOrdenDiaTemplate> puntos) {
        for (PuntoOrdenDiaTemplate punto : puntos) {
            punto.setRecuentoVotos(getNumVotosByPuntoId(punto.getId()));
        }
    }

    private void setListaVotosPuntosTemplate(List<PuntoOrdenDiaTemplate> puntos) {
        for (PuntoOrdenDiaTemplate punto : puntos) {
            punto.setVotosPunto(getVotosByPuntoId(punto.getId()));
        }
    }

    private void setSubpuntosOrdenDiaVotosTemplate(List<PuntoOrdenDiaTemplate> subpuntos, List<VotoDTO> votos) {
        if (subpuntos != null) {
            for (PuntoOrdenDiaTemplate subpunto : subpuntos) {
                for (VotoDTO voto : votos) {
                    if (voto.getPuntoOrdenDiaId().equals(subpunto.getId())) {
                        subpunto.setVoto(voto.getVoto());
                        subpunto.setVotanteId(voto.getPersonaId());
                    }
                }
                subpunto.setRecuentoVotos(getNumVotosByPuntoId(subpunto.getId()));
                subpunto.setVotosPunto(getVotosByPuntoId(subpunto.getId()));
                subpunto.setVotosPuntoSecreto(getPuntoSecretoVotosByPuntoId(subpunto.getId()));

                setSubpuntosOrdenDiaVotosTemplate(subpunto.getSubpuntos(), votos);
            }
        }
    }

    private void setVotosSecretosPuntosTemplate(List<PuntoOrdenDiaTemplate> puntos) {
        for (PuntoOrdenDiaTemplate punto : puntos) {
            PuntoSecretoVotosDTO puntoSecreto = getPuntoSecretoVotosByPuntoId(punto.getId());
            punto.setVotosPuntoSecreto(puntoSecreto);

            if (puntoSecreto != null) {
                Long recuentoVotos = puntoSecreto.getVotosFavor() + puntoSecreto.getVotosContra() + puntoSecreto.getVotosBlanco();
                punto.setRecuentoVotos(recuentoVotos);
            }
        }
    }

    private void setVotosSecretosSubPuntosTemplate(List<PuntoOrdenDiaTemplate> puntos) {
        for (PuntoOrdenDiaTemplate punto : puntos) {
            List<PuntoOrdenDiaTemplate> subPuntos = punto.getSubpuntos();

            if (subPuntos != null) {
                for (PuntoOrdenDiaTemplate subPunto : subPuntos) {
                    PuntoSecretoVotosDTO subPuntoSecreto = getPuntoSecretoVotosByPuntoId(subPunto.getId());
                    subPunto.setVotosPuntoSecreto(subPuntoSecreto);

                    if (subPuntoSecreto != null) {
                        Long recuentoVotos = subPuntoSecreto.getVotosFavor() + subPuntoSecreto.getVotosContra() + subPuntoSecreto.getVotosBlanco();
                        subPunto.setRecuentoVotos(recuentoVotos);
                    }
                }
            }
        }
    }

    private Long getNumVotosByPuntoId(Long puntoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qVotoDTO)
                .where(qVotoDTO.puntoOrdenDiaId.eq(puntoId)).count();
    }

    private List<PersonaPuntoVotoDTO> getVotosByPuntoId(Long puntoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPersonaPuntoVoto)
                .where(qPersonaPuntoVoto.puntoOrdenDia.id.eq(puntoId)
                ).list(qPersonaPuntoVoto);
    }

    private List<VotoDTO> getVotosByMiembroAndPuntosIds(List<Long> puntosIds, Long personaId, Long organoReunionMiembroId) {
        JPAQuery query = new JPAQuery(entityManager);

        return organoReunionMiembroId != null ?
                query.from(qVotoDTO)
                        .where(qVotoDTO.personaId.eq(personaId)
                                .and(qVotoDTO.puntoOrdenDiaId.in(puntosIds))
                                .and(qVotoDTO.organoReunionMiembroId.eq(organoReunionMiembroId))
                        ).list(qVotoDTO) : new ArrayList<>();
    }

    public boolean hasPersonaVotosEmitidosByReunionId(Long reunionId, Long personaId, Long suplenteId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qVotoDTO)
                .join(qPuntoOrdenDia).on(qPuntoOrdenDia.id.eq(qVotoDTO.puntoOrdenDiaId))
                .where((qVotoDTO.personaId.eq(personaId)
                        .or(qVotoDTO.personaId.eq(suplenteId)))
                        .and(qPuntoOrdenDia.reunion.id.eq(reunionId))
                )
                .count() > 0;
    }


    public boolean hasMiembroVotosEmitidosByPuntoId(Long organoReunionMiembroId, Long puntoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qVotoDTO).
                where(qVotoDTO.puntoOrdenDiaId.eq(puntoId).
                        and(qVotoDTO.organoReunionMiembroId.eq(organoReunionMiembroId))).
                exists();
    }

    public void actualizarVoto(PersonaPuntoVoto personaPuntoVoto) {
        PersonaPuntoVotoDTO personaPuntoVotoDTO =
                new PersonaPuntoVotoDTOBuilder(personaPuntoVoto)
                        .build();

        update(personaPuntoVotoDTO);
    }

    public VotoDTO getVotoByMiembroAndPuntoId(Long miembroId, Long puntoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qVotoDTO)
                .where(qVotoDTO.organoReunionMiembroId.eq(miembroId)
                        .and(qVotoDTO.puntoOrdenDiaId.eq(puntoId))
                ).singleResult(qVotoDTO);
    }

    public PersonaPuntoVotoDTO getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(String miembroId, Long puntoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPersonaPuntoVoto)
                .where(qPersonaPuntoVoto.organoReunionMiembro.miembroId.eq(miembroId)
                        .and(qPersonaPuntoVoto.puntoOrdenDia.id.eq(puntoId))
                ).singleResult(qPersonaPuntoVoto);
    }

    @Transactional
    public PuntoOrdenDiaRecuentoVotosDTO getRecuentoVotosByPuntoId(Long puntoId) throws TipoRecuentoVotoNoExisteException {
        QPuntoOrdenDia qPuntoOrdenDiaInferiores = new QPuntoOrdenDia("qPuntoOrdenDiaInferiores");
        List<Tuple> puntoOrdenDiaVotos = new JPAQuery(entityManager)
                .from(qPuntoOrdenDia).leftJoin(qPuntoOrdenDia.puntosInferiores, qPuntoOrdenDiaInferiores).fetch()
                .where(qPuntoOrdenDia.id.eq(puntoId))
                .list(qPuntoOrdenDia,
                        getRecuentoVotosSubQuery(TipoVotoEnum.FAVOR, puntoId, false).count(),
                        getRecuentoVotosSubQuery(TipoVotoEnum.CONTRA, puntoId, false).count(),
                        getRecuentoVotosSubQuery(TipoVotoEnum.BLANCO, puntoId, false).count(),
                        getRecuentoVotosSubQuery(null, puntoId, false).count()
                );

        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaVotos.get(0).get(qPuntoOrdenDia);

        return new PuntoOrdenDiaRecuentoVotosDTO(
                puntoOrdenDia,
                puntoOrdenDiaVotos.get(0).get(1, Long.class),
                puntoOrdenDiaVotos.get(0).get(2, Long.class),
                puntoOrdenDiaVotos.get(0).get(3, Long.class),
                puntoOrdenDiaVotos.get(0).get(4, Long.class),
                isAprobadoPunto(puntoOrdenDia),
                this.getVotantesNombreByPuntoId(puntoId)
        );
    }

    private List<Tuple> getRecuentoVotosDoblesByPuntoId(Long puntoId) {
        return new JPAQuery(entityManager)
                .from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.id.eq(puntoId))
                .list(
                        getRecuentoVotosSubQuery(TipoVotoEnum.FAVOR, puntoId, true).count(),
                        getRecuentoVotosSubQuery(TipoVotoEnum.CONTRA, puntoId, true).count(),
                        getRecuentoVotosSubQuery(TipoVotoEnum.BLANCO, puntoId, true).count(),
                        getRecuentoVotosSubQuery(null, puntoId, true).count()
                );
    }

    private JPASubQuery getRecuentoVotosSubQuery(TipoVotoEnum tipoVoto, Long puntoId, boolean recuentoVotoDoble) {
        return new JPASubQuery()
                .from(qVotoDTO)
                .where(qVotoDTO.puntoOrdenDiaId.eq(puntoId)
                        .and(tipoVoto != null ? qVotoDTO.voto.eq(tipoVoto.toString()) : null)
                        .and(recuentoVotoDoble ? qVotoDTO.isVotoDoble.eq(true) : null)
                );
    }

    public boolean isAprobadoPunto(PuntoOrdenDia puntoOrdenDia) throws TipoRecuentoVotoNoExisteException {
        puntoOrdenDia.checkValidezTipoRecuento();

        if (puntoOrdenDia.getTipoRecuentoVoto() == null) return false;

        boolean isPuntoSecreto = false;
        if (puntoOrdenDia.getTipoVoto() != null && !puntoOrdenDia.getTipoVoto())
            isPuntoSecreto = true;

        String tipoVotacion = puntoOrdenDia.getTipoVotacion();

        switch (TipoRecuentoVotoEnum.valueOf(puntoOrdenDia.getTipoRecuentoVoto())) {
            case MAYORIA_SIMPLE:
                if (isPuntoSecreto) {
                    if (tipoVotacion.equals(TipoVotacionEnum.TELEMATICA.name())) {
                        return isAprobadoPuntoConMayoriaSimple(puntoOrdenDia.getId());
                    } else {
                        return isAprobadoPuntoSecretoConMayoriaSimple(puntoOrdenDia.getId());
                    }
                } else {
                    return isAprobadoPuntoConMayoriaSimple(puntoOrdenDia.getId());
                }
            case MAYORIA_ABSOLUTA_PRESENTES:
                if (isPuntoSecreto) {
                    if (tipoVotacion.equals(TipoVotacionEnum.TELEMATICA.name())) {
                        return isAprobadoConMayoriaAbsolutaPresentes(puntoOrdenDia.getId());
                    } else {
                        return isAprobadoSecretoConMayoriaAbsolutaPresentes(puntoOrdenDia.getId());
                    }
                } else {
                    return isAprobadoConMayoriaAbsolutaPresentes(puntoOrdenDia.getId());
                }
            case MAYORIA_ABSOLUTA_ORGANO:
                if (isPuntoSecreto) {
                    if (tipoVotacion.equals(TipoVotacionEnum.TELEMATICA.name())) {
                        return isAprobadoConMayoriaAbsolutaOrgano(puntoOrdenDia);
                    } else {
                        return isAprobadoSecretoConMayoriaAbsolutaOrgano(puntoOrdenDia);
                    }
                } else {
                    return isAprobadoConMayoriaAbsolutaOrgano(puntoOrdenDia);
                }
            case MAYORIA_DOS_TERCIOS:
                if (isPuntoSecreto) {
                    if (tipoVotacion.equals(TipoVotacionEnum.TELEMATICA.name())) {
                        return isAprobadoConMayoriaDosTercios(puntoOrdenDia);
                    } else {
                        return isAprobadoSecretoConMayoriaDosTercios(puntoOrdenDia);
                    }
                } else {
                    return isAprobadoConMayoriaDosTercios(puntoOrdenDia);
                }
            default:
                return false;
        }
    }

    @Transactional
    public boolean isAprobadoPuntoConMayoriaSimple(Long puntoOrdenDiaId) {
        Long votosAFavor = getVotosAfavorByPuntoId(puntoOrdenDiaId);
        Long votosEnContra = getVotosEnContraByPuntoId(puntoOrdenDiaId);

        if (votosAFavor == votosEnContra) {
            votosAFavor += getVotosDoblesAfavorByPuntoId(puntoOrdenDiaId);
            votosEnContra += getVotosDoblesEnContraByPuntoId(puntoOrdenDiaId);
        }

        if (votosAFavor > votosEnContra) return true;

        return false;
    }

    @Transactional
    public boolean isAprobadoPuntoSecretoConMayoriaSimple(Long puntoOrdenDiaId) {
        PuntoSecretoVotosDTO puntoSecreto = getPuntoSecretoVotosByPuntoId(puntoOrdenDiaId);

        if (puntoSecreto == null) return false;

        Long votosAFavor = puntoSecreto.getVotosFavor();
        Long votosEnContra = puntoSecreto.getVotosContra();

        if (votosAFavor > votosEnContra) return true;

        return false;
    }

    @Transactional
    public boolean isAprobadoConMayoriaAbsolutaPresentes(Long puntoOrdenDiaId) {
        Long votosAFavor = getVotosAfavorByPuntoId(puntoOrdenDiaId);
        Long votosEmitidos = getVotosEmitidosByPuntoId(puntoOrdenDiaId);

        if (votosAFavor == (votosEmitidos / 2.0) && votosEmitidos > 0) {
            votosAFavor += getVotosDoblesAfavorByPuntoId(puntoOrdenDiaId);
        }

        if (votosAFavor > (votosEmitidos / 2.0) && votosEmitidos > 0)
            return true;

        return false;
    }

    @Transactional
    public boolean isAprobadoSecretoConMayoriaAbsolutaPresentes(Long puntoOrdenDiaId) {
        PuntoSecretoVotosDTO puntoSecreto = getPuntoSecretoVotosByPuntoId(puntoOrdenDiaId);

        if (puntoSecreto == null) return false;

        Long votosAFavor = puntoSecreto.getVotosFavor();
        Long votosEmitidos = votosAFavor + puntoSecreto.getVotosContra() + puntoSecreto.getVotosBlanco();

        if (votosAFavor > (votosEmitidos / 2.0) && votosEmitidos > 0)
            return true;

        return false;
    }

    @Transactional
    public boolean isAprobadoConMayoriaAbsolutaOrgano(PuntoOrdenDia puntoOrdenDia) {
        Reunion reunion =
                reunionDAO.getReunionConMiembrosAndPuntosDiaById(puntoOrdenDia.getReunion().getId());

        Integer numMiembros = 0;
        for (OrganoReunion reunionOrgano : reunion.getReunionOrganos()) {
            numMiembros += reunionOrgano.getMiembros().size();
        }

        Long votosAFavor = getVotosAfavorByPuntoId(puntoOrdenDia.getId());

        if (votosAFavor == (numMiembros / 2.0) && numMiembros > 0) {
            votosAFavor += getVotosDoblesAfavorByPuntoId(puntoOrdenDia.getId());
        }

        if (votosAFavor > (numMiembros / 2.0) && numMiembros > 0)
            return true;

        return false;
    }

    @Transactional
    public boolean isAprobadoSecretoConMayoriaAbsolutaOrgano(PuntoOrdenDia puntoOrdenDia) {
        Reunion reunion =
                reunionDAO.getReunionConMiembrosAndPuntosDiaById(puntoOrdenDia.getReunion().getId());

        PuntoSecretoVotosDTO puntoSecreto = getPuntoSecretoVotosByPuntoId(puntoOrdenDia.getId());

        if (puntoSecreto == null) return false;

        Integer numMiembros = 0;
        for (OrganoReunion reunionOrgano : reunion.getReunionOrganos()) {
            numMiembros += reunionOrgano.getMiembros().size();
        }

        Long votosAFavor = puntoSecreto.getVotosFavor();

        if (votosAFavor > (numMiembros / 2.0) && numMiembros > 0)
            return true;

        return false;
    }

    @Transactional
    public boolean isAprobadoConMayoriaDosTercios(PuntoOrdenDia puntoOrdenDia) {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(puntoOrdenDia.getReunion().getId());
        Integer numMiembros = 0;

        for (OrganoReunion reunionOrgano : reunion.getReunionOrganos()) {
            numMiembros += reunionOrgano.getMiembros().size();
        }

        Long votosAFavor = getVotosAfavorByPuntoId(puntoOrdenDia.getId());

        double cantidadVotosNecesarios = Math.ceil(numMiembros * (2d / 3d));

        return votosAFavor >= cantidadVotosNecesarios;
    }

    @Transactional
    public boolean isAprobadoSecretoConMayoriaDosTercios(PuntoOrdenDia puntoOrdenDia) {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(puntoOrdenDia.getReunion().getId());
        PuntoSecretoVotosDTO puntoSecreto = getPuntoSecretoVotosByPuntoId(puntoOrdenDia.getId());
        Integer numMiembros = 0;

        if (puntoSecreto == null) return false;

        for (OrganoReunion reunionOrgano : reunion.getReunionOrganos()) {
            numMiembros += reunionOrgano.getMiembros().size();
        }

        Long votosAFavor = puntoSecreto.getVotosFavor();

        double cantidadVotosNecesarios = Math.ceil(numMiembros * (2d / 3d));

        return votosAFavor >= cantidadVotosNecesarios;
    }

    public Long getVotosAfavorByPuntoId(Long puntoOrdenDiaId) {
        return getVotosAfavorByPuntoId(puntoOrdenDiaId, false);
    }

    public Long getVotosDoblesAfavorByPuntoId(Long puntoOrdenDiaId) {
        return getVotosAfavorByPuntoId(puntoOrdenDiaId, true);
    }

    public Long getVotosAfavorByPuntoId(Long puntoOrdenDiaId, boolean recuentoVotoDoble) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qVotoDTO)
                .where(qVotoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId)
                        .and(qVotoDTO.voto.eq(TipoVotoEnum.FAVOR.toString()))
                        .and(recuentoVotoDoble ? qVotoDTO.isVotoDoble.eq(true) : null)
                ).count();
    }

    public Long getVotosEnContraByPuntoId(Long puntoOrdenDiaId) {
        return getVotosEnContraByPuntoId(puntoOrdenDiaId, false);
    }

    public Long getVotosDoblesEnContraByPuntoId(Long puntoOrdenDiaId) {
        return getVotosEnContraByPuntoId(puntoOrdenDiaId, true);
    }

    public Long getVotosEnContraByPuntoId(Long puntoOrdenDiaId, boolean recuentoVotoDoble) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qVotoDTO)
                .where(qVotoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId)
                        .and(qVotoDTO.voto.eq(TipoVotoEnum.CONTRA.toString()))
                        .and(recuentoVotoDoble ? qVotoDTO.isVotoDoble.eq(true) : null)
                ).count();
    }

    public Long getVotosEmitidosByPuntoId(Long puntoOrdenDiaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qVotoDTO)
                .where(qVotoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId))
                .count();
    }

    public List<VotanteVoto> getVotantesNombreByPuntoId(Long puntoId) {
        List<VotanteVoto> votantesTitularesSinSuplentes = getVotantesTitularesNombresSinSuplentesByPuntoId(puntoId);
        List<VotanteVoto> votantesSuplentes = getVotantesSuplentesNombresByPuntoId(puntoId);

        return Stream.concat(votantesTitularesSinSuplentes.stream(), votantesSuplentes.stream()).collect(Collectors.toList());
    }

    private List<VotanteVoto> getVotantesTitularesNombresSinSuplentesByPuntoId(Long puntoId) {
        return new JPAQuery(entityManager).from(qOrganoReunionMiembro).leftJoin(qVotoDTO)
                .on(qOrganoReunionMiembro.id.eq(qVotoDTO.organoReunionMiembroId))
                .where(qVotoDTO.puntoOrdenDiaId.eq(puntoId).and(qOrganoReunionMiembro.suplenteId.isNull()))
                .list(qOrganoReunionMiembro.nombre, qOrganoReunionMiembro.delegadoVotoNombre, qVotoDTO.voto).stream().map(nombres -> {
                    String titular = nombres.get(qOrganoReunionMiembro.nombre);
                    String delegado = nombres.get(qOrganoReunionMiembro.delegadoVotoNombre);
                    String voto = nombres.get(qVotoDTO.voto);

                    if (delegado != null) {
                        return new VotanteVoto(titular, delegado, voto);
                    } else {
                        return new VotanteVoto(titular, voto);
                    }
                }).collect(Collectors.toList());
    }

    private List<VotanteVoto> getVotantesSuplentesNombresByPuntoId(Long puntoId) {
        return new JPAQuery(entityManager).from(qOrganoReunionMiembro).leftJoin(qVotoDTO)
                .on(qOrganoReunionMiembro.id.eq(qVotoDTO.organoReunionMiembroId))
                .where(qVotoDTO.puntoOrdenDiaId.eq(puntoId).and(qOrganoReunionMiembro.suplenteId.isNotNull()))
                .list(qOrganoReunionMiembro.suplenteNombre, qVotoDTO.voto).stream().map(votante -> {
                    String nombre = votante.get(qOrganoReunionMiembro.suplenteNombre);
                    String voto = votante.get(qVotoDTO.voto);
                    return new VotanteVoto(nombre, voto);
                }).collect(Collectors.toList());
    }

    @Transactional
    public void actualizaVotacionAbiertaDelPunto(Long puntoId, boolean votacionAbierta) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        updateClause.set(qPuntoOrdenDia.votacionAbierta, votacionAbierta)
                .set(qPuntoOrdenDia.votacionCerradaManualmente, !votacionAbierta)
                .where(qPuntoOrdenDia.id.eq(puntoId))
                .execute();
    }

    public List<OrganoReunionMiembro> getMiembrosNatoPorVotarPunto(Long puntoId) {
        Long reunionId = puntoOrdenDiaDAO.getReunionIdByPuntoId(puntoId);

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where((qOrganoReunionMiembro.nato.isTrue())
                        .or(qOrganoReunionMiembro.cargoCodigo.eq(CodigoCargoEnum.PRESIDENTE.codigo.toString())
                                .or(qOrganoReunionMiembro.cargoCodigo.eq(CodigoCargoEnum.SECRETARIO.codigo.toString()))
                        )
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                        .and(qOrganoReunionMiembro.id.notIn(
                                new JPASubQuery()
                                        .from(qVotoDTO)
                                        .where(qVotoDTO.puntoOrdenDiaId.eq(puntoId))
                                        .list(qVotoDTO.organoReunionMiembroId)
                        ))
                        .and(qOrganoReunionMiembro.asistenciaConfirmada.eq(true)
                                .or(qOrganoReunionMiembro.asistenciaConfirmada.isNull())
                        )
                )
                .list(qOrganoReunionMiembro);
    }

    public PuntoSecretoVotosDTO getPuntoSecretoVotosByPuntoId(Long puntoId) {
        return new JPAQuery(entityManager)
                .from(qPuntoSecretoVotosDTO)
                .where(qPuntoSecretoVotosDTO.puntoOrdenDia.id.eq(puntoId))
                .singleResult(qPuntoSecretoVotosDTO);
    }

    public void insertPuntoSecretoVotos(PuntoSecretoVotosDTO puntoSecretoVotosDTO) {
        insert(puntoSecretoVotosDTO);
    }

    public void updatePuntoSecretoVotos(PuntoSecretoVotosDTO puntoSecretoVotosDTO) {
        update(puntoSecretoVotosDTO);
    }

}
