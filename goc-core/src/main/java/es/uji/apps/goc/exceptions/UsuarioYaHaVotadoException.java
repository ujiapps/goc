package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class UsuarioYaHaVotadoException extends CoreBaseException
{
    public UsuarioYaHaVotadoException() {
        super("El usuario ya ha emitido un voto");
    }
}
