package es.uji.apps.goc.exceptions;

import es.uji.apps.goc.dto.Reunion;
import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoAdmiteVotacionYAceptaCambioDeVotoException extends CoreBaseException
{
    public NoAdmiteVotacionYAceptaCambioDeVotoException() {
        super("La reunión no es telemática y acepta cambio de votación");
    }

    public static void check(Reunion reunion) throws NoAdmiteVotacionYAceptaCambioDeVotoException {
        boolean hasVotacion = reunion.isVotacionTelematica() != null && reunion.isVotacionTelematica();
        boolean hasAdmiteCambioVotacion = reunion.getAdmiteCambioVoto() == true;

        if (!hasVotacion && hasAdmiteCambioVotacion) {
            throw new NoAdmiteVotacionYAceptaCambioDeVotoException();
        }
    }
}
