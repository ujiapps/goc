package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ReunionNoAdmiteCambioVotoException extends CoreBaseException
{
    public ReunionNoAdmiteCambioVotoException() {
        super("La reunión no admite cambio de voto");
    }
}
