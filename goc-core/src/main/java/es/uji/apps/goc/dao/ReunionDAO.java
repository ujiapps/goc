package es.uji.apps.goc.dao;

import com.google.common.base.Strings;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.StringExpression;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ReunionDAO extends BaseDAODatabaseImpl {

    public static final String FUNCTION_TRANSLATE = "function('translate', {0}, 'ÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöü', 'AEIOUAEIOUAEIOUAEIOUAEIOUAEIOU')";

    @Value("${goc.reunion.mostrarPublicarAcuerdos:false}")
    public boolean mostrarPublicarAcuerdos;

    private QReunion qReunion = QReunion.reunion;
    private QReunionBusqueda qReunionBusqueda = QReunionBusqueda.reunionBusqueda;
    private QReunionEditor qReunionEditor = QReunionEditor.reunionEditor;
    private QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;
    private QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    private QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;
    private QOrganoReunionInvitado qOrganoReunionInvitado = QOrganoReunionInvitado.organoReunionInvitado;
    private QOrganoInvitado qOrganoInvitado = QOrganoInvitado.organoInvitado;
    private QOrganoParametro qOrganoParametro = QOrganoParametro.organoParametro;
    private QPuntoOrdenDiaDescriptor qPuntoOrdenDiaDescriptor = QPuntoOrdenDiaDescriptor.puntoOrdenDiaDescriptor;
    private QDescriptor qDescriptor = QDescriptor.descriptor1;
    private QClave qClave = QClave.clave1;
    private QReunionComentario qReunionComentario = QReunionComentario.reunionComentario;
    private QReunionDocumento qReunionDocumento = QReunionDocumento.reunionDocumento;
    private QReunionInvitado qReunionInvitado = QReunionInvitado.reunionInvitado;
    private QReunionPermiso qReunionPermiso = QReunionPermiso.reunionPermiso;
    private QReunionHojaFirmas qReunionHojaFirmas = QReunionHojaFirmas.reunionHojaFirmas;
    private QOrganoAutorizado qOrganoAutorizado = QOrganoAutorizado.organoAutorizado;
    private QReunionDiligencia qReunionDiligencia = QReunionDiligencia.reunionDiligencia;
    private QPersonaPuntoVotoDTO qPersonaPuntoVoto = QPersonaPuntoVotoDTO.personaPuntoVotoDTO;

    public Long getReunionesByEditorIdTotal(Long connectedUserId, Boolean completada, Long tipoOrganoId, String organoId,
                                            Boolean externo, String querySearch) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = getReunionesWhereExpression(completada, tipoOrganoId, organoId, externo, querySearch);

        return query.from(qReunionEditor)
                .join(qReunionEditor.reunion, qReunion)
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion)
                .where(qReunionEditor.editorId.eq(connectedUserId)
                        .and(where))
                .distinct()
                .count();
    }

    public List<ReunionEditor> getReunionesByEditorIdList(Long connectedUserId, Boolean completada, Long tipoOrganoId, String organoId,
                                                          Boolean externo, String querySearch, Integer start, Integer limit) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = getReunionesWhereExpression(completada, tipoOrganoId, organoId, externo, querySearch);

        List<Tuple> reuniones = query.from(qReunionEditor)
                .join(qReunionEditor.reunion, qReunion)
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion)
                .where(qReunionEditor.editorId.eq(connectedUserId)
                        .and(where))
                .offset(start)
                .limit(limit)
                .distinct()
                .orderBy(qReunion.fecha.desc())
                .list(qReunion.id, qReunion.asunto, qReunion.asuntoAlternativo, qReunion.fecha, qReunion.duracion, qReunion.completada,
                        qReunion.avisoPrimeraReunion, qReunion.avisoPrimeraReunionUser, qReunion.avisoPrimeraReunionFecha, qReunion.votacionTelematica,
                        qReunionEditor.numeroDocumentos);

        return getReunionEditors(reuniones);
    }

    public Long getReunionesByAdminTotal(Boolean completada, Long tipoOrganoId,
                                         String organoId, Boolean externo, String querySearch) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = getReunionesWhereExpression(completada, tipoOrganoId, organoId, externo, querySearch);

        return query.from(qReunion)
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion)
                .where(where)
                .distinct()
                .count();
    }

    public List<ReunionEditor> getReunionesByAdminList(Boolean completada, Long tipoOrganoId,
                                                       String organoId, Boolean externo, String querySearch, Integer start, Integer limit) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = getReunionesWhereExpression(completada, tipoOrganoId, organoId, externo, querySearch);

        List<Tuple> reuniones = query.from(qReunion)
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion)
                .where(where)
                .offset(start)
                .limit(limit)
                .distinct()
                .orderBy(qReunion.fecha.desc())
                .list(qReunion.id, qReunion.asunto, qReunion.asuntoAlternativo, qReunion.fecha, qReunion.duracion, qReunion.completada,
                        qReunion.avisoPrimeraReunion, qReunion.avisoPrimeraReunionUser, qReunion.avisoPrimeraReunionFecha, qReunion.votacionTelematica,
                        new JPASubQuery().from(qReunionDocumento)
                                .where(qReunionDocumento.reunion.id.eq(qReunion.id))
                                .count());

        return getReunionEditors(reuniones);
    }

    private List<ReunionEditor> getReunionEditors(List<Tuple> reuniones) {
        List<ReunionEditor> reunionesConOrganos = new ArrayList<>();

        for (Tuple tuple : reuniones) {

            ReunionEditor reunionEditor = new ReunionEditor();

            reunionEditor.setId(tuple.get(qReunion.id));
            reunionEditor.setAsunto(tuple.get(qReunion.asunto));
            reunionEditor.setAsuntoAlternativo(tuple.get(qReunion.asuntoAlternativo));
            reunionEditor.setFecha(tuple.get(qReunion.fecha));
            reunionEditor.setDuracion(tuple.get(qReunion.duracion));
            reunionEditor.setCompletada(tuple.get(qReunion.completada));
            reunionEditor.setAvisoPrimeraReunion(tuple.get(qReunion.avisoPrimeraReunion));
            reunionEditor.setAvisoPrimeraReunionUser(tuple.get(qReunion.avisoPrimeraReunionUser));
            reunionEditor.setAvisoPrimeraReunionFecha(tuple.get(qReunion.avisoPrimeraReunionFecha));
            reunionEditor.setVotacionTelematica(tuple.get(qReunion.votacionTelematica));
            reunionEditor.setNumeroDocumentos(tuple.get(10, Long.class));

            reunionesConOrganos.add(reunionEditor);
        }

        return reunionesConOrganos;
    }

    private BooleanExpression getReunionesWhereExpression(Boolean completada, Long tipoOrganoId, String organoId, Boolean externo, String querySearch) {
        BooleanExpression where = qReunion.completada.eq(completada);

        if (tipoOrganoId != null) {
            where = where.and(qOrganoReunion.tipoOrganoId.eq(tipoOrganoId));
        }

        if (organoId != null) {
            where = where.and(qOrganoReunion.organoId.eq(organoId));
        }

        if (externo != null) {
            where = where.and(qOrganoReunion.externo.eq(externo));
        }

        StringExpression asuntoLimpio = Expressions.stringTemplate(FUNCTION_TRANSLATE, qReunion.asunto);
        StringExpression organoNombreLimpio = Expressions.stringTemplate(FUNCTION_TRANSLATE, qOrganoReunion.organoNombre);

        if (querySearch != null && !querySearch.isEmpty()) {
            where = where.and(asuntoLimpio.containsIgnoreCase(querySearch)
                    .or(qPuntoOrdenDia.titulo.containsIgnoreCase(querySearch))
                    .or(qPuntoOrdenDia.acuerdos.containsIgnoreCase(querySearch))
                    .or(qPuntoOrdenDia.deliberaciones.containsIgnoreCase(querySearch))
                    .or(organoNombreLimpio.containsIgnoreCase(querySearch)));
        }
        return where;
    }

    public ReunionEditor getReunionByIdAndEditorId(Long reunionId, Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunionEditor)
                .where((qReunionEditor.editorId.eq(connectedUserId)).and(qReunionEditor.id.eq(reunionId)))
                .uniqueResult(qReunionEditor);
    }

    public Reunion getReunionConOrganosById(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<Reunion> reuniones = query.from(qReunion)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion).fetch()
                .leftJoin(qOrganoReunion.miembros, qOrganoReunionMiembro).fetch()
                .where(qReunion.id.eq(reunionId))
                .list(qReunion);

        if (reuniones.size() == 0) {
            return null;
        }

        return reuniones.get(0);
    }

    @Transactional
    public void marcarReunionComoCompletadaYActualizarAcuerdoYUrl(Long reunionId, Long responsableActaId,
                                                                  String acuerdos, String acuerdosAlternativos, RespuestaFirma respuestaFirma) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);
        update.set(qReunion.completada, true)
                .set(qReunion.fechaCompletada, new Date())
                .set(qReunion.miembroResponsableActa.id, responsableActaId)
                .set(qReunion.acuerdos, acuerdos)
                .set(qReunion.acuerdosAlternativos, acuerdosAlternativos)
                .set(qReunion.urlActa, respuestaFirma.getUrlActa())
                .set(qReunion.urlActaAlternativa, respuestaFirma.getUrlActaAlternativa())
                .where(qReunion.id.eq(reunionId));
        update.execute();
    }

    @Transactional
    public Reunion getReunionById(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<Reunion> reuniones = query.from(qReunion).where(qReunion.id.eq(reunionId)).list(qReunion);

        System.out.println(reuniones);

        if (reuniones.size() == 0) {
            return null;
        }

        return reuniones.get(0);
    }

    public Reunion getReunionConMiembrosAndPuntosDiaById(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<Reunion> reuniones = query.from(qReunion)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion)
                .fetch()
                .leftJoin(qOrganoReunion.miembros, qOrganoReunionMiembro)
                .fetch()
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .fetch()
                .where(qReunion.id.eq(reunionId))
                .list(qReunion);

        if (reuniones.size() == 0) {
            return null;
        }

        return reuniones.get(0);
    }

    public List<Reunion> getPendientesNotificacion(Date fecha) {
        JPAQuery query = new JPAQuery(entityManager);

        Date now = new Date();
        return query.from(qReunion)
                .where(qReunion.notificada.ne(true).and(qReunion.fecha.after(now)).and(qReunion.fecha.before(fecha)))
                .list(qReunion);
    }

    public Long getTodasReunionesAbiertasAccesiblesDePersonaIdCount(Long connectedUserId, Boolean mostrarTodas) {
        List<Tuple> list = getTodasReunionesQueryExpresion(connectedUserId, true, mostrarTodas, null, null, null, null);

        return (long) list.size();
    }

    public List<ReunionPermiso> getTodasReunionesAbiertasAccesiblesDePersonaIdList(Long connectedUserId, Boolean mostrarTodas, Integer startSearch, Integer numResults) {
        List<Tuple> tuples = getTodasReunionesQueryExpresion(connectedUserId, true, mostrarTodas, null, null, startSearch, numResults);

        List<ReunionPermiso> reuniones = new ArrayList<>();

        for (Tuple tuple : tuples) {
            ReunionPermiso reunion = new ReunionPermiso();

            reunion.setId(tuple.get(qReunionPermiso.id));
            reunion.setAsunto(tuple.get(qReunionPermiso.asunto));
            reunion.setAsuntoAlternativo(tuple.get(qReunionPermiso.asuntoAlternativo));
            reunion.setFecha(tuple.get(qReunionPermiso.fecha));
            reunion.setUrlActa(tuple.get(qReunionPermiso.urlActa));
            reunion.setUrlActaAlternativa(tuple.get(qReunionPermiso.urlActaAlternativa));
            reunion.setUrlAsistencia(tuple.get(qReunionPermiso.urlAsistencia));
            reunion.setUrlAsistenciaAlternativa(tuple.get(qReunionPermiso.urlAsistenciaAlternativa));

            JPAQuery queryOrganos = new JPAQuery(entityManager);
            List<OrganoReunion> organos = queryOrganos.from(qOrganoReunion)
                    .where(qOrganoReunion.reunion.id.eq(tuple.get(qReunionPermiso.id)))
                    .list(qOrganoReunion);

            reunion.setOrganoNombre(organos.stream().map(OrganoReunion::getOrganoNombre).collect(Collectors.joining(", ")));
            reunion.setOrganoNombreAlt(organos.stream().map(OrganoReunion::getOrganoNombreAlternativo).collect(Collectors.joining(", ")));

            JPAQuery queryPermisos = new JPAQuery(entityManager);
            List<ReunionPermiso> permisos = queryPermisos.from(qReunionPermiso)
                    .where(qReunionPermiso.id.eq(tuple.get(qReunionPermiso.id))
                            .and(qReunionPermiso.personaId.eq(connectedUserId)))
                    .list(qReunionPermiso);

            //Producen duplicados en el distinct
            reunion.setEnlaceActaProvisionalActivo(permisos.stream().anyMatch(ReunionPermiso::getEnlaceActaProvisionalActivo));
            reunion.setAsistente(permisos.stream().filter(Objects::nonNull).anyMatch(ReunionPermiso::getAsistente));

            reuniones.add(reunion);
        }

        return reuniones;
    }

    private List<Tuple> getTodasReunionesQueryExpresion(Long connectedUserId, Boolean mostrarAbiertas, Boolean mostrarTodas, Long organoId, Boolean organoExterno, Integer startSearch, Integer numResults) {
        JPAQuery query = new JPAQuery(entityManager);
        BooleanExpression where = qReunionPermiso.personaId.eq(connectedUserId);

        if (!mostrarTodas) {
            where = where.and(qReunionPermiso.verReunionSinConvocar.isTrue()
                    .or(qReunionPermiso.avisoPrimeraReunion.isTrue()));
        }

        if (mostrarAbiertas) {
            where = where.and(qReunionPermiso.completada.isFalse());
        } else {
            where = where.and(qReunionPermiso.completada.isTrue());
        }

        if (organoId != null) {
            where = where.and(qReunionPermiso.organoId.eq(organoId).and(qReunionPermiso.organoExterno.eq(organoExterno)));
        }

        query = query.from(qReunionPermiso)
                .where(where);

        if (startSearch != null) {
            query = query.offset(startSearch)
                    .limit(numResults);
        }

        return query
                .orderBy(qReunionPermiso.fecha.desc())
                .distinct()
                .list(qReunionPermiso.id, qReunionPermiso.asunto, qReunionPermiso.asuntoAlternativo, qReunionPermiso.fecha,
                        qReunionPermiso.urlActa, qReunionPermiso.urlActaAlternativa,
                        qReunionPermiso.urlAsistencia, qReunionPermiso.urlAsistenciaAlternativa);
    }

    public Long getTodasReunionesCerradasAccesiblesDePersonaIdAndOrganoCount(Long connectedUserId, Long organoId, Boolean organoExterno) {
        List<Tuple> list = getTodasReunionesQueryExpresion(connectedUserId, false, true, organoId, organoExterno, null, null);

        return (long) list.size();
    }

    public List<ReunionPermiso> getTodasReunionesCerradasAccesiblesDePersonaIdAndOrganoList(Long connectedUserId, Long organoId, Boolean organoExterno, Integer startSearch, Integer numResults) {
        List<Tuple> tuples = getTodasReunionesQueryExpresion(connectedUserId, false, true, organoId, organoExterno, startSearch, numResults);

        List<ReunionPermiso> reuniones = new ArrayList<>();

        for (Tuple tuple : tuples) {
            ReunionPermiso reunion = new ReunionPermiso();

            reunion.setId(tuple.get(qReunionPermiso.id));
            reunion.setAsunto(tuple.get(qReunionPermiso.asunto));
            reunion.setAsuntoAlternativo(tuple.get(qReunionPermiso.asuntoAlternativo));
            reunion.setFecha(tuple.get(qReunionPermiso.fecha));
            reunion.setUrlActa(tuple.get(qReunionPermiso.urlActa));
            reunion.setUrlActaAlternativa(tuple.get(qReunionPermiso.urlActaAlternativa));
            reunion.setUrlAsistencia(tuple.get(qReunionPermiso.urlAsistencia));
            reunion.setUrlAsistenciaAlternativa(tuple.get(qReunionPermiso.urlAsistenciaAlternativa));

            JPAQuery queryPermisos = new JPAQuery(entityManager);
            List<ReunionPermiso> permisos = queryPermisos.from(qReunionPermiso)
                    .where(qReunionPermiso.id.eq(tuple.get(qReunionPermiso.id))
                            .and(qReunionPermiso.personaId.eq(connectedUserId)))
                    .list(qReunionPermiso);

            //Producen duplicados en el distinct
            reunion.setEnlaceActaProvisionalActivo(permisos.stream().anyMatch(ReunionPermiso::getEnlaceActaProvisionalActivo));
            reunion.setAsistente(permisos.stream().filter(Objects::nonNull).anyMatch(ReunionPermiso::getAsistente));

            reuniones.add(reunion);
        }

        return reuniones;
    }

    public List<OrganosPermisosWrapper> getTodosOrganosReunionesCerradasAccesiblesDePersonaId(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<Tuple> tuples = query.from(qReunionPermiso)
                .where(qReunionPermiso.personaId.eq(connectedUserId)
                        .and(qReunionPermiso.completada.isTrue()))
                .distinct()
                .list(qReunionPermiso.organoId, qReunionPermiso.organoExterno);

        List<OrganosPermisosWrapper> organos = new ArrayList<>();

        for (Tuple tuple : tuples) {
            organos.add(new OrganosPermisosWrapper(tuple.get(qReunionPermiso.organoId), tuple.get(qReunionPermiso.organoExterno)));
        }

        return organos;
    }

    public List<ReunionPermiso> getReunionesAccesiblesConvocadasDePersonaId(Long connectedUserId, Boolean mostrarAbiertas) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = qReunionPermiso.personaId.eq(connectedUserId).and(qReunionPermiso.avisoPrimeraReunion.isTrue());

        if (mostrarAbiertas != null) {
            if (mostrarAbiertas) {
                where = where.and(qReunionPermiso.completada.isFalse());
            } else {
                where = where.and(qReunionPermiso.completada.isTrue());
            }
        }

        return query.from(qReunionPermiso)
                .where(where)
                .orderBy(qReunionPermiso.fecha.desc())
                .list(qReunionPermiso);
    }

    public String getNombreAsistente(Long reunionId, Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunionPermiso)
                .where(qReunionPermiso.personaId.eq(connectedUserId)
                        .and(qReunionPermiso.id.eq(reunionId))
                        .and(qReunionPermiso.asistente.isTrue()))
                .uniqueResult(qReunionPermiso.personaNombre);
    }

    public Boolean tieneAcceso(Long reunionId, Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        ReunionPermiso acceso = query.from(qReunionPermiso)
                .where((qReunionPermiso.personaId.eq(connectedUserId).and(qReunionPermiso.id.eq(reunionId))).or(qReunionPermiso.responsableVotoId.eq(connectedUserId)).and(qReunionPermiso.id.eq(reunionId)))
                .uniqueResult(qReunionPermiso);

        return (acceso != null);
    }

    public List<Integer> getAnyosConReunionesPublicas() {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunion)
                .join(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .where(qReunion.publica.isTrue()
                        .and((qReunion.completada.isTrue()).or(qPuntoOrdenDia.urlActa.isNotNull()))
                        .and(qPuntoOrdenDia.publico.isTrue()))
                .distinct()
                .orderBy(qReunion.fecha.year().desc())
                .list(qReunion.fecha.year());
    }

    public List<Long> getIdsReunionesPublicas(Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression whereAnyo = null;

        if (anyo != null) {
            whereAnyo = qReunion.fecha.year().eq(anyo);
        }

        return query.from(qReunion)
                .where(qReunion.publica.isTrue()
                        .and(qReunion.completada.isTrue().or(qReunion.urlActa.isNotNull()))
                        .and(whereAnyo))
                .list(qReunion.id);
    }

    public Long getReunionesPublicas(Long tipoOrganoId, Long organoId, Long descriptorId, Long claveId, Integer anyo,
                                     Date fInicio, Date fFin, String texto, Boolean idiomaAlternativo) {
        return getQueryReunionesPublicas(tipoOrganoId, organoId, descriptorId, claveId, anyo, fInicio, fFin, texto,
                idiomaAlternativo).distinct().count();
    }

    @Transactional
    public BuscadorReunionesWrapper getReunionesPublicasPaginated(Long tipoOrganoId, Long organoId, Long descriptorId,
                                                                  Long claveId, Integer anyo, Date fInicio, Date fFin, String texto, Boolean idiomaAlternativo,
                                                                  Integer startSeach, Integer numResults) {
        JPAQuery reunionesQuery =
                getQueryReunionesPublicas(tipoOrganoId, organoId, descriptorId, claveId, anyo, fInicio, fFin, texto,
                        idiomaAlternativo);
        JPAQuery reunionesQueryCount = reunionesQuery;

        List<Tuple> idsReuniones = reunionesQuery.orderBy(qReunion.fecha.desc())
                .distinct()
                .offset(startSeach)
                .limit(numResults)
                .list(new QTuple(qReunion.id, qReunion.fecha));

        Long numeroReunionesPublicasTotales = reunionesQueryCount.count();

        JPAQuery query = new JPAQuery(entityManager);
        List<Reunion> reuniones = query.from(qReunion)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion)
                .fetch()
                .where(qReunion.id.in(idsReuniones.stream().map(i -> i.get(qReunion.id)).collect(Collectors.toList())))
                .orderBy(qReunion.fecha.desc())
                .list(qReunion);

        return new BuscadorReunionesWrapper(reuniones, numeroReunionesPublicasTotales);
    }

    private JPAQuery getQueryReunionesPublicas(Long tipoOrganoId, Long organoId, Long descriptorId, Long claveId,
                                               Integer anyo, Date fInicio, Date fFin, String texto, Boolean idiomaAlternativo) {
        BooleanExpression whereAnyo = null;
        BooleanExpression whereOrgano = null;
        BooleanExpression whereTipoOrgano = null;
        BooleanExpression whereDescriptor = null;
        BooleanExpression whereClave = null;

        if (anyo != null) {
            whereAnyo = qReunion.fecha.year().eq(anyo);
        }

        if (organoId != null) {
            whereOrgano = qOrganoReunion.organoId.eq(String.valueOf(organoId));
        }

        if (tipoOrganoId != null) {
            whereTipoOrgano = qOrganoReunion.tipoOrganoId.eq(tipoOrganoId);
        }

        if (claveId != null) {
            whereClave = qClave.id.eq(claveId);
        }

        if (descriptorId != null) {
            whereDescriptor = qDescriptor.id.eq(descriptorId);
        }

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qReunion)
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion);

        if (claveId != null || descriptorId != null) {
            query.join(qPuntoOrdenDia.puntoOrdenDiaDescriptores, qPuntoOrdenDiaDescriptor)
                    .join(qPuntoOrdenDiaDescriptor.clave, qClave)
                    .leftJoin(qClave.descriptor, qDescriptor);
        }

        if (!mostrarPublicarAcuerdos) {
            query.where(qReunion.publica.isTrue()
                    .and(qReunion.completada.isTrue())
                    .and(whereDescriptor)
                    .and(whereClave)
                    .and(whereTipoOrgano)
                    .and(whereOrgano)
                    .and(whereAnyo));
        } else {
            query.where(qReunion.publica.isTrue()
                    .and(qReunion.completada.isTrue().or(qPuntoOrdenDia.urlActa.isNotNull()))
                    .and(whereDescriptor)
                    .and(whereClave)
                    .and(whereTipoOrgano)
                    .and(whereOrgano)
                    .and(whereAnyo));
        }

        query.where(qPuntoOrdenDia.publico.isTrue());


        if (fInicio != null) {
            query.where(qReunion.fecha.goe(fInicio));
        }

        if (fFin != null) {
            query.where(qReunion.fecha.lt(add1Day(fFin)));
        }

        if (!Strings.isNullOrEmpty(texto)) {
            String textoSinAcentosLike = StringUtils.limpiaAcentos(texto);

            if (idiomaAlternativo) {
                StringExpression asuntoAlternativo = Expressions.stringTemplate(FUNCTION_TRANSLATE, qReunion.asuntoAlternativo);
                StringExpression descripcionAlternativa = Expressions.stringTemplate(FUNCTION_TRANSLATE, qReunion.descripcionAlternativa);
                StringExpression tituloPODAlternativo = Expressions.stringTemplate(FUNCTION_TRANSLATE, qPuntoOrdenDia.tituloAlternativo);

                query.where(asuntoAlternativo.containsIgnoreCase(textoSinAcentosLike)
                        .or((descripcionAlternativa.containsIgnoreCase(textoSinAcentosLike)
                                .or(tituloPODAlternativo.containsIgnoreCase(textoSinAcentosLike)
                                        .or(qPuntoOrdenDia.acuerdosAlternativos.like("%" + textoSinAcentosLike + "%")
                                                .or(qPuntoOrdenDia.deliberacionesAlternativas.like("%" + textoSinAcentosLike + "%")))))));
            } else {
                StringExpression asunto = Expressions.stringTemplate(FUNCTION_TRANSLATE, qReunion.asunto);
                StringExpression descripcion = Expressions.stringTemplate(FUNCTION_TRANSLATE, qReunion.descripcion);
                StringExpression tituloPOD = Expressions.stringTemplate(FUNCTION_TRANSLATE, qPuntoOrdenDia.titulo);

                query.where(asunto.containsIgnoreCase(textoSinAcentosLike)
                        .or((descripcion.containsIgnoreCase(textoSinAcentosLike)
                                .or(tituloPOD.containsIgnoreCase(textoSinAcentosLike)
                                        .or(qPuntoOrdenDia.acuerdos.like("%" + textoSinAcentosLike + "%")
                                                .or(qPuntoOrdenDia.deliberaciones.like("%" + textoSinAcentosLike + "%")))))));
            }
        }

        return query;
    }

    private Date add1Day(Date fecha) {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        c.add(Calendar.DATE, 1);

        return c.getTime();
    }

    @Transactional
    public void deleteByReunionId(Long reunionId) {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qOrganoReunionMiembro);
        deleteClause.where(qOrganoReunionMiembro.reunionId.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qOrganoReunionInvitado);
        deleteClause.where(qOrganoReunionInvitado.reunionId.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qOrganoReunion);
        deleteClause.where(qOrganoReunion.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunionDocumento);
        deleteClause.where(qReunionDocumento.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunionComentario);
        deleteClause.where(qReunionComentario.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunionInvitado);
        deleteClause.where(qReunionInvitado.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunionDiligencia);
        deleteClause.where(qReunionDiligencia.reunionId.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunionHojaFirmas);
        deleteClause.where(qReunionHojaFirmas.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunion);
        deleteClause.where(qReunion.id.eq(reunionId)).execute();
    }

    public List<OrganoReunion> getOrganosReunionByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunion).where(qOrganoReunion.reunion.id.eq(reunionId)).list(qOrganoReunion);
    }

    public List<InvitadoTemplate> getInvitadosPresencialesByReunionId(Long reunionId) {
        JPAQuery queryReunionesInvitados = new JPAQuery(entityManager);
        JPAQuery queryOrganosInvitados = new JPAQuery(entityManager);

        List<ReunionInvitado> invitadosPorReunion = queryReunionesInvitados.from(qReunionInvitado)
                .where(qReunionInvitado.reunion.id.eq(reunionId))
                .list(qReunionInvitado);

        List<OrganoReunionInvitado> invitadosPorOrgano = queryOrganosInvitados.from(qOrganoReunionInvitado)
                .where(qOrganoReunionInvitado.reunionId.in(reunionId)
                        .and(qOrganoReunionInvitado.soloConsulta.eq(false)))
                .distinct()
                .list(qOrganoReunionInvitado);

        List<InvitadoTemplate> invitados = new ArrayList<>();

        addToInvidatosListFromReunionInvitados(invitados, invitadosPorReunion);
        addToInvitadosListFromOrganoInvitados(invitados, invitadosPorOrgano);

        return invitados;
    }

    public OrganoParametro getOrganoParametro(String organoId, Boolean ordinario) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoParametro)
                .where(qOrganoParametro.organoParametroPK.organoId.eq(organoId)
                        .and(qOrganoParametro.organoParametroPK.ordinario.eq(ordinario))
                )
                .uniqueResult(qOrganoParametro);
    }

    public List<Persona> getInvitadosByReunionId(Long reunionId) {
        JPAQuery queryReunionesInvitados = new JPAQuery(entityManager);
        JPAQuery queryOrganosInvitados = new JPAQuery(entityManager);

        List<Persona> personas = new ArrayList<>();

        List<ReunionInvitado> invitadosPorReunion = queryReunionesInvitados.from(qReunionInvitado)
                .where(qReunionInvitado.reunion.id.eq(reunionId))
                .list(qReunionInvitado);

        List<OrganoReunionInvitado> invitadosPorOrgano = queryOrganosInvitados.from(qOrganoReunionInvitado)
                .where(qOrganoReunionInvitado.reunionId.in(reunionId))
                .distinct()
                .list(qOrganoReunionInvitado);

        addToPersonasListFromReunionInvitados(personas, invitadosPorReunion);
        addToPersonasListFromOrganoInvitados(personas, invitadosPorOrgano);

        return personas;
    }

    private void addToInvidatosListFromReunionInvitados(List<InvitadoTemplate> invitados, List<ReunionInvitado> invitadosPorReunion) {

        for (ReunionInvitado reunionInvitado : invitadosPorReunion) {
            InvitadoTemplate invitadoTemplate = new InvitadoTemplate();

            invitadoTemplate.setId(reunionInvitado.getPersonaId());
            invitadoTemplate.setNombre(reunionInvitado.getPersonaNombre());
            invitadoTemplate.setEmail(reunionInvitado.getPersonaEmail());
            invitadoTemplate.setAsistencia(reunionInvitado.isAsistencia());

            invitados.add(invitadoTemplate);
        }

    }

    private void addToInvitadosListFromOrganoInvitados(List<InvitadoTemplate> invitados, List<OrganoReunionInvitado> invitadosPorOrgano) {

        for (OrganoReunionInvitado organoReunionInvitado : invitadosPorOrgano) {
            InvitadoTemplate invitadoTemplate = new InvitadoTemplate();
            OrganoParametro organoParametro = getOrganoParametro(organoReunionInvitado.getOrganoId(), organoReunionInvitado.getOrganoExterno());

            invitadoTemplate.setId(Long.parseLong(organoReunionInvitado.getPersonaId()));
            invitadoTemplate.setNombre(organoReunionInvitado.getNombre());
            invitadoTemplate.setEmail(organoReunionInvitado.getEmail());
            invitadoTemplate.setAsistencia(organoReunionInvitado.getAsistencia());

            if (organoParametro != null && organoParametro.getVerAsistencia() != null) {
                invitadoTemplate.setMostrarAsistencia(organoParametro.getVerAsistencia());
            } else {
                invitadoTemplate.setMostrarAsistencia(true);
            }

            invitados.add(invitadoTemplate);
        }

    }

    public void addToPersonasListFromReunionInvitados(List<Persona> personas, List<ReunionInvitado> invitados) {
        personas.addAll(invitados.stream()
                .filter(i -> !personaContainsId(personas, i.getPersonaId()))
                .map(i -> toPersona(i))
                .collect(Collectors.toList()));
    }

    public Persona toPersona(ReunionInvitado reunionInvitado) {
        Persona persona = new Persona();

        persona.setId(reunionInvitado.getPersonaId());
        persona.setEmail(reunionInvitado.getPersonaEmail());
        persona.setNombre(reunionInvitado.getPersonaNombre());

        return persona;
    }

    public void addToPersonasListFromOrganoInvitados(List<Persona> personas, List<OrganoReunionInvitado> invitados) {
        personas.addAll(invitados.stream()
                .filter(i -> !personaContainsId(personas, ParamUtils.parseLong(i.getPersonaId())))
                .map(i -> toPersona(i))
                .collect(Collectors.toList()));
    }

    public Persona toPersona(OrganoReunionInvitado organoReunionInvitado) {
        Persona persona = new Persona();

        persona.setId(ParamUtils.parseLong(organoReunionInvitado.getPersonaId()));
        persona.setEmail(organoReunionInvitado.getEmail());
        persona.setNombre(organoReunionInvitado.getNombre());

        return persona;
    }

    public boolean personaContainsId(List<Persona> personas, Long id) {
        return personas.stream().anyMatch(p -> id.equals(p.getId()));
    }

    public void updateAcuerdoPuntoDelDiaUrlActa(RespuestaFirmaPuntoOrdenDiaAcuerdo acuerdo) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);

        update.set(qPuntoOrdenDia.urlActa, acuerdo.getUrlActa())
                .set(qPuntoOrdenDia.urlActaAlternativa, acuerdo.getUrlActaAlternativa())
                .where(qPuntoOrdenDia.id.eq(acuerdo.getId()));

        update.execute();
    }

    public void updateAsistenciaMiembrosYSuplentes(Long reunionId, RespuestaFirmaAsistencia respuestaFirmaAsistencia) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.set(qOrganoReunionMiembro.urlAsistencia, respuestaFirmaAsistencia.getUrlAsistencia())
                .set(qOrganoReunionMiembro.urlAsistenciaAlternativa,
                        respuestaFirmaAsistencia.getUrlAsistenciaAlternativa())
                .where(qOrganoReunionMiembro.asistencia.isTrue()
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                        .and((qOrganoReunionMiembro.miembroId.eq(respuestaFirmaAsistencia.getPersonaId())
                                .and(qOrganoReunionMiembro.suplenteId.isNull())).or(qOrganoReunionMiembro.suplenteId.eq(
                                        Long.parseLong(respuestaFirmaAsistencia.getPersonaId()))
                                .and(qOrganoReunionMiembro.suplenteId.isNotNull()))));

        update.execute();
    }

    public void updateAsistenciaInvitadoReunion(Long reunionId, RespuestaFirmaAsistencia respuestaFirmaAsistencia) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionInvitado);

        update.set(qReunionInvitado.urlAsistencia, respuestaFirmaAsistencia.getUrlAsistencia())
                .set(qReunionInvitado.urlAsistenciaAlternativa, respuestaFirmaAsistencia.getUrlAsistenciaAlternativa())
                .where(qReunionInvitado.reunion.id.eq(reunionId)
                        .and(qReunionInvitado.personaId.eq(Long.parseLong(respuestaFirmaAsistencia.getPersonaId()))));

        update.execute();
    }

    public void updateAsistenciaInvitadoOrgano(Long reunionId, RespuestaFirmaAsistencia respuestaFirmaAsistencia) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionInvitado);

        update.set(qOrganoReunionInvitado.urlAsistencia, respuestaFirmaAsistencia.getUrlAsistencia())
                .set(qOrganoReunionInvitado.urlAsistenciaAlternativa,
                        respuestaFirmaAsistencia.getUrlAsistenciaAlternativa())
                .where(qOrganoReunionInvitado.reunionId.eq(reunionId)
                        .and(qOrganoReunionInvitado.personaId.eq(respuestaFirmaAsistencia.getPersonaId())));

        update.execute();
    }

    public ReunionHojaFirmas getHojaFirmasByReunionId(Long reunionid) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qReunionHojaFirmas)
                .where(qReunionHojaFirmas.reunion.id.eq(reunionid))
                .uniqueResult(qReunionHojaFirmas);
    }

    @Transactional
    public void deleteHojaFirmasAnterior(Long reunionId) {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qReunionHojaFirmas);
        deleteClause.where(qReunionHojaFirmas.reunion.id.eq(reunionId)).execute();
    }

    public Reunion getUltimaReunionByReunionIdOrganos(Long reunionId, List<String> organosReunion) {
        Reunion reunion = getReunionById(reunionId);
        ListIterator<String> iterator = organosReunion.listIterator();
        Long idUltimaReunion = null;
        if (iterator.hasNext()) {
            BooleanExpression be = qOrganoReunion.organoId.eq(iterator.next());

            while (iterator.hasNext()) {
                be = be.or(qOrganoReunion.organoId.eq(iterator.next()));
            }

            JPAQuery query = new JPAQuery(entityManager);
            List<Tuple> tuple = query.from(qReunion)
                    .join(qReunion.reunionOrganos, qOrganoReunion)
                    .where(be.and(qReunion.fecha.before(reunion.getFecha())))
                    .groupBy(qReunion.id, qReunion.fecha)
                    .having(qReunion.id.count().eq(Long.valueOf(organosReunion.size())))
                    .orderBy(qReunion.fecha.desc())
                    .list(qReunion.id, qReunion.id.count());
            if (tuple != null && !tuple.isEmpty()) idUltimaReunion = tuple.get(0).get(qReunion.id);
        }
        return idUltimaReunion != null ? getReunionById(idUltimaReunion) : null;
    }

    @Transactional
    public void updateAsistenciaInvitadoReunionAsincrona(Long reunionId, Long personaId, String urlAsistencia) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionInvitado);

        update.set(qReunionInvitado.urlAsistencia, urlAsistencia)
                .where(qReunionInvitado.reunion.id.eq(reunionId).and(qReunionInvitado.personaId.eq(personaId)));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaInvitadoOrganoReunionAsincrona(Long reunionId, Long personaId, String urlAsistencia) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionInvitado);

        update.set(qOrganoReunionInvitado.urlAsistencia, urlAsistencia)
                .where(qOrganoReunionInvitado.reunionId.eq(reunionId)
                        .and(qOrganoReunionInvitado.personaId.eq(personaId.toString())));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaMiembrosYSuplentesAsincrona(Long reunionId, Long personaId, String urlAsistencia) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.set(qOrganoReunionMiembro.urlAsistencia, urlAsistencia)
                .where(qOrganoReunionMiembro.asistencia.isTrue()
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                        .and((qOrganoReunionMiembro.miembroId.eq(personaId.toString())
                                .and(qOrganoReunionMiembro.suplenteId.isNull())).or(
                                qOrganoReunionMiembro.suplenteId.eq(personaId)
                                        .and(qOrganoReunionMiembro.suplenteId.isNotNull()))));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaAlternativaInvitadoReunionAsincrona(Long reunionId, Long personaId,
                                                                    String urlAsistenciaAlternativa) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionInvitado);

        update.set(qReunionInvitado.urlAsistenciaAlternativa, urlAsistenciaAlternativa)
                .where(qReunionInvitado.reunion.id.eq(reunionId).and(qReunionInvitado.personaId.eq(personaId)));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaAlternativaInvitadoOrganoReunionAsincrona(Long reunionId, Long personaId,
                                                                          String urlAsistenciaAlternativa) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionInvitado);

        update.set(qOrganoReunionInvitado.urlAsistenciaAlternativa, urlAsistenciaAlternativa)
                .where(qOrganoReunionInvitado.reunionId.eq(reunionId)
                        .and(qOrganoReunionInvitado.personaId.eq(personaId.toString())));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaAlternativaMiembrosYSuplentesAsincrona(Long reunionId, Long personaId,
                                                                       String urlAsistenciaAlternativa) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.set(qOrganoReunionMiembro.urlAsistenciaAlternativa, urlAsistenciaAlternativa)
                .where(qOrganoReunionMiembro.asistencia.isTrue()
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                        .and((qOrganoReunionMiembro.miembroId.eq(personaId.toString())
                                .and(qOrganoReunionMiembro.suplenteId.isNull())).or(
                                qOrganoReunionMiembro.suplenteId.eq(personaId)
                                        .and(qOrganoReunionMiembro.suplenteId.isNotNull()))));

        update.execute();
    }

    @Transactional
    public void reseteaEstadoEditadoPuntosOrdenDiaByreunionId(Long reunionId, Boolean estado) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);

        update.set(qPuntoOrdenDia.editado, estado).where(qPuntoOrdenDia.reunion.id.eq(reunionId));
        update.execute();
    }

    @Transactional
    public void updatePublica(Long reunionId, Boolean publica) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);

        update.set(qReunion.publica, publica).where(qReunion.id.eq(reunionId));
        update.execute();
    }

    public void reseteaURLActa(Long reunionId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);

        update.setNull(qReunion.urlActa).where(qReunion.id.eq(reunionId));
        update.execute();
    }

    public void eliminaResponsableActa(Long reunionId) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);

        update.setNull(qReunion.miembroResponsableActa).where(qReunion.id.eq(reunionId));
        update.execute();
    }

    public Boolean admiteCambioVoto(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunion).where(qReunion.id.eq(reunionId).and(qReunion.admiteCambioVoto.isTrue())).exists();
    }
}
