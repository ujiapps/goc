package es.uji.apps.goc.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import es.uji.commons.rest.StringUtils;

public class InvitadoFirma
{
    private Long id;

    private String nombre;

    private String email;
    
    private boolean asistencia;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }
    
    @JsonIgnore
    public String getNombreLimpio()
    {
        if (nombre == null) return null;

        return StringUtils.limpiaAcentos(nombre).trim();
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public boolean getAsistencia() {
		return asistencia;
	}

	public boolean isAsistencia() {
		return asistencia;
	}

	public void setAsistencia(boolean asistencia) {
		this.asistencia = asistencia;
	}
}
