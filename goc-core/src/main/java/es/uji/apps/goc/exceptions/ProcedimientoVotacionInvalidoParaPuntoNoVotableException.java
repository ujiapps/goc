package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ProcedimientoVotacionInvalidoParaPuntoNoVotableException extends CoreBaseException
{
    public ProcedimientoVotacionInvalidoParaPuntoNoVotableException(String procedimiento) {
        super("El procedimiento de votación '" + procedimiento + "' no es válido para un punto no votable");
    }
}
