package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EntidadNoValidaException extends CoreBaseException
{
    public EntidadNoValidaException()
    {
        super("appI18N.common.datosInvalidos");
    }
}
