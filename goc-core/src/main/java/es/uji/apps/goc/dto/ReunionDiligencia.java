package es.uji.apps.goc.dto;

import es.uji.commons.sso.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GOC_REUNIONES_DILIGENCIAS")
public class ReunionDiligencia {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "REUNION_ID")
    private Long reunionId;

    @Column(name = "USUARIO_REABRE")
    private long userId;

    @Column(name = "FECHA_REAPERTURA")
    private Date fechaReapertura;

    @Column(name = "MOTIVO_REAPERTURA")
    private String motivoReapertura;


    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public long getUserId() { return userId; }

    public void setUserId(long userId) { this.userId = userId; }

    public Date getFechaReapertura() { return fechaReapertura; }

    public void setFechaReapertura(Date fechaReapertura) { this.fechaReapertura = fechaReapertura; }

    public String getMotivoReapertura() { return motivoReapertura; }

    public void setMotivoReapertura(String motivoReapertura) { this.motivoReapertura = motivoReapertura; }

    public Long getReunionId() { return reunionId; }

    public void setReunionId(Long reunionId) { this.reunionId = reunionId; }

}
