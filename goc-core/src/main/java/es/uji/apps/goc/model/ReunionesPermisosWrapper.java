package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.ReunionPermiso;

import java.util.List;

public class ReunionesPermisosWrapper {
    private List<ReunionPermiso> reuniones;
    private Long numeroReuniones;

    public ReunionesPermisosWrapper() {
    }

    public ReunionesPermisosWrapper(List<ReunionPermiso> reuniones, Long numeroReuniones) {
        this.reuniones = reuniones;
        this.numeroReuniones = numeroReuniones;
    }

    public List<ReunionPermiso> getReuniones() {
        return reuniones;
    }

    public void setReuniones(List<ReunionPermiso> reuniones) {
        this.reuniones = reuniones;
    }

    public Long getNumeroReuniones() {
        return numeroReuniones;
    }

    public void setNumeroReuniones(Long numeroReuniones) {
        this.numeroReuniones = numeroReuniones;
    }
}

