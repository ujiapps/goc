package es.uji.apps.goc.model.punto;

import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.ReunionAVotar;

public class PuntoVotableActualizar extends PuntoVotable
{
    public PuntoVotableActualizar(PuntoOrdenDia puntoOrdenDia, ReunionAVotar reunionAVotar, String voto)
            throws TipoVotoNoExisteException, ReunionNoAdmiteCambioVotoException,
            PuntoNoVotableException, PuntoNoVotableEnContraException {
        super(puntoOrdenDia, reunionAVotar, voto);

        if (!getReunionAVotar().isAdmiteCambioVoto())
            throw new ReunionNoAdmiteCambioVotoException();
    }
}
