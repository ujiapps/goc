package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class VotoNoDelegadoException extends CoreBaseException
{
    public VotoNoDelegadoException() {
        super("No se peude votar como delegado");
    }
}
