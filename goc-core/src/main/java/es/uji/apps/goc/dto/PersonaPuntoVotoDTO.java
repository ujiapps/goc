package es.uji.apps.goc.dto;

import org.hibernate.annotations.Type;
import org.jsoup.helper.Validate;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.TipoVotoNoExisteException;
import es.uji.apps.goc.model.PersonaPuntoVoto;

@Entity
@Table(name = "GOC_PERSONA_PUNTO_VOTO")
public class PersonaPuntoVotoDTO
{
    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    private UUID id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @ManyToOne
    @JoinColumn(name = "ORGANO_REUNION_MIEMBRO_ID")
    private OrganoReunionMiembro organoReunionMiembro;

    @ManyToOne
    @JoinColumn(name = "PUNTO_ORDEN_DIA_ID")
    private PuntoOrdenDia puntoOrdenDia;

    @Column(name = "VOTO")
    private String voto;

    @Column(name="VOTO_DOBLE")
    private boolean votoDoble;

    public PersonaPuntoVotoDTO() {}

    public PersonaPuntoVotoDTO(UUID id, OrganoReunionMiembro organoReunionMiembro, PuntoOrdenDia puntoOrdenDia, String voto, boolean votoDoble) {
        this.id = id;
        this.organoReunionMiembro = organoReunionMiembro;
        this.puntoOrdenDia = puntoOrdenDia;
        this.voto = voto;
        this.votoDoble = votoDoble;
    }
    
    public PersonaPuntoVotoDTO(PersonaPuntoVoto personaPuntoVoto) {
        this();
        Validate.notNull(personaPuntoVoto);
        this.puntoOrdenDia = new PuntoOrdenDia(personaPuntoVoto.getPuntoAVotarId());
        this.organoReunionMiembro = personaPuntoVoto.getOrganoReunionMiembro();
        this.personaId = personaPuntoVoto.getPersona().getId();
        this.voto = personaPuntoVoto.getVoto();
        this.votoDoble = personaPuntoVoto.isVotoDoble();

        if (personaPuntoVoto.getId() != null)
            this.id = personaPuntoVoto.getId();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        Validate.notNull(id);
        this.id = id;
    }

    public OrganoReunionMiembro getOrganoReunionMiembro() {
        return organoReunionMiembro;
    }

    public void setOrganoReunionMiembro(OrganoReunionMiembro organoReunionMiembro) {
        Validate.notNull(organoReunionMiembro);
        this.organoReunionMiembro = organoReunionMiembro;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        Validate.notNull(personaId);
        this.personaId = personaId;
    }

    public PuntoOrdenDia getPuntoOrdenDia() {
        return puntoOrdenDia;
    }

    public void setPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia) {
        Validate.notNull(puntoOrdenDia);
        this.puntoOrdenDia = puntoOrdenDia;
    }

    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) throws TipoVotoNoExisteException {
        Validate.notNull(voto);
        if (!TipoVotoEnum.checkVoto(voto))
            throw new TipoVotoNoExisteException();

        this.voto = voto;
    }

    public boolean isVotoDoble() {
        return votoDoble;
    }

    public void setVotoDoble(boolean votoDoble) {
        this.votoDoble = votoDoble;
    }
}
