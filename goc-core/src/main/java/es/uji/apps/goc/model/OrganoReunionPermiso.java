package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.ReunionPermiso;

import java.util.List;

public class OrganoReunionPermiso {
    private Long id;
    private String nombre;
    private String nombreAlternativo;
    private List<ReunionPermiso> reuniones;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreAlternativo() {
        return nombreAlternativo;
    }

    public void setNombreAlternativo(String nombreAlternativo) {
        this.nombreAlternativo = nombreAlternativo;
    }

    public List<ReunionPermiso> getReuniones() {
        return reuniones;
    }

    public void setReuniones(List<ReunionPermiso> reuniones) {
        this.reuniones = reuniones;
    }
}

