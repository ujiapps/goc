package es.uji.apps.goc.dto;

import org.apache.xpath.operations.Bool;
import org.hibernate.annotations.Type;
import org.jsoup.helper.Validate;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.uji.apps.goc.model.VotantePrivado;

@Entity
@Table(name = "GOC_VOTANTES_PRIVADOS")
public class VotantePrivadoDTO
{
    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    private UUID id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "VOTO_DOBLE")
    private Boolean votoDoble = false;

    @ManyToOne
    @JoinColumn(name = "ORGANO_REUNION_MIEMBRO_ID")
    private OrganoReunionMiembro organoReunionMiembro;

    @ManyToOne
    @JoinColumn(name = "PUNTO_ORDEN_DIA_ID")
    private PuntoOrdenDia puntoOrdenDia;

    public VotantePrivadoDTO() {
    }

    public VotantePrivadoDTO(VotantePrivado votantePrivado) {
        Validate.notNull(votantePrivado);

        this.personaId = votantePrivado.getPersona().getId();
        this.organoReunionMiembro = votantePrivado.getOrganoReunionMiembro();
        this.puntoOrdenDia = new PuntoOrdenDia(votantePrivado.getPuntoAVotarId());
        this.votoDoble = votantePrivado.isVotoDoble();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public OrganoReunionMiembro getOrganoReunionMiembro() {
        return organoReunionMiembro;
    }

    public void setOrganoReunionMiembro(OrganoReunionMiembro organoReunionMiembro) {
        this.organoReunionMiembro = organoReunionMiembro;
    }

    public boolean getVotoDoble() {
        return votoDoble;
    }

    public void setVotoDoble(Boolean votoDoble) {
        if(votoDoble != null) this.votoDoble = votoDoble;
    }
}
