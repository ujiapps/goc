package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class UsuarioNoTienePermisoParaVotarException extends CoreDataBaseException
{
    public UsuarioNoTienePermisoParaVotarException() {
        super("No tienes permiso para emitir votos");
    }
}
