package es.uji.apps.goc.dto;

import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.Type;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.uji.apps.goc.model.PuntoVotoPrivado;

@Entity
@Table(name = "GOC_PUNTOS_VOTOS_PRIVADOS")
public class PuntoVotoPrivadoDTO
{
    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "PUNTO_ORDEN_DIA_ID")
    private PuntoOrdenDia puntoOrdenDia;

    @Column(name = "VOTO")
    private String voto;

    public PuntoVotoPrivadoDTO() {
    }

    public PuntoVotoPrivadoDTO(PuntoVotoPrivado puntoVotoPrivado) {
        Validate.notNull(puntoVotoPrivado);

        this.puntoOrdenDia = puntoVotoPrivado.getPunto().getPuntoOrdenDia();
        this.voto = puntoVotoPrivado.getPunto().getVoto();

        if (puntoVotoPrivado.getId() != null)
            this.id = puntoVotoPrivado.getId();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public PuntoOrdenDia getPuntoOrdenDia() {
        return puntoOrdenDia;
    }

    public void setPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia) {
        this.puntoOrdenDia = puntoOrdenDia;
    }

    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) {
        this.voto = voto;
    }
}
