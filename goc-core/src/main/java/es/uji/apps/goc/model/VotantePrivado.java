package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.exceptions.AsistenciaNoConfirmadaException;
import es.uji.apps.goc.exceptions.PuntoSecretoNoAdmiteCambioVotoException;
import es.uji.apps.goc.exceptions.VotoDelegadoException;
import es.uji.apps.goc.exceptions.VotoException;
import es.uji.apps.goc.model.punto.PuntoVotable;

public class VotantePrivado extends Votante {
    private boolean isVotoDoble;

    public VotantePrivado(
            Persona persona,
            PuntoVotable punto,
            OrganoReunionMiembro organoReunionMiembro,
            boolean isVotoDoble
    ) throws VotoDelegadoException, PuntoSecretoNoAdmiteCambioVotoException, AsistenciaNoConfirmadaException, VotoException {
        super(persona, punto, organoReunionMiembro);

        if (punto.getPuntoOrdenDia().getTipoVoto() != false)
            throw new PuntoSecretoNoAdmiteCambioVotoException();
        this.isVotoDoble = isVotoDoble;
    }

    public boolean isVotoDoble() {
        return isVotoDoble;
    }

    public void setVotoDoble(boolean votoDoble) {
        isVotoDoble = votoDoble;
    }
}
