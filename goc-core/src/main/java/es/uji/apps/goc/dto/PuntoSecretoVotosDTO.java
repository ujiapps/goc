package es.uji.apps.goc.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "GOC_PUNTO_SECRETO_VOTOS")
public class PuntoSecretoVotosDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PUNTO_ORDEN_DIA_ID")
    private PuntoOrdenDia puntoOrdenDia;

    @Column(name = "VOTOS_FAVOR")
    private Long votosFavor;
    
    @Column(name = "VOTOS_CONTRA")
    private Long votosContra;
    
    @Column(name = "VOTOS_BLANCO")
    private Long votosBlanco;
    
    @Transient
    private List<PuntoSecretoVotosDTO> subpuntos;

    public PuntoSecretoVotosDTO() {}

    public PuntoSecretoVotosDTO(Long id, PuntoOrdenDia puntoOrdenDia, Long votosFavor, Long votosContra, Long votosBlanco) {
        this.id = id;
        this.puntoOrdenDia = puntoOrdenDia;
        this.votosFavor = votosFavor;
        this.votosContra = votosContra;
        this.votosBlanco = votosBlanco;
        this.setSubpuntos(new ArrayList<>());
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PuntoOrdenDia getPuntoOrdenDia() {
		return puntoOrdenDia;
	}

	public void setPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia) {
		this.puntoOrdenDia = puntoOrdenDia;
	}

	public Long getVotosFavor() {
		return votosFavor;
	}

	public void setVotosFavor(Long votosFavor) {
		this.votosFavor = votosFavor;
	}

	public Long getVotosContra() {
		return votosContra;
	}

	public void setVotosContra(Long votosContra) {
		this.votosContra = votosContra;
	}

	public Long getVotosBlanco() {
		return votosBlanco;
	}

	public void setVotosBlanco(Long votosBlanco) {
		this.votosBlanco = votosBlanco;
	}

	public List<PuntoSecretoVotosDTO> getSubpuntos() {
		return subpuntos;
	}

	public void setSubpuntos(List<PuntoSecretoVotosDTO> subpuntos) {
		this.subpuntos = subpuntos;
	}
    
}
