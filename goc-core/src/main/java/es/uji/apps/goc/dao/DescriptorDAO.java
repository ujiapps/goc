package es.uji.apps.goc.dao;

import com.google.common.collect.Lists;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;

import org.springframework.stereotype.Repository;

import java.util.Iterator;
import java.util.List;

import es.uji.apps.goc.dto.Descriptor;
import es.uji.apps.goc.dto.QClave;
import es.uji.apps.goc.dto.QDescriptor;
import es.uji.apps.goc.dto.QDescriptorTipoOrgano;
import es.uji.apps.goc.dto.QPuntoOrdenDia;
import es.uji.apps.goc.dto.QPuntoOrdenDiaDescriptor;
import es.uji.apps.goc.dto.QReunion;
import es.uji.apps.goc.dto.QTipoOrganoLocal;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DescriptorDAO extends BaseDAODatabaseImpl
{
    private QDescriptor qDescriptor = QDescriptor.descriptor1;
    private QReunion qReunion = QReunion.reunion;
    private QPuntoOrdenDiaDescriptor qPuntoOrdenDiaDescriptor = QPuntoOrdenDiaDescriptor.puntoOrdenDiaDescriptor;
    private QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    private QClave qClave = QClave.clave1;
    private QDescriptorTipoOrgano qDescriptorTipoOrgano = QDescriptorTipoOrgano.descriptorTipoOrgano;
    private QTipoOrganoLocal qTipoOrgano = QTipoOrganoLocal.tipoOrganoLocal;

    public List<Descriptor> getDescriptores()
    {
        JPAQuery jpaQuery = new JPAQuery(entityManager);
        return jpaQuery.from(qDescriptor).list(qDescriptor);
    }

    public Descriptor getDescriptor(String idDescriptor)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qDescriptor).where(qDescriptor.id.eq(Long.valueOf(idDescriptor))).uniqueResult(qDescriptor);
    }

    public List<Descriptor> getDescriptoresConReunionesPublicas(List<Long> idsReuniones, Integer anyo)
    {
        BooleanExpression beWhere = null;
        if (anyo != null)
        {
            beWhere = qReunion.fecha.year().eq(anyo);
        }

        Iterator<List<Long>> iterator =
            Lists.partition(idsReuniones, 1000).iterator();
        if(iterator.hasNext()) {
            if(beWhere != null)
                beWhere = beWhere.and(qReunion.id.in(iterator.next()));
            else
                beWhere = qReunion.id.in(iterator.next());
            while (iterator.hasNext())
                beWhere = beWhere.or(qReunion.id.in(iterator.next()));
        }

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPuntoOrdenDiaDescriptor)
                .join(qPuntoOrdenDiaDescriptor.puntoOrdenDia, qPuntoOrdenDia)
                .join(qPuntoOrdenDia.reunion, qReunion)
                .join(qPuntoOrdenDiaDescriptor.clave, qClave)
                .join(qClave.descriptor, qDescriptor)
                .where(beWhere)
                .list(qDescriptor);
    }

    public List<Descriptor> getDescriptoresByTipoOrganoId(Long tipoOrganoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qDescriptor)
                .join(qDescriptor.descriptoresTiposOrgano, qDescriptorTipoOrgano)
                .join(qDescriptorTipoOrgano.tipoOrgano, qTipoOrgano)
                .where(qTipoOrgano.id.eq(tipoOrganoId))
                .list(qDescriptor);
    }

    public List<Descriptor> getDescriptoresNoRestringidos()
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qDescriptor).where(qDescriptor.descriptoresTiposOrgano.isEmpty()).list(qDescriptor);
    }
}