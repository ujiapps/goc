package es.uji.apps.goc.templates;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;

public class HTMLTemplate extends GenericTemplate implements Template
{
    private final Locale locale;
    private String application;
    
    public HTMLTemplate(String name, String path)
    {
        this(name, new Locale("ca"), path);
    }

    public HTMLTemplate(String name, Locale locale, String path)
    {
        super(name, path);

        this.locale = locale;
    }

    public HTMLTemplate(String name, Locale locale, String application, String path)
    {
        super(name, path);

        this.locale = locale;
        this.application = application;
    }

    @Override
    public byte[] process()
    {
        TemplateEngine templateEngine = TemplateEngineFactory.getTemplateEngine("HTML5",
            this.path, ".html", application);

        Context context = new Context(locale);
        context.setVariables(properties);

        return templateEngine.process(name, context).getBytes();
    }
}