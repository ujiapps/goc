package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import es.uji.apps.goc.dto.QReunionHojaFirmas;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ReunionHojaFirmasDAO extends BaseDAODatabaseImpl
{
    private QReunionHojaFirmas qReunionHojaFirmas = QReunionHojaFirmas.reunionHojaFirmas;


    @Transactional
    public void deleteHojaFirmasById(Long hojaFirmasId)
    {
        JPADeleteClause jpaDeleteClause = new JPADeleteClause(entityManager, qReunionHojaFirmas);

        jpaDeleteClause.where(qReunionHojaFirmas.id.eq(hojaFirmasId)).execute();
    }
}