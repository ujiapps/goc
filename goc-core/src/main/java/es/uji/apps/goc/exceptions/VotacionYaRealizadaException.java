package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class VotacionYaRealizadaException extends CoreBaseException {
    public VotacionYaRealizadaException() {
        super("No se puede excusar de una reunion si ya se ha votado.");
    }
}
