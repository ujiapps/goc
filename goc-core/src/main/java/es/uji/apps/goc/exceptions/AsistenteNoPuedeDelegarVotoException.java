package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class AsistenteNoPuedeDelegarVotoException extends CoreBaseException
{

    public AsistenteNoPuedeDelegarVotoException() {
        super("Un miembro que asiste a la reunión no puede delegar el voto");
    }
}
