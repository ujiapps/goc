package es.uji.apps.goc.notifications;

public class ArchivoAdjunto
{
    private String contentType;
    private String filename;
    private String content;

    public ArchivoAdjunto()
    {
    }

    public String getcontentType()
    {
        return contentType;
    }

    public void setcontentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
