package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class PersonaNoAutorizadaException extends CoreBaseException
{
    public PersonaNoAutorizadaException()
    {
        super("appI18N.common.personaNoAutorizada");
    }
}
