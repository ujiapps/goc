package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TipoRecuentoVotoNoExisteException extends CoreBaseException
{
    public TipoRecuentoVotoNoExisteException(){
        super("El tipo de recuento voto no existe");
    }
}
