package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class VotacionNoAbiertaException extends CoreBaseException
{
    public VotacionNoAbiertaException() {
        super("La votación está cerrada");
    }
}
