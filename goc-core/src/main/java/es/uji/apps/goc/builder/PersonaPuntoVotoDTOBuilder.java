package es.uji.apps.goc.builder;

import es.uji.apps.goc.dto.PersonaPuntoVotoDTO;
import es.uji.apps.goc.model.PersonaPuntoVoto;
import org.apache.commons.lang3.Validate;

public class PersonaPuntoVotoDTOBuilder
{
    private PersonaPuntoVoto personaPuntoVoto;

    public PersonaPuntoVotoDTOBuilder(PersonaPuntoVoto personaPuntoVoto) {
        Validate.notNull(personaPuntoVoto);
        this.personaPuntoVoto = personaPuntoVoto;
    }

    public PersonaPuntoVotoDTO build() {
        PersonaPuntoVotoDTO personaPuntoVotoDTO = new PersonaPuntoVotoDTO(personaPuntoVoto);
        personaPuntoVoto.checkVotacionAbierta();

        return personaPuntoVotoDTO;
    }
}
