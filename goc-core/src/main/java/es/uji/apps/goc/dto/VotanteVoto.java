package es.uji.apps.goc.dto;

public class VotanteVoto {
    private String nombreTitular;

    private String nombreDelegado;

    private String voto;

    public VotanteVoto(String nombreTitular, String voto){
        this.nombreTitular = nombreTitular;
        this.voto = voto;
    }

    public VotanteVoto(String nombreTitular, String nombreDelegado, String voto){
        this.nombreTitular = nombreTitular;
        this.nombreDelegado = nombreDelegado;
        this.voto = voto;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public String getNombreDelegado() {
        return nombreDelegado;
    }

    public String getVoto() {
        return voto;
    }
}
