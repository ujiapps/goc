package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class PuntoProcedimientoNoVotable extends CoreDataBaseException
{
    public PuntoProcedimientoNoVotable()
    {
        super("El punto tiene un procedimiento de votación no votable");
    }
}
