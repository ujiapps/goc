package es.uji.apps.goc.exceptions;

import es.uji.apps.goc.dto.Reunion;
import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoEsTelematicaExceptionConVotacion extends CoreBaseException
{
    public NoEsTelematicaExceptionConVotacion()
    {
        super("La reunión no es telemática y acepta votación");
    }

    public static void check(Reunion reunion) throws NoEsTelematicaExceptionConVotacion {
        if ((reunion.isTelematica() == null || !reunion.isTelematica()) && reunion.isVotacionTelematica() == true) {
            throw new NoEsTelematicaExceptionConVotacion();
        }
    }
}
