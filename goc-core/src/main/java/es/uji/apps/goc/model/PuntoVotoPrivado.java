package es.uji.apps.goc.model;

import org.apache.commons.lang3.Validate;

import java.util.UUID;

import es.uji.apps.goc.model.punto.PuntoVotable;

public class PuntoVotoPrivado
{
    private UUID id;

    private PuntoVotable punto;

    public PuntoVotoPrivado(PuntoVotable punto) {
        Validate.notNull(punto);
        this.punto = punto;
    }

    public PuntoVotoPrivado(UUID id, PuntoVotable punto) {
        this(punto);
        Validate.notNull(id);
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public PuntoVotable getPunto() {
        return punto;
    }
}
