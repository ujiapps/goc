package es.uji.apps.goc.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonaServiceTest extends BaseServiceTest {

    @Autowired
    private PersonaService personaService;

    @Test
    public void shouldReturnTrueWhenUserIsAutorizadoEnOrgano()
    {
        Boolean isAutorizadoEnReunion = personaService.isAutorizadoEnReunion(reunionConOrganoYMiembros.getId(), miembroYAutorizdo.getPersonaId());
        assertTrue(isAutorizadoEnReunion);
    }

    @Test
    public void shouldReturnFalseWhenUserIsAutorizadoEnOrgano()
    {
        Boolean isAutorizadoEnReunion = personaService.isAutorizadoEnReunion(reunionConOrganoYMiembros.getId(), 684645L);
        assertFalse(isAutorizadoEnReunion);
    }
}