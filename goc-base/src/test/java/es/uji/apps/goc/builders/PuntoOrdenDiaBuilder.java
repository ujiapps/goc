package es.uji.apps.goc.builders;

import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.enums.TipoRecuentoVotoEnum;
import es.uji.apps.goc.enums.TipoVotacionEnum;

import javax.persistence.EntityManager;
import java.util.Set;

public class PuntoOrdenDiaBuilder {

    private PuntoOrdenDia puntoOrdenDia;

    public PuntoOrdenDiaBuilder() {
        this.puntoOrdenDia = new PuntoOrdenDia();
        this.puntoOrdenDia.setOrden(1L);
        this.puntoOrdenDia.setPublico(true);
        this.puntoOrdenDia.setVotacionCerradaManualmente(false);
    }

    public PuntoOrdenDiaBuilder(Reunion reunion) {
        this();
        this.puntoOrdenDia.setReunion(reunion);
        this.puntoOrdenDia.setVotacionCerradaManualmente(false);
    }

    public PuntoOrdenDiaBuilder withAcuerdos() {
        puntoOrdenDia.setAcuerdos("Acuerdos");
        return this;
    }

    public PuntoOrdenDiaBuilder withAcuerdosAlt() {
        puntoOrdenDia.setAcuerdosAlternativos("AcuerdosAlternativos");
        return this;
    }

    public PuntoOrdenDiaBuilder withDeliberaciones() {
        puntoOrdenDia.setDeliberaciones("Deliberaciones");
        return this;
    }

    public PuntoOrdenDiaBuilder withdeliberacionesAltenrativas() {
        puntoOrdenDia.setDeliberacionesAlternativas("withdeliberacionesAltenrativas");
        return this;
    }

    public PuntoOrdenDiaBuilder withUrlcta() {
        puntoOrdenDia.setUrlActa("withUrlacta");
        return this;
    }

    public PuntoOrdenDiaBuilder withUrlActaAlternativa() {
        puntoOrdenDia.setUrlActaAlternativa("withUrlActaAlternativa");
        return this;
    }

    public PuntoOrdenDiaBuilder withUrlActaAnterior() {
        puntoOrdenDia.setUrlActaAnterior("withUrlActaAnterior");
        return this;
    }

    public PuntoOrdenDiaBuilder withUrlActaAnteriorAlternativa() {
        puntoOrdenDia.setUrlActaAnteriorAlt("withUrlActaAnteriorAlternativa");
        return this;
    }

    public PuntoOrdenDiaBuilder withPuntoSuperior(PuntoOrdenDia puntoSuperior) {
        puntoOrdenDia.setPuntoSuperior(puntoSuperior);
        return this;
    }

    public PuntoOrdenDiaBuilder withPuntoinferiores(Set<PuntoOrdenDia> puntosInferiores) {
        puntoOrdenDia.setPuntosInferiores(puntosInferiores);
        return this;
    }

    public PuntoOrdenDia build(EntityManager entityManager) {
        entityManager.persist(puntoOrdenDia);
        return puntoOrdenDia;
    }

    public PuntoOrdenDiaBuilder withTipoVoto(Boolean tipoVoto) {
        puntoOrdenDia.setTipoVoto(tipoVoto);
        return this;
    }

    public PuntoOrdenDiaBuilder withVotacionAbierta(Boolean votacionAbierta) {
        puntoOrdenDia.setVotacionAbierta(votacionAbierta);
        return this;
    }

    public PuntoOrdenDiaBuilder withProcedimientoAbreviado() {
        puntoOrdenDia.setTipoProcedimientoVotacion(
                TipoProcedimientoVotacionEnum.ABREVIADO.name()
        );

        return this;
    }

    public PuntoOrdenDiaBuilder withProcedimientoOrdinario() {
        puntoOrdenDia.setTipoProcedimientoVotacion(
                TipoProcedimientoVotacionEnum.ORDINARIO.name()
        );

        return this;
    }

    public PuntoOrdenDiaBuilder withTipoRecuentoVoto(TipoRecuentoVotoEnum tipoRecuentoVoto) {
        puntoOrdenDia.setTipoRecuentoVoto(tipoRecuentoVoto.name());
        return this;
    }

    public PuntoOrdenDiaBuilder withMayoriaSimple() {
        puntoOrdenDia.setTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE.name());
        return this;
    }

    public PuntoOrdenDiaBuilder withMayoriaNoVotable() {
        puntoOrdenDia.setTipoRecuentoVoto(TipoRecuentoVotoEnum.NO_VOTABLE.name());
        return this;
    }

    public PuntoOrdenDiaBuilder withTipoVotacion() {
        puntoOrdenDia.setTipoVotacion(TipoVotacionEnum.TELEMATICA.toString());
        return this;
    }


    public PuntoOrdenDia getPuntoOrdenDia() {
        return puntoOrdenDia;
    }
}
