package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.OrganoAutorizado;

public class OrganoAutorizadoBuilder
{
    OrganoAutorizado organoAutorizado;

    public OrganoAutorizadoBuilder()
    {
        this.organoAutorizado = new OrganoAutorizado();
    }

    public OrganoAutorizadoBuilder withId(Long id)
    {
        this.organoAutorizado.setId(id);
        return this;
    }

    public OrganoAutorizadoBuilder withPersonaId(Long personaId)
    {
        this.organoAutorizado.setPersonaId(personaId);
        return this;
    }

    public OrganoAutorizadoBuilder withPersonaNombre(String personaNombre)
    {
        this.organoAutorizado.setPersonaNombre(personaNombre);
        return this;
    }

    public OrganoAutorizadoBuilder withPersonaEmail(String email)
    {
        this.organoAutorizado.setPersonaEmail(email);
        return this;
    }

    public OrganoAutorizadoBuilder withOrganoExterno(boolean externo)
    {
        this.organoAutorizado.setOrganoExterno(externo);
        return this;
    }

    public OrganoAutorizadoBuilder withOrganoId(String organoId)
    {
        this.organoAutorizado.setOrganoId(organoId);
        return this;
    }

    public OrganoAutorizado build(EntityManager entityManager)
    {
        entityManager.persist(this.organoAutorizado);
        return this.organoAutorizado;
    }
}
