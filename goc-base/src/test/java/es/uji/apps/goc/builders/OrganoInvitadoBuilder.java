package es.uji.apps.goc.builders;

import es.uji.apps.goc.dto.OrganoInvitado;

import javax.persistence.EntityManager;

public class OrganoInvitadoBuilder
{
    private OrganoInvitado organoInvitado;

    public OrganoInvitadoBuilder(){
        this.organoInvitado = new OrganoInvitado();
    }

    public OrganoInvitadoBuilder withPersonaId(Long personaId){
        this.organoInvitado.setPersonaId(personaId);
        return this;
    }

    public OrganoInvitadoBuilder withOrganoId(String organoId){
        this.organoInvitado.setOrganoId(organoId);
        return this;
    }

    public OrganoInvitadoBuilder withPersonaNombre(String nombre){
        this.organoInvitado.setPersonaNombre(nombre);
        return this;
    }

    public OrganoInvitadoBuilder withPersonaEmail(String email){
        this.organoInvitado.setPersonaEmail(email);
        return this;
    }

    public OrganoInvitadoBuilder withOrganoExterno(Boolean externo){
        this.organoInvitado.setOrganoExterno(externo);
        return this;
    }

    public OrganoInvitado build(EntityManager entityManager){
        entityManager.persist(this.organoInvitado);
        return this.organoInvitado;
    }
}
