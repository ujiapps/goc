package es.uji.apps.goc.services;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.exceptions.OrganoNoDisponibleException;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.TipoOrgano;
import es.uji.apps.goc.services.rest.OrganoResource;
import es.uji.commons.rest.UIEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class OrganoServiceTest
{
    @PersistenceContext
    protected EntityManager entityManager;

    final Long CREADOR_ID = 88849L;

    @Autowired
    private OrganoService organoService;

    private TipoOrganoLocal tipoOrganoLocal;

    private OrganoInvitado organoInvitadoLocal;
    private OrganoInvitado organoInvitadoExterno;

    private OrganoAutorizado organoAutorizadoLocal;
    private OrganoAutorizado organoAutorizadoExterno;

    private OrganoLocal organoLocalConMiembros;

    private Organo organoFromLocal;
    private Organo organoExterno;

    private Reunion reunion;

    private OrganoReunion organoReunion;
    private OrganoReunion organoReunionExterno;


    @Before
    public void  setUp(){

        tipoOrganoLocal = new TipoOrganoBuilder().withCodigo("2").withNombre("TEST").build(entityManager);

        organoLocalConMiembros =
                new OrganoLocalBuilder().withNombre("organoLocalConMiembros").withTipoOrgano(tipoOrganoLocal).withCreadorId(CREADOR_ID)
                        .withMiembro(new MiembroLocalBuilder().withEmail("admin@4tic.com").build(entityManager)).withTipoProcedimientoVotacion("ABREVIADO")
                        .build(entityManager);

        TipoOrgano tipoOrganoFromTipoOrganoLocal = new TipoOrgano(tipoOrganoLocal.getId(),
                tipoOrganoLocal.getCodigo(), tipoOrganoLocal.getNombre(),
                tipoOrganoLocal.getNombreAlternativo());

        organoFromLocal = new Organo(organoLocalConMiembros.getId().toString(),
                organoLocalConMiembros.getNombre(),
                organoLocalConMiembros.getNombreAlternativo(),
                tipoOrganoFromTipoOrganoLocal, false);

         organoExterno = new Organo(organoLocalConMiembros.getId().toString(),
                "organoExterno",
                "organoExterno",
                tipoOrganoFromTipoOrganoLocal, true);


        organoInvitadoLocal = new OrganoInvitadoBuilder().withOrganoId(organoLocalConMiembros.getId().toString())
                .withPersonaId(1L).withPersonaNombre("invitadoLocal").withPersonaEmail("invitadoLocal@4tic.com")
                .withOrganoExterno(false).build(entityManager);

        organoInvitadoExterno = new OrganoInvitadoBuilder().withOrganoId(organoExterno.getId())
                .withPersonaId(1L).withPersonaNombre("invitadoExterno").withPersonaEmail("invitadoExterno@4tic.com")
                .withOrganoExterno(true).build(entityManager);

        organoAutorizadoLocal = new OrganoAutorizadoBuilder().withOrganoId(organoLocalConMiembros.getId().toString())
                .withPersonaId(1L).withPersonaNombre("autorizadoLocal").withPersonaEmail("autorizadoLocal@4tic.com")
                .withOrganoExterno(false).build(entityManager);

        organoAutorizadoExterno = new OrganoAutorizadoBuilder().withOrganoId(organoExterno.getId())
                .withPersonaId(1L).withPersonaNombre("autorizadoExterno").withPersonaEmail("autorizadoExterno@4tic.com")
                .withOrganoExterno(true).build(entityManager);


        reunion = new ReunionBuilder().withAsunto("reunionOrganoService")
                .withFecha(new Date()).withCreadorId(CREADOR_ID).withOrgano(organoFromLocal).withOrgano(organoExterno).build(entityManager);

        organoReunion = new OrganoReunionBuilder(reunion, organoFromLocal).build(entityManager);
        organoReunionExterno = new OrganoReunionBuilder(reunion, organoExterno).build(entityManager);
    }

    @Test
    public void shouldgetOnlyInvitadoLocal(){
        boolean invitadoLocalEncotrado = false;

        List<OrganoInvitado> organoInvitados = organoService.getInvitados(organoLocalConMiembros.getId().toString(), false);
        for(OrganoInvitado organoInvitado : organoInvitados){
            if(organoInvitado.getPersonaNombre().equals("invitadoLocal") && !organoInvitado.isOrganoExterno()){
                invitadoLocalEncotrado = true;
            }
        }
        assertThat("Encontrado solo el local", organoInvitados.size(), is(1));
        assertThat("Encotrado invitado local", invitadoLocalEncotrado, is(true));
    }

    @Test
    public void shouldgetOnlyInvitadoExterno(){
        boolean invitadoLocalEncotrado = false;

        List<OrganoInvitado> organoInvitados = organoService.getInvitados(organoExterno.getId(), true);
        for(OrganoInvitado organoInvitado : organoInvitados){
            if(organoInvitado.getPersonaNombre().equals("invitadoExterno") && organoInvitado.isOrganoExterno()){
                invitadoLocalEncotrado = true;
            }
        }
        assertThat("Encontrado solo el externo", organoInvitados.size(), is(1));
        assertThat("Encotrado invitado Externo", invitadoLocalEncotrado, is(true));
    }


    @Test
    public void shouldgetOnlyAutorizadoLocal(){
        boolean autrorizadoLocalEncotrado = false;

        List<OrganoAutorizado> organoAutorizados = organoService.getAutorizados(organoLocalConMiembros.getId().toString() ,false);
        for(OrganoAutorizado organoInvitado : organoAutorizados){
            if(organoInvitado.getPersonaNombre().equals("autorizadoLocal") && !organoInvitado.isOrganoExterno()){
                autrorizadoLocalEncotrado = true;
            }
        }
        assertThat("Encontrado solo el local", organoAutorizados.size(), is(1));
        assertThat("Encotrado invitado local", autrorizadoLocalEncotrado, is(true));
    }

    @Test
    public void shouldgetOnlyAutorizadoExterno(){
        boolean autorizadoLocalEncotrado = false;

        List<OrganoAutorizado> organoAutorizados = organoService.getAutorizados(organoExterno.getId(), true);
        for(OrganoAutorizado organoInvitado : organoAutorizados){
            if(organoInvitado.getPersonaNombre().equals("autorizadoExterno") && organoInvitado.isOrganoExterno()){
                autorizadoLocalEncotrado = true;
            }
        }
        assertThat("Encontrado solo el externo", organoAutorizados.size(), is(1));
        assertThat("Encotrado invitado Externo", autorizadoLocalEncotrado, is(true));
    }

    @Test
    public void shouldEditNombreOrganoLocalNoActualizaNombreOrganoAsociadoAReunion() throws OrganoNoDisponibleException
    {
        organoService.updateOrgano(Long.parseLong(organoFromLocal.getId()),
                "organoLocalNombreUpdated", organoFromLocal.getNombreAlternativo(), organoFromLocal.getTipoOrgano().getId(),
                false, organoFromLocal.getConvocarSinOrdenDia(), organoFromLocal.getEmail(),
                organoFromLocal.getActaProvisionalActiva(), organoFromLocal.getDelegacionVotoMultiple() ,CREADOR_ID,
                TipoProcedimientoVotacionEnum.ABREVIADO.toString(), organoFromLocal.getPresidenteVotoDoble(),
                organoFromLocal.getPermiteAbstencionVoto(), organoFromLocal.getVerAsistencia(), organoFromLocal.getVerDelegaciones());

        List<Organo> organosByReunionId = organoService.getOrganosByReunionId(reunion.getId(), CREADOR_ID);
        assertThat("La reunion contiene almenos un organo", organosByReunionId.size()>0);
        Boolean nombreEncotrado  = false;
        for (Organo organo : organosByReunionId){
            if (organo.getNombre().equals("organoLocalNombreUpdated")){
                nombreEncotrado = true;
            }
        }
        assertThat("el organo no ha modificado su nombre en la reunión", nombreEncotrado, is(false));
    }

    @Test
    public void deleteOrganoNoBorraOrganosReuniones() throws OrganoNoDisponibleException
    {
        organoService.updateOrgano(Long.parseLong(organoFromLocal.getId()),
                organoFromLocal.getNombre(), organoFromLocal.getNombreAlternativo(), organoFromLocal.getTipoOrgano().getId(),
                true, organoFromLocal.getConvocarSinOrdenDia(), organoFromLocal.getEmail(),
                organoFromLocal.getActaProvisionalActiva(), organoFromLocal.getDelegacionVotoMultiple(),CREADOR_ID,
                TipoProcedimientoVotacionEnum.ORDINARIO.name(), organoFromLocal.getPresidenteVotoDoble(),
                organoFromLocal.getPermiteAbstencionVoto(), organoFromLocal.getVerAsistencia(), organoFromLocal.getVerDelegaciones());

        List<Organo> organosByReunionId = organoService.getOrganosByReunionId(reunion.getId(), CREADOR_ID);
        assertThat("La reunion contiene almenos un organo", organosByReunionId.size()>0);
        Boolean nombreEncotrado  = false;
        for (Organo organo : organosByReunionId){
            if (organo.getId().equals(organoFromLocal.getId())){
                nombreEncotrado = true;
            }
        }
        assertThat("el organo dehabilitado todavía se encuentra en la reunión", nombreEncotrado, is(true));
    }

    @Test
    public void añadimosAutorizadoAOrganoLocalNoSeAñadeAOrganoExternoConMismoId(){
        organoAutorizadoLocal = new OrganoAutorizadoBuilder().withOrganoId(organoLocalConMiembros.getId().toString())
                .withPersonaId(1L).withPersonaNombre("autorizadoLocal2").withPersonaEmail("autorizadoLocal2@4tic.com")
                .withOrganoExterno(false).build(entityManager);

        List<OrganoAutorizado> autorizadosOrganoExternoConMismoId = organoService.getAutorizados(organoLocalConMiembros.getId().toString(), true);

        Boolean seHaEncontradoAutorizadoLocalEnExterno = false;
        for(OrganoAutorizado organoAutorizadoEnExterno: autorizadosOrganoExternoConMismoId){
            if(organoAutorizadoEnExterno.getPersonaNombre().equals("autorizadoLocal2")){
                seHaEncontradoAutorizadoLocalEnExterno = true;
            }
        }
        assertThat("Se ha encontrado el autorizado del organo Local en el organo externo", !seHaEncontradoAutorizadoLocalEnExterno);
    }

    @Test
    public void añadimosAutorizadoAOrganoExternoNoSeAñadeAOrganoLocalConMismoId(){
        organoAutorizadoExterno = new OrganoAutorizadoBuilder().withOrganoId(organoExterno.getId())
                .withPersonaId(1L).withPersonaNombre("AutorizadoExterno2").withPersonaEmail("autorizadoExterno2@4tic.com")
                .withOrganoExterno(true).build(entityManager);

        List<OrganoAutorizado> autorizadosOrganoLocalConMismoId  = organoService.getAutorizados(organoExterno.getId(), false);

        Boolean seHaEncontradoAutorizadoExternoEnLocal = false;
        for(OrganoAutorizado organoInvitadoEnExterno: autorizadosOrganoLocalConMismoId ){
            if(organoInvitadoEnExterno.getPersonaNombre().equals("AutorizadoExterno2")){
                seHaEncontradoAutorizadoExternoEnLocal = true;
            }
        }
        assertThat("Se ha encontrado el autorizado del organo externo en el organo local", !seHaEncontradoAutorizadoExternoEnLocal);
    }

    @Test
    public void añadimosInvitadoAOrganoLocalNoSeAñadeAOrganoExternoConMismoId(){
        organoInvitadoLocal = new OrganoInvitadoBuilder().withOrganoId(organoLocalConMiembros.getId().toString())
                .withPersonaId(1L).withPersonaNombre("invitadoLocal2").withPersonaEmail("invitadoLocal2@4tic.com")
                .withOrganoExterno(false).build(entityManager);

        List<OrganoInvitado> invitadosOrganoExternoConMismoId = organoService.getInvitados(organoLocalConMiembros.getId().toString(), true);

        Boolean seHaEncontradoInvitadoLocalEnExterno = false;
        for(OrganoInvitado organoInvitadoEnExterno: invitadosOrganoExternoConMismoId){
            if(organoInvitadoEnExterno.getPersonaNombre().equals("invitadoLocal2")){
                seHaEncontradoInvitadoLocalEnExterno = true;
            }
        }
        assertThat("Se ha encontrado el invitado del organo local en el organo externo", !seHaEncontradoInvitadoLocalEnExterno);
    }

    @Test
    public void añadimosInvitadoAOrganoExternoNoSeAñadeAOrganoLocalConMismoId(){
        organoInvitadoExterno = new OrganoInvitadoBuilder().withOrganoId(organoExterno.getId())
                .withPersonaId(1L).withPersonaNombre("invitadoExterno2").withPersonaEmail("invitadoExterno2@4tic.com")
                .withOrganoExterno(true).build(entityManager);

        List<OrganoInvitado> invitadosOrganoLocalConMismoId = organoService.getInvitados(organoLocalConMiembros.getId().toString(), false);

        Boolean seHaEncontradoInvitadoExternoEnLocal = false;
        for(OrganoInvitado organoInvitadoEnExterno: invitadosOrganoLocalConMismoId){
            if(organoInvitadoEnExterno.getPersonaNombre().equals("invitadoExterno2")){
                seHaEncontradoInvitadoExternoEnLocal = true;
            }
        }
        assertThat("Se ha encontrado el invitado del organo externo en el organo local", !seHaEncontradoInvitadoExternoEnLocal);
    }

    @Test
    public void borramosAutorizadoAOrganoLocalNoSeBorraAOrganoExternoConMismoId(){
        organoService.removeAutorizado(organoAutorizadoLocal.getId());

        List<OrganoAutorizado> autorizadosExternos = organoService.getAutorizados(organoLocalConMiembros.getId().toString(), true);
        Boolean seHaEncontradoAutorizadoExterno = false;
        for(OrganoAutorizado autorizadoExterno: autorizadosExternos){
            if(autorizadoExterno.getPersonaId().equals(organoAutorizadoLocal.getPersonaId())){
                seHaEncontradoAutorizadoExterno = true;
            }
        }

        assertThat("No se ha encontrado el autorizado en el organo externo tras borralo del órgano local",seHaEncontradoAutorizadoExterno );

    }

    @Test
    public void borramosAutorizadoAOrganoExternoNoSeBorraAOrganoLocalConMismoId(){
        organoService.removeAutorizado(organoAutorizadoExterno.getId());

        List<OrganoAutorizado> autorizadosLocalesConMismoOrganoId = organoService.getAutorizados(organoLocalConMiembros.getId().toString(), false);

        Boolean seHaEncontradoAutorizadoLocal = false;
        for(OrganoAutorizado autorizadoExterno: autorizadosLocalesConMismoOrganoId){
            if(autorizadoExterno.getPersonaId().equals(organoAutorizadoLocal.getPersonaId())){
                seHaEncontradoAutorizadoLocal = true;
            }
        }

        assertThat("No se ha encontrado el autorizado en el organo Local tras borralo del órgano Externo",seHaEncontradoAutorizadoLocal);

    }

    @Test
    public void borramosInvitadoAOrganoLocalNoSeBorraAOrganoExternoConMismoId(){
        organoService.removeInvitado(organoInvitadoLocal.getId());

        List<OrganoInvitado> invitadosExternos = organoService.getInvitados(organoLocalConMiembros.getId().toString(), true);
        Boolean seHaEncontradoInvitadoExterno = false;
        for(OrganoInvitado invitadoExterno: invitadosExternos){
            if(invitadoExterno.getPersonaId().equals(organoAutorizadoLocal.getPersonaId())){
                seHaEncontradoInvitadoExterno = true;
            }
        }

        assertThat("No se ha encontrado el invitado en el organo externo tras borralo del órgano Local",seHaEncontradoInvitadoExterno);
    }

    @Test
    public void borramosInvitadoAOrganoExternoNoSeBorraAOrganoLocalConMismoId(){
        organoService.removeInvitado(organoInvitadoExterno.getId());

        List<OrganoInvitado> autorizadosLocales = organoService.getInvitados(organoLocalConMiembros.getId().toString(), false);
        Boolean seHaEncontradoInvitadoLocal = false;
        for(OrganoInvitado invitadoLocal: autorizadosLocales){
            if(invitadoLocal.getPersonaId().equals(organoAutorizadoLocal.getPersonaId())){
                seHaEncontradoInvitadoLocal = true;
            }
        }

        assertThat("No se ha encontrado el invitado en el organo Local tras borralo del órgano Externo",seHaEncontradoInvitadoLocal);
    }

    @Test
    public void recuperamosElTipoDeVotacionAbreviadoDeUnaReunion(){
        String tipoProcedimiento = organoService.getTipoProcedimientoVotacionByReunionId(reunion.getId());

        Assert.assertEquals("ABREVIADO", tipoProcedimiento);
    }
}
