package es.uji.apps.goc.services;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.enums.TipoRecuentoVotoEnum;
import es.uji.apps.goc.exceptions.ProcedimientoVotacionInvalidoParaPuntoNoVotableException;
import es.uji.apps.goc.exceptions.PuntoOrdenDiaConVotosEmitidosException;
import es.uji.apps.goc.exceptions.RecuentoVotoInvalidoParaPuntoNoVotableException;
import es.uji.apps.goc.exceptions.TipoRecuentoVotoNoExisteException;
import es.uji.apps.goc.notifications.AvisosReunion;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class PuntoOrdenDiaTest extends BaseServiceTest {
    @PersistenceContext
    protected EntityManager entityManager;
    @Autowired
    PuntoOrdenDiaService puntoOrdenDiaService;
    @Autowired
    ReunionService reunionService;
    private Reunion reunion;
    private OrganoReunionMiembro organoReunionMiembro;
    private Reunion reunionCompletada;

    @Mock
    private AvisosReunion avisosReunion;

    @Before
    public void setUp() {
        this.reunion =
                new ReunionBuilder()
                        .withTelematica(true)
                        .withAvisoPrimeraReunion(false)
                        .build(entityManager);

        OrganoReunion organoReunion =
                new OrganoReunionBuilder()
                        .withOrganoId("1000")
                        .withReunion(reunion)
                        .build(entityManager);

        organoReunionMiembro =
                new OrganoReunionMiembroBuilder()
                        .withCargo("NOT", "Notario")
                        .withEmail("miembro@4tic.com")
                        .withMiembroId(1L)
                        .withOrganoReunion(organoReunion)
                        .withAsistencia(true)
                        .withEmail("test@test.com")
                        .build(entityManager);

        this.reunionCompletada =
                new ReunionBuilder()
                        .withAvisoPrimeraReunion(true)
                        .withVotacionTelematica(true)
                        .build(entityManager);

        puntoOrdenDiaService.setAvisosReunion(avisosReunion);
    }

    @Test
    public void shouldPuntoOrdenDiaVotacionAbiertaNullWhenDefault() {
        PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDiaBuilder().getPuntoOrdenDia();
        assertNull(puntoOrdenDia.getTipoVoto());
    }

    @Test
    public void shouldPuntoOrdenDiaVotacionAbiertaFalseWhenSetFalse() {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withTipoVoto(false)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE)
                        .getPuntoOrdenDia();

        assertThat(puntoOrdenDia.getTipoVoto(), is(false));
    }

    @Test
    public void shouldPuntoOrdenDiaVotacionAbiertaTrueWhenSetTrue() {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE)
                        .getPuntoOrdenDia();

        assertThat(puntoOrdenDia.getTipoVoto(), is(true));
    }

    @Test
    public void shouldTipoProcedimientoAbreviadoWhenSelectAbreviado()
            throws TipoRecuentoVotoNoExisteException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            RecuentoVotoInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdenDiaAbreviado =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoAbreviado()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE)
                        .getPuntoOrdenDia();
        puntoOrdenDiaAbreviado.setReunion(reunion);

        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDiaAbreviado, reunion);
        assertThat(puntoOrdenDia.getTipoProcedimientoVotacion(), is(TipoProcedimientoVotacionEnum.ABREVIADO.name()));
    }

    @Test
    public void shouldTipoProcedimientoAbreviadoWhenSelectOrdinario()
            throws TipoRecuentoVotoNoExisteException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException, RecuentoVotoInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdenDiaOrdinario =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoOrdinario()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE)
                        .getPuntoOrdenDia();
        puntoOrdenDiaOrdinario.setReunion(reunion);

        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDiaOrdinario, reunion);
        assertThat(puntoOrdenDia.getTipoProcedimientoVotacion(), is(TipoProcedimientoVotacionEnum.ORDINARIO.name()));
    }

    @Test
    public void shouldTipoProcedimientoAbreviadoWhenSetToAbreviado()
            throws Exception {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoOrdinario()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE)
                        .build(entityManager);
        puntoOrdenDia.setReunion(reunion);

        puntoOrdenDia.setTipoProcedimientoVotacion(
                TipoProcedimientoVotacionEnum.ABREVIADO.name()
        );
        puntoOrdenDia = puntoOrdenDiaService.updatePuntoOrdenDia(
                puntoOrdenDia.getId(), puntoOrdenDia.getTitulo(), puntoOrdenDia.getTituloAlternativo(),
                puntoOrdenDia.getDescripcion(), puntoOrdenDia.getDescripcionAlternativa(), puntoOrdenDia.getDeliberaciones(),
                puntoOrdenDia.getDeliberacionesAlternativas(), puntoOrdenDia.getAcuerdos(), puntoOrdenDia.getAcuerdosAlternativos(),
                puntoOrdenDia.getOrden(), puntoOrdenDia.isPublico(), 1L, puntoOrdenDia.isEditado(),
                puntoOrdenDia.getTipoVoto(), puntoOrdenDia.getTipoProcedimientoVotacion(),
                puntoOrdenDia.getTipoRecuentoVoto(), puntoOrdenDia.getVotableEnContra(), puntoOrdenDia.getTipoVotacion()
        );
        assertThat(puntoOrdenDia.getTipoProcedimientoVotacion(), is(TipoProcedimientoVotacionEnum.ABREVIADO.name()));
    }

    @Test
    public void shouldTipoProcedimientOrdinarioWhenSetToOrdinario()
            throws Exception {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoAbreviado()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE)
                        .build(entityManager);
        puntoOrdenDia.setReunion(reunion);

        puntoOrdenDia.setTipoProcedimientoVotacion(
                TipoProcedimientoVotacionEnum.ORDINARIO.name()
        );
        puntoOrdenDia = puntoOrdenDiaService.updatePuntoOrdenDia(
                puntoOrdenDia.getId(), puntoOrdenDia.getTitulo(), puntoOrdenDia.getTituloAlternativo(),
                puntoOrdenDia.getDescripcion(), puntoOrdenDia.getDescripcionAlternativa(), puntoOrdenDia.getDeliberaciones(),
                puntoOrdenDia.getDeliberacionesAlternativas(), puntoOrdenDia.getAcuerdos(), puntoOrdenDia.getAcuerdosAlternativos(),
                puntoOrdenDia.getOrden(), puntoOrdenDia.isPublico(), 1L, puntoOrdenDia.isEditado(),
                puntoOrdenDia.getTipoVoto(), puntoOrdenDia.getTipoProcedimientoVotacion(),
                puntoOrdenDia.getTipoRecuentoVoto(), puntoOrdenDia.getVotableEnContra(),
                puntoOrdenDia.getTipoVotacion()
        );
        assertThat(puntoOrdenDia.getTipoProcedimientoVotacion(), is(TipoProcedimientoVotacionEnum.ORDINARIO.name()));
    }

    @Test
    public void shouldPuntoHasMayoriaSimpleWhenCreateWithMayoriaSimple()
            throws TipoRecuentoVotoNoExisteException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            RecuentoVotoInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoAbreviado()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE)
                        .build(entityManager);
        puntoOrdenDia.setReunion(reunion);

        PuntoOrdenDia puntoConMayoriaSimple = puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDia, reunion);
        Assert.assertEquals(puntoConMayoriaSimple.getTipoRecuentoVoto(), TipoRecuentoVotoEnum.MAYORIA_SIMPLE.name());
    }

    @Test
    public void shouldPuntoHasMayoriaAbsolutaPresentesWhenCreateWithMayoriaAbsolutaPresentes()
            throws TipoRecuentoVotoNoExisteException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            RecuentoVotoInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoAbreviado()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_PRESENTES)
                        .build(entityManager);
        puntoOrdenDia.setReunion(reunion);

        PuntoOrdenDia puntoConMayoriaAbsolutaPresentes = puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDia, reunion);
        Assert.assertEquals(
                puntoConMayoriaAbsolutaPresentes.getTipoRecuentoVoto(),
                TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_PRESENTES.name()
        );
    }

    @Test
    public void shouldPuntoHasMayoriaAbsolutaOrganoWhenCreateWithMayoriaAbsolutaOrgano()
            throws TipoRecuentoVotoNoExisteException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            RecuentoVotoInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoAbreviado()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_ORGANO)
                        .build(entityManager);
        puntoOrdenDia.setReunion(reunion);

        PuntoOrdenDia puntoConMayoriaAbsolutaOrgano = puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDia, reunion);
        Assert.assertEquals(puntoConMayoriaAbsolutaOrgano.getTipoRecuentoVoto(), TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_ORGANO.name());
    }

    @Test
    public void shouldPuntosHasMayoriaSimpleWhenSetMayoriaSimple()
            throws TipoRecuentoVotoNoExisteException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            RecuentoVotoInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdenDiaConMayoriaAbsolutaOrgano =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoAbreviado()
                        .withTipoVoto(true)
                        .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_ORGANO)
                        .build(entityManager);
        puntoOrdenDiaConMayoriaAbsolutaOrgano.setReunion(reunion);

        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDiaConMayoriaAbsolutaOrgano, reunion);

        puntoOrdenDia.setTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE.name());
        Assert.assertEquals(puntoOrdenDia.getTipoRecuentoVoto(), TipoRecuentoVotoEnum.MAYORIA_SIMPLE.name());
    }

    @Test(expected = RecuentoVotoInvalidoParaPuntoNoVotableException.class)
    public void shouldThrowErrorWhenCreatePuntoNoVotableWithMayoriaSimple()
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withMayoriaSimple()
                        .withTipoVoto(null)
                        .build(entityManager);
        puntoOrdenDia.setReunion(reunion);

        puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDia, reunion);
    }

    @Test(expected = ProcedimientoVotacionInvalidoParaPuntoNoVotableException.class)
    public void shouldThrowErrorWhenCreatePuntoNoVotableWithProcedimientoOrdinario()
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoOrdinario()
                        .withMayoriaNoVotable()
                        .withTipoVoto(null)
                        .build(entityManager);
        puntoOrdenDia.setReunion(reunion);

        puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDia, reunion);
    }

    @Test()
    public void shouldBeVotacionAbiertaWhenSetFromVotacionSecretaAndPuntoHasNotVotos()
            throws Exception {
        PuntoOrdenDia puntoOrdenDiaSecretoSinVotos =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoOrdinario()
                        .withMayoriaSimple()
                        .withTipoVoto(false)
                        .withTipoVotacion()
                        .build(entityManager);

        puntoOrdenDiaSecretoSinVotos.setReunion(reunion);
        puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDiaSecretoSinVotos, reunion);

        PuntoOrdenDia puntoOrdenDiaAbierto = puntoOrdenDiaService.updatePuntoOrdenDia(
                puntoOrdenDiaSecretoSinVotos.getId(), puntoOrdenDiaSecretoSinVotos.getTitulo(),
                puntoOrdenDiaSecretoSinVotos.getTituloAlternativo(), puntoOrdenDiaSecretoSinVotos.getDescripcion(),
                puntoOrdenDiaSecretoSinVotos.getDescripcionAlternativa(), puntoOrdenDiaSecretoSinVotos.getDeliberaciones(),
                puntoOrdenDiaSecretoSinVotos.getDeliberacionesAlternativas(), puntoOrdenDiaSecretoSinVotos.getAcuerdos(),
                puntoOrdenDiaSecretoSinVotos.getAcuerdosAlternativos(), puntoOrdenDiaSecretoSinVotos.getOrden(),
                puntoOrdenDiaSecretoSinVotos.isPublico(), 1L, puntoOrdenDiaSecretoSinVotos.isEditado(),
                true, puntoOrdenDiaSecretoSinVotos.getTipoProcedimientoVotacion(),
                puntoOrdenDiaSecretoSinVotos.getTipoRecuentoVoto(), puntoOrdenDiaSecretoSinVotos.getVotableEnContra(),
                puntoOrdenDiaSecretoSinVotos.getTipoVotacion()
        );

        assertTrue(puntoOrdenDiaAbierto.getTipoVoto());
    }

    @Test()
    public void shouldNotHasVotacionWhenSetFromVotacionSecretaAndPuntoHasNotVotos()
            throws Exception {
        PuntoOrdenDia puntoOrdenDiaSecretoSinVotos =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoOrdinario()
                        .withMayoriaSimple()
                        .withTipoVoto(false)
                        .withTipoVotacion()
                        .build(entityManager);

        puntoOrdenDiaSecretoSinVotos.setReunion(reunion);
        puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDiaSecretoSinVotos, reunion);

        PuntoOrdenDia puntoOrdenDiaNoVotable = puntoOrdenDiaService.updatePuntoOrdenDia(
                puntoOrdenDiaSecretoSinVotos.getId(), puntoOrdenDiaSecretoSinVotos.getTitulo(),
                puntoOrdenDiaSecretoSinVotos.getTituloAlternativo(), puntoOrdenDiaSecretoSinVotos.getDescripcion(),
                puntoOrdenDiaSecretoSinVotos.getDescripcionAlternativa(), puntoOrdenDiaSecretoSinVotos.getDeliberaciones(),
                puntoOrdenDiaSecretoSinVotos.getDeliberacionesAlternativas(), puntoOrdenDiaSecretoSinVotos.getAcuerdos(),
                puntoOrdenDiaSecretoSinVotos.getAcuerdosAlternativos(), puntoOrdenDiaSecretoSinVotos.getOrden(),
                puntoOrdenDiaSecretoSinVotos.isPublico(), 1L, puntoOrdenDiaSecretoSinVotos.isEditado(),
                null, TipoProcedimientoVotacionEnum.NO_VOTABLE.name(),
                TipoRecuentoVotoEnum.NO_VOTABLE.name(), puntoOrdenDiaSecretoSinVotos.getVotableEnContra(),
                puntoOrdenDiaSecretoSinVotos.getTipoVotacion()
        );

        assertNull(puntoOrdenDiaNoVotable.getTipoVoto());
        assertEquals(puntoOrdenDiaNoVotable.getTipoProcedimientoVotacion(), TipoProcedimientoVotacionEnum.NO_VOTABLE.name());
        assertEquals(puntoOrdenDiaNoVotable.getTipoRecuentoVoto(), TipoRecuentoVotoEnum.NO_VOTABLE.name());
    }

    @Test(expected = PuntoOrdenDiaConVotosEmitidosException.class)
    public void shouldThrowExceptionWhenUpdatePuntoWithVotosEmitidos()
            throws Exception {
        Boolean tipoVotoOriginal = false;

        PuntoOrdenDia puntoOrdenDiaSecretoConVotos =
                new PuntoOrdenDiaBuilder(reunion)
                        .withProcedimientoOrdinario()
                        .withMayoriaSimple()
                        .withTipoVoto(tipoVotoOriginal)
                        .withTipoVotacion()
                        .build(entityManager);

        new PersonaPuntoVotoBuilder(puntoOrdenDiaSecretoConVotos)
                .withVotoAFavor(organoReunionMiembro)
                .build(entityManager);

        puntoOrdenDiaService.updatePuntoOrdenDia(
                puntoOrdenDiaSecretoConVotos.getId(), puntoOrdenDiaSecretoConVotos.getTitulo(),
                puntoOrdenDiaSecretoConVotos.getTituloAlternativo(), puntoOrdenDiaSecretoConVotos.getDescripcion(),
                puntoOrdenDiaSecretoConVotos.getDescripcionAlternativa(), puntoOrdenDiaSecretoConVotos.getDeliberaciones(),
                puntoOrdenDiaSecretoConVotos.getDeliberacionesAlternativas(), puntoOrdenDiaSecretoConVotos.getAcuerdos(),
                puntoOrdenDiaSecretoConVotos.getAcuerdosAlternativos(), puntoOrdenDiaSecretoConVotos.getOrden(),
                puntoOrdenDiaSecretoConVotos.isPublico(), 1L, puntoOrdenDiaSecretoConVotos.isEditado(),
                !tipoVotoOriginal, puntoOrdenDiaSecretoConVotos.getTipoProcedimientoVotacion(),
                puntoOrdenDiaSecretoConVotos.getTipoRecuentoVoto(), puntoOrdenDiaSecretoConVotos.getVotableEnContra(),
                puntoOrdenDiaSecretoConVotos.getTipoVotacion()
        );
    }

    @Test()
    public void puntoAbreviadoShouldBeOpenWhenAddToReunionCompletada()
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoAbreviado =
                new PuntoOrdenDiaBuilder(reunionCompletada)
                        .withProcedimientoAbreviado()
                        .withTipoVoto(true)
                        .withMayoriaSimple()
                        .build(entityManager);

        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.addPuntoOrdenDia(puntoAbreviado, reunionCompletada);
        Assert.assertTrue(puntoOrdenDia.getVotacionAbierta());
        Assert.assertNotNull(puntoOrdenDia.getFechaInicioVotacion());
    }

    @Test()
    public void puntoOrdinarioShouldBeClosedWhenAddToReunionCompletada()
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        PuntoOrdenDia puntoOrdinario =
                new PuntoOrdenDiaBuilder(reunionCompletada)
                        .withProcedimientoOrdinario()
                        .withTipoVoto(true)
                        .withMayoriaSimple()
                        .build(entityManager);

        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdinario, reunion);
        Assert.assertTrue(puntoOrdenDia.getVotacionAbierta() == null || puntoOrdenDia.getVotacionAbierta() == false);
        Assert.assertNull(puntoOrdenDia.getFechaInicioVotacion());
    }

    @Test
    public void puntoOrdenDiaWithReunionSinVotacionTelematicaShouldNotThrowError() throws ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException {

        Reunion reunionSinTelematica =
                new ReunionBuilder()
                        .build(entityManager);

        PuntoOrdenDia puntoNoVotable =
                new PuntoOrdenDiaBuilder(reunionSinTelematica)
                        .build(entityManager);

        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.addPuntoOrdenDia(puntoNoVotable, reunion);
        assertNull(puntoOrdenDia.getTipoProcedimientoVotacion());
        assertNull(puntoOrdenDia.getVotacionAbierta());
        assertNull(puntoOrdenDia.getTipoVoto());
    }
}
