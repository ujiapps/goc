package es.uji.apps.goc.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnit4.class)
public class PuntoOrdenDiaMultinivelTest {
    @Test
    public void shouldReturnIndexWithoutPrefixWhenZero() {
        assertThat(PuntoOrdenDiaMultinivel.getIndex(0, 2), is("2"));
    }

    @Test
    public void shouldReturnIndexWithPrefixWhenNotZero() {
        assertThat(PuntoOrdenDiaMultinivel.getIndex(1, 2), is("1.2"));
    }
}
