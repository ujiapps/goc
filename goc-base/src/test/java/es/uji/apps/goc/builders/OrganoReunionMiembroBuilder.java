package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.model.Persona;

public class OrganoReunionMiembroBuilder
{
    OrganoReunionMiembro organoReunionMiembro;

    public OrganoReunionMiembroBuilder(){
        this.organoReunionMiembro = new OrganoReunionMiembro();
    }

    public OrganoReunionMiembroBuilder withId(Long id)
    {
        this.organoReunionMiembro.setId(id);
        return this;
    }

    public OrganoReunionMiembroBuilder withOrganoReunion(OrganoReunion organoReunion)
    {
        this.organoReunionMiembro.setOrganoId(organoReunion.getOrganoId());
        this.organoReunionMiembro.setOrganoReunion(organoReunion);
        return this;
    }

    public OrganoReunionMiembroBuilder withNombre(String nombre)
    {
        this.organoReunionMiembro.setNombre(nombre);
        return this;
    }

    public OrganoReunionMiembroBuilder withEmail(String email)
    {
        this.organoReunionMiembro.setEmail(email);
        return this;
    }

    public OrganoReunionMiembroBuilder withMiembroId(Long miembroId)
    {
        this.organoReunionMiembro.setMiembroId(miembroId.toString());
        return this;
    }

    public OrganoReunionMiembroBuilder withMiembro(MiembroLocal miembro)
    {
        this.organoReunionMiembro.setMiembroId(miembro.getPersonaId().toString());
        this.organoReunionMiembro.setNombre(miembro.getNombre());
        this.organoReunionMiembro.setEmail(miembro.getEmail());
        return this;
    }

    public OrganoReunionMiembroBuilder withVotoDelegado(OrganoReunionMiembro delegado)
    {
        this.organoReunionMiembro.setDelegadoVotoId(Long.valueOf(delegado.getMiembroId()));
        this.organoReunionMiembro.setDelegadoVotoEmail(delegado.getEmail());
        this.organoReunionMiembro.setDelegadoVotoNombre(delegado.getNombre());
        return this;
    }

    public OrganoReunionMiembroBuilder withAsistencia(boolean asistencia)
    {
        this.organoReunionMiembro.setAsistencia(asistencia);
        this.organoReunionMiembro.setAsistenciaConfirmada(asistencia);
        return this;
    }

    public OrganoReunionMiembroBuilder withCargo(String codigo, String nombre)
    {
        this.organoReunionMiembro.setCargoCodigo(codigo);
        this.organoReunionMiembro.setCargoNombre(nombre);
        return this;
    }

    // La suplencia se estable con un objeto de la clase Persona
    public OrganoReunionMiembroBuilder withSuplente(Persona persona)
    {
        this.organoReunionMiembro.setSuplenteId(persona.getId());
        this.organoReunionMiembro.setSuplenteNombre(persona.getNombre());
        this.organoReunionMiembro.setSuplenteEmail(persona.getEmail());
        return withAsistencia(true);
    }


    public OrganoReunionMiembro build(EntityManager entityManager)
    {
        if (organoReunionMiembro.getOrganoReunion() != null
                && organoReunionMiembro.getOrganoReunion().getReunion() != null) {
            organoReunionMiembro.setReunionId(organoReunionMiembro.getOrganoReunion().getReunion().getId());
            organoReunionMiembro.setOrganoExterno(organoReunionMiembro.getOrganoReunion().getExterno());
        }
        entityManager.persist(this.organoReunionMiembro);
        return this.organoReunionMiembro;
    }
}
