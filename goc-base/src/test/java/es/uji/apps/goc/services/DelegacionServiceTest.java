package es.uji.apps.goc.services;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.CodigoCargoEnum;
import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.ReunionAVotar;
import es.uji.apps.goc.model.punto.PuntoVotable;
import es.uji.apps.goc.model.punto.PuntoVotableActualizar;
import es.uji.apps.goc.notifications.AvisosReunion;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class DelegacionServiceTest extends BaseServiceTest {
    private static final Long CREADOR_ID = 2222l;
    private static final Long PERSONA_ID = 1L;
    private static final String PERSONA_NAME = "persona";
    private static final String PERSONA_EMAIL = "persona@email.com";
    @PersistenceContext
    protected EntityManager entityManager;
    @Mock
    AvisosReunion avisosReunionService;
    @InjectMocks
    @Autowired
    OrganoReunionMiembroService organoReunionMiembroService;
    @Autowired
    ReunionService reunionService;
    @Autowired
    ReunionDAO reunionDAO;
    @Autowired
    VotacionService votacionService;
    private Reunion reunionConOrganoNoDelegacion;
    private Reunion reunion;
    private Persona persona;
    private Persona delegado;
    private Persona elQueDelega;
    private Persona noAsistente;
    private PuntoOrdenDia puntoOrdenDiaAbierto;
    private OrganoReunionMiembro organoReunionMiembroSecretario;
    private OrganoReunionMiembro organoReunionMiembroDelegado;
    private OrganoReunionMiembro organoReunionQueDelega;
    private OrganoReunionMiembro organoReunionMiembroNoAsistente;
    private OrganoLocal organoLocal;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.doNothing().when(avisosReunionService).enviaAvisoDelegacionVoto(anyObject(), anyObject(), anyObject(), anyObject(), anyBoolean());

        persona = new Persona(PERSONA_ID, PERSONA_NAME, PERSONA_EMAIL);
        delegado = new Persona(2L, "ABC", "prueba@test.com");
        elQueDelega = new Persona(5L, "El que delega", "elquedelega@test.com");
        noAsistente = new Persona(3L, "NO ASISTE", "noasiste@test.com");

        this.puntoOrdenDiaAbierto
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(true)
                .withVotacionAbierta(true)
                .withMayoriaSimple()
                .withProcedimientoAbreviado()
                .withTipoVotacion()
                .build(entityManager);

        MiembroLocal validMember =
                new MiembroLocalBuilder()
                        .withNombre(persona.getNombre())
                        .withEmail(persona.getEmail())
                        .withNato(true)
                        .withPersonaId(persona.getId())
                        .build(entityManager);

        MiembroLocal noAsistenteValidMember =
                new MiembroLocalBuilder()
                        .withNombre(noAsistente.getNombre())
                        .withEmail(noAsistente.getEmail())
                        .withNato(true)
                        .withPersonaId(noAsistente.getId())
                        .build(entityManager);

        MiembroLocal miembroQueDelegaVoto =
                new MiembroLocalBuilder()
                        .withNombre(elQueDelega.getNombre())
                        .withEmail(elQueDelega.getEmail())
                        .withPersonaId(elQueDelega.getId())
                        .build(entityManager);

        MiembroLocal miembroDelegado =
                new MiembroLocalBuilder()
                        .withNombre(delegado.getNombre())
                        .withEmail(delegado.getEmail())
                        .withPersonaId(delegado.getId())
                        .build(entityManager);

        Set<PuntoOrdenDia> puntosOrdenDia = new HashSet<>();
        puntosOrdenDia.add(puntoOrdenDiaAbierto);
        reunion =
                new ReunionBuilder()
                        .withDelegacionVoto()
                        .withAdmiteCambioVoto(true)
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAvisoPrimeraReunion(true)
                        .withAcuerdos("Acuerdos")
                        .withCreadorId(CREADOR_ID)
                        .withPuntosOrdenDia(puntosOrdenDia)
                        .build(entityManager);

        reunionConOrganoNoDelegacion =
                new ReunionBuilder()
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAvisoPrimeraReunion(true)
                        .withCreadorId(CREADOR_ID)
                        .build(entityManager);

        new OrganoReunionBuilder()
                .withReunion(reunionConOrganoNoDelegacion)
                .build(entityManager);

        TipoOrganoLocal tipoOrgano = new TipoOrganoBuilder().withNombre("tipoOrgano").build(entityManager);

        organoLocal = new OrganoLocalBuilder()
                .withCreadorId(persona.getId())
                .withTipoOrgano(tipoOrgano)
                .notOrdinario()
                .withDelegacionVotoMultiple(false)
                .build(entityManager);

        OrganoReunion organoReunion =
                new OrganoReunionBuilder()
                        .withOrganoId(organoLocal.getId().toString())
                        .withReunion(reunion)
                        .withExterno(false)
                        .build(entityManager);

        organoReunionMiembroSecretario =
                new OrganoReunionMiembroBuilder()
                        .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                        .withMiembro(validMember)
                        .withMiembroId(validMember.getPersonaId())
                        .withOrganoReunion(organoReunion)
                        .withAsistencia(false)
                        .build(entityManager);

        organoReunionMiembroNoAsistente =
                new OrganoReunionMiembroBuilder()
                        .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                        .withMiembro(noAsistenteValidMember)
                        .withMiembroId(noAsistenteValidMember.getPersonaId())
                        .withOrganoReunion(organoReunion)
                        .withAsistencia(false)
                        .build(entityManager);


        organoReunionMiembroDelegado =
                new OrganoReunionMiembroBuilder()
                        .withCargo("NOT", "Notario")
                        .withMiembro(miembroDelegado)
                        .withMiembroId(miembroDelegado.getPersonaId())
                        .withOrganoReunion(organoReunion)
                        .withAsistencia(true)
                        .build(entityManager);

        organoReunionQueDelega = new OrganoReunionMiembroBuilder()
                .withCargo(CodigoCargoEnum.GERENTE.codigo, "Gerente")
                .withMiembro(miembroQueDelegaVoto)
                .withMiembroId(miembroQueDelegaVoto.getPersonaId())
                .withOrganoReunion(organoReunion)
                .withAsistencia(false)
                .withVotoDelegado(organoReunionMiembroDelegado)
                .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test(expected = ReunionNoAdmiteDelegacionVotoException.class)
    public void shouldThrowErrorWhenOrganoReunionNoAdmiteDelegacionVoto() throws Exception {
        organoReunionMiembroService.estableceDelegadoVoto(reunionConOrganoNoDelegacion.getId(), persona.getId(), organoReunionMiembroDelegado
                        .getId(),
                "alguien a quien delegar", "alguienaquiendelegar@test.com", organoReunionMiembroSecretario.getId());
    }

    @Test
    @Ignore
    public void shouldGetReunionWhenMiembroHasVotoDelegado() {
        Assert.assertTrue(reunionDAO.tieneAcceso(reunion.getId(), Long.valueOf(organoReunionQueDelega.getMiembroId())));
        ReunionTemplate reunionTemplateDesdeReunion =
                reunionService.getReunionTemplateDesdeReunion(reunion, elQueDelega.getId(), true, true);
        Assert.assertNotNull(reunionTemplateDesdeReunion);
    }

    @Test(expected = UsuarioYaHaVotadoException.class)
    public void shouldThrowUsuarioYaHaVotadoExceptionWhenDelegarVotoDespuesDeVotar() throws Exception {
        organoReunionMiembroSecretario.setAsistencia(true);
        organoReunionMiembroSecretario.setAsistenciaConfirmada(true);
        entityManager.merge(organoReunionMiembroSecretario);
        forceSqlViewUpdate();

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotarFavor);

        organoReunionMiembroSecretario.setAsistencia(false);
        organoReunionMiembroSecretario.setAsistenciaConfirmada(false);
        entityManager.merge(organoReunionMiembroSecretario);
        forceSqlViewUpdate();

        organoReunionMiembroService.estableceDelegadoVoto(reunion.getId(), persona.getId(), organoReunionMiembroDelegado
                        .getId(),
                "alguien a quien delegar", "alguienaquiendelegar@test.com", organoReunionMiembroSecretario.getId());
    }

    @Test(expected = UsuarioYaHaVotadoException.class)
    public void shouldThrowUsuarioYaHaVotadoExceptionWhenBorrarDelegacionVotoDespuesDeVotarElDelegado() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoDelegado(delegado, organoReunionQueDelega.getId(), puntoAVotarFavor);
        forceSqlViewUpdate();

        organoReunionMiembroService.borraDelegadoVoto(reunion.getId(), organoReunionQueDelega
                .getId());
    }

    @Test
    public void shouldVotarComoDelegado() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoDelegado(delegado, organoReunionQueDelega.getId(), puntoAVotarFavor);
    }

    @Test(expected = AsistenteNoPuedeDelegarVotoException.class)
    public void shouldThrowAsistenteNoPuedeDelegarVotoExceptionOnDelegarVotoIfMiembroAsiste() throws Exception {
        organoReunionMiembroSecretario.setAsistencia(true);
        organoReunionMiembroSecretario.setAsistenciaConfirmada(true);
        entityManager.merge(organoReunionMiembroSecretario);
        forceSqlViewUpdate();

        organoReunionMiembroService.estableceDelegadoVoto(reunion.getId(), persona.getId(), organoReunionMiembroDelegado
                        .getId(),
                "alguien a quien delegar", "alguienaquiendelegar@test.com", organoReunionMiembroSecretario.getId());
    }

    @Test(expected = NoPuedeDelegarVotoEnMiembroNoAsistenteException.class)
    public void shouldThrowNoPuedeDelegarVotoEnMiembroNoAsistenteExceptionOnDelegarVotoIfDelegadoNoAsiste() throws Exception {
        organoReunionMiembroDelegado.setAsistencia(false);
        organoReunionMiembroDelegado.setAsistenciaConfirmada(false);
        entityManager.merge(organoReunionMiembroDelegado);
        forceSqlViewUpdate();

        organoReunionMiembroService.estableceDelegadoVoto(reunion.getId(), persona.getId(), delegado.getId(),
                "alguien a quien delegar", "alguienaquiendelegar@test.com", organoReunionMiembroSecretario.getId());
    }

    @Test
    public void shouldDelegarVoto() throws Exception {
        OrganoParametro organoParametro = new OrganoParametro();
        organoParametro.setOrganoParametroPK(new OrganoParametroPK(organoLocal.getId().toString(), false));
        organoParametro.setDelegacionVotoMultiple(true);
        entityManager.merge(organoParametro);

        organoReunionMiembroService.estableceDelegadoVoto(reunion.getId(), persona.getId(), delegado
                        .getId(),
                "alguien a quien delegar", "alguienaquiendelegar@test.com", organoReunionMiembroSecretario.getId());
    }

    @Test(expected = DelegadoVotoYaAsignadoAnteriormente.class)
    public void shouldThrowErrorWhenDelegarVotoMiembroConDelegacionOrganoNoAdmiteDelegacionMultiple() throws Exception {
        /*OrganoParametro organoParametro = new OrganoParametro();
        organoParametro.setOrganoParametroPK(new OrganoParametroPK(organoLocal.getId().toString(), false));
        organoParametro.setDelegacionVotoMultiple(false);
        entityManager.persist(organoParametro);*/

        forceSqlViewUpdate();

        organoReunionMiembroService.estableceDelegadoVoto(reunion.getId(), persona.getId(), delegado
                        .getId(),
                "alguien a quien delegar", "alguienaquiendelegar@test.com", organoReunionMiembroSecretario.getId());

        organoReunionMiembroService.estableceDelegadoVoto(reunion.getId(), persona.getId(), delegado
                        .getId(),
                "alguien a quien delegar", "alguienaquiendelegar@test.com", organoReunionMiembroNoAsistente.getId());
    }

    @Test(expected = VotoDelegadoException.class)
    public void shouldThrowExceptionWhenElQueHaDelegadoElVotoIntentaVotar()
            throws PuntoSecretoNoAdmiteCambioVotoException, UsuarioNoTienePermisoParaVotarException,
            TipoVotoNoExisteException, PuntoNoVotableException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException, VotoDelegadoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, AsistenciaNoConfirmadaException,
            VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(elQueDelega, puntoAVotarFavor);
    }

    @Test
    public void shouldVotarWhenHasVotoDelegado()
            throws PuntoSecretoNoAdmiteCambioVotoException, UsuarioNoTienePermisoParaVotarException,
            TipoVotoNoExisteException, PuntoNoVotableException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException, VotoDelegadoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, AsistenciaNoConfirmadaException, VotoNoDelegadoException,
            VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(delegado, puntoAVotarFavor);
        forceSqlViewUpdate();
        votacionService.votarPuntoDelegado(delegado, organoReunionQueDelega.getId(), puntoAVotarFavor);
    }

    @Test(expected = NoSePuedeActualizarVotoInexistenteException.class)
    public void shouldThrowNoSePuedeActualizarVotoInexistenteExceptionWhenActualizarVotoComoTitularWhenHasVotoDelegado()
            throws PuntoSecretoNoAdmiteCambioVotoException, UsuarioNoTienePermisoParaVotarException,
            TipoVotoNoExisteException, PuntoNoVotableException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException, VotoDelegadoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, AsistenciaNoConfirmadaException, VotoNoDelegadoException,
            ReunionNoAdmiteCambioVotoException, NoSePuedeActualizarVotoInexistenteException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());

        votacionService.votarPuntoDelegado(delegado, organoReunionQueDelega.getId(), puntoAVotarFavor);
        PuntoVotableActualizar puntoAActualizar = new PuntoVotableActualizar(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.actualizarVotoTitular(delegado, puntoAActualizar);
    }

    @Test(expected = VotoDelegadoException.class)
    public void shouldThrowVotoDelegadoExceptionWhenActualizarVotoTitularWhenHasVotoDelegado()
            throws PuntoSecretoNoAdmiteCambioVotoException, UsuarioNoTienePermisoParaVotarException,
            TipoVotoNoExisteException, PuntoNoVotableException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException, VotoDelegadoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, AsistenciaNoConfirmadaException, VotoNoDelegadoException,
            ReunionNoAdmiteCambioVotoException, NoSePuedeActualizarVotoInexistenteException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());

        votacionService.votarPuntoDelegado(delegado, organoReunionQueDelega.getId(), puntoAVotarFavor);
        PuntoVotableActualizar puntoAActualizar = new PuntoVotableActualizar(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.actualizarVotoDelegado(elQueDelega, organoReunionQueDelega.getId(), puntoAActualizar);
    }

    @Test
    public void shouldActualizarVotoDelegadoWhenHasVotoDelegado()
            throws PuntoSecretoNoAdmiteCambioVotoException, UsuarioNoTienePermisoParaVotarException,
            TipoVotoNoExisteException, PuntoNoVotableException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException, VotoDelegadoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, AsistenciaNoConfirmadaException, VotoNoDelegadoException,
            ReunionNoAdmiteCambioVotoException, NoSePuedeActualizarVotoInexistenteException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());

        votacionService.votarPuntoDelegado(delegado, organoReunionQueDelega.getId(), puntoAVotarFavor);
        PuntoVotableActualizar puntoAActualizar = new PuntoVotableActualizar(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.actualizarVotoDelegado(delegado, organoReunionQueDelega.getId(), puntoAActualizar);
    }

    @Test
    public void votosEmitidosInPuntosAsDelegadoShouldContainsTitularYDelegadoAsEmisorVoto() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoDelegado(delegado, organoReunionQueDelega.getId(), puntoAVotar);
        forceSqlViewUpdate();

        List<PuntoOrdenDiaRecuentoVotosDTO> recuentoVotosByReunionId = votacionService.getRecuentoVotosByReunion(reunion);
        PuntoOrdenDiaRecuentoVotosDTO votoEmitido = recuentoVotosByReunionId.get(0);
        VotanteVoto nombreVotante = votoEmitido.getVotantesNombre().get(0);
        Assert.assertEquals(nombreVotante.getNombreTitular(), elQueDelega.getNombre());
        Assert.assertEquals(nombreVotante.getNombreDelegado(), delegado.getNombre());

    }
}

