package es.uji.apps.goc.services;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import org.junit.Before;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ActiveProfiles("test")
@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class BaseServiceTest {
    @PersistenceContext
    protected EntityManager entityManager;

    final Long CREADOR_ID = 88849L;

    public TipoOrganoLocal tipoOrganoLocal;

    public OrganoLocal organoLocalConMiembros;

    public Reunion reunionConOrganoYMiembros;

    public MiembroLocal miembroYAutorizdo;

    @Before
    public void baseSetUp()
    {
        tipoOrganoLocal = new TipoOrganoBuilder().withCodigo("2").withNombre("TEST").build(entityManager);

        miembroYAutorizdo = new MiembroLocalBuilder().withEmail("admin@4tic.com").withPersonaId(CREADOR_ID).build(entityManager);

        organoLocalConMiembros =
                new OrganoLocalBuilder().withNombre("organoLocalConMiembros").withTipoOrgano(tipoOrganoLocal).withCreadorId(CREADOR_ID)
                        .withMiembro(miembroYAutorizdo)
                        .build(entityManager);

        reunionConOrganoYMiembros = new ReunionBuilder().withAsunto("Reunion").withOrgano(organoLocalConMiembros).build(entityManager);
        new OrganoAutorizadoBuilder().withOrganoId(organoLocalConMiembros.getId().toString()).withPersonaId(miembroYAutorizdo.getPersonaId()).withOrganoExterno(false).build(entityManager);
        entityManager.flush();
        entityManager.clear();
    }

    protected void forceSqlViewUpdate() {
        entityManager.flush();
        entityManager.clear();
    }
}
