package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.PersonaPuntoVotoDTO;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.TipoVotoNoExisteException;

public class PersonaPuntoVotoBuilder {

    private PersonaPuntoVotoDTO personaPuntoVotoDTO;

    public PersonaPuntoVotoBuilder(PuntoOrdenDia puntoOrdenDia){
        personaPuntoVotoDTO = new PersonaPuntoVotoDTO();
        personaPuntoVotoDTO.setPuntoOrdenDia(puntoOrdenDia);
    }

    public PersonaPuntoVotoBuilder withVotoAFavor(OrganoReunionMiembro organoReunionMiembro) throws TipoVotoNoExisteException {
        personaPuntoVotoDTO.setVoto(TipoVotoEnum.FAVOR.name());
        personaPuntoVotoDTO.setOrganoReunionMiembro(organoReunionMiembro);
        personaPuntoVotoDTO.setPersonaId(1L);

        return this;
    }

    public PersonaPuntoVotoDTO build(EntityManager entityManager) {
        entityManager.persist(personaPuntoVotoDTO);

        return personaPuntoVotoDTO;
    }
}
