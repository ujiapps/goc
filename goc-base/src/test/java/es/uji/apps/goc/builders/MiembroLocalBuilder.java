package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.model.Miembro;

public class MiembroLocalBuilder
{
    MiembroLocal miembroLocal;

    public MiembroLocalBuilder()
    {
        this.miembroLocal = new MiembroLocal();
    }

    public MiembroLocalBuilder withId(Long id)
    {
        this.miembroLocal.setId(id);
        return this;
    }

    public MiembroLocalBuilder withNombre(String nombre)
    {
        this.miembroLocal.setNombre(nombre);
        return this;
    }

    public MiembroLocalBuilder withEmail(String email)
    {
        this.miembroLocal.setEmail(email);
        return this;
    }

    public MiembroLocalBuilder withPersonaId(Long personaId)
    {
        this.miembroLocal.setPersonaId(personaId);
        return this;
    }

    public MiembroLocalBuilder withOrganoLocal(OrganoLocal organoLocal)
    {
        this.miembroLocal.setOrgano(organoLocal);
        return this;
    }

    public MiembroLocalBuilder withNato(Boolean nato)
    {
        this.miembroLocal.setNato(nato);
        return this;
    }

    public MiembroLocal build(EntityManager entityManager)
    {
        entityManager.persist(miembroLocal);
        return miembroLocal;
    }
}
