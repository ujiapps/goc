package es.uji.apps.goc.services;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dao.VotacionDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.CodigoCargoEnum;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.ReunionAVotar;
import es.uji.apps.goc.model.punto.PuntoVotable;
import es.uji.apps.goc.model.punto.PuntoVotableActualizar;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class VotacionServiceTest extends BaseServiceTest {
    private static final Long CREADOR_ID = 2222l;
    private static final Long PERSONA_ID = 1L;
    private static final String PERSONA_NAME = "persona";
    private static final String PERSONA_EMAIL = "persona@email.com";

    private static final Long AUTORIZADO_ID = 2L;
    private static final String AUTORIZADO_NAME = "Autorizado";
    private static final String AUTORIZADO_EMAIL = "autorizado@test.com";

    private static final Long SUPLENTE_ID = 3L;
    private static final String SUPLENTE_NAME = "Suplente";
    private static final String SUPLENTE_EMAIL = "suplente@test.com";
    @Autowired
    public VotacionService votacionService;
    @Autowired
    public VotacionDAO votacionDAO;
    @Autowired
    public OrganoReunionMiembroService organoReunionMiembroService;
    @Autowired
    public ReunionDAO reunionDAO;
    @PersistenceContext
    protected EntityManager entityManager;
    @Autowired
    PersonalizationConfig personalizationConfig;
    private Reunion reunion;
    private Persona invalidPersona;
    private PuntoOrdenDia puntoOrdenDiaAbierto;
    private PuntoOrdenDia puntoOrdenDiaSecreto;
    private PuntoOrdenDia puntoNoVotable;
    private PuntoOrdenDia puntoOrdinario;
    private OrganoReunionMiembro organoReunionMiembroSecretario;
    private OrganoReunionMiembro organoReunionMiembroNotario;
    private OrganoReunionMiembro organoReunionMiembroNoAsistente;
    private Persona persona;
    private Persona autorizado;
    private Persona suplente;
    private Persona personaSinAsistencia;
    private Long miembroId;

    @Before
    public void setUp() throws InterruptedException {
        persona = new Persona(PERSONA_ID, PERSONA_NAME, PERSONA_EMAIL);
        invalidPersona = new Persona(123456L, "ABC", "prueba@gmail.com");
        autorizado = new Persona(AUTORIZADO_ID, AUTORIZADO_NAME, AUTORIZADO_EMAIL);
        suplente = new Persona(SUPLENTE_ID, SUPLENTE_NAME, SUPLENTE_EMAIL);
        personaSinAsistencia = new Persona(300L, "Carlos", "carev@gmail.com");

        this.puntoOrdenDiaAbierto
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(true)
                .withVotacionAbierta(true)
                .withProcedimientoAbreviado()
                .withTipoVotacion()
                .build(entityManager);

        this.puntoOrdenDiaSecreto
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(false)
                .withVotacionAbierta(true)
                .withProcedimientoAbreviado()
                .withTipoVotacion()
                .build(entityManager);

        this.puntoNoVotable
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(null)
                .build(entityManager);

        this.puntoOrdinario
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(true)
                .withProcedimientoOrdinario()
                .withVotacionAbierta(false)
                .withTipoVotacion()
                .build(entityManager);

        Set<PuntoOrdenDia> puntosOrdenDia = new HashSet<>();
        puntosOrdenDia.add(puntoOrdenDiaAbierto);
        puntosOrdenDia.add(puntoOrdenDiaSecreto);
        puntosOrdenDia.add(puntoNoVotable);
        puntosOrdenDia.add(puntoOrdinario);

        MiembroLocal validMember =
                new MiembroLocalBuilder()
                        .withNombre("Pedro")
                        .withNato(true)
                        .withPersonaId(persona.getId())
                        .build(entityManager);

        MiembroLocal invalidMember =
                new MiembroLocalBuilder()
                        .withNombre("María")
                        .withPersonaId(invalidPersona.getId())
                        .build(entityManager);

        MiembroLocal miembroSinAsistencia =
                new MiembroLocalBuilder()
                        .withNombre("Carlos")
                        .withNato(true)
                        .withPersonaId(personaSinAsistencia.getId())
                        .build(entityManager);

        reunion =
                new ReunionBuilder()
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAvisoPrimeraReunion(true)
                        .withAcuerdos("Acuerdos")
                        .withCreadorId(CREADOR_ID)
                        .withPuntosOrdenDia(puntosOrdenDia)
                        .withFecha(new Date())
                        .build(entityManager);

        TipoOrganoLocal tipoOrgano = new TipoOrganoBuilder().withNombre("tipo").build(entityManager);

        OrganoLocal organo = new OrganoLocalBuilder()
                .withNombre("OrganoVotar")
                .withTipoOrgano(tipoOrgano)
                .withTipoProcedimientoVotacion(TipoProcedimientoVotacionEnum.ABREVIADO.name())
                .build(entityManager);

        OrganoReunion organoReunion =
                new OrganoReunionBuilder()
                        .withOrganoId(organo.getId().toString())
                        .withReunion(reunion)
                        .build(entityManager);

        organoReunionMiembroSecretario =
                new OrganoReunionMiembroBuilder()
                        .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                        .withEmail("miembro@4tic.com")
                        .withMiembroId(validMember.getPersonaId())
                        .withOrganoReunion(organoReunion)
                        .withAsistencia(true)
                        .withEmail(persona.getEmail())
                        .build(entityManager);

        organoReunionMiembroNotario =
                new OrganoReunionMiembroBuilder()
                        .withCargo("NOT", "Notario")
                        .withEmail("miembro@4tic.com")
                        .withMiembroId(invalidMember.getPersonaId())
                        .withOrganoReunion(organoReunion)
                        .withAsistencia(true)
                        .withEmail(invalidMember.getEmail())
                        .build(entityManager);

        organoReunionMiembroNoAsistente =
                new OrganoReunionMiembroBuilder()
                        .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                        .withMiembroId(miembroSinAsistencia.getPersonaId())
                        .withOrganoReunion(organoReunion)
                        .withAsistencia(false)
                        .withEmail(miembroSinAsistencia.getEmail())
                        .build(entityManager);

        this.miembroId = validMember.getPersonaId();

        puntoOrdinario.setReunion(reunion);
        puntoOrdenDiaSecreto.setReunion(reunion);
        puntoOrdinario.setReunion(reunion);
        entityManager.merge(organo);
        entityManager.merge(puntoOrdenDiaAbierto);
        entityManager.merge(puntoOrdenDiaSecreto);
        entityManager.merge(puntoOrdinario);


        entityManager.merge(organoReunionMiembroSecretario);
        entityManager.merge(organoReunionMiembroNotario);
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void shouldValidMemberAbleToVotarPuntoAFavor()
            throws TipoVotoNoExisteException, NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion,
            ReunionNoAbiertaException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
        PersonaPuntoVotoDTO personaPuntoVotoDTO = votacionService
                .getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(miembroId.toString(), puntoAVotar.getPuntoOrdenDia().getId());

        assertThat(personaPuntoVotoDTO.getPuntoOrdenDia().getId(), is(puntoOrdenDiaAbierto.getId()));
        assertThat(personaPuntoVotoDTO.getVoto(), is(TipoVotoEnum.FAVOR.name()));
    }

    @Test
    public void shouldValidMemberAbleToVotarPuntoEnContra()
            throws TipoVotoNoExisteException, NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion,
            ReunionNoAbiertaException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
        PersonaPuntoVotoDTO personaPuntoVotoDTO = votacionService
                .getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(miembroId.toString(), puntoAVotar.getPuntoOrdenDia().getId());

        assertThat(personaPuntoVotoDTO.getPuntoOrdenDia().getId(), is(puntoOrdenDiaAbierto.getId()));
        assertThat(personaPuntoVotoDTO.getVoto(), is(TipoVotoEnum.CONTRA.name()));
    }

    @Test
    public void shouldValidMemberAbleToVotarPuntoEnBlanco()
            throws TipoVotoNoExisteException, NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion,
            ReunionNoAbiertaException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.BLANCO.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
        PersonaPuntoVotoDTO personaPuntoVotoDTO = votacionService
                .getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(miembroId.toString(), puntoAVotar.getPuntoOrdenDia().getId());

        assertThat(personaPuntoVotoDTO.getPuntoOrdenDia().getId(), is(puntoOrdenDiaAbierto.getId()));
        assertThat(personaPuntoVotoDTO.getVoto(), is(TipoVotoEnum.BLANCO.name()));
    }

    @Test(expected = NoVotacionTelematicaException.class)
    public void shouldThrowErrorWhenVotarPuntoWithNoVotacionTelematica()
            throws TipoVotoNoExisteException, NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion,
            PuntoSecretoNoAdmiteCambioVotoException, ReunionNoAbiertaException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        reunion.setVotacionTelematica(false);
        entityManager.merge(reunion);

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }

    @Test
    public void shouldDejarVotarPuntoWithReunionNoTelematica()
            throws TipoVotoNoExisteException, NoVotacionTelematicaException,
            ReunionNoAbiertaException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            UsuarioNoTienePermisoParaVotarException, PuntoNoVotableException, VotoDelegadoException,
            AsistenciaNoConfirmadaException, PuntoNoVotableEnContraException, VotacionNoAbiertaException, NoEsTelematicaExceptionConVotacion, VotoException {
        reunion.setTelematica(false);
        entityManager.merge(reunion);

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);

        PersonaPuntoVotoDTO personaPuntoVotoDTO = votacionService
                .getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(miembroId.toString(), puntoAVotar.getPuntoOrdenDia().getId());

        assertThat(personaPuntoVotoDTO.getPuntoOrdenDia().getId(), is(puntoOrdenDiaAbierto.getId()));
        assertThat(personaPuntoVotoDTO.getVoto(), is(TipoVotoEnum.FAVOR.name()));
    }


    @Test
    public void shouldValidMemberAbleToActualizarVoto()
            throws TipoVotoNoExisteException, ReunionNoAdmiteCambioVotoException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, PuntoSecretoNoAdmiteCambioVotoException,
            NoSePuedeActualizarVotoInexistenteException, ReunionNoAbiertaException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        reunion.setAdmiteCambioVoto(true);
        entityManager.merge(reunion);

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);

        PuntoVotableActualizar
                puntoAActualizar = new PuntoVotableActualizar(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.actualizarVotoTitular(persona, puntoAActualizar);

        PersonaPuntoVotoDTO votoById = votacionService.getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(miembroId.toString(), puntoOrdenDiaAbierto.getId());
        assertThat(votoById.getVoto(), is(TipoVotoEnum.CONTRA.name()));
    }

    @Test(expected = ReunionNoAdmiteCambioVotoException.class)
    public void shouldThrowExceptionOnActualizarVotoInReunionAdmiteCambioVotoFalse()
            throws TipoVotoNoExisteException, ReunionNoAdmiteCambioVotoException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, PuntoSecretoNoAdmiteCambioVotoException,
            NoSePuedeActualizarVotoInexistenteException, ReunionNoAbiertaException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);

        PuntoVotableActualizar
                puntoAActualizar = new PuntoVotableActualizar(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.actualizarVotoTitular(persona, puntoAActualizar);
    }

    @Test
    public void shouldActualizarVotoSecreto()
            throws TipoVotoNoExisteException, ReunionNoAdmiteCambioVotoException,
            NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion, PuntoSecretoNoAdmiteCambioVotoException,
            NoSePuedeActualizarVotoInexistenteException, ReunionNoAbiertaException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {
        reunion.setAdmiteCambioVoto(true);
        entityManager.merge(reunion);

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);

        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaSecreto, reunionAVotar, TipoVotoEnum.BLANCO.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);

        PuntoVotableActualizar
                puntoAActualizar = new PuntoVotableActualizar(puntoOrdenDiaSecreto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.actualizarVotoTitular(persona, puntoAActualizar);

        PersonaPuntoVotoDTO votoById = votacionService.getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(miembroId.toString(), puntoOrdenDiaSecreto.getId());
        assertThat(votoById.getVoto(), is(TipoVotoEnum.FAVOR.name()));
    }

    @Test(expected = NoSePuedeActualizarVotoInexistenteException.class)
    public void shouldThrowExceptionOnActualizarVotoNoExistente()
            throws NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion,
            PuntoSecretoNoAdmiteCambioVotoException, ReunionNoAdmiteCambioVotoException, TipoVotoNoExisteException,
            NoSePuedeActualizarVotoInexistenteException, ReunionNoAbiertaException, PuntoNoVotableException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        reunion.setAdmiteCambioVoto(true);
        entityManager.merge(reunion);

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotableActualizar
                puntoAActualizar = new PuntoVotableActualizar(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.actualizarVotoTitular(persona, puntoAActualizar);
    }

    @Test(expected = ReunionNoAbiertaException.class)
    public void shouldThrowExceptionOnVotarAndReunionIsNotAbierta()
            throws NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion, TipoVotoNoExisteException,
            ReunionNoAbiertaException, PuntoNoVotableException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException {
        reunion.setAvisoPrimeraReunionFecha(null);
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
    }

    @Test(expected = ReunionYaCompletadaException.class)
    public void shouldThrowErrorWhenAbrirVotacionAndReunionIsCompleta()
            throws ReunionYaCompletadaException, CargoNoSecretarioNoPuedeAbrirVotacionException,
            PuntoProcedimientoNoVotable, NoEsTelematicaExceptionConVotacion, VotacionNoAbiertaException {
        reunion.setCompletada(true);
        entityManager.merge(reunion);

        votacionService.abreVotacionPunto(puntoOrdinario.getId(), persona);
    }

    @Test(expected = CargoNoSecretarioNoPuedeAbrirVotacionException.class)
    public void shouldThrowErrorWhenMiembroNotSecretarioAbreVotacion()
            throws ReunionYaCompletadaException, CargoNoSecretarioNoPuedeAbrirVotacionException,
            PuntoProcedimientoNoVotable, NoEsTelematicaExceptionConVotacion, VotacionNoAbiertaException {
        votacionService.abreVotacionPunto(puntoOrdinario.getId(), invalidPersona);
    }

    @Test()
    public void secretarioShouldBeAbleToCloseVotacionPuntoOrdinario()
            throws ReunionYaCompletadaException, CargoNoSecretarioNoPuedeAbrirVotacionException,
            PuntoProcedimientoNoVotable, NoEsTelematicaExceptionConVotacion, VotacionNoAbiertaException {
        puntoOrdinario.setVotacionAbierta(true);
        entityManager.merge(puntoOrdinario);

        votacionService.cierraVotacionPunto(puntoOrdinario.getId(), persona);
    }

    @Test(expected = VotacionNoAbiertaException.class)
    public void shouldThrowErrorWhenCloseVotacionPuntoOrdinarioConVotacionAbiertaFalse()
            throws PuntoProcedimientoNoVotable, ReunionYaCompletadaException, CargoNoSecretarioNoPuedeAbrirVotacionException,
            VotacionNoAbiertaException, NoEsTelematicaExceptionConVotacion {
        votacionService.cierraVotacionPunto(puntoOrdinario.getId(), persona);
    }


    @Test
    public void secretarioShouldBeAbleToOpenVotacionPuntoAbreviado()
            throws ReunionYaCompletadaException,
            CargoNoSecretarioNoPuedeAbrirVotacionException, PuntoProcedimientoNoVotable,
            NoEsTelematicaExceptionConVotacion {

        votacionService.abreVotacionPunto(puntoOrdenDiaAbierto.getId(), persona);
    }

    @Test()
    public void secretarioShouldBeAbleToOpenVotacionPuntoOrdinario()
            throws ReunionYaCompletadaException, CargoNoSecretarioNoPuedeAbrirVotacionException,
            PuntoProcedimientoNoVotable, NoEsTelematicaExceptionConVotacion, VotacionNoAbiertaException {
        puntoOrdinario.setVotacionAbierta(false);
        entityManager.merge(puntoOrdinario);

        votacionService.abreVotacionPunto(puntoOrdinario.getId(), persona);
    }

    @Test()
    public void shouldReturnListOfMiembrosNatosWhenCloseVotacionPuntoOrdinarioAndFaltanMiembrosPorVotar()
            throws VotacionNoAbiertaException, ReunionYaCompletadaException, CargoNoSecretarioNoPuedeAbrirVotacionException,
            PuntoProcedimientoNoVotable, NoEsTelematicaExceptionConVotacion {
        puntoOrdinario.setVotacionAbierta(true);
        entityManager.merge(puntoOrdinario);

        personalizationConfig.miembrosNatoVotoObligado = true;
        List<OrganoReunionMiembro> miembrosNatosPorVotar =
                votacionService.cierraVotacionPunto(puntoOrdinario.getId(), persona);

        Assert.assertTrue(miembrosNatosPorVotar.size() > 0);
    }

    @Test()
    public void shouldReturnNullIfMiembrosNatosNoObligadosAVotarWhenCloseVotacionPuntoOrdinario()
            throws VotacionNoAbiertaException, ReunionYaCompletadaException, CargoNoSecretarioNoPuedeAbrirVotacionException,
            PuntoProcedimientoNoVotable, NoEsTelematicaExceptionConVotacion {
        puntoOrdinario.setVotacionAbierta(true);
        entityManager.merge(puntoOrdinario);

        personalizationConfig.miembrosNatoVotoObligado = false;
        List<OrganoReunionMiembro> miembrosNatosPorVotar =
                votacionService.cierraVotacionPunto(puntoOrdinario.getId(), persona);

        Assert.assertNull(miembrosNatosPorVotar);
    }

    @Test(expected = UsuarioYaHaVotadoSoloSePermiteActualizacionException.class)
    public void shouldThrowErrorWhenUsuarioYaHaVotadoPuntoWithVotacionAbierta()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
        entityManager.flush();
        entityManager.clear();
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }

    @Test(expected = UsuarioYaHaVotadoSoloSePermiteActualizacionException.class)
    public void shouldThrowErrorWhenUsuarioYaHaVotadoPuntoWithVotacionSecreta()
            throws NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaSecreto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
        entityManager.flush();
        entityManager.clear();
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }

    @Test(expected = UsuarioNoTienePermisoParaVotarException.class)
    public void shouldThrowErrorWhenMiembroAutorizadoVotaPuntoAbierto()
            throws NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(autorizado, puntoAVotar);
    }

    @Test(expected = UsuarioNoTienePermisoParaVotarException.class)
    public void shouldThrowErrorWhenMiembroAutorizadoVotaPuntoSecreto()
            throws NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaSecreto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(autorizado, puntoAVotar);
    }

    @Test(expected = PuntoNoVotableException.class)
    public void shouldThrowErrorWhenVotarPuntoNoVotable()
            throws NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, PuntoNoVotableException,
            UsuarioNoTienePermisoParaVotarException, VotoDelegadoException, AsistenciaNoConfirmadaException,
            VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoNoVotable, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }

    @Test
    @Ignore
    public void miembrosWithSuplentesShouldHasAccessToReunion() throws Exception {
        Long miembroId = Long.parseLong(organoReunionMiembroSecretario.getMiembroId());

        organoReunionMiembroService.estableceSuplente(
                reunion.getId(),
                persona.getId(),
                suplente.getId(),
                suplente.getNombre(),
                suplente.getEmail(),
                organoReunionMiembroSecretario.getId()
        );

        boolean miembroTieneAcceso = reunionDAO.tieneAcceso(reunion.getId(), miembroId);
        Assert.assertTrue(miembroTieneAcceso);
    }

    @Test(expected = VotoDelegadoException.class)
    public void shouldThrowErrorWhenVotanteEmiteVotoAnHasSuplente()
            throws Exception {
        organoReunionMiembroService.estableceSuplente(
                reunion.getId(),
                persona.getId(),
                suplente.getId(),
                suplente.getNombre(),
                suplente.getEmail(),
                organoReunionMiembroSecretario.getId()
        );

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }

    @Test(expected = PuntoNoVotableEnContraException.class)
    public void shouldThrowErrorWhenVotanteVotaEnContraSobrePuntoConVotacionEnContraDeshabilitada() throws NoVotacionTelematicaException,
            ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion, PuntoNoVotableException, PuntoNoVotableEnContraException,
            TipoVotoNoExisteException, UsuarioYaHaVotadoSoloSePermiteActualizacionException, PuntoSecretoNoAdmiteCambioVotoException,
            AsistenciaNoConfirmadaException, VotoDelegadoException, UsuarioNoTienePermisoParaVotarException, VotacionNoAbiertaException, VotoException {
        puntoOrdenDiaAbierto.setVotableEnContra(false);
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }

    public void shouldBeVotableVotaAFavorSobrePuntoConVotacionEnContraDeshabilitada() throws NoVotacionTelematicaException,
            ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion, PuntoNoVotableException, PuntoNoVotableEnContraException,
            TipoVotoNoExisteException, UsuarioYaHaVotadoSoloSePermiteActualizacionException, PuntoSecretoNoAdmiteCambioVotoException,
            AsistenciaNoConfirmadaException, VotoDelegadoException, UsuarioNoTienePermisoParaVotarException, VotacionNoAbiertaException, VotoException {
        puntoOrdenDiaAbierto.setVotableEnContra(false);
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }

    @Test(expected = VotacionNoAbiertaException.class)
    public void shouldThrowErrorWhenVotanteEmiteVotoAbreviadoAndFechaFinVotacionEsAnterior()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            PuntoNoVotableException, TipoVotoNoExisteException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            PuntoSecretoNoAdmiteCambioVotoException, AsistenciaNoConfirmadaException, VotoDelegadoException,
            UsuarioNoTienePermisoParaVotarException, VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {

        reunion.setFechaFinVotacion(Date.from(
                new GregorianCalendar(2019, Calendar.MAY, 3).toZonedDateTime().toInstant()));
        entityManager.flush();
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }

    @Test(expected = VotacionNoAbiertaException.class)
    public void shouldThrowErrorWhenVotanteEmiteVotoOrdinarioAndFechaFinVotacionEsAnterior()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            PuntoNoVotableException, TipoVotoNoExisteException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            PuntoSecretoNoAdmiteCambioVotoException, AsistenciaNoConfirmadaException, VotoDelegadoException,
            UsuarioNoTienePermisoParaVotarException, VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {
        puntoOrdinario.setVotacionAbierta(true);
        reunion.setFechaFinVotacion(Date.from(
                new GregorianCalendar(2019, Calendar.MAY, 3).toZonedDateTime().toInstant()));
        entityManager.flush();
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdinario, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
    }
    // Una reunio que te fecha fin de votacion anterior a ara, el usuari no pot votar un punt en votacio abreviada.
    // Una reunio que te fecha fin de votacion anterior a ara, el usuari no pot votar un punt en votacio ordinaria.

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowErrorWhenVotanteEmiteVotoAbreviadoAndVotacionEstaCerradaAndFechaFinVotacionEsPosterior()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            PuntoNoVotableException, TipoVotoNoExisteException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            PuntoSecretoNoAdmiteCambioVotoException, AsistenciaNoConfirmadaException, VotoDelegadoException,
            UsuarioNoTienePermisoParaVotarException, PuntoProcedimientoNoVotable,
            CargoNoSecretarioNoPuedeAbrirVotacionException, ReunionYaCompletadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {

        reunion.setFechaFinVotacion(Date.from(
                new GregorianCalendar(2059, Calendar.MAY, 3).toZonedDateTime().toInstant()));

        puntoOrdenDiaAbierto.setVotacionAbierta(false);

        entityManager.flush();

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);

        votacionService.abreVotacionPunto(puntoOrdenDiaAbierto.getId(), persona);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowErrorWhenVotanteEmiteVotoOrdinarioAndVotacionEstaCerradaAndFechaFinVotacionEsPosterior()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            PuntoNoVotableException, TipoVotoNoExisteException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            PuntoSecretoNoAdmiteCambioVotoException, AsistenciaNoConfirmadaException, VotoDelegadoException,
            UsuarioNoTienePermisoParaVotarException, PuntoProcedimientoNoVotable,
            CargoNoSecretarioNoPuedeAbrirVotacionException, ReunionYaCompletadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {

        reunion.setFechaFinVotacion(Date.from(
                new GregorianCalendar(2059, Calendar.MAY, 3).toZonedDateTime().toInstant()));

        entityManager.flush();

        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdinario, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);

        votacionService.abreVotacionPunto(puntoOrdinario.getId(), persona);
    }
    // Una reunio que te data fin de votacion posterior a ara, si la votacio del punt es tanca abans, el usuari no pot votar un punt en votacio abreviada entre les dos dates.
    // Una reunio que te data fin de votacion posterior a ara, si la votacio del punt es tanca abans, el usuari no pot votar un punt en votacio ordinaria entre les dos dates.


    // No es pot obrir manualment la votacio en un punt en el que la votacio siga abreviada.
    // Es pot obrir manualment la votacio en un punt en el que la votacio siga ordinaria.

    @Test(expected = AsistenciaNoConfirmadaException.class)
    public void shouldThrownExceptionWhenVotanteNoAsistente() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(personaSinAsistencia, puntoAVotar);
    }

    @Test(expected = VotacionYaRealizadaException.class)
    public void shouldThrownExceptionWhenHasVotadoYExcusasAsistencia() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(persona, puntoAVotar);
        entityManager.flush();
        entityManager.clear();
        organoReunionMiembroService.estableceAsistencia(
                organoReunionMiembroSecretario.getId(),
                persona.getId(),
                false,
                "Hola.");
    }
}

