package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.model.Organo;

public class OrganoReunionBuilder
{
    OrganoReunion organoReunion;

    public OrganoReunionBuilder()
    {
        this.organoReunion = new OrganoReunion();
        this.organoReunion.setExterno(false);
    }

    public OrganoReunionBuilder(Reunion reunion, Organo organo){
        this.organoReunion = new OrganoReunion();
        this.organoReunion.setReunion(reunion);
        this.organoReunion.setOrganoId(organo.getId());
        this.organoReunion.setExterno(organo.isExterno());
        this.organoReunion.setTipoOrganoId(organo.getTipoOrgano().getId());
        this.organoReunion.setOrganoNombre(organo.getNombre());
        this.organoReunion.setOrganoNombreAlternativo(organo.getNombreAlternativo());
        this.organoReunion.setConvocarSinOrdenDia(organo.getConvocarSinOrdenDia());
    }

    public OrganoReunionBuilder withId(Long id)
    {
        this.organoReunion.setId(id);
        return this;
    }

    public OrganoReunionBuilder withReunion(Reunion reunion)
    {
        this.organoReunion.setReunion(reunion);
        return this;
    }

    public OrganoReunionBuilder withOrganoId(String organoId)
    {
        this.organoReunion.setOrganoId(organoId);
        return this;
    }

    public OrganoReunionBuilder withOrganoNombre(String nombre)
    {
        this.organoReunion.setOrganoNombre(nombre);
        return this;
    }

    public OrganoReunionBuilder withTipoOrgano(TipoOrganoLocal tipoOrgano){
        this.organoReunion.setTipoOrganoId(tipoOrgano.getId());
        return this;
    }

    public OrganoReunionBuilder withExterno(Boolean externo){
        this.organoReunion.setExterno(externo);
        return this;
    }

    public OrganoReunion build(EntityManager entityManager)
    {
        entityManager.persist(this.organoReunion);
        return this.organoReunion;
    }
}
