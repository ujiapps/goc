package es.uji.apps.goc.builders;

import java.util.Date;
import java.util.HashSet;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoParametro;
import es.uji.apps.goc.dto.OrganoParametroPK;
import es.uji.apps.goc.dto.TipoOrganoLocal;

public class OrganoLocalBuilder
{

    OrganoLocal organoLocal;
    OrganoParametro organoParametro;
    boolean ordinario = true;

    public OrganoLocalBuilder()
    {
        this.organoLocal = new OrganoLocal();
        this.organoLocal.setInactivo(false);
    }

    public OrganoLocalBuilder withId(Long id)
    {
        this.organoLocal.setId(id);
        return this;
    }

    public OrganoLocalBuilder withNombre(String nombre)
    {
        this.organoLocal.setNombre(nombre);
        return this;
    }

    public OrganoLocalBuilder withTipoOrgano(TipoOrganoLocal tipoOrgano)
    {
        this.organoLocal.setTipoOrgano(tipoOrgano);
        return this;
    }

    public OrganoLocalBuilder withCreadorId(Long creadorId)
    {
        this.organoLocal.setCreadorId(creadorId);
        return this;
    }

    public OrganoLocalBuilder withMiembro(MiembroLocal miembroLocal) {
        HashSet<MiembroLocal> miembros = new HashSet<>();
        miembros.add(miembroLocal);
        this.organoLocal.setMiembros(miembros);
        return this;
    }

    public OrganoLocalBuilder isInactivo(Boolean inactivo){
        this.organoLocal.setInactivo(inactivo);
        return this;
    }

    public OrganoLocalBuilder withOrganoParametro(){
        if (organoParametro == null)
            organoParametro = new OrganoParametro();

        return this;
    }

    public OrganoLocalBuilder withConvocarSinOrdenDia(){
        if (organoParametro == null)
            organoParametro = new OrganoParametro();

        organoParametro.setConvocarSinOrdenDia(true);
        return this;
    }

    public OrganoLocalBuilder withDelegacionVotoMultiple(boolean delegacionDeVoto) {
        if (organoParametro == null)
            organoParametro = new OrganoParametro();

        organoParametro.setDelegacionVotoMultiple(delegacionDeVoto);
        return this;
    }

    public OrganoLocalBuilder withTipoProcedimientoVotacion(String tipoProcedimientoVotacion){
        if (organoParametro == null)
            organoParametro = new OrganoParametro();
        this.organoParametro.setTipoProcedimientoVotacion(tipoProcedimientoVotacion);
        return this;
    }

    public OrganoLocalBuilder notOrdinario() {
        this.ordinario = false;
        return this;
    }

    public OrganoLocalBuilder withVotoDoblePresidente(Boolean votoDoble) {
        if (organoParametro == null)
            organoParametro = new OrganoParametro();
        
        this.organoParametro.setPresidenteVotoDoble(votoDoble);

        return this;
    }
    
    public OrganoLocalBuilder withVerDelegaciones(Boolean verDelegaciones) {
    	 if (organoParametro == null)
             organoParametro = new OrganoParametro();
         
         this.organoParametro.setVerDelegaciones(verDelegaciones);

         return this;
    }

    public OrganoLocal build(EntityManager entityManager)
    {
        this.organoLocal.setFechaCreacion(new Date());
        entityManager.persist(organoLocal);
        if (organoParametro != null) {
            organoParametro.setOrganoParametroPK(new OrganoParametroPK(organoLocal.getId().toString(), ordinario));
            entityManager.persist(organoParametro);
        }
        return organoLocal;
    }


}
