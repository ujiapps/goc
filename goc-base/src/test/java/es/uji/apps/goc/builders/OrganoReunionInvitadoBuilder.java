package es.uji.apps.goc.builders;

import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionInvitado;
import es.uji.apps.goc.dto.OrganoReunionMiembro;

import javax.persistence.EntityManager;

public class OrganoReunionInvitadoBuilder
{
    OrganoReunionInvitado organoReunionInvitado;

    public OrganoReunionInvitadoBuilder(){
        this.organoReunionInvitado = new OrganoReunionInvitado();
    }

    public OrganoReunionInvitadoBuilder withId(Long id)
    {
        this.organoReunionInvitado.setId(id);
        return this;
    }

    public OrganoReunionInvitadoBuilder withOrganoReunion(OrganoReunion organoReunion)
    {
        this.organoReunionInvitado.setOrganoReunion(organoReunion);
        return this;
    }

    public OrganoReunionInvitadoBuilder withNombre(String nombre)
    {
        this.organoReunionInvitado.setNombre(nombre);
        return this;
    }

    public OrganoReunionInvitadoBuilder withEmail(String email)
    {
        this.organoReunionInvitado.setEmail(email);
        return this;
    }
    public OrganoReunionInvitadoBuilder withReunionId(Long reunionId)
    {
        this.organoReunionInvitado.setReunionId(reunionId);
        return this;
    }

    public OrganoReunionInvitado build(EntityManager entityManager)
    {
        entityManager.persist(this.organoReunionInvitado);
        return this.organoReunionInvitado;
    }
}
