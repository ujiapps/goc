package es.uji.apps.goc.services;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dao.VotacionDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.CodigoCargoEnum;
import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.NoPuedeEliminarSuplenteSiYaSeHaVotadoException;
import es.uji.apps.goc.exceptions.UsuarioYaHaVotadoException;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.ReunionAVotar;
import es.uji.apps.goc.model.punto.PuntoVotable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class SuplenciaVotacionTest extends BaseServiceTest {
    private static final Long CREADOR_ID = 2222l;
    private static final Long PERSONA_ID = 1L;
    private static final String PERSONA_NAME = "persona";
    private static final String PERSONA_EMAIL = "persona@email.com";

    private static final Long SUPLENTE_ID = 2L;
    private static final String SUPLENTE_NAME = "Suplente";
    private static final String SUPLENTE_EMAIL = "suplente@test.com";
    @Autowired
    public VotacionDAO votacionDAO;
    @Autowired
    public ReunionDAO reunionDAO;
    @Autowired
    public VotacionService votacionService;
    @Autowired
    public OrganoReunionMiembroService organoReunionMiembroService;
    @PersistenceContext
    protected EntityManager entityManager;
    private Persona titular;
    private Persona suplente;
    private Reunion reunion;
    private PuntoOrdenDia puntoOrdenDiaAbierto;
    private PuntoOrdenDia puntoOrdenDiaSecreto;
    private OrganoReunionMiembro organoReunionMiembroWithSuplente;

    @Before
    public void setUp() {
        titular = new Persona(PERSONA_ID, PERSONA_NAME, PERSONA_EMAIL);
        suplente = new Persona(SUPLENTE_ID, SUPLENTE_NAME, SUPLENTE_EMAIL);

        puntoOrdenDiaAbierto = new PuntoOrdenDiaBuilder()
                .withTipoVoto(true)
                .withVotacionAbierta(true)
                .withMayoriaSimple()
                .withProcedimientoAbreviado()
                .withTipoVotacion()
                .build(entityManager);

        puntoOrdenDiaSecreto = new PuntoOrdenDiaBuilder()
                .withTipoVoto(false)
                .withVotacionAbierta(true)
                .withMayoriaSimple()
                .withProcedimientoAbreviado()
                .withTipoVotacion()
                .build(entityManager);

        Set<PuntoOrdenDia> puntosOrdenDia = new HashSet<>();
        puntosOrdenDia.add(puntoOrdenDiaAbierto);
        puntosOrdenDia.add(puntoOrdenDiaSecreto);

        MiembroLocal titularMember =
                new MiembroLocalBuilder()
                        .withNombre(titular.getNombre())
                        .withPersonaId(titular.getId())
                        .withEmail(titular.getEmail())
                        .withNato(true)
                        .build(entityManager);

        reunion =
                new ReunionBuilder()
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAvisoPrimeraReunion(true)
                        .withAcuerdos("Acuerdos")
                        .withCreadorId(CREADOR_ID)
                        .withPuntosOrdenDia(puntosOrdenDia)
                        .withFecha(new Date())
                        .build(entityManager);

        OrganoReunion organoReunion =
                new OrganoReunionBuilder()
                        .withOrganoId("4534")
                        .withReunion(reunion)
                        .build(entityManager);

        organoReunionMiembroWithSuplente =
                new OrganoReunionMiembroBuilder()
                        .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                        .withEmail(titular.getEmail())
                        .withMiembroId(titularMember.getId())
                        .withSuplente(suplente)
                        .withNombre(titular.getNombre())
                        .withOrganoReunion(organoReunion)
                        .withAsistencia(true)
                        .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void suplenteShouldBeAbleToEmitirVoto() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(suplente, puntoAVotar);
    }

    @Test(expected = UsuarioYaHaVotadoException.class)
    public void shouldThrowErrorWhenMiembroSelectSuplenteAndHasVotosEmitidos() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(suplente, puntoAVotar);

        forceSqlViewUpdate();

        organoReunionMiembroService.estableceSuplente(
                reunion.getId(),
                titular.getId(),
                suplente.getId(),
                suplente.getNombre(),
                suplente.getEmail(),
                organoReunionMiembroWithSuplente.getId()
        );
    }

    @Test(expected = NoPuedeEliminarSuplenteSiYaSeHaVotadoException.class)
    public void shouldThrowErrorWhenBorrarSuplenciaAndHasVotosEmitidos() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaAbierto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(suplente, puntoAVotar);

        forceSqlViewUpdate();

        organoReunionMiembroService.borraSuplente(
                reunion.getId(),
                organoReunionMiembroWithSuplente.getId(),
                titular.getId()
        );
    }

    @Test()
    public void votosEmitidosInPuntosPrivadosAsSuplenteShouldContainsSuplenteNombreAsEmisorVoto() throws Exception {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDiaSecreto, reunionAVotar, TipoVotoEnum.FAVOR.name());
        votacionService.votarPuntoTitular(suplente, puntoAVotar);
        forceSqlViewUpdate();

        List<PuntoOrdenDiaRecuentoVotosDTO> recuentoVotosByReunionId = votacionService.getRecuentoVotosByReunion(reunion);
        PuntoOrdenDiaRecuentoVotosDTO votoEmitido = recuentoVotosByReunionId.get(1);
        VotanteVoto nombreVotante = votoEmitido.getVotantesNombre().get(0);
        Assert.assertEquals(nombreVotante.getNombreTitular(), suplente.getNombre());
    }
}
