package es.uji.apps.goc.services;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.enums.TipoVisualizacionResultadosEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.firmas.FirmaService;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.RespuestaFirma;
import es.uji.apps.goc.notifications.AvisosReunion;
import es.uji.commons.sso.User;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyObject;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ReunionServiceTest extends BaseServiceTest {
    private static final Long CREADOR_ID = 2222l;
    private static final Long PERSONA_1_ID = 1L;
    private static final Long PERSONA_2_ID = 2L;
    private static final Long PERSONA_3_ID = 3L;
    @PersistenceContext
    protected EntityManager entityManager;
    @InjectMocks
    @Autowired
    ReunionService reunionService;
    @Mock
    AvisosReunion avisosReunionService;
    @Mock
    FirmaService firmaService;
    @Mock
    PersonaService personaService;
    @Autowired
    PuntoOrdenDiaDAO puntosOrdenDiaDAO;
    private OrganoLocal organo1;
    private OrganoLocal organo2;
    private Reunion reunion1;
    private Persona convocante = new Persona(CREADOR_ID, "Nicolas", "nicolas@example.com");
    private OrganoReunionMiembro organoReunionCreador;

    @Before
    public void initializate() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.doNothing().when(avisosReunionService).enviaAvisoNuevaReunion(anyObject(), anyObject(), anyObject(), anyObject(), anyObject());
        Mockito.when(firmaService.firmaExterna(anyObject())).thenReturn(new RespuestaFirma());
    }

    @Test
    @Ignore
    public void testObtenerMiembrosOtroOrganoAlConvocado() throws MiembrosExternosException {
        crearDosOrganos();
        crearUnAutorizadoEnLosDosOrganos();
        crearUnMiembroComunALosDosOrganos();
        crearDosMiembrosDiferentesEnCadaOrgano();
        crearReunionConUnOrgano();
        Reunion reunionById = reunionService.getReunionById(reunion1.getId());
        Assert.assertThat(reunionById.getAvisoPrimeraReunion(), is(true));
        List<Miembro> miembros =
                reunionService.obtenerMiembrosOrganoParaInformar(reunion1, String.valueOf(organo2.getId()), PERSONA_1_ID, false);
        Assert.assertThat(miembros.size(), is(1));
        Assert.assertThat(miembros.get(0).getEmail(), is("email3@goc.com"));
    }

    @Test
    @Ignore
    public void testValidarPermisosUsuario() {
        crearDosOrganos();
        crearUnAutorizadoEnLosDosOrganos();
        crearUnMiembroComunALosDosOrganos();
        crearDosMiembrosDiferentesEnCadaOrgano();
        crearReunionConUnOrgano();
        Reunion reunionById = reunionService.getReunionById(reunion1.getId());
        Assert.assertThat(reunionById.getAvisoPrimeraReunion(), is(true));
        String errors = reunionService.validarPermisosUsuarioParaInformarAOrgano(reunion1, PERSONA_1_ID,
                String.valueOf(organo2.getId()), false);
        Assert.assertThat(errors, is(nullValue()));
    }

    @Test
    @Ignore
    public void testValidarPermisosUsuarioFail() {
        crearDosOrganos();
        crearUnAutorizadoEnSoloUnOrgano();
        crearUnMiembroComunALosDosOrganos();
        crearDosMiembrosDiferentesEnCadaOrgano();
        crearReunionConUnOrgano();
        Reunion reunionById = reunionService.getReunionById(reunion1.getId());
        Assert.assertThat(reunionById.getAvisoPrimeraReunion(), is(true));
        String errors = reunionService.validarPermisosUsuarioParaInformarAOrgano(reunion1, PERSONA_1_ID,
                String.valueOf(organo2.getId()), false);
        Assert.assertThat(errors, is(notNullValue()));
    }

    private void crearUnAutorizadoEnSoloUnOrgano() {
        OrganoAutorizadoBuilder organoAutorizadoBuilder = new OrganoAutorizadoBuilder();
        organoAutorizadoBuilder.withOrganoId(organo1.getId().toString()).withPersonaId(PERSONA_1_ID)
                .withPersonaNombre("persona 1").withOrganoExterno(false).build(entityManager);
    }

    private void crearReunionConUnOrgano() {
        ReunionBuilder reunionBuilder = new ReunionBuilder();
        reunion1 = reunionBuilder.withAsunto("Reunión 1").withCreadorId(CREADOR_ID).withFecha(new Date())
                .withAvisoPrimeraReunion(true).build(entityManager);


        OrganoReunionBuilder organoReunionBuilder = new OrganoReunionBuilder();
        OrganoReunion organo1Reunion1 =
                organoReunionBuilder.withOrganoId(organo1.getId().toString()).withReunion(reunion1)
                        .withOrganoNombre(organo1.getNombre()).build(entityManager);

        reunion1.setReunionOrganos(new HashSet<>(Arrays.asList(organo1Reunion1)));
        entityManager.merge(reunion1);
    }

    private void crearDosMiembrosDiferentesEnCadaOrgano() {
        MiembroLocalBuilder miembroLocalBuilder = new MiembroLocalBuilder();
        miembroLocalBuilder.withOrganoLocal(organo1).withPersonaId(PERSONA_2_ID).withEmail("email2@goc.com")
                .withNombre("persona 2").build(entityManager);

        miembroLocalBuilder = new MiembroLocalBuilder();
        miembroLocalBuilder.withOrganoLocal(organo2).withPersonaId(PERSONA_3_ID).withEmail("email3@goc.com")
                .withNombre("persona 3").build(entityManager);
    }

    private void crearUnMiembroComunALosDosOrganos() {
        MiembroLocalBuilder miembroLocalBuilder = new MiembroLocalBuilder();
        miembroLocalBuilder.withOrganoLocal(organo1).withPersonaId(PERSONA_1_ID).withEmail("email1@goc.com")
                .withNombre("persona 1").build(entityManager);

        miembroLocalBuilder = new MiembroLocalBuilder();
        miembroLocalBuilder.withOrganoLocal(organo2).withPersonaId(PERSONA_1_ID).withEmail("email1@goc.com")
                .withNombre("persona 1").build(entityManager);
    }

    private void crearUnAutorizadoEnLosDosOrganos() {
        OrganoAutorizadoBuilder organoAutorizadoBuilder = new OrganoAutorizadoBuilder();
        organoAutorizadoBuilder.withOrganoId(organo1.getId().toString()).withPersonaId(PERSONA_1_ID)
                .withPersonaNombre("persona 1").withOrganoExterno(false).build(entityManager);
        organoAutorizadoBuilder = new OrganoAutorizadoBuilder();
        organoAutorizadoBuilder.withOrganoId(organo2.getId().toString()).withPersonaId(PERSONA_1_ID)
                .withPersonaNombre("persona 1").withOrganoExterno(false).build(entityManager);
    }

    private void crearDosOrganos() {
        TipoOrganoBuilder tipoOrganoBuilder = new TipoOrganoBuilder();
        TipoOrganoLocal tipoOrgano =
                tipoOrganoBuilder.withCodigo("mult").withNombre("miltipersonal").build(entityManager);
        OrganoLocalBuilder organoBuilder = new OrganoLocalBuilder();
        organo1 = organoBuilder.withNombre("Organo 1").withCreadorId(CREADOR_ID).withTipoOrgano(tipoOrgano)
                .build(entityManager);
        organoBuilder = new OrganoLocalBuilder();
        organo2 = organoBuilder.withNombre("Organo 2").withCreadorId(CREADOR_ID).withTipoOrgano(tipoOrgano)
                .build(entityManager);
    }

    private void crearReunionConVotacionTelematicaYOrgano() {
        PuntoOrdenDia puntoOrdenDiaAbreviado =
                new PuntoOrdenDiaBuilder()
                        .withProcedimientoAbreviado()
                        .getPuntoOrdenDia();

        Set<PuntoOrdenDia> puntosOrdenDia = new HashSet<>();
        puntosOrdenDia.add(puntoOrdenDiaAbreviado);

        organoReunionCreador =
                new OrganoReunionMiembroBuilder()
                        .withEmail("pepe@4tic.com")
                        .withMiembroId(CREADOR_ID)
                        .build(entityManager);

        Set<OrganoReunionMiembro> miembros = new HashSet<>();
        miembros.add(organoReunionCreador);

        TipoOrganoLocal tipo1 = new TipoOrganoBuilder().withNombre("Tipo 1").build(entityManager);

        ReunionBuilder reunionBuilder = new ReunionBuilder();
        reunion1 = reunionBuilder
                .withAsunto("Reunión 1")
                .withCreadorId(CREADOR_ID)
                .withFecha(new Date())
                .withAvisoPrimeraReunion(false)
                .withTelematica(true)
                .withVotacionTelematica(true)
                .withPuntosOrdenDia(puntosOrdenDia)
                .withOrgano("organo tipo 1", tipo1, entityManager)
                .withMiembros(miembros)
                .build(entityManager);

        entityManager.merge(reunion1);
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    @Ignore
    public void shouldReunionNotCompletadaAfterDuplicate() throws NoAdmiteVotacionYAceptaCambioDeVotoException {
        Reunion reunionCompletada = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID)
                .withAvisoPrimeraReunion(true).completada().withAcuerdos("Acuerdos").build(entityManager);
        Persona convocante = new Persona(CREADOR_ID, "Nicolas", "nicolas@example.com");
        Reunion reunionDuplicada = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);

        assertThat(reunionDuplicada.getAsunto(), Is.is("Reunión 1"));
        assertThat(isCompletada(reunionDuplicada), is(false));
        assertThat(reunionDuplicada.getFechaCompletada(), is(nullValue()));
    }

    @Test
    public void shouldTelematicaConVotacion() throws NoAdmiteVotacionYAceptaCambioDeVotoException {
        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .completada()
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAcuerdos("Acuerdos")
                        .build(entityManager);

        reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);
    }

    private boolean isCompletada(Reunion reunionDuplicada) {
        return (reunionDuplicada.isCompletada() || reunionDuplicada.getAcuerdos() != null || reunionDuplicada.getFechaCompletada() != null ||
                reunionDuplicada.getMiembroResponsableActa() != null || reunionDuplicada.getUrlActa() != null || reunionDuplicada.getUrlActa() != null ||
                reunionDuplicada.getConvocatoriaComienzo() != null || reunionDuplicada.getAvisoPrimeraReunion() || reunionDuplicada.isNotificada() ||
                reunionDuplicada.getAvisoPrimeraReunionFecha() != null || reunionDuplicada.getAvisoPrimeraReunionUser() != null || reunionDuplicada.getAvisoPrimeraReunionUserEmail() != null);
    }

    @Test
    public void shouldGetVotacionTelematicaFalseWhenDefault() {
        Reunion reunion = new ReunionBuilder().getReunion();
        assertThat(reunion.getVotacionTelematica(), is(false));
    }

    @Test
    public void shouldGetVotacionTelematicaFalseWhenValueIsNull() {
        Reunion reunion = new ReunionBuilder().withVotacionTelematica(null).getReunion();
        assertThat(reunion.getVotacionTelematica(), is(false));
    }

    @Test
    @Ignore
    public void shouldSetVotacionTelematicaFalseWhenValueIsNUll() {
        Reunion reunion = new ReunionBuilder().withVotacionTelematica(null).build(entityManager);
        assertThat(reunionService.getReunionById(reunion.getId()).getVotacionTelematica(), is(false));
    }

    @Test
    public void shouldGetAdmiteCambioVotacionFalseWhenDefault() {
        Reunion reunion = new ReunionBuilder().getReunion();
        assertThat(reunion.getAdmiteCambioVoto(), is(false));
    }

    @Test
    public void shouldSetAdmiteCambioVotoFalseWhenValueIsNUll() {
        Reunion reunion = new ReunionBuilder().withAdmiteCambioVoto(null).getReunion();
        assertThat(reunion.getAdmiteCambioVoto(), is(false));
    }

    @Test(expected = NoAdmiteVotacionYAceptaCambioDeVotoException.class)
    public void shouldNoEsTelematicaConAceptaCambioVotoExcepcionTelematicaNull() throws
            NoAdmiteVotacionYAceptaCambioDeVotoException, NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException {
        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .completada()
                        .withAcuerdos("Acuerdos")
                        .withVotacionTelematica(false)
                        .withAdmiteCambioVoto(true)
                        .build(entityManager);

        reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);
    }

    @Test
    public void shouldTelematicaConAceptaCambioVoto() throws NoAdmiteVotacionYAceptaCambioDeVotoException, NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException {
        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .completada()
                        .withTelematica(true)
                        .withAdmiteCambioVoto(true)
                        .withVotacionTelematica(true)
                        .withAcuerdos("Acuerdos")
                        .build(entityManager);


        Reunion reunion = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);
        assertThat(reunion.getAdmiteCambioVoto(), is(true));
    }

    @Ignore
    @Test(expected = NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException.class)
    public void shouldThrowErrorWhenUpdateReunionTelematicaFalseAndHasPuntoOrdenDiaWithVotacionAbierta()
            throws NoAdmiteVotacionYAceptaCambioDeVotoException, ReunionNoDisponibleException,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException, NoEsTelematicaExceptionConVotacion,
            NoVotacionTelematicaException {

        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withTipoVoto(true)
                        .withVotacionAbierta(true)
                        .getPuntoOrdenDia();

        Set<PuntoOrdenDia> puntosOrdenDia = new HashSet<>();
        puntosOrdenDia.add(puntoOrdenDia);

        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .completada()
                        .withTelematica(true)
                        .withAcuerdos("Acuerdos")
                        .withPuntosOrdenDia(puntosOrdenDia)
                        .build(entityManager);


        Reunion reunion = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);

        reunion.setTelematica(false);

        reunionService.updateReunion(
                reunion.getId(), reunion.getAsunto(), reunion.getAsuntoAlternativo(), reunion.getDescripcion(),
                reunion.getDescripcionAlternativa(), reunion.getDuracion(), reunion.getFecha(), reunion.getFechaSegundaConvocatoria(),
                reunion.getUbicacion(), reunion.getUbicacionAlternativa(), reunion.getUrlGrabacion(), reunion.getNumeroSesion(),
                reunion.isPublica(), reunion.isTelematica(), reunion.getTelematicaDescripcion(), reunion.getTelematicaDescripcionAlternativa(),
                reunion.isAdmiteSuplencia(), reunion.isAdmiteDelegacionVoto(), reunion.isAdmiteComentarios(), reunion.isEmailsEnNuevosComentarios(),
                5L, reunion.getHoraFin(), reunion.getConvocatoriaComienzo(), reunion.getVotacionTelematica(), reunion.isAdmiteCambioVoto(),
                reunion.getTipoVisualizacionResultados(), reunion.getFechaFinVotacion(), reunion.getResponsableVotoId(), reunion.getResponsableVotoNom(), reunion.getVerDeliberaciones(), reunion.getVerAcuerdos()
        );
    }

    @Ignore
    @Test(expected = NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException.class)
    public void shouldThrowErrorWhenUpdateReunionVotacionTelematicaFalseAndHasPuntoOrdenDiaWithVotacionAbierta()
            throws NoAdmiteVotacionYAceptaCambioDeVotoException, ReunionNoDisponibleException,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException, NoEsTelematicaExceptionConVotacion,
            NoVotacionTelematicaException {

        PuntoOrdenDia puntoOrdenDia =
                new PuntoOrdenDiaBuilder()
                        .withTipoVoto(true)
                        .withVotacionAbierta(true)
                        .getPuntoOrdenDia();

        Set<PuntoOrdenDia> puntosOrdenDia = new HashSet<>();
        puntosOrdenDia.add(puntoOrdenDia);

        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .completada()
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAcuerdos("Acuerdos")
                        .withPuntosOrdenDia(puntosOrdenDia)
                        .build(entityManager);


        Reunion reunion = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);

        reunion.setVotacionTelematica(false);

        reunionService.updateReunion(
                reunion.getId(), reunion.getAsunto(), reunion.getAsuntoAlternativo(), reunion.getDescripcion(),
                reunion.getDescripcionAlternativa(), reunion.getDuracion(), reunion.getFecha(), reunion.getFechaSegundaConvocatoria(),
                reunion.getUbicacion(), reunion.getUbicacionAlternativa(), reunion.getUrlGrabacion(), reunion.getNumeroSesion(),
                reunion.isPublica(), reunion.isTelematica(), reunion.getTelematicaDescripcion(), reunion.getTelematicaDescripcionAlternativa(),
                reunion.isAdmiteSuplencia(), reunion.isAdmiteDelegacionVoto(), reunion.isAdmiteComentarios(), reunion.isEmailsEnNuevosComentarios(),
                5L, reunion.getHoraFin(), reunion.getConvocatoriaComienzo(), reunion.getVotacionTelematica(), reunion.isAdmiteCambioVoto()
                , reunion.getTipoVisualizacionResultados(), reunion.getFechaFinVotacion(), reunion.getResponsableVotoId(), reunion.getResponsableVotoNom(), reunion.getVerDeliberaciones(), reunion.getVerAcuerdos());
    }

    @Test
    public void enviarConvocatoriaConPuntosOrdenDiaProcedimientoVotacionAbreviado()
            throws Exception {
        crearReunionConVotacionTelematicaYOrgano();
        User user = new User();
        user.setId(CREADOR_ID);

        Mockito.when(personaService.getPersonaFromDirectoryByPersonaId(anyObject())).thenReturn(new Persona(CREADOR_ID, "test", "test@test.com"));

        reunionService.enviarConvocatoria(reunion1.getId(), user, "convocatoria", "convocatoriaAlt");
        entityManager.flush();
        entityManager.clear();

        Reunion reunion = reunionService.getReunionById(reunion1.getId());

        Set<PuntoOrdenDia> reunionPuntosOrdenDia = reunion.getReunionPuntosOrdenDia();
        for (PuntoOrdenDia puntoOrdenDia : reunionPuntosOrdenDia) {
            if (puntoOrdenDia.getTipoProcedimientoVotacion().equals(TipoProcedimientoVotacionEnum.ABREVIADO.name())) {
                Assert.assertEquals(puntoOrdenDia.getFechaInicioVotacion().getTime(), reunion.getAvisoPrimeraReunionFecha().getTime());
            }
        }
    }

    @Test
    public void reunionShouldHasTipoVisualizacionResultadosAlAlcanzarFechaVotacion()
            throws NoAdmiteVotacionYAceptaCambioDeVotoException {
        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAcuerdos("Acuerdos")
                        .withTipoVisualizacionResultados(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION)
                        .build(entityManager);

        Reunion reunion = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);
        Assert.assertTrue(reunionService.getReunionById(reunion.getId()).getTipoVisualizacionResultados().getTipo());
    }

    @Test
    public void reunionShouldNotHasTipoVisualizacionResultadosAlCerrarReunion()
            throws NoAdmiteVotacionYAceptaCambioDeVotoException {
        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAcuerdos("Acuerdos")
                        .withTipoVisualizacionResultados(TipoVisualizacionResultadosEnum.AL_ALCANZAR_FECHA_FIN_VOTACION)
                        .build(entityManager);

        Reunion reunion = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);
        Assert.assertFalse(reunionService.getReunionById(reunion.getId()).getTipoVisualizacionResultados().getTipo());
    }

    @Test
    public void reunionShouldHasVerResultadoMientrasActivaWhenUpdate()
            throws NoAdmiteVotacionYAceptaCambioDeVotoException, ReunionNoDisponibleException,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException, NoEsTelematicaExceptionConVotacion,
            NoVotacionTelematicaException {
        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAcuerdos("Acuerdos")
                        .withTipoVisualizacionResultados(TipoVisualizacionResultadosEnum.AL_ALCANZAR_FECHA_FIN_VOTACION)
                        .build(entityManager);

        Reunion reunion = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);

        reunion.setTipoVisualizacionResultadosEnum(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION);

        reunionService.updateReunion(
                reunion.getId(), reunion.getAsunto(), reunion.getAsuntoAlternativo(), reunion.getDescripcion(),
                reunion.getDescripcionAlternativa(), reunion.getDuracion(), reunion.getFecha(), reunion.getFechaSegundaConvocatoria(),
                reunion.getUbicacion(), reunion.getUbicacionAlternativa(), reunion.getUrlGrabacion(), reunion.getNumeroSesion(),
                reunion.isPublica(), reunion.isTelematica(), reunion.getTelematicaDescripcion(), reunion.getTelematicaDescripcionAlternativa(),
                reunion.isAdmiteSuplencia(), reunion.isAdmiteDelegacionVoto(), reunion.isAdmiteComentarios(), reunion.isEmailsEnNuevosComentarios(),
                5L, reunion.getHoraFin(), reunion.getConvocatoriaComienzo(), reunion.getVotacionTelematica(), reunion.isAdmiteCambioVoto()
                , reunion.getTipoVisualizacionResultados(), reunion.getFechaFinVotacion(), reunion.getResponsableVotoId(), reunion.getResponsableVotoNom(), reunion.getVerDeliberaciones(), reunion.getVerAcuerdos());

        Assert.assertTrue(reunionService.getReunionById(reunion.getId()).getTipoVisualizacionResultados().getTipo());
    }

    @Test
    public void reunionShouldNotHasVerResultadoMientrasActivaWhenUpdate()
            throws NoAdmiteVotacionYAceptaCambioDeVotoException, ReunionNoDisponibleException,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException, NoEsTelematicaExceptionConVotacion,
            NoVotacionTelematicaException {
        Reunion reunionCompletada =
                new ReunionBuilder()
                        .withAsunto("Reunión 1")
                        .withCreadorId(CREADOR_ID)
                        .withAvisoPrimeraReunion(true)
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAcuerdos("Acuerdos")
                        .withTipoVisualizacionResultados(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION)
                        .build(entityManager);

        Reunion reunion = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);

        reunion.setTipoVisualizacionResultadosEnum(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION);

        reunionService.updateReunion(
                reunion.getId(), reunion.getAsunto(), reunion.getAsuntoAlternativo(), reunion.getDescripcion(),
                reunion.getDescripcionAlternativa(), reunion.getDuracion(), reunion.getFecha(), reunion.getFechaSegundaConvocatoria(),
                reunion.getUbicacion(), reunion.getUbicacionAlternativa(), reunion.getUrlGrabacion(), reunion.getNumeroSesion(),
                reunion.isPublica(), reunion.isTelematica(), reunion.getTelematicaDescripcion(), reunion.getTelematicaDescripcionAlternativa(),
                reunion.isAdmiteSuplencia(), reunion.isAdmiteDelegacionVoto(), reunion.isAdmiteComentarios(), reunion.isEmailsEnNuevosComentarios(),
                5L, reunion.getHoraFin(), reunion.getConvocatoriaComienzo(), reunion.getVotacionTelematica(), reunion.isAdmiteCambioVoto()
                , reunion.getTipoVisualizacionResultados(), reunion.getFechaFinVotacion(), reunion.getResponsableVotoId(), reunion.getResponsableVotoNom(), reunion.getVerDeliberaciones(), reunion.getVerAcuerdos());

        Assert.assertEquals(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION, reunionService.getReunionById(reunion.getId()).getTipoVisualizacionResultados());
    }

    @Test
    public void shouldCloseVotacionPuntoOrdinarioWhenCloseReunion()
            throws Exception {
        crearReunionConVotacionTelematicaYOrgano();
        PuntoOrdenDia puntoOrdinario = new PuntoOrdenDiaBuilder(reunion1).withProcedimientoOrdinario().withVotacionAbierta(true).withTipoVotacion()
                .build(entityManager);
        entityManager.flush();
        entityManager.clear();

        Mockito.when(personaService.getPersonaFromDirectoryByPersonaId(anyObject())).thenReturn(new Persona(CREADOR_ID, "test", "test@test.com"));
        User user = new User();
        user.setId(CREADOR_ID);

        reunionService.enviarConvocatoria(reunion1.getId(), user, "convocatoria", "convocatoriaAlt");
        reunionService.firmarReunion(reunion1.getId(), "", "", organoReunionCreador.getId(), user.getId());

        entityManager.flush();
        entityManager.clear();

        Reunion reunion = reunionService.getReunionById(reunion1.getId());

        Assert.assertTrue(reunion.isCompletada());
        Assert.assertFalse(reunion.getReunionPuntosOrdenDia().stream().filter(p -> p.getId().equals(puntoOrdinario.getId())).map(PuntoOrdenDia::getVotacionAbierta).findAny().orElse(false));
    }

    @Test
    public void shouldCloseVotacionPuntoAbreviadoWhenCloseReunion()
            throws Exception {
        crearReunionConVotacionTelematicaYOrgano();
        PuntoOrdenDia puntoAbreviado = new PuntoOrdenDiaBuilder(reunion1).withProcedimientoAbreviado().withVotacionAbierta(true).withTipoVotacion()
                .build(entityManager);
        entityManager.flush();
        entityManager.clear();

        Mockito.when(personaService.getPersonaFromDirectoryByPersonaId(anyObject())).thenReturn(new Persona(CREADOR_ID, "test", "test@test.com"));
        User user = new User();
        user.setId(CREADOR_ID);

        reunionService.enviarConvocatoria(reunion1.getId(), user, "convocatoria", "convocatoriaAlt");
        reunionService.firmarReunion(reunion1.getId(), "", "", organoReunionCreador.getId(), user.getId());

        entityManager.flush();
        entityManager.clear();

        Reunion reunion = reunionService.getReunionById(reunion1.getId());

        Assert.assertTrue(reunion.isCompletada());
        Assert.assertFalse(reunion.getReunionPuntosOrdenDia().stream().filter(p -> p.getId().equals(puntoAbreviado.getId())).map(PuntoOrdenDia::getVotacionAbierta).findAny().orElse(false));
    }
}
