package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.TipoOrganoLocal;

public class TipoOrganoBuilder
{
    TipoOrganoLocal tipoOrganoLocal;

    public TipoOrganoBuilder()
    {
        this.tipoOrganoLocal = new TipoOrganoLocal();
    }

    public TipoOrganoBuilder withId(Long id)
    {
        this.tipoOrganoLocal.setId(id);
        return this;
    }

    public TipoOrganoBuilder withNombre(String nombre)
    {
        this.tipoOrganoLocal.setNombre(nombre);
        return this;

    }

    public TipoOrganoBuilder withCodigo(String codigo)
    {
        this.tipoOrganoLocal.setCodigo(codigo);
        return this;
    }

    public TipoOrganoLocal build(EntityManager entityManager)
    {
        entityManager.persist(tipoOrganoLocal);
        return tipoOrganoLocal;
    }
}
