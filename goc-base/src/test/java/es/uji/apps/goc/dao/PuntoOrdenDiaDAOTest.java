package es.uji.apps.goc.dao;

import es.uji.apps.goc.builders.PuntoOrdenDiaBuilder;
import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.exceptions.ProcedimientoVotacionInvalidoParaPuntoNoVotableException;
import es.uji.apps.goc.exceptions.RecuentoVotoInvalidoParaPuntoNoVotableException;
import es.uji.apps.goc.exceptions.TipoRecuentoVotoNoExisteException;
import es.uji.apps.goc.services.ReunionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class PuntoOrdenDiaDAOTest {
    private static final Long CREADOR_ID = 2222l;

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    Reunion reunion;

    PuntoOrdenDia puntoOrdenDiaPadre;

    PuntoOrdenDia puntoOrdenDiaHijo;

    Set<PuntoOrdenDia> PuntosOrdenDiaHijos;
    @Autowired
    ReunionService reunionService;

    @Before
    public void setUp() {
        reunion = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).withFecha(new Date())
                .withAvisoPrimeraReunion(true).build(entityManager);

        PuntosOrdenDiaHijos = new HashSet<>();

        puntoOrdenDiaHijo = new PuntoOrdenDiaBuilder().withAcuerdos().withAcuerdosAlt().withDeliberaciones().withdeliberacionesAltenrativas()
                .withUrlcta().withUrlActaAlternativa().withUrlActaAnterior().withUrlActaAnteriorAlternativa().build(entityManager);
        PuntosOrdenDiaHijos.add(puntoOrdenDiaHijo);

        puntoOrdenDiaPadre = new PuntoOrdenDiaBuilder().withAcuerdos().withAcuerdosAlt().withDeliberaciones().withdeliberacionesAltenrativas()
                .withUrlcta().withUrlActaAlternativa().withUrlActaAnterior().withUrlActaAnteriorAlternativa().withPuntoinferiores(PuntosOrdenDiaHijos).build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void shouldDuplicatePuntosWhenOnlyOneLevel() throws TipoRecuentoVotoNoExisteException,
            RecuentoVotoInvalidoParaPuntoNoVotableException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException
    {
        List<PuntoOrdenDia> puntos = new ArrayList<>();
        puntos.add(new PuntoOrdenDia());
        reunionService.duplicaPuntosOrdenDia(reunion, puntos);

        assertThat(puntoOrdenDiaDAO.getPuntosByReunionId(reunion.getId()).size(), is(1));
    }

    @Test
    public void shouldDuplicatePuntosWhenPuntosHaveChildren()
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException
    {
        List<PuntoOrdenDia> puntos = new ArrayList<>();
        PuntoOrdenDia puntoPadre = new PuntoOrdenDia();
        PuntoOrdenDia puntoHijo = new PuntoOrdenDia();
        puntoHijo.setPuntoSuperior(puntoPadre);
        Set<PuntoOrdenDia> puntosHijos = new HashSet<>();
        puntosHijos.add(puntoHijo);
        puntoPadre.setPuntosInferiores(puntosHijos);

        puntos.add(puntoPadre);
        reunionService.duplicaPuntosOrdenDia(reunion, puntos);

        List<PuntoOrdenDia> puntosByReunionId = puntoOrdenDiaDAO.getPuntosByReunionId(reunion.getId());
        assertThat(puntosByReunionId.size(), is(1));
        assertThat(puntosByReunionId.get(0).getPuntosInferiores().size(), is(1));
    }

    @Test
    public void shouldDuplicatePuntosAndsetPuntoSuperiorId()
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException
    {
        List<PuntoOrdenDia> puntos = new ArrayList<>();
        PuntoOrdenDia puntoPadre = new PuntoOrdenDia();
        puntoPadre.setId(456453L);
        PuntoOrdenDia puntoHijo = new PuntoOrdenDia();
        puntoHijo.setPuntoSuperior(puntoPadre);
        Set<PuntoOrdenDia> puntosHijos = new HashSet<>();
        puntosHijos.add(puntoHijo);
        puntoPadre.setPuntosInferiores(puntosHijos);

        puntos.add(puntoPadre);
        reunionService.duplicaPuntosOrdenDia(reunion, puntos);

        List<PuntoOrdenDia> puntosByReunionId = puntoOrdenDiaDAO.getPuntosByReunionId(reunion.getId());
        assertThat(puntosByReunionId.size(), is(1));
        assertThat(puntosByReunionId.get(0).getPuntosInferiores().size(), is(1));
        assertThat(puntosByReunionId.get(0).getId().intValue() , not(456453));
        for(PuntoOrdenDia punto: puntosByReunionId.get(0).getPuntosInferiores()){
            assertThat(puntosByReunionId.get(0).getId(), is(punto.getPuntoSuperior().getId()));
        }
    }


    @Test
    public void ShouldDuplicatePuntosAsNoCompletados()
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException
    {

        List <PuntoOrdenDia>  puntosOrdenDia= new LinkedList<>();
        puntosOrdenDia.add(puntoOrdenDiaPadre);

        reunionService.duplicaPuntosOrdenDia(reunion, puntosOrdenDia);
        List<PuntoOrdenDia> puntosByReunionId = puntoOrdenDiaDAO.getPuntosByReunionId(reunion.getId());

        assertThat(isCompletado(puntosByReunionId.get(0)),is(false));

    }

    private boolean isCompletado(PuntoOrdenDia puntoOrdenDia) {
        return ( puntoOrdenDia.getAcuerdos() != null || puntoOrdenDia.getAcuerdosAlternativos() != null || puntoOrdenDia.getDeliberaciones() != null ||
                puntoOrdenDia.getDeliberacionesAlternativas() != null || puntoOrdenDia.getUrlActa() != null
                || puntoOrdenDia.getUrlActaAlternativa() != null || puntoOrdenDia.getUrlActaAnterior() != null || puntoOrdenDia.getUrlActaAnteriorAlt() != null);
    }

}
