package es.uji.apps.goc.builders;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionInvitado;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.enums.TipoVisualizacionResultadosEnum;
import es.uji.apps.goc.model.Organo;

public class ReunionBuilder {
    Reunion reunion;
    Set<OrganoReunion> organos;

    public ReunionBuilder() {
        this.reunion = new Reunion();
        this.reunion.setFechaCreacion(new Date());
        this.reunion.setCompletada(false);
        this.reunion.setAdmiteDelegacionVoto(false);
        this.organos = new HashSet<>();
    }


    public ReunionBuilder withDelegacionVoto() {
        this.reunion.setAdmiteDelegacionVoto(true);
        return this;
    }

    public ReunionBuilder withId(Long id) {
        this.reunion.setId(id);
        return this;
    }

    public ReunionBuilder withAsunto(String asunto) {
        this.reunion.setAsunto(asunto);
        return this;
    }

    public ReunionBuilder withFecha(Date fecha) {
        this.reunion.setFecha(fecha);
        return this;
    }

    public ReunionBuilder withOrgano(String nombre, TipoOrganoLocal tipoOrgano, EntityManager entityManager) {
        OrganoLocal organoLocal = new OrganoLocal();
        organoLocal.setNombre(nombre);
        entityManager.persist(organoLocal);

        OrganoReunion organoReunion = new OrganoReunion();
        organoReunion.setOrganoNombre(nombre);
        organoReunion.setTipoOrganoId(tipoOrgano.getId());
        organoReunion.setOrganoId(organoLocal.getId().toString());
        organoReunion.setExterno(false);
        this.organos.add(organoReunion);

        return this;
    }

    public ReunionBuilder withOrgano(Organo organo) {

        OrganoReunion organoReunion = new OrganoReunion();
        organoReunion.setOrganoNombre(organo.getNombre());
        organoReunion.setTipoOrganoId(organo.getTipoOrgano().getId());
        organoReunion.setOrganoId(organo.getId());
        organoReunion.setExterno(organo.isExterno());
        this.organos.add(organoReunion);

        return this;
    }

    public ReunionBuilder withOrgano(OrganoLocal organoLocal) {
        OrganoReunion organoReunion = new OrganoReunion();
        organoReunion.setOrganoNombre(organoLocal.getNombre());
        organoReunion.setOrganoId(organoLocal.getId().toString());
        organoReunion.setExterno(false);
        this.organos.add(organoReunion);

        return this;
    }

    public ReunionBuilder withMiembros(Set<OrganoReunionMiembro> miembros) {

        miembros.stream().forEach(miembro -> miembro.setOrganoId(this.organos.iterator().next().getOrganoId()));
        this.organos.iterator().next().setMiembros(miembros);
        return this;
    }

    public ReunionBuilder withInvitados(Set<OrganoReunionInvitado> invitados) {
        this.organos.iterator().next().setInvitados(invitados);
        return this;
    }


    public ReunionBuilder withCreadorId(Long creadorId) {
        this.reunion.setCreadorId(creadorId);
        return this;
    }

    public ReunionBuilder withAvisoPrimeraReunion(boolean avisoPrimeraReunion) {
        this.reunion.setAvisoPrimeraReunion(avisoPrimeraReunion);
        return this;
    }

    public ReunionBuilder withPuntosOrdenDia(Set<PuntoOrdenDia> puntosOrdenDia) {
        this.reunion.setReunionPuntosOrdenDia(puntosOrdenDia);
        return this;
    }

    public ReunionBuilder withTipoVisualizacionResultados(TipoVisualizacionResultadosEnum tipoVisualizacionResultados) {
        this.reunion.setTipoVisualizacionResultadosEnum(tipoVisualizacionResultados);
        return this;
    }

    public Reunion build(EntityManager entityManager) {
        Date date = new Date();
        this.reunion.setFechaCreacion(date);

        if (reunion.getAvisoPrimeraReunion() != null && reunion.getAvisoPrimeraReunion()) {
            reunion.setCompletada(false);
            reunion.setAvisoPrimeraReunionFecha(date);
        }

        entityManager.persist(reunion);

        Set<OrganoReunion> organosReunion = new HashSet<>();
        for (OrganoReunion organo : organos) {
            organo.setReunion(this.reunion);
            entityManager.persist(organo);

            if (organo.getMiembros() != null) {

                for (OrganoReunionMiembro miembro : organo.getMiembros()) {
                    miembro.setOrganoId(organo.getId().toString());
                    miembro.setOrganoReunion(organo);
                    miembro.setReunionId(this.reunion.getId());
                }
            }

            if (organo.getInvitados() != null) {
                for (OrganoReunionInvitado invitado : organo.getInvitados()) {
                    invitado.setOrganoId(organo.getId().toString());
                    invitado.setOrganoReunion(organo);
                    invitado.setReunionId(this.reunion.getId());
                }
            }
            organosReunion.add(organo);
        }

        for (PuntoOrdenDia puntoOrdenDia : this.reunion.getReunionPuntosOrdenDia()) {
            puntoOrdenDia.setReunion(this.reunion);
            Date avisoPrimeraReunionFecha = reunion.getAvisoPrimeraReunionFecha();
            if (avisoPrimeraReunionFecha != null) {

                if (isPuntoVotable(puntoOrdenDia))
                    puntoOrdenDia.setFechaInicioVotacion(avisoPrimeraReunionFecha);
            }

            entityManager.persist(puntoOrdenDia);
        }

        this.reunion.setReunionOrganos(organosReunion);
        return reunion;
    }

    public ReunionBuilder completada() {
        this.reunion.setCompletada(true);
        this.reunion.setFechaCompletada(new Date());
        return this;
    }

    public ReunionBuilder withAcuerdos(String acuerdos) {
        this.reunion.setAcuerdos(acuerdos);
        return this;
    }

    public ReunionBuilder withURLGrabacion(String url) {
        this.reunion.setUrlGrabacion(url);
        return this;
    }

    public ReunionBuilder withTelematica(Boolean telematica) {
        this.reunion.setTelematica(telematica);
        return this;
    }

    public ReunionBuilder withVotacionTelematica(Boolean votacionTelematica) {
        this.reunion.setVotacionTelematica(votacionTelematica);
        return this;
    }

    public ReunionBuilder withAdmiteCambioVoto(Boolean admiteCambioVoto) {
        this.reunion.setAdmiteCambioVoto(admiteCambioVoto);
        return this;
    }
    public ReunionBuilder withFechaFinVotacion(Date fechaFinVotacion) {
        this.reunion.setFechaFinVotacion(fechaFinVotacion);
        return this;
    }

    public Reunion getReunion() {
        return this.reunion;
    }

    private boolean isPuntoVotable(PuntoOrdenDia puntoOrdenDia) {
        return puntoOrdenDia.getTipoProcedimientoVotacion() != null && puntoOrdenDia.getTipoVoto() != null;
    }
}
