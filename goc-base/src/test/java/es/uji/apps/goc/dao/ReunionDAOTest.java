package es.uji.apps.goc.dao;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.notifications.AvisosReunion;
import es.uji.apps.goc.services.ReunionService;
import org.h2.tools.Server;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class ReunionDAOTest {
    private static final Long CREADOR_ID = 2222l;

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    ReunionService reunionService;
    @Autowired
    ReunionDAO reunionDAO;

    @Autowired
    AvisosReunion avisosReunion;

    @Before
    public void setUp() {
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void shouldGetOnlyReunionesCompletadas() {
        Reunion reunionCompletada1 = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);
        Reunion reunionCompletada2 = new ReunionBuilder().withAsunto("Reunión 2").withCreadorId(CREADOR_ID).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);
        Reunion reunionNoCompletada = new ReunionBuilder().withAsunto("Reunión No completada").withCreadorId(CREADOR_ID).withFecha(new Date())
                .withAvisoPrimeraReunion(true).build(entityManager);
        entityManager.flush();
        entityManager.clear();

        List<ReunionEditor> reunionesByEditorId = reunionDAO.getReunionesByEditorIdList(CREADOR_ID, true, null, null, null, null, 1, 1000);

        assertThat(reunionesByEditorId.size(), is(2));
        for (ReunionEditor reunion : reunionesByEditorId) {
            assertThat(reunion.getCompletada(), is(true));
        }
    }

    @Test
    public void shouldGetReunionesByTipoOrgano() throws SQLException {
        TipoOrganoLocal tipo1 = new TipoOrganoBuilder().withNombre("Tipo 1").build(entityManager);
        TipoOrganoLocal tipo2 = new TipoOrganoBuilder().withNombre("Tipo 2").build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Reunion reunionCompletada1 = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).withOrgano("organo tipo 1", tipo1, entityManager).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);
        Reunion reunionCompletada2 = new ReunionBuilder().withAsunto("Reunión 2").withCreadorId(CREADOR_ID).withOrgano("organo tipo 2", tipo2, entityManager).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<ReunionEditor> reunionesByEditorId = reunionDAO.getReunionesByEditorIdList(CREADOR_ID, true, tipo1.getId(), null, null, null, 1, 1000);

        assertThat(reunionesByEditorId.size(), is(1));
        assertThat(reunionesByEditorId.get(0).getTipoOrganoId(), is(tipo1.getId()));
    }

    @Test
    public void shouldGetReunionesByOrganoId() throws SQLException {
        TipoOrganoLocal tipo1 = new TipoOrganoBuilder().withNombre("Tipo 1").build(entityManager);
        TipoOrganoLocal tipo2 = new TipoOrganoBuilder().withNombre("Tipo 2").build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Reunion reunionCompletada1 = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).withOrgano("organo tipo 1", tipo1, entityManager).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);
        Reunion reunionCompletada2 = new ReunionBuilder().withAsunto("Reunión 2").withCreadorId(CREADOR_ID).withOrgano("organo tipo 2", tipo2, entityManager).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);

        entityManager.flush();
        entityManager.clear();

        String reunionOrganoId = reunionCompletada1.getReunionOrganos().iterator().next().getOrganoId();

        List<ReunionEditor> reunionesByEditorId = reunionDAO.getReunionesByEditorIdList(CREADOR_ID, false, null, reunionOrganoId, true, null, 1, 1000);

        assertThat(reunionesByEditorId.size(), is(1));
        assertThat(reunionesByEditorId.get(0).getOrganoId(), is(reunionOrganoId));
    }

    @Test
    public void shouldgetReunionesByAdminbyTipoOrgano() throws SQLException {
        TipoOrganoLocal tipo1 = new TipoOrganoBuilder().withNombre("Tipo 1").build(entityManager);
        TipoOrganoLocal tipo2 = new TipoOrganoBuilder().withNombre("Tipo 2").build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Reunion reunionCompletada1 = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).withOrgano("organo tipo 1", tipo1, entityManager).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);
        Reunion reunionCompletada2 = new ReunionBuilder().withAsunto("Reunión 2").withCreadorId(CREADOR_ID).withOrgano("organo tipo 2", tipo2, entityManager).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<ReunionEditor> getReunionesByAdmin = reunionDAO.getReunionesByEditorIdList(CREADOR_ID, true, tipo1.getId(), null, null, null, 1, 1000);

        assertThat(getReunionesByAdmin.size(), is(1));
        assertThat(getReunionesByAdmin.get(0).getTipoOrganoId(), is(tipo1.getId()));
    }

    @Test
    public void shouldgetReunionesByAdminByOrganoId() throws SQLException {
        TipoOrganoLocal tipo1 = new TipoOrganoBuilder().withNombre("Tipo 1").build(entityManager);
        TipoOrganoLocal tipo2 = new TipoOrganoBuilder().withNombre("Tipo 2").build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Reunion reunionCompletada1 = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).withOrgano("organo tipo 1", tipo1, entityManager).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);
        Reunion reunionCompletada2 = new ReunionBuilder().withAsunto("Reunión 2").withCreadorId(CREADOR_ID).withOrgano("organo tipo 2", tipo2, entityManager).withFecha(new Date())
                .withAvisoPrimeraReunion(true).completada().build(entityManager);

        entityManager.flush();
        entityManager.clear();

        String reunionOrganoId = reunionCompletada1.getReunionOrganos().iterator().next().getOrganoId();
        Server.createWebServer().start();

        List<ReunionEditor> getReunionesByAdmin = reunionDAO.getReunionesByEditorIdList(CREADOR_ID, false, null, reunionOrganoId, null, null, 1, 1000);

        assertThat(getReunionesByAdmin.size(), is(1));
        assertThat(getReunionesByAdmin.get(0).getOrganoId(), is(reunionOrganoId));
    }


    @Test
    public void shouldGetEmailsDeMiembrosAutorizadosEInvitadosByReunion()
            throws Exception {
        Reunion reunionCompletada1 = getReunionWithOrganoMiembroInvitadoYAutorizado();

        Reunion reunionById = reunionDAO.getReunionById(reunionCompletada1.getId());
        List<String> mailsMiembrosInvitadosYAutorizados = avisosReunion.getMiembros(reunionById, true);

        assertThat(mailsMiembrosInvitadosYAutorizados.size(), is(3));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("pepe@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("josefa@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("Autorizado@4tic.com"), is(true));

    }

    @Test
    public void shouldGetEmailsDeMiembrosAutorizadosEInvitadosByReunionDAOReunionById() throws Exception {
        Reunion reunionCompletada = getReunionWithOrganoMiembroInvitadoYAutorizado();

        Reunion reunionById = reunionDAO.getReunionById(reunionCompletada.getId());
        List<String> mailsMiembrosInvitadosYAutorizados = avisosReunion.getMiembros(reunionById, true);

        assertThat(mailsMiembrosInvitadosYAutorizados.size(), is(3));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("pepe@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("josefa@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("Autorizado@4tic.com"), is(true));

    }

    @Test
    public void shouldGetEmailsDeMiembrosAutorizadosEInvitadosByReunionDAOgetReunionConMiembrosAndPuntosDiaById() throws Exception {
        Reunion reunionCompletada = getReunionWithOrganoMiembroInvitadoYAutorizado();

        Reunion reunionConMiembros = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionCompletada.getId());
        List<String> mailsMiembrosInvitadosYAutorizados = avisosReunion.getMiembros(reunionConMiembros, true);

        assertThat(mailsMiembrosInvitadosYAutorizados.size(), is(3));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("pepe@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("josefa@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYAutorizados.contains("Autorizado@4tic.com"), is(true));

    }

    private Reunion getReunionWithOrganoMiembroInvitadoYAutorizado() {
        TipoOrganoLocal tipo1 = new TipoOrganoBuilder().withNombre("Tipo 1").build(entityManager);

        HashSet<OrganoReunionMiembro> organoReunionMiembros = new HashSet<>();
        HashSet<OrganoReunionInvitado> organoReunionInvitados = new HashSet<>();

        OrganoReunionMiembro miembro = new OrganoReunionMiembroBuilder().withEmail("pepe@4tic.com").build(entityManager);
        OrganoReunionInvitado invitado = new OrganoReunionInvitadoBuilder().withEmail("josefa@4tic.com").build(entityManager);

        organoReunionMiembros.add(miembro);
        organoReunionInvitados.add(invitado);

        Reunion reunionCompletada1 =
                new ReunionBuilder()
                        .withAsunto("Reunión 1").withCreadorId(CREADOR_ID)
                        .withOrgano("organo tipo 1", tipo1, entityManager)
                        .withMiembros(organoReunionMiembros).withInvitados(organoReunionInvitados).withFecha(new Date())
                        .withAvisoPrimeraReunion(true).completada().build(entityManager);

        OrganoAutorizado autorizado = new OrganoAutorizadoBuilder()
                .withOrganoId(reunionCompletada1.getReunionOrganos().iterator().next().getOrganoId())
                .withPersonaEmail("Autorizado@4tic.com")
                .build(entityManager);

        entityManager.flush();
        entityManager.clear();
        return reunionCompletada1;
    }
}
