package es.uji.apps.goc.services;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dao.OrganoReunionMiembroDAO;
import es.uji.apps.goc.dao.VotacionDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.*;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.ReunionAVotar;
import es.uji.apps.goc.model.punto.PuntoVotable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class RecuentoVotacionTest extends BaseServiceTest {
    @Autowired
    public VotacionService votacionService;
    @Autowired
    public VotacionDAO votacionDAO;
    @Autowired
    public OrganoReunionMiembroService organoReunionMiembroService;
    @Autowired
    public OrganoReunionMiembroDAO organoReunionMiembroDAO;
    @PersistenceContext
    protected EntityManager entityManager;
    private Persona miembro1;
    private Persona miembro2;
    private Persona miembro3;
    private Persona miembro4;
    private Persona miembro5;
    private Persona miembro6;
    private Persona miembro7;
    private Persona suplente;
    private Persona presidente;
    private OrganoReunionMiembro organoReunionMiembro1;
    private Reunion reunion;
    private PuntoOrdenDia puntoConMayoriaSimple;
    private PuntoOrdenDia puntoConMayoriaAbsoultaPresentes;
    private PuntoOrdenDia puntoConMayoriaAbsoultaOrgano;
    private PuntoOrdenDia puntoSecretoConMayoriaAbsoultaOrgano;
    private PuntoOrdenDia puntoConMayoriaDosTerciosOrgano;

    @Before
    public void setUp() {
        miembro1 = new Persona(1000L, "miembro-1", "miembro1@480.com");
        miembro2 = new Persona(2000L, "miembro-2", "miembro2@480.com");
        miembro3 = new Persona(3000L, "miembro-3", "miembro3@480.com");
        miembro4 = new Persona(4000L, "miembro-4", "miembro4@480.com");
        miembro5 = new Persona(5000L, "miembro-5", "miembro5@480.com");
        miembro6 = new Persona(6000L, "miembro-6", "miembro6@480.com");
        miembro7 = new Persona(6500L, "miembro-7", "miembro7@480.com");
        suplente = new Persona(7000L, "suplente", "suplente@480.com");
        presidente = new Persona(8000L, "presidente", "presidente@480.com");

        puntoConMayoriaSimple
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(true)
                .withProcedimientoAbreviado()
                .withVotacionAbierta(true)
                .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_SIMPLE)
                .withTipoVotacion()
                .build(entityManager);

        puntoConMayoriaAbsoultaPresentes
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(true)
                .withProcedimientoAbreviado()
                .withVotacionAbierta(true)
                .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_PRESENTES)
                .withTipoVotacion()
                .build(entityManager);

        puntoConMayoriaAbsoultaOrgano
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(true)
                .withProcedimientoAbreviado()
                .withVotacionAbierta(true)
                .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_ORGANO)
                .withTipoVotacion()
                .build(entityManager);

        puntoSecretoConMayoriaAbsoultaOrgano
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(false)
                .withProcedimientoAbreviado()
                .withVotacionAbierta(true)
                .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_ABSOLUTA_ORGANO)
                .withTipoVotacion()
                .build(entityManager);

        puntoConMayoriaDosTerciosOrgano
                = new PuntoOrdenDiaBuilder()
                .withTipoVoto(true)
                .withProcedimientoAbreviado()
                .withVotacionAbierta(true)
                .withTipoRecuentoVoto(TipoRecuentoVotoEnum.MAYORIA_DOS_TERCIOS)
                .withTipoVotacion()
                .build(entityManager);

        Set<PuntoOrdenDia> puntosOrdenDia = new HashSet<>();
        puntosOrdenDia.add(puntoConMayoriaSimple);
        puntosOrdenDia.add(puntoConMayoriaAbsoultaPresentes);
        puntosOrdenDia.add(puntoConMayoriaAbsoultaOrgano);
        puntosOrdenDia.add(puntoSecretoConMayoriaAbsoultaOrgano);
        puntosOrdenDia.add(puntoConMayoriaDosTerciosOrgano);

        MiembroLocal miembroLocal1 =
                new MiembroLocalBuilder()
                        .withNombre("Miembro-1")
                        .withPersonaId(miembro1.getId())
                        .build(entityManager);

        MiembroLocal miembroLocal2 =
                new MiembroLocalBuilder()
                        .withNombre("Miembro-2")
                        .withPersonaId(miembro2.getId())
                        .build(entityManager);

        MiembroLocal miembroLocal3 =
                new MiembroLocalBuilder()
                        .withNombre("Miembro-3")
                        .withPersonaId(miembro3.getId())
                        .build(entityManager);

        MiembroLocal miembroLocal4 =
                new MiembroLocalBuilder()
                        .withNombre("Miembro-4")
                        .withPersonaId(miembro4.getId())
                        .build(entityManager);

        MiembroLocal miembroLocal5 =
                new MiembroLocalBuilder()
                        .withNombre("Miembro-5")
                        .withPersonaId(miembro5.getId())
                        .build(entityManager);

        MiembroLocal miembroLocal6 =
                new MiembroLocalBuilder()
                        .withNombre("Miembro-6")
                        .withPersonaId(miembro6.getId())
                        .build(entityManager);

        MiembroLocal miembroLocal7 =
                new MiembroLocalBuilder()
                        .withNombre("Miembro-7")
                        .withPersonaId(miembro7.getId())
                        .build(entityManager);

        MiembroLocal presidenteLocal =
                new MiembroLocalBuilder()
                        .withNombre("Presidente")
                        .withPersonaId(presidente.getId())
                        .build(entityManager);

        reunion =
                new ReunionBuilder()
                        .withTelematica(true)
                        .withVotacionTelematica(true)
                        .withAvisoPrimeraReunion(true)
                        .withAcuerdos("Acuerdos")
                        .withCreadorId(CREADOR_ID)
                        .withPuntosOrdenDia(puntosOrdenDia)
                        .withTipoVisualizacionResultados(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION)
                        .withFecha(new Date())
                        .build(entityManager);

        TipoOrganoLocal tipoOrgano = new TipoOrganoBuilder().withNombre("tipo").build(entityManager);

        OrganoLocal organo = new OrganoLocalBuilder()
                .withNombre("OrganoVotar")
                .withTipoOrgano(tipoOrgano)
                .withTipoProcedimientoVotacion(TipoProcedimientoVotacionEnum.ABREVIADO.name())
                .withVotoDoblePresidente(true)
                .build(entityManager);

        OrganoReunion organoReunion =
                new OrganoReunionBuilder()
                        .withOrganoId(organo.getId().toString())
                        .withReunion(reunion)
                        .withExterno(true)
                        .build(entityManager);

        organoReunionMiembro1 = new OrganoReunionMiembroBuilder()
                .withEmail(miembro1.getEmail())
                .withMiembroId(miembroLocal1.getPersonaId())
                .withOrganoReunion(organoReunion)
                .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                .withAsistencia(true)
                .withNombre(miembroLocal1.getNombre())
                .build(entityManager);

        new OrganoReunionMiembroBuilder()
                .withEmail(miembro2.getEmail())
                .withMiembroId(miembroLocal2.getPersonaId())
                .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                .withOrganoReunion(organoReunion)
                .withAsistencia(true)
                .withNombre(miembroLocal2.getNombre())
                .build(entityManager);

        new OrganoReunionMiembroBuilder()
                .withEmail(miembro3.getEmail())
                .withMiembroId(miembroLocal3.getPersonaId())
                .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                .withOrganoReunion(organoReunion)
                .withAsistencia(true)
                .withNombre(miembroLocal3.getNombre())
                .build(entityManager);

        new OrganoReunionMiembroBuilder()
                .withEmail(miembro4.getEmail())
                .withMiembroId(miembroLocal4.getPersonaId())
                .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                .withOrganoReunion(organoReunion)
                .withAsistencia(true)
                .withNombre(miembroLocal4.getNombre())
                .build(entityManager);

        new OrganoReunionMiembroBuilder()
                .withEmail(miembro5.getEmail())
                .withMiembroId(miembroLocal5.getPersonaId())
                .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                .withOrganoReunion(organoReunion)
                .withNombre(miembroLocal5.getNombre())
                .withAsistencia(true)
                .build(entityManager);

        new OrganoReunionMiembroBuilder()
                .withEmail(miembro6.getEmail())
                .withMiembroId(miembroLocal6.getPersonaId())
                .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                .withOrganoReunion(organoReunion)
                .withAsistencia(true)
                .withNombre(miembroLocal6.getNombre())
                .build(entityManager);

        new OrganoReunionMiembroBuilder()
                .withEmail(miembro7.getEmail())
                .withMiembroId(miembroLocal7.getPersonaId())
                .withCargo(CodigoCargoEnum.SECRETARIO.codigo, "Secretario")
                .withOrganoReunion(organoReunion)
                .withAsistencia(true)
                .withNombre(miembroLocal7.getNombre())
                .build(entityManager);

        new OrganoReunionMiembroBuilder()
                .withEmail(presidente.getEmail())
                .withMiembroId(presidente.getId())
                .withCargo(CodigoCargoEnum.PRESIDENTE.codigo, "Presidente")
                .withOrganoReunion(organoReunion)
                .withAsistencia(true)
                .withNombre(presidente.getNombre())
                .build(entityManager);
    }

    @Test
    public void shouldPuntoConMayoriaSimpleAprobadoWhenVotosFavorIsGreaterThanVotosContra()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaSimple.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaSimple, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaSimple, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoPuntoConMayoriaSimple(puntoConMayoriaSimple.getId()), is(true));
    }

    @Test
    public void shouldPuntoConMayoriaSimpleNoAprobadoWhenVotosContraIsGreaterThanVotosFavor()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaSimple.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaSimple, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaSimple, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoPuntoConMayoriaSimple(puntoConMayoriaSimple.getId()), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaSimpleNoAprobadoWhenVotosContraIsEqualsVotosFavor()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaSimple.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaSimple, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaSimple, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoPuntoConMayoriaSimple(puntoConMayoriaSimple.getId()), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaSimpleAprobadoWhenVotosContraIsEqualsVotosFavorAndPresidenteVotoFavor()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaSimple.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaSimple, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaSimple, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(presidente, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoPuntoConMayoriaSimple(puntoConMayoriaSimple.getId()), is(true));
    }

    @Test
    public void shouldPuntoConMayoriaAbsolutaPresentesNoAprobadoWhenVotosFavorIsEqualToHalfTotalVotesAndTotalVotesIsPair()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaAbsoultaPresentes.setVotacionAbierta(true);
        PuntoVotable
                puntoAVotarFavor = new PuntoVotable(puntoConMayoriaAbsoultaPresentes, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable
                puntoAVotarContra = new PuntoVotable(puntoConMayoriaAbsoultaPresentes, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaAbsolutaPresentes(puntoConMayoriaAbsoultaPresentes.getId()), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaAbsolutaPresentesAprobadoWhenVotosFavorIsEqualToHalfTotalVotesAndTotalVotesIsPairAndPresidenteVotoFavor()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaAbsoultaPresentes.setVotacionAbierta(true);
        PuntoVotable
                puntoAVotarFavor = new PuntoVotable(puntoConMayoriaAbsoultaPresentes, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable
                puntoAVotarContra = new PuntoVotable(puntoConMayoriaAbsoultaPresentes, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(presidente, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaAbsolutaPresentes(puntoConMayoriaAbsoultaPresentes.getId()), is(true));
    }

    @Test
    public void shouldPuntoConMayoriaAbsolutaPresentesNoAprobadoWhenVotosFavorIsEqualToHalfTotalVotesAndTotalVotesIsNotPair()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable
                puntoAVotarFavor = new PuntoVotable(puntoConMayoriaAbsoultaPresentes, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable
                puntoAVotarContra = new PuntoVotable(puntoConMayoriaAbsoultaPresentes, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaAbsolutaPresentes(puntoConMayoriaAbsoultaPresentes.getId()), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaAbsolutaPresentesAprobadoWhenVotosFavorIsEqualToHalfPlusOneTotalVotesAndTotalVotesIsNotPair()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable
                puntoAVotarFavor = new PuntoVotable(puntoConMayoriaAbsoultaPresentes, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable
                puntoAVotarContra = new PuntoVotable(puntoConMayoriaAbsoultaPresentes, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaAbsolutaPresentes(puntoConMayoriaAbsoultaPresentes.getId()), is(true));
    }

    @Test
    public void shouldPuntoConMayoriaAbsolutaPresentesNotAprobadoWhenNotVotes() {
        assertThat(votacionDAO.isAprobadoConMayoriaAbsolutaPresentes(puntoConMayoriaAbsoultaPresentes.getId()), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaAbsolutaOrganoNoAprobadoWhenVotosFavorIsEqualToHalfTotalVotesAndTotalVotesIsPair()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable
                puntoAVotarFavor = new PuntoVotable(puntoConMayoriaAbsoultaOrgano, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable
                puntoAVotarContra = new PuntoVotable(puntoConMayoriaAbsoultaOrgano, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro7, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro6, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaAbsolutaOrgano(puntoConMayoriaAbsoultaOrgano), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaAbsolutaOrganoAprobadoWhenVotosFavorIsEqualToHalfTotalVotesAndTotalVotesIsPairAndPresidenteVotoFavor()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable
                puntoAVotarFavor = new PuntoVotable(puntoConMayoriaAbsoultaOrgano, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable
                puntoAVotarContra = new PuntoVotable(puntoConMayoriaAbsoultaOrgano, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(presidente, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro6, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaAbsolutaOrgano(puntoConMayoriaAbsoultaOrgano), is(true));
    }

    @Test
    public void shouldPuntoConMayoriaDosTerciosOrganoAprobadoWhenVotosFavorIsGreaterThanVotosContra()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, VotacionNoAbiertaException,
            TipoVotoNoExisteException, PuntoNoVotableException, PuntoNoVotableEnContraException,
            UsuarioNoTienePermisoParaVotarException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaDosTerciosOrgano.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro6, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro7, puntoAVotarContra);
        votacionService.votarPuntoTitular(presidente, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaDosTercios(puntoConMayoriaDosTerciosOrgano), is(true));
    }

    @Test
    public void shouldPuntoConMayoriaDosTerciosOrganoNoAprobadoWhenVotosFavorIsGreaterThanVotosContra()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, VotacionNoAbiertaException,
            TipoVotoNoExisteException, PuntoNoVotableException, PuntoNoVotableEnContraException,
            UsuarioNoTienePermisoParaVotarException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaDosTerciosOrgano.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro6, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro7, puntoAVotarContra);
        votacionService.votarPuntoTitular(presidente, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaDosTercios(puntoConMayoriaDosTerciosOrgano), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaDosTerciosOrganoNoAprobadoWhenVotosContraIsGreaterThanVotosFavor()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, VotacionNoAbiertaException,
            TipoVotoNoExisteException, PuntoNoVotableException, PuntoNoVotableEnContraException,
            UsuarioNoTienePermisoParaVotarException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaDosTerciosOrgano.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro6, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro7, puntoAVotarContra);
        votacionService.votarPuntoTitular(presidente, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaDosTercios(puntoConMayoriaDosTerciosOrgano), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaDosTerciosOrganoNoAprobadoWhenVotosFavorIsEqualToVotosContraAndPresidenteVotoFavor()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, VotacionNoAbiertaException,
            TipoVotoNoExisteException, PuntoNoVotableException, PuntoNoVotableEnContraException,
            UsuarioNoTienePermisoParaVotarException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaDosTerciosOrgano.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro6, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro7, puntoAVotarContra);
        votacionService.votarPuntoTitular(presidente, puntoAVotarFavor);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaDosTercios(puntoConMayoriaDosTerciosOrgano), is(false));
    }

    @Test
    public void shouldPuntoConMayoriaDosTerciosOrganoNoAprobadoWhenVotosFavorIsEqualToVotosContra()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, VotacionNoAbiertaException,
            TipoVotoNoExisteException, PuntoNoVotableException, PuntoNoVotableEnContraException,
            UsuarioNoTienePermisoParaVotarException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        puntoConMayoriaDosTerciosOrgano.setVotacionAbierta(true);
        PuntoVotable puntoAVotarFavor = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable puntoAVotarContra = new PuntoVotable(puntoConMayoriaDosTerciosOrgano, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro5, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro6, puntoAVotarContra);
        votacionService.votarPuntoTitular(miembro7, puntoAVotarContra);
        votacionService.votarPuntoTitular(presidente, puntoAVotarContra);

        entityManager.flush();
        entityManager.clear();

        assertThat(votacionDAO.isAprobadoConMayoriaDosTercios(puntoConMayoriaDosTerciosOrgano), is(false));
    }

    @Test
    public void shouldGetVotantesNombresWhenGetResultadosVotacionSecreta()
            throws TipoRecuentoVotoNoExisteException, TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, ReunionNoAbiertaException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable puntoAVotar = new PuntoVotable(puntoSecretoConMayoriaAbsoultaOrgano, reunionAVotar, TipoVotoEnum.CONTRA.name());
        votacionService.votarPuntoTitular(miembro1, puntoAVotar);
        votacionService.votarPuntoTitular(miembro2, puntoAVotar);

        entityManager.flush();
        entityManager.clear();

        List<PuntoOrdenDiaRecuentoVotosDTO> recuentoVotosByReunionId =
                votacionService.getRecuentoVotosByReunion(reunion);
        List<PuntoOrdenDiaRecuentoVotosTemplate> recuentoVotosTemplates = new ArrayList<>();
        for (PuntoOrdenDiaRecuentoVotosDTO puntoOrdenDiaRecuentoVotosDTO : recuentoVotosByReunionId) {
            if (puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getId().equals(puntoSecretoConMayoriaAbsoultaOrgano.getId())) {
                Assert.assertFalse(puntoOrdenDiaRecuentoVotosDTO.getVotantesNombre().isEmpty());
                recuentoVotosTemplates.add(new PuntoOrdenDiaRecuentoVotosTemplate(puntoOrdenDiaRecuentoVotosDTO));
            }
        }

        for (PuntoOrdenDiaRecuentoVotosTemplate recuentoVotosTemplate : recuentoVotosTemplates) {
            if (recuentoVotosTemplate.getId().equals(puntoSecretoConMayoriaAbsoultaOrgano.getId())) {
                Assert.assertFalse(recuentoVotosTemplate.getVotantesNombre().isEmpty());
            }
        }
    }


    @Test
    public void shouldResultadosVotacionMientrasActivaNoDeberianMostrarAbstencionesSiNoHanVotado()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException,
            TipoRecuentoVotoNoExisteException, VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {

        ReunionAVotar reunionAVotar = realizarVotacionSobrePuntoDelDia(reunion, puntoConMayoriaAbsoultaOrgano);

        entityManager.flush();
        entityManager.clear();

        List<OrganoReunionMiembro> miembros = organoReunionMiembroDAO.getMiembrosByReunionId(reunion.getId());
        List<PuntoOrdenDiaRecuentoVotosDTO> recuentoVotos = votacionService.getRecuentoVotosByReunion(reunion);

        String miembroNoVotanteNombreCapitalize = getCapitalized(miembro6.getNombre());

        for (PuntoOrdenDiaRecuentoVotosDTO puntoOrdenDia : recuentoVotos) {
            if (puntoOrdenDia.getPuntoOrdenDia().getId().equals(puntoConMayoriaAbsoultaOrgano.getId())) {
                Assert.assertNotEquals(miembros.size(), puntoOrdenDia.getRecuentoVotos().intValue());
                Assert.assertFalse("Un miembro autorizado para votar y que no ha votado, no aparece en el recuento de votos",
                        puntoOrdenDia.getVotantesNombre().stream().anyMatch(votanteVoto -> votanteVoto.getNombreTitular().equals(miembroNoVotanteNombreCapitalize)));
            }
        }
    }

    @Test
    public void shouldResultadosVotacionEnReunionCompletadaNoDeberianMostrarAbstencionesSiNoHanVotado()
            throws NoVotacionTelematicaException, ReunionNoAbiertaException, NoEsTelematicaExceptionConVotacion,
            TipoVotoNoExisteException, PuntoSecretoNoAdmiteCambioVotoException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException,
            TipoRecuentoVotoNoExisteException, VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {

        ReunionAVotar reunionAVotar = realizarVotacionSobrePuntoDelDia(reunion, puntoConMayoriaAbsoultaOrgano);

        reunion.setCompletada(true);

        entityManager.flush();
        entityManager.clear();

        List<OrganoReunionMiembro> miembros = votacionService.getAsistentesByReunionId(reunionAVotar.getReunionId());
        List<PuntoOrdenDiaRecuentoVotosDTO> recuentoVotos = votacionService.getRecuentoVotosByReunion(reunion);

        String miembroNoVotanteNombreCapitalize = getCapitalized(miembro6.getNombre());

        for (PuntoOrdenDiaRecuentoVotosDTO puntoOrdenDia : recuentoVotos) {
            if (puntoOrdenDia.getPuntoOrdenDia().getId().equals(puntoConMayoriaAbsoultaOrgano.getId())) {
                Assert.assertNotEquals(miembros.size(), puntoOrdenDia.getRecuentoVotos().intValue());
                Assert.assertFalse("Un miembro autorizado para votar y que no ha votado, no aparece en el recuento de votos",
                        puntoOrdenDia.getVotantesNombre().stream().anyMatch(votanteVoto -> votanteVoto.getNombreTitular().equals(miembroNoVotanteNombreCapitalize)));
            }
        }
        reunion.setCompletada(false);
    }

    private ReunionAVotar realizarVotacionSobrePuntoDelDia(Reunion reunion, PuntoOrdenDia puntoOrdenDia)
            throws NoEsTelematicaExceptionConVotacion, NoVotacionTelematicaException, ReunionNoAbiertaException,
            PuntoNoVotableException, TipoVotoNoExisteException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            PuntoSecretoNoAdmiteCambioVotoException, AsistenciaNoConfirmadaException, VotoDelegadoException,
            UsuarioNoTienePermisoParaVotarException, VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {
        ReunionAVotar reunionAVotar = new ReunionAVotar(reunion);
        PuntoVotable
                puntoAVotarFavor = new PuntoVotable(puntoOrdenDia, reunionAVotar, TipoVotoEnum.FAVOR.name());
        PuntoVotable
                puntoAVotarContra = new PuntoVotable(puntoOrdenDia, reunionAVotar, TipoVotoEnum.CONTRA.name());

        votacionService.votarPuntoTitular(miembro1, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro2, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro3, puntoAVotarFavor);
        votacionService.votarPuntoTitular(miembro4, puntoAVotarContra);
        return reunionAVotar;
    }

    private String getCapitalized(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

}
