package es.uji.apps.goc.notifications;

import es.uji.apps.goc.DateUtils;
import es.uji.apps.goc.charset.ResourceBundleUTF8;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.Reunion;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnit4.class)
public class AvisosReunionTest {

    private static ResourceBundleUTF8 rbCatala = new ResourceBundleUTF8("i18nEmails", "ca");
    private static ResourceBundleUTF8 rbCastellano = new ResourceBundleUTF8("i18nEmails", "es");

    @Test
    public void shouldEnviaAvisoAltaSuplenteTextAuxHaveCorretText(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);
        String textCatala = avisosReunion.getTextoAuxiliar("Miembro", "Cargo","mail.reunion.suplenteDesignado", rbCatala);
        String textCastellano = avisosReunion.getTextoAuxiliar("Miembro", "Cargo", "mail.reunion.suplenteDesignado",rbCastellano);
        assertThat(textCatala, is("Heu estat dessignat com a suplent de Miembro (Cargo)"));
        assertThat(textCastellano, is("Ha sido designado como suplente de Miembro (Cargo)"));
    }

    @Test
    public void shouldEnviaAvisoAltaSuplenteAsuntoHaveCorretText(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);
        OrganoReunion organoReunion = new OrganoReunion();
        organoReunion.setOrganoNombre("Órgano");
        Set<OrganoReunion> organoList = new HashSet<OrganoReunion>();
        organoList.add(organoReunion);
        Reunion reunion = new Reunion();
        reunion.setReunionOrganos(organoList);
        String textCatala = avisosReunion.getTextoAsuntoReunion(rbCatala, "mail.reunion.suplenteEnReunion",reunion);
        String textCastellano = avisosReunion.getTextoAsuntoReunion(rbCastellano, "mail.reunion.suplenteEnReunion",reunion);
        assertThat(textCatala, is("Suplent en reunió Órgano"));
        assertThat(textCastellano, is("Suplente en reunión Órgano"));
    }

    @Test
    public void shouldTextosEnviaAvisoBajaSuplenteBeCorrect(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);
        String textCatala = avisosReunion.getTextoAuxiliar("Miembro", "Cargo","mail.reunion.suplenteBaja", rbCatala);
        String textCastellano = avisosReunion.getTextoAuxiliar("Miembro", "Cargo", "mail.reunion.suplenteBaja",rbCastellano);
        assertThat(textCatala, is("Heu estat donat de baixa com a suplent de Miembro (Cargo)"));
        assertThat(textCastellano, is("Ha sido dado de baja como suplente de Miembro (Cargo)"));

        OrganoReunion organoReunion = new OrganoReunion();
        organoReunion.setOrganoNombre("Órgano");
        Set<OrganoReunion> organoList = new HashSet<OrganoReunion>();
        organoList.add(organoReunion);
        Reunion reunion = new Reunion();
        reunion.setReunionOrganos(organoList);
        String asuntoCatala = avisosReunion.getTextoAsuntoReunion(rbCatala, "mail.reunion.suplenteBajaEnReunion",reunion);
        String asuntoCastellano = avisosReunion.getTextoAsuntoReunion(rbCastellano, "mail.reunion.suplenteBajaEnReunion",reunion);
        assertThat(asuntoCatala, is("Baixa com a suplent en reunió Órgano"));
        assertThat(asuntoCastellano, is("Baja como suplente en reunión Órgano"));
    }

    @Test
    public void shouldTextosenviaAvisoAltaDelegacionVotoBeCorrect(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);
        String textCatala = avisosReunion.getTextoAuxiliar("Miembro", "Cargo","mail.reunion.delegacionVoto", rbCatala);
        String textCastellano = avisosReunion.getTextoAuxiliar("Miembro", "Cargo", "mail.reunion.delegacionVoto",rbCastellano);
        assertThat(textCatala, is("Se vos ha dessignat la delegació de vot de Miembro (Cargo)"));
        assertThat(textCastellano, is("Se os ha designado la delegación de voto de Miembro (Cargo)"));

        OrganoReunion organoReunion = new OrganoReunion();
        organoReunion.setOrganoNombre("Órgano");
        Set<OrganoReunion> organoList = new HashSet<OrganoReunion>();
        organoList.add(organoReunion);
        Reunion reunion = new Reunion();
        reunion.setReunionOrganos(organoList);
        String asuntoCatala = avisosReunion.getTextoAsuntoReunion(rbCatala, "mail.reunion.delegacionVotoEnReunion",reunion);
        String asuntoCastellano = avisosReunion.getTextoAsuntoReunion(rbCastellano, "mail.reunion.delegacionVotoEnReunion",reunion);
        assertThat(asuntoCatala, is("Delegació de vot en reunió Órgano"));
        assertThat(asuntoCastellano, is("Delegación de voto en reunión Órgano"));
    }

    @Test
    public void shouldTextosEnviaAvisoBajaDelegacionVotoBeCorrect(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);
        String textCatala = avisosReunion.getTextoAuxiliar("Miembro", "Cargo","mail.reunion.delegacionVotoBaja", rbCatala);
        String textCastellano = avisosReunion.getTextoAuxiliar("Miembro", "Cargo", "mail.reunion.delegacionVotoBaja",rbCastellano);
        assertThat(textCatala, is("Heu estat donat de baixa de la delegació de vot de Miembro (Cargo)"));
        assertThat(textCastellano, is("Ha sido dado de baja de la delegación de voto de Miembro (Cargo)"));

        OrganoReunion organoReunion = new OrganoReunion();
        organoReunion.setOrganoNombre("Órgano");
        Set<OrganoReunion> organoList = new HashSet<OrganoReunion>();
        organoList.add(organoReunion);
        Reunion reunion = new Reunion();
        reunion.setReunionOrganos(organoList);
        String asuntoCatala = avisosReunion.getTextoAsuntoReunion(rbCatala, "mail.reunion.delegacionVotoBajaEnReunion",reunion);
        String asuntoCastellano = avisosReunion.getTextoAsuntoReunion(rbCastellano, "mail.reunion.delegacionVotoBajaEnReunion",reunion);
        assertThat(asuntoCatala, is("Baixa de delegació de vot en reunió Órgano"));
        assertThat(asuntoCastellano, is("Baja de delegación de voto en reunión Órgano"));
    }

    @Test
    public void shouldTextosenviaAvisoNuevaReunionBeCorrect(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);

        OrganoReunion organoReunion = new OrganoReunion();
        organoReunion.setOrganoNombre("Órgano");
        Set<OrganoReunion> organoList = new HashSet<OrganoReunion>();
        organoList.add(organoReunion);
        Reunion reunion = new Reunion();
        reunion.setAvisoPrimeraReunion(true);
        reunion.setReunionOrganos(organoList);
        String asuntoAvisadaCatala = avisosReunion.getAsuntoReunionNuevaORectificada(reunion, rbCatala, "[GOC]");
        String asuntoAvisadaCastellano = avisosReunion.getAsuntoReunionNuevaORectificada(reunion, rbCastellano, "[GOC]");
        assertThat(asuntoAvisadaCatala, is("[GOC] [Rectificada] convocatòria Órgano"));
        assertThat(asuntoAvisadaCastellano, is("[GOC] [Rectificada] convocatoria Órgano"));

        reunion.setAvisoPrimeraReunion(false);
        String asuntoNoAvisadaCatala = avisosReunion.getAsuntoReunionNuevaORectificada(reunion, rbCatala, "[GOC]");
        String asuntoNoAvisadaCastellano = avisosReunion.getAsuntoReunionNuevaORectificada(reunion, rbCastellano, "[GOC]");
        assertThat(asuntoNoAvisadaCatala, is("[GOC] convocatòria Órgano"));
        assertThat(asuntoNoAvisadaCastellano, is("[GOC] convocatoria Órgano"));
    }

    @Test
    public void shouldenviaAvisoReunionProximaBeCorrect(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Reunion reunion = new Reunion();
        reunion.setAsunto("ASUNTO");
        Date date = new Date();
        date.setTime(10L);
        reunion.setFecha(date);
        String reunionProximacatala = avisosReunion.getAsuntoEnviaAvisoReunionProxima(reunion, rbCatala, df, DateUtils.getDiaSemanaTexto(reunion.getFecha(), "ca"));
        String reunionProximaCastellano = avisosReunion.getAsuntoEnviaAvisoReunionProxima(reunion, rbCastellano, df, DateUtils.getDiaSemanaTexto(reunion.getFecha(), "es"));
        assertThat(reunionProximacatala, is("Recordatori reunió: ASUNTO dijous (01/01/1970 01:00)"));
        assertThat(reunionProximaCastellano, is("Recordatorio reunión: ASUNTO jueves (01/01/1970 01:00)"));

    }

    @Test
    public void shouldTextosEnviaAvisoNuevoComentarioAConvocanteBeCorrect(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);

        String reunionAsuntoNuevoComentariocatala = avisosReunion.getTexto( rbCatala,"mail.reunion.nuevoComentario");
        String reunionAsuntoNuevoComentarioCastellano = avisosReunion.getTexto( rbCastellano,"mail.reunion.nuevoComentario");
        assertThat(reunionAsuntoNuevoComentariocatala, is("Nou comentari en la reunió"));
        assertThat(reunionAsuntoNuevoComentarioCastellano, is("Nuevo comentario en la reunión"));

        String anadioComentariocatala = avisosReunion.getTexto( rbCatala,"mail.reunion.anadioNuevoComentario");
        String anadioComentarioCastellano = avisosReunion.getTexto( rbCastellano,"mail.reunion.anadioNuevoComentario");
        assertThat(anadioComentariocatala, is("va afegir un comentari."));
        assertThat(anadioComentarioCastellano, is("añadió un nuevo comentario."));

        String masinfoCatala = avisosReunion.getTexto( rbCatala,"mail.reunion.masInfo");
        String masInfoCastellano = avisosReunion.getTexto( rbCastellano,"mail.reunion.masInfo");
        assertThat(masinfoCatala, is("Per a més informació, podeu consultar el detall de la reunió a "));
        assertThat(masInfoCastellano, is("Para más información, puede consultar el detalle de la reunión en "));
    }

    @Test
    public void shouldTextosEnviarAvisoNuevoComentarioPuntoOrdenDiaAsistentesAutorizadosBeCorrect(){
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);

        String reunionAsuntoNuevoComentariocatala = avisosReunion.getTexto( rbCatala,"mail.reunion.nuevoComentario");
        String reunionAsuntoNuevoComentarioCastellano = avisosReunion.getTexto( rbCastellano,"mail.reunion.nuevoComentario");
        assertThat(reunionAsuntoNuevoComentariocatala, is("Nou comentari en la reunió"));
        assertThat(reunionAsuntoNuevoComentarioCastellano, is("Nuevo comentario en la reunión"));

        String anadioComentarioPuntoCatala = avisosReunion.getTexto( rbCatala,"mail.reunion.comentarioPunto");
        String anadioComentarioPuntoCastellano = avisosReunion.getTexto( rbCastellano,"mail.reunion.comentarioPunto");
        assertThat(anadioComentarioPuntoCatala, is("Comentari afegit al punt"));
        assertThat(anadioComentarioPuntoCastellano, is("Comentario añadido en el punto"));

        String masinfoCatala = avisosReunion.getTexto( rbCatala,"mail.reunion.masInfo");
        String masInfoCastellano = avisosReunion.getTexto( rbCastellano,"mail.reunion.masInfo");
        assertThat(masinfoCatala, is("Per a més informació, podeu consultar el detall de la reunió a "));
        assertThat(masInfoCastellano, is("Para más información, puede consultar el detalle de la reunión en "));
    }


    @Test
    public void shouldTextosGetMensajeAvisoAnulacionBeCorrect() {
        AvisosReunion avisosReunion = new AvisosReunion(null, null, null, null, null, null);

        String reunionAnulacionCatala = avisosReunion.getTexto(rbCatala, "mail.reunion.anulacion");
        String reunionAnulacionCastellano = avisosReunion.getTexto(rbCastellano, "mail.reunion.anulacion");
        assertThat(reunionAnulacionCatala, is("[ANUL·LACIÓ]"));
        assertThat(reunionAnulacionCastellano, is("[ANULACIÓN]"));


        String convocatoriaCatala = avisosReunion.getTexto(rbCatala, "mail.reunion.convocatoria");
        String convocatoriaCastellano = avisosReunion.getTexto(rbCastellano, "mail.reunion.convocatoria");
        assertThat(convocatoriaCatala, is("convocatòria"));
        assertThat(convocatoriaCastellano, is("convocatoria"));
    }
}
