package es.uji.apps.goc.services;

import com.google.common.base.Strings;
import es.uji.apps.goc.dao.*;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.VotanteRepresentado;
import es.uji.apps.goc.notifications.AvisosReunion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Component
public class OrganoReunionMiembroService {
    @Autowired
    private OrganoReunionMiembroDAO organoReunionMiembroDAO;

    @Autowired
    private OrganoReunionInvitadoDAO organoReunionInvitadoDAO;

    @Autowired
    private MiembroService miembroService;

    @Autowired
    private OrganoReunionDAO organoReunionDAO;

    @Autowired
    private CargoDAO cargoDAO;

    @Autowired
    private ReunionService reunionService;

    @Autowired
    private AvisosReunion avisosReunion;

    @Autowired
    private OrganoInvitadoDAO organoInvitadoDAO;

    @Autowired
    private VotacionDAO votacionDAO;

    public boolean isExcusableByMiembroReunion(Reunion reunion, Long connectedUserId) {
        List<OrganoReunionMiembro> miembroAsistente = organoReunionMiembroDAO.getMiembroByAsistenteIdOrSuplenteId(reunion.getId(), connectedUserId);
        return miembroAsistente.size() == 0 || miembroAsistente.get(0).getPersonasPuntoVoto().isEmpty();
    }

    @Transactional
    public void updateOrganoReunionInvitadosDesdeOrganosUI(List<UIEntity> organosUI, Long reunionId,
                                                           Long connectedUserId) {
        organoReunionInvitadoDAO.deleteAllByReunionId(reunionId);

        if (organosUI != null) {
            for (UIEntity organoUI : organosUI) {
                updateOrganoReunionInvitadosDesdeOrganoUI(organoUI, reunionId);
            }
        }
    }

    @Transactional
    public void updateOrganoReunionInvitadosDesdeOrganoUI(UIEntity organoUI, Long reunionId) {
        String organoId = organoUI.get("id");
        Boolean organoExterno = Boolean.valueOf(organoUI.get("externo"));
        if (organoId == null) return;

        List<OrganoInvitado> organoInvitados = organoInvitadoDAO.getInvitadosByOrgano(organoId, organoExterno);

        for (OrganoInvitado organoInvitado : organoInvitados) {
            OrganoReunionInvitado organoReunionInvitado = new OrganoReunionInvitado();

            organoReunionInvitado.setReunionId(reunionId);
            organoReunionInvitado.setPersonaId(organoInvitado.getPersonaId().toString());
            organoReunionInvitado.setEmail(organoInvitado.getPersonaEmail());
            organoReunionInvitado.setNombre(organoInvitado.getPersonaNombre());
            organoReunionInvitado.setOrganoExterno(new Boolean(organoUI.get("externo")));
            organoReunionInvitado.setOrganoId(organoId);
            organoReunionInvitado.setSoloConsulta(organoInvitado.isSoloConsulta());
            organoReunionInvitado.setAsistencia(true);

            OrganoReunion organoReunion = organoReunionDAO.getOrganoReunionByReunionIdAndOrganoId(reunionId, ParamUtils.parseLong(organoId));

            organoReunionInvitado.setOrganoReunion(organoReunion);

            organoReunionInvitadoDAO.insert(organoReunionInvitado);
        }
    }

    public void updateOrganoReunionMiembrosDesdeOrganosUI(List<UIEntity> organosUI, Long reunionId,
                                                          Long connectedUserId) throws Exception {
        if (organosUI != null) {
            for (UIEntity organoUI : organosUI) {
                updateOrganoReunionMiembrosDesdeOrganoUI(organoUI, reunionId);
            }
        }
    }

    @Transactional
    protected void addOrganoReunionMiembros(OrganoReunion organoReunion, Long connectedUserId)
            throws MiembrosExternosException {
        List<Miembro> miembros = new ArrayList();
        if (organoReunion.isExterno()) {
            miembros = miembroService.getMiembrosExternos(organoReunion.getOrganoId(), connectedUserId);
            updateCargoMiembrosIfResponsableActa(miembros);
        } else {
            miembros = miembroService.getMiembrosLocales(Long.parseLong(organoReunion.getOrganoId()), connectedUserId);
        }

        for (Miembro miembro : miembros) {
            OrganoReunionMiembro organoReunionMiembro = miembro.toOrganoReunionMiembro(organoReunion);
            organoReunionMiembroDAO.insert(organoReunionMiembro);
        }
    }

    private void updateCargoMiembrosIfResponsableActa(List<Miembro> miembros) {
        List<String> cargoResponsablesActa = cargoDAO.getCargoResponsableActaIds();
        miembros.forEach(miembro -> {
            es.uji.apps.goc.model.Cargo cargoMiembro = miembro.getCargo();
            cargoMiembro.setResponsableActa(cargoResponsablesActa.contains(miembro.getCargo().getId()));
        });
    }

    @Transactional
    public void updateOrganoReunionMiembrosDesdeOrganoUI(UIEntity organoUI, Long reunionId) throws Exception {
        List<UIEntity> miembrosUI = organoUI.getRelations().get("miembros");
        String organoId = organoUI.get("id");
        Boolean externo = new Boolean(organoUI.get("externo"));

        if (miembrosUI == null) {
            return;
        }

        for (UIEntity miembroUI : miembrosUI) {
            String email = miembroUI.get("email");
            Long suplenteId = Long.parseLong(miembroUI.get("suplenteId"));
            String suplenteNombre = miembroUI.get("suplenteNombre");
            String suplenteEmail = miembroUI.get("suplenteEmail");

            String delegadoVotoNombre = miembroUI.get("delegadoVotoNombre");
            String delegadoVotoEmail = miembroUI.get("delegadoVotoEmail");


            Long delegadoVotoId = null;


            if (miembroUI.get("delegadoVotoId") != null) {
                delegadoVotoId = Long.parseLong(miembroUI.get("delegadoVotoId"));

                if (delegadoVotoId.equals(0L)) {
                    delegadoVotoId = null;
                }

            }


            if (suplenteId.equals(0L)) {
                suplenteId = null;
            }

            Boolean asistencia = new Boolean(miembroUI.get("asistencia"));
            Boolean justificaAusencia = new Boolean(miembroUI.get("justificaAusencia"));
            Boolean guardarAsistencia = false;

            OrganoReunionMiembro miembroGuardado =
                    organoReunionMiembroDAO.getByReunionAndOrganoAndEmail(reunionId, organoId, externo, email);

            if (!asistencia.equals(miembroGuardado.getAsistencia())) {
                guardarAsistencia = true;
            }

            enviaMailSuplente(reunionId, suplenteId, suplenteEmail, miembroGuardado);

            organoReunionMiembroDAO.updateAsistenteReunionByEmail(reunionId, organoId, externo, email, asistencia,
                    suplenteId, suplenteNombre, suplenteEmail, guardarAsistencia, justificaAusencia, delegadoVotoId, delegadoVotoNombre, delegadoVotoEmail);
        }
    }

    @Transactional
    public void updateOrganoReunionMiembroUI(UIEntity miembroOrganoUI) throws Exception {

        UIEntity miembroUI = miembroOrganoUI.getRelations().get("miembro").get(0);

        Long reunionId = Long.parseLong(miembroUI.get("reunionId"));
        String organoId = miembroUI.get("organoId");
        Boolean externo = organoReunionMiembroDAO.getMiembroById(miembroUI.getLong("id")).getOrganoExterno();
        String email = miembroUI.get("email");
        Long suplenteId = Long.parseLong(miembroUI.get("suplenteId"));
        String suplenteNombre = miembroUI.get("suplenteNombre");
        String suplenteEmail = miembroUI.get("suplenteEmail");
        if (suplenteId.equals(0L)) {
            suplenteId = null;
        }
        Boolean asistencia = new Boolean(miembroUI.get("asistencia"));
        Boolean justificaAusencia = new Boolean(miembroUI.get("justificaAusencia"));
        Boolean guardarAsistencia = false;

        OrganoReunionMiembro miembroGuardado =
                organoReunionMiembroDAO.getByReunionAndOrganoAndEmail(reunionId, organoId, externo, email);

        String delegadoVotoNombre = miembroUI.get("delegadoVotoNombre");
        String delegadoVotoEmail = miembroUI.get("delegadoVotoEmail");

        Long delegadoVotoId = null;

        if (miembroUI.get("delegadoVotoId") != null) {
            delegadoVotoId = Long.parseLong(miembroUI.get("delegadoVotoId"));

            if (delegadoVotoId.equals(0L)) {
                delegadoVotoId = null;
            }

        }

        if (!asistencia.equals(miembroGuardado.getAsistencia())) {
            guardarAsistencia = true;
        }

        if (suplenteId != null) {
            for (OrganoReunionMiembro miembro : organoReunionMiembroDAO.getMiembrosByReunionId(reunionId)) {
                if (miembro.getMiembroId().equals(suplenteId.toString())) {
                    throw new MiembroNoPuedeSerSuplenteException();
                }

                if (miembro.getSuplenteId() != null && miembro.getSuplenteId().equals(suplenteId) && !miembro.getMiembroId().toString().equals(miembroUI.get("miembroId"))) {
                    throw new SuplenteYaAsignadoAnteriormente();
                }
            }
        }

        enviaMailSuplente(reunionId, suplenteId, suplenteEmail, miembroGuardado);

        organoReunionMiembroDAO.updateAsistenteReunionByEmail(reunionId, organoId, externo, email, asistencia,
                suplenteId, suplenteNombre, suplenteEmail, guardarAsistencia, justificaAusencia, delegadoVotoId, delegadoVotoNombre, delegadoVotoEmail);
    }


    @Transactional
    public void estableceAsistencia(
            Long reunionMiembroId,
            Long connectedUserId,
            Boolean asistencia,
            String justificaion
    )
            throws AsistenteNoEncontradoException, VotacionYaRealizadaException {
        OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(reunionMiembroId);

        if (!miembro.getPersonasPuntoVoto().isEmpty() && !asistencia) {
            throw new VotacionYaRealizadaException();
        }

        miembro.setAsistenciaConfirmada(asistencia);
        miembro.setAsistencia(asistencia);
        miembro.setMotivoAusencia(justificaion);
        miembro.setJustificaAusencia(!Strings.isNullOrEmpty(justificaion) && asistencia != null && !asistencia);
        organoReunionMiembroDAO.update(miembro);
    }

    @Transactional
    public void estableceSuplente(Long reunionId, Long connectedUserId, Long suplenteId, String suplenteNombre,
                                  String suplenteEmail, Long organoMiembroId) throws Exception {
        OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(organoMiembroId);

        if (votacionDAO.hasPersonaVotosEmitidosByReunionId(reunionId, connectedUserId, suplenteId))
            throw new UsuarioYaHaVotadoException();

        if (organoReunionMiembroDAO.vecesSuplenteEnReunion(suplenteId, reunionId) >= 1L) {
            throw new SuplenteYaAsignadoAnteriormente();
        }

        if (organoReunionMiembroDAO.soyDelegadoEnReunion(reunionId, connectedUserId)) {

            for (OrganoReunionMiembro miembroAfectado : organoReunionMiembroDAO.getMiembrosDeLosQueSoyDelegado(reunionId, connectedUserId)) {
                miembroAfectado.setDelegadoVotoId(null);
                miembroAfectado.setDelegadoVotoEmail(null);
                miembroAfectado.setDelegadoVotoNombre(null);

                organoReunionMiembroDAO.update(miembroAfectado);

                avisosReunion.enviarMailAMiembroPorDelegadoEliminado(reunionId, miembroAfectado);
            }
        }

        enviaMailSuplente(reunionId, suplenteId, suplenteEmail, miembro);

        miembro.setSuplenteId(suplenteId);
        miembro.setSuplenteNombre(suplenteNombre);
        miembro.setSuplenteEmail(suplenteEmail);

        organoReunionMiembroDAO.update(miembro);
    }

    @Transactional
    public void estableceDelegadoVoto(Long reunionId, Long connectedUserId, Long delegadoVotoId,
                                      String delegadoVotoNombre, String delegadoVotoEmail, Long organoMiembroId) throws Exception {
        if (reunionService.isReunionAdmiteDelegacionVoto(reunionId)) {
            OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(organoMiembroId);
            OrganoReunionMiembro miembroDelegado = organoReunionMiembroDAO.getOrganoReunionMiembroByPersonaAndReunionId(new Persona(delegadoVotoId, delegadoVotoNombre, delegadoVotoEmail), reunionId);

            if (miembroAsiste(miembro)) {
                throw new AsistenteNoPuedeDelegarVotoException();
            } else {
                if (hasMiembroVotos(miembro)) {
                    throw new UsuarioYaHaVotadoException();
                } else if (hasDelegadoOSuplente(miembroDelegado)) {
                    throw new UsuarioTieneDelegadoOSuplenteException();
                } else {
                    if (organoReunionMiembroDAO.soyDelegadoEnReunion(reunionId, connectedUserId)) {
                        if (votacionDAO.hasPersonaVotosEmitidosByReunionId(reunionId, connectedUserId, connectedUserId)) {
                            throw new UsuarioYaHaVotadoException();
                        }

                        for (OrganoReunionMiembro miembroAfectado : organoReunionMiembroDAO.getMiembrosDeLosQueSoyDelegado(reunionId, connectedUserId)) {
                            miembroAfectado.setDelegadoVotoId(null);
                            miembroAfectado.setDelegadoVotoEmail(null);
                            miembroAfectado.setDelegadoVotoNombre(null);

                            organoReunionMiembroDAO.update(miembroAfectado);

                            avisosReunion.enviarMailAMiembroPorDelegadoEliminado(reunionId, miembroAfectado);
                        }
                    }

                    if (miembroDelegadoAsiste(miembroDelegado)) {
                        if (reunionService.compruebaDelegacionVotoMultipleEnOrgano(delegadoVotoId, Long.parseLong(miembro.getOrganoId()), miembro.getOrganoExterno(), reunionId)) {
                            enviaMailDelegadoVoto(reunionId, delegadoVotoId, delegadoVotoEmail, miembro);

                            miembro.setDelegadoVotoId(delegadoVotoId);
                            miembro.setDelegadoVotoEmail(delegadoVotoEmail);
                            miembro.setDelegadoVotoNombre(delegadoVotoNombre);
                            miembro.setAsistencia(false);

                            organoReunionMiembroDAO.update(miembro);
                        } else {
                            throw new DelegadoVotoYaAsignadoAnteriormente();
                        }
                    } else {
                        throw new NoPuedeDelegarVotoEnMiembroNoAsistenteException();
                    }
                }
            }
        } else {
            throw new ReunionNoAdmiteDelegacionVotoException();
        }
    }

    private boolean hasDelegadoOSuplente(OrganoReunionMiembro miembro) {
        return miembro.getDelegadoVotoId() != null || miembro.getSuplenteId() != null;
    }

    public boolean hasMiembroVotos(OrganoReunionMiembro miembro) {
        Set<PersonaPuntoVotoDTO> votosPublicos = miembro.getPersonasPuntoVoto();
        Set<VotantePrivadoDTO> votosPrivados = miembro.getVotantesPrivados();
        return votosPublicos != null && votosPublicos.size() > 0 || votosPrivados != null && votosPrivados.size() > 0;
    }

    private boolean miembroAsiste(OrganoReunionMiembro miembro) {
        return miembro.getAsistencia() != null && miembro.getAsistencia() && miembro.isHaConfirmado() != null && miembro.isHaConfirmado();
    }

    private boolean miembroDelegadoAsiste(OrganoReunionMiembro miembro) {
        return miembro.getAsistencia() != null && miembro.getAsistencia() || miembro.getAsistenciaConfirmada() != null
                && miembro.getAsistenciaConfirmada();
    }

    @Transactional
    public void borraSuplente(Long reunionId, Long miembroId, Long connectedUserId) throws Exception {
        OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(miembroId);

        if (votacionDAO.hasPersonaVotosEmitidosByReunionId(reunionId, connectedUserId, miembro.getSuplenteId()))
            throw new NoPuedeEliminarSuplenteSiYaSeHaVotadoException();

        enviaMailSuplente(reunionId, null, null, miembro);

        miembro.setSuplenteId(null);
        miembro.setSuplenteNombre(null);
        miembro.setSuplenteEmail(null);

        organoReunionMiembroDAO.update(miembro);
    }

    @Transactional
    public void borraDelegadoVoto(Long reunionId, Long miembroId) throws Exception {
        OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(miembroId);
        if (hasMiembroVotos(miembro)) {
            throw new UsuarioYaHaVotadoException();
        } else {
            enviaMailDelegadoVoto(reunionId, null, null, miembro);

            miembro.setDelegadoVotoId(null);
            miembro.setDelegadoVotoEmail(null);
            miembro.setDelegadoVotoNombre(null);
            miembro.setAsistencia(true);

            organoReunionMiembroDAO.update(miembro);
        }
    }

    public boolean tieneSuplente(Long reunionMiembroId) {
        if (reunionMiembroId != null) {
            OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(reunionMiembroId);

            if (miembro.getSuplenteId() != null) return true;
        }

        return false;
    }

    public boolean esElSuplente(Long reunionMiembroId, Long personaId) {
        if (reunionMiembroId != null) {
            OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(reunionMiembroId);

            if (miembro.getSuplenteId() != null && miembro.getSuplenteId().equals(personaId)) return true;
        }

        return false;
    }

    public boolean tieneDelegado(Long reunionMiembroId) {
        if (reunionMiembroId != null) {
            OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(reunionMiembroId);

            if (miembro.getDelegadoVotoId() != null) return true;
        }
        return false;
    }

    public boolean asiste(Long reunionMiembroId) {
        if (reunionMiembroId != null) {
            OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(reunionMiembroId);

            return miembro.getAsistencia();
        }
        return false;
    }

    public List<VotanteRepresentado> getVotantesRepresentadosByReunionIdAndPersonaId(Long reunionId, Long personaId) {
        return organoReunionMiembroDAO.getVotantesRepresentadosByReunionIdAndPersonaId(reunionId, personaId);
    }

    public Long getOrganoReunionMiembroIdByPersonaAndReunionId(Persona persona, Long reunionId) {
        OrganoReunionMiembro organoReunionMiembroByPersonaAndReunionId =
                organoReunionMiembroDAO.getOrganoReunionMiembroByPersonaAndReunionId(persona, reunionId);

        return organoReunionMiembroByPersonaAndReunionId != null ?
                organoReunionMiembroByPersonaAndReunionId.getId() :
                null;
    }

    public List<OrganoReunionMiembro> getMiembroReunionByOrganoAndReunionId(String organoId, Boolean externo, Long reunionId) {
        return organoReunionMiembroDAO.getMiembroReunionByOrganoAndReunionId(organoId, externo, reunionId);
    }

    private void enviaMailSuplente(Long reunionId, Long suplenteId, String suplenteEmail,
                                   OrganoReunionMiembro miembroGuardado) throws Exception {
        if (miembroGuardado == null) return;

        if (suplenteId != null && !suplenteId.equals(miembroGuardado.getSuplenteId())) {
            avisosReunion.enviaAvisoAltaSuplente(reunionId, suplenteEmail, miembroGuardado.getNombre(),
                    miembroGuardado.getCargoNombre());

            if (miembroGuardado.getSuplenteId() != null) {
                avisosReunion.enviaAvisoBajaSuplente(reunionId, miembroGuardado.getSuplenteEmail(),
                        miembroGuardado.getNombre(), miembroGuardado.getCargoNombre());
            }
        }

        if (suplenteId == null && miembroGuardado.getSuplenteId() != null) {
            avisosReunion.enviaAvisoBajaSuplente(reunionId, miembroGuardado.getSuplenteEmail(),
                    miembroGuardado.getNombre(), miembroGuardado.getCargoNombre());
        }
    }

    private void enviaMailDelegadoVoto(Long reunionId, Long delegadoVotoId, String delegadoVotoEmail,
                                       OrganoReunionMiembro miembroGuardado) throws Exception {
        if (miembroGuardado == null) return;

        if (delegadoVotoId != null && !delegadoVotoId.equals(miembroGuardado.getDelegadoVotoId())) {
            avisosReunion.enviaAvisoDelegacionVoto(reunionId, delegadoVotoEmail, miembroGuardado.getNombre(),
                    miembroGuardado.getCargoNombre(), true);

            if (miembroGuardado.getDelegadoVotoId() != null) {
                avisosReunion.enviaAvisoDelegacionVoto(reunionId, miembroGuardado.getDelegadoVotoEmail(),
                        miembroGuardado.getNombre(), miembroGuardado.getCargoNombre(), false);
            }
        }

        if (delegadoVotoId == null && miembroGuardado.getDelegadoVotoId() != null) {
            avisosReunion.enviaAvisoDelegacionVoto(reunionId, miembroGuardado.getDelegadoVotoEmail(),
                    miembroGuardado.getNombre(), miembroGuardado.getCargoNombre(), false);
        }
    }

    public List<OrganoReunionMiembro> getMiembrosByReunionId(Long reunionId) {
        return organoReunionMiembroDAO.getMiembrosByReunionId(reunionId);
    }
}
