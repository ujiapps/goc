package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.InvitadoTemplate;
import es.uji.apps.goc.dto.PuntoOrdenDiaTemplate;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionTemplate;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.services.PersonaService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.apps.goc.templates.PDFTemplate;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.sso.AccessManager;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Path("actas")
public class ActaResource {
    @InjectParam
    private ReunionService reunionService;

    @InjectParam
    private LanguageConfig languageConfig;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private ReunionDAO reunionDAO;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @GET
    @Path("{reunionId}")
    @Produces("application/pdf")
    public Template reunion(@PathParam("reunionId") Long reunionId, @QueryParam("lang") String lang,
                            @Context HttpServletRequest request)
            throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
            PersonasExternasException, InvalidAccessException, ActaProvisionalDesactivadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        if (!reunionDAO.tieneAcceso(reunionId, connectedUserId)) {
            throw new InvalidAccessException("No se tiene acceso a esta reunión");
        }

        Persona convocante = personaService.getPersonaFromDirectoryByPersonaId(reunion.getCreadorId());

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, false,
                languageConfig.isMainLangauge(lang));

        reunionService.checkIfActaProvisionalActivadaByReunion(reunionTemplate, connectedUserId);

        String applang = languageConfig.getLangCode(lang);

        boolean asisteAlgunInvitado = false;

        for (InvitadoTemplate invitado : reunionTemplate.getInvitados()) {
            if (invitado.getAsistencia() == true) {
                asisteAlgunInvitado = true;
            }
        }

        Template template = new PDFTemplate("acta-" + applang, personalizationConfig.templatesPath);
        if (reunionTemplate.getPuntosOrdenDia().size() == 1) {
            template.put("puntoUnico", reunionTemplate.getPuntosOrdenDia().get(0));
            template.put("isPuntoUnico", true);
        }
        template.put("logo", personalizationConfig.logoDocumentos);
        template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
        template.put("reunion", reunionTemplate);
        numeracionMultinivel(reunionTemplate.getPuntosOrdenDia(), null);
        template.put("convocante", convocante);
        template.put("applang", applang);
        template.put("publicUrl", personalizationConfig.publicUrl);
        template.put("asisteAlgunInvitado", asisteAlgunInvitado);

        return template;
    }

    public void numeracionMultinivel(List<PuntoOrdenDiaTemplate> puntos, String nivelPadre) {
        int i = 1;
        for (PuntoOrdenDiaTemplate puntoTemplate : puntos) {
            String multiNivel = nivelPadre != null ? String.format("%s.%s", nivelPadre, i) : String.valueOf(i);
            puntoTemplate.setNumeracionMultinivel(multiNivel);
            List<PuntoOrdenDiaTemplate> puntosInferiores = puntoTemplate.getSubpuntos();
            if (puntosInferiores != null) {
                List<PuntoOrdenDiaTemplate> subpuntos = new ArrayList<PuntoOrdenDiaTemplate>(puntosInferiores);
                subpuntos.sort(Comparator.comparing(punto -> punto.getOrden()));
                numeracionMultinivel(subpuntos, multiNivel);
            }
            i++;

        }
    }

}
