package es.uji.apps.goc.services.rest;

import com.google.common.base.Strings;
import com.mysema.query.Tuple;
import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.ContentDisposition;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.PuntosOrdenDiaTiposEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.DocumentoUI;
import es.uji.apps.goc.model.PuntosOrdenDiaAdjuntos;
import es.uji.apps.goc.services.PersonaService;
import es.uji.apps.goc.services.PuntoOrdenDiaDocumentoService;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Path("/reuniones/{reunionId}/puntosOrdenDia")
public class ReunionPuntosOrdenDiaResource extends CoreBaseService {
    @PathParam("reunionId")
    Long reunionId;
    @InjectParam
    private ReunionService reunionService;
    @InjectParam
    private PuntoOrdenDiaService puntoOrdenDiaService;
    @InjectParam
    private PuntoOrdenDiaDocumentoService puntoOrdenDiaDocumentoService;
    @InjectParam
    private PersonalizationConfig personalizationConfig;
    @InjectParam
    private PersonaService personaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getReunionPuntosOrdenDia() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<PuntoOrdenDiaMultinivel> puntosMultinivelByReunionId =
                puntoOrdenDiaService.getPuntosMultinivelByReunionId(reunionId);

        return puntosMultinivelConNumeroDocumentosToUI(puntosMultinivelByReunionId, null);
    }


    private List<UIEntity> puntosMultinivelConNumeroDocumentosToUI(
            List<PuntoOrdenDiaMultinivel> puntosOrdenDia,
            String nivelPadre
    ) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<UIEntity> puntosOrdenDiaUI = new ArrayList<>();
        int i = 1;

        for (PuntoOrdenDiaMultinivel puntoOrdenDia : puntosOrdenDia) {
            UIEntity puntoOrdenDiaUI = UIEntity.toUI(puntoOrdenDia);
            puntoOrdenDiaUI.put("votableEnContra", puntoOrdenDia.getVotableEnContra());
            puntoOrdenDiaUI.put("numeroDocumentos", puntoOrdenDiaDocumentoService.getNumeroDocumentosPorPuntoOrdenDia(puntoOrdenDia.getId(), connectedUserId));
            puntoOrdenDiaUI.put("numeroAcuerdos", puntoOrdenDiaDocumentoService.getNumeroAcuerdosPorPuntoOrdenDia(puntoOrdenDia.getId(), connectedUserId));
            String multinivel = nivelPadre != null ? String.format("%s.%s", nivelPadre, i) : String.valueOf(i);
            numeracionMultinivel(puntosOrdenDia, nivelPadre);
            puntoOrdenDiaUI.put("numeracionMultinivel", multinivel);
            puntoOrdenDiaUI.put("numeracion", i);

            puntosOrdenDiaUI.add(puntoOrdenDiaUI);
            Set<PuntoOrdenDiaMultinivel> puntosInferiores = puntoOrdenDia.getPuntosInferiores();
            if (puntosInferiores != null) {
                List<PuntoOrdenDiaMultinivel> subpuntos = new ArrayList<>(puntosInferiores);
                subpuntos.sort(Comparator.comparing(punto -> punto.getOrden()));
                List<UIEntity> subpuntosUI =
                        puntosMultinivelConNumeroDocumentosToUI(subpuntos, multinivel);
                puntoOrdenDiaUI.put("data", subpuntosUI);
                puntoOrdenDiaUI.put("expanded", true);
            } else {
                puntoOrdenDiaUI.put("leaf", true);
            }
            i++;
        }
        return puntosOrdenDiaUI;
    }

    public void numeracionMultinivel(List<PuntoOrdenDiaMultinivel> puntos, String nivelPadre) {
        int i = 1;
        for (PuntoOrdenDiaMultinivel puntoMultinivel : puntos) {
            String multiNivel = nivelPadre != null ? String.format("%s.%s", nivelPadre, i) : String.valueOf(i);
            puntoMultinivel.setNumeracionMultinivel(multiNivel);
            List<PuntoOrdenDiaMultinivel> puntosInferiores = puntoMultinivel.getPuntosInferioresAsOrderedList();
            if (puntosInferiores != null) {
                List<PuntoOrdenDiaMultinivel> subpuntos = new ArrayList<PuntoOrdenDiaMultinivel>(puntosInferiores);
                subpuntos.sort(Comparator.comparing(punto -> punto.getOrden()));
                numeracionMultinivel(subpuntos, multiNivel);
            }
            i++;

        }
    }

    private List<UIEntity> puntosOrdenDiaConNumeroDocumentosToUI(List<PuntoOrdenDia> listaPuntosOrdenDia,
                                                                 List<Tuple> listaNumeroDocumentosPorPuntoOrdenDiaId, List<Tuple> listaNumeroAcuerdosPorPuntoOrdenDiaId) {
        List<UIEntity> puntosOrdenDiaUI = new ArrayList<>();

        for (PuntoOrdenDia puntoOrdenDia : listaPuntosOrdenDia) {
            UIEntity puntoOrdenDiaUI = UIEntity.toUI(puntoOrdenDia);
            puntoOrdenDiaUI.put("numeroDocumentos", getNumeroDocumentosByPuntoOrdenDiaId(puntoOrdenDia.getId(),
                    listaNumeroDocumentosPorPuntoOrdenDiaId));
            puntoOrdenDiaUI.put("numeroAcuerdos", getNumeroAcuerdosByPuntoOrdenDiaId(puntoOrdenDia.getId(),
                    listaNumeroAcuerdosPorPuntoOrdenDiaId));
            puntosOrdenDiaUI.add(puntoOrdenDiaUI);
        }

        return puntosOrdenDiaUI;
    }

    private Long getNumeroDocumentosByPuntoOrdenDiaId(Long puntoOrdenDiaId,
                                                      List<Tuple> listaNumeroDocumentosPorPuntoOrdenDiaId) {
        Long num = 0L;

        for (Tuple tupla : listaNumeroDocumentosPorPuntoOrdenDiaId) {
            Long id = tupla.get(QPuntoOrdenDiaDocumento.puntoOrdenDiaDocumento.puntoOrdenDia.id);

            if (id.equals(puntoOrdenDiaId)) {
                return tupla.get(QPuntoOrdenDiaDocumento.puntoOrdenDiaDocumento.puntoOrdenDia.id.count());
            }
        }

        return num;
    }

    private Long getNumeroAcuerdosByPuntoOrdenDiaId(Long puntoOrdenDiaId,
                                                    List<Tuple> listaNumeroAcuerdosPorPuntoOrdenDiaId) {
        Long num = 0L;

        for (Tuple tupla : listaNumeroAcuerdosPorPuntoOrdenDiaId) {
            Long id = tupla.get(QPuntoOrdenDiaAcuerdo.puntoOrdenDiaAcuerdo.puntoOrdenDia.id);

            if (id.equals(puntoOrdenDiaId)) {
                return tupla.get(QPuntoOrdenDiaAcuerdo.puntoOrdenDiaAcuerdo.puntoOrdenDia.id.count());
            }
        }

        return num;
    }

    @DELETE
    @Path("{puntoOrdenDiaId}")
    public Response borrarPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId)
            throws ReunionYaCompletadaException, PuntoDelDiaConAcuerdosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.compruebaReunionNoCompletada(reunionId);
        puntoOrdenDiaService.borrarPuntoOrdenDia(reunionId, puntoOrdenDiaId, connectedUserId);

        return Response.ok().build();
    }

    @PUT
    @Path("{puntoOrdenDiaId}/subir")
    public Response subePuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId)
            throws ReunionYaCompletadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.compruebaReunionNoCompletada(reunionId);
        puntoOrdenDiaService.subePuntoOrdenDia(reunionId, puntoOrdenDiaId, connectedUserId);

        return Response.ok().build();
    }

    @PUT
    @Path("{puntoOrdenDiaId}/bajar")
    public Response bajaPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId)
            throws ReunionYaCompletadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.compruebaReunionNoCompletada(reunionId);
        puntoOrdenDiaService.bajaPuntoOrdenDia(reunionId, puntoOrdenDiaId, connectedUserId);

        return Response.ok().build();
    }

    @PUT
    @Path("{puntoOrdenDiaId}/mover")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response moverPuntos(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaAMoverId, UIEntity detallePuntosAMover)
            throws ReunionYaCompletadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.compruebaReunionNoCompletada(reunionId);
        Long puntoOrdenDiaDestinoId = ParamUtils.parseLong(detallePuntosAMover.get("puntoOrdenDiaDestinoId"));
        Long idHermanoAnterior = ParamUtils.parseLong(detallePuntosAMover.get("idHermanoAnterior"));
        Boolean crearNivel = Boolean.valueOf(detallePuntosAMover.get("crearNivel"));

        puntoOrdenDiaService.moverPuntos(reunionId, puntoOrdenDiaDestinoId, puntoOrdenDiaAMoverId, idHermanoAnterior, crearNivel, connectedUserId);

        return Response.ok().build();
    }

    @PUT
    @Path("{puntoOrdenDiaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity modificaPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId, UIEntity puntoOrdenDiaUI)
            throws PuntoOrdenDiaNoDisponibleException, ReunionYaCompletadaException, TipoRecuentoVotoNoExisteException,
            RecuentoVotoInvalidoParaPuntoNoVotableException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            PuntoOrdenDiaConVotosEmitidosException, NotificacionesException, MiembrosExternosException,
            ReunionNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String titulo = puntoOrdenDiaUI.get("titulo");
        String tituloAlternativo = puntoOrdenDiaUI.get("tituloAlternativo");
        String descripcion = puntoOrdenDiaUI.get("descripcion");
        String descripcionAlternativa = puntoOrdenDiaUI.get("descripcionAlternativa");
        String deliberaciones = puntoOrdenDiaUI.get("deliberaciones");
        String deliberacionesAlternativas = puntoOrdenDiaUI.get("deliberacionesAlternativas");
        String acuerdos = puntoOrdenDiaUI.get("acuerdos");
        String acuerdosAlternativos = puntoOrdenDiaUI.get("acuerdosAlternativos");
        Boolean publico = new Boolean(puntoOrdenDiaUI.get("publico"));
        puntoOrdenDiaDocumentoService.updatePrivacidadAcuerdos(puntoOrdenDiaId, publico);
        Long orden = ParamUtils.parseLong(puntoOrdenDiaUI.get("orden"));
        Boolean puntoEditado = reunionService.isReabierta(reunionId);
        Boolean votacionAbierta = Strings.isNullOrEmpty(puntoOrdenDiaUI.get("tipoVoto")) ? null : puntoOrdenDiaUI.getBoolean("tipoVoto");
        String tipoProcedimientoVotacion = puntoOrdenDiaUI.get("tipoProcedimientoVotacion");
        String tipoRecuentoVoto = puntoOrdenDiaUI.get("tipoRecuentoVoto");
        Boolean votableEnContra = puntoOrdenDiaUI.getBoolean("votableEnContra");
        String tipoVotacion = puntoOrdenDiaUI.get("tipoVotacion");

        reunionService.compruebaReunionNoCompletada(reunionId);

        PuntoOrdenDia puntoOrdenDia =
                puntoOrdenDiaService.updatePuntoOrdenDia(puntoOrdenDiaId, titulo, tituloAlternativo, descripcion,
                        descripcionAlternativa, deliberaciones, deliberacionesAlternativas, acuerdos,
                        acuerdosAlternativos, orden, publico, connectedUserId, puntoEditado, votacionAbierta,
                        tipoProcedimientoVotacion, tipoRecuentoVoto, votableEnContra, tipoVotacion);

        return UIEntity.toUI(puntoOrdenDia);
    }

    @PUT
    @Path("publico/{puntoOrdenDiaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity modificaVisibilidadPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId, UIEntity puntoOrdenDiaUI)
            throws PuntoOrdenDiaNoDisponibleException, RolesPersonaExternaException, PersonaNoAutorizadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Long reunionId = Long.parseLong(puntoOrdenDiaUI.get("reunionId"));

        if (personaService.isAdmin(connectedUserId) || personaService.isAutorizadoEnReunion(reunionId, connectedUserId)) {
            Boolean publico = new Boolean(puntoOrdenDiaUI.get("publico"));

            PuntoOrdenDia puntoOrdenDia =
                    puntoOrdenDiaService.updateVisibilidadPuntoOrdenDia(puntoOrdenDiaId, publico);
            puntoOrdenDiaDocumentoService.updatePrivacidadAcuerdos(puntoOrdenDiaId, publico);
            return UIEntity.toUI(puntoOrdenDia);
        }
        throw new PersonaNoAutorizadaException();
    }

    @PUT
    @Path("{puntoOrdenDiaId}/acuerdosydeliberaciones")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity modificaAcuerdosyDeliberaciones(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId, UIEntity puntoOrdenDiaUI)
            throws ReunionNoDisponibleException, PuntoOrdenDiaNoDisponibleException, ReunionYaCompletadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String deliberaciones = puntoOrdenDiaUI.get("deliberaciones");
        String deliberacionesAlternativas = puntoOrdenDiaUI.get("deliberacionesAlternativas");
        String acuerdos = puntoOrdenDiaUI.get("acuerdos");
        String acuerdosAlternativos = puntoOrdenDiaUI.get("acuerdosAlternativos");
        Boolean puntoEditado = reunionService.isReabierta(reunionId);

        reunionService.compruebaReunionNoCompletada(reunionId);

        PuntoOrdenDia puntoOrdenDia =
                puntoOrdenDiaService.updateAcuerdosYDeliberaciones(puntoOrdenDiaId, deliberaciones, deliberacionesAlternativas, acuerdos,
                        acuerdosAlternativos, connectedUserId, puntoEditado);

        return UIEntity.toUI(puntoOrdenDia);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addPuntoOrdenDia(UIEntity puntoOrdenDiaUI)
            throws ReunionYaCompletadaException, TipoRecuentoVotoNoExisteException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException, RecuentoVotoInvalidoParaPuntoNoVotableException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaUIToModel(puntoOrdenDiaUI);
        reunionService.compruebaReunionNoCompletada(puntoOrdenDia.getReunion().getId());

        puntoOrdenDia.setVotacionCerradaManualmente(false);

        Reunion reunion = reunionService.getReunionById(puntoOrdenDia.getReunion().getId());
        puntoOrdenDia = puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDia, reunion);

        return UIEntity.toUI(puntoOrdenDia);
    }

    private PuntoOrdenDia puntoOrdenDiaUIToModel(UIEntity puntoOrdenDiaUI) {
        PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDia();

        if (ParamUtils.parseLong(puntoOrdenDiaUI.get("id")) != null) {
            puntoOrdenDia.setId(new Long(puntoOrdenDiaUI.get("id")));
        }

        puntoOrdenDia.setTitulo((puntoOrdenDiaUI.get("titulo")));
        puntoOrdenDia.setTituloAlternativo((puntoOrdenDiaUI.get("tituloAlternativo")));
        puntoOrdenDia.setDescripcion(puntoOrdenDiaUI.get("descripcion"));
        puntoOrdenDia.setDescripcionAlternativa(puntoOrdenDiaUI.get("descripcionAlternativa"));
        puntoOrdenDia.setDeliberaciones(puntoOrdenDiaUI.get("deliberaciones"));
        puntoOrdenDia.setDeliberacionesAlternativas(puntoOrdenDiaUI.get("deliberacionesAlternativas"));
        puntoOrdenDia.setAcuerdos(puntoOrdenDiaUI.get("acuerdos"));
        puntoOrdenDia.setAcuerdosAlternativos(puntoOrdenDiaUI.get("acuerdosAlternativos"));
        puntoOrdenDia.setPublico(new Boolean(puntoOrdenDiaUI.get("publico")));
        puntoOrdenDia.setVotableEnContra(Boolean.valueOf(puntoOrdenDiaUI.get("votableEnContra")));
        if (reunionService.isReabierta(reunionId)) {
            puntoOrdenDia.setEditado(true);
        }

        PuntoOrdenDia puntoSuperior = (puntoOrdenDiaUI.get("puntoSuperior") != null && ParamUtils.parseLong(puntoOrdenDiaUI.get("puntoSuperior")) != null) ?
                new PuntoOrdenDia(new Long(puntoOrdenDiaUI.get("puntoSuperior"))) :
                null;
        puntoOrdenDia.setPuntoSuperior(puntoSuperior);

        puntoOrdenDia.setTipoProcedimientoVotacion(puntoOrdenDiaUI.get("tipoProcedimientoVotacion"));

        Reunion reunion = new Reunion(Long.parseLong(puntoOrdenDiaUI.get("reunionId")));
        puntoOrdenDia.setReunion(reunion);
        Boolean votacionAbierta = Strings.isNullOrEmpty(puntoOrdenDiaUI.get("tipoVoto")) ? null : puntoOrdenDiaUI.getBoolean("tipoVoto");
        puntoOrdenDia.setTipoVoto(votacionAbierta);
        puntoOrdenDia.setTipoRecuentoVoto(puntoOrdenDiaUI.get("tipoRecuentoVoto"));
        puntoOrdenDia.setTipoVotacion(puntoOrdenDiaUI.get("tipoVotacion"));

        return puntoOrdenDia;
    }

    @POST
    @Path("revisionactaanterior")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPuntoOrdenDiaUltimoActa()
            throws ReunionNoDisponibleException, RolesPersonaExternaException, TipoRecuentoVotoNoExisteException,
            RecuentoVotoInvalidoParaPuntoNoVotableException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Reunion reunionActual = reunionService.getReunionByIdAndEditorId(reunionId, connectedUserId);
        reunionService.addPuntoRevisionUltimoActa(reunionActual, connectedUserId);

        return Response.ok().build();
    }

    @GET
    @Path("{puntoOrdenDiaId}/documentacionyadjuntos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPuntoOrdenDiaDocumentacionYAdjuntos(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<PuntoOrdenDiaDocumento> documentos =
                puntoOrdenDiaDocumentoService.getDatosDocumentosByPuntoOrdenDiaId(puntoOrdenDiaId, connectedUserId);

        List<PuntoOrdenDiaAcuerdo> acuerdos =
                puntoOrdenDiaDocumentoService.getDatosAcuerdosByPuntoOrdenDiaId(puntoOrdenDiaId, connectedUserId);

        return puntoOrdenDiaDocumentosYAcuerdosToUI(documentos, acuerdos);
    }


    @PUT
    @Path("{puntoOrdenDiaId}/documentacionyadjuntos/{tipo}/subir/{documentoId}")
    public Response subirOrdenDocumento(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId, @PathParam("tipo") String tipo, @PathParam("documentoId") Long documentoId) {

        if (tipo.equals("ADJUNTO")) {
            puntoOrdenDiaDocumentoService.subeOrdenDocumento(puntoOrdenDiaId, documentoId);
        } else {
            puntoOrdenDiaDocumentoService.subeOrdenAcuerdo(puntoOrdenDiaId, documentoId);
        }


        return Response.ok().build();
    }

    @PUT
    @Path("{puntoOrdenDiaId}/documentacionyadjuntos/{tipo}/bajar/{documentoId}")
    public Response bajarOrdenDocumento(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId, @PathParam("tipo") String tipo, @PathParam("documentoId") Long documentoId) {

        if (tipo.equals("ADJUNTO")) {
            puntoOrdenDiaDocumentoService.bajaOrdenDocumento(puntoOrdenDiaId, documentoId);
        } else {
            puntoOrdenDiaDocumentoService.bajaOrdenAcuerdo(puntoOrdenDiaId, documentoId);
        }


        return Response.ok().build();
    }

    private List<UIEntity> puntoOrdenDiaDocumentosYAcuerdosToUI(List<PuntoOrdenDiaDocumento> documentos, List<PuntoOrdenDiaAcuerdo> acuerdos) {
        List<UIEntity> listaUI = documentos.stream().map(PuntosOrdenDiaAdjuntos::new).map(UIEntity::toUI).collect(Collectors.toList());
        listaUI.addAll(acuerdos.stream().map(PuntosOrdenDiaAdjuntos::new).map(UIEntity::toUI).collect(Collectors.toList()));

        return listaUI;
    }

    @GET
    @Path("{puntoOrdenDiaId}/documentos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPuntoOrdenDiaDocumentos(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<PuntoOrdenDiaDocumento> documentos =
                puntoOrdenDiaDocumentoService.getDatosDocumentosByPuntoOrdenDiaId(puntoOrdenDiaId, connectedUserId);

        return puntoOrdenDiaDocumentosToUI(documentos);
    }

    @GET
    @Path("{puntoOrdenDiaId}/acuerdos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPuntoOrdenDiaAcuerdos(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<PuntoOrdenDiaAcuerdo> acuerdos =
                puntoOrdenDiaDocumentoService.getDatosAcuerdosByPuntoOrdenDiaId(puntoOrdenDiaId, connectedUserId);

        return puntoOrdenDiaAcuerdosToUI(acuerdos);
    }

    private List<UIEntity> puntoOrdenDiaDocumentosToUI(List<PuntoOrdenDiaDocumento> documentos) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (PuntoOrdenDiaDocumento puntoOrdenDiaDocumento : documentos) {
            listaUI.add(puntoOrdenDiaDocumentoToUI(DocumentoUI.fromPuntoDiaDocumento(puntoOrdenDiaDocumento)));
        }

        return listaUI;
    }

    private List<UIEntity> puntoOrdenDiaAcuerdosToUI(List<PuntoOrdenDiaAcuerdo> acuerdos) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo : acuerdos) {
            listaUI.add(puntoOrdenDiaDocumentoToUI(DocumentoUI.fromPuntoDiaAcuerdos(puntoOrdenDiaAcuerdo)));
        }

        return listaUI;
    }

    private UIEntity puntoOrdenDiaDocumentoToUI(DocumentoUI documento) {
        UIEntity ui = new UIEntity();

        ui.put("id", documento.getId());
        ui.put("creadorId", documento.getCreadorId());
        ui.put("fechaAdicion", documento.getFechaAdicion());
        ui.put("descripcion", documento.getDescripcion());
        ui.put("descripcionAlternativa", documento.getDescripcionAlternativa());
        ui.put("mimeType", documento.getMimeType());
        ui.put("nombreFichero", documento.getNombreFichero());
        ui.put("publico", documento.getPublico());

        return ui;
    }

    @GET
    @Path("{puntoOrdenDiaId}/{tipo}/{documentoId}/descargar")
    public Response descargarPuntoOrdenDiaAdjunto(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                  @PathParam("documentoId") Long adjuntoId, @PathParam("tipo") PuntosOrdenDiaTiposEnum tipo)
            throws DocumentoNoEncontradoException, IOException {
        Documentable documentable = null;
        if (tipo.equals(PuntosOrdenDiaTiposEnum.ADJUNTO)) {
            documentable = puntoOrdenDiaDocumentoService.getDocumentoById(adjuntoId);
        } else if (tipo.equals(PuntosOrdenDiaTiposEnum.ACUERDO)) {
            documentable = puntoOrdenDiaDocumentoService.getAcuerdoById(adjuntoId);
        }

        if (documentable == null) {
            throw new DocumentoNoEncontradoException();
        }

        DocumentoUI documento = new DocumentoUI();
        documento.setData(documentable.getDatos());
        documento.setNombreFichero(documentable.getNombreFichero());
        documento.setMimeType(documentable.getMimeType());
        return sendDocumento(documento);
    }

    @GET
    @Path("{puntoOrdenDiaId}/documentos/{documentoId}/descargar")
    public Response descargarPuntoOrdenDiaDocumento(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                    @PathParam("documentoId") Long documentoId)
            throws DocumentoNoEncontradoException, IOException {
        return descargarPuntoOrdenDiaAdjunto(puntoOrdenDiaId, documentoId, PuntosOrdenDiaTiposEnum.ADJUNTO);
    }

    @GET
    @Path("{puntoOrdenDiaId}/acuerdos/{acuerdoId}/descargar")
    public Response descargarPuntoOrdenDiaAcuerdo(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                  @PathParam("acuerdoId") Long acuerdoId)
            throws DocumentoNoEncontradoException, IOException {
        return descargarPuntoOrdenDiaAdjunto(puntoOrdenDiaId, acuerdoId, PuntosOrdenDiaTiposEnum.ACUERDO);
    }

    private Response sendDocumento(DocumentoUI documento)
            throws IOException {
        String nombreFichero = documento.getNombreFichero();
        String contentType = documento.getMimeType();
        byte[] data = documento.getData();

        return Response.ok(data)
                .header("Content-Disposition", "attachment; filename = \"" + nombreFichero + "\"")
                .header("Content-Length", data.length)
                .header("Content-Type", contentType)
                .build();
    }

    @DELETE
    @Path("{puntoOrdenDiaId}/{tipo}/{adjuntoId}")
    public Response borrarAdjuntoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                               @PathParam("adjuntoId") Long adjuntoId, @PathParam("tipo") PuntosOrdenDiaTiposEnum tipo)
            throws ReunionYaCompletadaException, PuntoOrdenDiaNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.compruebaReunionNoCompletada(reunionId);
        if (tipo.equals(PuntosOrdenDiaTiposEnum.ADJUNTO)) {
            puntoOrdenDiaDocumentoService.borrarDocumento(adjuntoId);
        }
        if (tipo.equals(PuntosOrdenDiaTiposEnum.ACUERDO)) {
            puntoOrdenDiaDocumentoService.borrarAcuerdo(adjuntoId);
        }

        if (reunionService.isReabierta(reunionId)) {
            puntoOrdenDiaService.actualizaPuntoEditado(puntoOrdenDiaId);
        }

        return Response.ok().build();
    }

    @DELETE
    @Path("{puntoOrdenDiaId}/{tipo}/{adjuntoId}/inhabilitar")
    public Response inhabilitarAdjuntoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                    @PathParam("adjuntoId") Long adjuntoId, @PathParam("tipo") PuntosOrdenDiaTiposEnum tipo)
            throws ReunionYaCompletadaException, PuntoOrdenDiaNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.compruebaReunionNoCompletada(reunionId);
        if (tipo.equals(PuntosOrdenDiaTiposEnum.ADJUNTO)) {
            puntoOrdenDiaDocumentoService.inhabilitarDocumento(adjuntoId, connectedUserId);
        }
        if (tipo.equals(PuntosOrdenDiaTiposEnum.ACUERDO)) {
            puntoOrdenDiaDocumentoService.inhabilitarAcuerdo(adjuntoId, connectedUserId);
        }

        if (reunionService.isReabierta(reunionId)) {
            puntoOrdenDiaService.actualizaPuntoEditado(puntoOrdenDiaId);
        }

        return Response.ok().build();
    }

    @PUT
    @Path("{puntoOrdenDiaId}/{tipo}/{adjuntoId}/motivoEdicion")
    public Response cambiarMotivoEdicionAdjuntoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                             @PathParam("adjuntoId") Long adjuntoId, @PathParam("tipo") PuntosOrdenDiaTiposEnum tipo,
                                                             UIEntity motivoEdicion)
            throws ReunionYaCompletadaException, PuntoOrdenDiaNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.compruebaReunionNoCompletada(reunionId);
        if (tipo.equals(PuntosOrdenDiaTiposEnum.ADJUNTO)) {
            puntoOrdenDiaDocumentoService.cambiarMotivoEdicionDocumento(adjuntoId, connectedUserId, motivoEdicion.get("motivoEdicion"));
        }
        if (tipo.equals(PuntosOrdenDiaTiposEnum.ACUERDO)) {
            puntoOrdenDiaDocumentoService.cambiarMotivoEdicionAcuerdo(adjuntoId, connectedUserId, motivoEdicion.get("motivoEdicion"));
        }

        if (reunionService.isReabierta(reunionId)) {
            puntoOrdenDiaService.actualizaPuntoEditado(puntoOrdenDiaId);
        }

        return Response.ok().build();
    }

    @POST
    @Path("{puntoOrdenDiaId}/{tipo}/{adjuntoId}/guardarModificacionDocumento")
    public Response guardarModificacionDocumento(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                 @PathParam("adjuntoId") Long adjuntoId, @PathParam("tipo") PuntosOrdenDiaTiposEnum tipo,
                                                 UIEntity motivoEdicion) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        puntoOrdenDiaDocumentoService.guardarModificacionDocumento(adjuntoId, connectedUserId, motivoEdicion.get("motivoEdicion"));
        return Response.ok().build();
    }

    @DELETE
    @Path("{puntoOrdenDiaId}/documentos/{documentoId}")
    public Response borrarDocumentoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                 @PathParam("documentoId") Long documentoId)
            throws ReunionYaCompletadaException, PuntoOrdenDiaNoDisponibleException {
        return borrarAdjuntoPuntoOrdenDia(puntoOrdenDiaId, documentoId, PuntosOrdenDiaTiposEnum.ADJUNTO);
    }

    @DELETE
    @Path("{puntoOrdenDiaId}/acuerdos/{acuerdoId}")
    public Response borrarAcuerdoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                               @PathParam("acuerdoId") Long acuerdoId)
            throws ReunionYaCompletadaException, PuntoOrdenDiaNoDisponibleException {
        return borrarAdjuntoPuntoOrdenDia(puntoOrdenDiaId, acuerdoId, PuntosOrdenDiaTiposEnum.ACUERDO);
    }

    @POST
    @Path("{puntoOrdenDiaId}/{tipo}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public UIEntity subirAdjuntoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId, FormDataMultiPart multiPart,
                                              @PathParam("tipo") PuntosOrdenDiaTiposEnum tipo,
                                              @QueryParam("subirDesdeFicha") boolean subirDesdeFicha)
            throws IOException, ReunionYaCompletadaException, EntidadNoValidaException, NoSuchAlgorithmException,
            PuntoOrdenDiaNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        DocumentoUI documento = extractDocumentoFromMultipart(multiPart);

        reunionService.compruebaReunionNoCompletada(reunionId);
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.getPuntoById(puntoOrdenDiaId);

        Documentable puntoOrdenDiaAdjunto = null;
        if (tipo.equals(PuntosOrdenDiaTiposEnum.ADJUNTO)) {
            puntoOrdenDiaAdjunto = puntoOrdenDiaDocumentoService.addDocumento(puntoOrdenDiaId, documento, connectedUserId, subirDesdeFicha);
        } else if (tipo.equals(PuntosOrdenDiaTiposEnum.ACUERDO)) {
            puntoOrdenDiaAdjunto = puntoOrdenDiaDocumentoService.addAcuerdo(puntoOrdenDia, documento, connectedUserId);
        }

        if (reunionService.isReabierta(reunionId)) {
            puntoOrdenDiaService.actualizaPuntoEditado(puntoOrdenDiaId);
        }

        return UIEntity.toUI(puntoOrdenDiaAdjunto);
    }

    @POST
    @Path("{puntoOrdenDiaId}/documentos")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public UIEntity subirDocumentoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                FormDataMultiPart multiPart)
            throws IOException, ReunionYaCompletadaException, EntidadNoValidaException, NoSuchAlgorithmException,
            PuntoOrdenDiaNoDisponibleException {
        return subirAdjuntoPuntoOrdenDia(puntoOrdenDiaId, multiPart, PuntosOrdenDiaTiposEnum.ADJUNTO, true);
    }

    @PUT
    @Path("{puntoOrdenDiaId}/{tipo}/{adjuntoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateAdjuntoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                               @PathParam("adjuntoId") Long adjuntoId, PuntosOrdenDiaAdjuntos puntoOrdenDiaAdjuntoUI,
                                               @PathParam("tipo") PuntosOrdenDiaTiposEnum tipo)
            throws ReunionYaCompletadaException, IOException, NoSuchAlgorithmException,
            PuntoOrdenDiaNoDisponibleException {
        reunionService.compruebaReunionNoCompletada(reunionId);
        PuntosOrdenDiaAdjuntos puntoOrdenDiaAdjunto = null;

        if (tipo.equals(PuntosOrdenDiaTiposEnum.ADJUNTO)) {
            PuntoOrdenDiaDocumento documento = puntoOrdenDiaDocumentoService.getDocumentoById(adjuntoId);

            documento.setDescripcion(puntoOrdenDiaAdjuntoUI.getDescripcion());
            documento.setDescripcionAlternativa(puntoOrdenDiaAdjuntoUI.getDescripcionAlternativa());
            documento.setPublico(puntoOrdenDiaAdjuntoUI.getPublico());
            documento.setMostrarEnFicha(puntoOrdenDiaAdjuntoUI.getMostrarEnFicha());

            puntoOrdenDiaAdjunto = new PuntosOrdenDiaAdjuntos(puntoOrdenDiaDocumentoService.updateDocumento(documento, null));
        } else if (tipo.equals(PuntosOrdenDiaTiposEnum.ACUERDO)) {
            PuntoOrdenDiaAcuerdo acuerdo = puntoOrdenDiaDocumentoService.getAcuerdoById(adjuntoId);
            acuerdo.setDescripcion(puntoOrdenDiaAdjuntoUI.getDescripcion());
            acuerdo.setPublico(puntoOrdenDiaAdjuntoUI.getPublico());
            acuerdo.setDescripcionAlternativa(puntoOrdenDiaAdjuntoUI.getDescripcionAlternativa());

            puntoOrdenDiaAdjunto = new PuntosOrdenDiaAdjuntos(puntoOrdenDiaDocumentoService.updateAcuerdo(acuerdo, null));
        }

        if (reunionService.isReabierta(reunionId)) {
            puntoOrdenDiaService.actualizaPuntoEditado(puntoOrdenDiaId);
        }

        return UIEntity.toUI(puntoOrdenDiaAdjunto);
    }

    @POST
    @Path("{puntoOrdenDiaId}/{tipo}/{adjuntoId}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateAdjuntoFicheroPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                                      @PathParam("adjuntoId") Long adjuntoId,
                                                      @PathParam("tipo") PuntosOrdenDiaTiposEnum tipo,
                                                      FormDataMultiPart multiPart)
            throws ReunionYaCompletadaException, IOException, NoSuchAlgorithmException,
            PuntoOrdenDiaNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.compruebaReunionNoCompletada(reunionId);
        DocumentoUI adjuntoUI = extractDocumentoFromMultipart(multiPart);

        PuntosOrdenDiaAdjuntos puntoOrdenDiaAdjunto = null;

        if (tipo.equals(PuntosOrdenDiaTiposEnum.ADJUNTO)) {
            PuntoOrdenDiaDocumento documento = puntoOrdenDiaDocumentoService.getDocumentoById(adjuntoId);
            documento.setEditorId(connectedUserId);
            documento.setFechaEdicion(new Date());
            puntoOrdenDiaAdjunto = new PuntosOrdenDiaAdjuntos(puntoOrdenDiaDocumentoService.updateDocumento(documento, adjuntoUI));
        } else if (tipo.equals(PuntosOrdenDiaTiposEnum.ACUERDO)) {
            PuntoOrdenDiaAcuerdo acuerdo = puntoOrdenDiaDocumentoService.getAcuerdoById(adjuntoId);
            acuerdo.setEditorId(connectedUserId);
            acuerdo.setFechaEdicion(new Date());
            puntoOrdenDiaAdjunto = new PuntosOrdenDiaAdjuntos(puntoOrdenDiaDocumentoService.updateAcuerdo(acuerdo, adjuntoUI));
        }

        if (reunionService.isReabierta(reunionId)) {
            puntoOrdenDiaService.actualizaPuntoEditado(puntoOrdenDiaId);
        }

        return UIEntity.toUI(puntoOrdenDiaAdjunto);
    }

    @POST
    @Path("{puntoOrdenDiaId}/acuerdos")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public UIEntity subirAcuerdoPuntoOrdenDia(@PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
                                              FormDataMultiPart multiPart)
            throws IOException, ReunionYaCompletadaException, NoSuchAlgorithmException,
            PuntoOrdenDiaNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        DocumentoUI documento = extractDocumentoFromMultipart(multiPart);

        reunionService.compruebaReunionNoCompletada(reunionId);
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.getPuntoById(puntoOrdenDiaId);

        PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo =
                puntoOrdenDiaDocumentoService.addAcuerdo(puntoOrdenDia, documento, connectedUserId);

        if (reunionService.isReabierta(reunionId)) {
            puntoOrdenDiaService.actualizaPuntoEditado(puntoOrdenDiaId);
        }

        return UIEntity.toUI(puntoOrdenDiaAcuerdo);
    }

    private DocumentoUI extractDocumentoFromMultipart(FormDataMultiPart multiPart)
            throws IOException {
        String fileName = "";
        String mimeType = "";
        InputStream data = null;
        String descripcion = "";
        String descripcionAlternativa = "";
        DocumentoUI documento = new DocumentoUI();

        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            String mime = bodyPart.getHeaders().getFirst("Content-Type");
            if (mime != null && !mime.isEmpty()) {
                mimeType = mime;
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches()) {
                    fileName = m.group(1);
                }
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                data = bpe.getInputStream();
            } else {
                ContentDisposition cd = bodyPart.getContentDisposition();
                Map<String, String> parameters = cd.getParameters();

                if (parameters.get("name").equals("descripcion")) {
                    descripcion = bodyPart.getEntityAs(String.class);
                }

                if (parameters.get("name").equals("descripcionAlternativa")) {
                    descripcionAlternativa = bodyPart.getEntityAs(String.class);
                }
            }
        }

        documento.setDescripcion(descripcion);
        documento.setDescripcionAlternativa(descripcionAlternativa);
        documento.setNombreFichero(fileName);
        documento.setMimeType(mimeType);
        documento.setData(StreamUtils.inputStreamToByteArray(data));

        return documento;
    }
}
