package es.uji.apps.goc.services;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.PuntoOrdenDiaRecuentoVotosDTO;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.notifications.AvisosReunion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Component
public class PuntoOrdenDiaService {
    @Autowired
    PersonalizationConfig personalizationConfig;
    @Autowired
    AvisosReunion avisosReunion;
    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;
    @Autowired
    private ReunionDAO reunionDAO;
    @Autowired
    private VotacionService votacionService;

    public void setAvisosReunion(AvisosReunion avisosReunion) {
        this.avisosReunion = avisosReunion;
    }

    public List<PuntoOrdenDia> getPuntosByReunionId(Long reunionId, Long connectedUserId) {
        return puntoOrdenDiaDAO.getPuntosByReunionId(reunionId);
    }

    public List<PuntoOrdenDiaMultinivel> getPuntosMultinivelByReunionId(Long reunionId) {
        List<PuntoOrdenDiaMultinivel> puntosMultinivelByReunionId =
                puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateado(reunionId);

        return puntosMultinivelByReunionId;
    }

    @Transactional(rollbackFor = PuntoDelDiaConAcuerdosException.class)
    public void borrarPuntoOrdenDia(Long reunionId, Long puntoOrdenDiaId, Long connectedUserId)
            throws PuntoDelDiaConAcuerdosException {
        puntoOrdenDiaDAO.deleteByPuntoId(puntoOrdenDiaId);
    }

    public PuntoOrdenDia updatePuntoOrdenDia(Long puntoOrdenDiaId, String titulo, String tituloAlternativo,
                                             String descripcion, String descripcionAlternativa, String deliberaciones, String deliberacionesAlternativas,
                                             String acuerdos, String acuerdosAlternativos, Long orden, Boolean publico, Long connectedUserId,
                                             Boolean editado, Boolean tipoVoto, String tipoProcedimientoVotacion, String tipoRecuentoVoto,
                                             Boolean votableEnContra, String tipoVotacion)
            throws PuntoOrdenDiaNoDisponibleException, TipoRecuentoVotoNoExisteException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            PuntoOrdenDiaConVotosEmitidosException, NotificacionesException, MiembrosExternosException,
            ReunionNoDisponibleException {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        if (puntoOrdenDia == null) {
            throw new PuntoOrdenDiaNoDisponibleException();
        }

        if (personalizationConfig.habilitarVotaciones) {
            Optional<PuntoOrdenDiaRecuentoVotosDTO> puntoOrdenDiaRecuentoVotos =
                    Optional.ofNullable(votacionService.getRecuentoVotosPunto(puntoOrdenDia.getReunion(), puntoOrdenDiaId));

            boolean tieneVotosEmitidos =
                    puntoOrdenDiaRecuentoVotos.isPresent() && puntoOrdenDiaRecuentoVotos.get().getRecuentoVotos() > 0;

            boolean procedimientoVotoHaCambiado =
                    puntoOrdenDia.getTipoProcedimientoVotacion() != null ? !puntoOrdenDia.getTipoProcedimientoVotacion()
                            .equals(tipoProcedimientoVotacion) : tipoProcedimientoVotacion != null;

            boolean tipoVotoHaCambiado = puntoOrdenDia.getTipoVoto() != null ? !puntoOrdenDia.getTipoVoto()
                    .equals(tipoVoto) : tipoVoto != null;

            boolean votableEnContraHaCambiado =
                    puntoOrdenDia.getVotableEnContra() != null ? !puntoOrdenDia.getVotableEnContra()
                            .equals(votableEnContra) : votableEnContra != null;

            boolean tipoRecuentoVotosHaCambiado = puntoOrdenDia.getTipoRecuentoVoto() != null ? !puntoOrdenDia.getTipoRecuentoVoto()
                    .equals(tipoRecuentoVoto) : tipoRecuentoVoto != null;

            boolean tipoVotacionHaCambiado = puntoOrdenDia.getTipoVotacion() != null ? !puntoOrdenDia.getTipoVotacion()
                    .equals(tipoVotacion) : tipoVotacion != null;

            if (tieneVotosEmitidos && (tipoVotoHaCambiado || votableEnContraHaCambiado || procedimientoVotoHaCambiado)) {
                throw new PuntoOrdenDiaConVotosEmitidosException();
            }

            if (procedimientoVotoHaCambiado && tipoProcedimientoVotacion != null) {
                setParametrosTipoProcedimientoVotacion(puntoOrdenDia, puntoOrdenDia.getReunion(),
                        tipoProcedimientoVotacion.equals(TipoProcedimientoVotacionEnum.ABREVIADO.toString()));
            }

            if (puntoOrdenDia.getReunion().getAvisoPrimeraReunion() && (tipoVotoHaCambiado || votableEnContraHaCambiado || procedimientoVotoHaCambiado || tipoRecuentoVotosHaCambiado || tipoVotacionHaCambiado)) {
                avisosReunion.enviarAvisoCambioCondicionesDeVotoEnPOD(puntoOrdenDia.getReunion(), puntoOrdenDia, procedimientoVotoHaCambiado, tipoVotoHaCambiado, votableEnContraHaCambiado, tipoRecuentoVotosHaCambiado, tipoVotacionHaCambiado);
            }
        }

        puntoOrdenDia.setTitulo(titulo);
        puntoOrdenDia.setTituloAlternativo(tituloAlternativo);
        puntoOrdenDia.setDescripcion(descripcion);
        puntoOrdenDia.setDescripcionAlternativa(descripcionAlternativa);
        puntoOrdenDia.setDeliberaciones(deliberaciones);
        puntoOrdenDia.setDeliberacionesAlternativas(deliberacionesAlternativas);
        puntoOrdenDia.setAcuerdos(acuerdos);
        puntoOrdenDia.setAcuerdosAlternativos(acuerdosAlternativos);
        puntoOrdenDia.setOrden(orden);
        puntoOrdenDia.setPublico(publico);
        puntoOrdenDia.setEditado(editado);
        puntoOrdenDia.setTipoVoto(tipoVoto);
        puntoOrdenDia.setVotableEnContra(votableEnContra);
        puntoOrdenDia.setTipoProcedimientoVotacion(tipoProcedimientoVotacion);
        puntoOrdenDia.setTipoRecuentoVoto(tipoRecuentoVoto);
        puntoOrdenDia.setTipoVotacion(tipoVotacion);

        if (personalizationConfig.habilitarVotaciones) {
            puntoOrdenDia.checkValidezTipoRecuento();
            puntoOrdenDia.checkValidezAmbitoVotacion();
        }

        return puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    @Transactional(rollbackFor = TipoRecuentoVotoNoExisteException.class)
    public PuntoOrdenDia addPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia, Reunion reunion)
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        return addPuntoOrdenDia(puntoOrdenDia, true, reunion);
    }

    @Transactional(rollbackFor = TipoRecuentoVotoNoExisteException.class)
    public PuntoOrdenDia addPuntoOrdenDiaSinCambiarOrden(PuntoOrdenDia puntoOrdenDia, Reunion reunion)
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        return addPuntoOrdenDia(puntoOrdenDia, false, reunion);
    }

    private PuntoOrdenDia addPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia, boolean setOrden, Reunion reunion)
            throws TipoRecuentoVotoNoExisteException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            RecuentoVotoInvalidoParaPuntoNoVotableException {
        if (personalizationConfig.habilitarVotaciones) {
            puntoOrdenDia.checkValidezTipoRecuento();
            puntoOrdenDia.checkValidezAmbitoVotacion();
        }

        if (setOrden) {
            PuntoOrdenDia ultimo =
                    puntoOrdenDiaDAO.getUltimoPuntoOrdenDiaByReunionId(puntoOrdenDia.getReunion().getId());

            if (ultimo != null) {
                puntoOrdenDia.setOrden(ultimo.getOrden() + 10L);
            } else {
                puntoOrdenDia.setOrden(10L);
            }
        }

        if (!setOrden) {
            puntoOrdenDia.setOrden(puntoOrdenDia.getOrden() + 10L);
        }

        if (personalizationConfig.habilitarVotaciones && reunion.getVotacionTelematica() != null && reunion.getVotacionTelematica()) {
            boolean isAbreviado =
                    puntoOrdenDia.getTipoProcedimientoVotacion().equals(TipoProcedimientoVotacionEnum.ABREVIADO.name());

            puntoOrdenDia.setFechaFinVotacion(reunion.getFechaFinVotacion());
            puntoOrdenDia.setVotacionCerradaManualmente(false);

            if (reunion.isAbierta() && isAbreviado) {
                puntoOrdenDia.setVotacionAbierta(true);
                puntoOrdenDia.setFechaInicioVotacion(reunion.getAvisoPrimeraReunionFecha());
            } else {
                puntoOrdenDia.setVotacionAbierta(false);
            }
            setParametrosTipoProcedimientoVotacion(puntoOrdenDia, reunion, isAbreviado);
        }

        return puntoOrdenDiaDAO.insert(puntoOrdenDia);
    }

    private void setParametrosTipoProcedimientoVotacion(PuntoOrdenDia puntoOrdenDia, Reunion reunion, boolean isAbreviado) {
        if (reunion.isAbierta() && isAbreviado) {
            puntoOrdenDia.setVotacionAbierta(true);

            puntoOrdenDia.setFechaInicioVotacion(reunion.getAvisoPrimeraReunionFecha());
        } else {
            puntoOrdenDia.setVotacionAbierta(false);
        }
    }

    @Transactional
    public void subePuntoOrdenDia(Long reunionId, Long puntoOrdenDiaId, Long connectedUserId) {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        PuntoOrdenDia anteriorPuntoOrdenDia =
                puntoOrdenDiaDAO.getAnteriorPuntoOrdenDiaByOrdenYNivel(reunionId, puntoOrdenDia.getOrden(),
                        puntoOrdenDia.getPuntoSuperior());

        if (anteriorPuntoOrdenDia != null) {
            Long orden = puntoOrdenDia.getOrden();
            puntoOrdenDiaDAO.actualizaOrden(puntoOrdenDiaId, anteriorPuntoOrdenDia.getOrden());
            puntoOrdenDiaDAO.flush();
            puntoOrdenDiaDAO.actualizaOrden(anteriorPuntoOrdenDia.getId(), orden);
            puntoOrdenDiaDAO.flush();

            return;
        }
    }

    @Transactional
    public void bajaPuntoOrdenDia(Long reunionId, Long puntoOrdenDiaId, Long connectedUserId) {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        PuntoOrdenDia siguientePuntoOrdenDia =
                puntoOrdenDiaDAO.getSiguientePuntoOrdenDiaByOrdenYNivel(reunionId, puntoOrdenDia.getOrden(),
                        puntoOrdenDia.getPuntoSuperior());

        if (siguientePuntoOrdenDia != null) {
            Long orden = puntoOrdenDia.getOrden();
            puntoOrdenDiaDAO.actualizaOrden(puntoOrdenDiaId, siguientePuntoOrdenDia.getOrden());
            puntoOrdenDiaDAO.actualizaOrden(siguientePuntoOrdenDia.getId(), orden);
        }
    }

    public void deleteByReunionId(Long reunionId)
            throws PuntoDelDiaConAcuerdosException {
        for (PuntoOrdenDia puntoOrdenDia : puntoOrdenDiaDAO.getPuntosByReunionId(reunionId)) {
            puntoOrdenDiaDAO.deleteByPuntoId(puntoOrdenDia.getId());
        }
    }

    @Transactional
    public void moverPuntos(Long reunionId, Long puntoOrdenDiaDestinoId, Long puntoOrdenDiaAMoverId,
                            Long idHermanoAnterior, Boolean crearNivel, Long connectedUserId) {
        Long incremento = 10L;
        PuntoOrdenDia puntoOrdenDiaAMover = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaAMoverId);
        PuntoOrdenDia puntoOrdenDiaDestinoPadre =
                (puntoOrdenDiaDestinoId == null) ? null : puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaDestinoId);
        PuntoOrdenDia puntoOrdenDiaPrevio =
                (idHermanoAnterior == null) ? null : puntoOrdenDiaDAO.getPuntoOrdenDiaById(idHermanoAnterior);

        if (crearNivel) {
            puntoOrdenDiaAMover.setPuntoSuperior(puntoOrdenDiaPrevio);
        } else {
            puntoOrdenDiaAMover.setPuntoSuperior(puntoOrdenDiaDestinoPadre);
        }
        puntoOrdenDiaAMover.setOrden(
                (puntoOrdenDiaPrevio == null) ? incremento : puntoOrdenDiaPrevio.getOrden() + incremento);
        puntoOrdenDiaDAO.update(puntoOrdenDiaAMover);

        puntoOrdenDiaDAO.incrementaOrdenPuntosSiguientesConMismoPadre(reunionId, puntoOrdenDiaAMoverId,
                puntoOrdenDiaAMover.getOrden());
    }

    public PuntoOrdenDia updateAcuerdosYDeliberaciones(Long puntoOrdenDiaId, String deliberaciones,
                                                       String deliberacionesAlternativas, String acuerdos, String acuerdosAlternativos, Long connectedUserId,
                                                       Boolean puntoeditado)
            throws PuntoOrdenDiaNoDisponibleException {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);

        if (puntoOrdenDia == null) {
            throw new PuntoOrdenDiaNoDisponibleException();
        }

        puntoOrdenDia.setDeliberaciones(deliberaciones);
        puntoOrdenDia.setDeliberacionesAlternativas(deliberacionesAlternativas);
        puntoOrdenDia.setAcuerdos(acuerdos);
        puntoOrdenDia.setAcuerdosAlternativos(acuerdosAlternativos);
        puntoOrdenDia.setEditado(puntoeditado);

        return puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    public PuntoOrdenDia getPuntoById(Long puntoOrdenDiaId) {
        List<PuntoOrdenDia> puntoOrdenDias = puntoOrdenDiaDAO.get(PuntoOrdenDia.class, puntoOrdenDiaId);
        if (puntoOrdenDias != null && !puntoOrdenDias.isEmpty()) {
            return puntoOrdenDias.get(0);
        }
        return null;
    }

    @Transactional
    public void addPuntoOrdenDiaUrlActa(Long puntoOrdenDiaId, String urlActa) {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        puntoOrdenDia.setUrlActa(urlActa);
        puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    @Transactional
    public void addPuntoOrdenDiaUrlActaAlternativa(Long puntoOrdenDiaId, String urlActaAlternativa) {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        puntoOrdenDia.setUrlActaAlternativa(urlActaAlternativa);
        puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    public PuntoOrdenDia updateVisibilidadPuntoOrdenDia(Long puntoOrdenDiaId, Boolean publico)
            throws PuntoOrdenDiaNoDisponibleException {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);

        if (puntoOrdenDia == null) {
            throw new PuntoOrdenDiaNoDisponibleException();
        }
        puntoOrdenDia.setPublico(publico);

        return puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    public void iniciaFechaVotacionPuntosAbreviados(Reunion reunion)
            throws NoVotacionTelematicaException {
        puntoOrdenDiaDAO.iniciaFechaVotacionPuntosAbreviados(reunion);
    }

    public void actualizadoFechaFinVotacionPuntos(Reunion reunion)
            throws NoVotacionTelematicaException {
        puntoOrdenDiaDAO.actualizadoFechaFinVotacionPuntos(reunion);
    }

    public void eliminaFechaFinVotacionPuntos(Reunion reunion)
            throws NoVotacionTelematicaException {
        puntoOrdenDiaDAO.eliminaFechaFinVotacionPuntos(reunion);
    }

    public void cierraFechaVotacionPuntos(Reunion reunion)
            throws ReunionNoAbiertaException {
        puntoOrdenDiaDAO.cierraFechaVotacionPuntos(reunion);
    }

    public void actualizaPuntoEditado(Long puntoOrdenDiaId)
            throws PuntoOrdenDiaNoDisponibleException {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);

        if (puntoOrdenDia == null) {
            throw new PuntoOrdenDiaNoDisponibleException();
        }
        puntoOrdenDia.setEditado(true);

        puntoOrdenDiaDAO.update(puntoOrdenDia);
    }
}
