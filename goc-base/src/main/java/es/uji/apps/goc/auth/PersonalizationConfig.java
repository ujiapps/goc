package es.uji.apps.goc.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PersonalizationConfig
{
    @Value("${goc.logo}")
    public String logo;

    @Value("${goc.logoPublic}")
    public String logoPublic;

    @Value("${goc.logoDocumentos}")
    public String logoDocumentos;

    @Value("${goc.nombreInstitucion}")
    public String nombreInstitucion;

    @Value("${goc.ciudadInstitucion}")
    public String ciudadInstitucion;

    @Value("${goc.charset}")
    public String charset;

    @Value("${goc.customCSS:}")
    public String customCSS;

    @Value("${goc.rolAdministrador:ADMIN}")
    public String rolAdministrador;

    @Value("${goc.rolUsuario:USUARIO}")
    public String rolUsuario;

    @Value("${goc.rolGestor:GESTOR}")
    public String rolGestor;

    @Value("${goc.staticsUrl://static.uji.es}")
    public String staticsUrl;

    @Value("${goc.tituloAutomatico:}")
    public String tituloAutomatico;

    @Value("${goc.tituloAutomaticoAlternativo:}")
    public String tituloAutomaticoAlternativo;

    @Value("${goc.templates.path:classpath:templates/}")
    public String templatesPath;

    @Value("${goc.publicUrl}")
    public String publicUrl;

    @Value("${goc.menu.oficios:false}")
    public boolean menuOficios;
    
    @Value("${goc.menu.descriptores:false}")
    public boolean menuDescriptores;

    @Value("${goc.reuniones.mostrarInformarOtrosOrganos:false}")
    public boolean mostrarInformarOtrosOrganos;

    @Value("${goc.debug:false}")
    public boolean debug;

    @Value("${goc.enlaceManual:#{null}}")
    public String enlaceManual;
    
    @Value("${goc.enlaceManualMiembros:#{null}}")
    public String enlaceManualMiembros;

    @Value("${goc.reunion.enlaceMasOpciones:#{null}}")
    public String enlaceMasOpciones;

    @Value("${goc.reunion.activarOpcionesPorDefecto:true}")
    public boolean activarOpcionesPorDefecto;

    @Value("${goc.reunion.admiteCambioVoto:true}")
    public boolean admiteCambioVoto;

    @Value("${goc.sesionTimeout:28800}")
    public int sesionTimeout;

    @Value("${goc.reunion.basePathDocumentacion:/etc/uji/goc/documentacion}")
    public String basePathDocumentacion;

    @Value("${goc.reunion.mostrarPublicarAcuerdos:false}")
    public boolean mostrarPublicarAcuerdos;

    @Value("${goc.tipoDeCifrado:SHA-256}")
    public String tipoDeCifrado;

    @Value("${goc.reunion.mostrarConvocante:true}")
    public boolean mostrarConvocante;

    @Value("${goc.votaciones.enabled:false}")
    public boolean habilitarVotaciones;

    @Value("${goc.miembros.miembrosNatos:true}")
    public boolean miembrosNatos;

    @Value("${goc.votacion.permiteAbstencionVoto:true}")
    public boolean permiteAbstencionVoto;

    @Value("${goc.votacion.miembrosNatoVotoObligado:false}")
    public boolean miembrosNatoVotoObligado;

    @Value("${goc.votacion.presidenteVotoDoble:false}")
    public boolean presidenteVotoDoble;

    @Value("${goc.smtp.enabled:true}")
    public boolean smtpEnabled;
    
    @Value("${goc.reunion.mostrarDocumentoEnFicha:false}")
    public boolean mostrarDocumentoEnFicha;
    
    @Value("${goc.reunion.verDeliberaciones:true}")
    public boolean verDeliberaciones;
    
    @Value("${goc.reunion.verAcuerdos:true}")
    public boolean verAcuerdos;

}
