package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.ReunionAVotar;
import es.uji.apps.goc.model.punto.PuntoVotable;
import es.uji.apps.goc.model.punto.PuntoVotableActualizar;
import es.uji.apps.goc.services.PersonaService;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.apps.goc.services.VotacionService;
import es.uji.apps.goc.templates.HTMLTemplate;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.jsoup.helper.Validate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Service
@Path("votos")
public class VotacionResource extends CoreBaseService {
    @InjectParam
    private VotacionService votacionService;

    @InjectParam
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private ReunionService reunionService;

    @InjectParam
    private ReunionDAO reunionDAO;

    @InjectParam
    private LanguageConfig languageConfig;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @Value("${goc.templates.path:classpath:templates/}")
    private String templatesPath;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void votarPunto(
            @QueryParam("puntoId") Long puntoId,
            @QueryParam("voto") String voto,
            @QueryParam("representadoId") Long representadoReunionMiembroId,
            @QueryParam("miembroVotoPresencialId") Long miembroVotoPresencialId
    ) throws TipoVotoNoExisteException, NoVotacionTelematicaException, NoEsTelematicaExceptionConVotacion,
            PersonasExternasException, PuntoSecretoNoAdmiteCambioVotoException, ReunionNoAbiertaException,
            UsuarioYaHaVotadoSoloSePermiteActualizacionException, UsuarioNoTienePermisoParaVotarException,
            PuntoNoVotableException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotoNoDelegadoException,
            VotacionNoAbiertaException, PuntoNoVotableEnContraException, VotoException {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.getPuntoById(puntoId);
        ReunionAVotar reunionAVotar = new ReunionAVotar(puntoOrdenDia.getReunion());
        PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDia, reunionAVotar, voto);

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaFromDirectoryByPersonaId(connectedUserId);

        if (representadoReunionMiembroId != null) {
            votacionService.votarPuntoDelegado(persona, representadoReunionMiembroId, puntoAVotar);
        } else {
            if (miembroVotoPresencialId != null) { // Voto presencial
                votacionService.votarPuntoPresencial(miembroVotoPresencialId, puntoAVotar);
            } else { // Voto no presencial
                votacionService.votarPuntoTitular(persona, puntoAVotar);
            }
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void actualizarVoto(
            @QueryParam("puntoId") Long puntoId,
            @QueryParam("voto") String voto,
            @QueryParam("representadoId") Long representadoReunionMiembroId,
            @QueryParam("miembroVotoPresencialId") Long miembroVotoPresencialId
    ) throws TipoVotoNoExisteException, ReunionNoAdmiteCambioVotoException, NoVotacionTelematicaException,
            NoEsTelematicaExceptionConVotacion, PersonasExternasException, PuntoSecretoNoAdmiteCambioVotoException,
            NoSePuedeActualizarVotoInexistenteException, ReunionNoAbiertaException, PuntoNoVotableException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotacionNoAbiertaException,
            PuntoNoVotableEnContraException, VotoException {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.getPuntoById(puntoId);
        ReunionAVotar reunionAVotar = new ReunionAVotar(puntoOrdenDia.getReunion());
        PuntoVotableActualizar puntoAActualizar = new PuntoVotableActualizar(puntoOrdenDia, reunionAVotar, voto);

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaFromDirectoryByPersonaId(connectedUserId);

        if (representadoReunionMiembroId != null) {
            votacionService.actualizarVotoDelegado(persona, representadoReunionMiembroId, puntoAActualizar);
        } else {
            if (miembroVotoPresencialId != null) { // Voto presencial
                votacionService.actualizarVotoPresencial(miembroVotoPresencialId, puntoAActualizar);
            } else {
                votacionService.actualizarVotoTitular(persona, puntoAActualizar);
            }
        }

    }


    @GET
    @Path("resultado/votacion/reunion/{reunionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getNombreTitularVotoByReunion(@PathParam("reunionId") Long reunionId) throws TipoRecuentoVotoNoExisteException {
        Reunion reunion = reunionService.getReunionById(reunionId);
        List<PuntoOrdenDiaRecuentoVotosDTO> puntoOrdenDiaRecuentoVotosDTOS = votacionService.getRecuentoVotosByReunionOrdered(reunion);

        List<UIEntity> bounchUIEntities = new ArrayList<UIEntity>();


        for (int index = 0; index < puntoOrdenDiaRecuentoVotosDTOS.size(); index++) {
            List<VotanteVoto> votos = puntoOrdenDiaRecuentoVotosDTOS.get(index).getVotantesNombre();

            if (votos.size() != 0) {

                for (int i = 0; i < votos.size(); i++) {

                    String nombreDelegadoVotoActual = votos.get(i).getNombreDelegado();
                    if (nombreDelegadoVotoActual != null) {
                        UIEntity ui = new UIEntity();
                        ui.put("nombreTitular", votos.get(i).getNombreTitular());
                        bounchUIEntities.add(ui);
                    }
                }
            }

        }

        return bounchUIEntities;
    }

    @GET
    @Path("resultado/votacion/{reunionId}")
    @Produces(MediaType.TEXT_HTML)
    public Template resultadoVotacion(
            @PathParam("reunionId") Long reunionId,
            @QueryParam("lang") String lang
    )
            throws PuntoDelDiaNoDisponibleException, ReunionNoDisponibleException, InvalidAccessException, TipoRecuentoVotoNoExisteException {
        Validate.notNull(reunionId);
        Validate.isTrue(reunionId > 0);

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Reunion reunion = reunionService.getReunionById(reunionId);

        if (reunion == null)
            throw new ReunionNoDisponibleException();

        if (!reunionDAO.tieneAcceso(reunionId, connectedUserId))
            throw new InvalidAccessException("No se tiene acceso a esta reunión");

        List<PuntoOrdenDiaRecuentoVotosDTO> puntoOrdenDiaRecuentoVotosDTOS =
                votacionService.getRecuentoVotosByReunionOrdered(reunion);

        if (puntoOrdenDiaRecuentoVotosDTOS == null)
            throw new PuntoDelDiaNoDisponibleException();

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(
                reunion,
                connectedUserId,
                true,
                languageConfig.isMainLangauge(lang)
        );

        List<PuntoOrdenDiaRecuentoVotosTemplate> recuentoVotosTemplates = new ArrayList<>();
        for (PuntoOrdenDiaRecuentoVotosDTO puntoOrdenDiaRecuentoVotosDTO : puntoOrdenDiaRecuentoVotosDTOS) {
            PuntoOrdenDiaRecuentoVotosDTO puntoRecuentoVotosDTO =
                    new PuntoOrdenDiaRecuentoVotosDTO(puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia(), 0L, 0L, 0L, 0L, false, new ArrayList<VotanteVoto>());

            PuntoSecretoVotosDTO puntoSecretoVotosDTO = votacionService.getPuntoSecretoVotosByPuntoId(puntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getId());

            if (puntoSecretoVotosDTO != null) {
                puntoRecuentoVotosDTO =
                        votacionService.mapperPuntoSecretoVotosDTOToPuntoOrdenDiaRecuentoVotosDTO(puntoSecretoVotosDTO);
            } else {
                puntoRecuentoVotosDTO = puntoOrdenDiaRecuentoVotosDTO;
            }

            addResultadoVotacionSubpuntos(puntoOrdenDiaRecuentoVotosDTO, puntoRecuentoVotosDTO);

            recuentoVotosTemplates.add(new PuntoOrdenDiaRecuentoVotosTemplate(puntoRecuentoVotosDTO));
        }

        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("votacion-resultado-" + applang, templatesPath);
        template.put("reunion", reunionTemplate);
        template.put("applang", applang);
        template.put("logo", personalizationConfig.logo);
        template.put("logoPublic", (personalizationConfig.logoPublic != null) ? personalizationConfig.logoPublic : personalizationConfig.logo);
        template.put("charset", personalizationConfig.charset);
        template.put("mainLanguage", languageConfig.mainLanguage);
        template.put("alternativeLanguage", languageConfig.alternativeLanguage);
        template.put("enlaceManualMiembros", personalizationConfig.enlaceManualMiembros);
        template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
        template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
        template.put("customCSS", (personalizationConfig.customCSS != null) ? personalizationConfig.customCSS : "");
        template.put("connectedUserId", connectedUserId);
        template.put("puntosOrdenDia", recuentoVotosTemplates);

        return template;
    }

    private void addResultadoVotacionSubpuntos(PuntoOrdenDiaRecuentoVotosDTO puntoOrdenDiaRecuentoVotosDTO, PuntoOrdenDiaRecuentoVotosDTO puntoRecuentoVotosDTO) throws TipoRecuentoVotoNoExisteException {
        List<PuntoOrdenDiaRecuentoVotosDTO> subpuntosDTOs = new ArrayList<>();

        for (PuntoOrdenDiaRecuentoVotosDTO subpuntoOrdenDiaRecuentoVotosDTO : puntoOrdenDiaRecuentoVotosDTO.getSubpuntos()) {

            PuntoOrdenDiaRecuentoVotosDTO subPuntoRecuentoVotosDTO =
                    new PuntoOrdenDiaRecuentoVotosDTO(subpuntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia(), 0L, 0L, 0L, 0L, false, new ArrayList<VotanteVoto>());

            PuntoSecretoVotosDTO subPuntoSecretoVotosDTO = votacionService.getPuntoSecretoVotosByPuntoId(subpuntoOrdenDiaRecuentoVotosDTO.getPuntoOrdenDia().getId());

            if (subPuntoSecretoVotosDTO != null) {

                subPuntoRecuentoVotosDTO = votacionService.mapperPuntoSecretoVotosDTOToPuntoOrdenDiaRecuentoVotosDTO(subPuntoSecretoVotosDTO);

                subpuntosDTOs.add(subPuntoRecuentoVotosDTO);
            } else {
                subpuntosDTOs.add(subpuntoOrdenDiaRecuentoVotosDTO);
            }

            addResultadoVotacionSubpuntos(subpuntoOrdenDiaRecuentoVotosDTO, subPuntoRecuentoVotosDTO);
        }

        puntoRecuentoVotosDTO.setSubpuntos(subpuntosDTOs);
    }

    @PUT
    @Path("abre/votacion/{puntoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void abreVotacionPunto(@PathParam("puntoId") Long puntoId)
            throws PersonasExternasException, ReunionYaCompletadaException,
            CargoNoSecretarioNoPuedeAbrirVotacionException,
            NoEsTelematicaExceptionConVotacion, PuntoProcedimientoNoVotable {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaFromDirectoryByPersonaId(connectedUserId);

        votacionService.abreVotacionPunto(puntoId, persona);
    }

    @PUT
    @Path("cierra/votacion/{puntoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> cierraVotacionPunto(@PathParam("puntoId") Long puntoId)
            throws PersonasExternasException, VotacionNoAbiertaException, ReunionYaCompletadaException,
            CargoNoSecretarioNoPuedeAbrirVotacionException,
            NoEsTelematicaExceptionConVotacion, PuntoProcedimientoNoVotable {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaFromDirectoryByPersonaId(connectedUserId);
        return UIEntity.toUI(votacionService.cierraVotacionPunto(puntoId, persona));
    }

    @PUT
    @Path("secretos/{puntoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void guardarVotacionPuntoSecreto(@PathParam("puntoId") Long puntoId, UIEntity votos) {
        Long votosFavor = Long.parseLong(votos.get("votosFavor"));
        Long votosContra = Long.parseLong(votos.get("votosContra"));
        Long votosBlanco = Long.parseLong(votos.get("votosBlanco"));
        votacionService.guardarVotacionPuntoSecreto(puntoId, votosFavor, votosContra, votosBlanco);
    }
}
