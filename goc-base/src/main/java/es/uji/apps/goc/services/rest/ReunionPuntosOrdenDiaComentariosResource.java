package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.AsistenteNoEncontradoException;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.ReunionNoAdmiteComentariosException;
import es.uji.apps.goc.exceptions.ReunionYaCompletadaException;
import es.uji.apps.goc.model.ComentarioPuntoOrdenDia;
import es.uji.apps.goc.services.PuntoOrdenDiaComentarioService;
import es.uji.apps.goc.services.ReunionComentarioService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;

@Path("/reuniones/{reunionId}/puntosOrdenDia/{puntoOrdenDiaId}/comentarios")
public class ReunionPuntosOrdenDiaComentariosResource extends CoreBaseService
{

    @PathParam("reunionId")
    Long reunionId;

    @PathParam("puntoOrdenDiaId")
    Long puntoORdenDiaId;

    @InjectParam
    PuntoOrdenDiaComentarioService puntoOrdenDiaComentarioService;

    @InjectParam
    ReunionComentarioService reunionComentarioService;


    @InjectParam
    ReunionService reunionService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addComentarioPuntoOrdenDia(UIEntity puntoOrdenDiaComentarioUI)
        throws PersonasExternasException, ReunionNoAdmiteComentariosException, Exception
    {
        ComentarioPuntoOrdenDia comentarioPuntoOrdenDia = toComentarioPuntoOrdenDia(puntoOrdenDiaComentarioUI);
        Reunion reunion = reunionService.getReunionById(reunionId);
        if(reunion.isAdmiteComentarios())
        {
            ComentarioPuntoOrdenDia comentarioPuntoOrdenDiaCreado = puntoOrdenDiaComentarioService.addComentario(comentarioPuntoOrdenDia);

            return UIEntity.toUI(comentarioPuntoOrdenDiaCreado);
        }
        throw new ReunionNoAdmiteComentariosException("Esta reunión no admite comentarios");
    }

    private ComentarioPuntoOrdenDia toComentarioPuntoOrdenDia(UIEntity puntoOrdenDiaComentarioUI)
    {
        ComentarioPuntoOrdenDia comentarioPuntoOrdenDia = new ComentarioPuntoOrdenDia();
        comentarioPuntoOrdenDia.setComentario(puntoOrdenDiaComentarioUI.get("comentario"));
        comentarioPuntoOrdenDia.setCreadorId(AccessManager.getConnectedUserId(request));
        comentarioPuntoOrdenDia.setFecha(new Date());
        comentarioPuntoOrdenDia.setPuntoOrdenDiaId(puntoORdenDiaId);
        String comentaruioSuperiorString = puntoOrdenDiaComentarioUI.get("comentarioSuperiorId");
        if (comentaruioSuperiorString != null && !comentaruioSuperiorString.isEmpty())
        {
            Long comentarioSuperiorId = Long.valueOf(comentaruioSuperiorString);
            if (comentarioSuperiorId != null)
            {
                comentarioPuntoOrdenDia.setComentarioSuperiorId(comentarioSuperiorId);
            }
        }
        return comentarioPuntoOrdenDia;
    }

    @DELETE
    @Path("{comentarioId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Long deleteComentario(@PathParam("reunionId") Long reunionId, @PathParam("comentarioId") Long comentarioId)
            throws ReunionYaCompletadaException
    {
        User userConnected = AccessManager.getConnectedUser(request);

        reunionService.compruebaReunionNoCompletada(reunionId);
        puntoOrdenDiaComentarioService.deleteComentario(comentarioId, userConnected);
        return comentarioId;
    }
}
