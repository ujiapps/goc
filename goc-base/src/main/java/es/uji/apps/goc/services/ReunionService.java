package es.uji.apps.goc.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysema.query.Tuple;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import es.uji.apps.goc.Utils;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.*;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.CodigoCargoEnum;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.enums.TipoRecuentoVotoEnum;
import es.uji.apps.goc.enums.TipoVisualizacionResultadosEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.firmas.FirmaService;
import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.*;
import es.uji.apps.goc.notifications.AvisosReunion;
import es.uji.apps.goc.notifications.Mensaje;
import es.uji.commons.sso.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Component
public class ReunionService {
    @Autowired
    PersonalizationConfig personalizationConfig;
    @Autowired
    FirmaService firmaService;
    @Value("${goc.external.firmasEndpoint}")
    private String firmasEndpoint;
    @Value("${goc.external.publicarAcuerdosEndpoint}")
    private String publicarAcuerdosEndpoint;
    @Value("${goc.external.authToken}")
    private String authToken;
    @Value("${goc.publicUrl}")
    private String publicUrl;
    @Autowired
    private ReunionDAO reunionDAO;
    @Autowired
    private OrganoReunionDAO organoReunionDAO;
    @Autowired
    private OrganoReunionMiembroService organoReunionMiembroService;
    @Autowired
    private ReunionComentarioService reunionComentarioService;
    @Autowired
    private OrganoService organoService;
    @Autowired
    private PersonaService personaService;
    @Autowired
    private ReunionDocumentoDAO reunionDocumentoDAO;
    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;
    @Autowired
    private OrganoReunionMiembroDAO organoReunionMiembroDAO;
    @Autowired
    private PuntoOrdenDiaDocumentoDAO puntoOrdenDiaDocumentoDAO;
    @Autowired
    private PuntoOrdenDiaAcuerdoDAO puntoOrdenDiaAcuerdoDAO;
    @Autowired
    private PuntoOrdenDiaDescriptorDAO puntoOrdenDiaDescriptorDAO;
    @Autowired
    private AvisosReunion avisosReunion;
    @Autowired
    private PuntoOrdenDiaService puntoOrdenDiaService;
    @Autowired
    private ReunionInvitadoDAO reunionInvitadoDAO;
    @Autowired
    private TipoOrganoDAO tipoOrganoDAO;
    @Autowired
    private OrganoDAO organoDAO;
    @Autowired
    private DescriptorDAO descriptorDAO;
    @Autowired
    private ClaveDAO claveDAO;
    @Autowired
    private PuntoOrdenDiaComentarioDAO puntoOrdenDiaComentarioDAO;
    @Autowired
    private LanguageConfig languageConfig;
    @Autowired
    private MiembroService miembroService;
    @Autowired
    private ReunionDiligenciaDAO reunionDiligenciaDAO;
    @Autowired
    private PuntoOrdenDiaComentarioService puntoOrdenDiaComentarioService;
    @Autowired
    private ReunionHojaFirmasDAO reunionHojaFirmasDAO;

    private static boolean containsPersonaId(List<Miembro> lista, Long personaId) {
        return lista.stream().filter(m -> m.getPersonaId().equals(personaId)).findFirst().isPresent();
    }

    public List<ReunionEditor> getReunionesByEditorId(Boolean completada, String organoId, Long tipoOrganoId,
                                                      Boolean externo, Long connectedUserId, String query, Integer start, Integer limit) {
        List<ReunionEditor> reuniones =
                reunionDAO.getReunionesByEditorIdList(connectedUserId, completada, tipoOrganoId, organoId, externo, query, start, limit);

        return reuniones;
    }

    public Reunion addReunion(Reunion reunion, Long connectedUserId)
            throws PersonasExternasException, NoAdmiteVotacionYAceptaCambioDeVotoException {
        Persona convocante = personaService.getPersonaFromDirectoryByPersonaId(connectedUserId);

        return addReunion(reunion, connectedUserId, convocante);
    }

    @Transactional
    public Reunion addReunion(Reunion reunion, Long connectedUserId, Persona convocante)
            throws NoAdmiteVotacionYAceptaCambioDeVotoException {
        reunion.setCreadorId(connectedUserId);
        reunion.setCreadorNombre(convocante.getNombre());
        reunion.setCreadorEmail(convocante.getEmail());
        reunion.setNotificada(false);
        reunion.setCompletada(false);
        reunion.setFechaCompletada(null);
        reunion.setAvisoPrimeraReunion(false);
        reunion.setFechaCreacion(new Date());

        reunion.setAvisoPrimeraReunionUserEmail(null);
        reunion.setAvisoPrimeraReunionUser(null);
        reunion.setAvisoPrimeraReunionFecha(null);

        reunion.setConvocatoriaComienzo(null);
        reunion.setMiembroResponsableActa(null);
        reunion.setAcuerdos(null);

        if (reunion.getTipoVisualizacionResultados().equals(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION)) {
            reunion.setFechaFinVotacion(null);
        }

        if (personalizationConfig.habilitarVotaciones) {
            NoAdmiteVotacionYAceptaCambioDeVotoException.check(reunion);
        }

        return reunionDAO.insert(reunion);
    }

    @Transactional
    public Reunion updateReunion(Long reunionId, String asunto, String asuntoAlternativo, String descripcion,
                                 String descripcionAlternativa, Long duracion, Date fecha, Date fechaSegundaConvocatoria, String ubicacion,
                                 String ubicacionAlternativa, String urlGrabacion, Long numeroSesion, Boolean publica, Boolean telematica,
                                 String telematicaDescripcion, String telematicaDescripcionAlternativa, Boolean admiteSuplencia,
                                 Boolean admiteDelegacionVoto, Boolean admiteComentarios, Boolean emailsEnNuevosComentarios,
                                 Long connectedUserId, String horaFin, Long convocatoriaComienzo, Boolean votacionTelematica,
                                 Boolean admiteCambioVoto, TipoVisualizacionResultadosEnum tipoVisualizacionResultados,
                                 Date fechaFinVotacion, Long responsableVotoId, String responsabelVotoNom, Boolean verDeliberaciones, Boolean verAcuerdos)
            throws ReunionNoDisponibleException, NoVotacionTelematicaException,
            NoAdmiteVotacionYAceptaCambioDeVotoException {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        reunion.setAsunto(asunto);
        reunion.setAsuntoAlternativo(asuntoAlternativo);
        reunion.setDescripcion(descripcion);
        reunion.setDescripcionAlternativa(descripcionAlternativa);
        reunion.setDuracion(duracion);
        reunion.setUbicacion(ubicacion);
        reunion.setUbicacionAlternativa(ubicacionAlternativa);
        reunion.setUrlGrabacion(urlGrabacion);
        reunion.setFecha(fecha);
        reunion.setFechaSegundaConvocatoria(fechaSegundaConvocatoria);
        reunion.setNumeroSesion(numeroSesion);
        reunion.setPublica(publica);
        reunion.setTelematica(telematica);
        reunion.setTelematicaDescripcion(telematicaDescripcion);
        reunion.setTelematicaDescripcionAlternativa(telematicaDescripcionAlternativa);
        reunion.setAdmiteSuplencia(admiteSuplencia);
        reunion.setAdmiteDelegacionVoto(admiteDelegacionVoto);
        reunion.setAdmiteComentarios(admiteComentarios);
        reunion.setEmailsEnNuevosComentarios(emailsEnNuevosComentarios);
        reunion.setHoraFin(horaFin);
        reunion.setConvocatoriaComienzo(convocatoriaComienzo);
        reunion.setVotacionTelematica(votacionTelematica);
        reunion.setAdmiteCambioVoto(admiteCambioVoto);
        reunion.setTipoVisualizacionResultadosEnum(tipoVisualizacionResultados);
        reunion.setFechaFinVotacion(fechaFinVotacion);
        reunion.setResponsableVotoId(responsableVotoId);
        reunion.setResponsableVotoNom(responsabelVotoNom);
        reunion.setVerDeliberaciones(verDeliberaciones);
        reunion.setVerAcuerdos(verAcuerdos);


        if (reunion.isVotacionTelematica()) {
            if (reunion.getTipoVisualizacionResultados().equals(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION)) {
                reunion.setFechaFinVotacion(null);
            }

            if (reunion.getTipoVisualizacionResultados()
                    .equals(TipoVisualizacionResultadosEnum.AL_ALCANZAR_FECHA_FIN_VOTACION)) {
                puntoOrdenDiaService.actualizadoFechaFinVotacionPuntos(reunion);
            }

            if (reunion.getTipoVisualizacionResultados()
                    .equals(TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION)) {
                puntoOrdenDiaService.eliminaFechaFinVotacionPuntos(reunion);
            }
        }

        if (!reunion.isVotacionTelematica()) {
            reunion.setFechaFinVotacion(null);
        }

        if (personalizationConfig.habilitarVotaciones) {
            NoAdmiteVotacionYAceptaCambioDeVotoException.check(reunion);
        }

        return reunionDAO.update(reunion);
    }

    @Transactional(rollbackFor = {PuntoDelDiaConAcuerdosException.class, ReunionNoDisponibleException.class})
    public void removeReunionById(Long reunionId, Long connectedUserId)
            throws ReunionNoDisponibleException, PuntoDelDiaConAcuerdosException, Exception {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        Mensaje mensaje = null;
        if (reunion.getAvisoPrimeraReunion()) {
            mensaje = avisosReunion.getMensajeAvisoAnulacion(reunion);
        }

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        puntoOrdenDiaService.deleteByReunionId(reunionId);
        reunionDAO.deleteByReunionId(reunionId);

        if (reunion.getAvisoPrimeraReunion()) {
            avisosReunion.enviarAvisoAnulacionReunion(mensaje);
        }
    }

    @Transactional
    public void updateInvitadosByReunionId(Long reunionId, List<ReunionInvitado> reunionInvitados, Long connectedUserId)
            throws ReunionNoDisponibleException {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        reunionInvitadoDAO.deleteByReunionId(reunionId);

        for (ReunionInvitado reunionInvitado : reunionInvitados) {
            reunionInvitadoDAO.insert(reunionInvitado);
        }
    }

    @Transactional
    public void updateInvitadoById(ReunionInvitado reunionInvitado)
            throws ReunionNoDisponibleException {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionInvitado.getReunion().getId());

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        reunionInvitadoDAO.updateReunionInvitado(reunionInvitado);

    }

    @Transactional
    public void updateOrganosReunionByReunionId(Long reunionId, List<Organo> organos, Long connectedUserId)
            throws ReunionNoDisponibleException, MiembrosExternosException, OrganosExternosException {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        List<Long> listaIdsLocales = new ArrayList<>();
        List<String> listaIdsExternos = new ArrayList<>();

        if (organos != null) {
            for (Organo organo : organos) {
                if (organo.isExterno()) {
                    listaIdsExternos.add(organo.getId());
                } else {
                    listaIdsLocales.add(Long.parseLong(organo.getId()));
                }
            }
        }

        borraOrganosNoNecesarios(reunion, listaIdsExternos, listaIdsLocales);

        addOrganosLocalesNoExistentes(reunion, listaIdsLocales, connectedUserId);
        addOrganosExternosNoExistentes(reunion, listaIdsExternos, connectedUserId);
    }

    private void addOrganosExternosNoExistentes(Reunion reunion, List<String> listaIdsExternos, Long connectedUserId)
            throws MiembrosExternosException, OrganosExternosException {
        List<String> listaActualOrganosExternosIds = reunion.getReunionOrganos()
                .stream()
                .filter(el -> el.isExterno())
                .map(elem -> elem.getOrganoId())
                .collect(Collectors.toList());

        List<Organo> organosExternos = organoService.getOrganosExternos();

        for (String organoId : listaIdsExternos) {
            if (!listaActualOrganosExternosIds.contains(organoId)) {
                OrganoReunion organoReunion = new OrganoReunion();
                organoReunion.setReunion(reunion);
                organoReunion.setOrganoId(organoId);
                organoReunion.setExterno(true);

                Organo organo = getOrganoExternoById(organoId, organosExternos);
                organoReunion.setOrganoNombre(organo.getNombre());
                organoReunion.setOrganoNombreAlternativo(organo.getNombreAlternativo());
                organoReunion.setTipoOrganoId(organo.getTipoOrgano().getId());

                organoReunionDAO.insert(organoReunion);

                organoReunionMiembroService.addOrganoReunionMiembros(organoReunion, connectedUserId);
            }
        }
    }

    private Organo getOrganoExternoById(String organoId, List<Organo> organosExternos) {
        for (Organo organo : organosExternos) {
            if (organo.getId().equals(organoId)) {
                return organo;
            }
        }

        return null;
    }

    private void addOrganosLocalesNoExistentes(Reunion reunion, List<Long> listaIdsLocales, Long connectedUserId)
            throws MiembrosExternosException {
        List<Long> listaActualOrganosLocalesIds = reunion.getReunionOrganos()
                .stream()
                .filter(el -> el.isExterno() == false)
                .map(elem -> Long.parseLong(elem.getOrganoId()))
                .collect(Collectors.toList());

        for (Long organoId : listaIdsLocales) {
            if (!listaActualOrganosLocalesIds.contains(organoId)) {
                Organo organo = organoService.getOrganoById(organoId);
                OrganoReunion organoReunion = new OrganoReunion();
                organoReunion.setReunion(reunion);
                organoReunion.setOrganoId(organoId.toString());
                organoReunion.setExterno(false);
                organoReunion.setOrganoNombre(organo.getNombre());
                organoReunion.setOrganoNombreAlternativo(organo.getNombreAlternativo());
                organoReunion.setTipoOrganoId(organo.getTipoOrgano().getId());
                organoReunion.setConvocarSinOrdenDia(organo.getConvocarSinOrdenDia());

                organoReunionDAO.insert(organoReunion);

                organoReunionMiembroService.addOrganoReunionMiembros(organoReunion, connectedUserId);
            }
        }
    }

    private void borraOrganosNoNecesarios(Reunion reunion, List<String> listaIdsExternos, List<Long> listaIdsLocales) {
        for (OrganoReunion cr : reunion.getReunionOrganos()) {
            if (!cr.isExterno() && !listaIdsLocales.contains(Long.parseLong(cr.getOrganoId()))) {
                organoReunionDAO.delete(OrganoReunion.class, cr.getId());
            }

            if (cr.isExterno() && !listaIdsExternos.contains(cr.getOrganoId())) {
                organoReunionDAO.delete(OrganoReunion.class, cr.getId());
            }

        }
    }

    @Transactional(rollbackFor = FirmaReunionException.class)
    public void firmarReunion(Long reunionId, String acuerdos, String acuerdosAlternativos, Long responsableActaId,
                              Long connectedUserId)
            throws ReunionYaCompletadaException, FirmaReunionException, OrganosExternosException,
            PersonasExternasException, IOException, NoSuchAlgorithmException, NoVotacionTelematicaException,
            ReunionNoAbiertaException {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion.isCompletada()) {
            throw new ReunionYaCompletadaException();
        }

        if (reunion.isTelematica() && reunion.isVotacionTelematica()) {
            puntoOrdenDiaService.cierraFechaVotacionPuntos(reunion);
        }

        reunion.setCompletada(true);
        reunion.setAcuerdos(acuerdos);
        reunion.setAcuerdosAlternativos(acuerdosAlternativos);

        ReunionFirma reunionFirma = reunionFirmaDesdeReunion(reunion, responsableActaId, connectedUserId);

        RespuestaFirma respuestaFirma = firmaService.firmaExterna(reunionFirma);

        actualizarAcuerdosPuntosDelDia(respuestaFirma);
        actualizarAsistencias(reunion.getId(), respuestaFirma);

        reunionDAO.marcarReunionComoCompletadaYActualizarAcuerdoYUrl(reunionId, responsableActaId, acuerdos,
                acuerdosAlternativos, respuestaFirma);
    }

    @Transactional
    public void publicarAcuerdos(Long reunionId, Long connectedUserId)
            throws OrganosExternosException, PersonasExternasException, IOException, NoSuchAlgorithmException,
            PublicarAcuerdosException {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        ReunionFirma reunionFirma = reunionFirmaDesdeReunion(reunion, null, connectedUserId);

        Client client = Client.create(Utils.createClientConfig());

        WebResource getPublicarAcuerdosResource = client.resource(this.publicarAcuerdosEndpoint);

        ClientResponse response = getPublicarAcuerdosResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken)
                .post(ClientResponse.class, reunionFirma);

        if (response.getStatus() != 200) {
            throw new PublicarAcuerdosException();
        }

        RespuestaFirma respuestaPublicarAcuerdos = RespuestaFirma.buildRespuestaFirma(response);

        actualizarAcuerdosPuntosDelDia(respuestaPublicarAcuerdos);
    }

    private void actualizarAsistencias(Long reunionId, RespuestaFirma respuestaFirma) {
        for (RespuestaFirmaAsistencia respuestaFirmaAsistencia : respuestaFirma.getAsistencias()) {
            reunionDAO.updateAsistenciaInvitadoReunion(reunionId, respuestaFirmaAsistencia);
            reunionDAO.updateAsistenciaInvitadoOrgano(reunionId, respuestaFirmaAsistencia);
            reunionDAO.updateAsistenciaMiembrosYSuplentes(reunionId, respuestaFirmaAsistencia);
        }
    }

    @Transactional
    public void actualizarAsistenciaAsincrona(Long reunionId, Long personaId, String urlAsistencia) {
        reunionDAO.updateAsistenciaInvitadoReunionAsincrona(reunionId, personaId, urlAsistencia);
        reunionDAO.updateAsistenciaInvitadoOrganoReunionAsincrona(reunionId, personaId, urlAsistencia);
        reunionDAO.updateAsistenciaMiembrosYSuplentesAsincrona(reunionId, personaId, urlAsistencia);
    }

    @Transactional
    public void actualizarAsistenciaAlternativaAsincrona(Long reunionId, Long personaId, String urlAsistencia) {
        reunionDAO.updateAsistenciaAlternativaInvitadoReunionAsincrona(reunionId, personaId, urlAsistencia);
        reunionDAO.updateAsistenciaAlternativaInvitadoOrganoReunionAsincrona(reunionId, personaId, urlAsistencia);
        reunionDAO.updateAsistenciaAlternativaMiembrosYSuplentesAsincrona(reunionId, personaId, urlAsistencia);
    }

    private void actualizarAcuerdosPuntosDelDia(RespuestaFirma respuestaFirma) {
        for (RespuestaFirmaPuntoOrdenDiaAcuerdo acuerdo : respuestaFirma.getPuntoOrdenDiaAcuerdos()) {
            reunionDAO.updateAcuerdoPuntoDelDiaUrlActa(acuerdo);
        }
    }

    private ReunionFirma reunionFirmaDesdeReunion(Reunion reunion, Long responsableActaId, Long connectedUserId)
            throws OrganosExternosException, PersonasExternasException, IOException, NoSuchAlgorithmException {
        ReunionFirma reunionFirma = new ReunionFirma();

        reunionFirma.setId(reunion.getId());
        reunionFirma.setAsunto(reunion.getAsunto());
        reunionFirma.setAsuntoAlternativo(reunion.getAsuntoAlternativo());
        reunionFirma.setDescripcion(reunion.getDescripcion());
        reunionFirma.setDescripcionAlternativa(reunion.getDescripcionAlternativa());
        reunionFirma.setDuracion(reunion.getDuracion());
        reunionFirma.setNumeroSesion(reunion.getNumeroSesion());
        reunionFirma.setAcuerdos(reunion.getAcuerdos());
        reunionFirma.setAcuerdosAlternativos(reunion.getAcuerdosAlternativos());
        reunionFirma.setUbicacion(reunion.getUbicacion());
        reunionFirma.setUbicacionAlternativa(reunion.getUbicacionAlternativa());
        reunionFirma.setFecha(reunion.getFecha());
        reunionFirma.setFechaSegundaConvocatoria(reunion.getFechaSegundaConvocatoria());
        reunionFirma.setUrlGrabacion(reunion.getUrlGrabacion());
        reunionFirma.setTelematica(reunion.isTelematica());
        reunionFirma.setTelematicaDescripcion(reunion.getTelematicaDescripcion());
        reunionFirma.setTelematicaDescripcionAlternativa(reunion.getTelematicaDescripcionAlternativa());
        reunionFirma.setAdmiteSuplencia(reunion.isAdmiteSuplencia());
        reunionFirma.setAdmiteDelegacionVoto(reunion.isAdmiteDelegacionVoto());
        reunionFirma.setCompletada(reunion.getCompletada());
        reunionFirma.setCreadorNombre(reunion.getCreadorNombre());
        reunionFirma.setCreadorEmail(reunion.getCreadorEmail());
        reunionFirma.setConvocatoriaComienzo(reunion.getConvocatoriaComienzo());
        reunionFirma.setHoraFin(reunion.getHoraFin());
        reunionFirma.setReabierta(reunion.isReabierta());

        OrganoReunionMiembro responsable = new OrganoReunionMiembro();

        if (responsableActaId != null) {
            responsable = organoReunionMiembroDAO.getMiembroById(responsableActaId);

            reunionFirma.setResponsableActaId(responsable.getMiembroId());
            reunionFirma.setResponsableActaEmail(responsable.getEmail());
            reunionFirma.setResponsableActa(responsable.getNombre());
            reunionFirma.setCargoResponsableActa(responsable.getCargoNombre());
            reunionFirma.setCargoAlternativoResponsableActa(responsable.getCargoNombreAlternativo());
            reunionFirma.setCargoCodigoResponsableActa(responsable.getCargoCodigo());
        }

        List<Organo> organos = organoService.getOrganosByReunionId(reunion.getId(), connectedUserId);
        List<ReunionComentario> comentarios =
                reunionComentarioService.getComentariosByReunionId(reunion.getId(), connectedUserId);

        List<OrganoFirma> listaOrganosFirma = getOrganosFirmaDesdeOrganos(organos, reunion);
        reunionFirma.setOrganos(listaOrganosFirma);

        List<ReunionDocumento> reunionDocumentos = reunionDocumentoDAO.getDatosDocumentosByReunionId(reunion.getId());
        List<DocumentoFirma> listaDocumentosFirma = getReunionDocumentosFirmaDesdeDocumentos(reunion.getId(), reunionDocumentos);

        reunionFirma.setDocumentos(listaDocumentosFirma);
        reunionFirma.setComentarios(getComentariosFirmaDesdeComentarios(comentarios));

        List<PuntoOrdenDiaMultinivel> puntosOrdenDia =
                puntoOrdenDiaService.getPuntosMultinivelByReunionId(reunion.getId());
        List<PuntoOrdenDiaFirma> listaPuntosOrdenDiaFirma = getPuntosOrdenDiaFirmaDesdePuntosOrdenDia(puntosOrdenDia);

        reunionFirma.setPuntosOrdenDia(listaPuntosOrdenDiaFirma);

        List<InvitadoTemplate> invitados = reunionDAO.getInvitadosPresencialesByReunionId(reunion.getId());
        List<InvitadoFirma> invitadosFirma = getInvitadosFirmaAsistentesDesdeReunionInvitados(invitados);
        List<InvitadoFirma> invitadosFirmaOrdenados = invitadosFirma.stream().sorted(Comparator.comparing(InvitadoFirma::getNombreLimpio)).collect(Collectors.toList());
        reunionFirma.setInvitados(invitadosFirmaOrdenados);

        reunionFirma.setFirmante(getFirmante(reunionFirma));
    
        return reunionFirma;
    }

    private ResponsableFirma getFirmante(ReunionFirma reunionFirma) {
        ResponsableFirma responsableFirma = new ResponsableFirma();

        responsableFirma.setId(reunionFirma.getResponsableActaId());
        responsableFirma.setNombre(reunionFirma.getResponsableActa());
        responsableFirma.setEmail(reunionFirma.getResponsableActaEmail());

        for (OrganoFirma organoFirma : reunionFirma.getOrganos()) {
            for (MiembroFirma miembroFirma : organoFirma.getMiembros()) {
                if (miembroFirma.getAsistencia() && miembroFirma.getCargo() != null && CodigoCargoEnum.SECRETARIO.codigo.equals(
                        miembroFirma.getCargo().getCodigo()) && miembroFirma.getEmail() != null) {
                    responsableFirma.setId(miembroFirma.getId());
                    responsableFirma.setNombre(miembroFirma.getNombre());
                    responsableFirma.setEmail(miembroFirma.getEmail());
                    responsableFirma.setOrgano(organoFirma.getNombre());
                    responsableFirma.setOrganoAlternativo((organoFirma.getNombreAlternativo()));
                }
            }
        }

        return responsableFirma;
    }

    private List<InvitadoFirma> getInvitadosFirmaAsistentesDesdeReunionInvitados(List<InvitadoTemplate> invitados) {
    	
        return invitados.stream().filter(i -> i.getAsistencia()).map(this::getInvitadoFirmaDesdeInvitado).collect(Collectors.toList());
    }

    private InvitadoFirma getInvitadoFirmaDesdeInvitado(InvitadoTemplate invitado) {
        InvitadoFirma invitadoFirma = new InvitadoFirma();

        invitadoFirma.setId(invitado.getId());
        invitadoFirma.setNombre(invitado.getNombre());
        invitadoFirma.setEmail(invitado.getEmail());
        invitadoFirma.setAsistencia(invitado.getAsistencia());

        return invitadoFirma;
    }

    private List<PuntoOrdenDiaFirma> getPuntosOrdenDiaFirmaDesdePuntosOrdenDia(
            List<PuntoOrdenDiaMultinivel> puntosOrdenDia) {
        List<PuntoOrdenDiaFirma> listaPuntosOrdenDiaFirma = new ArrayList<>();

        for (PuntoOrdenDiaMultinivel puntoOrdenDia : puntosOrdenDia) {
            listaPuntosOrdenDiaFirma.add(getPuntoOrdenDiaFirmaDesdePuntoOrdenDia(puntoOrdenDia));
        }
        return listaPuntosOrdenDiaFirma;
    }

    private PuntoOrdenDiaFirma getPuntoOrdenDiaFirmaDesdePuntoOrdenDia(PuntoOrdenDiaMultinivel puntoOrdenDia) {
        PuntoOrdenDiaFirma puntoOrdenDiaFirma = new PuntoOrdenDiaFirma();
        puntoOrdenDiaFirma.setId(puntoOrdenDia.getId());
        puntoOrdenDiaFirma.setTitulo(puntoOrdenDia.getTitulo());
        puntoOrdenDiaFirma.setTituloAlternativo(puntoOrdenDia.getTituloAlternativo());
        puntoOrdenDiaFirma.setAcuerdos(puntoOrdenDia.getAcuerdos());
        puntoOrdenDiaFirma.setAcuerdosAlternativos(puntoOrdenDia.getAcuerdosAlternativos());
        puntoOrdenDiaFirma.setDeliberaciones(puntoOrdenDia.getDeliberaciones());
        puntoOrdenDiaFirma.setDeliberacionesAlternativas(puntoOrdenDia.getDeliberacionesAlternativas());
        puntoOrdenDiaFirma.setDescripcion(puntoOrdenDia.getDescripcion());
        puntoOrdenDiaFirma.setDescripcionAlternativa(puntoOrdenDia.getDescripcionAlternativa());
        puntoOrdenDiaFirma.setOrden(puntoOrdenDia.getOrden());
        puntoOrdenDiaFirma.setEditado(puntoOrdenDia.getEditado());

        List<PuntoOrdenDiaDocumento> documentos =
                puntoOrdenDiaDocumentoDAO.getDatosDocumentosByPuntoOrdenDiaId(puntoOrdenDia.getId(), false);

        List<PuntoOrdenDiaAcuerdo> documentosAcuerdos =
                puntoOrdenDiaAcuerdoDAO.getDatosAcuerdosByPuntoOrdenDiaId(puntoOrdenDia.getId());

        puntoOrdenDiaFirma.setDocumentos(getDocumentosFirmaDesdePuntosOrdenDiaDocumentos(puntoOrdenDia.getReunion().getId(), puntoOrdenDia.getId(), documentos));
        puntoOrdenDiaFirma.setDocumentosAcuerdos(
                getDocumentosAcuerdosFirmaDesdePuntosOrdenDiaDocumentos(puntoOrdenDia.getReunion().getId(), puntoOrdenDia.getId(), documentosAcuerdos));

        if (puntoOrdenDia.getPuntosInferiores() != null) {
            List<PuntoOrdenDiaFirma> subpuntosFirma = new ArrayList<>();

            for (PuntoOrdenDiaMultinivel puntoOrdenDiaMultinivel : getsubpuntosOrdenDiaOrdenados(puntoOrdenDia)) {
                subpuntosFirma.add(getPuntoOrdenDiaFirmaDesdePuntoOrdenDia(puntoOrdenDiaMultinivel));
            }

            puntoOrdenDiaFirma.setSubpuntos(subpuntosFirma);
        }

        return puntoOrdenDiaFirma;
    }

    private List<PuntoOrdenDiaMultinivel> getsubpuntosOrdenDiaOrdenados(PuntoOrdenDiaMultinivel puntoOrdenDia) {
        return puntoOrdenDia.getPuntosInferiores().stream().sorted().collect(Collectors.toList());
    }

    private List<DocumentoFirma> getDocumentosAcuerdosFirmaDesdePuntosOrdenDiaDocumentos(
            Long reunionId, Long puntoOrdenDiaId, List<PuntoOrdenDiaAcuerdo> documentos) {
        List<DocumentoFirma> listaDocumentoTemplate = new ArrayList<>();

        for (PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo : documentos) {
            listaDocumentoTemplate.add(getAcuerdoDocumentoFirmaDesdePuntoOrdenDiaDocumento(reunionId, puntoOrdenDiaId, puntoOrdenDiaAcuerdo));
        }

        return listaDocumentoTemplate;
    }

    private DocumentoFirma getAcuerdoDocumentoFirmaDesdePuntoOrdenDiaDocumento(
            Long reunionId, Long puntoOrdenDiaId, PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo) {
        DocumentoFirma documentoFirma = new DocumentoFirma();

        documentoFirma.setId(puntoOrdenDiaAcuerdo.getId());
        documentoFirma.setDescripcion(puntoOrdenDiaAcuerdo.getDescripcion());
        documentoFirma.setDescripcionAlternativa(puntoOrdenDiaAcuerdo.getDescripcionAlternativa());
        documentoFirma.setMimeType(puntoOrdenDiaAcuerdo.getMimeType());
        documentoFirma.setFechaAdicion(puntoOrdenDiaAcuerdo.getFechaAdicion());
        documentoFirma.setCreadorId(puntoOrdenDiaAcuerdo.getCreadorId());
        documentoFirma.setNombreFichero(puntoOrdenDiaAcuerdo.getNombreFichero());
        documentoFirma.setHash(puntoOrdenDiaAcuerdo.getHash());
        documentoFirma.setUrlDescarga(publicUrl + "/goc/rest/reuniones/" + reunionId + "/puntosOrdenDia/" + puntoOrdenDiaId + "/acuerdos/" + puntoOrdenDiaAcuerdo.getId() + "/descargar");

        return documentoFirma;
    }

    private List<DocumentoFirma> getDocumentosFirmaDesdePuntosOrdenDiaDocumentos(
            Long reunionId, Long puntoOrdenDiaId, List<PuntoOrdenDiaDocumento> documentos) {
        List<DocumentoFirma> listaDocumentoTemplate = new ArrayList<>();

        for (PuntoOrdenDiaDocumento puntoOrdenDiaDocumento : documentos) {
            listaDocumentoTemplate.add(getDocumentoFirmaDesdePuntoOrdenDiaDocumento(reunionId, puntoOrdenDiaId, puntoOrdenDiaDocumento));
        }

        return listaDocumentoTemplate;
    }

    private DocumentoFirma getDocumentoFirmaDesdePuntoOrdenDiaDocumento(Long reunionId, Long puntoOrdenDiaId, PuntoOrdenDiaDocumento puntoOrdenDiaDocumento) {
        DocumentoFirma documentoFirma = new DocumentoFirma();

        documentoFirma.setId(puntoOrdenDiaDocumento.getId());
        documentoFirma.setDescripcion(puntoOrdenDiaDocumento.getDescripcion());
        documentoFirma.setDescripcionAlternativa(puntoOrdenDiaDocumento.getDescripcionAlternativa());
        documentoFirma.setMimeType(puntoOrdenDiaDocumento.getMimeType());
        documentoFirma.setFechaAdicion(puntoOrdenDiaDocumento.getFechaAdicion());
        documentoFirma.setCreadorId(puntoOrdenDiaDocumento.getCreadorId());
        documentoFirma.setNombreFichero(puntoOrdenDiaDocumento.getNombreFichero());
        documentoFirma.setHash(puntoOrdenDiaDocumento.getHash());
        documentoFirma.setUrlDescarga(publicUrl + "/goc/rest/reuniones/" + reunionId + "/puntosOrdenDia/" + puntoOrdenDiaId + "/documentos/" + puntoOrdenDiaDocumento.getId() + "/descargar");

        return documentoFirma;
    }

    private List<Comentario> getComentariosFirmaDesdeComentarios(List<ReunionComentario> comentarios) {
        List<Comentario> listaComentariosFirma = new ArrayList<>();

        for (ReunionComentario comentario : comentarios) {
            listaComentariosFirma.add(getComentarioFirmaDesdeComentario(comentario));
        }

        return listaComentariosFirma;
    }

    private Comentario getComentarioFirmaDesdeComentario(ReunionComentario comentario) {
        Comentario comentarioFirma = new Comentario();
        comentarioFirma.setId(comentario.getId());
        comentarioFirma.setComentario(comentario.getComentario());
        comentarioFirma.setCreadorNombre(comentario.getCreadorNombre());
        comentarioFirma.setCreadorId(comentario.getCreadorId());
        comentarioFirma.setFecha(comentario.getFecha());

        return comentarioFirma;
    }

    private List<DocumentoFirma> getReunionDocumentosFirmaDesdeDocumentos(Long reunionId, List<ReunionDocumento> reunionDocumentos) {
        List<DocumentoFirma> listaDocumentoFirma = new ArrayList<>();

        for (ReunionDocumento reunionDocumento : reunionDocumentos) {
            listaDocumentoFirma.add(getDocumentoFirmaDesdeReunionDocumento(reunionId, reunionDocumento));
        }

        return listaDocumentoFirma;
    }

    private DocumentoFirma getDocumentoFirmaDesdeReunionDocumento(Long reunionId, ReunionDocumento reunionDocumento) {
        DocumentoFirma documentoFirma = new DocumentoFirma();

        documentoFirma.setId(reunionDocumento.getId());
        documentoFirma.setDescripcion(reunionDocumento.getDescripcion());
        documentoFirma.setDescripcionAlternativa(reunionDocumento.getDescripcionAlternativa());
        documentoFirma.setMimeType(reunionDocumento.getMimeType());
        documentoFirma.setFechaAdicion(reunionDocumento.getFechaAdicion());
        documentoFirma.setCreadorId(reunionDocumento.getCreadorId());
        documentoFirma.setNombreFichero(reunionDocumento.getNombreFichero());
        documentoFirma.setHash(reunionDocumento.getHash());
        documentoFirma.setUrlDescarga(publicUrl + "/goc/rest/reuniones/" + reunionId + "/documentos/" + reunionDocumento.getId() + "/descargar");

        return documentoFirma;
    }

    public ReunionTemplate getReunionTemplateDesdeReunionForBuscador(Reunion reunion, boolean isMainLangauge,
                                                                     Long connectedUserId) {
        boolean showNoAsistentes = false;
        return getReunionTemplate(reunion, isMainLangauge, connectedUserId, true, showNoAsistentes);
    }

    public ReunionTemplate getReunionTemplateDesdeReunion(Reunion reunion, Long connectedUserId,
                                                          Boolean withNoAsistentes, Boolean mainLanguage) {
        ReunionTemplate reunionTemplate =
                getReunionTemplate(reunion, mainLanguage, connectedUserId, false, withNoAsistentes);

        OrganoReunionMiembro responsable = new OrganoReunionMiembro();

        if (reunion.getMiembroResponsableActa() != null) {
            responsable = organoReunionMiembroDAO.getMiembroById(reunion.getMiembroResponsableActa().getId());

            reunionTemplate.setResponsableActa(responsable.getNombre());
            reunionTemplate.setCargoResponsableActa(
                    mainLanguage ? responsable.getCargoNombre() : responsable.getCargoNombreAlternativo());
        }

        List<ReunionComentario> comentarios =
                reunionComentarioService.getComentariosByReunionId(reunion.getId(), connectedUserId);

        List<ReunionDocumento> reunionDocumentos = reunionDocumentoDAO.getDatosDocumentosByReunionId(reunion.getId());
        List<DocumentoTemplate> listaDocumentosTemplate =
                getReunionDocumentosTemplateDesdeDocumentos(reunionDocumentos, mainLanguage);

        reunionTemplate.setDocumentos(listaDocumentosTemplate);
        reunionTemplate.setComentarios(getComentariosTemplateDessdeComentarios(comentarios));

        List<InvitadoTemplate> invitados = reunionDAO.getInvitadosPresencialesByReunionId(reunion.getId());
        reunionTemplate.setInvitados(invitados);

        ResponsableFirma firmante = getFirmante(reunionTemplate);
        if (firmante.getId() != null) {
            reunionTemplate.setFirmante(firmante);
        }

        reunionTemplate.setReabierta(reunion.getReabierta());
        reunionTemplate.setVotacionTelematica(reunion.getVotacionTelematica());
        reunionTemplate.setAdmiteCambioVoto(reunion.getAdmiteCambioVoto());
        reunionTemplate.setVerDeliberaciones(reunion.getVerDeliberaciones());
        reunionTemplate.setVerAcuerdos(reunion.getVerAcuerdos());
        return reunionTemplate;
    }

    private ReunionTemplate getReunionTemplate(Reunion reunion, Boolean mainLanguage, Long connectedUserId,
                                               Boolean isForBuscador, boolean showNoAsistentes) {
        ReunionTemplate reunionTemplate = new ReunionTemplate();

        reunionTemplate.setId(reunion.getId());
        reunionTemplate.setAsunto(mainLanguage ? reunion.getAsunto() : reunion.getAsuntoAlternativo());
        reunionTemplate.setDescripcion(mainLanguage ? reunion.getDescripcion() : reunion.getDescripcionAlternativa());
        reunionTemplate.setDuracion(reunion.getDuracion());
        reunionTemplate.setNumeroSesion(reunion.getNumeroSesion());
        reunionTemplate.setAcuerdos(mainLanguage ? reunion.getAcuerdos() : reunion.getAcuerdosAlternativos());
        reunionTemplate.setUbicacion(mainLanguage ? reunion.getUbicacion() : reunion.getUbicacionAlternativa());
        reunionTemplate.setFecha(reunion.getFecha());
        reunionTemplate.setFechaSegundaConvocatoria(reunion.getFechaSegundaConvocatoria());
        reunionTemplate.setUrlGrabacion(reunion.getUrlGrabacion());
        reunionTemplate.setUrlActaAntesReapertura(reunion.getUrlActaAntesReapertura());
        reunionTemplate.setUrlActaAlternativaAntesReapertura(reunion.getUrlActaAlternativaAntesReapertura());
        reunionTemplate.setTelematica(reunion.isTelematica());
        reunionTemplate.setTelematicaDescripcion(
                mainLanguage ? reunion.getTelematicaDescripcion() : reunion.getTelematicaDescripcionAlternativa());
        reunionTemplate.setPublica(reunion.isPublica());
        reunionTemplate.setAdmiteSuplencia(reunion.isAdmiteSuplencia());
        reunionTemplate.setAdmiteDelegacionVoto(reunion.isAdmiteDelegacionVoto());
        reunionTemplate.setAdmiteComentarios(reunion.isAdmiteComentarios());
        reunionTemplate.setEmailsEnNuevosComentarios(reunion.isEmailsEnNuevosComentarios());
        reunionTemplate.setCompletada(reunion.getCompletada());
        reunionTemplate.setCreadorNombre(reunion.getCreadorNombre());
        reunionTemplate.setCreadorEmail(reunion.getCreadorEmail());
        reunionTemplate.setCreadorId(reunion.getCreadorId());
        reunionTemplate.setUrlActa(mainLanguage ? reunion.getUrlActa() : reunion.getUrlActaAlternativa());
        reunionTemplate.setHoraFinalizacion(reunion.getHoraFin());
        reunionTemplate.setConvocatoriaComienzo(reunion.getConvocatoriaComienzo());
        Set<OrganoReunion> reunionOrganos = reunion.getReunionOrganos();
        reunionTemplate.setFechaFinVotacion(reunion.getFechaFinVotacion());

        if (reunionOrganos != null && isForBuscador) {
            ArrayList<OrganoTemplate> organoTemplates = new ArrayList<>();
            for (OrganoReunion reunionOrgano : reunionOrganos) {
                organoTemplates.add(getOrganoTemplateDesdeOrganoReunion(reunionOrgano, mainLanguage));
            }
            reunionTemplate.setOrganos(organoTemplates);
        } else {
            List<Organo> organos = organoService.getOrganosByReunionId(reunion.getId(), connectedUserId);
            List<OrganoTemplate> listaOrganosTemplate =
                    getOrganosTemplateDesdeOrganos(organos, reunion, showNoAsistentes, mainLanguage);
            reunionTemplate.setOrganos(listaOrganosTemplate);
        }

        List<PuntoOrdenDiaMultinivel> puntosOrdenDia = puntoOrdenDiaService.getPuntosMultinivelByReunionId(reunion.getId());

        List<PuntoOrdenDiaTemplate> listaPuntosOrdenDiaTemplate =
                getPuntosOrdenDiaTemplateDesdePuntosOrdenDia(puntosOrdenDia, reunion.getId(), mainLanguage,
                        isForBuscador, connectedUserId);
        reunionTemplate.setPuntosOrdenDia(listaPuntosOrdenDiaTemplate);
        reunionTemplate.setAbierta(reunion.isAbierta());
        reunionTemplate.setMostrarResultados(getMostrarResultados(reunion, puntosOrdenDia));
        reunionTemplate.setVerDeliberaciones(reunion.getVerDeliberaciones());
        reunionTemplate.setVerAcuerdos(reunion.getVerAcuerdos());

        return reunionTemplate;
    }

    private boolean getMostrarResultados(Reunion reunion, List<PuntoOrdenDiaMultinivel> puntosOrdenDia) {
        if (!reunion.getVotacionTelematica()) return false;

        if (puntosOrdenDia != null && existeAlgunPuntoConVotacionCerrada(puntosOrdenDia.stream().collect(Collectors.toSet()))) {
            return true;
        }

        return reunion.getTipoVisualizacionResultados() == TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION ? reunion.isCompletada() : reunion.getFechaFinVotacion() != null && reunion.getFechaFinVotacion()
                .before(new Date());
    }

    private boolean existeAlgunPuntoConVotacionCerrada(Set<PuntoOrdenDiaMultinivel> puntosOrdenDia) {
        if (puntosOrdenDia == null || puntosOrdenDia.size() == 0) return false;

        if (puntosOrdenDia.stream().anyMatch(p -> p.isVotacionCerradaManualmente() != null && p.isVotacionCerradaManualmente()))
            return true;

        for (PuntoOrdenDiaMultinivel puntoOrdenDia : puntosOrdenDia) {
            if (existeAlgunPuntoConVotacionCerrada(puntoOrdenDia.getPuntosInferiores())) return true;
        }

        return false;
    }

    private OrganoTemplate getOrganoTemplateDesdeOrganoReunion(OrganoReunion organoReunion, boolean isMainLanguage) {
        OrganoTemplate organoTemplate = new OrganoTemplate();
        organoTemplate.setId(organoReunion.getOrganoId());
        organoTemplate.setNombre(
                isMainLanguage ? organoReunion.getOrganoNombre() : organoReunion.getOrganoNombreAlternativo());
        return organoTemplate;
    }

    private ResponsableFirma getFirmante(ReunionTemplate reunionTemplate) {
        ResponsableFirma responsableFirma = new ResponsableFirma();

        responsableFirma.setNombre(reunionTemplate.getResponsableActa());

        for (OrganoTemplate organoTemplate : reunionTemplate.getOrganos()) {
            if (organoTemplate.getAsistentes() != null) {
                for (MiembroTemplate miembroTemplate : organoTemplate.getAsistentes()) {
                    if (miembroTemplate.getAsistencia() && miembroTemplate.getCargo() != null && CodigoCargoEnum.SECRETARIO.codigo.equals(
                            miembroTemplate.getCargo().getCodigo()) && miembroTemplate.getEmail() != null) {
                        responsableFirma.setId(miembroTemplate.getId());
                        responsableFirma.setNombre(miembroTemplate.getNombre());
                        responsableFirma.setEmail(miembroTemplate.getEmail());
                        responsableFirma.setOrgano(organoTemplate.getNombre());
                    }
                }
            }
        }

        return responsableFirma;
    }

    private List<Comentario> getComentariosTemplateDessdeComentarios(List<ReunionComentario> comentarios) {
        List<Comentario> listaComentariosTemplate = new ArrayList<>();

        for (ReunionComentario comentario : comentarios) {
            listaComentariosTemplate.add(getComentarioTemplateDessdeComentario(comentario));
        }

        return listaComentariosTemplate;
    }

    private Comentario getComentarioTemplateDessdeComentario(ReunionComentario comentario) {
        Comentario comentarioTemplate = new Comentario();
        comentarioTemplate.setId(comentario.getId());
        comentarioTemplate.setComentario(comentario.getComentario());
        comentarioTemplate.setFecha(comentario.getFecha());
        comentarioTemplate.setCreadorNombre(comentario.getCreadorNombre());
        comentarioTemplate.setCreadorId(comentario.getCreadorId());

        return comentarioTemplate;
    }

    private List<InvitadoTemplate> getInvitadosTemplateDesdeReunionInvitados(List<Persona> invitados) {
        List<InvitadoTemplate> listaInvitadosTemplate = new ArrayList<>();

        for (Persona invitado : invitados) {
            listaInvitadosTemplate.add(getInvitadoTemplateDessdeInvitado(invitado));
        }

        return listaInvitadosTemplate;
    }

    private InvitadoTemplate getInvitadoTemplateDessdeInvitado(Persona invitado) {
        InvitadoTemplate invitadoTemplate = new InvitadoTemplate();

        invitadoTemplate.setId(invitado.getId());
        invitadoTemplate.setNombre(invitado.getNombre());
        invitadoTemplate.setEmail(invitado.getEmail());

        return invitadoTemplate;
    }

    private List<OrganoTemplate> getOrganosTemplateDesdeOrganos(List<Organo> organos, Reunion reunion,
                                                                Boolean withNoAsistentes, boolean mainLanguage) {
        List<OrganoTemplate> listaOrganoTemplate = new ArrayList<>();

        for (Organo organo : organos) {
            listaOrganoTemplate.add(getOrganoTemplateDesdeOrgano(organo, reunion, withNoAsistentes, mainLanguage));
        }

        return listaOrganoTemplate;
    }

    private OrganoTemplate getOrganoTemplateDesdeOrgano(Organo organo, Reunion reunion, Boolean withNoAsistentes,
                                                        boolean mainLanguage) {
        OrganoTemplate organoTemplate = new OrganoTemplate();
        organoTemplate.setId(organo.getId());
        organoTemplate.setNombre(mainLanguage ? organo.getNombre() : organo.getNombreAlternativo());
        organoTemplate.setTipoCodigo(organo.getTipoOrgano().getCodigo());
        organoTemplate.setTipoNombre(
                mainLanguage ? organo.getTipoOrgano().getNombre() : organo.getTipoOrgano().getNombreAlternativo());
        organoTemplate.setTipoOrganoId(organo.getTipoOrgano().getId());

        OrganoParametro organoParametro = organoDAO.getOrganoParametro(organo.getId(), organo.isExterno());
        if (organoParametro != null && organoParametro.getActaProvisionalActiva() != null) {
            organoTemplate.setActaProvisionalActiva(organoParametro.getActaProvisionalActiva());
        } else {
            organoTemplate.setActaProvisionalActiva(true);
        }

        if (organoParametro != null && organoParametro.getPermiteAbstencionVoto() != null) {
            organoTemplate.setPermiteAbstencionVoto(organoParametro.getPermiteAbstencionVoto());
        } else {
            organoTemplate.setPermiteAbstencionVoto(personalizationConfig.permiteAbstencionVoto);
        }

        if (organoParametro != null && organoParametro.getVerAsistencia() != null) {
            organoTemplate.setVerAsistentes(organoParametro.getVerAsistencia());
        } else {
            organoTemplate.setVerAsistentes(true);
        }

        if (organoParametro != null && organoParametro.getVerDelegaciones() != null) {
            organoTemplate.setVerDelegaciones(organoParametro.getVerDelegaciones());
        } else {
            organoTemplate.setVerDelegaciones(true);
        }

        List<OrganoReunionMiembro> listaAsistentes;
        List<OrganoReunionMiembro> listaMiembros =
                organoReunionMiembroDAO.getMiembroReunionByOrganoAndReunionId(organo.getId(), organo.isExterno(),
                        reunion.getId());
        List<OrganoReunionMiembro> listaMiembrosTodosLosOrganos = organoReunionMiembroDAO.getMiembrosByReunionId(reunion.getId());

        if (withNoAsistentes) {
            listaAsistentes = listaMiembros;
        } else {
            listaAsistentes =
                    organoReunionMiembroDAO.getAsistenteReunionByOrganoAndReunionId(organo.getId(), organo.isExterno(),
                            reunion.getId());
        }

        organoTemplate.setAsistentes(
                getAsistentesDesdeListaOrganoReunionMiembro(listaAsistentes, listaMiembrosTodosLosOrganos, mainLanguage));

        List<OrganoReunionMiembro> listaAsistentesConfirmados = listaAsistentes.stream().filter(a -> a.getAsistencia().equals(true)).collect(Collectors.toList());

        organoTemplate.setAusentes(
                getAusentesDesdeListaOrganoReunionMiembro(listaAsistentesConfirmados, listaMiembros, mainLanguage));
        organoTemplate.setAusentesJustificanAusencia(
                getAusentesJustificanAusenciaDesdeListaOrganoReunionMiembro(listaAsistentesConfirmados, listaMiembrosTodosLosOrganos, mainLanguage));

        return organoTemplate;
    }

    private List<OrganoFirma> getOrganosFirmaDesdeOrganos(List<Organo> organos, Reunion reunion) {
        List<OrganoFirma> listaOrganoFirma = new ArrayList<>();

        for (Organo organo : organos) {
            listaOrganoFirma.add(getOrganoFirmaDesdeOrgano(organo, reunion));
        }

        return listaOrganoFirma;
    }

    private OrganoFirma getOrganoFirmaDesdeOrgano(Organo organo, Reunion reunion) {
        OrganoFirma organoFirma = new OrganoFirma();
        organoFirma.setId(organo.getId());
        organoFirma.setNombre(organo.getNombre());
        organoFirma.setNombreAlternativo(organo.getNombreAlternativo());
        organoFirma.setTipoCodigo(organo.getTipoOrgano().getCodigo());
        organoFirma.setTipoNombre(organo.getTipoOrgano().getNombre());
        organoFirma.setTipoNombreAlternativo(organo.getTipoOrgano().getNombreAlternativo());
        organoFirma.setTipoOrganoId(organo.getTipoOrgano().getId());
        organoFirma.setExterno(organo.isExterno());

        List<OrganoReunionMiembro> listaMiembros =
                organoReunionMiembroDAO.getMiembroReunionByOrganoAndReunionId(organo.getId(), organo.isExterno(),
                        reunion.getId());
        List<OrganoReunionMiembro> listaMiembrosTodosLosOrganos =
                organoReunionMiembroDAO.getMiembrosByReunionId(reunion.getId());

        organoFirma.setMiembros(getAsistentesFirmaDesdeListaOrganoReunionMiembro(listaMiembros, listaMiembrosTodosLosOrganos));

        return organoFirma;
    }

    private List<MiembroFirma> getAsistentesFirmaDesdeListaOrganoReunionMiembro(
            List<OrganoReunionMiembro> listaMiembros, List<OrganoReunionMiembro> listaMiembrosTodosLosOrganos) {
        List<MiembroFirma> listaMiembroFirma = new ArrayList<>();

        for (OrganoReunionMiembro organoReunionMiembro : listaMiembros) {
            listaMiembroFirma.add(getAsistenteFirmaDesdeOrganoReunionMiembro(organoReunionMiembro));
        }

        for (MiembroFirma miembroFirma : listaMiembroFirma) {
            setDelegacionDeVotoToFirma(miembroFirma, listaMiembrosTodosLosOrganos);
        }

        return listaMiembroFirma;
    }

    private void setDelegacionDeVotoToFirma(MiembroFirma miembroFirma, List<OrganoReunionMiembro> miembros) {
        for (OrganoReunionMiembro miembro : miembros) {
            if (miembro.getDelegadoVotoId() != null && miembroFirma.getId()
                    .equals(miembro.getDelegadoVotoId().toString()) && !(miembroFirma.getId()
                    .equals(miembro.getMiembroId()))) {
                miembroFirma.addDelegacionDeVoto(miembro.getNombre());
            }
        }

        miembroFirma.buildNombresDelegacionesDeVoto();
    }

    private MiembroFirma getAsistenteFirmaDesdeOrganoReunionMiembro(OrganoReunionMiembro organoReunionMiembro) {
        MiembroFirma miembroFirma = new MiembroFirma();
        miembroFirma.setNombre(organoReunionMiembro.getNombre());
        miembroFirma.setEmail(organoReunionMiembro.getEmail());
        miembroFirma.setId(organoReunionMiembro.getMiembroId());
        miembroFirma.setSuplente(organoReunionMiembro.getSuplenteNombre());
        miembroFirma.setSuplenteId(organoReunionMiembro.getSuplenteId());
        miembroFirma.setDelegadoVotoId(organoReunionMiembro.getDelegadoVotoId());
        miembroFirma.setDelegadoVoto(organoReunionMiembro.getDelegadoVotoNombre());
        miembroFirma.setAsistenciaConfirmada(organoReunionMiembro.getAsistenciaConfirmada());
        miembroFirma.setAsistencia(organoReunionMiembro.getAsistencia());
        miembroFirma.setJustificaAusencia(organoReunionMiembro.getJustificaAusencia());

        Cargo cargo = new Cargo();
        cargo.setId(organoReunionMiembro.getCargoId());
        cargo.setCodigo(organoReunionMiembro.getCargoCodigo());
        cargo.setNombre(organoReunionMiembro.getCargoNombre());
        cargo.setNombreAlternativo(organoReunionMiembro.getCargoNombreAlternativo());
        cargo.setResponsableActa(organoReunionMiembro.getResponsableActa());

        miembroFirma.setCargo(cargo);

        return miembroFirma;
    }

    private List<MiembroTemplate> getAsistentesDesdeListaOrganoReunionMiembro(
            List<OrganoReunionMiembro> listaAsistentes, List<OrganoReunionMiembro> listaMiembros, boolean mainLanguage) {
        List<MiembroTemplate> listaMiembroTemplate = new ArrayList<>();

        for (OrganoReunionMiembro organoReunionMiembro : listaAsistentes) {
            listaMiembroTemplate.add(getAsistenteDesdeOrganoReunionMiembro(organoReunionMiembro, mainLanguage));
        }

        for (MiembroTemplate miembroTemplate : listaMiembroTemplate) {
            setDelegacionDeVotoToPlantilla(miembroTemplate, listaMiembros);
        }

        return listaMiembroTemplate;
    }

    private List<MiembroTemplate> getAusentesDesdeListaOrganoReunionMiembro(List<OrganoReunionMiembro> listaAsistentes,
                                                                            List<OrganoReunionMiembro> listaMiembros, boolean mainLanguage) {


        return listaMiembros.stream()
                .filter(a -> !listaAsistentes.contains(a))
                .map(a -> getAsistenteDesdeOrganoReunionMiembro(a, mainLanguage))
                .collect(Collectors.toList());
    }

    private List<MiembroTemplate> getAusentesJustificanAusenciaDesdeListaOrganoReunionMiembro(List<OrganoReunionMiembro> listaAsistentes,
                                                                                              List<OrganoReunionMiembro> listaMiembros, boolean mainLanguage) {
        return listaMiembros.stream()
                .filter(a -> !listaAsistentes.contains(a) && a.getJustificaAusencia())
                .map(a -> getAsistenteDesdeOrganoReunionMiembro(a, mainLanguage))
                .collect(Collectors.toList());
    }

    private void setDelegacionDeVotoToPlantilla(MiembroTemplate miembroTemplate, List<OrganoReunionMiembro> miembros) {
        for (OrganoReunionMiembro miembro : miembros) {
            if (miembro.getDelegadoVotoId() != null && miembroTemplate.getMiembroId()
                    .equals(miembro.getDelegadoVotoId().toString()) && !(miembroTemplate.getMiembroId()
                    .equals(miembro.getMiembroId()))) {
                miembroTemplate.addDelegacionDeVoto(miembro.getNombre());
            }
        }
    }

    private MiembroTemplate getAsistenteDesdeOrganoReunionMiembro(OrganoReunionMiembro organoReunionMiembro,
                                                                  boolean mainLanguage) {
        MiembroTemplate miembroTemplate = new MiembroTemplate();
        miembroTemplate.setNombre(organoReunionMiembro.getNombre());
        miembroTemplate.setEmail(organoReunionMiembro.getEmail());
        miembroTemplate.setCondicion(
                mainLanguage ? organoReunionMiembro.getCondicion() : organoReunionMiembro.getCondicionAlternativa());
        miembroTemplate.setId(organoReunionMiembro.getId().toString());
        miembroTemplate.setMiembroId(organoReunionMiembro.getMiembroId());
        miembroTemplate.setSuplente(organoReunionMiembro.getSuplenteNombre());
        miembroTemplate.setSuplenteId(organoReunionMiembro.getSuplenteId());
        miembroTemplate.setDelegadoVoto(organoReunionMiembro.getDelegadoVotoNombre());
        miembroTemplate.setDelegadoVotoId(organoReunionMiembro.getDelegadoVotoId());
        miembroTemplate.setAsistenciaConfirmada(organoReunionMiembro.getAsistenciaConfirmada());
        miembroTemplate.setHaConfirmado(organoReunionMiembro.isHaConfirmado());
        miembroTemplate.setJustificaAusencia(organoReunionMiembro.getJustificaAusencia());
        miembroTemplate.setAsistencia(organoReunionMiembro.getAsistencia());

        CargoTemplate cargo = new CargoTemplate();
        cargo.setId(organoReunionMiembro.getCargoId());
        cargo.setNombre(
                mainLanguage ? organoReunionMiembro.getCargoNombre() : organoReunionMiembro.getCargoNombreAlternativo());
        cargo.setCodigo(organoReunionMiembro.getCargoCodigo());
        cargo.setSuplente(
                organoReunionMiembro.getCargoSuplente() != null ? organoReunionMiembro.getCargoSuplente() : false);

        miembroTemplate.setCargo(cargo);

        return miembroTemplate;
    }

    private List<DocumentoTemplate> getReunionDocumentosTemplateDesdeDocumentos(
            List<ReunionDocumento> reunionDocumentos, boolean mainLanguage) {
        List<DocumentoTemplate> listaDocumento = new ArrayList<>();

        for (ReunionDocumento reunionDocumento : reunionDocumentos) {
            listaDocumento.add(getDocumentoTemplateDesdeReunionDocumento(reunionDocumento, mainLanguage));
        }
        return listaDocumento;
    }

    private DocumentoTemplate getDocumentoTemplateDesdeReunionDocumento(ReunionDocumento reunionDocumento,
                                                                        boolean mainLanguage) {
        DocumentoTemplate documento = new DocumentoTemplate();
        documento.setId(reunionDocumento.getId());
        documento.setHash(reunionDocumento.getHash());
        documento.setDescripcion(
                mainLanguage ? reunionDocumento.getDescripcion() : reunionDocumento.getDescripcionAlternativa());
        documento.setMimeType(reunionDocumento.getMimeType());
        documento.setFechaAdicion(reunionDocumento.getFechaAdicion());
        documento.setCreadorId(reunionDocumento.getCreadorId());
        documento.setNombreFichero(reunionDocumento.getNombreFichero());
        documento.setPublico(false);

        return documento;
    }

    private List<PuntoOrdenDiaTemplate> getPuntosOrdenDiaTemplateDesdePuntosOrdenDia(
            List<PuntoOrdenDiaMultinivel> puntosOrdenDia, Long reunionId, boolean mainLanguage, Boolean isForBuscador,
            Long connectedUserId) {
        List<PuntoOrdenDiaTemplate> listaPuntosOrdenDiaTemplate = new ArrayList<>();

        for (PuntoOrdenDiaMultinivel puntoOrdenDia : puntosOrdenDia) {
            listaPuntosOrdenDiaTemplate.add(
                    getPuntoOrdenDiaTemplateDesdePuntoOrdenDiaMultinivel(puntoOrdenDia, reunionId, mainLanguage,
                            isForBuscador, connectedUserId));
        }

        return listaPuntosOrdenDiaTemplate;
    }

    private PuntoOrdenDiaTemplate getPuntoOrdenDiaTemplateDesdePuntoOrdenDiaMultinivel(
            PuntoOrdenDiaMultinivel puntoOrdenDia, Long reunionId, boolean mainLanguage, Boolean isForBuscador,
            Long connectedUserId) {
        PuntoOrdenDiaTemplate puntoOrdenDiaTemplate = new PuntoOrdenDiaTemplate();
        puntoOrdenDiaTemplate.setId(puntoOrdenDia.getId());
        puntoOrdenDiaTemplate.setOrden(puntoOrdenDia.getOrden());
        puntoOrdenDiaTemplate.setAcuerdos(
                mainLanguage ? puntoOrdenDia.getAcuerdos() : puntoOrdenDia.getAcuerdosAlternativos());
        puntoOrdenDiaTemplate.setDeliberaciones(
                mainLanguage ? puntoOrdenDia.getDeliberaciones() : puntoOrdenDia.getDeliberacionesAlternativas());
        puntoOrdenDiaTemplate.setDescripcion(
                mainLanguage ? puntoOrdenDia.getDescripcion() : puntoOrdenDia.getDescripcionAlternativa());
        puntoOrdenDiaTemplate.setTitulo(
                mainLanguage ? puntoOrdenDia.getTitulo() : puntoOrdenDia.getTituloAlternativo());
        puntoOrdenDiaTemplate.setPublico(puntoOrdenDia.isPublico());
        puntoOrdenDiaTemplate.setUrlActa(
                mainLanguage ? puntoOrdenDia.getUrlActa() : puntoOrdenDia.getUrlActaAlternativa());
        puntoOrdenDiaTemplate.setUrlActaAnterior(
                mainLanguage ? puntoOrdenDia.getUrlActaAnterior() : puntoOrdenDia.getUrlActaAnteriorAlt());

        List<PuntoOrdenDiaDocumento> documentos =
                puntoOrdenDiaDocumentoDAO.getDatosDocumentosByPuntoOrdenDiaId(puntoOrdenDia.getId(), true);
        puntoOrdenDiaTemplate.setDocumentos(
                getDocumentosTemplateDesdePuntosOrdenDiaDocumentos(documentos, mainLanguage));

        List<PuntoOrdenDiaAcuerdo> acuerdos =
                puntoOrdenDiaAcuerdoDAO.getDatosAcuerdosByPuntoOrdenDiaId(puntoOrdenDia.getId());
        puntoOrdenDiaTemplate.setDocumentosAcuerdos(
                getAcuerdosTemplateDesdePuntosOrdenDiaAcuerdos(acuerdos, mainLanguage));

        if (!isForBuscador) {
            List<PuntoOrdenDiaDescriptor> descriptores =
                    puntoOrdenDiaDescriptorDAO.getDescriptoresOrdenDia(puntoOrdenDia.getId());
            puntoOrdenDiaTemplate.setDescriptores(
                    getDescriptoresTemplateDesdePuntosOrdenDiaAcuerdos(descriptores, mainLanguage));

            List<PuntoOrdenDiaComentario> comentarios =
                    puntoOrdenDiaComentarioDAO.getComentariosByPuntoId(puntoOrdenDia.getId());
            puntoOrdenDiaTemplate.setComentarios(
                    getComentariosTemplateDesdePUntoOrdenDiaComentarios(comentarios, reunionId, connectedUserId));
        }

        if (puntoOrdenDia.getPuntosInferiores() != null) {
            List<PuntoOrdenDiaTemplate> subpuntosTemplate = new ArrayList<>();

            for (PuntoOrdenDiaMultinivel puntoOrdenDiaMultinivel : getsubpuntosOrdenDiaOrdenados(puntoOrdenDia)) {
                subpuntosTemplate.add(
                        getPuntoOrdenDiaTemplateDesdePuntoOrdenDiaMultinivel(puntoOrdenDiaMultinivel, reunionId,
                                mainLanguage, false, connectedUserId));
            }
            puntoOrdenDiaTemplate.setSubpuntos(subpuntosTemplate);
        }

        puntoOrdenDiaTemplate.setTipoVoto(puntoOrdenDia.getTipoVoto());
        puntoOrdenDiaTemplate.setVotableEnContra(puntoOrdenDia.getVotableEnContra());
        puntoOrdenDiaTemplate.setTipoRecuentoVoto(puntoOrdenDia.getTipoRecuentoVoto());
        puntoOrdenDiaTemplate.setTipoProcedimientoVotacion(puntoOrdenDia.getTipoProcedimientoVotacion());
        puntoOrdenDiaTemplate.setVotacionAbierta(puntoOrdenDia.getVotacionAbierta());
        puntoOrdenDiaTemplate.setAbrible(puntoOrdenDia.isVotacionAbrible());
        puntoOrdenDiaTemplate.setTipoVotacion(puntoOrdenDia.getTipoVotacion());

        return puntoOrdenDiaTemplate;
    }

    private List<ComentarioPuntoOrdenDia> getComentariosTemplateDesdePUntoOrdenDiaComentarios(
            List<PuntoOrdenDiaComentario> comentarios, Long reunionId, Long connectedUserId) {
        List<ComentarioPuntoOrdenDia> comentariosTemplate = new ArrayList<>();
        for (PuntoOrdenDiaComentario puntoOrdenDiaComentario : comentarios) {
            ComentarioPuntoOrdenDia comentarioPuntoOrdenDia =
                    ComentarioPuntoOrdenDia.dtoToModel(puntoOrdenDiaComentario);
            comentarioPuntoOrdenDia.setPermitirBorrado(
                    puntoOrdenDiaComentarioService.isPermiteBorradoComentarioPunto(reunionId, comentarioPuntoOrdenDia,
                            connectedUserId));
            if (puntoOrdenDiaComentario.getRespuestas() != null && !puntoOrdenDiaComentario.getRespuestas().isEmpty()) {
                comentarioPuntoOrdenDia.setRespuestas(getComentariosTemplateDesdePUntoOrdenDiaComentarios(
                        new ArrayList<>(puntoOrdenDiaComentario.getRespuestas()), reunionId, connectedUserId));
            }
            comentariosTemplate.add(comentarioPuntoOrdenDia);
        }
        return comentariosTemplate;
    }

    private List<DocumentoTemplate> getDocumentosTemplateDesdePuntosOrdenDiaDocumentos(
            List<PuntoOrdenDiaDocumento> documentos, boolean mainLanguage) {
        List<DocumentoTemplate> listaDocumento = new ArrayList<>();

        for (PuntoOrdenDiaDocumento puntoOrdenDiaDocumento : documentos) {
            listaDocumento.add(
                    getDocumentoTemplateDesdePuntoOrdenDiaDocumentable(puntoOrdenDiaDocumento, mainLanguage));
        }

        return listaDocumento;
    }

    private List<DocumentoTemplate> getAcuerdosTemplateDesdePuntosOrdenDiaAcuerdos(List<PuntoOrdenDiaAcuerdo> acuerdos,
                                                                                   boolean mainLanguage) {
        List<DocumentoTemplate> listaDocumento = new ArrayList<>();

        for (PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo : acuerdos) {
            listaDocumento.add(getDocumentoTemplateDesdePuntoOrdenDiaDocumentable(puntoOrdenDiaAcuerdo, mainLanguage));
        }

        return listaDocumento;
    }

    private List<DescriptorTemplate> getDescriptoresTemplateDesdePuntosOrdenDiaAcuerdos(
            List<PuntoOrdenDiaDescriptor> descriptores, boolean mainLanguage) {
        List<DescriptorTemplate> listaDesciptores = new ArrayList<>();

        for (PuntoOrdenDiaDescriptor descriptor : descriptores) {
            listaDesciptores.add(getDescriptorTemplateDesdePuntoOrdenDiaAcuerdo(descriptor, mainLanguage));
        }

        return listaDesciptores;
    }

    private DocumentoTemplate getDocumentoTemplateDesdePuntoOrdenDiaDocumentable(Documentable puntoOrdenDiaDocumento,
                                                                                 boolean mainLanguage) {
        DocumentoTemplate documento = new DocumentoTemplate();
        documento.setId(puntoOrdenDiaDocumento.getId());
        documento.setHash(puntoOrdenDiaDocumento.getHash());
        documento.setDescripcion(
                mainLanguage ? puntoOrdenDiaDocumento.getDescripcion() : puntoOrdenDiaDocumento.getDescripcionAlternativa());
        documento.setMimeType(puntoOrdenDiaDocumento.getMimeType());
        documento.setFechaAdicion(puntoOrdenDiaDocumento.getFechaAdicion());
        documento.setCreadorId(puntoOrdenDiaDocumento.getCreadorId());
        documento.setNombreFichero(puntoOrdenDiaDocumento.getNombreFichero());
        documento.setPublico(puntoOrdenDiaDocumento.getPublico());
        documento.setOrden(puntoOrdenDiaDocumento.getOrden());


        return documento;
    }

    private DescriptorTemplate getDescriptorTemplateDesdePuntoOrdenDiaAcuerdo(PuntoOrdenDiaDescriptor descriptor,
                                                                              boolean mainLanguage) {
        DescriptorTemplate descriptorTemplate = new DescriptorTemplate();

        descriptorTemplate.setId(descriptor.getId());
        descriptorTemplate.setPuntoOrdenDiaId(descriptor.getPuntoOrdenDia().getId());
        descriptorTemplate.setClaveId(descriptor.getClave().getId());
        descriptorTemplate.setDescriptorId(descriptor.getClave().getDescriptor().getId());
        descriptorTemplate.setDescriptorNombre(
                mainLanguage ? descriptor.getClave().getDescriptor().getDescriptor() : descriptor.getClave()
                        .getDescriptor()
                        .getDescriptorAlternativo());
        descriptorTemplate.setDescriptorDescripcion(
                mainLanguage ? descriptor.getClave().getDescriptor().getDescripcion() : descriptor.getClave()
                        .getDescriptor()
                        .getDescripcionAlternativa());
        descriptorTemplate.setClaveNombre(
                mainLanguage ? descriptor.getClave().getClave() : descriptor.getClave().getClaveAlternativa());

        return descriptorTemplate;
    }

    public Reunion getReunionConOrganosById(Long reunionId, Long connectedUserId) {
        return reunionDAO.getReunionConOrganosById(reunionId);
    }

    public ReunionesPermisosWrapper getReunionesAbiertasAccesiblesByPersonaId(Long connectedUserId, Integer startSearch, Integer numResults)
            throws RolesPersonaExternaException {
        ReunionesPermisosWrapper wrapper = new ReunionesPermisosWrapper();

        if (personaService.isAdmin(personaService.getRolesFromPersonaId(connectedUserId))) {
            wrapper.setNumeroReuniones(reunionDAO.getTodasReunionesAbiertasAccesiblesDePersonaIdCount(connectedUserId, true));
            wrapper.setReuniones(reunionDAO.getTodasReunionesAbiertasAccesiblesDePersonaIdList(connectedUserId, true, startSearch, numResults));
        } else {
            wrapper.setNumeroReuniones(reunionDAO.getTodasReunionesAbiertasAccesiblesDePersonaIdCount(connectedUserId, false));
            wrapper.setReuniones(reunionDAO.getTodasReunionesAbiertasAccesiblesDePersonaIdList(connectedUserId, false, startSearch, numResults));
        }

        return wrapper;
    }

    public ReunionesPermisosWrapper getReunionesCerradasAccesiblesByPersonaIdAndOrgano(Long connectedUserId, Long organoId, Boolean organoExterno, Integer startSearch, Integer numResults) {
        ReunionesPermisosWrapper wrapper = new ReunionesPermisosWrapper();

        wrapper.setNumeroReuniones(reunionDAO.getTodasReunionesCerradasAccesiblesDePersonaIdAndOrganoCount(connectedUserId, organoId, organoExterno));
        wrapper.setReuniones(reunionDAO.getTodasReunionesCerradasAccesiblesDePersonaIdAndOrganoList(connectedUserId, organoId, organoExterno, startSearch, numResults));

        return wrapper;
    }

    public List<OrganosPermisosWrapper> getOrganosReunionesCerradasAccesiblesByPersonaId(Long connectedUserId/*, Long organoId, Boolean organoExterno, Integer pagina*/)
            throws RolesPersonaExternaException, OrganosExternosException {

        List<OrganosPermisosWrapper> organosWrapper = reunionDAO.getTodosOrganosReunionesCerradasAccesiblesDePersonaId(connectedUserId);
        List<Organo> organosExternos = organoService.getOrganosExternos();
        Organo organoAux;

        for (OrganosPermisosWrapper organo : organosWrapper) {

            if (!organo.isExterno()) {
                organoAux = organoService.getOrganoById(organo.getId());
            } else {
                organoAux = getOrganoExternoById(organo.getId().toString(), organosExternos);
            }

            if (organoAux != null) {
                organo.setNombre(organoAux.getNombre());
                organo.setNombreAlt(organoAux.getNombreAlternativo());
            }
        }

        return organosWrapper;
    }

    public void compruebaReunionNoCompletada(Long reunionId)
            throws ReunionYaCompletadaException {
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (reunion.getCompletada() != null && reunion.getCompletada()) {
            throw new ReunionYaCompletadaException();
        }
    }

    public void compruebaReunionAdmiteSuplencia(Long reunionId)
            throws ReunionNoAdmiteSuplenciaException {
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (reunion.isAdmiteSuplencia() != null && !reunion.isAdmiteSuplencia()) {
            throw new ReunionNoAdmiteSuplenciaException();
        }
    }

    public boolean isReunionAdmiteDelegacionVoto(Long reunionId) {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        if (reunion == null || reunion.isAdmiteDelegacionVoto() != null && !reunion.isAdmiteDelegacionVoto()) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean compruebaDelegacionVotoMultipleEnOrgano(Long delegadoVotoId, Long organoId, Boolean ordinario, Long reunionId) {
        if (organoDAO.getOrganoAdmiteVotoMultipleByDelegadoVotoId(organoId, ordinario)) return true;
        else if (!organoReunionMiembroDAO.haSidoAsignadoDelegadoEnReunion(delegadoVotoId, reunionId)) return true;
        else return false;
    }

    @Transactional
    public void enviarBorrador(Long reunionId, List<String> emails) throws Exception {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);
        avisosReunion.enviaBorrador(reunion, emails);
    }

    @Transactional
    public String enviarConvocatoria(Long reunionId, User user, String textoConvocatoria, String textoConvocatoriaAlternativo)
            throws Exception {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);

        String error = checkAllFeaturesOfReunion(reunion);
        if (error != null) return error;

        avisosReunion.enviaAvisoNuevaReunion(reunion, textoConvocatoria, textoConvocatoriaAlternativo, "", "");

        if (!reunion.getAvisoPrimeraReunion()) {
            Persona personaConvocante = personaService.getPersonaFromDirectoryByPersonaId(user.getId());

            reunion.setAvisoPrimeraReunion(true);
            reunion.setAvisoPrimeraReunionUser(user.getName());
            reunion.setAvisoPrimeraReunionFecha(new Date());
            reunion.setAvisoPrimeraReunionUserEmail(personaConvocante.getEmail());

            if (personalizationConfig.habilitarVotaciones) {
                if (reunion.isVotacionTelematica()) {
                    puntoOrdenDiaService.iniciaFechaVotacionPuntosAbreviados(reunion);
                }
            }
            reunionDAO.update(reunion);
        }

        return null;
    }

    public String enviarEmail(Long reunionId, User user, String textoConvocatoria)
            throws Exception {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);

        if (reunion.noContieneMiembros()) return "appI18N.reuniones.reunionSinMiembros";

        avisosReunion.enviaEmailTextoLibre(reunion, textoConvocatoria);

        return null;
    }

    public String checkReunionToClose(Long reunionId) {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);

        String error = checkAllFeaturesOfReunion(reunion);
        if (error != null) return error;

        return null;
    }

    private String checkAllFeaturesOfReunion(Reunion reunion) {
        if (!isTodosOrganosConvocarSinOrdenDiaActivado(reunion)) {
            if (reunion.getReunionPuntosOrdenDia().size() == 0) return "appI18N.reuniones.reunionSinOrdenDia";
        }

        if (reunion.noContieneMiembros()) return "appI18N.reuniones.reunionSinMiembros";
        return null;
    }

    private boolean isTodosOrganosConvocarSinOrdenDiaActivado(Reunion reunion) {
        for (OrganoReunion organoReunion : reunion.getReunionOrganos()) {
            if (!organoService.isConvocarSinOrdenDiaActivo(organoReunion.getOrganoId(), organoReunion.isExterno()))
                return false;
        }

        return true;
    }

    public List<TipoOrganoLocal> getTiposOrganosConReunionesPublicas() {
        return tipoOrganoDAO.getTiposOrganoConReunionesPublicas();
    }

    public List<Organo> getOrganosConReunionesPublicas(Long tipoOrganoId, Integer anyo) {
        QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;
        List<Tuple> organosConReunionesPublicas = organoDAO.getOrganosConReunionesPublicas(tipoOrganoId, anyo);
        return organosConReunionesPublicas.stream().map(o -> {
            String id = o.get(qOrganoReunion.organoId);
            String nombre = o.get(qOrganoReunion.organoNombre);
            String nombreAlternativo = o.get(qOrganoReunion.organoNombreAlternativo);
            return new Organo(id, nombre, nombreAlternativo, null);
        }).collect(Collectors.toList());
    }

    public List<Descriptor> getDescriptoresConReunionesPublicas(Integer anyo) {
        List<Long> idsReuniones = reunionDAO.getIdsReunionesPublicas(anyo);
        List<Long> distinctIdsReuniones = idsReuniones.stream().distinct().collect(Collectors.toList());
        return descriptorDAO.getDescriptoresConReunionesPublicas(distinctIdsReuniones, anyo)
                .stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public List<Integer> getAnyosConReunionesPublicas() {
        return reunionDAO.getAnyosConReunionesPublicas();
    }

    public List<Clave> getClavesConReunionesPublicas(Long descriptorId, Integer anyo) {
        List<Long> idsReuniones = reunionDAO.getIdsReunionesPublicas(anyo);
        return claveDAO.getClavesConReunionesPublicas(idsReuniones, descriptorId, anyo);
    }

    @Transactional
    public BuscadorReunionesWrapper getReunionesPublicas(AcuerdosSearch acuerdosSearch) {
        return reunionDAO.getReunionesPublicasPaginated(acuerdosSearch.getTipoOrganoId(), acuerdosSearch.getOrganoId(),
                acuerdosSearch.getDescriptorId(), acuerdosSearch.getClaveId(), acuerdosSearch.getAnyo(),
                acuerdosSearch.getfInicio(), acuerdosSearch.getfFin(), acuerdosSearch.getTexto(),
                acuerdosSearch.getIdiomaAlternativo(), acuerdosSearch.getStartSearch(), acuerdosSearch.getNumResults());
    }

    public Reunion getReunionByIdAndEditorId(Long reunionId, Long connectedUserId)
            throws ReunionNoDisponibleException, RolesPersonaExternaException {
        List<String> rolesFromPersonaId = personaService.getRolesFromPersonaId(connectedUserId);
        if (!personaService.isAdmin(rolesFromPersonaId)) {
            ReunionEditor reunionEditor = reunionDAO.getReunionByIdAndEditorId(reunionId, connectedUserId);

            if (reunionEditor == null) {
                throw new ReunionNoDisponibleException();
            }
        }

        return reunionDAO.getReunionById(reunionId);
    }

    public List<OrganoReunion> getOrganosReunionByReunionId(Long reunionId) {
        return reunionDAO.getOrganosReunionByReunionId(reunionId);
    }

    public List<ReunionInvitado> getInvitadosReunionByReunionId(Long reunionId) {
        return reunionInvitadoDAO.getInvitadosByReunionId(reunionId);
    }

    public String getNombreAsistente(Long reunionId, Long connectedUserId) {
        return reunionDAO.getNombreAsistente(reunionId, connectedUserId);
    }

    public Reunion getReunionById(Long reunionId) {
        return reunionDAO.getReunionById(reunionId);
    }

    public ReunionHojaFirmas getHojaFirmasByReunionId(Long reunionId) {
        return reunionDAO.getHojaFirmasByReunionId(reunionId);
    }

    @Transactional
    public ReunionHojaFirmas subirHojaFirmasByReunionId(Long reunionId, ReunionHojaFirmas hojaFirmas) {
        reunionDAO.deleteHojaFirmasAnterior(reunionId);
        hojaFirmas.setReunion(reunionDAO.getReunionById(reunionId));
        ReunionHojaFirmas hojaFirmasActualizada = reunionDAO.update(hojaFirmas);
        return hojaFirmasActualizada;
    }

    public List<ReunionEditor> getReunionesByAdmin(Boolean completada, Long tipoOrganoId,
                                                   String organoId, Boolean externo, String query, Integer start, Integer limit) {
        return reunionDAO.getReunionesByAdminList(completada, tipoOrganoId, organoId, externo, query, start, limit);
    }

    public Reunion getUltimaReunionByReunionIdOrganos(Long reunionId, List<String> organosReunionIds) {
        return reunionDAO.getUltimaReunionByReunionIdOrganos(reunionId, organosReunionIds);
    }

    public void addPuntoRevisionUltimoActa(Reunion reunionActual, Long connectedUserId)
            throws TipoRecuentoVotoNoExisteException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            RecuentoVotoInvalidoParaPuntoNoVotableException {
        List<String> idsOrganos = getIdsOrganosReunion(reunionActual.getId());
        Reunion reunionAnterior = getUltimaReunionByReunionIdOrganos(reunionActual.getId(), idsOrganos);

        if (reunionAnterior != null) {
            PuntoOrdenDia puntoOrdenDia = creaPuntoOrdenDiaRevisionActa(reunionAnterior, reunionActual);
            puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDia, reunionActual);
        }
    }

    private List<String> getIdsOrganosReunion(Long reunionId) {
        List<OrganoReunion> organosReunionByReunionId = getOrganosReunionByReunionId(reunionId);
        return organosReunionByReunionId.stream().map(o -> o.getOrganoId()).collect(Collectors.toList());
    }

    private PuntoOrdenDia creaPuntoOrdenDiaRevisionActa(Reunion reunionAnterior, Reunion reunionActual) {
        PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDia();

        String url = reunionAnterior.getUrlActa() != null ? reunionAnterior.getUrlActa() : getUrlActaAnteriorDefault(
                reunionAnterior);

        puntoOrdenDia.setTitulo(getTituloByLanguage(languageConfig.mainLanguage));
        puntoOrdenDia.setUrlActaAnterior(url);
        puntoOrdenDia.setPublico(false);

        if (languageConfig.alternativeLanguage != null) {
            String urlAlternativa =
                    getUrlActaAnteriorDefault(reunionAnterior) + "?lang=" + languageConfig.alternativeLanguage;
            url =
                    reunionAnterior.getUrlActaAlternativa() != null ? reunionAnterior.getUrlActaAlternativa() : urlAlternativa;
            puntoOrdenDia.setTituloAlternativo(getTituloByLanguage(languageConfig.alternativeLanguage));
            puntoOrdenDia.setUrlActaAnteriorAlt(url);
        }
        puntoOrdenDia.setReunion(reunionActual);

        puntoOrdenDia.setTipoProcedimientoVotacion(TipoProcedimientoVotacionEnum.NO_VOTABLE.name());
        puntoOrdenDia.setTipoRecuentoVoto(TipoRecuentoVotoEnum.NO_VOTABLE.name());
        puntoOrdenDia.setVotableEnContra(false);
        puntoOrdenDia.setVotacionCerradaManualmente(false);

        return puntoOrdenDia;
    }

    private String getUrlActaAnteriorDefault(Reunion reunionAnterior) {
        return "/goc/rest/actas/" + reunionAnterior.getId();
    }

    private String getTituloByLanguage(String language) {
        ResourceBundle resourceBundleAlternativeLanguage =
                ResourceBundle.getBundle("i18n", Locale.forLanguageTag(language));
        return resourceBundleAlternativeLanguage.getString("puntoOrdenDia.revisarActaAnterior");
    }

    public String informarOrgano(Long reunionId, Long connectedUserId, String organoId, Boolean enviarOrdenDia,
                                 Boolean organoExterno, String textoInformar, String textoInformarAlternativo)
            throws Exception {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);
        String error = validarDatosParaInformarOrgano(reunion, connectedUserId, organoId, organoExterno);
        if (error != null) return error;

        List<Miembro> miembrosAInformar =
                obtenerMiembrosOrganoParaInformar(reunion, organoId, connectedUserId, organoExterno);

        List<Organo> organos = organoService.getOrganosByReunionId(reunionId, connectedUserId);
        avisosReunion.informarOrgano(reunion, organos, miembrosAInformar, enviarOrdenDia, textoInformar, textoInformarAlternativo);
        return null;
    }

    protected List<Miembro> obtenerMiembrosOrganoParaInformar(Reunion reunion, String organoId, Long connectedUserId,
                                                              Boolean organoExterno)
            throws MiembrosExternosException {

        List<Organo> organosByReunion = organoService.getOrganosByReunionId(reunion.getId(), connectedUserId);
        List<Miembro> miembrosConvocados = new ArrayList<>();
        for (Organo organo : organosByReunion) {
            if (organo.isExterno()) {
                miembrosConvocados.addAll(miembroService.getMiembrosExternos(organo.getId(), connectedUserId));
            } else {
                miembrosConvocados.addAll(
                        miembroService.getMiembrosLocales(Long.parseLong(organo.getId()), connectedUserId));
            }
        }


        List<Miembro> miembrosOrganoAInformar = new ArrayList<>();
        if (organoExterno) {
            miembrosOrganoAInformar.addAll(miembroService.getMiembrosExternos(organoId, connectedUserId));
        } else {
            miembrosOrganoAInformar = miembroService.getMiembrosLocales(Long.valueOf(organoId), connectedUserId);
        }
        List<Miembro> miembrosParaInformar = new ArrayList<>();
        for (Miembro miembro : miembrosOrganoAInformar) {
            if (!containsPersonaId(miembrosConvocados, miembro.getPersonaId())) {
                miembrosParaInformar.add(miembro);
            }
        }

        return miembrosParaInformar;
    }

    public String validarDatosParaInformarOrgano(Reunion reunion, Long connectedUserId, String organoId,
                                                 Boolean organoExterno) {
        String error = validarPermisosUsuarioParaInformarAOrgano(reunion, connectedUserId, organoId, organoExterno);
        if (error != null) return error;
        error = validarDatosReunion(reunion);
        if (error != null) return error;

        return null;
    }

    private String validarDatosReunion(Reunion reunion) {
        return reunion.getAvisoPrimeraReunion() ? null : "appI18N.reuniones.reunionNoConvocada";
    }

    protected String validarPermisosUsuarioParaInformarAOrgano(Reunion reunion, Long connectedUserId, String organoId,
                                                               Boolean organoExterno) {
        List<Organo> organosByReunion = organoService.getOrganosByReunionId(reunion.getId(), connectedUserId);
        List<OrganoAutorizado> autorizadosOrganoAInformar = organoService.getAutorizados(organoId, organoExterno);
        boolean esAutorizadoOrganoAInformar = autorizadosOrganoAInformar.stream()
                .filter(a -> a.getPersonaId().equals(connectedUserId))
                .findFirst()
                .isPresent();
        boolean esAutorizadoOrganosConvocados = false;
        for (Organo organo : organosByReunion) {
            List<OrganoAutorizado> autorizadosOrganoConvocante =
                    organoService.getAutorizados(organo.getId(), organo.isExterno());

            esAutorizadoOrganosConvocados = autorizadosOrganoConvocante.stream()
                    .filter(a -> a.getPersonaId().equals(connectedUserId))
                    .findFirst()
                    .isPresent();
            if (esAutorizadoOrganosConvocados) break;
        }

        return (esAutorizadoOrganoAInformar && esAutorizadoOrganosConvocados) ? null : "appI18N.reuniones.permisosInsuficientesInformarOrgano";
    }

    @Transactional
    public void addUrlActa(Long reunionId, String urlActa) {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        reunion.setUrlActa(urlActa);
        reunionDAO.update(reunion);
    }

    @Transactional
    public void addUrlActaAlternativa(Long reunionId, String urlActaAlternativa) {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        reunion.setUrlActaAlternativa(urlActaAlternativa);
        reunionDAO.update(reunion);
    }

    @Transactional(rollbackFor = {PersonasExternasException.class, NoEsTelematicaExceptionConVotacion.class})
    public Reunion duplicaReunion(Reunion reunion, Long connectedUserId)
            throws PersonasExternasException, NoAdmiteVotacionYAceptaCambioDeVotoException,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException {
        reunion = addReunion(reunion, connectedUserId);
        return reunion;
    }

    @Transactional(rollbackFor = {PersonasExternasException.class})
    public void duplicaReunionPuntos(Long reunionCopiadaId, Long reunionOriginalId, Long connectedUserId)
            throws PersonasExternasException, TipoRecuentoVotoNoExisteException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException, RecuentoVotoInvalidoParaPuntoNoVotableException {
        duplicaPuntosOrdenDia(reunionCopiadaId, reunionOriginalId, connectedUserId);
    }

    private void duplicaPuntosOrdenDia(Long ReunionCopiadaId, Long reunionOriginalId, Long connectedUserId)
            throws TipoRecuentoVotoNoExisteException, RecuentoVotoInvalidoParaPuntoNoVotableException,
            ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        List<PuntoOrdenDia> puntosReunionOriginal =
                puntoOrdenDiaService.getPuntosByReunionId(reunionOriginalId, connectedUserId);
        Reunion reunionCopiada = reunionDAO.getReunionById(ReunionCopiadaId);

        duplicaPuntosOrdenDia(reunionCopiada, puntosReunionOriginal);
    }

    @Transactional(rollbackFor = TipoRecuentoVotoNoExisteException.class)
    public void duplicaPuntosOrdenDia(Reunion reunion, List<PuntoOrdenDia> puntosReunionOriginal)
            throws TipoRecuentoVotoNoExisteException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException,
            RecuentoVotoInvalidoParaPuntoNoVotableException {
        if (puntosReunionOriginal != null) {
            for (PuntoOrdenDia puntoOrdenDia : puntosReunionOriginal) {
                if (puntoOrdenDia.getUrlActaAnterior() == null) {
                    PuntoOrdenDia nuevoPunto = puntoOrdenDiaDAO.detach(puntoOrdenDia);
                    nuevoPunto.setReunion(reunion);
                    nuevoPunto.cleanBeforeDuplicate();

                    puntoOrdenDiaService.addPuntoOrdenDiaSinCambiarOrden(nuevoPunto, reunion);
                }
            }
        }
    }

    public void eliminarMiembrosConCargoSuplente(ReunionTemplate reunionTemplate) {
        for (OrganoTemplate organoTemplate : reunionTemplate.getOrganos()) {
            List<MiembroTemplate> asistentesConCargoNoSuplentes = organoTemplate.getAsistentes()
                    .stream()
                    .filter(a -> !a.getCargo().getSuplente())
                    .collect(Collectors.toList());
            organoTemplate.setAsistentes(asistentesConCargoNoSuplentes);
        }

    }

    public void compruebaCargosFirmantesValidos(Long reunionId)
            throws FaltaPresidenteYSecretarioException, FaltaPresidenteException, FaltaSecretarioException {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);
        reunion.hasMiembrosPresidenteYSecretario();
    }

    @Transactional
    public void reabrirReunion(Long reunionId, String motivoReapertura, User connectedUser) {
        Reunion reunionById = reunionDAO.getReunionById(reunionId);
        if (reunionById != null) {
            reunionById.setCompletada(false);
            reunionById.setReabierta(true);
            reunionById.setUrlActaAntesReapertura(reunionById.getUrlActa());
            reunionById.setUrlActaAlternativaAntesReapertura(reunionById.getUrlActaAlternativa());
            reunionById.setUrlActa(null);
            reunionById.setUrlActaAlternativa(null);

            reunionDAO.reseteaEstadoEditadoPuntosOrdenDiaByreunionId(reunionId, false);
            reunionDAO.update(reunionById);
            reunionDAO.eliminaResponsableActa(reunionId);

            ReunionDiligencia reunionDiligencia = new ReunionDiligencia();
            reunionDiligencia.setReunionId(reunionId);
            reunionDiligencia.setFechaReapertura(new Date());
            reunionDiligencia.setMotivoReapertura(motivoReapertura);
            reunionDiligencia.setUserId(connectedUser.getId());
            reunionDiligencia.setMotivoReapertura(motivoReapertura);

            reunionDiligenciaDAO.insert(reunionDiligencia);
        }
    }

    public ReunionAcuerdos getAcuerdosByReunionId(Long reunionId) {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        ReunionAcuerdos reunionacuerdos = new ReunionAcuerdos();
        reunionacuerdos.setAcuerdos(reunion.getAcuerdos());
        reunionacuerdos.setResponsableActa(reunion.getMiembroResponsableActa());

        return reunionacuerdos;
    }

    public Boolean isReabierta(Long reunionId) {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        return reunion.isReabierta();
    }

    @Transactional
    public void modificaVisibilidadReunion(Long reunionId, Boolean publica) {
        if (publica != null && reunionId != null) {
            reunionDAO.updatePublica(reunionId, publica);
        }
    }

    @Transactional
    public void eliminarDelegacionesExistentesYNotificar(Long reunionId)
            throws Exception {
        List<String> mailPersonasReceptorasVotoDelegadoANotificar =
                organoReunionMiembroDAO.getMailPersonaseceptorasVotoDelegado(reunionId);
        List<OrganoReunionMiembro> personasVotoDelegadoANotificar =
                organoReunionMiembroDAO.getMailPersonasVotoDelegado(reunionId);

        organoReunionMiembroDAO.eliminarDelegacionesExistentes(reunionId);
        for (OrganoReunionMiembro personaVotoDelegado : personasVotoDelegadoANotificar) {
            avisosReunion.enviaAvisoDelegacionVoto(reunionId, personaVotoDelegado.getDelegadoVotoEmail(),
                    personaVotoDelegado.getNombre(), personaVotoDelegado.getCargoNombre(), false);
        }
        avisosReunion.notificarPersonasQueYahabianDelegado(mailPersonasReceptorasVotoDelegadoANotificar, reunionId);
    }

    public List<OrganoReunionMiembro> personasVotoDelegado(Long reunionId) {
        return organoReunionMiembroDAO.getNombrePersonasVotoDelegado(reunionId);
    }

    public boolean compruebaReunionConvocada(Long reunionId) {
        return reunionDAO.getReunionById(reunionId).getAvisoPrimeraReunion();
    }

    @Transactional
    public List<ReunionInvitado> existenNuevosInvitados(Long reunionId, List<ReunionInvitado> reunionInvitadosTodos)
            throws Exception {

        List<Persona> reunionInvitadosNotificados = reunionDAO.getInvitadosByReunionId(reunionId);

        List<ReunionInvitado> invitadosANoNotificar = new ArrayList<>();
        for (Persona reunionInvitadoNotificadosAnteriormente : reunionInvitadosNotificados) {
            for (ReunionInvitado reunionInvitadoEnUpdate : reunionInvitadosTodos) {
                if (reunionInvitadoNotificadosAnteriormente.getId().equals(reunionInvitadoEnUpdate.getPersonaId())) {
                    invitadosANoNotificar.add(reunionInvitadoEnUpdate);
                }
            }
        }
        reunionInvitadosTodos.removeAll(invitadosANoNotificar);

        return reunionInvitadosTodos;
    }

    @Transactional
    public void notificaNuevosInvitados(Long reunionId, List<ReunionInvitado> reunionInvitadosTodos)
            throws Exception {
        Reunion reunionById = reunionDAO.getReunionById(reunionId);
        List<String> emailsInvitadosNoNotificados = reunionInvitadosTodos.stream()
                .map(reunionInvitado -> reunionInvitado.getPersonaEmail())
                .collect(Collectors.toList());
        avisosReunion.enviaAvisonuevaReunionNuevosInvitados(reunionById, emailsInvitadosNoNotificados);
    }

    @Transactional
    public void deleteHojaFirmasById(Long hojaFirmasId) {
        reunionHojaFirmasDAO.deleteHojaFirmasById(hojaFirmasId);
    }

    public void checkIfActaProvisionalActivadaByReunion(ReunionTemplate reunionTemplate, Long connectedUserId)
            throws ActaProvisionalDesactivadaException {
        if (!reunionTemplate.isCompletada()) {
            List<OrganoTemplate> organos = reunionTemplate.getOrganos();
            if (organos != null && !organos.isEmpty()) {
                for (OrganoTemplate organo : organos) {
                    if (!organo.getActaProvisionalActiva()) {
                        throw new ActaProvisionalDesactivadaException();
                    }
                }
            }
        }
    }

    @Transactional
    public void updateMiembrosYCargosReunion(Long reunionId, Long connectedUserId)
            throws OrganosExternosException, MiembrosExternosException {
        List<OrganoReunion> organosReunionByReunionId = getOrganosReunionByReunionId(reunionId);
        for (OrganoReunion organoReunion : organosReunionByReunionId) {
            if (organoReunion.getExterno()) {
                actualizaMiembrosOrganoExterno(organoReunion, reunionId, connectedUserId);
            } else if (!organoReunion.getExterno()) {
                actualizaMiembrosOrganoNoExterno(organoReunion, reunionId, connectedUserId);
            }
        }
    }

    private void actualizaMiembrosOrganoNoExterno(OrganoReunion organoReunion, Long reunionId, Long connectedUserId) {
        if (organoReunion != null) {
            List<Miembro> miembrosActualizados =
                    miembroService.getMiembrosLocales(Long.parseLong(organoReunion.getOrganoId()), connectedUserId);
            if (miembrosActualizados != null) {
                updateListOrganoReunionMiembroFromMiembro(organoReunion, reunionId, miembrosActualizados, false);
            }
        }
    }

    @Transactional
    public void actualizaMiembrosOrganoExterno(OrganoReunion organoReunion, Long reunionId, Long connectedUserId)
            throws MiembrosExternosException {
        if (organoReunion != null) {
            List<Miembro> miembrosExternos =
                    miembroService.getMiembrosExternos(organoReunion.getOrganoId().toString(), connectedUserId);
            if (miembrosExternos != null) {
                updateListOrganoReunionMiembroFromMiembro(organoReunion, reunionId, miembrosExternos, true);
            } else {
                throw new MiembrosExternosException();
            }
        }
    }

    @Transactional
    public void updateListOrganoReunionMiembroFromMiembro(OrganoReunion organoReunion, Long reunionId,
                                                          List<Miembro> miembrosNuevos, Boolean externo) {
        List<OrganoReunionMiembro> miembroReunionByOrganoAndReunionId =
                organoReunionMiembroDAO.getMiembroReunionByOrganoAndReunionId(organoReunion.getOrganoId(), externo,
                        reunionId);
        if (miembrosNuevos != null) {
            Boolean encontrado = false;
            for (OrganoReunionMiembro organoReunionMiembro : miembroReunionByOrganoAndReunionId) {
                for (Miembro miembronuevo : miembrosNuevos) {
                    if (miembronuevo.getPersonaId().toString().equals(organoReunionMiembro.getMiembroId())) {
                        encontrado = true;

                        organoReunionMiembro.setEmail(miembronuevo.getEmail());
                        organoReunionMiembro.setCargoId(miembronuevo.getCargo().getId());
                        organoReunionMiembro.setCargoNombre(miembronuevo.getCargo().getNombre());
                        organoReunionMiembro.setCargoNombreAlternativo(miembronuevo.getCargo().getNombreAlternativo());
                        organoReunionMiembro.setCargoCodigo(miembronuevo.getCargo().getCodigo());

                        if (!externo) {
                            organoReunionMiembro.setNato(miembronuevo.getNato());
                        }

                        organoReunionMiembroDAO.update(organoReunionMiembro);
                        miembrosNuevos.remove(miembronuevo);
                        break;
                    }
                }
                if (!encontrado) {
                    organoReunionMiembroDAO.deleteOrganoReunionMiembro(organoReunionMiembro);
                }
                encontrado = false;
            }
            for (Miembro miembro : miembrosNuevos) {
                OrganoReunionMiembro organoReunionMiembroFromMiembro = miembro.toOrganoReunionMiembro(organoReunion);
                organoReunionMiembroDAO.insert(organoReunionMiembroFromMiembro);
            }
        }
    }
}
