package es.uji.apps.goc.firmas;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.Utils;
import es.uji.apps.goc.dto.ReunionFirma;
import es.uji.apps.goc.exceptions.FirmaReunionException;
import es.uji.apps.goc.model.RespuestaFirma;
import es.uji.apps.goc.model.RespuestaFirmaAsistencia;
import es.uji.apps.goc.model.RespuestaFirmaPuntoOrdenDiaAcuerdo;

@Service
@Component
public class FirmaService
{
    @Value("${goc.external.firmasEndpoint}")
    private String firmasEndpoint;

    @Value("${goc.external.authToken}")
    private String authToken;

    @Transactional
    public RespuestaFirma firmaExterna(ReunionFirma reunionFirma) throws FirmaReunionException {
        Client client = Client.create(Utils.createClientConfig());

        WebResource getFirmasResource = client.resource(this.firmasEndpoint);

        ClientResponse response = getFirmasResource.type(MediaType.APPLICATION_JSON)
            .header("X-UJI-AuthToken", authToken)
            .post(ClientResponse.class, reunionFirma);

        if (response.getStatus() != 200)
        {
            throw new FirmaReunionException();
        }

        RespuestaFirma respuestaFirma = RespuestaFirma.buildRespuestaFirma(response);
        return respuestaFirma;
    }

    public RespuestaFirma firmaReunion(ReunionFirma reunionFirma)
    {
        RespuestaFirma respuestaFirma = new RespuestaFirma();

        List<RespuestaFirmaPuntoOrdenDiaAcuerdo> puntoOrdenDiaAcuerdos = new ArrayList<>();
        List<RespuestaFirmaAsistencia> asistencias = new ArrayList<>();

        respuestaFirma.setUrlActa("http://www.uji.es/acta");
        respuestaFirma.setUrlActaAlternativa("http://www.uji.es/acta");

        RespuestaFirmaPuntoOrdenDiaAcuerdo respuestaFirmaPuntoOrdenDiaAcuerdo = new RespuestaFirmaPuntoOrdenDiaAcuerdo();
        respuestaFirmaPuntoOrdenDiaAcuerdo.setId(1);
        respuestaFirmaPuntoOrdenDiaAcuerdo.setUrlActa("http://www.uji.es/acuerdoPuntoOrdenDia/1");
        puntoOrdenDiaAcuerdos.add(respuestaFirmaPuntoOrdenDiaAcuerdo);

        RespuestaFirmaAsistencia respuestaFirmaAsistencia = new RespuestaFirmaAsistencia();
        respuestaFirmaAsistencia.setPersonaId("88849");
        respuestaFirmaAsistencia.setUrlAsistencia("http://www.uji.es/asistencia/88849");
        asistencias.add(respuestaFirmaAsistencia);

        RespuestaFirmaAsistencia respuestaFirmaAsistencia2 = new RespuestaFirmaAsistencia();
        respuestaFirmaAsistencia2.setPersonaId("5121");
        respuestaFirmaAsistencia2.setUrlAsistencia("http://www.uji.es/asistencia/5121");
        asistencias.add(respuestaFirmaAsistencia2);

        respuestaFirma.setPuntoOrdenDiaAcuerdos(puntoOrdenDiaAcuerdos);
        respuestaFirma.setAsistencias(asistencias);

        return respuestaFirma;
    }
}