package es.uji.apps.goc.services;

import es.uji.apps.goc.dao.*;
import es.uji.apps.goc.dto.*;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.model.ComentarioPuntoOrdenDia;
import es.uji.apps.goc.notifications.AvisosReunion;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PuntoOrdenDiaComentarioService
{

    @Autowired
    PuntoOrdenDiaComentarioDAO puntoOrdenDiaComentarioDAO;

    @Autowired
    PersonaService personaService;

    @Autowired
    AvisosReunion avisosReunion;

    @Autowired
    PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    OrganoReunionMiembroDAO organoReunionMiembroDAO;

    @Autowired
    ReunionDAO reunionDAO;

    @Autowired
    OrganoAutorizadoDAO organoAutorizadoDAO;

    public ComentarioPuntoOrdenDia addComentario(ComentarioPuntoOrdenDia comentarioPuntoOrdenDia) throws Exception
    {
        PuntoOrdenDiaComentario puntoOrdenDiaComentario = comentarioPuntoOrdenDiaToDTO(comentarioPuntoOrdenDia);
        PuntoOrdenDia puntoOrdenDia =
            puntoOrdenDiaDAO.getPuntoOrdenDiaById(comentarioPuntoOrdenDia.getPuntoOrdenDiaId());
        puntoOrdenDiaComentario.setPuntoOrdenDia(puntoOrdenDia);
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(puntoOrdenDia.getReunion().getId());

        List<OrganoReunionMiembro> asistentes =
            organoReunionMiembroDAO.getAsistentesByReunionId(reunion.getId());

        puntoOrdenDiaComentarioDAO.insert(puntoOrdenDiaComentario);

        avisosReunion.enviarAvisoNuevoComentarioPuntoOrdenDiaAsistentesAutorizados(reunion, puntoOrdenDiaComentario,
            asistentes);

        return ComentarioPuntoOrdenDia.dtoToModel(puntoOrdenDiaComentario);
    }


    private PuntoOrdenDiaComentario comentarioPuntoOrdenDiaToDTO(ComentarioPuntoOrdenDia comentarioPuntoOrdenDia)
        throws PersonasExternasException
    {
        PuntoOrdenDiaComentario puntoOrdenDiaComentario = new PuntoOrdenDiaComentario();
        puntoOrdenDiaComentario.setComentario(comentarioPuntoOrdenDia.getComentario());
        puntoOrdenDiaComentario.setCreadorId(comentarioPuntoOrdenDia.getCreadorId());
        puntoOrdenDiaComentario.setFecha(comentarioPuntoOrdenDia.getFecha());
        puntoOrdenDiaComentario.setPuntoOrdenDia(new PuntoOrdenDia(comentarioPuntoOrdenDia.getPuntoOrdenDiaId()));
        puntoOrdenDiaComentario.setCreadorNombre(personaService.getPersonaFromDirectoryByPersonaId(comentarioPuntoOrdenDia.getCreadorId()).getNombre());
        if(comentarioPuntoOrdenDia.getComentarioSuperiorId() != null)
        {
            PuntoOrdenDiaComentario comentarioPadre = new PuntoOrdenDiaComentario(comentarioPuntoOrdenDia.getComentarioSuperiorId());
            puntoOrdenDiaComentario.setComentarioPadre(comentarioPadre);
        }
        return puntoOrdenDiaComentario;
    }

    @Transactional
    public void deleteComentario(Long comentarioId, User connectedUser)
    {
        puntoOrdenDiaComentarioDAO.deleteComentarioFromUser(comentarioId, connectedUser.getId());
    }

    public boolean isPermiteBorradoComentarioPunto(Long reunionId, ComentarioPuntoOrdenDia comentarioPuntoOrdenDia, Long connectedUserId)
    {
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (reunion.getCompletada() != null && reunion.getCompletada()) return false;

        List<OrganoAutorizado> listaAurotizados = organoAutorizadoDAO.getAutorizadosByReunionId(reunion.getId());

        List<OrganoAutorizado> listaAutorizadosFiltrada = listaAurotizados.stream()
                .filter(l -> l.getPersonaId().equals(connectedUserId))
                .collect(Collectors.toList());

        if (!listaAutorizadosFiltrada.isEmpty() || reunion.getCreadorId().equals(connectedUserId)) return true;

        if (comentarioPuntoOrdenDia != null && comentarioPuntoOrdenDia.getCreadorId().equals(connectedUserId)) return true;

        return false;

    }
}
