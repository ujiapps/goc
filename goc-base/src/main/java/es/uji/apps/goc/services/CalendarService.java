package es.uji.apps.goc.services;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.ParameterList;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.parameter.Cn;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.Method;
import net.fortuna.ical4j.model.property.Name;
import net.fortuna.ical4j.model.property.Organizer;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.TzId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.XProperty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.stream.Collectors;

import es.uji.apps.goc.dao.CalendarioDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.PersonaToken;
import es.uji.apps.goc.dto.PersonaTokenKey;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionPermiso;

@Service
public class CalendarService
{

    @Autowired
    ReunionDAO reunionDAO;

    @Autowired
    CalendarioDAO calendarioDAO;

    @Transactional
    public String crearFeed(Long connectedUserId)
    {
        return crearTokenUsuario(connectedUserId);
    }

    public Calendar obtenerCalendarioUsuario(String token, String lang)
    {
        Long connectedUserId = getUsuarioByToken(token);
        List<ReunionPermiso> reunionesFuturasByUsuario = getReunionesFuturasByUsuario(connectedUserId);
        return crearIcalFromReuniones(reunionesFuturasByUsuario, lang);
    }

    private Long getUsuarioByToken(String token)
    {
        return calendarioDAO.getIdPersonaByToken(token);
    }

    private List<ReunionPermiso> getReunionesFuturasByUsuario(Long connectedUserId)
    {
    	Boolean mostrarAbiertas = null;
        List<ReunionPermiso> reunionesAccesiblesByPersonaId =
            reunionDAO.getReunionesAccesiblesConvocadasDePersonaId(connectedUserId, mostrarAbiertas);
        return
            reunionesAccesiblesByPersonaId.stream().filter(r -> r.getFecha().after(new Date())).collect(Collectors.toList());
    }

    private String crearTokenUsuario(Long connectedUserId)
    {
        String token = UUID.randomUUID().toString();
        guardarTokenUsuario(connectedUserId, token);
        return token;
    }

    private void guardarTokenUsuario(
        Long connectedUserId,
        String token
    )
    {
        eliminarTokenUsuario(connectedUserId);

        PersonaTokenKey personaTokenKey = new PersonaTokenKey();
        personaTokenKey.setIdPersona(connectedUserId);
        personaTokenKey.setToken(token);
        PersonaToken personaToken = new PersonaToken();
        personaToken.setId(personaTokenKey);

        calendarioDAO.insert(personaToken);
    }

    private void eliminarTokenUsuario(Long connectedUserId)
    {
        calendarioDAO.deleteByIdPersona(connectedUserId);
    }

    private Calendar crearIcalFromReuniones(List<ReunionPermiso> reuniones, String lang)
    {
        List<Reunion> reunionesCompletas =
            reuniones.stream().map(r -> reunionDAO.getReunionById(r.getId())).collect(Collectors.toList());
        Calendar calendar = null;
        try
        {
            calendar = creaICal(reunionesCompletas, lang);
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        return calendar;
    }


    private Calendar creaICal(List<Reunion> reuniones, String lang) throws URISyntaxException
    {
        Calendar icsCalendar = createVcalendar(lang);
        for(Reunion reunion : reuniones)
        {
            VEvent meeting = createEvent(reunion);
            icsCalendar.getComponents().add(meeting);
        }
        return icsCalendar;
    }

    private VEvent createEvent(Reunion reunion) throws URISyntaxException {
        String asunto = reunion.getAsunto();
        DateTime start = null;
        DateTime end = null;
        if(reunion.getFecha() != null && reunion.getDuracion() != null) {
            start = new DateTime(reunion.getFecha());
            LocalDateTime endDate = LocalDateTime.ofInstant(reunion.getFecha().toInstant(), ZoneId.systemDefault()).plusMinutes(reunion.getDuracion());
            end = new DateTime(Date.from(endDate.atZone(ZoneId.systemDefault()).toInstant()));
        }
        VEvent meeting = new VEvent(start, end, asunto);
        Uid uid = new Uid(reunion.getId() + "@goc.com");
        meeting.getProperties().add(uid);
        meeting.getProperties().add(new Location(reunion.getUbicacion()));
        ParameterList parameterList = new ParameterList();
        parameterList.add(new Cn(reunion.getCreadorNombre()));
        if(reunion.getCreadorEmail() != null && reunion.getCreadorNombre() != null) {
            Organizer organizer = new Organizer(parameterList, reunion.getCreadorEmail());
            meeting.getProperties().add(organizer);
        }
        return meeting;
    }

    private Calendar createVcalendar(String lang) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("i18n",
            Locale.forLanguageTag(lang));
        Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(new ProdId("-//GOC//Calendario reuniones 1.0//EN"));
        icsCalendar.getProperties().add(CalScale.GREGORIAN);
        icsCalendar.getProperties().add(new XProperty("X-WR-CALNAME", resourceBundle.getString("calendario.nombre")));
        icsCalendar.getProperties().add(new Name(resourceBundle.getString("calendario.nombre")));
        icsCalendar.getProperties().add(Method.PUBLISH);
        VTimeZone vTimeZone = new VTimeZone();
        vTimeZone.getProperties().add(new TzId("Europe/Madrid"));
        icsCalendar.getComponents().add(vTimeZone);
        return icsCalendar;
    }
}
