package es.uji.apps.goc.services.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.goc.DateUtils;
import es.uji.apps.goc.Utils;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.CodigoCargoEnum;
import es.uji.apps.goc.enums.TipoVisualizacionResultadosEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.ExtGridFilter;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.ReunionAcuerdos;
import es.uji.apps.goc.model.TextoConvocatoria;
import es.uji.apps.goc.services.*;
import es.uji.apps.goc.templates.PDFTemplate;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.*;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Path("reuniones")
public class ReunionResource extends CoreBaseService {
    private final ReunionDAO reunionDAO;
    @InjectParam
    private ReunionService reunionService;

    @InjectParam
    private DescriptorService descriptorService;

    @InjectParam
    private ReunionDocumentoService reunionDocumentoService;

    @InjectParam
    private OrganoService organoService;

    @InjectParam
    private OrganoReunionMiembroService organoReunionMiembroService;

    @InjectParam
    private LanguageConfig languageConfig;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @InjectParam
    private PersonaService personaService;


    @Value("${goc.templates.path:classpath:templates/}")
    private String templatesPath;

    public ReunionResource(ReunionDAO reunionDAO) {
        this.reunionDAO = reunionDAO;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getReuniones(@QueryParam("organoId") String organoId,
                                        @QueryParam("tipoOrganoId") Long tipoOrganoId, @QueryParam("externo") Boolean externo,
                                        @QueryParam("filter") String filter,
                                        @QueryParam("start") Integer start,
                                        @QueryParam("limit") Integer limit)
            throws OrganosExternosException, RolesPersonaExternaException, IOException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String query = null;
        if (filter != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            List<ExtGridFilter> filtros = objectMapper.readValue(filter, new TypeReference<List<ExtGridFilter>>() {
            });
            query = filtros.get(0).getValue();
        }

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);

        if (personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador)) {
            responseMessage.setData(UIEntity.toUI(reunionDAO.getReunionesByAdminList(false, tipoOrganoId, organoId, externo, query, start, limit)));
            responseMessage.setTotalCount(reunionDAO.getReunionesByAdminTotal(false, tipoOrganoId, organoId, externo, query).intValue());
        } else {
            responseMessage.setData(UIEntity.toUI(reunionDAO.getReunionesByEditorIdList(connectedUserId, false, tipoOrganoId, organoId, externo, query, start, limit)));
            responseMessage.setTotalCount(reunionDAO.getReunionesByEditorIdTotal(connectedUserId, false, tipoOrganoId, organoId, externo, query).intValue());
        }

        return responseMessage;
    }

    @GET
    @Path("{reunionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getReunionById(@PathParam("reunionId") Long reunionId)
            throws ReunionNoDisponibleException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        return UIEntity.toUI(reunionService.getReunionByIdAndEditorId(reunionId, connectedUserId));
    }

    @GET
    @Path("completadas")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getReunionesCompletadas(@QueryParam("organoId") String organoId,
                                                   @QueryParam("tipoOrganoId") Long tipoOrganoId, @QueryParam("externo") Boolean externo,
                                                   @QueryParam("filter") String filter,
                                                   @QueryParam("start") Integer start,
                                                   @QueryParam("limit") Integer limit)
            throws OrganosExternosException, RolesPersonaExternaException, IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String query = null;
        if (filter != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            List<ExtGridFilter> filtros = objectMapper.readValue(filter, new TypeReference<List<ExtGridFilter>>() {
            });
            query = filtros.get(0).getValue();
        }

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);

        if (personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador)) {
            responseMessage.setData(UIEntity.toUI(reunionDAO.getReunionesByAdminList(true, tipoOrganoId, organoId, externo, query, start, limit)));
            responseMessage.setTotalCount(reunionDAO.getReunionesByAdminTotal(true, tipoOrganoId, organoId, externo, query).intValue());
        } else {
            responseMessage.setData(UIEntity.toUI(reunionDAO.getReunionesByEditorIdList(connectedUserId, true, tipoOrganoId, organoId, externo, query, start, limit)));
            responseMessage.setTotalCount(reunionDAO.getReunionesByEditorIdTotal(connectedUserId, true, tipoOrganoId, organoId, externo, query).intValue());
        }

        return responseMessage;
    }

    @Path("{reunionId}/puntosOrdenDia")
    public ReunionPuntosOrdenDiaResource getReunionPuntosOrdenDiaResource(
            @InjectParam ReunionPuntosOrdenDiaResource reunionPuntosOrdenDiaResource) {
        return reunionPuntosOrdenDiaResource;
    }

    @Path("{reunionId}/documentos")
    public ReunionDocumentosResource getReunionDocumentosResource(
            @InjectParam ReunionDocumentosResource reunionDocumentosResource) {
        return reunionDocumentosResource;
    }

    @Path("{reunionId}/miembros")
    public ReunionMiembroResource getReunionMiembroResource(@InjectParam ReunionMiembroResource reunionMiembroResource) {
        return reunionMiembroResource;
    }

    @GET
    @Path("{reunionId}/tienesuplente/{reunionMiembroId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage tieneSuplente(@PathParam("reunionId") Long reunionId,
                                         @PathParam("reunionMiembroId") Long reunionMiembroId)
            throws AsistenteNoEncontradoException, ReunionYaCompletadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        boolean tieneSuplente = organoReunionMiembroService.tieneSuplente(reunionMiembroId);

        if (tieneSuplente) {
            return new ResponseMessage(true, "true");
        }

        return new ResponseMessage(true, "false");
    }

    @POST
    @Path("{reunionId}/confirmar/{reunionMiembroId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void confirmarAsistencia(@PathParam("reunionId") Long reunionId,
                                    @PathParam("reunionMiembroId") Long reunionMiembroId, UIEntity ui)
            throws AsistenteNoEncontradoException, ReunionYaCompletadaException, VotacionYaRealizadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Boolean asistencia = new Boolean(ui.get("confirmacion"));
        String justificaion = ui.get("justificacion");

        reunionService.compruebaReunionNoCompletada(reunionId);

        organoReunionMiembroService.estableceAsistencia(reunionMiembroId, connectedUserId, asistencia, justificaion);
    }

    @POST
    @Path("{reunionId}/suplente")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void establecerSuplente(@PathParam("reunionId") Long reunionId, UIEntity suplente)
            throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long suplenteId = Long.parseLong(suplente.get("suplenteId"));
        String suplenteNombre = suplente.get("suplenteNombre");
        String suplenteEmail = suplente.get("suplenteEmail");
        Long organoMiembroId = Long.parseLong(suplente.get("organoMiembroId"));

        reunionService.compruebaReunionNoCompletada(reunionId);
        reunionService.compruebaReunionAdmiteSuplencia(reunionId);

        if (suplenteId != null) {
            for (OrganoReunionMiembro miembro : organoReunionMiembroService.getMiembrosByReunionId(reunionId)) {
                if (miembro.getMiembroId().equals(suplenteId.toString())) {
                    throw new MiembroNoPuedeSerSuplenteException();
                }

                if (miembro.getSuplenteId() != null && miembro.getSuplenteId().equals(suplenteId)) {
                    throw new SuplenteYaAsignadoAnteriormente();
                }
            }
        }

        organoReunionMiembroService.estableceSuplente(reunionId, connectedUserId, suplenteId, suplenteNombre,
                suplenteEmail, organoMiembroId);
    }

    @DELETE
    @Path("{reunionId}/suplente")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void borraSuplente(@PathParam("reunionId") Long reunionId, UIEntity miembro)
            throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long miembroId = Long.parseLong(miembro.get("organoMiembroId"));

        reunionService.compruebaReunionNoCompletada(reunionId);
        reunionService.compruebaReunionAdmiteSuplencia(reunionId);
        organoReunionMiembroService.borraSuplente(reunionId, miembroId, connectedUserId);
    }

    @POST
    @Path("{reunionId}/delegadovoto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void establecerDelegacionVoto(@PathParam("reunionId") Long reunionId, UIEntity delegadoVoto)
            throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long delegadoVotoId = Long.parseLong(delegadoVoto.get("delegadoVotoId"));
        String delegadoVotoNombre = delegadoVoto.get("delegadoVotoNombre");
        String delegadoVotoEmail = delegadoVoto.get("delegadoVotoEmail");
        Long organoMiembroId = Long.parseLong(delegadoVoto.get("organoMiembroId"));
        Long organoMiembroDelegadoId = Long.parseLong(delegadoVoto.get("organoMiembroDelegadoId"));

        reunionService.compruebaReunionNoCompletada(reunionId);
        organoReunionMiembroService.estableceDelegadoVoto(reunionId, connectedUserId, delegadoVotoId,
                delegadoVotoNombre, delegadoVotoEmail, organoMiembroId);
    }

    @DELETE
    @Path("{reunionId}/delegadovoto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void borraDelegacionVoto(@PathParam("reunionId") Long reunionId, UIEntity miembro)
            throws Exception {
        Long miembroId = Long.parseLong(miembro.get("organoMiembroId"));

        reunionService.compruebaReunionNoCompletada(reunionId);
        organoReunionMiembroService.borraDelegadoVoto(reunionId, miembroId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addReunion(UIEntity reunionUI)
            throws NotificacionesException, MiembrosExternosException, PersonasExternasException, UrlGrabacionException,
            NoEsTelematicaExceptionConVotacion, NoAdmiteVotacionYAceptaCambioDeVotoException,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException, ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Reunion reunion = reunionUIToModel(reunionUI);
        reunion = reunionService.addReunion(reunion, connectedUserId);

        return UIEntity.toUI(reunion);
    }

    @POST
    @Path("{reunionId}/duplica")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity duplicaReunion(@PathParam("reunionId") Long reunionId, UIEntity reunionUI)
            throws PersonasExternasException, UrlGrabacionException, NoEsTelematicaExceptionConVotacion,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException, NoAdmiteVotacionYAceptaCambioDeVotoException,
            NoVotacionTelematicaConPuntosOrdenDiaVotacionAbiertaException, ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Reunion reunion = reunionUIToModel(reunionUI);

        //Campos que no se deben duplicar
        reunion.setId(null);
        reunion.setComentarios(null);

        reunion = reunionService.duplicaReunion(reunion, connectedUserId);
        return UIEntity.toUI(reunion);
    }

    @POST
    @Path("{reunionId}/duplicapuntos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response duplicaReunionPuntos(@PathParam("reunionId") Long reunionOriginalId, Long reunionCopiadaId)
            throws PersonasExternasException, TipoRecuentoVotoNoExisteException,
            RecuentoVotoInvalidoParaPuntoNoVotableException, ProcedimientoVotacionInvalidoParaPuntoNoVotableException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        reunionService.duplicaReunionPuntos(reunionCopiadaId, reunionOriginalId, connectedUserId);
        return Response.ok().build();
    }

    @PUT
    @Path("{reunionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity modificaReunion(@PathParam("reunionId") Long reunionId, UIEntity reunionUI)
            throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String asunto = reunionUI.get("asunto");
        String asuntoAlternativo = reunionUI.get("asuntoAlternativo");
        String descripcion = reunionUI.get("descripcion");
        String descripcionAlternativa = reunionUI.get("descripcionAlternativa");
        String ubicacion = reunionUI.get("ubicacion");
        String ubicacionAlternativa = reunionUI.get("ubicacionAlternativa");
        String urlGrabacion = reunionUI.get("urlGrabacion");
        Boolean publica = new Boolean(reunionUI.get("publica"));
        Boolean telematica = new Boolean(reunionUI.get("telematica"));
        String telematicaDescripcion = reunionUI.get("telematicaDescripcion");
        String telematicaDescripcionAlternativa = reunionUI.get("telematicaDescripcionAlternativa");
        Boolean admiteSuplencia = new Boolean(reunionUI.get("admiteSuplencia"));
        Boolean admiteDelegacionVoto = new Boolean(reunionUI.get("admiteDelegacionVoto"));
        if (!admiteDelegacionVoto && existenPersonasVotoDelegado(reunionId)) {
            reunionService.eliminarDelegacionesExistentesYNotificar(reunionId);
        }
        Boolean admiteComentarios = new Boolean(reunionUI.get("admiteComentarios"));
        Boolean emailsEnNuevosComentarios = new Boolean(reunionUI.get("emailsEnNuevosComentarios"));
        String horaFin = reunionUI.get("horaFin");
        Long convocatoriaComienzo = reunionUI.getLong("convocatoriaComienzo");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        LocalDateTime dateTime = LocalDateTime.parse(reunionUI.get("fecha"), formatter);
        Date fecha = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());

        Date fechaSegundaConvocatoria = null;

        Long responsableVotoId = reunionUI.getLong("responsableVotoId");

        //Guardar el id a null en caso de que no se haya elegido responsable
        if (responsableVotoId == 0L) {
            responsableVotoId = null;
        }

        String responsableVotoNom = reunionUI.get("responsableVotoNom");

        if (reunionUI.get("fechaSegundaConvocatoria") != null && !reunionUI.get("fechaSegundaConvocatoria").isEmpty()) {
            LocalDateTime dateTimeSegundaConvocatoria =
                    LocalDateTime.parse(reunionUI.get("fechaSegundaConvocatoria"), formatter);
            fechaSegundaConvocatoria =
                    Date.from(dateTimeSegundaConvocatoria.atZone(ZoneId.systemDefault()).toInstant());
        }

        Date fechaFinVotacion = DateUtils.dateFormatter(reunionUI.get("fechaFinVotacion"));

        Long numeroSesion = null;

        if (!reunionUI.get("numeroSesion").isEmpty()) {
            numeroSesion = Long.parseLong(reunionUI.get("numeroSesion"));
        }

        Long duracion = Long.parseLong(reunionUI.get("duracion"));
        Boolean aceptaCambioVoto = new Boolean(reunionUI.get("admiteCambioVoto"));
        Boolean verDeliberaciones = reunionUI.getBoolean("verDeliberaciones");
        Boolean verAcuerdos = reunionUI.getBoolean("verAcuerdos");
        TipoVisualizacionResultadosEnum tipoVisualizacionResultados = new Boolean(reunionUI.get(
                "tipoVisualizacionResultados")) ? TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION : TipoVisualizacionResultadosEnum.AL_ALCANZAR_FECHA_FIN_VOTACION;
        reunionService.compruebaReunionNoCompletada(reunionId);

        Reunion reunion = reunionUIToModel(reunionUI);
        reunion.setId(reunionId);

        reunion =
                reunionService.updateReunion(reunionId, asunto, asuntoAlternativo, descripcion, descripcionAlternativa,
                        duracion, fecha, fechaSegundaConvocatoria, ubicacion, ubicacionAlternativa, urlGrabacion,
                        numeroSesion, publica, telematica, telematicaDescripcion, telematicaDescripcionAlternativa,
                        admiteSuplencia, admiteDelegacionVoto, admiteComentarios, emailsEnNuevosComentarios,
                        connectedUserId, horaFin, convocatoriaComienzo, reunion.getVotacionTelematica(),
                        aceptaCambioVoto, tipoVisualizacionResultados, fechaFinVotacion, responsableVotoId, responsableVotoNom, verDeliberaciones, verAcuerdos);

        return UIEntity.toUI(reunion);
    }

    private boolean existenPersonasVotoDelegado(Long reunionId) {
        List<OrganoReunionMiembro> organoReunionMiembros = reunionService.personasVotoDelegado(reunionId);
        return organoReunionMiembros != null && organoReunionMiembros.size() > 0;
    }

    @GET
    @Path("{reunionId}/existendelegados")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> nombresVotosDelegadosReunion(@PathParam("reunionId") Long reunionId) {
        return UIEntity.toUI(reunionService.personasVotoDelegado(reunionId));
    }

    private Reunion reunionUIToModel(UIEntity reunionUI)
            throws UrlGrabacionException, ParseException {
        Reunion reunion = new Reunion();
        String urlGrabacion = reunionUI.get("urlGrabacion");

        if (!urlGrabacion.isEmpty()) {
            try {
                new URL(urlGrabacion);
            } catch (MalformedURLException e) {
                throw new UrlGrabacionException();
            }
        }

        if (ParamUtils.parseLong(reunionUI.get("id")) != null) {
            reunion.setId(new Long(reunionUI.get("id")));
        }

        reunion.setAsunto((reunionUI.get("asunto")));
        reunion.setAsuntoAlternativo((reunionUI.get("asuntoAlternativo")));
        reunion.setDescripcion(reunionUI.get("descripcion"));
        reunion.setDescripcionAlternativa(reunionUI.get("descripcionAlternativa"));
        reunion.setUbicacion(reunionUI.get("ubicacion"));
        reunion.setUbicacionAlternativa(reunionUI.get("ubicacionAlternativa"));
        reunion.setUrlGrabacion(urlGrabacion);
        reunion.setTelematica(new Boolean(reunionUI.get("telematica")));
        reunion.setTelematicaDescripcion(reunionUI.get("telematicaDescripcion"));
        reunion.setTelematicaDescripcionAlternativa(reunionUI.get("telematicaDescripcionAlternativa"));

        if (reunionUI.get("responsableVotoId") != null && !reunionUI.get("responsableVotoId").isEmpty()) {
            reunion.setResponsableVotoId(Long.parseLong(reunionUI.get("responsableVotoId")));
            reunion.setResponsableVotoNom(reunionUI.get("responsableVotoNom"));
        }

        if (reunionUI.get("numeroSesion") != null && !reunionUI.get("numeroSesion").isEmpty()) {
            reunion.setNumeroSesion(Long.parseLong(reunionUI.get("numeroSesion")));
        }

        reunion.setPublica(new Boolean(reunionUI.get("publica")));
        reunion.setAdmiteSuplencia(new Boolean(reunionUI.get("admiteSuplencia")));
        reunion.setAdmiteDelegacionVoto(new Boolean(reunionUI.get("admiteDelegacionVoto")));
        reunion.setAdmiteComentarios(new Boolean(reunionUI.get("admiteComentarios")));
        reunion.setEmailsEnNuevosComentarios(new Boolean(reunionUI.get("emailsEnNuevosComentarios")));

        reunion.setVotacionTelematica(new Boolean(reunionUI.get("votacionTelematica")));

        DateTimeFormatter formatterEspanyol = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        DateTimeFormatter formatterIngles = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

        if (reunionUI.get("fecha") != null && !reunionUI.get("fecha").isEmpty()) {
            LocalDateTime dateTime;
            try {
                dateTime = LocalDateTime.parse(reunionUI.get("fecha"), formatterEspanyol);
            } catch (DateTimeParseException e) {
                dateTime = LocalDateTime.parse(reunionUI.get("fecha"), formatterIngles);
            }
            reunion.setFecha(Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant()));
        }

        DateTimeFormatter formatterToUse = formatterEspanyol;
        if (reunionUI.get("fechaSegundaConvocatoria") != null) {
            if (reunionUI.get("fechaSegundaConvocatoria").indexOf('T') >= 0) {
                formatterToUse = formatterIngles;
            }

            if (!reunionUI.get("fechaSegundaConvocatoria").isEmpty()) {
                LocalDateTime dateTimeSegundaConvocatoria =
                        LocalDateTime.parse(reunionUI.get("fechaSegundaConvocatoria"), formatterToUse);
                reunion.setFechaSegundaConvocatoria(
                        Date.from(dateTimeSegundaConvocatoria.atZone(ZoneId.systemDefault()).toInstant()));
            }
        }

        reunion.setFechaFinVotacion(DateUtils.dateFormatter(reunionUI.get("fechaFinVotacion")));


        reunion.setDuracion(Long.parseLong(reunionUI.get("duracion")));

        reunion.setAdmiteCambioVoto(new Boolean(reunionUI.get("admiteCambioVoto")));
        reunion.setTipoVisualizacionResultadosEnum(reunionUI.get("tipoVisualizacionResultados") == null || new Boolean(
                reunionUI.get(
                        "tipoVisualizacionResultados")) ? TipoVisualizacionResultadosEnum.AL_FINALIZAR_REUNION : TipoVisualizacionResultadosEnum.AL_ALCANZAR_FECHA_FIN_VOTACION);
        reunion.setVerDeliberaciones(reunionUI.getBoolean("verDeliberaciones"));
        reunion.setVerAcuerdos(reunionUI.getBoolean("verAcuerdos"));

        return reunion;
    }


    @PUT
    @Path("publica/{reunionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modificaVisibilidadReunion(@PathParam("reunionId") Long reunionId, UIEntity uiEntity)
            throws RolesPersonaExternaException, PersonaNoAutorizadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        if (personaService.isAdmin(connectedUserId) || personaService.isAutorizadoEnReunion(reunionId, connectedUserId)) {
            Boolean publica = new Boolean(uiEntity.get("publica"));
            reunionService.modificaVisibilidadReunion(reunionId, publica);
        } else {
            throw new PersonaNoAutorizadaException();
        }
    }

    @PUT
    @Path("{reunionId}/completada")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modificaReunionCompletada(@PathParam("reunionId") Long reunionId, UIEntity data)
            throws ReunionNoDisponibleException, ReunionYaCompletadaException, FirmaReunionException,
            OrganosExternosException, PersonasExternasException, IOException, NoSuchAlgorithmException,
            NoVotacionTelematicaException, ReunionNoAbiertaException, FaltaPresidenteYSecretarioException,
            FaltaPresidenteException, FaltaSecretarioException {
        String acuerdos = data.get("acuerdos");
        String acuerdosAlternativos = data.get("acuerdosAlternativos");

        Long responsableActaId = Long.parseLong(data.get("responsable"));
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        reunionService.compruebaReunionNoCompletada(reunionId);
        //reunionService.compruebaCargosFirmantesValidos(reunionId);
        reunionService.firmarReunion(reunionId, acuerdos, acuerdosAlternativos, responsableActaId, connectedUserId);
    }

    @PUT
    @Path("{reunionId}/enviarconvocatoria")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage enviarConvocatoria(@PathParam("reunionId") Long reunionId,
                                              TextoConvocatoria textoConvocatoria)
            throws Exception {
        User connectedUser = AccessManager.getConnectedUser(request);

        String messageError =
                reunionService.enviarConvocatoria(reunionId, connectedUser, textoConvocatoria.getTextoConvocatoria(), textoConvocatoria.getTextoConvocatoriaAlternativo());

        if (messageError == null) {
            return new ResponseMessage(true, "appI18N.reuniones.convocatoriaEnviada");
        }

        return new ResponseMessage(true, messageError);
    }

    @PUT
    @Path("{reunionId}/enviarborrador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response enviarBorrador(@PathParam("reunionId") Long reunionId, UIEntity UIEmails) throws Exception {
        List<String> emails = UIEmails.getArray("emails");

        reunionService.enviarBorrador(reunionId, emails);
        return Response.ok().build();
    }

    @PUT
    @Path("{reunionId}/informarOrgano")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage informarOrgano(@PathParam("reunionId") Long reunionId, UIEntity uiEntity,
                                          @QueryParam("externo") Boolean organoExterno)
            throws Exception {
        User connectedUser = AccessManager.getConnectedUser(request);

        String organoId = uiEntity.get("organoId");
        String enviarOrden = uiEntity.get("enviarOrden");
        String textoInformacion = uiEntity.get("textoInformar");
        Boolean enviarOrdenDia = enviarOrden.equals("on") ? true : false;
        String textoInformacionAlternativo = uiEntity.get("textoInformarAlternativo");
        String messageError = reunionService.informarOrgano(reunionId, connectedUser.getId(), organoId, enviarOrdenDia,
                organoExterno, textoInformacion, textoInformacionAlternativo);

        if (messageError == null) {
            return new ResponseMessage(true, "appI18N.reuniones.convocatoriaEnviada");
        }

        return new ResponseMessage(true, messageError);
    }

    @PUT
    @Path("{reunionId}/enviaremail")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage enviarEmail(@PathParam("reunionId") Long reunionId, UIEntity uiEntity)
            throws Exception {
        User connectedUser = AccessManager.getConnectedUser(request);

        String email = uiEntity.get("cuerpoEmail");

        String messageError = reunionService.enviarEmail(reunionId, connectedUser, email);

        if (messageError == null) {
            return new ResponseMessage(true, "appI18N.reuniones.emailEnviado");
        }

        return new ResponseMessage(true, messageError);
    }

    @GET
    @Path("{reunionId}/checktoclose")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage checkReunionToClose(@PathParam("reunionId") Long reunionId) {
        String messageError = reunionService.checkReunionToClose(reunionId);

        if (messageError == null) {
            return new ResponseMessage(true, "");
        }

        return new ResponseMessage(true, messageError);
    }

    @GET
    @Path("{reunionId}/acuerdos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getAcuerdos(@PathParam("reunionId") Long reunionId) {
        ReunionAcuerdos reunionAcuerdos = reunionService.getAcuerdosByReunionId(reunionId);
        return UIEntity.toUI(reunionAcuerdos);
    }


    @PUT
    @Path("{reunionId}/organos")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modificaReunionesOrgano(@PathParam("reunionId") Long reunionId, UIEntity reunionOrganosUI)
            throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<UIEntity> organosUI = reunionOrganosUI.getRelations().get("organos");
        List<Organo> organos = creaOrganosDesdeOrganosUI(organosUI);

        reunionService.compruebaReunionNoCompletada(reunionId);
        compruebaPermisosAutorizadoOrganos(organos, connectedUserId);
        reunionService.updateOrganosReunionByReunionId(reunionId, organos, connectedUserId);

        organoReunionMiembroService.updateOrganoReunionMiembrosDesdeOrganosUI(organosUI, reunionId, connectedUserId);
        organoReunionMiembroService.updateOrganoReunionInvitadosDesdeOrganosUI(organosUI, reunionId, connectedUserId);

        return Response.ok().build();
    }

    @PUT
    @Path("{reunionId}/organos/{organoId}/miembros/{miembroId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modificaReunionOrgano(@PathParam("reunionId") Long reunionId, @PathParam("organoId") Long organoId,
                                          @PathParam("miembroId") Long miembroId, UIEntity miembroUI) throws Exception {
        reunionService.compruebaReunionNoCompletada(reunionId);
        organoReunionMiembroService.updateOrganoReunionMiembroUI(miembroUI);

        return Response.ok().build();
    }

    private void compruebaPermisosAutorizadoOrganos(List<Organo> organos, Long connectedUserId)
            throws OrganoConvocadoNoPermitidoException, RolesPersonaExternaException {
        if (!organoService.usuarioConPermisosParaConvocarOrganos(organos, connectedUserId)) {
            throw new OrganoConvocadoNoPermitidoException();
        }
    }

    private List<Organo> creaOrganosDesdeOrganosUI(List<UIEntity> organosUI) {
        List<Organo> organos = new ArrayList<>();
        if (organosUI != null) {
            for (UIEntity organoUI : organosUI) {
                Organo organo = new Organo(organoUI.get("id"));
                organo.setExterno(new Boolean(organoUI.get("externo")));
                organos.add(organo);
            }
        }
        return organos;
    }

    @DELETE
    @Path("{reunionId}")
    public Response borraOrgano(@PathParam("reunionId") Long reunionId, UIEntity entity)
            throws ReunionNoDisponibleException, ReunionYaCompletadaException, PuntoDelDiaConAcuerdosException,
            Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        reunionService.compruebaReunionNoCompletada(reunionId);
        reunionService.removeReunionById(reunionId, connectedUserId);

        return Response.ok().build();
    }


    @GET
    @Path("{reunionId}/asistencia")
    @Produces("application/pdf")
    public Template reunion(@PathParam("reunionId") Long reunionId, @QueryParam("lang") String lang,
                            @Context HttpServletRequest request)
            throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
            ReunionNoCompletadaException, AsistenteNoEncontradoException, PersonasExternasException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Reunion reunion = reunionService.getReunionConOrganosById(reunionId, connectedUserId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        if (!reunion.getCompletada()) {
            throw new ReunionNoCompletadaException();
        }

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, false,
                languageConfig.isMainLangauge(lang));
        String nombreAsistente = reunionService.getNombreAsistente(reunionId, connectedUserId);
        String horaFin = reunion.getHoraFin();

        Long horaInitMiliseconds = reunionTemplate.getFecha().getTime();

        Long duracionMiliseconds = reunionTemplate.getDuracion() * 60000L;

        Date horaFinalizacion = new Date(horaInitMiliseconds + duracionMiliseconds);
        if (nombreAsistente == null) {
            throw new AsistenteNoEncontradoException();
        }

        String applang = languageConfig.getLangCode(lang);

        Template template = new PDFTemplate("asistencia-" + applang, templatesPath);
        template.put("logo", personalizationConfig.logoDocumentos);
        template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
        template.put("nombreAsistente", nombreAsistente);
        template.put("tituloReunion", reunionTemplate.getAsunto());
        template.put("fechaReunion", getFechaReunion(reunionTemplate.getFecha()));
        template.put("horaReunion", getHoraReunion(reunionTemplate.getFecha()));
        template.put("nombreConvocante", reunionTemplate.getCreadorNombre());
        template.put("horaFin", horaFin);
        template.put("horaFinal", getHoraReunion(horaFinalizacion));
        return template;
    }

    @GET
    @Path("{reunionId}/asistentes")
    @Produces("application/pdf")
    public Template listaAsistentes(@PathParam("reunionId") Long reunionId, @QueryParam("lang") String lang,
                                    @Context HttpServletRequest request)
            throws ReunionNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Reunion reunion = reunionService.getReunionConOrganosById(reunionId, connectedUserId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, false,
                languageConfig.isMainLangauge(lang));

        reunionService.eliminarMiembrosConCargoSuplente(reunionTemplate);

        List<OrganoTemplate> organosTemplate = reunionTemplate.getOrganos();

        /*Ordena a los miembros y suplentes para la hoja de firmas*/
        for (int i = 0; i < organosTemplate.size(); i++) {
            List<MiembroTemplate> asistentesTemplate = organosTemplate.get(i).getAsistentes();

            Collections.sort(asistentesTemplate, new Comparator<MiembroTemplate>() {

                @Override
                public int compare(MiembroTemplate arg0, MiembroTemplate arg1) {

                    if (arg0.getSuplente() != null && arg1.getSuplente() != null) {

                        return Normalizer.normalize(arg0.getSuplente(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").compareTo(Normalizer.normalize(arg1.getSuplente(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
                    }

                    if (arg0.getSuplente() != null && arg1.getNombre() != null) {

                        return Normalizer.normalize(arg0.getSuplente(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").compareTo(Normalizer.normalize(arg1.getNombre(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
                    }

                    if (arg0.getNombre() != null && arg1.getSuplente() != null) {

                        return Normalizer.normalize(arg0.getNombre(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").compareTo(Normalizer.normalize(arg1.getSuplente(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
                    }

                    return Normalizer.normalize(arg0.getNombre(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").compareTo(Normalizer.normalize(arg1.getNombre(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
                }
            });

            organosTemplate.get(i).setAsistentes(primeroPresidenteYSecretario(asistentesTemplate));

            reunionTemplate.setOrganos(organosTemplate);
        }

        String applang = languageConfig.getLangCode(lang);

        Template template = new PDFTemplate("asistentes-" + applang, templatesPath);
        template.put("logo", personalizationConfig.logoDocumentos);
        template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
        template.put("reunion", reunionTemplate);

        return template;
    }

    private List<MiembroTemplate> primeroPresidenteYSecretario(List<MiembroTemplate> miembros) {
        if (miembros == null) return null;

        List<MiembroTemplate> miembrosOrdenados = new ArrayList<>();

        miembrosOrdenados.addAll(miembros.stream().filter(m -> m.getCargo().getCodigo().equalsIgnoreCase(CodigoCargoEnum.PRESIDENTE.codigo)).collect(Collectors.toList()));
        miembrosOrdenados.addAll(miembros.stream().filter(m -> m.getCargo().getCodigo().equalsIgnoreCase(CodigoCargoEnum.SECRETARIO.codigo)).collect(Collectors.toList()));
        miembrosOrdenados.addAll(miembros.stream().filter(m -> !(m.getCargo().getCodigo().equalsIgnoreCase(CodigoCargoEnum.SECRETARIO.codigo) || (m.getCargo().getCodigo().equalsIgnoreCase(CodigoCargoEnum.PRESIDENTE.codigo)))).collect(Collectors.toList()));

        return miembrosOrdenados;
    }

    private String getFechaReunion(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        return sdf.format(fecha);
    }

    private String getHoraReunion(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        return sdf.format(fecha);
    }

    @GET
    @Path("{reunionId}/descriptores")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> listaDescriptores(@PathParam("reunionId") Long reunionId)
            throws ReunionNoDisponibleException, OrganosExternosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        return UIEntity.toUI(descriptorService.getDescriptoresByReunionId(reunionId, connectedUserId));
    }

    @GET
    @Path("{reunionId}/invitados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> listaInvitados(@PathParam("reunionId") Long reunionId)
            throws ReunionNoDisponibleException, OrganosExternosException {
        return UIEntity.toUI(reunionService.getInvitadosReunionByReunionId(reunionId));
    }

    @PUT
    @Path("{reunionId}/invitados")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> modificaInvitados(@PathParam("reunionId") Long reunionId, UIEntity reunionInvitadosUI)
            throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ReunionInvitado> reunionInvitados = creaInvitadosDesdeInvitadosUI(reunionId, reunionInvitadosUI);
        List<ReunionInvitado> reunionInvitadosTodosUpdate =
                creaInvitadosDesdeInvitadosUI(reunionId, reunionInvitadosUI);

        reunionService.compruebaReunionNoCompletada(reunionId);

        List<String> emailsDeInvitadosNotificados = Collections.emptyList();
        if (reunionService.compruebaReunionConvocada(reunionId)) {
            List<ReunionInvitado> emailsInvitadosNotificados =
                    reunionService.existenNuevosInvitados(reunionId, reunionInvitados);
            reunionService.updateInvitadosByReunionId(reunionId, reunionInvitadosTodosUpdate, connectedUserId);
            return UIEntity.toUI(emailsInvitadosNotificados);
        }
        reunionService.updateInvitadosByReunionId(reunionId, reunionInvitados, connectedUserId);
        return UIEntity.toUI(emailsDeInvitadosNotificados);
    }

    @PUT
    @Path("{reunionId}/invitados/{invitadoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modificaInvitado(@PathParam("reunionId") Long reunionId, @PathParam("invitadoId") Long invitadoId, UIEntity reunionInvitadosUI)
            throws Exception {

        UIEntity invitadoUI = reunionInvitadosUI.getRelations().get("invitados").get(0);
        Reunion reunion = new Reunion(reunionId);

        ReunionInvitado reunionInvitado = new ReunionInvitado();
        reunionInvitado.setReunion(reunion);
        reunionInvitado.setId(Long.parseLong(invitadoUI.get("id")));
        reunionInvitado.setPersonaId(invitadoUI.getLong("personaId"));
        reunionInvitado.setPersonaNombre(invitadoUI.get("personaNombre"));
        reunionInvitado.setPersonaEmail(invitadoUI.get("personaEmail"));
        reunionInvitado.setMotivoInvitacion(invitadoUI.get("motivoInvitacion"));
        reunionInvitado.setAsistencia(invitadoUI.getBoolean("asistencia"));

        reunionService.compruebaReunionNoCompletada(reunionId);

        reunionService.updateInvitadoById(reunionInvitado);
    }

    @GET
    @Path("{reunionId}/organos/{organoId}/invitados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInvitadosOrganoByReunion(@PathParam("reunionId") Long reunionId, @PathParam("organoId") String organoId,
                                                      @QueryParam("externo") Boolean externo) {
        return UIEntity.toUI(organoService.getInvitadosOrganoByReunion(reunionId, organoId, externo));
    }

    @PUT
    @Path("{reunionId}/organos/{organoId}/invitados/{invitadoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void modificaInvitadoOrganoByReunion(UIEntity invitadoUI) {
        organoService.modificaInvitadoOrganoByReunion(invitadoUI);
    }

    @PUT
    @Path("{reunionId}/notificainvitados")
    @Consumes(MediaType.APPLICATION_JSON)
    public void notificaInvitados(@PathParam("reunionId") Long reunionId, UIEntity reunionInvitadosUI)
            throws Exception {
        List<ReunionInvitado> reunionInvitados = creaInvitadosDesdeInvitadosUI(reunionId, reunionInvitadosUI);
        reunionService.notificaNuevosInvitados(reunionId, reunionInvitados);
    }

    @GET
    @Path("{reunionId}/hojafirmas")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getHojaFirmas(@PathParam("reunionId") Long reunionId) {
        ReunionHojaFirmas hojaFirmasByReunionId = reunionService.getHojaFirmasByReunionId(reunionId);
        if (hojaFirmasByReunionId != null) {
            return hojaFirmasToUI(hojaFirmasByReunionId);

        }
        return UIEntity.toUI(new Object());
    }

    @GET
    @Path("{reunionId}/hojafirmas/descargar")
    public Response getHojaFirmasDocumento(@PathParam("reunionId") Long reunionId) {
        ReunionHojaFirmas hojaFirmasByReunionId = reunionService.getHojaFirmasByReunionId(reunionId);
        return Response.ok(hojaFirmasByReunionId.getDatos())
                .header("Content-Disposition",
                        "attachment; filename = \"" + hojaFirmasByReunionId.getNombreFichero() + "\"")
                .header("Content-Length", hojaFirmasByReunionId.getDatos().length)
                .header("Content-Type", hojaFirmasByReunionId.getMimeType())
                .build();
    }

    @DELETE
    @Path("{reunionId}/hojafirmas/{hojaFirmasId}/borrar")
    public Response deleteHojaFirmasDocumento(@PathParam("reunionId") Long reunionId,
                                              @PathParam("hojaFirmasId") Long hojaFirmasId)
            throws RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        if (isAdminOAutorizadoEnOrganoReunion(reunionId, connectedUserId)) {
            reunionService.deleteHojaFirmasById(hojaFirmasId);
            return Response.ok().build();
        }
        return Response.serverError().build();
    }

    private boolean isAdminOAutorizadoEnOrganoReunion(Long reunionId, Long connectedUserId)
            throws RolesPersonaExternaException {
        return personaService.isAdmin(
                personaService.getRolesFromPersonaId(connectedUserId)) || organoService.isAutorizadoOrganoEnReunion(
                connectedUserId, reunionId);
    }

    @POST
    @Path("{reunionId}/hojafirmas")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public UIEntity subirHojaFirmas(@PathParam("reunionId") Long reunionId, FormDataMultiPart multiPart)
            throws IOException, NoSuchAlgorithmException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String fileName = "";
        String mimeType = "";
        InputStream data = null;

        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            String mime = bodyPart.getHeaders().getFirst("Content-Type");
            if (mime != null && !mime.isEmpty()) {
                mimeType = mime;
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches()) {
                    fileName = m.group(1);
                }
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                data = bpe.getInputStream();
            }
        }
        ReunionHojaFirmas hojaFirmas = new ReunionHojaFirmas();
        hojaFirmas.setMimeType(mimeType);
        hojaFirmas.setNombreFichero(fileName);
        byte[] datos = StreamUtils.inputStreamToByteArray(data);
        hojaFirmas.setDatos(datos);
        hojaFirmas.setHash(Utils.getHash(datos, personalizationConfig.tipoDeCifrado));
        hojaFirmas.setCreadorId(connectedUserId);
        hojaFirmas.setFechaAdicion(new Date());
        return UIEntity.toUI(reunionService.subirHojaFirmasByReunionId(reunionId, hojaFirmas));
    }

    @POST
    @Path("{reunionId}/publicaracuerdos")
    @Consumes(MediaType.APPLICATION_JSON)
    public void publicarAcuerdos(@PathParam("reunionId") Long reunionId)
            throws OrganosExternosException, PersonasExternasException, IOException, NoSuchAlgorithmException,
            PublicarAcuerdosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.publicarAcuerdos(reunionId, connectedUserId);
    }

    private UIEntity hojaFirmasToUI(ReunionHojaFirmas hojaFirmasByReunionId) {
        UIEntity ui = new UIEntity();
        ui.put("id", hojaFirmasByReunionId.getId());
        ui.put("fechaAdicion", hojaFirmasByReunionId.getFechaAdicion());
        ui.put("mimeType", hojaFirmasByReunionId.getMimeType());
        ui.put("nombreFichero", hojaFirmasByReunionId.getNombreFichero());

        return ui;
    }

    private List<ReunionInvitado> creaInvitadosDesdeInvitadosUI(Long reunionId, UIEntity reunionInvitadosUI) {
        List<ReunionInvitado> invitados = new ArrayList<>();
        Reunion reunion = new Reunion(reunionId);

        List<UIEntity> invitadosUI = reunionInvitadosUI.getRelations().get("invitados");

        if (invitadosUI == null) return new ArrayList<>();

        for (UIEntity invitadoUI : invitadosUI) {
            ReunionInvitado reunionInvitado = new ReunionInvitado();

            reunionInvitado.setReunion(reunion);
            reunionInvitado.setPersonaId(invitadoUI.getLong("personaId"));
            reunionInvitado.setPersonaNombre(invitadoUI.get("personaNombre"));
            reunionInvitado.setPersonaEmail(invitadoUI.get("personaEmail"));
            reunionInvitado.setMotivoInvitacion(invitadoUI.get("motivoInvitacion"));
            reunionInvitado.setAsistencia(invitadoUI.getBoolean("asistencia"));

            invitados.add(reunionInvitado);
        }

        return invitados;
    }

    @PUT
    @Path("{reunionId}/reabrir")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response reabrirReunion(@PathParam("reunionId") Long reunionId, UIEntity uiEntity)
            throws Exception {
        User connectedUser = AccessManager.getConnectedUser(request);

        String motivoReapertura = uiEntity.get("motivoReapertura");

        reunionService.reabrirReunion(reunionId, motivoReapertura, connectedUser);

        return Response.ok().build();
    }

    @GET
    @Path("{reunionId}/procedimientoVoto")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getProcedimientoVotoByReunionId(@PathParam("reunionId") Long reunionId) {
        return procedimientoVotacionResponse(organoService.getTipoProcedimientoVotacionByReunionId(reunionId));
    }

    private UIEntity procedimientoVotacionResponse(String tipoProcedimiento) {
        UIEntity ui = new UIEntity();
        ui.put("tipoProcedimientoVotacion", tipoProcedimiento);
        return ui;
    }

    @PUT
    @Path("{reunionId}/sincronizar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sincronizarMiembrosYcargos(@PathParam("reunionId") Long reunionId)
            throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        reunionService.updateMiembrosYCargosReunion(reunionId, connectedUserId);

        return Response.ok().build();
    }
}
