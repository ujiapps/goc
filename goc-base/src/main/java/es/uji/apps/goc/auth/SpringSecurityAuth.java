package es.uji.apps.goc.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.integrations.CuentasClient;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.services.PersonaService;
import es.uji.commons.sso.User;
import es.uji.commons.sso.dao.SessionDAO;

public class SpringSecurityAuth implements Filter
{
    Logger logger = LoggerFactory.getLogger(DefaulSessionFromPropertiesAuth.class);
    private static final String SESSION_ROLES = "roles";

    private FilterConfig filterConfig = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException
    {
        HttpServletRequest clientRequest = (HttpServletRequest) request;
        HttpServletResponse clientResponse = (HttpServletResponse) response;

        String url = clientRequest.getRequestURI();
        String headerAuthToken = clientRequest.getHeader("X-UJI-AuthToken");
        String headerAuthUser = clientRequest.getHeader("X-UJI-AuthUser");

        if (isValidForDoFilter(clientRequest, url, headerAuthToken, headerAuthUser))
        {
            filterChain.doFilter(request, response);
            return;
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null)
        {
            clientResponse.sendRedirect("/goc/forbidden.jsp");
            filterChain.doFilter(request, response);
            return;
        }

        User user;
        if(authentication.getCredentials() instanceof Persona && isTokenValid(headerAuthToken))
        {
            user = createUserFromPersona((Persona)authentication.getCredentials());
        }
        else
        {
            SAMLCredential credential = (SAMLCredential) authentication.getCredentials();

            AuthConfig authConfig = getAuthConfig();

            String userName = credential.getAttributeAsString(authConfig.userNameAttribute);
            user = createUserFromDefaulLocalValues(userName);
        }
        registerUserInHttpSession(clientRequest, user);

        filterChain.doFilter(request, response);
    }

    private boolean isValidForDoFilter(
        HttpServletRequest clientRequest,
        String url,
        String headerAuthToken,
        String headerAuthUser
    )
    {
        return isExcludedUrl(url) ||
            isCorrectExternalAPICall(url, headerAuthToken, headerAuthUser) ||
            sessionAlreadyRegistered(clientRequest) ||
            isForbidenPage(url) ||
            isCalendarCall(clientRequest) ||
            isCorrectNotificationAPICAll(url, headerAuthToken);
    }

    private boolean isCorrectNotificationAPICAll(
        String url,
        String headerAuthToken
    )
    {
        return url.startsWith("/goc/rest/notificaciones") && isTokenValid(headerAuthToken);
    }

    private boolean isCalendarCall(HttpServletRequest request)
    {
        return request.getRequestURI().contains("calendario") &&
            request.getMethod().equalsIgnoreCase("GET");
    }

    private User createUserFromPersona(Persona persona)
    {
        User user = new User();
        user.setId(persona.getId());
        user.setName(persona.getNombre());
        user.setActiveSession(UUID.randomUUID().toString());

        return user;
    }

    private boolean isForbidenPage(String url)
    {
        return (url != null && url.startsWith("/goc/forbidden.jsp"));
    }

    private boolean sessionAlreadyRegistered(HttpServletRequest clientRequest)
    {
        return clientRequest.getSession().getAttribute("www$persona") != null;
    }

    private User createUserFromDefaulLocalValues(String userName)
            throws IOException
    {
        try
        {
            User user = new User();
            user.setId(getCuentasClient().obtainPersonaIdFrom(userName));
            user.setName(userName);
            user.setActiveSession(UUID.randomUUID().toString());

            return user;
        }
        catch (Exception e)
        {
            throw new IOException("No se ha podido recuperar el id del usuario conectado");
        }
    }

    private void registerUserInHttpSession(HttpServletRequest clientRequest, User user)
    {
        HttpSession serverSession = clientRequest.getSession();
        serverSession.setAttribute("www$persona", user);
        serverSession.setAttribute(SESSION_ROLES, getRolesFromPersonaId(user.getId()));
        PersonalizationConfig personalizationConfig = getPersonalizationConfig();
        serverSession.setMaxInactiveInterval(personalizationConfig.sesionTimeout);
    }

    private SessionDAO getSessionDAO()
    {
        WebApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(filterConfig.getServletContext());
        return context.getBean(SessionDAO.class);
    }

    private CuentasClient getCuentasClient()
    {
        WebApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(filterConfig.getServletContext());
        return context.getBean(CuentasClient.class);
    }

    private AuthConfig getAuthConfig()
    {
        WebApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(filterConfig.getServletContext());
        return context.getBean(AuthConfig.class);
    }

    private PersonalizationConfig getPersonalizationConfig()
    {
        WebApplicationContext context = WebApplicationContextUtils
            .getWebApplicationContext(filterConfig.getServletContext());
        return context.getBean(PersonalizationConfig.class);
    }

    private boolean isCorrectExternalAPICall(
        String url,
        String headerAuthToken,
        String headerAuthUser
    )
    {
        if (headerAuthToken != null && headerAuthUser == null)
        {
            String token = filterConfig.getInitParameter("authToken");

            if (token != null && headerAuthToken.equals(token))
            {
                return true;
            }
        }

        return false;
    }

    private boolean isTokenValid(String headerAuthToken)
    {
        String token = filterConfig.getInitParameter("authToken");

        if (token != null && headerAuthToken.equals(token))
        {
            return true;
        }
        return false;
    }

    @Override
    public void destroy()
    {

    }

    private boolean isExcludedUrl(String url)
    {
        String excludedUrls = filterConfig.getInitParameter("excludedUrls");
        Pattern pattern = Pattern.compile(excludedUrls);
        return pattern.matcher(url).matches();
    }

    private List<String> getRolesFromPersonaId(Long personaId)
    {
        WebApplicationContext context = WebApplicationContextUtils
            .getWebApplicationContext(filterConfig.getServletContext());
        PersonaService personaService = context.getBean(PersonaService.class);
        try
        {
            return personaService.getRolesFromPersonaId(personaId);
        } catch (RolesPersonaExternaException e)
        {
            logger.error("No se ha podido recuperar los roles de la persona con id " + personaId);
        }

        return null;
    }
}
