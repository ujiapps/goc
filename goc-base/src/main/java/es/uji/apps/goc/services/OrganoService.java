package es.uji.apps.goc.services;

import com.google.common.base.Strings;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.*;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.exceptions.OrganoNoDisponibleException;
import es.uji.apps.goc.exceptions.OrganosExternosException;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.JSONListaOrganosExternosDeserializer;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.TipoOrgano;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Component
public class OrganoService {
    @Autowired
    private OrganoDAO organoDAO;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private OrganoAutorizadoDAO organoAutorizadoDAO;

    @Autowired
    private OrganoInvitadoDAO organoInvitadoDAO;

    @Autowired
    private OrganoReunionInvitadoDAO organoReunionInvitadoDAO;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PersonalizationConfig personalizationConfig;

    @Value("${goc.external.authToken}")
    private String authToken;

    @Value("${goc.external.organosEndpoint}")
    private String organosExternosEndpoint;

    public List<Organo> getOrganosByAdminAndAutorizadoId(Long connectedUserId, boolean isAdmin, String searchString)
            throws OrganosExternosException {
        List<Organo> organos = new ArrayList<>();
        List<Organo> todosOrganosExternos = getOrganosExternos();

        if (searchString != null && !searchString.isEmpty()) {
            todosOrganosExternos = todosOrganosExternos.stream().filter(o -> o.getNombreLimpio().toUpperCase().contains(searchString.toUpperCase())).collect(Collectors.toList());
        }

        if (isAdmin) {
            organos.addAll(organoDAO.getOrganosByAdmin(false, searchString));
            organos.addAll(todosOrganosExternos);
        } else {
            organos.addAll(organoDAO.getOrganosByUserId(connectedUserId, searchString));

            List<Organo> organosAutorizado = organoDAO.getOrganosByAutorizadoId(connectedUserId);

            if (searchString != null && !searchString.isEmpty()) {
                organosAutorizado = organosAutorizado.stream().filter(o -> o.getNombreLimpio().toUpperCase().contains(searchString.toUpperCase())).collect(Collectors.toList());
            }

            organos.addAll(organosAutorizado.stream().filter(o -> !organos.contains(o)).collect(Collectors.toList()));

            List<Organo> organosExternos = organoDAO.getOrganosExternosByAutorizadoId(connectedUserId, todosOrganosExternos);
            organos.addAll(organosExternos.stream().filter(o -> !organos.contains(o)).collect(Collectors.toList()));
        }

        return organos;
    }

    public List<Organo> getOrganosPorAutorizadoId(Long connectedUserId, boolean isAdmin)
            throws OrganosExternosException {
        List<Organo> organos = new ArrayList<>();
        List<Organo> organosExternos = getOrganosExternos();

        if (isAdmin) {
            organos.addAll(organosExternos);
            organos.addAll(getOrganosByAdmin(true));
        } else {
            List<String> listaOrganosIdsPermitidos = getOrganosIdsPermitidosAutorizado(connectedUserId);
            organos.addAll(organosExternos.stream().filter(o -> listaOrganosIdsPermitidos.contains(o.getId()))
                    .collect(Collectors.toList()));

            List<Organo> organosLocales = organoDAO.getOrganosByAutorizadoId(connectedUserId);
            organos.addAll(organosLocales.stream().filter(o -> !o.isInactivo()).collect(Collectors.toList()));
        }

        return organos;
    }

    private List<String> getOrganosIdsPermitidosAutorizado(Long connectedUserId) {
        List<OrganoAutorizado> organoAutorizados = organoAutorizadoDAO.getAutorizadosByUserId(connectedUserId);

        return organoAutorizados.stream()
                .filter(c -> c.isOrganoExterno())
                .map(c -> c.getOrganoId())
                .collect(Collectors.toList());
    }

    @Transactional
    public Organo addOrgano(Organo organo, Long connectedUserId)
            throws PersonasExternasException {
        Organo newOrgano = organoDAO.insertOrgano(organo, connectedUserId);

        Persona persona = personaService.getPersonaFromDirectoryByPersonaId(connectedUserId);

        OrganoAutorizado autorizado = new OrganoAutorizado();
        autorizado.setOrganoId(newOrgano.getId());
        autorizado.setOrganoExterno(newOrgano.isExterno());
        autorizado.setSubirDocumentos(true);
        autorizado.setPersonaId(persona.getId());
        autorizado.setPersonaNombre(persona.getNombre());
        autorizado.setPersonaEmail(persona.getEmail());

        addAutorizado(autorizado);

        return newOrgano;
    }

    @Transactional
    public Organo updateOrgano(Long organoId, String nombre, String nombreAlternativo, Long tipoOrganoId,
                               Boolean inactivo, Boolean convocarSinOrdenDia, String email, Boolean actaProvisionalActiva,
                               Boolean delegacionVotoMultiple, Long connectedUserId, String tipoProcedimientoVotacionId,
                               Boolean presidenteVotoDoble, Boolean permiteAbstencionVoto, Boolean verAsistentes, Boolean verDelegaciones)

            throws OrganoNoDisponibleException {
        Organo organo = organoDAO.getOrganoById(organoId);

        if (organo == null) {
            throw new OrganoNoDisponibleException();
        }

        organo.setNombre(nombre);
        organo.setNombreAlternativo(nombreAlternativo);

        TipoOrgano tipoOrgano = new TipoOrgano(tipoOrganoId);
        organo.setTipoOrgano(tipoOrgano);
        organo.setInactivo(inactivo);

        actualizarParametros(organoId.toString(), organo.isExterno(), convocarSinOrdenDia, email, actaProvisionalActiva,
                delegacionVotoMultiple, tipoProcedimientoVotacionId, presidenteVotoDoble, permiteAbstencionVoto, verAsistentes, verDelegaciones);

        return organoDAO.updateOrgano(organo);
    }

    @Transactional
    public void actualizarParametros(
            String organoId,
            Boolean externo,
            Boolean convocarSinOrdenDia,
            String email,
            Boolean actaProvisionalActiva,
            Boolean delegacionVotoMultiple,
            String tipoProcedimientoVotacion,
            Boolean presidenteVotoDoble,
            Boolean permiteAbstencionVoto,
            Boolean verAsistentes,
            Boolean verDelegaciones
    ) {
        organoDAO.insertOrUpdateParametros(organoId, externo, convocarSinOrdenDia, email, actaProvisionalActiva, delegacionVotoMultiple,
                tipoProcedimientoVotacion, presidenteVotoDoble, permiteAbstencionVoto, verAsistentes, verDelegaciones);
    }

    public List<Organo> getOrganosExternos()
            throws OrganosExternosException {
        WebResource getOrganosResource = Client.create().resource(this.organosExternosEndpoint);

        ClientResponse response = getOrganosResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new OrganosExternosException();
        }

        JSONListaOrganosExternosDeserializer jsonDeserializer =
                response.getEntity(JSONListaOrganosExternosDeserializer.class);

        List<OrganoExterno> listaOrganosExternos = jsonDeserializer.getOrganos();
        return organosExternosDTOToOrgano(listaOrganosExternos);
    }

    public List<Organo> getOrganosByReunionId(Long reunionId, Long connectedUserId) {
        List<Organo> organos = new ArrayList<>();

        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        for (OrganoReunion organoReunion : reunion.getReunionOrganos()) {
            organos.add(getOrganoDeOrganoReunion(organoReunion));
        }

        organos.sort((o1, o2) -> o1.getNombre().compareTo(o2.getNombre()));

        return organos;
    }

    private Organo getOrganoDeOrganoReunion(OrganoReunion organoReunion) {
        Organo organo = new Organo();

        organo.setId(organoReunion.getOrganoId());
        organo.setExterno(organoReunion.isExterno());
        organo.setNombre(organoReunion.getOrganoNombre());
        organo.setNombreAlternativo(organoReunion.getOrganoNombreAlternativo());

        TipoOrgano tipoOrgano = new TipoOrgano(organoReunion.getTipoOrganoId());
        organo.setTipoOrgano(tipoOrgano);

        return organo;
    }

    private List<Organo> organosExternosDTOToOrgano(List<OrganoExterno> listaOrganosExternos) {
        List<Organo> organos = new ArrayList<>();

        for (OrganoExterno organoExterno : listaOrganosExternos) {
            Organo organo = organoExternoDTOToOrgano(organoExterno);
            organos.add(organo);
        }

        return organos;
    }

    private Organo organoExternoDTOToOrgano(OrganoExterno organoExternoDTO) {
        Organo organo = new Organo();

        organo.setId(organoExternoDTO.getId());
        organo.setNombre(organoExternoDTO.getNombre());
        organo.setNombreAlternativo(organoExternoDTO.getNombreAlternativo());
        organo.setInactivo(organoExternoDTO.isInactivo());
        organo.setExterno(true);
        OrganoParametro organoParametro =
                organoDAO.getOrganoParametro(organo.getId(), true);

        organo.setPermiteAbstencionVoto(personalizationConfig.permiteAbstencionVoto);
        organo.setConvocarSinOrdenDia(false);
        organo.setActaProvisionalActiva(true);
        organo.setDelegacionVotoMultiple(true);
        organo.setPresidenteVotoDoble(personalizationConfig.presidenteVotoDoble);
        organo.setTipoProcedimientoVotacionEnum(TipoProcedimientoVotacionEnum.ORDINARIO);
        organo.setVerDelegaciones(true);

        if (organoParametro != null) {
            if (organoParametro.getConvocarSinOrdenDia() != null)
                organo.setConvocarSinOrdenDia(organoParametro.getConvocarSinOrdenDia());

            if (organoParametro.getEmail() != null)
                organo.setEmail(organoParametro.getEmail());

            if (organoParametro.getActaProvisionalActiva() != null)
                organo.setActaProvisionalActiva(organoParametro.getActaProvisionalActiva());

            if (organoParametro.getDelegacionVotoMultiple() != null)
                organo.setDelegacionVotoMultiple(organoParametro.getDelegacionVotoMultiple());

            if (organoParametro.getPermiteAbstencionVoto() != null)
                organo.setPermiteAbstencionVoto(organoParametro.getPermiteAbstencionVoto());

            if (!Strings.isNullOrEmpty(organoParametro.getTipoProcedimientoVotacion())) {
                organo.setTipoProcedimientoVotacionEnum(
                        TipoProcedimientoVotacionEnum.valueOf(organoParametro.getTipoProcedimientoVotacion()));
            }

            if (organoParametro.getPresidenteVotoDoble() != null)
                organo.setPresidenteVotoDoble(organoParametro.getPresidenteVotoDoble());

            if (organoParametro.getVerAsistencia() != null)
                organo.setVerAsistencia(organoParametro.getVerAsistencia());

            if (organoParametro.getVerDelegaciones() != null)
                organo.setVerDelegaciones(organoParametro.getVerDelegaciones());
        }

        TipoOrgano tipoOrgano = new TipoOrgano();
        tipoOrgano.setId(organoExternoDTO.getTipoOrganoId());
        tipoOrgano.setNombre(organoExternoDTO.getTipoNombre());
        tipoOrgano.setNombreAlternativo(organoExternoDTO.getTipoNombreAlternativo());
        tipoOrgano.setCodigo(organoExternoDTO.getTipoCodigo());

        organo.setTipoOrgano(tipoOrgano);

        return organo;
    }

    private Boolean isActaProvisionalActiva(
            String organoId,
            boolean ordinario
    ) {
        OrganoParametro organoParametro =
                organoDAO.getOrganoParametro(organoId, ordinario);

        if (organoParametro != null)
            if (organoParametro.getActaProvisionalActiva() != null)
                return organoParametro.getActaProvisionalActiva();

        return true;
    }

    public List<OrganoAutorizado> getAutorizados(String organoId, Boolean externo) {
        return organoAutorizadoDAO.getAutorizadosByOrgano(organoId, externo);
    }

    public OrganoAutorizado addAutorizado(OrganoAutorizado organoAutorizado) {
        List<OrganoAutorizado> listaAutorizados =
                organoAutorizadoDAO.getAutorizadosByOrgano(organoAutorizado.getOrganoId(),
                        organoAutorizado.isOrganoExterno());

        List<OrganoAutorizado> existeAutorizado = listaAutorizados.stream()
                .filter(oa -> oa.getPersonaId().equals(organoAutorizado.getPersonaId()))
                .collect(Collectors.toList());

        if (existeAutorizado.size() == 1) {
            return existeAutorizado.get(0);
        }

        return organoAutorizadoDAO.insert(organoAutorizado);
    }

    @Transactional
    public void removeAutorizado(Long organoAutorizadoId) {
        organoAutorizadoDAO.delete(OrganoAutorizado.class, organoAutorizadoId);
    }

    public List<OrganoInvitado> getInvitados(String organoId, Boolean externo) {
        return organoInvitadoDAO.getInvitadosByOrgano(organoId, externo);
    }

    public List<OrganoReunionInvitado> getInvitadosOrganoByReunion(Long reunionId, String organoId, Boolean externo) {
        return organoReunionInvitadoDAO.getInvitadosOrganoByReunion(reunionId, organoId, externo);
    }

    public void modificaInvitadoOrganoByReunion(UIEntity invitadoUI) {
        UIEntity invitado = invitadoUI.getRelations().get("invitado").get(0);
        OrganoReunionInvitado organoReunionInvitado = organoReunionInvitadoDAO.getInvitadoOrganoById(invitado.getLong("id"));
        organoReunionInvitado.setAsistencia(invitado.getBoolean("asistencia"));
        organoReunionInvitadoDAO.update(organoReunionInvitado);
    }

    public OrganoInvitado addInvitado(OrganoInvitado organoInvitado) {
        List<OrganoInvitado> listaInvitados = organoInvitadoDAO.getInvitadosByOrgano(organoInvitado.getOrganoId(), organoInvitado.isOrganoExterno());

        List<OrganoInvitado> existeInvitado = listaInvitados.stream()
                .filter(oa -> oa.getPersonaId().equals(organoInvitado.getPersonaId()))
                .collect(Collectors.toList());

        if (existeInvitado.size() == 1) {
            return existeInvitado.get(0);
        }

        return organoInvitadoDAO.insert(organoInvitado);
    }

    @Transactional
    public void removeInvitado(Long organoInvitadoId) {
        organoInvitadoDAO.delete(OrganoInvitado.class, organoInvitadoId);
    }

    public void updateInvitado(OrganoInvitado organoInvitado) {
        organoInvitadoDAO.update(organoInvitado);
    }

    public boolean usuarioConPermisosParaConvocarOrganos(List<Organo> organos, Long connectedUserId)
            throws RolesPersonaExternaException {

        Boolean isAdmin = personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador);

        if (isAdmin) {
            return true;
        }

        Boolean permisosAdecuados = true;
        List<OrganoAutorizado> listaPermisosOrganoAutorizado =
                organoAutorizadoDAO.getAutorizadosByUserId(connectedUserId);

        for (Organo organo : organos) {
            Boolean encontrado = false;
            for (OrganoAutorizado organoAutorizado : listaPermisosOrganoAutorizado) {
                if (organoAutorizado.getOrganoId()
                        .equals(organo.getId().toString()) && organoAutorizado.isOrganoExterno()
                        .equals(organo.isExterno())) {
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                permisosAdecuados = false;
                break;
            }
        }

        return permisosAdecuados;
    }

    public Organo getOrganoById(Long organoId) {
        return organoDAO.getOrganoById(organoId);
    }

    public void updateAutorizado(OrganoAutorizado organo) {
        organoAutorizadoDAO.update(organo);
    }

    public List<Organo> getOrganosByAdmin(boolean soloActivos) throws OrganosExternosException {
        List<Organo> organos = new ArrayList<>();
        organos.addAll(getOrganosExternos());

        List<Organo> organosAutorizado = organoDAO.getOrganosByAdmin(soloActivos, null);
        organos.addAll(organosAutorizado.stream().filter(o -> !organos.contains(o)).collect(Collectors.toList()));

        return organos;
    }

    public boolean isConvocarSinOrdenDiaActivo(String organoId, Boolean ordinario) {
        OrganoParametro organoParametro =
                organoDAO.getOrganoParametro(organoId, ordinario);

        return organoParametro != null && organoParametro.getConvocarSinOrdenDia();
    }

//    public boolean verAsistentes(String organoId,Boolean ordinario) {
//    	OrganoParametro organoParametro =
//                organoDAO.getOrganoParametro(organoId, ordinario);
//
//            return organoParametro != null && organoParametro.getVerAsistencia();
//    }

    public Boolean isAutorizadoOrganoEnReunion(Long connectedUserId, Long reunionId) {
        List<OrganoAutorizado> autorizadosByReunionId = organoAutorizadoDAO.getAutorizadosByReunionId(reunionId);
        List<Long> personasIdDeAutorizados = autorizadosByReunionId.stream().map(autorizadoByReunionId -> autorizadoByReunionId.getPersonaId()).collect(Collectors.toList());

        return personasIdDeAutorizados.contains(connectedUserId);
    }

    public String getTipoProcedimientoVotacionByReunionId(Long reunionId) {
        List<String> tipoProcedimiento = organoDAO.getTipoProcedimientoByReunionId(reunionId);

        if (tipoProcedimiento.size() == 0) {
            return null;
        }

        return tipoProcedimiento.get(0);
    }
}
