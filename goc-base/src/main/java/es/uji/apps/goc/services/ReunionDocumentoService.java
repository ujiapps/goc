package es.uji.apps.goc.services;

import es.uji.apps.goc.Utils;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.*;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.exceptions.EntidadNoValidaException;
import es.uji.commons.rest.StreamUtils;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Component
public class ReunionDocumentoService {
    private static final int ONE_DAY = 1000 * 60 * 60 * 24;
    private static Logger log = LoggerFactory.getLogger(ReunionDocumentoService.class);
    @Autowired
    private ReunionDocumentoDAO reunionDocumentoDAO;

    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    private PuntoOrdenDiaDocumentoDAO puntoOrdenDiaDocumentoDAO;

    @Autowired
    private PuntoOrdenDiaAcuerdoDAO puntoOrdenDiaAcuerdoDAO;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private ModificacionDocumentoDAO modificacionDocumentoDAO;

    @Autowired
    private PersonalizationConfig personalizationConfig;

    public List<ReunionDocumento> getDocumentosByReunionId(Long reunionId, Long connectedUserId) {
        return reunionDocumentoDAO.getDatosDocumentosByReunionId(reunionId);
    }

    public ReunionDocumento getDocumentoById(Long documentoId) {
        return reunionDocumentoDAO.getDocumentoById(documentoId);
    }

    public void borrarDocumento(Long documentoId, Long reunionId, Long connectedUserId) {
        reunionDocumentoDAO.delete(ReunionDocumento.class, documentoId);
    }

    public void borrarDocumentoOrdenDia(Long documentoId, Long reunionId, Long connectedUserId) {
        if (isCreadorDocumentoOrdenDia(documentoId, connectedUserId)) {
            reunionDocumentoDAO.delete(PuntoOrdenDiaDocumento.class, documentoId);
        }
    }

    private boolean isCreadorDocumentoOrdenDia(Long documentoDiaId, Long connectedUserId) {
        return puntoOrdenDiaDocumentoDAO.getDatosDocumentoById(documentoDiaId).getCreadorId().equals(connectedUserId);
    }

    public ReunionDocumento addDocumento(Long reunionId, String fileName, String descripcion,
                                         String descripcionAlternativa, String mimeType, InputStream data,
                                         Long connectedUserId) throws IOException, EntidadNoValidaException, NoSuchAlgorithmException {

        ReunionDocumento reunionDocumento = new ReunionDocumento();
        Reunion reunion = new Reunion(reunionId);
        reunionDocumento.setReunion(reunion);
        byte[] datos = StreamUtils.inputStreamToByteArray(data);
        reunionDocumento.setDatos(datos);
        reunionDocumento.setHash(Utils.getHash(datos, personalizationConfig.tipoDeCifrado));
        reunionDocumento.setMimeType(mimeType);
        reunionDocumento.setNombreFichero(fileName);
        reunionDocumento.setDescripcion(descripcion);
        reunionDocumento.setDescripcionAlternativa(descripcionAlternativa);
        reunionDocumento.setFechaAdicion(new Date());
        reunionDocumento.setCreadorId(connectedUserId);
        reunionDocumento.setEditorId(null);
        reunionDocumento.setFechaEdicion(null);
        reunionDocumento.setMotivoEdicion(null);
        reunionDocumento.setActivo(true);

        Long ultimo = reunionDocumentoDAO.getOrdenUltimoDocumento(reunionId);

        if (ultimo == null) {
            reunionDocumento.setOrden(10L);
        } else {
            reunionDocumento.setOrden(ultimo + 10L);
        }

        try {
            return reunionDocumentoDAO.insert(reunionDocumento);
        } catch (DataIntegrityViolationException e) {
            log.error("ERROR", e);
            throw new EntidadNoValidaException();
        }
    }

    public ReunionDocumento updateDocumento(Long documentoId, String fileName, String mimeType, InputStream data,
                                            Long connectedUserId)
            throws IOException, NoSuchAlgorithmException {

        ReunionDocumento documentoAnterior = reunionDocumentoDAO.getDocumentoById(documentoId);

        byte[] datos = StreamUtils.inputStreamToByteArray(data);
        documentoAnterior.setDatos(datos);
        documentoAnterior.setHash(Utils.getHash(datos, personalizationConfig.tipoDeCifrado));
        documentoAnterior.setMimeType(mimeType);
        documentoAnterior.setNombreFichero(fileName);
        documentoAnterior.setFechaAdicion(new Date());
        documentoAnterior.setCreadorId(connectedUserId);
        documentoAnterior.setEditorId(connectedUserId);
        documentoAnterior.setFechaEdicion(new Date());

        return reunionDocumentoDAO.update(documentoAnterior);
    }

    public String getDocumentosReunionZip(Long reunionId) throws IOException {
        List<PuntoOrdenDiaMultinivel> puntos = getPuntosOrdenDiaOrdeanosPorNivel(reunionId);
        String uuid = generarZip(reunionId, puntos);

        return uuid;
    }

    private String formatearParaZip(String texto) {
        return texto.replaceAll("/", "-");
    }

    private String generarZip(Long reunionId, List<PuntoOrdenDiaMultinivel> puntos) throws IOException {
        String uuid = UUID.randomUUID().toString();
        String zipPath = personalizationConfig.basePathDocumentacion + '/' + uuid + '/' + uuid;
        File directorioBase = crearDirectorioBase(uuid);
        ZipArchiveOutputStream zout = new ZipArchiveOutputStream(new FileOutputStream(zipPath));
        zout.setEncoding("UTF-8");
        zout.setCreateUnicodeExtraFields(ZipArchiveOutputStream.UnicodeExtraFieldPolicy.ALWAYS);


        Reunion reunion = reunionDAO.getReunionById(reunionId);
        String basePath = formatearParaZip(reunion.getAsunto());
        anyadirDocumentosReunion(reunionId, directorioBase, zout, basePath);
        anyadirNivel(puntos, directorioBase, zout, basePath, 0);
        zout.close();

        return zipPath;
    }

    private void anyadirDocumentosReunion(
            Long reunionId,
            File directorioBase,
            ZipArchiveOutputStream zout,
            String path
    ) throws IOException {
        List<ReunionDocumento> documentosByReunionId = reunionDocumentoDAO.getDocumentosActivosByReunionId(reunionId);
        for (ReunionDocumento reunionDocumento : documentosByReunionId) {
            File file = new File(directorioBase.getAbsolutePath() + "/" + reunionDocumento.getNombreFichero());
            addContent(file, reunionDocumento.getDatos());
            addFileToZip(file, zout, path);
        }
    }

    private File crearDirectorioBase(String uuid) {
        File baseDoc = new File(personalizationConfig.basePathDocumentacion);
        if (!baseDoc.exists()) {
            baseDoc.mkdir();
            log.info("Crando el directorio para almacenar la documentación a descargar: " + personalizationConfig.basePathDocumentacion);
        }

        File base = new File(personalizationConfig.basePathDocumentacion + '/' + uuid);
        if (!base.exists()) {
            base.mkdir();
        } else {
            base.delete();
            base.mkdir();
        }

        return base;
    }

    @Transactional
    public void subeOrdenDocumentos(Long reunionId, Long documentoId, Long connectedUserId) {
        ReunionDocumento documento = reunionDocumentoDAO.getDatosDocumentoById(documentoId);
        ReunionDocumento documentoAnterior = reunionDocumentoDAO.getDocumentoAnterior(reunionId, documento.getOrden());

        if (documentoAnterior != null) {
            Long orden = documento.getOrden();
            reunionDocumentoDAO.cambiarOrden(documentoId, documentoAnterior.getOrden());
            reunionDocumentoDAO.cambiarOrden(documentoAnterior.getId(), orden);
        }

    }

    @Transactional
    public void bajaOrdenDocumentos(Long reunionId, Long documentoId, Long connectedUserId) {
        ReunionDocumento documento = reunionDocumentoDAO.getDatosDocumentoById(documentoId);
        ReunionDocumento documentoPosterior = reunionDocumentoDAO.getDocumentoPosterior(reunionId, documento.getOrden());

        if (documentoPosterior != null) {
            Long orden = documento.getOrden();
            reunionDocumentoDAO.cambiarOrden(documentoId, documentoPosterior.getOrden());
            reunionDocumentoDAO.flush();
            reunionDocumentoDAO.cambiarOrden(documentoPosterior.getId(), orden);
            reunionDocumentoDAO.flush();
        }
    }

    @Scheduled(cron = "0 59 23 * * ?")
    public void eliminarDocumentosAntiguos() {
        if (personalizationConfig.basePathDocumentacion != null && !personalizationConfig.basePathDocumentacion.trim().equals("")) {
            LocalDate today = LocalDate.now();
            LocalDate earlier = today.minusDays(1);

            Date threshold = Date.from(earlier.atStartOfDay(ZoneId.systemDefault()).toInstant());
            AgeFileFilter filter = new AgeFileFilter(threshold);

            File path = new File(personalizationConfig.basePathDocumentacion);

            File[] oldFolders = FileFilterUtils.filter(filter, path.listFiles());

            for (File folder : oldFolders) {
                FileUtils.deleteQuietly(folder);
            }
        }
    }

    private void anyadirNivel(
            List<PuntoOrdenDiaMultinivel> puntos,
            File directorioBase,
            ZipArchiveOutputStream zout,
            String path,
            int prefix
    ) throws IOException {
        int sufix = 1;

        if (puntos != null && !puntos.isEmpty()) {
            for (PuntoOrdenDiaMultinivel puntoOrdenDiaMultinivel : puntos) {
                String puntoOrdenDiaTitulo = PuntoOrdenDiaMultinivel.getIndex(prefix, sufix);
                File directorio = anyadirDirectorio(puntoOrdenDiaTitulo, directorioBase);
                directorio.mkdir();
                String pathPunto = path + "/" + puntoOrdenDiaTitulo;
                anyadirDocumentos(puntoOrdenDiaMultinivel.getId(), directorio, zout, pathPunto);
                anyadirNivel(puntoOrdenDiaMultinivel.getPuntosInferioresAsOrderedList(), directorio, zout, pathPunto, sufix);
                sufix++;
            }
        }
    }

    private void anyadirDocumentos(
            Long puntoOrdeDiaMultinivelId,
            File directorio,
            ZipArchiveOutputStream zout,
            String path
    ) throws IOException {
        List<PuntoOrdenDiaDocumento> documentos =
                puntoOrdenDiaDocumentoDAO.getDocumentosActivosByPuntoOrdenDiaId(puntoOrdeDiaMultinivelId);
        List<PuntoOrdenDiaAcuerdo> acuerdos =
                puntoOrdenDiaAcuerdoDAO.getAcuerdosActivosByPuntoOrdenDiaId(puntoOrdeDiaMultinivelId);

        for (PuntoOrdenDiaDocumento puntoOrdenDiaDocumento : documentos) {
            File documento =
                    new File(directorio.getAbsolutePath() + "/" + puntoOrdenDiaDocumento.getNombreFichero());
            addContent(documento, puntoOrdenDiaDocumento.getDatos());
            addFileToZip(documento, zout, path);
        }
        for (PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo : acuerdos) {
            File acuerdo = new File(directorio.getAbsolutePath() + "/" + puntoOrdenDiaAcuerdo.getNombreFichero());
            addContent(acuerdo, puntoOrdenDiaAcuerdo.getDatos());
            addFileToZip(acuerdo, zout, path);
        }
    }

    private File anyadirDirectorio(String puntoOrdenDiaMultinivelTitulo, File directorioBase) {
        File directorioPunto = new File(directorioBase.getAbsolutePath() + "/" + puntoOrdenDiaMultinivelTitulo);
        directorioPunto.mkdir();
        return directorioPunto;
    }

    private List<PuntoOrdenDiaMultinivel> getPuntosOrdenDiaOrdeanosPorNivel(Long reunionId) {
        return puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateado(reunionId);
    }

    private void addFileToZip(File file, ZipArchiveOutputStream zout, String path) throws IOException {
        byte[] tmpBuf = new byte[1024];
        FileInputStream in = new FileInputStream(file.getAbsolutePath());
        String pathDocumento = path + "/" + file.getName();
        ZipArchiveEntry ze = new ZipArchiveEntry(pathDocumento);
        zout.putArchiveEntry(ze);
        int len;
        while ((len = in.read(tmpBuf)) > 0) {
            zout.write(tmpBuf, 0, len);
        }
        zout.closeArchiveEntry();
        in.close();
    }

    private void addContent(
            File file,
            byte[] datos
    ) throws IOException {
        OutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(datos);
        fileOutputStream.close();
    }

    @Transactional
    public void cambiarMotivoEdicion(Long documentoId, String motivoEdicion, Long connectedUserId) {
        reunionDocumentoDAO.updateMotivoEdicion(documentoId, motivoEdicion, connectedUserId);
    }

    @Transactional
    public void cambiarActivo(Long documentoId, Long connectedUserId) {
        reunionDocumentoDAO.disable(documentoId, connectedUserId);
    }

    public boolean hayAlgunaDocumentacion(Long reunionId) {
        return reunionDocumentoDAO.tieneDocumentosReunionOPuntosOrdenDia(reunionId);
    }

    public void guardarModificacionDocumento(Long documentoId, Long connectedUserId, String motivoEdicion) {
        ModificacionDocumento modificacionDocumento = new ModificacionDocumento();
        modificacionDocumento.setDocumentoId(documentoId);
        modificacionDocumento.setEditorId(connectedUserId);
        modificacionDocumento.setFechaEdicion(new Date());
        modificacionDocumento.setMotivoEdicion(motivoEdicion);
        modificacionDocumentoDAO.insert(modificacionDocumento);
    }
}
