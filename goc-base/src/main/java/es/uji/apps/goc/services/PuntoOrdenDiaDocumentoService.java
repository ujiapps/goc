package es.uji.apps.goc.services;

import es.uji.apps.goc.Utils;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.ModificacionDocumentoDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaAcuerdoDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDocumentoDAO;
import es.uji.apps.goc.dto.ModificacionDocumento;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.exceptions.EntidadNoValidaException;
import es.uji.apps.goc.model.DocumentoUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

@Service
@Component
public class PuntoOrdenDiaDocumentoService {
    private static Logger log = LoggerFactory.getLogger(PuntoOrdenDiaDocumentoService.class);

    @Autowired
    private PuntoOrdenDiaDocumentoDAO puntoOrdenDiaDocumentoDAO;

    @Autowired
    private PuntoOrdenDiaAcuerdoDAO puntoOrdenDiaAcuerdoDAO;

    @Autowired
    private ModificacionDocumentoDAO modificacionDocumentoDAO;

    @Autowired
    private PersonalizationConfig personalizationConfig;

    public Long getNumeroDocumentosPorPuntoOrdenDia(Long puntoOrdenDiaId, Long connectedUserId) {
        return puntoOrdenDiaDocumentoDAO.getNumeroDocumentosPorPuntoOrdenDia(puntoOrdenDiaId);
    }

    public Long getNumeroAcuerdosPorPuntoOrdenDia(Long puntoOrdenDiaId, Long connectedUserId) {
        return puntoOrdenDiaAcuerdoDAO.getNumeroAcuerdosPorPuntoOrdenDia(puntoOrdenDiaId);
    }

    public PuntoOrdenDiaDocumento addDocumento(Long puntoOrdenDiaId, DocumentoUI documento, Long connectedUserId, boolean subirDesdeFicha)
            throws EntidadNoValidaException, IOException, NoSuchAlgorithmException {
        PuntoOrdenDiaDocumento puntoOrdenDiaDocumento = new PuntoOrdenDiaDocumento();
        PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDia(puntoOrdenDiaId);
        puntoOrdenDiaDocumento.setPuntoOrdenDia(puntoOrdenDia);
        puntoOrdenDiaDocumento.setDatos(documento.getData());
        puntoOrdenDiaDocumento.setHash(Utils.getHash(documento.getData(), personalizationConfig.tipoDeCifrado));
        puntoOrdenDiaDocumento.setMimeType(documento.getMimeType());
        puntoOrdenDiaDocumento.setNombreFichero(documento.getNombreFichero());
        puntoOrdenDiaDocumento.setDescripcion(documento.getDescripcion());
        puntoOrdenDiaDocumento.setDescripcionAlternativa(documento.getDescripcionAlternativa());
        puntoOrdenDiaDocumento.setFechaAdicion(new Date());
        puntoOrdenDiaDocumento.setCreadorId(connectedUserId);
        puntoOrdenDiaDocumento.setPublico(documento.getPublico());
        puntoOrdenDiaDocumento.setEditorId(null);
        puntoOrdenDiaDocumento.setFechaEdicion(null);
        puntoOrdenDiaDocumento.setMotivoEdicion(null);
        puntoOrdenDiaDocumento.setActivo(true);

        Long ultimo = puntoOrdenDiaDocumentoDAO.getOrdenUltimoDocumento(puntoOrdenDiaId);

        if (ultimo == null) {
            puntoOrdenDiaDocumento.setOrden(10L);
        } else {
            puntoOrdenDiaDocumento.setOrden(ultimo + 10L);
        }

        if (subirDesdeFicha) {
            puntoOrdenDiaDocumento.setMostrarEnFicha(true);
        } else {
            if (puntoOrdenDiaDocumento.getMostrarEnFicha() == null) {
                puntoOrdenDiaDocumento.setMostrarEnFicha(personalizationConfig.mostrarDocumentoEnFicha);
            }
        }

        try {
            return puntoOrdenDiaDocumentoDAO.insert(puntoOrdenDiaDocumento);
        } catch (DataIntegrityViolationException e) {
            log.error("ERROR", e);
            throw new EntidadNoValidaException();
        }

    }

    public PuntoOrdenDiaAcuerdo addAcuerdo(PuntoOrdenDia puntoOrdenDia, DocumentoUI documento, Long connectedUserId) throws IOException, NoSuchAlgorithmException {
        PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo = new PuntoOrdenDiaAcuerdo();
        puntoOrdenDiaAcuerdo.setPuntoOrdenDia(puntoOrdenDia);
        puntoOrdenDiaAcuerdo.setDatos(documento.getData());
        puntoOrdenDiaAcuerdo.setHash(Utils.getHash(documento.getData(), personalizationConfig.tipoDeCifrado));
        puntoOrdenDiaAcuerdo.setMimeType(documento.getMimeType());
        puntoOrdenDiaAcuerdo.setNombreFichero(documento.getNombreFichero());
        puntoOrdenDiaAcuerdo.setDescripcion(documento.getDescripcion());
        puntoOrdenDiaAcuerdo.setDescripcionAlternativa(documento.getDescripcionAlternativa());
        puntoOrdenDiaAcuerdo.setFechaAdicion(new Date());
        puntoOrdenDiaAcuerdo.setCreadorId(connectedUserId);
        puntoOrdenDiaAcuerdo.setPublico(puntoOrdenDia.getPublico());
        puntoOrdenDiaAcuerdo.setEditorId(null);
        puntoOrdenDiaAcuerdo.setFechaEdicion(null);
        puntoOrdenDiaAcuerdo.setMotivoEdicion(null);
        puntoOrdenDiaAcuerdo.setActivo(true);

        Long ultimo = puntoOrdenDiaAcuerdoDAO.getUltimoAcuerdo(puntoOrdenDia.getId());

        if (ultimo == null) {
            puntoOrdenDiaAcuerdo.setOrden(10L);
        } else {
            puntoOrdenDiaAcuerdo.setOrden(ultimo + 10L);
        }

        return puntoOrdenDiaAcuerdoDAO.insert(puntoOrdenDiaAcuerdo);
    }

    @Transactional
    public void subeOrdenDocumento(Long puntoId, Long docId) {
        PuntoOrdenDiaDocumento documento = puntoOrdenDiaDocumentoDAO.getDatosDocumentoById(docId);
        PuntoOrdenDiaDocumento anterior = puntoOrdenDiaDocumentoDAO.getDocumentoAnterior(puntoId, documento.getOrden());
        if (anterior != null) {
            puntoOrdenDiaDocumentoDAO.cambiarOrdenDocumento(docId, anterior.getOrden());
            puntoOrdenDiaAcuerdoDAO.flush();
            puntoOrdenDiaDocumentoDAO.cambiarOrdenDocumento(anterior.getId(), documento.getOrden());
            puntoOrdenDiaDocumentoDAO.flush();
        }
    }

    @Transactional
    public void bajaOrdenDocumento(Long puntoId, Long docId) {
        PuntoOrdenDiaDocumento documento = puntoOrdenDiaDocumentoDAO.getDatosDocumentoById(docId);
        PuntoOrdenDiaDocumento posterior = puntoOrdenDiaDocumentoDAO.getDocumentoPosterior(puntoId, documento.getOrden());
        if (posterior != null) {
            puntoOrdenDiaDocumentoDAO.cambiarOrdenDocumento(docId, posterior.getOrden());
            puntoOrdenDiaDocumentoDAO.cambiarOrdenDocumento(posterior.getId(), documento.getOrden());
        }
    }

    @Transactional
    public void subeOrdenAcuerdo(Long puntoId, Long docId) {
        PuntoOrdenDiaAcuerdo documento = puntoOrdenDiaAcuerdoDAO.getDatosAcuerdoById(docId);
        PuntoOrdenDiaAcuerdo anterior = puntoOrdenDiaAcuerdoDAO.getAcuerdoAnterior(puntoId, documento.getOrden());
        if (anterior != null) {
            puntoOrdenDiaAcuerdoDAO.cambiarOrdenAcuerdo(docId, anterior.getOrden());
            puntoOrdenDiaAcuerdoDAO.flush();
            puntoOrdenDiaAcuerdoDAO.cambiarOrdenAcuerdo(anterior.getId(), documento.getOrden());
            puntoOrdenDiaAcuerdoDAO.flush();
        }
    }

    @Transactional
    public void bajaOrdenAcuerdo(Long puntoId, Long docId) {
        PuntoOrdenDiaAcuerdo documento = puntoOrdenDiaAcuerdoDAO.getDatosAcuerdoById(docId);
        PuntoOrdenDiaAcuerdo posterior = puntoOrdenDiaAcuerdoDAO.getAcuerdoPosterior(puntoId, documento.getOrden());
        if (posterior != null) {
            puntoOrdenDiaAcuerdoDAO.cambiarOrdenAcuerdo(docId, posterior.getOrden());
            puntoOrdenDiaAcuerdoDAO.cambiarOrdenAcuerdo(posterior.getId(), documento.getOrden());
        }
    }

    public void borrarDocumento(Long documentoId) {
        puntoOrdenDiaDocumentoDAO.delete(PuntoOrdenDiaDocumento.class, documentoId);
    }

    public void borrarAcuerdo(Long documentoId) {
        puntoOrdenDiaAcuerdoDAO.delete(PuntoOrdenDiaAcuerdo.class, documentoId);
    }

    @Transactional
    public void inhabilitarDocumento(Long documentoId, Long connectedUserId) {
        puntoOrdenDiaDocumentoDAO.disable(documentoId, connectedUserId);
    }

    @Transactional
    public void inhabilitarAcuerdo(Long documentoId, Long connectedUserId) {
        puntoOrdenDiaAcuerdoDAO.disable(documentoId, connectedUserId);
    }

    @Transactional
    public void cambiarMotivoEdicionDocumento(Long documentoId, Long connectedUserId, String motivoEdicion) {
        puntoOrdenDiaDocumentoDAO.updateMotivoEdicion(documentoId, motivoEdicion, connectedUserId);
    }

    @Transactional
    public void cambiarMotivoEdicionAcuerdo(Long documentoId, Long connectedUserId, String motivoEdicion) {
        puntoOrdenDiaAcuerdoDAO.updateMotivoEdicion(documentoId, motivoEdicion, connectedUserId);
    }

    public PuntoOrdenDiaDocumento getDocumentoById(Long documentoId) {
        return puntoOrdenDiaDocumentoDAO.getDocumentoById(documentoId);
    }

    public PuntoOrdenDiaAcuerdo getAcuerdoById(Long acuerdoId) {
        return puntoOrdenDiaAcuerdoDAO.getAcuerdoById(acuerdoId);
    }

    public List<PuntoOrdenDiaDocumento> getDatosDocumentosByPuntoOrdenDiaId(Long puntoOrdenDiaId, Long connectedUserId) {
        return puntoOrdenDiaDocumentoDAO.getDatosDocumentosByPuntoOrdenDiaId(puntoOrdenDiaId, false);
    }

    public List<PuntoOrdenDiaAcuerdo> getDatosAcuerdosByPuntoOrdenDiaId(Long puntoOrdenDiaId, Long connectedUserId) {
        return puntoOrdenDiaAcuerdoDAO.getDatosAcuerdosByPuntoOrdenDiaId(puntoOrdenDiaId);
    }

    public PuntoOrdenDiaAcuerdo updateAcuerdo(PuntoOrdenDiaAcuerdo acuerdo, DocumentoUI documentoUI)
            throws IOException, NoSuchAlgorithmException {
        if (documentoUI != null) {
            acuerdo.setDatos(documentoUI.getData());
            acuerdo.setHash(Utils.getHash(documentoUI.getData(), personalizationConfig.tipoDeCifrado));
            acuerdo.setMimeType(documentoUI.getMimeType());
            acuerdo.setNombreFichero(documentoUI.getNombreFichero());
        }
        return puntoOrdenDiaAcuerdoDAO.update(acuerdo);
    }

    public PuntoOrdenDiaDocumento updateDocumento(PuntoOrdenDiaDocumento documento, DocumentoUI documentoUI)
            throws IOException, NoSuchAlgorithmException {
        if (documentoUI != null) {
            documento.setDatos(documentoUI.getData());
            documento.setHash(Utils.getHash(documentoUI.getData(), personalizationConfig.tipoDeCifrado));
            documento.setMimeType(documentoUI.getMimeType());
            documento.setNombreFichero(documentoUI.getNombreFichero());
            documento.setFechaAdicion(new Date());
        }
        return puntoOrdenDiaDocumentoDAO.update(documento);
    }

    @Transactional
    public void updatePrivacidadAcuerdos(Long puntoOrdenDiaId, Boolean publico) {
        puntoOrdenDiaAcuerdoDAO.updatePrivacidadAcuerdosPunto(puntoOrdenDiaId, publico);
    }

    public void guardarModificacionDocumento(Long documentoId, Long connectedUserId, String motivoEdicion) {
        ModificacionDocumento modificacionDocumento = new ModificacionDocumento();
        modificacionDocumento.setDocumentoId(documentoId);
        modificacionDocumento.setEditorId(connectedUserId);
        modificacionDocumento.setFechaEdicion(new Date());
        modificacionDocumento.setMotivoEdicion(motivoEdicion);
        modificacionDocumentoDAO.insert(modificacionDocumento);
    }
}
