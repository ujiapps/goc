package es.uji.apps.goc.services.rest;

import es.uji.apps.goc.enums.PuntosOrdenDiaTiposEnum;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("puntosordendia")
public class PuntosOrdenDiaResource extends CoreBaseService {

    @GET
    @Path("/tipos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPuntosOrdenDiaTiposToUi()
    {
        List<UIEntity> puntosOrdenDiaTiposUI = new ArrayList<>();

        for(PuntosOrdenDiaTiposEnum tipo : PuntosOrdenDiaTiposEnum.values()){
            UIEntity uiEntity = new UIEntity();
            uiEntity.put("id", tipo.toString());
            puntosOrdenDiaTiposUI.add(uiEntity);
        }

      return puntosOrdenDiaTiposUI;
    }
}
