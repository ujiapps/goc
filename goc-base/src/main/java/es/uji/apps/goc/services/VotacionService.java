package es.uji.apps.goc.services;

import com.google.common.base.Strings;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.OrganoDAO;
import es.uji.apps.goc.dao.OrganoReunionMiembroDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dao.VotacionDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.enums.CodigoCargoEnum;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.enums.TipoVotacionEnum;
import es.uji.apps.goc.enums.TipoVotoEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.*;
import es.uji.apps.goc.model.punto.PuntoVotable;
import es.uji.apps.goc.model.punto.PuntoVotableActualizar;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class VotacionService {
    @Autowired
    private VotacionDAO votacionDAO;

    @Autowired
    private OrganoDAO organoDAO;

    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    private OrganoReunionMiembroDAO organoReunionMiembroDAO;

    @Autowired
    private OrganoReunionMiembroService organoReunionMiembroService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PersonalizationConfig personalizationConfig;

    @Autowired
    private ReunionService reunionService;

    @Autowired
    private PuntoOrdenDiaService puntoOrdenDiaService;

    public void votarPuntoTitular(Persona persona, PuntoVotable puntoAVotar)
            throws UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            UsuarioNoTienePermisoParaVotarException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        OrganoReunionMiembro organoReunionMiembro =
                organoReunionMiembroDAO.getOrganoReunionMiembroByPersonaAndReunionId(persona, puntoAVotar.getReunionAVotar().getReunionId());
        if (organoReunionMiembro != null) {
            if (organoReunionMiembro.getDelegadoVotoId() == null) {
                votarPunto(persona, puntoAVotar, organoReunionMiembro);
            } else {
                throw new VotoDelegadoException();
            }
        } else {
            throw new UsuarioNoTienePermisoParaVotarException();
        }
    }

    public void votarPuntoDelegado(Persona persona, Long representadoReunionMiembroId, PuntoVotable puntoAVotar)
            throws UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            UsuarioNoTienePermisoParaVotarException, VotoDelegadoException, AsistenciaNoConfirmadaException,
            VotoNoDelegadoException, VotoException {
        OrganoReunionMiembro organoReunionMiembro = organoReunionMiembroDAO.getMiembroById(representadoReunionMiembroId);
        if (organoReunionMiembro != null) {
            if (organoReunionMiembro.getDelegadoVotoId() != null) {
                votarPunto(persona, puntoAVotar, organoReunionMiembro);
            } else {
                throw new VotoNoDelegadoException();
            }
        } else {
            throw new UsuarioNoTienePermisoParaVotarException();
        }
    }

    public void votarPuntoPresencial(Long miembroVotoPresencialId, PuntoVotable puntoAVotar)
            throws UsuarioYaHaVotadoSoloSePermiteActualizacionException, VotoDelegadoException, AsistenciaNoConfirmadaException,
            UsuarioNoTienePermisoParaVotarException, PersonasExternasException, NumberFormatException, VotoException {
        OrganoReunionMiembro organoReunionMiembro = organoReunionMiembroDAO.getMiembroById(miembroVotoPresencialId);

        Long personaId = organoReunionMiembro.getDelegadoVotoId() != null ? organoReunionMiembro.getDelegadoVotoId() : (organoReunionMiembro.getSuplenteId() != null ? organoReunionMiembro.getSuplenteId() : ParamUtils.parseLong(organoReunionMiembro.getMiembroId()));

        Persona persona = personaService.getPersonaFromDirectoryByPersonaId(personaId);
        votarPunto(persona, puntoAVotar, organoReunionMiembro);
    }

    public PuntoSecretoVotosDTO getPuntoSecretoVotosByPuntoId(Long puntoId) {
        return votacionDAO.getPuntoSecretoVotosByPuntoId(puntoId);
    }

    public void guardarVotacionPuntoSecreto(Long puntoId, Long votosFavor, Long votosContra, Long votosBlanco) {
        PuntoSecretoVotosDTO puntoSecretoVotos = votacionDAO.getPuntoSecretoVotosByPuntoId(puntoId);

        if (puntoSecretoVotos == null) {
            PuntoSecretoVotosDTO puntoSecreto = new PuntoSecretoVotosDTO();
            PuntoOrdenDia punto = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoId);
            puntoSecreto.setPuntoOrdenDia(punto);
            puntoSecreto.setVotosFavor(votosFavor);
            puntoSecreto.setVotosContra(votosContra);
            puntoSecreto.setVotosBlanco(votosBlanco);

            votacionDAO.insertPuntoSecretoVotos(puntoSecreto);
        } else {
            puntoSecretoVotos.setVotosFavor(votosFavor);
            puntoSecretoVotos.setVotosContra(votosContra);
            puntoSecretoVotos.setVotosBlanco(votosBlanco);

            votacionDAO.updatePuntoSecretoVotos(puntoSecretoVotos);
        }
    }

    private void votarPunto(
            Persona persona,
            PuntoVotable puntoAVotar,
            OrganoReunionMiembro organoReunionMiembro
    ) throws UsuarioYaHaVotadoSoloSePermiteActualizacionException, VotoDelegadoException,
            AsistenciaNoConfirmadaException, VotoException {
        if (!puntoAVotar.getPuntoOrdenDia().getTipoVotacion().equals(TipoVotacionEnum.PRESENCIAL.toString())) {
            if (reunionMiembroHasVotoEmitidoPunto(puntoAVotar, organoReunionMiembro)) {
                throw new UsuarioYaHaVotadoSoloSePermiteActualizacionException();
            } else {
                votarPuntoAbierto(persona, puntoAVotar, organoReunionMiembro);
            }
        } else {
            votarPuntoAbierto(persona, puntoAVotar, organoReunionMiembro);
        }
    }

    private boolean reunionMiembroHasVotoEmitidoPunto(
            PuntoVotable puntoAVotar,
            OrganoReunionMiembro organoReunionMiembro
    ) {
        return
                votacionDAO.getVotoByMiembroAndPuntoId(organoReunionMiembro.getId(), puntoAVotar.getPuntoOrdenDia().getId())
                        != null;
    }


    private PersonaPuntoVotoDTO votarPuntoAbierto(Persona persona, PuntoVotable puntoAVotar, OrganoReunionMiembro organoReunionMiembro)
            throws VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        PersonaPuntoVoto personaPuntoVoto = new PersonaPuntoVoto(persona, puntoAVotar, organoReunionMiembro, isVotoDoble(organoReunionMiembro));
        return votacionDAO.votarPunto(personaPuntoVoto);
    }

    private PuntoVotoPrivadoDTO votarPuntoPrivado(Persona persona, PuntoVotable puntoAVotar, OrganoReunionMiembro organoReunionMiembro)
            throws PuntoSecretoNoAdmiteCambioVotoException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        PuntoVotoPrivado puntoVotoPrivado = new PuntoVotoPrivado(puntoAVotar);
        VotantePrivado votantePrivado = new VotantePrivado(persona, puntoAVotar, organoReunionMiembro, isVotoDoble(organoReunionMiembro));

        return votacionDAO.votarPuntoPrivado(puntoVotoPrivado, votantePrivado);
    }

    private boolean isVotoDoble(OrganoReunionMiembro organoReunionMiembro) {
        OrganoParametro organoParametro = organoDAO.getOrganoParametro(organoReunionMiembro.getOrganoReunion().getOrganoId(), organoReunionMiembro.getOrganoReunion().getExterno());
        Boolean presidenteVotoDoble = organoParametro == null ? personalizationConfig.presidenteVotoDoble : organoParametro.getPresidenteVotoDoble();
        return organoReunionMiembro.getCargoCodigo().equals(CodigoCargoEnum.PRESIDENTE.codigo) && presidenteVotoDoble;
    }

    public void actualizarVotoTitular(Persona persona, PuntoVotableActualizar puntoAActualizar)
            throws NoSePuedeActualizarVotoInexistenteException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        OrganoReunionMiembro organoReunionMiembro =
                organoReunionMiembroDAO.getOrganoReunionMiembroByPersonaAndReunionId(persona, puntoAActualizar.getReunionAVotar().getReunionId());

        actualizarVoto(persona, puntoAActualizar, organoReunionMiembro);
    }

    public void actualizarVotoDelegado(Persona persona, Long representadoReunionMiembroId, PuntoVotableActualizar puntoAActualizar)
            throws NoSePuedeActualizarVotoInexistenteException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        OrganoReunionMiembro organoReunionMiembro =
                organoReunionMiembroDAO.getMiembroById(representadoReunionMiembroId);

        actualizarVoto(persona, puntoAActualizar, organoReunionMiembro);
    }

    public void actualizarVotoPresencial(Long miembroVotoPresencialId, PuntoVotableActualizar puntoAActualizar)
            throws NoSePuedeActualizarVotoInexistenteException, VotoDelegadoException, AsistenciaNoConfirmadaException,
            PersonasExternasException, NumberFormatException, VotoException {
        OrganoReunionMiembro organoReunionMiembro = organoReunionMiembroDAO.getMiembroById(miembroVotoPresencialId);
        Long personaId = organoReunionMiembro.getDelegadoVotoId() != null ? organoReunionMiembro.getDelegadoVotoId() : (organoReunionMiembro.getSuplenteId() != null ? organoReunionMiembro.getSuplenteId() : ParamUtils.parseLong(organoReunionMiembro.getMiembroId()));
        Persona persona = personaService.getPersonaFromDirectoryByPersonaId(personaId);

        actualizarVoto(persona, puntoAActualizar, organoReunionMiembro);
    }

    private void actualizarVoto(
            Persona persona,
            PuntoVotableActualizar puntoAActualizar,
            OrganoReunionMiembro organoReunionMiembro
    ) throws NoSePuedeActualizarVotoInexistenteException, VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        PersonaPuntoVotoDTO personaPuntoVotoDTO = votacionDAO
                .getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(organoReunionMiembro.getMiembroId(),
                        puntoAActualizar.getPuntoOrdenDia().getId());

        if (personaPuntoVotoDTO == null)
            throw new NoSePuedeActualizarVotoInexistenteException();

        PersonaPuntoVoto personaPuntoVoto =
                new PersonaPuntoVoto(personaPuntoVotoDTO.getId(), persona, puntoAActualizar, organoReunionMiembro, isVotoDoble(organoReunionMiembro));

        votacionDAO.actualizarVoto(personaPuntoVoto);
    }

    public List<PuntoOrdenDiaTemplate> getPuntosOrdenDiaVotosTemplate(
            List<PuntoOrdenDiaTemplate> puntoOrdenDiaTemplates, Long persaId, Long reunionId, Long organoReunionMiembroId) {
        return votacionDAO.getPuntosOrdenDiaVotosTemplate(puntoOrdenDiaTemplates, persaId, reunionId, organoReunionMiembroId);
    }

    public PersonaPuntoVotoDTO getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(String miembroId, Long puntoId) {
        return votacionDAO.getPersonaPuntoVotoAbiertoByMiembroAndPuntoId(miembroId, puntoId);
    }

    public List<OrganoReunionMiembro> getAsistentesByReunionId(Long reunionId) {
        return organoReunionMiembroDAO.getAsistentesByReunionId(reunionId);
    }

    public List<PuntoOrdenDiaRecuentoVotosDTO> getRecuentoVotosByReunion(Reunion reunion) throws TipoRecuentoVotoNoExisteException {
        List<PuntoOrdenDiaRecuentoVotosDTO> puntoOrdenDiaRecuentoVotosDTOS = new ArrayList<>();

        for (Long punto : puntoOrdenDiaDAO.getPuntosPadreIdsByReunionId(reunion.getId())) {
            puntoOrdenDiaRecuentoVotosDTOS.add(getRecuentoVotosPunto(reunion, punto));
        }
        return puntoOrdenDiaRecuentoVotosDTOS;
    }

    public List<PuntoOrdenDiaRecuentoVotosDTO> getRecuentoVotosByReunionOrderByTitulo(Reunion reunion, boolean isMainLanguage) throws TipoRecuentoVotoNoExisteException {
        return getRecuentoVotosByReunion(reunion).stream()
                .sorted(Comparator.comparing(punto -> isMainLanguage ? punto.getPuntoOrdenDia().getTitulo() : punto.getPuntoOrdenDia().getTituloAlternativo()))
                .collect(Collectors.toList());
    }

    public List<PuntoOrdenDiaRecuentoVotosDTO> getRecuentoVotosByReunionOrdered(Reunion reunion) throws TipoRecuentoVotoNoExisteException {
        return getRecuentoVotosByReunion(reunion).stream()
                .sorted(Comparator.comparing(punto -> punto.getPuntoOrdenDia().getOrden()))
                .collect(Collectors.toList());
    }

    public PuntoOrdenDiaRecuentoVotosDTO getRecuentoVotosPuntoSecreto(Reunion reunion, Long puntoId) throws TipoRecuentoVotoNoExisteException {
        return getRecuentoVotosPunto(reunion, puntoId);
    }

    public PuntoOrdenDiaRecuentoVotosDTO getRecuentoVotosPunto(Reunion reunion, Long puntoId) throws TipoRecuentoVotoNoExisteException {
        PuntoOrdenDiaRecuentoVotosDTO recuentoVotos = votacionDAO.getRecuentoVotosByPuntoId(puntoId);
        Set<PuntoOrdenDia> subpuntos = recuentoVotos.getPuntoOrdenDia().getPuntosInferiores();

        if (subpuntos == null) return recuentoVotos;

        List<PuntoOrdenDia> subpuntosOrdenados = subpuntos.stream().sorted(Comparator.comparing((s -> s.getOrden()))).collect(
                Collectors.toList());

        for (PuntoOrdenDia subpunto : subpuntosOrdenados)
            recuentoVotos.addSubpunto(getRecuentoVotosPunto(reunion, subpunto.getId()));

        return recuentoVotos;
    }

    private boolean noVotanteWithNombre(String nombre, PuntoOrdenDiaRecuentoVotosDTO recuentoVotos) {
        return recuentoVotos.getVotantesNombre()
                .stream()
                .noneMatch(votanteVoto -> votanteVoto.getNombreTitular().equals(nombre));
    }

    @Transactional
    public List<OrganoReunionMiembro> cierraVotacionPunto(Long puntoId, Persona persona)
            throws VotacionNoAbiertaException, ReunionYaCompletadaException,
            CargoNoSecretarioNoPuedeAbrirVotacionException,
            NoEsTelematicaExceptionConVotacion, PuntoProcedimientoNoVotable {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoId);

        this.checkIfPossibleSetVotacionAbierta(puntoOrdenDia, persona);

        if (puntoOrdenDia.getVotacionAbierta() != true)
            throw new VotacionNoAbiertaException();

        if (personalizationConfig.miembrosNatoVotoObligado) {
            List<OrganoReunionMiembro> miembrosNatoPorVotarPunto = votacionDAO.getMiembrosNatoPorVotarPunto(puntoOrdenDia.getId());

            if (!miembrosNatoPorVotarPunto.isEmpty())
                return miembrosNatoPorVotarPunto;
        }
        this.votacionDAO.actualizaVotacionAbiertaDelPunto(puntoId, false);
        return null;
    }

    @Transactional
    public void abreVotacionPunto(Long puntoId, Persona persona)
            throws CargoNoSecretarioNoPuedeAbrirVotacionException, ReunionYaCompletadaException,
            NoEsTelematicaExceptionConVotacion, PuntoProcedimientoNoVotable {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoId);

        this.checkIfPossibleSetVotacionAbierta(puntoOrdenDia, persona);
        if (puntoOrdenDia.getVotacionAbierta() == null || puntoOrdenDia.getVotacionAbierta() == false) {
            this.votacionDAO.actualizaVotacionAbiertaDelPunto(puntoId, true);
        }
    }

    @Transactional
    public void checkIfPossibleSetVotacionAbierta(PuntoOrdenDia puntoOrdenDia, Persona persona)
            throws NoEsTelematicaExceptionConVotacion, ReunionYaCompletadaException,
            CargoNoSecretarioNoPuedeAbrirVotacionException, PuntoProcedimientoNoVotable {


        if (!puntoOrdenDia.getReunion().isVotacionTelematica())
            throw new NoEsTelematicaExceptionConVotacion();

        if (puntoOrdenDia.getTipoProcedimientoVotacion().equals(TipoProcedimientoVotacionEnum.NO_VOTABLE.toString()))
            throw new PuntoProcedimientoNoVotable();

        if (puntoOrdenDia.getReunion().getCompletada() == true)
            throw new ReunionYaCompletadaException();

        if (!isRolGestorAperturaVotacion(puntoOrdenDia.getReunion().getId(), persona)) {
            throw new CargoNoSecretarioNoPuedeAbrirVotacionException();
        }
    }

    public boolean isRolGestorAperturaVotacion(Long reunionId, Persona persona) {
        OrganoReunionMiembro miembro = organoReunionMiembroDAO.getOrganoReunionMiembroByPersonaAndReunionId(
                persona, reunionId
        );

        Reunion reunion = reunionService.getReunionById(reunionId);

        if ((reunion.getResponsableVotoId() != null && reunion.getResponsableVotoId().longValue() != 0L) && (reunion.getResponsableVotoId().longValue() == persona.getId().longValue())) {
            return true;

        } else if (miembro != null) {
            String cargoCodigo = miembro.getCargoCodigo();
            return !Strings.isNullOrEmpty(cargoCodigo) && CodigoCargoEnum.SECRETARIO.codigo.equals(cargoCodigo);
        } else {
            return false;
        }
    }

    public boolean isAprobadoPunto(PuntoOrdenDia puntoOrdenDia) throws TipoRecuentoVotoNoExisteException {
        return votacionDAO.isAprobadoPunto(puntoOrdenDia);
    }

    @Transactional
    public void actualizarListaVotosReunionTemplate(ReunionTemplate reunionTemplate, List<PuntoOrdenDiaTemplate> puntosOrdenDia, Long personaId,
                                                    Long reunionId, Long miembroId) {
        reunionTemplate.setPuntosOrdenDia(getPuntosOrdenDiaVotosTemplate(
                puntosOrdenDia,
                personaId,
                reunionId,
                miembroId
        ));
    }

    @Transactional
    public PuntoOrdenDiaRecuentoVotosDTO mapperPuntoSecretoVotosDTOToPuntoOrdenDiaRecuentoVotosDTO(PuntoSecretoVotosDTO puntoSecretoVotosDTO) throws TipoRecuentoVotoNoExisteException {
        PuntoOrdenDia puntoOrdenDia = puntoSecretoVotosDTO.getPuntoOrdenDia();
        Long votosFavor = puntoSecretoVotosDTO.getVotosFavor();
        Long votosContra = puntoSecretoVotosDTO.getVotosContra();
        Long votosBlanco = puntoSecretoVotosDTO.getVotosBlanco();
        Long recuentoVotos = votosFavor + votosContra + votosBlanco;
        boolean isAprobado = isAprobadoPunto(puntoSecretoVotosDTO.getPuntoOrdenDia());

        PuntoOrdenDiaRecuentoVotosDTO puntoRecuentoVotosDTO =
                new PuntoOrdenDiaRecuentoVotosDTO(
                        puntoOrdenDia, votosFavor, votosContra, votosBlanco, recuentoVotos, isAprobado, new ArrayList<VotanteVoto>()
                );

        return puntoRecuentoVotosDTO;
    }

    @Transactional
    public void insertarVotosPresencialesPorDefectoFavorTemplate(ReunionTemplate reunionTemplate) throws NoVotacionTelematicaException,
            ReunionNoAbiertaException, VotacionNoAbiertaException, TipoVotoNoExisteException, PuntoNoVotableException, PuntoNoVotableEnContraException,
            UsuarioNoTienePermisoParaVotarException, PersonasExternasException, NumberFormatException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        List<PuntoOrdenDiaTemplate> puntos = reunionTemplate.getPuntosOrdenDia();
        for (PuntoOrdenDiaTemplate punto : puntos) {
            insertaVotosPresencialesPorPunto(reunionTemplate, punto);

            if (punto.getSubpuntos() != null) {
                for (PuntoOrdenDiaTemplate subpunto : punto.getSubpuntos()) {
                    insertaVotosPresencialesPorPunto(reunionTemplate, subpunto);
                }
            }
        }
    }

    private void insertaVotosPresencialesPorPunto(ReunionTemplate reunionTemplate, PuntoOrdenDiaTemplate subpunto) throws NoVotacionTelematicaException, ReunionNoAbiertaException, VotacionNoAbiertaException, TipoVotoNoExisteException, PuntoNoVotableException, PuntoNoVotableEnContraException, UsuarioYaHaVotadoSoloSePermiteActualizacionException, VotoDelegadoException, AsistenciaNoConfirmadaException, UsuarioNoTienePermisoParaVotarException, PersonasExternasException, VotoException {
        if (subpunto.getTipoVoto() != null && subpunto.getTipoVoto()) {
            if (subpunto.getTipoVotacion() != null && subpunto.getTipoVotacion().equals(TipoVotacionEnum.PRESENCIAL.toString()) && subpunto.isVotacionAbierta()) {
                List<OrganoTemplate> organos = reunionTemplate.getOrganos();
                for (OrganoTemplate organo : organos) {
                    List<MiembroTemplate> asistentes = organo.getAsistentes();
                    for (MiembroTemplate asistente : asistentes) {
                        if ((asistente.getAsistencia() || asistente.getSuplenteId() != null || asistente.getDelegadoVotoId() != null) && (!votacionDAO.hasMiembroVotosEmitidosByPuntoId(ParamUtils.parseLong(asistente.getId()), subpunto.getId()))) {
                            PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.getPuntoById(subpunto.getId());
                            ReunionAVotar reunionAVotar = new ReunionAVotar(puntoOrdenDia.getReunion());
                            PuntoVotable puntoAVotar = new PuntoVotable(puntoOrdenDia, reunionAVotar, TipoVotoEnum.FAVOR.toString());
                            votarPuntoPresencial(Long.parseLong(asistente.getId()), puntoAVotar);
                        }
                    }
                }
            }
        }
    }
}
