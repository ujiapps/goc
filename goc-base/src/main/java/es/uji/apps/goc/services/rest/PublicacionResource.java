package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.DateUtils;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.OrganoAutorizadoDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.*;
import es.uji.apps.goc.services.*;
import es.uji.apps.goc.templates.HTMLTemplate;
import es.uji.apps.goc.templates.PDFTemplate;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Path("publicacion")
public class PublicacionResource extends CoreBaseService {
    public static final int RESULTADOS_POR_PAGINA = 10;

    @InjectParam
    private ReunionDAO reunionDAO;

    @InjectParam
    private OrganoAutorizadoDAO organoAutorizadoDAO;

    @InjectParam
    private LanguageConfig languageConfig;

    @InjectParam
    private ReunionService reunionService;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @InjectParam
    private ReunionDocumentoService reunionDocumentoService;

    @InjectParam
    private VotacionService votacionService;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private OrganoReunionMiembroService organoReunionMiembroService;

    @InjectParam
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @Value("${goc.templates.path:classpath:templates/}")
    private String templatesPath;

    @GET
    @Path("reuniones")
    @Produces(MediaType.TEXT_HTML)
    public Template reuniones(@QueryParam("lang") String lang, @QueryParam("pagina") @DefaultValue("0") Integer pagina) throws RolesPersonaExternaException {
        return reunionesAbiertas(lang, pagina);
    }

    @GET
    @Path("reuniones-abiertas")
    @Produces(MediaType.TEXT_HTML)
    public Template reunionesAbiertas(@QueryParam("lang") String lang, @QueryParam("pagina") @DefaultValue("0") Integer pagina) throws RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ReunionesPermisosWrapper wrapper = reunionService.getReunionesAbiertasAccesiblesByPersonaId(connectedUserId, pagina * RESULTADOS_POR_PAGINA, RESULTADOS_POR_PAGINA);
        List<ReunionPermiso> reuniones = wrapper.getReuniones();
        Long numReuniones = wrapper.getNumeroReuniones();

        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("reuniones-abiertas-" + applang, templatesPath);
        template.put("logo", personalizationConfig.logo);
        template.put("logoPublic", (personalizationConfig.logoPublic != null) ? personalizationConfig.logoPublic : personalizationConfig.logo);
        template.put("reuniones", reuniones);
        template.put("applang", applang);
        template.put("lang", lang);
        template.put("enlaceManualMiembros", personalizationConfig.enlaceManualMiembros);
        template.put("charset", personalizationConfig.charset);
        template.put("mainLanguage", languageConfig.mainLanguage);
        template.put("alternativeLanguage", languageConfig.alternativeLanguage);
        template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
        template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
        template.put("customCSS", (personalizationConfig.customCSS != null) ? personalizationConfig.customCSS : "");
        template.put("connectedUserId", connectedUserId);

        if (pagina > 0) {
            template.put("hasPrevPage", true);
        }

        if (numReuniones > ((pagina * RESULTADOS_POR_PAGINA) + RESULTADOS_POR_PAGINA)) {
            template.put("hasNextPage", true);
        }

        template.put("pagina", pagina);

        return template;
    }

    @GET
    @Path("reuniones-cerradas")
    @Produces(MediaType.TEXT_HTML)
    public Template reunionesCerradas(@QueryParam("lang") String lang, @QueryParam("pagina") @DefaultValue("0") Integer pagina, @QueryParam("organoId") Long organoId, @QueryParam("organoExterno") Boolean organoExterno) throws RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ReunionesPermisosWrapper wrapper = reunionService.getReunionesCerradasAccesiblesByPersonaIdAndOrgano(connectedUserId, organoId, organoExterno, pagina * RESULTADOS_POR_PAGINA, RESULTADOS_POR_PAGINA);

        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("tpl-reuniones-historico-" + applang, templatesPath);
        template.put("applang", applang);
        template.put("lang", lang);
        template.put("organoId", organoId);
        template.put("organoExterno", organoExterno);
        template.put("pagina", pagina);
        template.put("reuniones", wrapper.getReuniones());

        if (pagina > 0) {
            template.put("hasPrevPage", true);
        }

        if (wrapper.getNumeroReuniones() > ((pagina * RESULTADOS_POR_PAGINA) + RESULTADOS_POR_PAGINA)) {
            template.put("hasNextPage", true);
        }

        template.put("pagina", pagina);

        return template;
    }

    @GET
    @Path("reuniones-historico")
    @Produces(MediaType.TEXT_HTML)
    public Template reunionesHistorico(@QueryParam("lang") String lang) throws RolesPersonaExternaException, OrganosExternosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<OrganosPermisosWrapper> organos = reunionService.getOrganosReunionesCerradasAccesiblesByPersonaId(connectedUserId);

        organos = languageConfig.isMainLangauge(lang)
                ? organos.stream().sorted(Comparator.comparing(OrganosPermisosWrapper::getNombre, Comparator.nullsLast(String.CASE_INSENSITIVE_ORDER))).collect(Collectors.toList()) :
                organos.stream().sorted(Comparator.comparing(OrganosPermisosWrapper::getNombreAlt, Comparator.nullsLast(String.CASE_INSENSITIVE_ORDER))).collect(Collectors.toList());

        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("reuniones-historico-" + applang, templatesPath);
        template.put("logo", personalizationConfig.logo);
        template.put("logoPublic", (personalizationConfig.logoPublic != null) ? personalizationConfig.logoPublic : personalizationConfig.logo);
        template.put("organos", organos);
        template.put("applang", applang);
        template.put("lang", lang);
        template.put("enlaceManualMiembros", personalizationConfig.enlaceManualMiembros);
        template.put("charset", personalizationConfig.charset);
        template.put("mainLanguage", languageConfig.mainLanguage);
        template.put("alternativeLanguage", languageConfig.alternativeLanguage);
        template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
        template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
        template.put("customCSS", (personalizationConfig.customCSS != null) ? personalizationConfig.customCSS : "");
        template.put("connectedUserId", connectedUserId);

        return template;
    }

    @GET
    @Path("acuerdos")
    @Produces(MediaType.TEXT_HTML)
    public Template acuerdos(@QueryParam("lang") String lang, @QueryParam("tipoOrganoId") Long tipoOrganoId,
                             @QueryParam("organoId") Long organoId, @QueryParam("descriptorId") Long descriptorId,
                             @QueryParam("claveId") Long claveId, @QueryParam("anyo") Integer anyo,
                             @QueryParam("fInicio") String fInicio, @QueryParam("fFin") String fFin, @QueryParam("texto") String texto,
                             @QueryParam("pagina") @DefaultValue("0") Integer pagina) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String applang = languageConfig.getLangCode(lang);

        List<Integer> anyos = reunionService.getAnyosConReunionesPublicas();
        List<TipoOrganoLocal> tiposOrganos = reunionService.getTiposOrganosConReunionesPublicas();
        List<Organo> organos = new ArrayList<>();
        List<Descriptor> descriptoresConReunionesPublicas = null;
        List<Clave> claves = null;
        List<ReunionTemplate> reunionesTemplate = null;

        descriptoresConReunionesPublicas = reunionService.getDescriptoresConReunionesPublicas(anyo);

        if (tipoOrganoId != null) {
            organos = reunionService.getOrganosConReunionesPublicas(tipoOrganoId, anyo);
        }

        if (!descriptoresConReunionesPublicas.isEmpty() && descriptorId != null) {
            claves = reunionService.getClavesConReunionesPublicas(descriptorId, anyo);
        }

        AcuerdosSearch acuerdosSearch = new AcuerdosSearch(anyo, pagina * RESULTADOS_POR_PAGINA, RESULTADOS_POR_PAGINA);

        if (!descriptoresConReunionesPublicas.isEmpty()) {
            acuerdosSearch.setClaveId(claveId);
            acuerdosSearch.setDescriptorId(descriptorId);
        }

        acuerdosSearch.setfInicio(getDate(fInicio));
        acuerdosSearch.setfFin(getDate(fFin));
        acuerdosSearch.setTexto(texto);
        acuerdosSearch.setTipoOrganoId(tipoOrganoId);
        acuerdosSearch.setOrganoId(organoId);

        acuerdosSearch.setIdiomaAlternativo(!languageConfig.isMainLangauge(lang));

        BuscadorReunionesWrapper reunionesPublicasPaginadasWrapper = reunionService.getReunionesPublicas(acuerdosSearch);
        Long numReuniones = reunionesPublicasPaginadasWrapper.getNumeroReuniones();

        reunionesTemplate = buildReunionTemplateBuscador(connectedUserId, reunionesPublicasPaginadasWrapper.getReuniones(), lang, true);

        Template template = new HTMLTemplate("acuerdos-" + applang, templatesPath);
        template.put("logo", personalizationConfig.logo);
        template.put("logoPublic", (personalizationConfig.logoPublic != null) ? personalizationConfig.logoPublic : personalizationConfig.logo);
        template.put("applang", applang);
        template.put("charset", personalizationConfig.charset);
        template.put("lang", lang);
        template.put("mainLanguage", languageConfig.mainLanguage);
        template.put("alternativeLanguage", languageConfig.alternativeLanguage);
        template.put("enlaceManualMiembros", personalizationConfig.enlaceManualMiembros);
        template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
        template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
        template.put("customCSS", (personalizationConfig.customCSS != null) ? personalizationConfig.customCSS : "");
        template.put("connectedUserId", connectedUserId);
        template.put("tiposOrganos", tiposOrganos);
        template.put("tipoOrganoId", tipoOrganoId);
        template.put("organoId", organoId);
        template.put("organos", organos);
        template.put("reuniones", reunionesTemplate);
        template.put("descriptores", descriptoresConReunionesPublicas);
        template.put("descriptorId", descriptorId);
        template.put("claves", claves);
        template.put("claveId", claveId);
        template.put("anyos", anyos);
        template.put("anyo", anyo);
        template.put("fInicio", fInicio);
        template.put("fFin", fFin);
        template.put("texto", texto);

        if (pagina > 0) {
            template.put("hasPrevPage", true);
        }

        if (numReuniones > ((pagina * RESULTADOS_POR_PAGINA) + RESULTADOS_POR_PAGINA)) {
            template.put("hasNextPage", true);
        }

        template.put("pagina", pagina);

        return template;
    }

    public Date getDate(String value) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            return formatter.parse(value);
        } catch (Exception e) {
            return null;
        }
    }

    private List<ReunionTemplate> buildReunionTemplateBuscador(Long connectedUserId, List<Reunion> reuniones, String lang, boolean isForBuscadorAcuerdos) {
        boolean isMainLangauge = languageConfig.isMainLangauge(lang);
        List<ReunionTemplate> reunionesTemplate;
        if (isForBuscadorAcuerdos) {
            reunionesTemplate = reuniones.stream()
                    .map(r -> reunionService.getReunionTemplateDesdeReunionForBuscador(r,
                            isMainLangauge, connectedUserId))
                    .collect(Collectors.toList());

        } else {
            reunionesTemplate = reuniones.stream()
                    .map(r -> reunionService.getReunionTemplateDesdeReunion(r, connectedUserId, false,
                            isMainLangauge))
                    .collect(Collectors.toList());
        }
        return reunionesTemplate;
    }

    @GET
    @Path("reuniones/{reunionId}")
    @Produces(MediaType.TEXT_HTML)
    public Template reunion(@PathParam("reunionId") Long reunionId, @QueryParam("lang") String lang)
            throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
            PersonasExternasException, InvalidAccessException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        if (!reunionDAO.tieneAcceso(reunionId, connectedUserId)) {
            throw new InvalidAccessException("No se tiene acceso a esta reunión");
        }

        boolean permitirComentarios = reunion.isPermitirComentarios(connectedUserId, organoAutorizadoDAO.getAutorizadosByReunionId(reunionId));
        boolean permitirSubirDocumentos = reunion.isPermitirSubirDocumentos(connectedUserId, organoAutorizadoDAO.getAutorizadosByReunionId(reunionId));
        boolean isExcusable = organoReunionMiembroService.isExcusableByMiembroReunion(reunion, connectedUserId);

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, true,
                languageConfig.isMainLangauge(lang));
        LocalDate fecha = reunion.getFecha().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Date fechaActual = new Date();
        boolean iniciadaReunion = fechaActual.after(reunion.getFecha()) || fechaActual.equals(reunion.getFecha());
        String applang = languageConfig.getLangCode(lang);
        String diaSemanaTexto = fecha.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("ca"));

        Template template = new HTMLTemplate("reunion-" + applang, templatesPath);

        PuntoOrdenDiaTemplate puntoUnico = new PuntoOrdenDiaTemplate();

        if (reunionTemplate.getPuntosOrdenDia().size() == 1) {
            puntoUnico = reunionTemplate.getPuntosOrdenDia().get(0);
            template.put("puntoUnico", puntoUnico);
            template.put("isPuntoUnico", true);
        }

        template.put("logo", personalizationConfig.logo);
        template.put("logoPublic", (personalizationConfig.logoPublic != null) ? personalizationConfig.logoPublic : personalizationConfig.logo);
        template.put("charset", personalizationConfig.charset);
        template.put("enlaceManualMiembros", personalizationConfig.enlaceManualMiembros);
        template.put("reunion", reunionTemplate);
        template.put("applang", applang);
        template.put("mainLanguage", languageConfig.mainLanguage);
        template.put("alternativeLanguage", languageConfig.alternativeLanguage);
        template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
        template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
        template.put("customCSS", (personalizationConfig.customCSS != null) ? personalizationConfig.customCSS : "");
        template.put("connectedUserId", connectedUserId);
        template.put("permitirComentarios", permitirComentarios);
        template.put("permitirSubirDocumentos", permitirSubirDocumentos);
        template.put("isExcusable", isExcusable);
        template.put("hayAlgunDocumento", reunionDocumentoService.hayAlgunaDocumentacion(reunionId));
        template.put("diaSemanaTexto", diaSemanaTexto);
        template.put("mostrarConvocante", this.personalizationConfig.mostrarConvocante);
        template.put("iniciadaReunion", iniciadaReunion);
        template.put("votacionTelematica", reunionTemplate.getVotacionTelematica());

        boolean foundOrganoWithActaProvisionalDesactivada = false;
        for (OrganoTemplate organo : reunionTemplate.getOrganos()) {
            if (!organo.getActaProvisionalActiva()) {
                template.put("actaProvisionalActiva", false);
                foundOrganoWithActaProvisionalDesactivada = true;
            }
        }
        if (!foundOrganoWithActaProvisionalDesactivada)
            template.put("actaProvisionalActiva", true);

        return template;
    }

    @GET
    @Path("reuniones/{reunionId}/acuerdos")
    @Produces(MediaType.TEXT_HTML)
    public Template reunionAcuerdos(@PathParam("reunionId") Long reunionId, @QueryParam("lang") String lang)
            throws InvalidAccessException, ReunionNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }


        if (!reunionDAO.tieneAcceso(reunionId, connectedUserId)) {
            throw new InvalidAccessException("No se tiene acceso a esta reunión");
        }

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, true,
                languageConfig.isMainLangauge(lang));

        String applang = languageConfig.getLangCode(lang);

        Template template = new HTMLTemplate("reunion-acuerdos-" + applang, templatesPath);

        template.put("logo", personalizationConfig.logo);
        template.put("logoPublic", (personalizationConfig.logoPublic != null) ? personalizationConfig.logoPublic : personalizationConfig.logo);
        template.put("charset", personalizationConfig.charset);
        template.put("reunion", reunionTemplate);
        template.put("applang", applang);
        template.put("mainLanguage", languageConfig.mainLanguage);
        template.put("alternativeLanguage", languageConfig.alternativeLanguage);
        template.put("enlaceManualMiembros", personalizationConfig.enlaceManualMiembros);
        template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
        template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
        template.put("customCSS", (personalizationConfig.customCSS != null) ? personalizationConfig.customCSS : "");
        template.put("connectedUserId", connectedUserId);

        return template;
    }

    @GET
    @Path("reuniones/{reunionId}/acuerdos/{puntoOrdenDiaId}")
    @Produces("application/pdf")
    public Template reunionAcuerdoCertificado(@PathParam("reunionId") Long reunionId,
                                              @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId, @QueryParam("lang") String lang)
            throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
            PersonasExternasException, InvalidAccessException, ReunionNoCompletadaException,
            PuntoDelDiaNoDisponibleException, PuntoDelDiaNoTieneAcuerdosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        if (!reunionDAO.tieneAcceso(reunionId, connectedUserId)) {
            throw new InvalidAccessException("No se tiene acceso a esta reunión");
        }

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, true,
                languageConfig.isMainLangauge(lang));

        PuntoOrdenDiaTemplate puntoOrdenDiaTemplate =
                getPuntoOrdenDiaTemplate(reunionTemplate.getPuntosOrdenDia(), puntoOrdenDiaId);

        if (puntoOrdenDiaTemplate == null) {
            throw new PuntoDelDiaNoDisponibleException();
        }

        if (puntoOrdenDiaTemplate.getAcuerdos() == null || puntoOrdenDiaTemplate.getAcuerdos().isEmpty()) {
            throw new PuntoDelDiaNoTieneAcuerdosException();
        }

        String applang = languageConfig.getLangCode(lang);

        Template template = new PDFTemplate("reunion-acuerdo-" + applang, templatesPath);
        template.put("logo", personalizationConfig.logoDocumentos);
        template.put("puntoOrdenDia", puntoOrdenDiaTemplate);
        template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
        template.put("fechaReunion", getFechaReunion(reunionTemplate.getFecha()));
        template.put("fechaReunionFormateada", getFechaReunionFormateada(reunionTemplate.getFecha(), lang));
        template.put("numeroSesion", reunionTemplate.getNumeroSesion());
        template.put("tituloReunion", reunionTemplate.getAsunto());
        template.put("organos", getNombreOrganos(reunionTemplate));
        template.put("numeroOrganos", reunionTemplate.getOrganos().size());
        template.put("firmante", reunionTemplate.getFirmante());
        template.put("ciudadInstitucion", personalizationConfig.ciudadInstitucion);

        return template;
    }

    @GET
    @Path("reuniones/puntos/{reunionId}")
    @Produces(MediaType.TEXT_HTML)
    public Template reunionPuntosOrdenDia(
            @PathParam("reunionId") Long reunionId,
            @QueryParam("lang") String lang,
            @QueryParam("organoReunionMiembroId") Long organoReunionMiembroId
    ) throws ReunionNoDisponibleException, InvalidAccessException, PuntoDelDiaNoDisponibleException,
            PersonasExternasException, PersonaNoAutorizadaException, UsuarioNoTienePermisoParaVotarException, NumberFormatException,
            NoVotacionTelematicaException, ReunionNoAbiertaException, VotacionNoAbiertaException, TipoVotoNoExisteException,
            PuntoNoVotableException, PuntoNoVotableEnContraException, UsuarioYaHaVotadoSoloSePermiteActualizacionException,
            VotoDelegadoException, AsistenciaNoConfirmadaException, VotoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Persona persona = personaService.getPersonaFromDirectoryByPersonaId(connectedUserId);
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null)
            throw new ReunionNoDisponibleException();

        if (!reunionDAO.tieneAcceso(reunionId, connectedUserId))
            throw new InvalidAccessException("No se tiene acceso a esta reunión");

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, true,
                languageConfig.isMainLangauge(lang));

        List<PuntoOrdenDiaTemplate> puntosOrdenDia = reunionTemplate.getPuntosOrdenDia();
        if (puntosOrdenDia == null)
            throw new PuntoDelDiaNoDisponibleException();

        Long organoReunionMiembroIdByPersonaAndReunionId =
                organoReunionMiembroService.getOrganoReunionMiembroIdByPersonaAndReunionId(persona, reunionId);

        if (organoReunionMiembroId != null) {
            List<VotanteRepresentado> votantesRepresentados = organoReunionMiembroService
                    .getVotantesRepresentadosByReunionIdAndPersonaId(reunionId, persona.getId());
            if (!votantesRepresentados.stream().filter(v -> v.getMiembroId().equals(organoReunionMiembroId)).findAny().isPresent()) {
                throw new PersonaNoAutorizadaException();
            }
        }

        Long miembroId = organoReunionMiembroId != null ? organoReunionMiembroId : organoReunionMiembroIdByPersonaAndReunionId;
        votacionService.actualizarListaVotosReunionTemplate(reunionTemplate, puntosOrdenDia, persona.getId(), reunionId, miembroId);

        // Inicializar votos a favor por defecto para puntos presenciales publicos, si no hay votos en el punto
        votacionService.insertarVotosPresencialesPorDefectoFavorTemplate(reunionTemplate);

        votacionService.actualizarListaVotosReunionTemplate(reunionTemplate, puntosOrdenDia, persona.getId(), reunionId, miembroId);

        String applang = languageConfig.getLangCode(lang);

        boolean isRolGestorAperturaVotacion = votacionService.isRolGestorAperturaVotacion(reunionId, persona);
        List<VotanteRepresentado> votantesRepresentados =
                organoReunionMiembroService.getVotantesRepresentadosByReunionIdAndPersonaId(reunionId, connectedUserId);

        Template template = new HTMLTemplate("votacion-" + applang, templatesPath);
        template.put("logo", personalizationConfig.logo);
        template.put("logoPublic", (personalizationConfig.logoPublic != null) ? personalizationConfig.logoPublic : personalizationConfig.logo);
        template.put("charset", personalizationConfig.charset);
        template.put("reunion", reunionTemplate);
        template.put("applang", applang);
        template.put("mainLanguage", languageConfig.mainLanguage);
        template.put("alternativeLanguage", languageConfig.alternativeLanguage);
        template.put("enlaceManualMiembros", personalizationConfig.enlaceManualMiembros);
        template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
        template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
        template.put("customCSS", (personalizationConfig.customCSS != null) ? personalizationConfig.customCSS : "");
        template.put("connectedUserId", connectedUserId);
        template.put("isRolGestorAperturaVotacion", isRolGestorAperturaVotacion);
        template.put("votantesRepresentados", votantesRepresentados);
        template.put("organoReunionMiembroId", organoReunionMiembroId);
        template.put("tieneDelegado", organoReunionMiembroService.tieneDelegado(organoReunionMiembroIdByPersonaAndReunionId));
        template.put("tieneSuplente", organoReunionMiembroService.tieneSuplente(organoReunionMiembroIdByPersonaAndReunionId));
        template.put("esElSuplente", organoReunionMiembroService.esElSuplente(organoReunionMiembroIdByPersonaAndReunionId, persona.getId()));
        template.put("asiste", organoReunionMiembroService.asiste(organoReunionMiembroIdByPersonaAndReunionId));

        template.put("permiteAbstencionVoto", true);
        for (OrganoTemplate organo : reunionTemplate.getOrganos()) {
            if (!organo.getPermiteAbstencionVoto()) {
                template.put("permiteAbstencionVoto", false);
            }
        }

        /*template.put("tieneSuplente", organoReunionMiembroService.tieneSuplente(organoReunionMiembroIdByPersonaAndReunionId, persona.getId()));*/
        return template;
    }

    private PuntoOrdenDiaTemplate getPuntoOrdenDiaTemplate(List<PuntoOrdenDiaTemplate> puntosOrdenDiaTemplate,
                                                           Long puntoOrdenDiaIdBuscado) {
        for (PuntoOrdenDiaTemplate puntoOrdenDiaTemplate : puntosOrdenDiaTemplate) {
            if (puntoOrdenDiaTemplate.getId().equals(puntoOrdenDiaIdBuscado)) {
                return puntoOrdenDiaTemplate;
            } else {
                if (puntoOrdenDiaTemplate.getSubpuntos() != null) {
                    PuntoOrdenDiaTemplate puntoOrdenDiaEnSubpuntos =
                            getPuntoOrdenDiaTemplate(puntoOrdenDiaTemplate.getSubpuntos(), puntoOrdenDiaIdBuscado);
                    if (puntoOrdenDiaEnSubpuntos != null) {
                        return puntoOrdenDiaEnSubpuntos;
                    }
                }
            }
        }

        return null;
    }

    private String getFechaReunion(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        return sdf.format(fecha);
    }

    private String getFechaReunionFormateada(Date fecha, String lang) {
        if ("ca".equalsIgnoreCase(lang)) return DateUtils.formattedDateInCatalan(fecha);
        if ("es".equalsIgnoreCase(lang)) return DateUtils.formattedDateInSpanish(fecha);

        return getFechaReunion(fecha);
    }

    private String getNombreOrganos(ReunionTemplate reunionTemplate) {
        List<String> nombreOrganos = new ArrayList<>();

        for (OrganoTemplate organo : reunionTemplate.getOrganos()) {
            nombreOrganos.add(organo.getNombre());
        }

        return StringUtils.join(nombreOrganos, ", ");
    }
}
