package es.uji.apps.goc.services.rest;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.exceptions.DocumentoNoEncontradoException;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.model.DocumentoUI;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.MiembroCSV;
import es.uji.apps.goc.services.MiembroService;

@Service
public class CSVExporter
{
    private static Logger log = LoggerFactory.getLogger(CSVExporter.class);

    @Autowired
    MiembroService miembroService;

    public DocumentoUI exportaMiembros(String organoId, Long connectedUserId)
        throws MiembrosExternosException, DocumentoNoEncontradoException
    {
        List<Miembro> miembros = miembroService.getMiembrosExternos(organoId.toString(), connectedUserId);
        List<MiembroCSV> miembrosCSV = getMiembrosCSVbyMiembros(miembros);
        if (miembrosCSV == null || miembrosCSV.isEmpty())
        {
            Long organoIdLocal = null;
            try
            {
                organoIdLocal = Long.valueOf(organoId);
            } catch (Exception e)
            {
                log.error("No se ha podido transformar el id del órgano");
            }
            miembros = miembroService.getMiembrosLocales(organoIdLocal, connectedUserId);
            miembrosCSV = getMiembrosCSVbyMiembros(miembros);
        }

        if (miembrosCSV == null)
        {
            throw new DocumentoNoEncontradoException();
        }

        return getDocumentoUIFromCsvOutputStream(miembrosCSV);
    }

    private DocumentoUI getDocumentoUIFromCsvOutputStream(List<MiembroCSV> miembrosCSV)
    {
        ByteArrayOutputStream outputStream = null;
        try
        {
            outputStream = getCsv(miembrosCSV);
        } catch (IOException e)
        {
            log.error("No se ha podido generar el archivo. " + e);
        }
        DocumentoUI documentoUI = new DocumentoUI();
        documentoUI.setMimeType("text/csv");
        documentoUI.setData(outputStream.toByteArray());
        return documentoUI;
    }

    private List<MiembroCSV> getMiembrosCSVbyMiembros(List<Miembro> miembros)
    {
        return miembros.stream().map(
            m -> new MiembroCSV(m.getId(), m.getNombre(), m.getEmail(), m.getCargo()))
            .collect(Collectors.toList());
    }

    private ByteArrayOutputStream getCsv(List<MiembroCSV> miembros) throws IOException
    {
        CsvMapper mapper = new CsvMapper();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Object baseObject = miembros.get(0);
        CsvSchema schema = mapper.schemaFor(baseObject.getClass()).withHeader();
        mapper.writer(schema).writeValue(outputStream,miembros);
        return outputStream;
    }
}
