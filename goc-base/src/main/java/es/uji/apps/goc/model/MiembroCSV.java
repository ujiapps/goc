package es.uji.apps.goc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MiembroCSV extends Miembro {

    public MiembroCSV(){
        super();
    }

    public MiembroCSV(Long id, String nombre, String email, Cargo cargo)
    {
        super.setId(id);
        super.setNombre(nombre);
        super.setEmail(email);
        super.setCargo(cargo);
    }

    @JsonProperty("Identificador")
    public Long getId()
    {
        return super.getId();
    }

    @JsonProperty("Nombre")
    public String getNombre()
    {
        return super.getNombre();
    }

    @JsonProperty("Email")
    public String getEmail()
    {
        return super.getEmail();
    }

    @JsonProperty("Cargo del miembro")
    public String getCargoNombre()
    {
        return super.getCargo().getNombre();
    }

    @JsonProperty("Cargo del miembro alternativo")
    public String getCargoNombreAlternativo()
    {
        return super.getCargo().getNombreAlternativo();
    }

    @JsonIgnore
    public Cargo getCargo(){
        return super.getCargo();
    }

    @JsonIgnore
    public Organo getOrgano(){
        return super.getOrgano();
    }

    @JsonIgnore
    public Long getPersonaId(){
        return  super.getPersonaId();
    }

    @JsonIgnore
    public boolean getNato(){
        return super.getNato();
    }

    @JsonIgnore
    public String getCondicion()
    {
        return super.getCondicion();
    }

    @JsonIgnore
    public String getCondicionAlternativa()
    {
        return super.getCondicionAlternativa();
    }

}
