package es.uji.apps.goc.auth;

import com.google.common.base.Strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.services.PersonaService;
import es.uji.commons.sso.User;

public class DefaulSessionFromPropertiesAuth implements Filter
{
    Logger logger = LoggerFactory.getLogger(DefaulSessionFromPropertiesAuth.class);
    private static final String SESSION_ROLES = "roles";
    private static final String DEFAULT_USER_ID_HEADER = "defaultUserId";
    private static final String DEFAULT_USER_NAME_HEADER = "defaultUserName";

    private FilterConfig filterConfig = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException
    {

        HttpServletRequest clientRequest = (HttpServletRequest) request;

        String url = clientRequest.getRequestURI();
        String headerAuthToken = clientRequest.getHeader("X-UJI-AuthToken");

        if (isExcludedUrl(url) ||
            isCorrectExternalAPICall(url, headerAuthToken) ||
            sessionAlreadyRegistered(clientRequest) ||
            isForbidenPage(url))
        {
            filterChain.doFilter(request, response);
            return;
        }

        String userName;
        String userId;
        if (requestHasAuthHeaders(clientRequest)) {
            DefaultUser defaultUser = getUserFromHeaders(clientRequest);
            userName = defaultUser.defaultUsername;
            userId = defaultUser.defaultUserId;
        }
        else {
            DefaultUser defaultUser = getDefaultUser();
            userName = defaultUser.defaultUsername;
            userId = defaultUser.defaultUserId;
        }

        User user = createUserFromDefaulLocalValues(userName, userId);
        registerUserInHttpSession(clientRequest, user);

        filterChain.doFilter(request, response);
    }

    private boolean requestHasAuthHeaders(HttpServletRequest clientRequest) {
        return !Strings.isNullOrEmpty(clientRequest.getHeader(DEFAULT_USER_ID_HEADER));
    }

    private boolean isForbidenPage(String url)
    {
        return (url != null && url.startsWith("/goc/forbidden.jsp"));
    }

    private boolean sessionAlreadyRegistered(HttpServletRequest clientRequest)
    {
        return clientRequest.getSession().getAttribute(User.SESSION_USER) != null;
    }

    private User createUserFromDefaulLocalValues(String userName, String userId)
            throws IOException
    {
        try
        {
            User user = new User();
            user.setId(Long.parseLong(userId));
            user.setName(userName);
            user.setActiveSession(UUID.randomUUID().toString());

            return user;
        }
        catch (Exception e)
        {
            throw new IOException("No se ha podido recuperar el id del usuario conectado");
        }
    }

    private void registerUserInHttpSession(HttpServletRequest clientRequest, User user)
    {
        HttpSession serverSession = clientRequest.getSession();
        serverSession.setAttribute(User.SESSION_USER, user);
        serverSession.setAttribute(SESSION_ROLES, getRolesFromPersonaId(user.getId()));
        PersonalizationConfig personalizationConfig = getPersonalizationConfig();
        serverSession.setMaxInactiveInterval(personalizationConfig.sesionTimeout);
    }

    private DefaultUser getDefaultUser()
    {
        WebApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(filterConfig.getServletContext());
        return context.getBean(DefaultUser.class);
    }

    private DefaultUser getUserFromHeaders(HttpServletRequest clientRequest) {
        DefaultUser defaultUser = new DefaultUser();
        defaultUser.defaultUserId = clientRequest.getHeader(DEFAULT_USER_ID_HEADER);
        defaultUser.defaultUsername = clientRequest.getHeader(DEFAULT_USER_NAME_HEADER);

        return defaultUser;
    }

    private PersonalizationConfig getPersonalizationConfig()
    {
        WebApplicationContext context = WebApplicationContextUtils
            .getWebApplicationContext(filterConfig.getServletContext());
        return context.getBean(PersonalizationConfig.class);
    }

    private boolean isCorrectExternalAPICall(String url, String headerAuthToken)
    {
        if (url.startsWith("/goc/rest/external"))
        {
            if (headerAuthToken != null)
            {
                String token = filterConfig.getInitParameter("authToken");

                if (token != null && headerAuthToken.equals(token))
                {
                    return true;
                }
            }
        }

        return false;
    }

    private  List<String> getRolesFromPersonaId(Long personaId)
    {
        WebApplicationContext context = WebApplicationContextUtils
            .getWebApplicationContext(filterConfig.getServletContext());
        PersonaService personaService = context.getBean(PersonaService.class);
        try
        {
            return personaService.getRolesFromPersonaId(personaId);
        } catch (RolesPersonaExternaException e)
        {
            logger.error("No se ha podido recuperar los roles de la persona con id " + personaId);
        }

        return null;
    }

    @Override
    public void destroy()
    {
    }

    private boolean isExcludedUrl(String url)
    {
        String excludedUrls = filterConfig.getInitParameter("excludedUrls");
        Pattern pattern = Pattern.compile(excludedUrls);
        return pattern.matcher(url).matches();
    }
}