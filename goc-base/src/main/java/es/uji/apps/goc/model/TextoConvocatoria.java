package es.uji.apps.goc.model;

public class TextoConvocatoria
{
    private String textoConvocatoria;
    
    private String textoConvocatoriaAlternativo;

    public String getTextoConvocatoria()
    {
        return textoConvocatoria;
    }

    public void setTextoConvocatoria(String textoConvocatoria)
    {
        this.textoConvocatoria = textoConvocatoria;
    }
    
    public String getTextoConvocatoriaAlternativo()
    {
        return textoConvocatoriaAlternativo;
    }

    public void setTextoConvocatoriaAlternativo(String textoConvocatoriaAlternativo)
    {
        this.textoConvocatoriaAlternativo = textoConvocatoriaAlternativo;
    }
}
