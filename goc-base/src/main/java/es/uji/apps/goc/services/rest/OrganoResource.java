package es.uji.apps.goc.services.rest;

import com.google.common.base.Strings;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dto.OrganoAutorizado;
import es.uji.apps.goc.dto.OrganoInvitado;
import es.uji.apps.goc.enums.TipoProcedimientoVotacionEnum;
import es.uji.apps.goc.exceptions.*;
import es.uji.apps.goc.model.DocumentoUI;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.TipoOrgano;
import es.uji.apps.goc.services.OrganoService;
import es.uji.apps.goc.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("organos")
public class OrganoResource extends CoreBaseService {
    @InjectParam
    OrganoService organoService;

    @InjectParam
    PersonaService personaService;

    @InjectParam
    CSVExporter csvExporter;

    @InjectParam
    PersonalizationConfig personalizationConfig;

    @InjectParam
    LanguageConfig languageConfig;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrganos(@QueryParam("reunionId") Long reunionId, @QueryParam("searchString") String searchString)
            throws OrganosExternosException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Organo> listaOrganos;

        if (reunionId != null) {
            listaOrganos = organoService.getOrganosByReunionId(reunionId, connectedUserId);
        } else {
            listaOrganos = organoService.getOrganosByAdminAndAutorizadoId(connectedUserId, personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador), searchString);
        }

        return organosToUI(listaOrganos);
    }

    @GET
    @Path("convocables")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrganosByAutorizado(@QueryParam("reunionId") Long reunionId)
            throws OrganosExternosException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Organo> listaOrganos;

        if (reunionId != null) {
            listaOrganos = organoService.getOrganosByReunionId(reunionId, connectedUserId);
        } else {
            listaOrganos =
                    organoService.getOrganosPorAutorizadoId(connectedUserId,
                            personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador));
        }

        return organosToUI(listaOrganos);

    }

    @GET
    @Path("activos/usuario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrganosActivosByUserId(@QueryParam("nombre") String nombre, @QueryParam("appLang") String lang)
            throws OrganosExternosException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Organo> listaOrganos;

        if (personaService.isUsuario(connectedUserId)) {
            listaOrganos =
                    organoService.getOrganosPorAutorizadoId(connectedUserId,
                            personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador));
        } else {
            listaOrganos = organoService.getOrganosByAdminAndAutorizadoId(connectedUserId, personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador), null);
        }

        if (nombre != null) {
            if (lang != null) {
                if (languageConfig.isMainLangauge(lang)) {
                    listaOrganos = listaOrganos.stream().filter(o -> o.getNombreLimpio().toLowerCase().contains(nombre.toLowerCase())).collect(Collectors.toList());
                } else {
                    listaOrganos = listaOrganos.stream().filter(o -> o.getNombreAlternativoLimpio().toLowerCase().contains(nombre.toLowerCase())).collect(Collectors.toList());
                }
            } else {
                listaOrganos = listaOrganos.stream().filter(o -> o.getNombreLimpio().toLowerCase().contains(nombre.toLowerCase())).collect(Collectors.toList());
            }
        }

        listaOrganos = listaOrganos.stream().filter(o -> o.isExterno() || !o.isInactivo()).collect(Collectors.toList());

        return organosToUI(listaOrganos);
    }

    private List<UIEntity> organosToUI(List<Organo> listaOrganos) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (Organo organo : listaOrganos.stream().sorted(Comparator.comparing(Organo::getNombreLimpio)).collect(Collectors.toList())) {
            listaUI.add(organoToUI(organo));
        }

        return listaUI;
    }

    private UIEntity organoToUI(Organo organo) {
        UIEntity ui = new UIEntity();
        ui.put("id", organo.getId());
        ui.put("nombre", organo.getNombre());
        ui.put("nombreAlternativo", organo.getNombreAlternativo());
        ui.put("externo", organo.isExterno());
        ui.put("inactivo", organo.isInactivo());
        ui.put("tipoOrganoId", organo.getTipoOrgano().getId());
        ui.put("convocarSinOrdenDia", organo.getConvocarSinOrdenDia());
        ui.put("email", organo.getEmail());
        ui.put("actaProvisionalActiva", organo.getActaProvisionalActiva());
        ui.put("delegacionVotoMultiple", organo.getDelegacionVotoMultiple());
        ui.put("tipoProcedimientoVotacionId", organo.getTipoProcedimientoVotacionEnum());
        ui.put("presidenteVotoDoble", organo.getPresidenteVotoDoble());
        ui.put("permiteAbstencionVoto", organo.getPermiteAbstencionVoto());
        ui.put("mostrarAsistencia", organo.getVerAsistencia());
        ui.put("verDelegaciones", organo.getVerDelegaciones());
        return ui;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addOrgano(UIEntity organoUI)
            throws PersonasExternasException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Organo organo = uiToModel(organoUI);
        organo = organoService.addOrgano(organo, connectedUserId);

        return UIEntity.toUI(organo);
    }

    @PUT
    @Path("{organoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity modificaOrgano(@PathParam("organoId") String organoId, UIEntity organoUI)
            throws OrganoNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String id = organoUI.get("id");
        String nombre = organoUI.get("nombre");
        String nombreAlternativo = organoUI.get("nombreAlternativo");
        Long tipoOrganoId = Long.parseLong(organoUI.get("tipoOrganoId"));
        Boolean inactivo = new Boolean(organoUI.get("inactivo"));
        Boolean convocarSinOrdenDia = new Boolean(organoUI.get("convocarSinOrdenDia"));
        Boolean actaProvisionalActiva = new Boolean(organoUI.get("actaProvisionalActiva"));
        Boolean delegacionVotoMultiple = new Boolean(organoUI.get("delegacionVotoMultiple"));
        Boolean externo = new Boolean(organoUI.get("externo"));
        String email = organoUI.get("email");
        String tipoProcedimientoVotacionId = organoUI.get("tipoProcedimientoVotacionId");
        Boolean presidenteVotoDoble = new Boolean(organoUI.get("presidenteVotoDoble"));
        Boolean permiteAbstencionVoto = new Boolean(organoUI.get("permiteAbstencionVoto"));
        Boolean verAsistentes = organoUI.getBoolean("mostrarAsistencia");
        Boolean verDelegaciones = organoUI.getBoolean("verDelegaciones");

        if (externo) {
            organoService.actualizarParametros(id, externo, convocarSinOrdenDia, email, actaProvisionalActiva, delegacionVotoMultiple,
                    tipoProcedimientoVotacionId, presidenteVotoDoble, permiteAbstencionVoto, verAsistentes, verDelegaciones);

            return organoUI;
        } else {
            Organo organo = organoService.updateOrgano(Long.valueOf(id), nombre, nombreAlternativo, tipoOrganoId, inactivo, convocarSinOrdenDia, email,
                    actaProvisionalActiva, delegacionVotoMultiple, connectedUserId, tipoProcedimientoVotacionId, presidenteVotoDoble, permiteAbstencionVoto, verAsistentes, verDelegaciones);

            return UIEntity.toUI(organo);
        }
    }

    private Organo uiToModel(UIEntity organoUI) {
        Organo organo = new Organo();

        if (ParamUtils.parseLong(organoUI.get("id")) != null) {
            organo.setId(organoUI.get("id"));
        }

        organo.setNombre(organoUI.get("nombre"));
        organo.setNombreAlternativo(organoUI.get("nombreAlternativo"));
        organo.setInactivo(false);
        organo.setConvocarSinOrdenDia(new Boolean(organoUI.get("convocarSinOrdenDia")));
        organo.setActaProvisionalActiva(new Boolean(organoUI.get("actaProvisionalActiva")));
        organo.setDelegacionVotoMultiple(new Boolean(organoUI.get("delegacionVotoMultiple")));
        organo.setEmail(organoUI.get("email"));

        TipoOrgano tipoOrgano = new TipoOrgano();
        tipoOrgano.setId(Long.parseLong(organoUI.get("tipoOrganoId")));
        organo.setTipoOrgano(tipoOrgano);

        String tipoProcedimientoVotacionId = organoUI.get("tipoProcedimientoVotacionId");
        if (!Strings.isNullOrEmpty(tipoProcedimientoVotacionId)) {
            organo.setTipoProcedimientoVotacionEnum(TipoProcedimientoVotacionEnum.valueOf(tipoProcedimientoVotacionId));
        }
        organo.setPresidenteVotoDoble(new Boolean(organoUI.get("presidenteVotoDoble")));

        organo.setPermiteAbstencionVoto(new Boolean(organoUI.get("permiteAbstencionVoto")));

        organo.setVerAsistencia(organoUI.getBoolean("mostrarAsistencia"));

        organo.setVerDelegaciones(organoUI.getBoolean("verDelegaciones"));

        return organo;
    }

    @GET
    @Path("autorizados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAutorizados(@QueryParam("organoId") String organoId,
                                         @QueryParam("externo") Boolean externo) {
        return UIEntity.toUI(organoService.getAutorizados(organoId, externo));
    }

    @POST
    @Path("autorizados")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addAutorizado(UIEntity autorizado) {
        OrganoAutorizado newOrganoAutorizado = organoService.addAutorizado(autorizado.toModel(OrganoAutorizado.class));

        return UIEntity.toUI(newOrganoAutorizado);
    }

    @DELETE
    @Path("autorizados/{organoAutorizadoId}")
    public Response borraAutorizado(@PathParam("organoAutorizadoId") Long organoAutorizadoId) {
        organoService.removeAutorizado(organoAutorizadoId);

        return Response.ok().build();
    }

    @PUT
    @Path("autorizados/{organoAutorizadoId}")
    public Response updateAutorizado(@PathParam("organoAutorizadoId") Long organoAutorizadoId, OrganoAutorizado organo) {
        organoService.updateAutorizado(organo);
        return Response.ok().build();
    }

    @GET
    @Path("invitados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInvitados(@QueryParam("organoId") String organoId, @QueryParam("externo") Boolean externo) {
        return UIEntity.toUI(organoService.getInvitados(organoId, externo));
    }

    @POST
    @Path("invitados")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addInvitado(UIEntity invitado) {
        OrganoInvitado newOrganoInvitado = organoService.addInvitado(invitado.toModel(OrganoInvitado.class));

        return UIEntity.toUI(newOrganoInvitado);
    }

    @PUT
    @Path("invitados/{organoInvitadoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateInvitado(UIEntity invitado, @PathParam("organoInvitadoId") Long organoInvitadoId) {
        organoService.updateInvitado(invitado.toModel(OrganoInvitado.class));

        return UIEntity.toUI(invitado);
    }

    @DELETE
    @Path("invitados/{organoInvitadoId}")
    public Response borraInvitado(@PathParam("organoInvitadoId") Long organoInvitadoId) {
        organoService.removeInvitado(organoInvitadoId);

        return Response.ok().build();
    }

    @GET
    @Path("{organoId}/export")
    public Response exportarMiembrosByOrganoId(@PathParam("organoId") String organoId)
            throws MiembrosExternosException, IOException, DocumentoNoEncontradoException {
        User connectedUser = AccessManager.getConnectedUser(request);
        DocumentoUI documentoUI = csvExporter.exportaMiembros(organoId, connectedUser.getId());
        return Response.ok(documentoUI.getData())
                .header("Content-Disposition", "attachment; filename = \"" + organoId + ".csv" + "\"")
                .header("Content-Length", documentoUI.getData().length).header("Content-Type", documentoUI.getMimeType())
                .build();
    }
}
