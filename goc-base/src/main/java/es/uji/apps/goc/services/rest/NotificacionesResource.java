package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.ReunionNoCompletadaException;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.apps.goc.services.ReunionMiembroService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.commons.rest.UIEntity;

@Path("notificaciones")
public class NotificacionesResource
{

    @InjectParam
    ReunionService reunionService;

    @InjectParam
    ReunionMiembroService reunionMiembroService;

    @InjectParam
    PuntoOrdenDiaService puntoOrdenDiaService;

    @InjectParam
    PersonalizationConfig personalizationConfig;

    @PUT
    @Path("reunion/{reunionId}/urlActa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarUrlActa(@PathParam("reunionId") Long reunionId, UIEntity uiEntity)
        throws ReunionNoCompletadaException
    {
        String urlActa = uiEntity.get("urlActa");

        validaReunionCerrada(reunionId);
        reunionService.addUrlActa(reunionId, urlActa);

        return Response.ok().build();
    }

    private void validaReunionCerrada(Long reunionId) throws ReunionNoCompletadaException
    {
        if(!personalizationConfig.mostrarPublicarAcuerdos)
        {
            Reunion reunionById = reunionService.getReunionById(reunionId);
            if (!reunionById.isCompletada())
                throw new ReunionNoCompletadaException();
        }
    }

    @PUT
    @Path("reunion/{reunionId}/urlActaAlternativa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarUrlActaAlternativa(@PathParam("reunionId") Long reunionId, UIEntity uiEntity)
        throws ReunionNoCompletadaException
    {
        String urlActaAlternativa = uiEntity.get("urlActaAlternativa");

        validaReunionCerrada(reunionId);
        reunionService.addUrlActaAlternativa(reunionId, urlActaAlternativa);

        return Response.ok().build();
    }

    @PUT
    @Path("puntoOrdenDia/{puntoOrdenDiaId}/urlActa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarUrlActaOrdenDia(
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
        UIEntity uiEntity) throws ReunionNoCompletadaException
    {
        String urlActaAlternativa = uiEntity.get("urlActa");

        PuntoOrdenDia puntoById = puntoOrdenDiaService.getPuntoById(puntoOrdenDiaId);
        validaReunionCerrada(puntoById.getReunion().getId());
        puntoOrdenDiaService.addPuntoOrdenDiaUrlActa(puntoOrdenDiaId, urlActaAlternativa);

        return Response.ok().build();
    }

    @PUT
    @Path("puntoOrdenDia/{puntoOrdenDiaId}/urlActaAlternativa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarUrlActaAlternativaOrdenDia(
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
        UIEntity uiEntity) throws ReunionNoCompletadaException
    {
        String urlActaAlternativa = uiEntity.get("urlActaAlternativa");

        PuntoOrdenDia puntoById = puntoOrdenDiaService.getPuntoById(puntoOrdenDiaId);
        validaReunionCerrada(puntoById.getReunion().getId());
        puntoOrdenDiaService.addPuntoOrdenDiaUrlActaAlternativa(puntoOrdenDiaId, urlActaAlternativa);

        return Response.ok().build();
    }

    @PUT
    @Path("reunion/{reunionId}/persona/{personaId}/urlAsistencia")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarUrlAsistencia(
        @PathParam("reunionId") Long reunionId,
        @PathParam("personaId") Long personaId,
        UIEntity uiEntity
    )
    {
        String urlAsistencia = uiEntity.get("urlAsistencia");
        reunionService.actualizarAsistenciaAsincrona(reunionId, personaId, urlAsistencia);

        return Response.ok().build();
    }

    @PUT
    @Path("reunion/{reunionId}/persona/{personaId}/urlAsistenciaAlternativa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarUrlAsistenciaAlternativa(
        @PathParam("reunionId") Long reunionId,
        @PathParam("personaId") Long personaId,
        UIEntity uiEntity
    )
    {
        String urlAsistenciaAlternativa = uiEntity.get("urlAsistenciaAlternativa");
        reunionService.actualizarAsistenciaAlternativaAsincrona(reunionId, personaId, urlAsistenciaAlternativa);

        return Response.ok().build();
    }
}
