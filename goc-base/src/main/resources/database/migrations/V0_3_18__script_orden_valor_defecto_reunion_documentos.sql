DECLARE
  l_orden NUMBER := 0;
  l_reun_id_anterior NUMBER :=0;
   -- cursor
  CURSOR c_reun_doc IS
SELECT ID, REUNION_ID, grd.FECHA_ADICION FROM GOC_REUNIONES_DOCUMENTOS grd GROUP BY ID,REUNION_ID, grd.FECHA_ADICION ORDER BY grd.FECHA_ADICION DESC;
   -- record    
   r_reun_doc c_reun_doc%ROWTYPE;
BEGIN
	OPEN c_reun_doc;
 
LOOP
    FETCH  c_reun_doc  INTO r_reun_doc;
    EXIT WHEN c_reun_doc%NOTFOUND;
	

    UPDATE
        GOC_REUNIONES_DOCUMENTOS
    SET  
        orden =
            CASE WHEN r_reun_doc.reunion_id = l_reun_id_anterior
                        THEN l_orden + 10
                        ELSE 10
            END
    WHERE
        id = r_reun_doc.id;
 

	IF (r_reun_doc.reunion_id = l_reun_id_anterior) THEN
		l_orden := l_orden + 10;
	ELSE
		l_orden := 10;
	END IF ;
    
	l_reun_id_anterior := r_reun_doc.reunion_id;
 
    
  END LOOP;
 
  CLOSE c_reun_doc;
END;	