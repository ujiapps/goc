DECLARE
  l_orden NUMBER := 0;
  l_punto_id_anterior NUMBER :=0;
   -- cursor
  CURSOR c_punto_acu IS
SELECT ID, PUNTO_ID, gpa.FECHA_ADICION FROM GOC_P_ORDEN_DIA_ACUERDOS gpa GROUP BY ID,PUNTO_ID, gpa.FECHA_ADICION ORDER BY gpa.FECHA_ADICION DESC;
   -- record    
   r_punto_acu c_punto_acu%ROWTYPE;
BEGIN
	OPEN c_punto_acu;
 
LOOP
    FETCH  c_punto_acu  INTO r_punto_acu;
    EXIT WHEN c_punto_acu%NOTFOUND;
	
    
    UPDATE
        GOC_P_ORDEN_DIA_ACUERDOS
    SET  
        orden =
            CASE WHEN r_punto_acu.punto_id = l_punto_id_anterior
                        THEN l_orden + 10
                        ELSE 10
            END
    WHERE
        id = r_punto_acu.id;
 
    
	IF (r_punto_acu.punto_id = l_punto_id_anterior) THEN
		l_orden := l_orden + 10;
	ELSE
		l_orden := 10;
	END IF ;
    
	l_punto_id_anterior := r_punto_acu.punto_id;
 
    
  END LOOP;
 
  CLOSE c_punto_acu;
END;	