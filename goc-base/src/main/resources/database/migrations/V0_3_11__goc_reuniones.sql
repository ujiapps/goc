ALTER TABLE GOC_REUNIONES_PUNTOS_ORDEN_DIA
    ADD (votacion_cerrada_manualmente NUMBER(1) DEFAULT 0 NOT NULL);

CREATE OR REPLACE FORCE VIEW GOC_VW_REU_PT_OR_DIA
            (ID, TITULO, TITULO_ALT, DESCRIPCION, DESCRIPCION_ALT,
             ORDEN, REUNION_ID, ACUERDOS, ACUERDOS_ALT, DELIBERACIONES,
             DELIBERACIONES_ALT, PUBLICO, URL_ACTA, URL_ACTA_ALT, URL_ACTA_ANTERIOR,
             URL_ACTA_ANTERIOR_ALT, EDITADO_EN_REAPERTURA, ID_PUNTO_SUPERIOR, PROFUNDIDAD, ORDEN_NIVEL_ZERO,
             TIPO_VOTO, PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_ABIERTA, VOTABLE_EN_CONTRA,
             VOTACION_CERRADA_MANUALMENTE)
            BEQUEATH DEFINER
AS
WITH
    ca
        AS
        (SELECT
             ID,
             TITULO,
             TITULO_ALT,
             DESCRIPCION,
             DESCRIPCION_ALT,
             ORDEN,
             REUNION_ID,
             ACUERDOS,
             ACUERDOS_ALT,
             DELIBERACIONES,
             DELIBERACIONES_ALT,
             PUBLICO,
             URL_ACTA,
             URL_ACTA_ALT,
             URL_ACTA_ANTERIOR,
             URL_ACTA_ANTERIOR_ALT,
             EDITADO_EN_REAPERTURA,
             ID_PUNTO_SUPERIOR,
             level AS PROFUNDIDAD,
             CONNECT_BY_ROOT orden AS ORDEN_NIVEL_ZERO,
             TIPO_VOTO,
             PROCEDIMIENTO_VOTACION,
             TIPO_RECUENTO_VOTO,
             VOTACION_ABIERTA,
             VOTABLE_EN_CONTRA,
             VOTACION_CERRADA_MANUALMENTE
         FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
         START WITH id IN (SELECT id
                           FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                           WHERE ID_PUNTO_SUPERIOR IS NULL)
         CONNECT BY ID_PUNTO_SUPERIOR = PRIOR id
        ) SELECT "ID","TITULO","TITULO_ALT","DESCRIPCION","DESCRIPCION_ALT","ORDEN","REUNION_ID","ACUERDOS","ACUERDOS_ALT","DELIBERACIONES","DELIBERACIONES_ALT","PUBLICO","URL_ACTA","URL_ACTA_ALT","URL_ACTA_ANTERIOR","URL_ACTA_ANTERIOR_ALT","EDITADO_EN_REAPERTURA","ID_PUNTO_SUPERIOR","PROFUNDIDAD","ORDEN_NIVEL_ZERO","TIPO_VOTO","PROCEDIMIENTO_VOTACION","TIPO_RECUENTO_VOTO","VOTACION_ABIERTA","VOTABLE_EN_CONTRA",
                 "VOTACION_CERRADA_MANUALMENTE"
FROM ca
ORDER BY ORDEN_NIVEL_ZERO asc;

ALTER TABLE GOC_REUNIONES RENAME COLUMN VER_RESULTADOS_MIENTRAS_ACTIVA TO TIPO_VISUALIZACION_RESULTADOS;

CREATE OR REPLACE FORCE VIEW GOC_VW_REUNIONES_EDITORES
(
    ID,
    ASUNTO,
    ASUNTO_ALT,
    FECHA,
    DURACION,
    NUM_DOCUMENTOS,
    EDITOR_ID,
    COMPLETADA,
    EXTERNO,
    ORGANO_ID,
    TIPO_ORGANO_ID,
    AVISO_PRIMERA_REUNION,
    AVISO_PRIMERA_REUNION_USER,
    AVISO_PRIMERA_REUNION_FECHA,
    URL_ACTA,
    URL_ACTA_ALT,
    VOTACION_TELEMATICA,
    TIPO_VISUALIZACION_RESULTADOS
)
AS
SELECT
    r.id,
    r.asunto,
    r.asunto_alt,
    r.fecha,
    r.duracion,
    (SELECT COUNT(*)
     FROM goc_reuniones_documentos rd
     WHERE rd.reunion_id = r.id)
                 num_documentos,
    r.creador_id editor_id,
    r.completada completada,
    o.externo,
    o.organo_id,
    o.tipo_organo_id,
    r.aviso_primera_reunion,
    r.aviso_primera_reunion_user,
    r.aviso_primera_reunion_fecha,
    r.url_acta,
    r.url_acta_alt,
    r.votacion_telematica,
    r.TIPO_VISUALIZACION_RESULTADOS
FROM goc_reuniones r LEFT OUTER JOIN goc_organos_reuniones o on r.id = o.reunion_id
UNION
SELECT
    r.id,
    r.asunto,
    r.asunto_alt,
    r.fecha,
    r.duracion,
    (SELECT COUNT(*)
     FROM goc_reuniones_documentos rd
     WHERE rd.reunion_id = r.id)
                 num_documentos,
    a.persona_id editor_id,
    r.completada completada,
    o.externo,
    o.organo_id,
    o.tipo_organo_id,
    r.aviso_primera_reunion,
    r.aviso_primera_reunion_user,
    r.aviso_primera_reunion_fecha,
    r.url_acta,
    r.url_acta_alt,
    r.votacion_telematica,
    r.TIPO_VISUALIZACION_RESULTADOS
FROM goc_reuniones r, goc_organos_reuniones o, goc_organos_autorizados a
WHERE r.id = o.reunion_id AND o.organo_id = a.organo_id;
