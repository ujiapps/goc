CREATE OR REPLACE FORCE VIEW GOC_VW_REU_PT_OR_DIA
            (
             ID,
             TITULO,
             TITULO_ALT,
             DESCRIPCION,
             DESCRIPCION_ALT,
             ORDEN,
             REUNION_ID,
             ACUERDOS,
             ACUERDOS_ALT,
             DELIBERACIONES,
             DELIBERACIONES_ALT,
             PUBLICO,
             URL_ACTA,
             URL_ACTA_ALT,
             URL_ACTA_ANTERIOR,
             URL_ACTA_ANTERIOR_ALT,
             EDITADO_EN_REAPERTURA,
             ID_PUNTO_SUPERIOR,
             PROFUNDIDAD,
             ORDEN_NIVEL_ZERO,
             TIPO_VOTO,
    PROCEDIMIENTO_VOTACION,
    TIPO_RECUENTO_VOTO,
    VOTACION_ABIERTA
                )
AS
WITH
    ca
        AS
        (SELECT
             ID,
             TITULO,
             TITULO_ALT,
             DESCRIPCION,
             DESCRIPCION_ALT,
             ORDEN,
             REUNION_ID,
             ACUERDOS,
             ACUERDOS_ALT,
             DELIBERACIONES,
             DELIBERACIONES_ALT,
             PUBLICO,
             URL_ACTA,
             URL_ACTA_ALT,
             URL_ACTA_ANTERIOR,
             URL_ACTA_ANTERIOR_ALT,
             EDITADO_EN_REAPERTURA,
             ID_PUNTO_SUPERIOR,
             level AS PROFUNDIDAD,
             CONNECT_BY_ROOT orden AS ORDEN_NIVEL_ZERO,
             TIPO_VOTO,
             PROCEDIMIENTO_VOTACION,
             TIPO_RECUENTO_VOTO,
             VOTACION_ABIERTA
         FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
         START WITH id IN (SELECT id
                           FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                           WHERE ID_PUNTO_SUPERIOR IS NULL)
         CONNECT BY ID_PUNTO_SUPERIOR = PRIOR id
        )
SELECT ID,
     TITULO,
     TITULO_ALT,
     DESCRIPCION,
     DESCRIPCION_ALT,
     ORDEN,
     REUNION_ID,
     ACUERDOS,
     ACUERDOS_ALT,
     DELIBERACIONES,
     DELIBERACIONES_ALT,
     PUBLICO,
     URL_ACTA,
     URL_ACTA_ALT,
     URL_ACTA_ANTERIOR,
     URL_ACTA_ANTERIOR_ALT,
     EDITADO_EN_REAPERTURA,
     ID_PUNTO_SUPERIOR,
     PROFUNDIDAD,
     ORDEN_NIVEL_ZERO,
     TIPO_VOTO,
     PROCEDIMIENTO_VOTACION,
     TIPO_RECUENTO_VOTO,
     VOTACION_ABIERTA
FROM ca
ORDER BY ORDEN_NIVEL_ZERO asc;