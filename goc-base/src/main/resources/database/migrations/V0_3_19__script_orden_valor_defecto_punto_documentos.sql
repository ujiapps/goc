DECLARE
  l_orden NUMBER := 0;
  l_punto_id_anterior NUMBER :=0;
   -- cursor
  CURSOR c_punto_doc IS
SELECT ID, PUNTO_ID, gpd.FECHA_ADICION FROM GOC_P_ORDEN_DIA_DOCUMENTOS gpd GROUP BY ID,PUNTO_ID, gpd.FECHA_ADICION ORDER BY gpd.FECHA_ADICION DESC;
   -- record    
   r_punto_doc c_punto_doc%ROWTYPE;
BEGIN
	OPEN c_punto_doc;
 
LOOP
    FETCH  c_punto_doc  INTO r_punto_doc;
    EXIT WHEN c_punto_doc%NOTFOUND;
	
    
    UPDATE
        GOC_P_ORDEN_DIA_DOCUMENTOS
    SET  
        orden =
            CASE WHEN r_punto_doc.punto_id = l_punto_id_anterior
                        THEN l_orden + 10
                        ELSE 10
            END
    WHERE
        id = r_punto_doc.id;
 
    
	IF (r_punto_doc.punto_id = l_punto_id_anterior) THEN
		l_orden := l_orden + 10;
	ELSE
		l_orden := 10;
	END IF ;
    
	l_punto_id_anterior := r_punto_doc.punto_id;
 
    
  END LOOP;
 
  CLOSE c_punto_doc;
END;	