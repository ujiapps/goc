ALTER TABLE GOC_REUNIONES_DOCUMENTOS ADD EDITOR_ID NUMBER;
ALTER TABLE GOC_REUNIONES_DOCUMENTOS ADD FECHA_EDICION DATE;
ALTER TABLE GOC_REUNIONES_DOCUMENTOS ADD MOTIVO_EDICION VARCHAR2(250);
ALTER TABLE GOC_REUNIONES_DOCUMENTOS ADD ACTIVO NUMBER(1,0) DEFAULT 1 NOT NULL;

ALTER TABLE GOC_P_ORDEN_DIA_DOCUMENTOS ADD EDITOR_ID NUMBER;
ALTER TABLE GOC_P_ORDEN_DIA_DOCUMENTOS ADD FECHA_EDICION DATE;
ALTER TABLE GOC_P_ORDEN_DIA_DOCUMENTOS ADD MOTIVO_EDICION VARCHAR2(250);
ALTER TABLE GOC_P_ORDEN_DIA_DOCUMENTOS ADD ACTIVO NUMBER(1,0) DEFAULT 1 NOT NULL;

ALTER TABLE GOC_P_ORDEN_DIA_ACUERDOS ADD EDITOR_ID NUMBER;
ALTER TABLE GOC_P_ORDEN_DIA_ACUERDOS ADD FECHA_EDICION DATE;
ALTER TABLE GOC_P_ORDEN_DIA_ACUERDOS ADD MOTIVO_EDICION VARCHAR2(250);
ALTER TABLE GOC_P_ORDEN_DIA_ACUERDOS ADD ACTIVO NUMBER(1,0) DEFAULT 1 NOT NULL;