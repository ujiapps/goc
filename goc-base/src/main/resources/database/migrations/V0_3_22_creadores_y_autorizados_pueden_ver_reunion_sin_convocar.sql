CREATE OR REPLACE FORCE VIEW UJI_REUNIONES.GOC_VW_REUNIONES_PERMISOS
(ID, COMPLETADA, FECHA, ASUNTO, ASUNTO_ALT,
 PERSONA_ID, PERSONA_NOMBRE, ASISTENTE, URL_ACTA, URL_ACTA_ALT,
 AVISO_PRIMERA_REUNION, URL_ASISTENCIA, URL_ASISTENCIA_ALT, ENLACE_ACTA_PROVISIONAL_ACTIVO, ORGANO_ID, ORGANO_EXTERNO,
 ORGANO_NOMBRE, ORGANO_NOMBRE_ALT, RESPONSABLE_VOTO_ID, RESPONSABLE_VOTO_NOM, VER_REUNION_SIN_CONVOCAR)
BEQUEATH DEFINER
AS
SELECT
    s.id,
    s.completada,
    s.fecha,
    s.asunto,
    s.asunto_alt,
    s.persona_id,
    s.persona_nombre,
    SUM(s.asistente) asistente,
    s.url_acta,
    s.url_acta_alt,
    s.aviso_primera_reunion,
    (SELECT a.url_asistencia
     FROM goc_vw_certificados_asistencia a
     WHERE a.reunion_id = s.id
       AND a.persona_id = s.persona_id
       AND ROWNUM = 1)
                     url_asistencia,
    (SELECT a.url_asistencia_alt
     FROM goc_vw_certificados_asistencia a
     WHERE a.reunion_id = s.id
       AND a.persona_id = s.persona_id
       AND ROWNUM = 1)
                     url_asistencia_alt,
    nvl(enlace_acta_provisional_activo, 0) enlace_acta_provisional_activo,
    organo_id,
    externo organo_externo,
    organo_nombre,
    organo_nombre_alt,
    responsable_voto_id,
    responsable_voto_nom,
    (select decode(count(*), 0, 0, 1)
     from goc_reuniones r
              left join goc_organos_reuniones orr ON r.id = orr.reunion_id
              left join goc_organos_autorizados ra ON orr.organo_id = ra.organo_id
     where r.id = s.id
       and (r.creador_id = s.persona_id
         or ra.persona_id = s.persona_id)) ver_reunion_sin_convocar
FROM (SELECT --creador
             r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.aviso_primera_reunion,
             r.creador_id     persona_id,
             r.creador_nombre persona_nombre,
             DECODE(suplente_id, NULL, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             op.acta_provisional_activa enlace_acta_provisional_activo,
             ore.organo_id,
             ore.externo,
             ore.ORGANO_NOMBRE organo_nombre,
             ore.ORGANO_NOMBRE_ALT organo_nombre_alt,
             r.responsable_voto_id,
             r.responsable_voto_nom
      FROM goc_reuniones r
               LEFT JOIN goc_organos_reuniones ore
                         ON r.id = ore.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON ore.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON ore.organo_id = op.id_organo AND ore.externo = op.ordinario
      UNION
      SELECT --autorizado
             r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.aviso_primera_reunion,
             ra.persona_id     persona_id,
             ra.persona_nombre persona_nombre,
             DECODE(suplente_id, NULL, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             OP.acta_provisional_activa enlace_acta_provisional_activo,
             orr.organo_id,
             orr.externo,
             orr.ORGANO_NOMBRE,
             orr.ORGANO_NOMBRE_ALT,
             r.responsable_voto_id,
             r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               JOIN goc_organos_autorizados ra ON orr.organo_id = ra.organo_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      UNION
      SELECT --invitado reunion
             r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.aviso_primera_reunion,
             ri.persona_id     persona_id,
             ri.persona_nombre persona_nombre,
             DECODE(orrm.suplente_id, NULL, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             op.acta_provisional_activa enlace_acta_provisional_activo,
             orr.organo_id,
             orr.externo,
             orr.ORGANO_NOMBRE,
             orr.ORGANO_NOMBRE_ALT,
             r.responsable_voto_id,
             r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_reuniones_invitados ri ON ri.reunion_id = r.id
               LEFT JOIN goc_organos_reuniones orr
                         ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      UNION
      SELECT --invitado organo
             r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.aviso_primera_reunion,
             ori.persona_id                        persona_id,
             ori.nombre                            persona_nombre,
             decode(ori.solo_consulta, 0, 1, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             op.acta_provisional_activa enlace_acta_provisional_activo,
             orr.organo_id,
             orr.externo,
             orr.ORGANO_NOMBRE,
             orr.ORGANO_NOMBRE_ALT,
             r.responsable_voto_id,
             r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               JOIN goc_organos_reuniones_invits ori ON ori.organo_reunion_id = orr.id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      UNION
      SELECT --suplente
             r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.aviso_primera_reunion,
             orrm.suplente_id     persona_id,
             orrm.suplente_nombre persona_nombre,
             DECODE(orrm.suplente_id, NULL, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             op.acta_provisional_activa enlace_acta_provisional_activo,
             orr.organo_id,
             orr.externo,
             orr.ORGANO_NOMBRE,
             orr.ORGANO_NOMBRE_ALT,
             r.responsable_voto_id,
             r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      WHERE suplente_id IS NOT NULL
      UNION
      SELECT --miembro asistente
             r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.aviso_primera_reunion,
             cast(orrm.miembro_id as number(*)) persona_id,
             orrm.nombre                     persona_nombre,
             DECODE(orrm.suplente_id, NULL, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             op.acta_provisional_activa enlace_acta_provisional_activo,
             orr.organo_id,
             orr.externo,
             orr.ORGANO_NOMBRE,
             orr.ORGANO_NOMBRE_ALT,
             r.responsable_voto_id,
             r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      WHERE orrm.asistencia = 1
      UNION
      SELECT --miembro no asistente
             r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.aviso_primera_reunion,
             cast(orrm.miembro_id as number(*)) persona_id,
             orrm.nombre     persona_nombre,
             DECODE(orrm.suplente_id, NULL, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             op.acta_provisional_activa enlace_acta_provisional_activo,
             orr.organo_id,
             orr.externo,
             orr.ORGANO_NOMBRE,
             orr.ORGANO_NOMBRE_ALT,
             r.responsable_voto_id,
             r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      WHERE orrm.asistencia = 0) s
GROUP BY s.id,
         s.completada,
         s.fecha,
         s.asunto,
         s.asunto_alt,
         s.persona_id,
         s.persona_nombre,
         s.url_acta,
         s.url_acta_alt,
         s.aviso_primera_reunion,
         s.enlace_acta_provisional_activo,
         s.organo_id,
         s.externo,
         s.organo_nombre,
         s.organo_nombre_alt,
         s.responsable_voto_id,
         s.responsable_voto_nom;

CREATE INDEX UJI_REUNIONES.goc_orm_or_i ON UJI_REUNIONES.GOC_ORGANOS_REUNIONES_MIEMBROS
    (ORGANO_REUNION_ID);

CREATE INDEX UJI_REUNIONES.goc_oa_o_i ON UJI_REUNIONES.goc_organos_autorizados
    (organo_id);

CREATE INDEX UJI_REUNIONES.goc_oa_o_i ON UJI_REUNIONES.goc_organos_autorizados
    (organo_id);

CREATE INDEX UJI_REUNIONES.goc_ori_or_i ON UJI_REUNIONES.goc_organos_reuniones_invits
    (ORGANO_REUNION_ID);