DROP TABLE GOC_VW_REUNIONES_EDITORES IF EXISTS;
DROP TABLE GOC_VW_REU_PT_OR_DIA IF EXISTS;
DROP TABLE GOC_VW_CERTIFICADOS_ASISTENCIA IF EXISTS;
DROP TABLE GOC_VW_REUNIONES_PERMISOS IF EXISTS;
DROP TABLE GOC_VW_VOTOS IF EXISTS;

CREATE
    OR
    REPLACE
    FORCE VIEW GOC_VW_REU_PT_OR_DIA
            (
             ID,
             TITULO,
             TITULO_ALT,
             DESCRIPCION,
             DESCRIPCION_ALT,
             ORDEN,
             REUNION_ID,
             ACUERDOS,
             ACUERDOS_ALT,
             DELIBERACIONES,
             DELIBERACIONES_ALT,
             PUBLICO,
             URL_ACTA,
             URL_ACTA_ALT,
             URL_ACTA_ANTERIOR,
             URL_ACTA_ANTERIOR_ALT,
             EDITADO_EN_REAPERTURA,
             ID_PUNTO_SUPERIOR,
             PROFUNDIDAD,
             ORDEN_NIVEL_ZERO,
             TIPO_VOTO,
             PROCEDIMIENTO_VOTACION,
             TIPO_RECUENTO_VOTO,
             VOTACION_ABIERTA,
             VOTABLE_EN_CONTRA,
             VOTACION_CERRADA_MANUALMENTE,
             TIPO_VOTACION
)
AS
WITH RECURSIVE ca(ID,
                  TITULO,
                  TITULO_ALT,
                  DESCRIPCION,
                  DESCRIPCION_ALT,
                  ORDEN,
                  REUNION_ID,
                  ACUERDOS,
                  ACUERDOS_ALT,
                  DELIBERACIONES,
                  DELIBERACIONES_ALT,
                  PUBLICO,
                  URL_ACTA,
                  URL_ACTA_ALT,
                  URL_ACTA_ANTERIOR,
                  URL_ACTA_ANTERIOR_ALT,
                  EDITADO_EN_REAPERTURA,
                  ID_PUNTO_SUPERIOR,
                  PROFUNDIDAD,
                  ORDEN_NIVEL_ZERO,
                  TIPO_VOTO,
                  PROCEDIMIENTO_VOTACION,
                  TIPO_RECUENTO_VOTO,
                  VOTACION_ABIERTA,
                  VOTABLE_EN_CONTRA,
                  VOTACION_CERRADA_MANUALMENTE,
                  TIPO_VOTACION
)
                   AS
                   (SELECT ID,
                           TITULO,
                           TITULO_ALT,
                           DESCRIPCION,
                           DESCRIPCION_ALT,
                           ORDEN,
                           REUNION_ID,
                           ACUERDOS,
                           ACUERDOS_ALT,
                           DELIBERACIONES,
                           DELIBERACIONES_ALT,
                           PUBLICO,
                           URL_ACTA,
                           URL_ACTA_ALT,
                           URL_ACTA_ANTERIOR,
                           URL_ACTA_ANTERIOR_ALT,
                           EDITADO_EN_REAPERTURA,
                           ID_PUNTO_SUPERIOR,
                           0 AS PROFUNDIDAD,
                           0 AS ORDEN_NIVEL_ZERO,
                           TIPO_VOTO,
                           PROCEDIMIENTO_VOTACION,
                           TIPO_RECUENTO_VOTO,
                           VOTACION_ABIERTA,
                           VOTABLE_EN_CONTRA,
                           VOTACION_CERRADA_MANUALMENTE,
                           TIPO_VOTACION
                    FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                    WHERE id IN (SELECT id
                                 FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                                 WHERE ID_PUNTO_SUPERIOR IS NULL)
                    UNION ALL
                    SELECT r.ID,
                           r.TITULO,
                           r.TITULO_ALT,
                           r.DESCRIPCION,
                           r.DESCRIPCION_ALT,
                           r.ORDEN,
                           r.REUNION_ID,
                           r.ACUERDOS,
                           r.ACUERDOS_ALT,
                           r.DELIBERACIONES,
                           r.DELIBERACIONES_ALT,
                           r.PUBLICO,
                           r.URL_ACTA,
                           r.URL_ACTA_ALT,
                           r.URL_ACTA_ANTERIOR,
                           r.URL_ACTA_ANTERIOR_ALT,
                           r.EDITADO_EN_REAPERTURA,
                           r.ID_PUNTO_SUPERIOR,
                           PROFUNDIDAD + 1 AS PROFUNDIDAD,
                           0               AS ORDEN_NIVEL_ZERO,
                           r.TIPO_VOTO,
                           r.PROCEDIMIENTO_VOTACION,
                           r.TIPO_RECUENTO_VOTO,
                           r.VOTACION_ABIERTA,
                           r.VOTABLE_EN_CONTRA,
                           r.VOTACION_CERRADA_MANUALMENTE,
                           r.TIPO_VOTACION
                    FROM ca
                             INNER JOIN GOC_REUNIONES_PUNTOS_ORDEN_DIA AS r
                    WHERE ca.id = r.ID_PUNTO_SUPERIOR
                   )
SELECT ID,
       TITULO,
       TITULO_ALT,
       DESCRIPCION,
       DESCRIPCION_ALT,
       ORDEN,
       REUNION_ID,
       ACUERDOS,
       ACUERDOS_ALT,
       DELIBERACIONES,
       DELIBERACIONES_ALT,
       PUBLICO,
       URL_ACTA,
       URL_ACTA_ALT,
       URL_ACTA_ANTERIOR,
       URL_ACTA_ANTERIOR_ALT,
       EDITADO_EN_REAPERTURA,
       ID_PUNTO_SUPERIOR,
       PROFUNDIDAD,
       ORDEN_NIVEL_ZERO,
       TIPO_VOTO,
       PROCEDIMIENTO_VOTACION,
       TIPO_RECUENTO_VOTO,
       VOTACION_ABIERTA,
       VOTABLE_EN_CONTRA,
       VOTACION_CERRADA_MANUALMENTE,
       TIPO_VOTACION
FROM ca
ORDER BY ORDEN_NIVEL_ZERO asc;

CREATE OR REPLACE FORCE VIEW GOC_VW_REUNIONES_EDITORES
(
    ID,
    ASUNTO,
    ASUNTO_ALT,
    FECHA,
    DURACION,
    NUM_DOCUMENTOS,
    EDITOR_ID,
    COMPLETADA,
    EXTERNO,
    ORGANO_ID,
    TIPO_ORGANO_ID,
    AVISO_PRIMERA_REUNION,
    AVISO_PRIMERA_REUNION_USER,
    AVISO_PRIMERA_REUNION_FECHA,
    URL_ACTA,
    URL_ACTA_ALT,
    VOTACION_TELEMATICA,
    TIPO_VISUALIZACION_RESULTADOS
)
AS
  SELECT
    r.id,
    r.asunto,
    r.asunto_alt,
    r.fecha,
    r.duracion,
    (SELECT COUNT(*)
     FROM goc_reuniones_documentos rd
     WHERE rd.reunion_id = r.id)
                 num_documentos,
    r.creador_id editor_id,
    r.completada completada,
    o.externo,
    o.organo_id,
    o.tipo_organo_id,
    r.aviso_primera_reunion,
    r.aviso_primera_reunion_user,
    r.aviso_primera_reunion_fecha,
    r.url_acta,
    r.url_acta_alt,
    r.votacion_telematica,
    r.TIPO_VISUALIZACION_RESULTADOS
FROM goc_reuniones r LEFT OUTER JOIN goc_organos_reuniones o on r.id = o.reunion_id
UNION
SELECT
    r.id,
    r.asunto,
    r.asunto_alt,
    r.fecha,
    r.duracion,
    (SELECT COUNT(*)
     FROM goc_reuniones_documentos rd
     WHERE rd.reunion_id = r.id)
                 num_documentos,
    a.persona_id editor_id,
    r.completada completada,
    o.externo,
    o.organo_id,
    o.tipo_organo_id,
    r.aviso_primera_reunion,
    r.aviso_primera_reunion_user,
    r.aviso_primera_reunion_fecha,
    r.url_acta,
    r.url_acta_alt,
    r.votacion_telematica,
    r.TIPO_VISUALIZACION_RESULTADOS
  FROM goc_reuniones r, goc_organos_reuniones o, goc_organos_autorizados a
  WHERE r.id = o.reunion_id AND o.organo_id = a.organo_id;

create view GOC_VW_CERTIFICADOS_ASISTENCIA as
SELECT ri.reunion_id,
       ri.persona_id,
       ri.url_asistencia,
       ri.url_asistencia_alt
FROM goc_reuniones_invitados ri
WHERE ri.url_asistencia IS NOT NULL
UNION
SELECT roi.reunion_id,
       roi.persona_id,
       roi.url_asistencia,
       roi.url_asistencia_alt
FROM goc_organos_reuniones_invits roi
WHERE roi.url_asistencia IS NOT NULL
UNION
SELECT r.id             reunion_id,
       orrm.suplente_id persona_id,
       orrm.url_asistencia,
       orrm.url_asistencia_alt
FROM goc_reuniones r,
     goc_organos_reuniones orr,
     goc_organos_reuniones_miembros orrm
WHERE r.id = orr.reunion_id
  AND orr.id = orrm.organo_reunion_id
  AND orrm.asistencia = 1
  AND suplente_id IS NOT NULL
  AND orrm.url_asistencia IS NOT NULL
UNION
SELECT r.id            reunion_id,
       orrm.miembro_id persona_id,
       orrm.url_asistencia,
       orrm.url_asistencia_alt
FROM goc_reuniones r,
     goc_organos_reuniones orr,
     goc_organos_reuniones_miembros orrm
WHERE r.id = orr.reunion_id
  AND orr.id = orrm.organo_reunion_id
  AND orrm.asistencia = 1
  AND suplente_id IS NULL
  AND orrm.url_asistencia IS NOT NULL;

CREATE OR REPLACE FORCE VIEW GOC_VW_REUNIONES_PERMISOS
  (
  	ID,
  	COMPLETADA, 
  	FECHA, 
  	ASUNTO, 
  	ASUNTO_ALT, 
  	PERSONA_ID, 
  	PERSONA_NOMBRE, 
  	ASISTENTE, 
  	URL_ACTA,
  	URL_ACTA_ALT, 
  	AVISO_PRIMERA_REUNION, 
  	URL_ASISTENCIA, 
  	URL_ASISTENCIA_ALT,
  	ENLACE_ACTA_PROVISIONAL_ACTIVO,
  	ORGANO_NOMBRE,
    ORGANO_NOMBRE_ALT,
    RESPONSABLE_VOTO_ID,
    RESPONSABLE_VOTO_NOM
  )
  AS
  SELECT
    s.id,
    s.completada,
    s.fecha,
    s.asunto,
    s.asunto_alt,
    s.persona_id,
    s.persona_nombre,
    SUM(s.asistente) asistente,
    s.url_acta,
    s.url_acta_alt,
    s.aviso_primera_reunion,
    (SELECT a.url_asistencia
     FROM goc_vw_certificados_asistencia a
     WHERE a.reunion_id = s.id
       AND a.persona_id = s.persona_id
       AND ROWNUM = 1)
                     url_asistencia,
    (SELECT a.url_asistencia_alt
     FROM goc_vw_certificados_asistencia a
     WHERE a.reunion_id = s.id
       AND a.persona_id = s.persona_id
       AND ROWNUM = 1)
                     url_asistencia_alt,
    s.enlace_acta_provisional_activo,
    s.organo_nombre,
    s.organo_nombre_alt,
    s.responsable_voto_id,
    s.responsable_voto_nom
FROM (SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.aviso_primera_reunion,
          r.creador_id     persona_id,
          r.creador_nombre persona_nombre,
          DECODE(suplente_id, NULL, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          op.acta_provisional_activa enlace_acta_provisional_activo,
          ore.ORGANO_NOMBRE organo_nombre,
          ore.ORGANO_NOMBRE_ALT organo_nombre_alt,
          r.responsable_voto_id,
          r.responsable_voto_nom
      FROM goc_reuniones r
               LEFT JOIN goc_organos_reuniones ore
                         ON r.id = ore.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON ore.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON ore.organo_id = op.id_organo AND ore.externo = op.ordinario
      UNION
      SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.aviso_primera_reunion,
          ra.persona_id     persona_id,
          ra.persona_nombre persona_nombre,
          DECODE(suplente_id, NULL, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          OP.acta_provisional_activa enlace_acta_provisional_activo,
          orr.ORGANO_NOMBRE,
          orr.ORGANO_NOMBRE_ALT,
          r.responsable_voto_id,
          r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               JOIN goc_organos_autorizados ra ON orr.organo_id = ra.organo_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      UNION
      SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.aviso_primera_reunion,
          ri.persona_id     persona_id,
          ri.persona_nombre persona_nombre,
          DECODE(orrm.suplente_id, NULL, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          op.acta_provisional_activa enlace_acta_provisional_activo,
          orr.ORGANO_NOMBRE,
          orr.ORGANO_NOMBRE_ALT,
          r.responsable_voto_id,
          r.responsable_voto_nom
      FROM goc_reuniones r
                JOIN goc_reuniones_invitados ri ON ri.reunion_id = r.id
                LEFT JOIN goc_organos_reuniones orr
                        ON r.id = orr.reunion_id
                LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
                LEFT JOIN goc_organos_parametros op
                        ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      UNION
      SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.aviso_primera_reunion,
          ori.persona_id                        persona_id,
          ori.nombre                            persona_nombre,
          decode(ori.solo_consulta, 0, 1, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          op.acta_provisional_activa enlace_acta_provisional_activo,
          orr.ORGANO_NOMBRE,
          orr.ORGANO_NOMBRE_ALT,
          r.responsable_voto_id,
          r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               JOIN goc_organos_reuniones_invits ori ON ori.organo_reunion_id = orr.id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      UNION
      SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.aviso_primera_reunion,
          orrm.suplente_id     persona_id,
          orrm.suplente_nombre persona_nombre,
          DECODE(orrm.suplente_id, NULL, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          op.acta_provisional_activa enlace_acta_provisional_activo,
          orr.ORGANO_NOMBRE,
          orr.ORGANO_NOMBRE_ALT,
          r.responsable_voto_id,
          r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      WHERE orrm.asistencia = 1
        AND suplente_id IS NOT NULL
      UNION
      SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.aviso_primera_reunion,
          cast(orrm.miembro_id as number(*)) persona_id,
          orrm.nombre                     persona_nombre,
          DECODE(orrm.suplente_id, NULL, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          op.acta_provisional_activa enlace_acta_provisional_activo,
          orr.ORGANO_NOMBRE,
          orr.ORGANO_NOMBRE_ALT,
          r.responsable_voto_id,
          r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      WHERE orrm.asistencia = 1
      UNION
      SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.aviso_primera_reunion,
          cast(orrm.miembro_id as number(*)) persona_id,
          orrm.nombre     persona_nombre,
          DECODE(orrm.suplente_id, NULL, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          op.acta_provisional_activa enlace_acta_provisional_activo,
          orr.ORGANO_NOMBRE,
          orr.ORGANO_NOMBRE_ALT,
          r.responsable_voto_id,
          r.responsable_voto_nom
      FROM goc_reuniones r
               JOIN goc_organos_reuniones orr ON r.id = orr.reunion_id
               LEFT JOIN goc_organos_reuniones_miembros orrm ON orr.id = orrm.organo_reunion_id
               LEFT JOIN goc_organos_parametros op
                         ON orr.organo_id = op.id_organo AND orr.externo = op.ordinario
      WHERE orrm.asistencia = 0) s
GROUP BY s.id,
         s.completada,
         s.fecha,
         s.asunto,
         s.asunto_alt,
         s.persona_id,
         s.persona_nombre,
         s.url_acta,
         s.url_acta_alt,
         s.aviso_primera_reunion,
         s.enlace_acta_provisional_activo,
         s.organo_nombre,
         s.organo_nombre_alt,
         s.responsable_voto_id,
         s.responsable_voto_nom;

CREATE OR REPLACE FORCE VIEW GOC_VW_VOTOS AS
SELECT
    ID,
    PUNTO_ORDEN_DIA_ID,
    PERSONA_ID,
    ORGANO_REUNION_MIEMBRO_ID,
    VOTO,
    VOTO_DOBLE,
    1 as VOTO_PUBLICO
FROM GOC_PERSONA_PUNTO_VOTO VOTOS
UNION
SELECT
    ID,
    PUNTO_ORDEN_DIA_ID,
    PERSONA_ID,
    ORGANO_REUNION_MIEMBRO_ID ,
    NULL as VOTO,
    VOTO_DOBLE,
    0 as VOTO_PUBLICO
FROM GOC_VOTANTES_PRIVADOS VOTOS;

alter sequence hibernate_sequence restart with 500;
