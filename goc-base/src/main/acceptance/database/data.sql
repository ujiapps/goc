---------------------------------------------------- CARGOS ----------------------------------------------------

INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (1, 'Rector', 'Rector', 'RE', 0);
INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (2, 'Vocal', 'Vocal', 'VO', 0);
INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (3, 'Secretario', 'Secretari', 'SE', 1);
INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (4, 'Gerente', 'Gerent', 'GE', 0);
INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (5, 'Presidente', 'President', 'PR', 0);


---------------------------------------------------- REUNIONES ----------------------------------------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA, CREADOR_NOMBRE,
                           CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA,
                           ASUNTO_ALT, DESCRIPCION_ALT, ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT,
                           AVISO_PRIMERA_REUNION_USER, AVISO_PRIMERA_REUNION_FECHA, AVISO_PRIMERA_REUNION_EMAIL,
                           REABIERTA, EMAILS_NUEVOS_COMENTARIOS)
VALUES (1, 'Dirección del centro completada', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 1,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        TO_DATE('2020-04-28 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 1, null, 0, null, 0,
        '4tic', 'test@test.com', 1, 1, 1, null, null, null, null, null, null, 0, 1, null, null, null, null,
        null, 1, 0);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA, EMAILS_NUEVOS_COMENTARIOS)
VALUES (2, 'Mi reunión', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 1,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 0, null, 0,
        '4tic', 'test@test.com', 1, 0, 1, null, null, null, null, null, null, 0, 1, null, null, null, null, null, 0, 0);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA, EMAILS_NUEVOS_COMENTARIOS)
VALUES (3, 'Reunión abierta', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 1,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 0, null, 0,
        '4tic', 'test@test.com', 1, 0, 1, null, null, null, null, null, null, 0, 1, null, null, null, null,
        null, 0, 0);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA, EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (4, 'Reunión votación', TO_DATE('2020-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 88849,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 1, null, 0,
        '4tic', 'test@test.com', 1, 0, 1, null, null, null, null, null, null, 1, 1, null, null, null,
        TO_DATE('2020-04-26 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO, TIPO_VISUALIZACION_RESULTADOS)
VALUES (5, 'Reunión no telematica', TO_DATE('2058-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 0, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 0, 0, 0);

---------------------------------------------------- TIPOS ORGANOS ----------------------------------------------------
INSERT INTO GOC_TIPOS_ORGANO (ID, CODIGO, NOMBRE, NOMBRE_ALT)
VALUES (1, 'OC', 'Órgano colegiado', null);
INSERT INTO GOC_TIPOS_ORGANO (ID, CODIGO, NOMBRE, NOMBRE_ALT)
VALUES (2, 'OE', 'Órgano externo', null);

---------------------------------------------------- ORGANOS ----------------------------------------------------
INSERT INTO GOC_ORGANOS (ID, NOMBRE, TIPO_ORGANO_ID, CREADOR_ID, FECHA_CREACION, INACTIVO, NOMBRE_ALT)
VALUES (1, 'Junta de gobierno', 1, 1, TO_DATE('2016-04-18 09:36:34', 'YYYY-MM-DD HH24:MI:SS'), 0,
        'Junta de govern');

INSERT INTO GOC_ORGANOS (ID, NOMBRE, TIPO_ORGANO_ID, CREADOR_ID, FECHA_CREACION, INACTIVO, NOMBRE_ALT)
VALUES (2, 'Dirección general', 2, 1, TO_DATE('2016-04-18 09:36:34', 'YYYY-MM-DD HH24:MI:SS'), 0,
        'Direcció general');
INSERT INTO GOC_ORGANOS (ID, NOMBRE, TIPO_ORGANO_ID, CREADOR_ID, FECHA_CREACION, INACTIVO, NOMBRE_ALT)
VALUES (3, 'Delegación de voto', 1, 1, TO_DATE('2016-04-18 09:36:34', 'YYYY-MM-DD HH24:MI:SS'), 0,
        'Delegació de vot');

---------------------------------------------------- ORGANOS ----------------------------------------------------

INSERT INTO GOC_ORGANOS_PARAMETROS (ID_ORGANO, ORDINARIO, CONVOCAR_SIN_ORDEN_DIA, EMAIL, ACTA_PROVISIONAL_ACTIVA, DELEGACION_VOTO_MULTIPLE, TIPO_PROCEDIMIENTO_VOTACION,
                                    PERMITE_ABSTENCION_VOTO, PRESIDENTE_VOTO_DOBLE)
VALUES (1, 0, 1, NULL, 1, 0, 'ABREVIADO', 1, 1);

INSERT INTO GOC_ORGANOS_PARAMETROS (ID_ORGANO, ORDINARIO, CONVOCAR_SIN_ORDEN_DIA, EMAIL, ACTA_PROVISIONAL_ACTIVA, DELEGACION_VOTO_MULTIPLE, TIPO_PROCEDIMIENTO_VOTACION,
                                    PERMITE_ABSTENCION_VOTO, PRESIDENTE_VOTO_DOBLE)
VALUES (2, 0, 1, 'emailorgano@test.com', 1, 0, 'ORDINARIO', 1, 0);

INSERT INTO GOC_ORGANOS_PARAMETROS (ID_ORGANO, ORDINARIO, CONVOCAR_SIN_ORDEN_DIA, EMAIL, ACTA_PROVISIONAL_ACTIVA, DELEGACION_VOTO_MULTIPLE, TIPO_PROCEDIMIENTO_VOTACION,
                                    PERMITE_ABSTENCION_VOTO, PRESIDENTE_VOTO_DOBLE)
VALUES (3, 0, 1, 'emailorgano@test.com', 1, 0, 'ABREVIADO', 1, 0);

---------------------------------------------------- AUTORIZADOS ----------------------------------------------------
INSERT INTO GOC_ORGANOS_AUTORIZADOS (ID, PERSONA_ID, PERSONA_NOMBRE, ORGANO_ID, ORGANO_EXTERNO, PERSONA_EMAIL)
VALUES (1, 2, 'Autorizado', 1, 0, 'autorizado@test.com');

INSERT INTO GOC_ORGANOS_AUTORIZADOS (ID, PERSONA_ID, PERSONA_NOMBRE, ORGANO_ID, ORGANO_EXTERNO, PERSONA_EMAIL)
VALUES (2, 2, 'Autorizado', 2, 0, 'autorizado@test.com');

---------------------------------------------------- ORGANOS REUNIONES ----------------------------------------------------

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (1, 1, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (2, 2, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (3, 3, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (4, 4, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

---------------------------------------------------- MIEMBROS ----------------------------------------------------

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID, PERSONA_ID, NATO)
VALUES (1, 'Test', 'test@test.com', 1, 3, 1, 1);

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID, PERSONA_ID, NATO)
VALUES (2, 'Secretario', 'secretario@test.com', 1, 3, 2, 1);

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID, PERSONA_ID, NATO)
VALUES (3, 'Rector', 'rector@test.com', 1, 3, 3, 1);

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID, PERSONA_ID, NATO)
VALUES (4, 'Suplente test', 'suplente@test.com', 1, 3, 4, 1);

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID, PERSONA_ID, NATO)
VALUES (5, 'Delegado test', 'delegado@test.com', 1, 3, 5, 1);

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID, PERSONA_ID, NATO)
VALUES (6, 'Presidente', 'presidente@test.com', 1, 5, 6, 1);

---------------------------------------------------- REUNION MIEMBROS ----------------------------------------------------

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE, ASISTENCIA_CONFIRMADA,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            MOTIVO_AUSENCIA)
VALUES (1, 2, 0, 2, '1', '4tic', 'test@test.com', 1, '3', 'Secretario', null, null, 1,
        null, 1, 1, null, null, null, 'SE', null, null, null, null, null, 0, null);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE, ASISTENCIA_CONFIRMADA,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            MOTIVO_AUSENCIA)
VALUES (2, 3, 0, 3, '1', '4tic', 'test@test.com', 1, '3', 'Secretario', null, null, 1,
        null, 1, 1, null, null, null, 'SE', null, null, null, null, null, 0, null);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE, ASISTENCIA_CONFIRMADA,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            MOTIVO_AUSENCIA)
VALUES (3, 4, 0, 4, '1', '4tic', 'test@test.com', 1, '3', 'SE', 'Secretario', null, null, 1,
        null, 1, null, null, null, null, null, null, null, null, null, 0, null);

---------------------------------------------------- PUNTOS ORDEN DÃA ----------------------------------------------------

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, PROCEDIMIENTO_VOTACION,
                                            ACUERDOS, DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, VOTACION_CERRADA_MANUALMENTE)
VALUES (1, 'Punto orden día ficheros', null, 20, 2, 'ORDINARIO', null, null, 0, null, null, null, null, null, null, null,
        null, null, 0, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, PROCEDIMIENTO_VOTACION,
                                            ACUERDOS, DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, VOTACION_CERRADA_MANUALMENTE)
VALUES (2, 'Punto orden día', null, 1, 3, 'ORDINARIO' ,null, null, 0, null, null, null, null, null, null, null,
        null, null, 0, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, PROCEDIMIENTO_VOTACION,
                                            ACUERDOS, DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, VOTACION_CERRADA_MANUALMENTE)
VALUES (3, 'Punto orden día votación privada', null, 1, 4, 'ORDINARIO', null, null, 0, null, null, null, null, null, null, null,
        null, null, 0, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, PROCEDIMIENTO_VOTACION,
                                            ACUERDOS, DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, VOTACION_CERRADA_MANUALMENTE)
VALUES (4, 'Punto orden día votación pública', null, 1, 4, 'ORDINARIO', null, null, 1, null, null, null, null, null, null, null,
        null, null, 0, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_CERRADA_MANUALMENTE)
VALUES (5, 'Un punto de orden del dia cualquiera',1, 5, 0, 0, null,
        null, null, TO_DATE('2058-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0);