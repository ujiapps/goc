-- Cambio del tipo de votación en la reunión

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (1000, 'Cambio del tipo de votación en la reunión', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, TITULO_ALT, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (1001, 'POD con votación secreta sin votos será abierta', 'POD con votación secreta sin votos será abierta', 1, 1000, 0, 0, 0, 'ORDINARIO', 'MAYORIA_SIMPLE');

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, TITULO_ALT, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (1002, 'POD con votación secreta sin votos no necesitará votación', 'POD con votación secreta sin votos no necesitará votación', 2, 1000, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE');

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, TITULO_ALT, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (1003, 'POD con votación abierta con votos', 'POD con votación abierta con votos', 3, 1000, 0, 0, 1, 'ORDINARIO', 'MAYORIA_SIMPLE');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (1002, 2, 0, 1000, '1', 'un votante', 'unvotante@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_PERSONA_PUNTO_VOTO (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID, VOTO)
VALUES ('123e4567-e89b-12d3-a456-426655440000', 1005, 1002, 1003, 'FAVOR');

-- Añadir un nuevo POD a una reunón convocada

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (2000, 'Añadir un nuevo POD a una reunón convocada', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (2001, 2000, 'Órgano votación', 1, 0, '1', 'Órgano votación');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (2002, 2001, 0, 2000, '1', 'un votante', 'unvotante@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

-- Resultados de la votación

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO, TIPO_VISUALIZACION_RESULTADOS)
VALUES (3000, 'Resultados de la votación', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (3001, 'Votación secreta', 1, 3000, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE');

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (3002, 'Votación abierta', 1, 3000, 0, 0, 1, 'ABREVIADO', 'MAYORIA_SIMPLE');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (3010, 3000, 'Órgano votación', 1, 0, '1', 'Órgano votación');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (3002, 3010, 0, 3000, '1', 'un votante', 'unvotante@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (3004, 3010, 0, 3000, '1', 'otro votante', 'otrovotante@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (3009, 3010, 0, 3000, '1', 'otro mas', 'otromas@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_VOTANTES_PRIVADOS (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID)
VALUES ('123e4567-e89b-12d3-a456-426655440001', 3005, 3002, 3001);

INSERT INTO GOC_PUNTOS_VOTOS_PRIVADOS (ID, PUNTO_ORDEN_DIA_ID, VOTO)
VALUES ('123e4567-e89b-12d3-a456-426655440002', 3001, 'FAVOR');

INSERT INTO GOC_VOTANTES_PRIVADOS (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID)
VALUES ('123e4567-e89b-12d3-a456-426655440003', 3006, 3004, 3001);

INSERT INTO GOC_PUNTOS_VOTOS_PRIVADOS (ID, PUNTO_ORDEN_DIA_ID, VOTO)
VALUES ('123e4567-e89b-12d3-a456-426655440004', 3001, 'CONTRA');




INSERT INTO GOC_PERSONA_PUNTO_VOTO (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID, VOTO)
VALUES ('123e4567-e89b-12d3-a456-426655440005', 3007, 3002, 3002, 'FAVOR');

INSERT INTO GOC_PERSONA_PUNTO_VOTO (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID, VOTO)
VALUES ('123e4567-e89b-12d3-a456-426655440007', 3008, 3004, 3002, 'CONTRA');



INSERT INTO GOC_PERSONA_PUNTO_VOTO (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID, VOTO)
VALUES ('123e4567-e89b-12d3-a456-426655440008', 3010, 3009, 3002, 'BLANCO');

--------------------------------------- Limitación de votos ---------------------------------------

--  ************ Reunion que admite cambio voto ************
INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (4000, 'Limitación de votos (Admite cambio voto)', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID, ORGANO_NOMBRE_ALT)
VALUES (4001, 4000, 'órgano test', 1, 0, '1', 'órgano test');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE, ASISTENCIA_CONFIRMADA,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            MOTIVO_AUSENCIA)
VALUES (4001, 4001, 0, 4000, '1', '4tic', 'test@test.com', 1, '3', 'SE', 'Jefe', null, null, 1,
        null, 1, null, null, null, null, null, null, null, null, null, 0, null);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTABLE_EN_CONTRA, VOTACION_ABIERTA)
VALUES (4001, 'Si la votación es abierta, un usuario no puede votar dos veces', 1, 4000, 0, 0, 1, 'ABREVIADO', 'MAYORIA_SIMPLE',
        TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE, TRUE);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTABLE_EN_CONTRA, VOTACION_ABIERTA)
VALUES (4002, 'Si la votación es secreta, un usuario no puede votar dos veces', 2, 4000, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE',
        TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE, TRUE);

-------------------------------------------------------------------------------------------------------

--------------------------------------- Cambio de voto emitido ---------------------------------------
-- ************ Reunion que admite cambio voto ************

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (5001, 'Cambio de voto emitido (admite cambio voto)', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID, ORGANO_NOMBRE_ALT)
VALUES (5001, 5001, 'órgano test', 1, 0, '1', 'órgano test');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE, ASISTENCIA_CONFIRMADA,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            MOTIVO_AUSENCIA)
VALUES (5001, 5001, 0, 5001, '1', '4tic', 'test@test.com', 1, '3', 'SE', 'Jefe', null, null, 1,
        null, 1, null, null, null, null, null, null, null, null, null, 0, null);


INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTABLE_EN_CONTRA, VOTACION_ABIERTA)
VALUES (5001, 'Si la votación secreta no se permite cambiar de voto', 1, 5001, 0, 0, 1, 'ABREVIADO', 'MAYORIA_SIMPLE',
        TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE, TRUE);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTABLE_EN_CONTRA, VOTACION_ABIERTA)
VALUES (5002, 'Si la reunión NO permite el cambio de voto y la votación es abierta el usuario no ha de poder cambiar de voto',
        2, 5001, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE',TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE, TRUE);

-- ************ Reunion que NO admite cambio voto ************
INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (5002, 'Cambio de voto emitido (No admite cambio voto)', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 0);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID, ORGANO_NOMBRE_ALT)
VALUES (5002, 5002, 'órgano test', 1, 0, '1', 'órgano test');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE, ASISTENCIA_CONFIRMADA,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            MOTIVO_AUSENCIA)
VALUES (5002, 5002, 0, 5002, '1', '4tic', 'test@test.com', 1, '3', 'SE', 'Jefe', null, null, 1,
        null, 1, null, null, null, null, null, null, null, null, null, 0, null);


INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTABLE_EN_CONTRA, VOTACION_ABIERTA)
VALUES (5003, 'Si la reunión permite el cambio de voto y la votación es abierta, el usuario ha de poder cambiar de voto',
        1, 5002, 0, 0, 1, 'ABREVIADO', 'MAYORIA_SIMPLE',TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE, TRUE);

-------------------------------------------------------------------------------------------------------

--------------------------------------- Miembro autorizado no puede votar ---------------------------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (6000, 'Miembro autorizado no puede votar', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_ABIERTA)
VALUES (6001, 'El autorizado no ha de poder votar ningún punto de una votación abierta. Mostrar error controlado',
        1, 6000, 0, 0, 1, 'ABREVIADO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_ABIERTA)
VALUES (6002, 'El autorizado no ha de poder votar ningún punto de una votación secreta. Mostrar error controlado',
        1, 6000, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (6001, 6000, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

-------------------------------------------------------------------------------------------------------

------------------------------------- Cierre/apertura votación ----------------------------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (7000, 'Cierre/apertura votación', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (7001, 7000, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (7001, 7001, 0, 7000, '1', 'secretario', 'secretario@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (7002, 7001, 0, 7000, '1', 'otro votante', 'otrovotante@test.com', 1, '1', 'RE', 'Rector', 1, 3, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (7001, 'POD con votación ordinaria cerrada', 1, 7000, 0, 0, 0, 'ORDINARIO', 'MAYORIA_SIMPLE');

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_ABIERTA,
                                            FECHA_INICIO_VOTACION)
VALUES (7002, 'POD con votación ordinaria abierta', 2, 7000, 0, 0, 0, 'ORDINARIO', 'MAYORIA_SIMPLE',
        1, TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (7003, 'POD con votación abreviada cerrada', 3, 7000, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE');

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_ABIERTA,
                                            FECHA_INICIO_VOTACION)
VALUES (7004, 'POD con votación abreviada abierta', 4, 7000, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE',
        1, TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

-------------------------------------------------------------------------------------------------------

----------------------- Control de emisión de votos cuando la reunión está cerrada --------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (8000, 'Si la votación es abreviada y la reunión no está convocada, no se ha de poder votar',
        TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,1, NOW(), 1, 1, 0, '4tic', 'test@test.com', 1, 0, 1,
        0, 1, null, 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION)
VALUES (8001, 'Si la votación es abreviada y la reunión no está convocada, no se ha de poder votar', 1, 8000, 0, 0, 0,
        'ABREVIADO', 'MAYORIA_SIMPLE', null);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (8001, 'Si la votación es abreviada y la reunión está convocada, se ha de poder votar',
        TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1,
        1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION)
VALUES (8002, 'Si la votación es abreviada y la reunión no está convocada, no se ha de poder votar', 1, 8001, 0, 0, 0,
        'ABREVIADO', 'MAYORIA_SIMPLE', NOW());

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (8002, 'Si la reunión está cerrada, no se ha de poder votar',
        TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,1, NOW(), 1, 1, 0, '4tic', 'test@test.com', 1, 1, 1,
        0, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (8003, 'Si la reunión está cerrada, no se ha de poder votar', 1, 8002, 0, 0, 0,
        'ABREVIADO', 'MAYORIA_SIMPLE');

-------------------------------------------------------------------------------------------------------

------------------------------------- Cierre de votación ----------------------------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (9000, 'Cierre votación', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID, ORGANO_NOMBRE_ALT)
VALUES (9001, 9000, 'órgano test', 1, 0, '1', 'órgano test');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (9001, 9001, 0, 9000, '1', 'secretario', 'secretario@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_ABIERTA,
                                            FECHA_INICIO_VOTACION)
VALUES (9002, 'POD con votación ordinaria abierta', 1, 9000, 0, 0, 0, 'ORDINARIO', 'MAYORIA_SIMPLE',
        1, TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_ABIERTA,
                                            FECHA_INICIO_VOTACION)
VALUES (9003, 'POD con votación abreviada abierta', 2, 9000, 0, 0, 0, 'ABREVIADA', 'MAYORIA_SIMPLE',
        1, TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

-------------------------------------------------------------------------------------------------------

--------------------------------------- Emision de votos ---------------------------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (10000, 'Los puntos no votables no se pueden votar', TO_DATE('2057-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_ABIERTA)
VALUES (10001, 'Si la reunión permite el cambio de voto y la votación es abierta, el usuario ha de poder cambiar de voto',
        1, 10000, 0, 0, null, 'ABREVIADO', 'MAYORIA_SIMPLE',TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID, ORGANO_NOMBRE_ALT)
VALUES (10002, 10000, 'órgano test', 1, 0, '1', 'órgano test');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (10003, 10002, 0, 10000, '1', 'secretario', 'test@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTABLE_EN_CONTRA, VOTACION_ABIERTA)
VALUES (10004, 'Punto abierto publico.',
        1, 10000, 0, 0, 1, 'ABREVIADO', 'MAYORIA_SIMPLE',TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE, TRUE);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_ABIERTA)
VALUES (10005, 'Punto abierto secreto.',
        1, 10000, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE',TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE);
-------------------------------------------------------------------------------------------------------

--------------------------------------- Delegación de voto --------------------------------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (11000, 'No permite delegación de voto', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 0, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (11001, 11000, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11001, 11001, 0, 11000, '1', 'secretario', 'secretario@test.com', 0, '3', 'SE', 'Secretario', 0, 1, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11002, 11001, 0, 11000, '1', 'otro votante', 'otrovotante@test.com', 1, '1', 'RE', 'Rector', 1, 3, 0);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (11003, 'Permite delegación de voto', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_ABIERTA,
                                            FECHA_INICIO_VOTACION)
VALUES (11004, 'POD con votación abreviada abierta', 1, 11000, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE',
        1, TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

-- Reunión que permite delegación de voto

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (11004, 11003, 'Delegación de voto', 1, 0, '3', 'Delegació de vot');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11005, 11004, 0, 11003, '3', 'secretario', 'secretario@test.com', 0, '3', 'SE', 'Secretario', 0, 1, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11006, 11004, 0, 11003, '3', 'votante asiste', 'otroasiste@test.com', 1, '4', 'GE', 'Gerente', 1, 2, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA, DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL)
VALUES (11007, 11004, 0, 11003, '3', 'voto delegado', 'votodelegado@test.com', 0, '1', 'RE', 'Rector', 0, 3, 0,
        2, 'votante asistente', 'otroasiste@test.com');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11008, 11004, 0, 11003, '3', 'votante asiste extra', 'otroasisteextra@test.com', 1, '4', 'GE', 'Gerente', 1, 4, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_ABIERTA,
                                            FECHA_INICIO_VOTACION)
VALUES (11007, 'POD con votación abreviada abierta', 1, 11003, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE',
        1, TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

-- No se ha de podel delegar en caso de haber votado
-- No se ha de poder eliminar la delegación de voto en caso de haber votado el delegado

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (11004, 'No permite delegar/eliminar delegación si ya se ha votado', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO)
VALUES (11008, 'POD con votación', 1, 11004, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (11005, 11004, 'Órgano votación', 1, 0, '1', 'Órgano votación');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11009, 11005, 0, 11004, '1', 'un votante', 'unvotante@test.com', 0, '3', 'SE', 'Secretario', 0, 1, 0);


INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11010, 11005, 0, 11004, '1', 'delegado', 'delegado@test.com', 1, '3', 'SE', 'Secretario', 1, 4, 0);

-- Se ha de poder delegar en caso de no asistir en un asistente
-- Si se ha delegado el voto, la persona que lo ha delegado no ha de poder votar

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA, delegado_voto_id, delegado_voto_email, delegado_voto_nombre)
VALUES (11011, 11005, 0, 11004, '1', 'miembro con delegado', 'test@test.com', 1, '3', 'SE', 'Secretario', 1, 5, 0,
        1, 'delegado@test.com', 'delegado');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11012, 11005, 0, 11004, '1', 'no asistente', 'noasistente@test.com', 0, '3', 'SE', 'Secretario', 0, 6, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11013, 11005, 0, 11004, '1', 'asistente', 'asistente@test.com', 1, '3', 'SE', 'Secretario', 1, 7, 0);

-- En los resultados de la votación si has votado como delegado debe indicar en representación de quién votas

INSERT INTO GOC_VOTANTES_PRIVADOS (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID)
VALUES ('133e4567-e89f-12d3-a456-426655440001', 1, 11009, 11008);

INSERT INTO GOC_VOTANTES_PRIVADOS (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID)
VALUES ('133e4567-e89f-12d3-a456-426655440002', 4, 11011, 11008);


-- ********************************************************************************************************************

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (11030, 'Se ha de poder votar por el miembro representado',
        TO_DATE('2057-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1,
        1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_ABIERTA)
VALUES (11031, 'Se ha de poder votar por el miembro representado',1, 11030, 0, 0, 1,
        'ABREVIADO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_ABIERTA)
VALUES (11032, 'Se ha de poder votar por el miembro representado',1, 11030, 0, 0, 0,
        'ABREVIADO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE);


INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID, ORGANO_NOMBRE_ALT)
VALUES (11030, 11030, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (11030, 11030, 0, 11030, '1', 'Miembro sin delegados', 'sindelegados@test.com', 1, '3', 'SE', 'Secretario', 1, 1,
        0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID, RESPONSABLE_ACTA)
VALUES (11031, 11030, 0, 11030, '1', 'Miembro asistente al que han delegado voto', 'test@test.com', 1, '3', 'SE',
        'Secretario', 1, 5, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID, RESPONSABLE_ACTA, DELEGADO_VOTO_ID,
                                            DELEGADO_VOTO_EMAIL, DELEGADO_VOTO_NOMBRE)
VALUES (11032, 11030, 0, 11030, '1', 'Miembro con delegado (id delegado: 5)', 'rector@test.com', 1, '3', 'SE',
        'Secretario', 1, 3, 0, 5, 'delegado@test.com', 'delegado test');

-- ********************************************************************************************************************

-------------------------------------------------------------------------------------------------------
--------------------------------------- Suplencia votación ---------------------------------------

-- ************
-- Los usuarios que indiquen que no irán e indiquen un suplente, tienen que seguir teniendo acceso a la reunión
-- ************

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (12000, 'Los usuarios que indiquen que no irán e indiquen un suplente, tienen que seguir teniendo acceso a la reunión',
        TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1,
        1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION)
VALUES (12001, 'Los usuarios que indiquen que no irán e indiquen un suplente, tienen que seguir teniendo acceso a la reunión',1, 12000, 0, 0, 1,
        'ABREVIADO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (12000, 12000, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID, RESPONSABLE_ACTA, SUPLENTE_ID,
                                            SUPLENTE_NOMBRE, SUPLENTE_EMAIL)
VALUES (12000, 12000, 0, 12000, '1', 'Test', 'test@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0, 3,
        'Suplente test', 'suplente@test.com');

-- ************
-- Si un titular no va y selecciona un suplente, el titular no podrá votar
-- ************

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (12001, 'Si un titular tiene suplente, el titular no podrá votar',
        TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1,
        1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_ABIERTA)
VALUES (12002, 'Si un titular tiene suplente, el titular no podrá votar',1, 12001, 0, 0, 1,
        'ABREVIADO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (12001, 12001, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID, RESPONSABLE_ACTA, SUPLENTE_ID,
                                            SUPLENTE_NOMBRE, SUPLENTE_EMAIL)
VALUES (12002, 12001, 0, 12001, '1', 'Test', 'test@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0, 3,
        'Suplente test', 'suplente@test.com');

-- ************
-- Si un titular no va y selecciona un suplente, el suplente podrá votar en su nombre
-- ************

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (12002, 'Si un titular tiene suplente, el suplente podrá votar en su nombre',
        TO_DATE('2057-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1,
        1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTABLE_EN_CONTRA, VOTACION_ABIERTA)
VALUES (12003, 'Si un titular tiene suplente, el suplente podrá votar en su nombre', 1, 12002, 0, 0, 1,
        'ABREVIADO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE, TRUE);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (12002, 12002, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID, RESPONSABLE_ACTA, SUPLENTE_ID,
                                            SUPLENTE_NOMBRE, SUPLENTE_EMAIL)

VALUES (12004, 12002, 0, 12002, '1', 'Test', 'test@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0, 12000,
        'Suplente test', 'suplente@test.com');

-- ***********************
-- Si un titular ha votado no puede elegir un suplente
-- ***********************

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO, TIPO_VISUALIZACION_RESULTADOS)
VALUES (12003, 'Reunión con suplentes y votacion del titular', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_ABIERTA)
VALUES (12004, 'Votación del titular con suplentes', 1, 12003, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE', TRUE);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (12005, 12003, 'Órgano votación', 1, 0, '1', 'Órgano votación');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (12005, 12005, 0, 12003, '1', 'un votante', 'unvotante@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (12006, 12005, 0, 12003, '1', 'otro votante', 'otrovotante@test.com', 1, '3', 'SE', 'Secretario', 1, 2, 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA, suplente_id, suplente_email, suplente_nombre)
VALUES (12007, 12005, 0, 12003, '1', 'miembro con suplente', 'consuplente@test.com', 1, '3', 'SE', 'Secretario', 1, 3, 0,
        10, 'suplente@test.com', 'suplente del miembro');

INSERT INTO GOC_VOTANTES_PRIVADOS (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID)
VALUES ('123e4567-e89f-12d3-a456-426655440001', 1, 12005, 12004);

INSERT INTO GOC_VOTANTES_PRIVADOS (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID)
VALUES ('123e4567-e89f-12d3-a456-426655440002', 10, 12007, 12004);


-- ***********************
-- La fecha de finalizacion de la votacion anterior a la actual impide que se pueda votar.
-- ***********************

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO, FECHA_FIN_VOTACION)
VALUES (13002, 'Reunion con fecha fin anterior',
        TO_DATE('2059-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1,
        1, 1, NOW(), 0, 0, 1, 1, TO_DATE('2012-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION, VOTACION_ABIERTA)
VALUES (13003, 'Abreviado', 1, 13002, 0, 0, 1,
        'ABREVIADO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), TRUE);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION)
VALUES (13004, 'Ordinario', 1, 13002, 0, 0, 1,
        'ORDINARIO', 'MAYORIA_SIMPLE', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (13005, 13002, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');


INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (13006, 13005, 0, 13002, '1', 'secretario', 'secretario@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);


-------------------------------------------------------------------------------------------------------

--------------------------------------- Tipo Procedimiento votación ---------------------------------------


INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (13000, 'Reunión sin organo', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (13001, 'Reunión con organo (Ordinario)', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (13002, 13001, 'Dirección general', 1, 0, '2', 'Dirección general');


INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (13003, 'Reunión con organo (Abreviado)', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (13004, 13003, 'Junta de gobierno', 1, 0, '1', 'Junta de gobierno');

-------------------------------------------------------------------------------------------------------

--------------------------------------- Reunion no telematica ---------------------------------------

-- ***********************
-- Si existe una reunion sin reunion telematica y con un punto de orden del dia se debe poder cambiar ese campo.
-- ***********************


INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO, TIPO_VISUALIZACION_RESULTADOS)
VALUES (14000, 'Reunión no telematica', TO_DATE('2058-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 0, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 0, 0, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, FECHA_INICIO_VOTACION)
VALUES (14001, 'Un punto de orden del dia cualquiera',1, 14000, 0, 0, null,
        null, null, TO_DATE('2058-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'));



-- ***********************
-- Si en una reunion hay un organo sin acta provisional activada no se deberia mostrar la opcion de verla
-- ***********************

INSERT INTO GOC_ORGANOS (ID, NOMBRE, TIPO_ORGANO_ID, CREADOR_ID, FECHA_CREACION, INACTIVO, NOMBRE_ALT)
VALUES (15001, 'Organo Sin Acta Provisional', 1, 1, TO_DATE('2016-04-18 09:36:34', 'YYYY-MM-DD HH24:MI:SS'), 0,
        'Junta de govern');

INSERT INTO GOC_ORGANOS_PARAMETROS (ID_ORGANO, ORDINARIO, CONVOCAR_SIN_ORDEN_DIA, EMAIL, ACTA_PROVISIONAL_ACTIVA, DELEGACION_VOTO_MULTIPLE, TIPO_PROCEDIMIENTO_VOTACION,
                                    PERMITE_ABSTENCION_VOTO, PRESIDENTE_VOTO_DOBLE)
VALUES (15001, 0, 1, 'emailorgano@test.com', 0, 0, 'ABREVIADO',1 , 0);


INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO, VER_RESULTADOS_MIENTRAS_ACTIVA)
VALUES (15002, 'Reunión Sin Acta Provisional', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (15005, 15002, 'Organo Sin Acta Provisional', 1, 0, '15001', 'Órgano votación');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA, suplente_id, suplente_email, suplente_nombre)
VALUES (15003, 15005, 0, 15002, '15001', 'miembro con suplente', 'consuplente@test.com', 1, '3', 'SE', 'Secretario', 1, 3, 0,
        10, 'suplente@test.com', 'suplente del miembro');


-------------------------------------------------------------------------------------------------------

--------------------------------------- Procedimiento votación ---------------------------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO)
VALUES (16001, 'Cambio del tipo procedimiento en la reunión', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, TITULO_ALT, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_CERRADA_MANUALMENTE)
VALUES (16002, 'POD sin votos', 'POD sin votos', 2, 16001, 0, 0, 0, 'ABREVIADO', 'MAYORIA_SIMPLE', 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, TITULO_ALT, ORDEN, REUNION_ID, PUBLICO, EDITADO_EN_REAPERTURA, TIPO_VOTO,
                                            PROCEDIMIENTO_VOTACION, TIPO_RECUENTO_VOTO, VOTACION_CERRADA_MANUALMENTE)
VALUES (16003, 'POD con votos', 'POD con votación abierta con votos', 3, 16001, 0, 0, 1, 'ABREVIADO', 'MAYORIA_SIMPLE', 0);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (16002, 2, 0, 16001, '1', 'un votante', 'unvotante@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);

INSERT INTO GOC_PERSONA_PUNTO_VOTO (ID, PERSONA_ID, ORGANO_REUNION_MIEMBRO_ID, PUNTO_ORDEN_DIA_ID, VOTO, VOTACION_CERRADA_MANUALMENTE)
VALUES ('123e4567-e89b-12d3-a456-426637440005', 1005, 16002, 16003, 'FAVOR', 0);