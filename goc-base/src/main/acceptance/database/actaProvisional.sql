-------------------------------------------------------------------------------------------------------
-- ***********************
--  Si todos los organos de la reunion tienen la opcion de acta provisional activada se deberia mostrar la opcion al verla.
-- ***********************

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO, TIPO_VISUALIZACION_RESULTADOS)
VALUES (22003, 'Reunion con todos los organos con el acta provisional activa', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (22005, 22003, 'Órgano votación', 1, 0, '1', 'Órgano votación');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA)
VALUES (22005, 22005, 0, 22003, '1', 'un votante', 'unvotante@test.com', 1, '3', 'SE', 'Secretario', 1, 1, 0);


-------------------------------------------------------------------------------------------------------
-- ***********************
-- Si en una reunion hay un organo sin acta provisional activada no se deberia mostrar la opcion de verla
-- ***********************

INSERT INTO GOC_ORGANOS (ID, NOMBRE, TIPO_ORGANO_ID, CREADOR_ID, FECHA_CREACION, INACTIVO, NOMBRE_ALT)
VALUES (24001, 'Organo Sin Acta Provisional', 1, 1, TO_DATE('2016-04-18 09:36:34', 'YYYY-MM-DD HH24:MI:SS'), 0,
        'Junta de govern');

INSERT INTO GOC_ORGANOS_PARAMETROS (ID_ORGANO, ORDINARIO, CONVOCAR_SIN_ORDEN_DIA, EMAIL, ACTA_PROVISIONAL_ACTIVA, DELEGACION_VOTO_MULTIPLE, TIPO_PROCEDIMIENTO_VOTACION,
                         PERMITE_ABSTENCION_VOTO, PRESIDENTE_VOTO_DOBLE)
VALUES (24001, 0, 1, 'emailorgano@test.com', 0, 0, 'ABREVIADO',1 , 0);


INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, CREADOR_ID, FECHA_CREACION, PUBLICA, TELEMATICA, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA, ADMITE_COMENTARIOS,
                           AVISO_PRIMERA_REUNION, ADMITE_DELEGACION_VOTO, AVISO_PRIMERA_REUNION_FECHA, REABIERTA,
                           EMAILS_NUEVOS_COMENTARIOS, VOTACION_TELEMATICA, ADMITE_CAMBIO_VOTO, TIPO_VISUALIZACION_RESULTADOS)
VALUES (24002, 'Reunión Sin Acta Provisional', TO_DATE('2021-01-01 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0,
        1, NOW(), 1, 1, 1, '4tic', 'test@test.com', 1, 0, 1, 1, 1, NOW(), 0, 0, 1, 1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (24005, 24002, 'Organo Sin Acta Provisional', 1, 0, '24001', 'Órgano votación');

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_CODIGO, CARGO_NOMBRE,
                                            ASISTENCIA_CONFIRMADA, MIEMBRO_ID,
                                            RESPONSABLE_ACTA, suplente_id, suplente_email, suplente_nombre)
VALUES (24003, 24005, 0, 24002, '24001', 'miembro de organo', 'consuplente@test.com', 1, '3', 'SE', 'Secretario', 1, 3, 0,
        10, 'suplente@test.com', 'suplente del miembro');

