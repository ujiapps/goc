const votarPunto = (reunionId, puntoId, voto, votoPrevio, miembroVotoPresencialId) => {
    if (votoPrevio === 'false') {
        emitirVoto(reunionId, puntoId, voto, miembroVotoPresencialId);
    } else {
        actualizarVoto(reunionId, puntoId, voto, miembroVotoPresencialId);
    }
};

const emitirVoto = (reunionId, puntoId, voto, miembroVotoPresencialId) => {
    CMPLoading.show();
    const url = getVotacionEndpoint(puntoId, voto, miembroVotoPresencialId);
    $.ajax({
        type: "POST",
        url: url,
        processData: false,
        cache: false,
        error: function (e) {
            const res = JSON.parse(e.responseText);
            CMPLoading.hide();
            alert(res.message);
        },
        success: () => window.location.reload(),
        complete: function () {
            CMPLoading.hide();
        },
    });
};

const actualizarVoto = (reunionId, puntoId, voto, miembroVotoPresencialId) => {
    CMPLoading.show();
    const url = getVotacionEndpoint(puntoId, voto, miembroVotoPresencialId);
    $.ajax({
        type: "PUT",
        url: url,
        processData: false,
        cache: false,
        error: function (e) {
            const res = JSON.parse(e.responseText);
            CMPLoading.hide();
            alert(res.message);
        },
        success: () => window.location.reload(),
        complete: function () {
            CMPLoading.hide();
        },
    });
};

const getVotacionEndpoint = (puntoId, voto, miembroVotoPresencialId) => {
    var url = "/goc/rest/votos/?puntoId=" + puntoId + "&voto=" + voto;
    const miembroId = document.getElementById("selector")?.value;

    if (miembroId && miembroId > 0)
        url += "&representadoId=" + miembroId;

    if (miembroVotoPresencialId && miembroVotoPresencialId > 0)
        url += "&miembroVotoPresencialId=" + miembroVotoPresencialId;

    return url;
};

const onChangeVotanteRepresentado = (reunionId) => {
    const miembroId = document.getElementById("selector").value;
    var url = new URL(`${window.location.origin}/goc/rest/publicacion/reuniones/puntos/${reunionId}/`);

    url.searchParams.set('lang', applang);
    if (miembroId > 0) {
        url.searchParams.set('organoReunionMiembroId', miembroId);
    }
    window.location = url;
}

const abrirVotacionPuntoOrdinario = (puntoId) => {
    const url = `/goc/rest/votos/abre/votacion/${puntoId}`;

    $.ajax({
        type: "PUT",
        url: url,
        processData: false,
        cache: false,
        error: function (e) {
            const res = JSON.parse(e.responseText);
            alert(res.message);
        },
        success: () => window.location.reload()
    });
};

const cierraVotacionPunto = (puntoId) => {
    const url = `/goc/rest/votos/cierra/votacion/${puntoId}`;

    $.ajax({
        type: "PUT",
        url: url,
        processData: false,
        cache: false,
        error: function (e) {
            const res = JSON.parse(e.responseText);
            alert(res.message);
        },
        success: function (res) {
            if (!res || res.data.length === 0) {
                window.location.reload();
            } else {
                const modal = document.getElementById('miembrosNatosPorVotar-modal');
                const modalCloseButton = document.getElementById('miembrosNatosPorVotar-close')

                const handleCloseModal = () => modal.classList.remove('modal--open');
                modalCloseButton.onclick = () => handleCloseModal();

                modal.classList.add("modal--open");
                const modalContent = document.getElementById('miembrosNatosPorVotar-modal-body');

                modalContent.innerHTML =
                    `<div>
                        <ul>${res.data.map(miembro => (`<li>${miembro.nombre}</li>`)).join('')} </ul>
                        <button 
                          class="button"
                          id="miembrosNatosPorVotar-close-btn"
                          style="margin-top: 15px" 
                         >
                          Aceptar
                         </button>
                     </div`;

                document.getElementById('miembrosNatosPorVotar-close-btn').onclick = () => handleCloseModal();
            }
        }
    });
};

const guardarVotacionPuntoSecreto = (puntoId) => {

    var idVotosFavor = "votosFavor-" + puntoId;
    var idVotosContra = "votosContra-" + puntoId;
    var idVotosBlanco = "votosBlanco-" + puntoId;

    const votosFavor = document.getElementById(idVotosFavor);
    const votosContra = document.getElementById(idVotosContra);
    const votosBlanco = document.getElementById(idVotosBlanco);

    const url = `/goc/rest/votos/secretos/${puntoId}`;

    var data = {
        votosFavor: votosFavor.value,
        votosContra: votosContra.value,
        votosBlanco: votosBlanco.value
    };

    $.ajax({
        type: "PUT",
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function () {
            window.location.reload();
            alert(appI18N.common.votosGuardadosOk);
        },
        error: function (e) {
            const res = JSON.parse(e.responseText);
            alert(res.message);
        }
    });

};