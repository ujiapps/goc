$(function () {
    $('.collapsible.collapsed').each(function (index) {
        $(this).prev().before('<a class="collaptor" href="#"><i class="fa fa-caret-right"></i></a>');
    });

    $(document).on("click", "a.collaptor", function (event) {
        toggle(event, $(this).next().next(), $(this).children());
    });

    $(document).on("click", "a.collaptor + a", function (event) {
        toggle(event, $(this).next(), $(this).prev());
    });

    $(document).on("click", "#filter-button", function (event) {
        CMPLoading.show();

        $('form[name=form-busqueda]').find('input[name="pagina"]').val(0);
        $('form[name=form-busqueda]').submit();
    });

    function toggle(event, container, caret) {
        event.preventDefault();
        container.toggleClass('collapsed expanded');
        caret.children().each(function () {
            $(this).toggleClass('fa-caret-right fa-caret-down');
        });
    }

    $(document).ready(function () {
        $('input[name=fInicio]').datepicker({
            firstDay: 1,
            dateFormat: 'dd/mm/yy',
            onClose: function (selectedDate) {
                $("input[name=fFin]").datepicker("option", "minDate", selectedDate);
            }
        });

        $('input[name=fFin]').datepicker({
            firstDay: 1,
            dateFormat: 'dd/mm/yy',
            onClose: function (selectedDate) {
                $("input[name=fInicio]").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
});