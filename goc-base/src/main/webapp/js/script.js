var appI18N;
$(function () {

    var reunionId = $("div.nuevo-comentario input[name=reunionId]").val();

    $(document).ajaxComplete(function (event, xhr, settings) {
        if (!hasGocHeader(xhr)) {
            showReloadWarning();
        }

    });
    $(document).ajaxError(function (event, xhr, settings) {
        if (!hasGocHeader(xhr)) {
            showReloadWarning();
        }

    });

    $(document).ready(function () {
        if (!reunionId) return;

        loadComentarios(reunionId, appI18N);
    });

    function hasGocHeader(xhr) {
        return xhr.getResponseHeader('x-goc-server') != null && xhr.getResponseHeader('x-goc-server') == 'true';
    }

    function showReloadWarning() {
        alert(appI18N.common.sesionCaducada);
        window.location.reload(true);
    }

    function loadComentarios(reunionId, appI18N) {
        $.ajax({
            type: "GET",
            url: "/goc/rest/reuniones/" + reunionId + "/comentarios",
            success: function (response) {
                if (response.data.length === 0) {
                    $('div.comentarios').html('');
                    return;
                }

                var html = '<h3 class="title">' + appI18N.acta.comentarios + '</h3>';

                response.data.map(function (comentario) {
                    var creador = comentario.creadorNombre ? comentario.creadorNombre : comentario.creadorId;
                    html += '<div class="row columns">';

                    if (comentario.permiteBorrado == 'true') {
                        html += '  <a href="#" class="delete-comentario" data-id="' + comentario.id + '"><i class="fa fa-times"></i></a>';
                    }

                    html +=
                        '  <div class="comentario">' +
                        '    <p class=""autor"><strong>' + appI18N.acta.autor + '</strong>: ' + creador + '</p>' +
                        '    <div class="texto">' + comentario.comentario + '</div>' +
                        '    <p class="fecha"><strong>' + appI18N.acta.fecha + '</strong>: ' + comentario.fecha + '</p>' +
                        '  </div>' +
                        '</div>';
                });

                $('div.comentarios').html(html);
            }
        });
    }

    function loadResultadosBusquedaMiembros(result) {
        var items = [];
        if (result.data.length > 0) {
            $('div.nuevo-delegado-voto .footer button').show();
            for (var i = 0; i < result.data.length; i++) {
                var persona = result.data[i];
                items.push('<li> <input type="hidden" name="organoMiembroDelegadoId" value="' + persona.organoId + '" /> <input type="radio" name="persona" value="' + persona.miembroId + '"/> <label style="display: inline; margin-right: 0px;" for="delegadoVotoNombre">' + persona.nombre + '</label> <span style="display: inline">(</span><label style="display: inline" for="delegadoVotoEmail">' + persona.email + '</label><span style="display: inline">)</span></li>')
            }

            $('div.nuevo-delegado-voto ul.resultados').html(items.join('\n'));
        } else {
            alert(appI18N.common.noResultados);
        }
    }

    function loadResultadosBusquedaPersonas(result) {
        var items = [];
        if (result.data.length > 0) {
            $('div.nuevo-suplente .footer button').show();

            for (var i = 0; i < result.data.length; i++) {
                var persona = result.data[i];
                items.push('<li><input type="radio" name="persona" value="' + persona.id + '"/> <label style="display: inline; margin-right: 0px;" for="suplenteNombre">' + persona.nombre + '</label> <span style="display: inline">(</span><label style="display: inline" for="suplenteEmail">' + persona.email + '</label><span style="display: inline">)</span></li>')
            }

            $('div.nuevo-suplente ul.resultados').html(items.join('\n'));
        } else {
            alert(appI18N.common.noResultados);
        }
    }

    function buscaPersona(query) {
        $('div.nuevo-suplente .footer button').hide();
        $('div.nuevo-suplente ul.resultados').html("");

        $.ajax({
            type: "GET",
            url: "/goc/rest/personas/",
            data: {query: query},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                loadResultadosBusquedaPersonas(result)
            }
        });
    }

    function buscaMiembro(query) {
        $('div.nuevo-delegado-voto .footer button').hide();
        $('div.nuevo-delegado-voto ul.resultados').html('');

        $.ajax({
            type: "GET",
            url: "/goc/rest/reuniones/" + reunionId + "/miembros/otros",
            data: {query: query},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                loadResultadosBusquedaMiembros(result)
            }
        });
    }

    $('button[name=suplente]').on('click', function (ev) {
        $('div.nuevo-suplente input[name=organoMiembroId]').val($(this).data('miembroid'));

        var suplente = $(this).data('suplente');

        if (suplente) {
            $('div.nuevo-suplente p.suplente-actual').show();
            $('div.nuevo-suplente p.suplente-actual > strong').html(suplente);
        } else {
            $('div.nuevo-suplente p.suplente-actual').hide();
        }

        $('div.nuevo-suplente .header input[name=query-persona]').val('');
        $('div.nuevo-suplente .footer button').hide();
        $('div.nuevo-suplente ul.resultados').html('');
        $('div.nuevo-suplente').modal({clickClose: false});
    });

    $('button[name=delegado-voto]').on('click', function (ev) {
        $('div.nuevo-delegado-voto input[name=organoMiembroId]').val($(this).data('miembroid'));

        var delegadoVoto = $(this).data('delegadoVoto');

        if (delegadoVoto) {
            $('div.nuevo-delegado-voto p.delegado-voto-actual').show();
            $('div.nuevo-delegado-voto p.delegado-voto-actual > strong').html(delegadoVoto);
        } else {
            $('div.nuevo-delegado-voto p.delegado-voto-actual').hide();
        }

        buscaMiembro($('div.nuevo-delegado-voto input[name=query-persona]').val());

        $('div.nuevo-delegado-voto .header input[name=query-persona]').val('');
        $('div.nuevo-delegado-voto .footer button').hide();
        $('div.nuevo-delegado-voto ul.resultados').html('');
        $('div.nuevo-delegado-voto').modal({clickClose: false});
    });

    $('div.nuevo-suplente input[name=query-persona]').keypress(function (e) {
        if (e.which == 13) {
            buscaPersona($('div.nuevo-suplente input[name=query-persona]').val());
        }
    });

    $('div.nuevo-suplente button[name=busca-persona]').on('click', function (ev) {
        buscaPersona($('div.nuevo-suplente input[name=query-persona]').val());
    });

    $('button[name=borra-suplente]').on('click', function (ev) {
        borraSuplente($('div.nuevo-suplente input[name=organoMiembroId]').val());
    });

    $('button[name=borra-suplente-directo]').on('click', function (ev) {
        borraSuplente($(this).attr('data-miembroid'));
    });

    $('button[name=borra-delegado-voto]').on('click', function (ev) {
        borraDelegadoVoto($('div.nuevo-delegado-voto input[name=organoMiembroId]').val());
    });

    $('button[name=borra-delegado-voto-directo]').on('click', function (ev) {
        borraDelegadoVoto($(this).attr('data-miembroid'));
    });

    function borraSuplente(organoMiembroId) {
        var data = {
            organoMiembroId: organoMiembroId
        };

        $.ajax({
            type: "DELETE",
            url: "/goc/rest/reuniones/" + reunionId + "/suplente",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                window.location.reload();
            },
            error: function (e) {
                const res = JSON.parse(e.responseText);
                alert(res.message);
            }
        });
    };

    function borraDelegadoVoto(organoMiembroId) {
        var data = {
            organoMiembroId: organoMiembroId
        };

        $.ajax({
            type: "DELETE",
            url: "/goc/rest/reuniones/" + reunionId + "/delegadovoto",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                window.location.reload();
            },
            error: function (e) {
                const res = JSON.parse(e.responseText);
                alert(res.message);
            }
        });
    };

    $('button[name=add-suplente]').on('click', function (ev) {
        var suplente = $('div.nuevo-suplente ul.resultados li input[type=radio]').filter(':checked').parent('li');
        var organoMiembroId = $('div.nuevo-suplente input[name=organoMiembroId]').val();

        var data = {
            suplenteId: suplente.children('input[type=radio]').val(),
            suplenteNombre: suplente.children('label[for=suplenteNombre]').html(),
            suplenteEmail: suplente.children('label[for=suplenteEmail]').html(),
            organoMiembroId: organoMiembroId
        };

        $.ajax({
            type: "POST",
            url: "/goc/rest/reuniones/" + reunionId + "/suplente",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                window.location.reload();
            },
            error: function (e) {
                const res = JSON.parse(e.responseText);
                alert(res.message);
            }
        });
    });

    $('button[name=add-delegado-voto]').on('click', function (ev) {
        var delegacionVoto = $('div.nuevo-delegado-voto ul.resultados li input[type=radio]').filter(':checked').parent('li');
        var organoMiembroId = $('div.nuevo-delegado-voto input[name=organoMiembroId]').val();

        var data = {
            delegadoVotoId: delegacionVoto.children('input[type=radio]').val(),
            delegadoVotoNombre: delegacionVoto.children('label[for=delegadoVotoNombre]').html(),
            delegadoVotoEmail: delegacionVoto.children('label[for=delegadoVotoEmail]').html(),
            organoMiembroId: organoMiembroId,
            organoMiembroDelegadoId: delegacionVoto.children('[name=organoMiembroDelegadoId]').val()
        };

        $.ajax({
            type: "POST",
            url: "/goc/rest/reuniones/" + reunionId + "/delegadovoto",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                window.location.reload();
            },
            error: function (e) {
                const res = JSON.parse(e.responseText);
                alert(res.message);
            }
        });
    });

    $('button.add-comentario').on('click', function () {
        $('div.nuevo-comentario').modal({clickClose: false});
        $('div.nuevo-comentario textarea').val('');

        return false;
    });

    $('button.post-comentario').on('click', function (element) {
        element.target.disabled = true;
        var data = {
            comentario: $('div.nuevo-comentario textarea').val(),
            reunionId: reunionId
        }

        $.ajax({
            type: "POST",
            url: "/goc/rest/reuniones/" + reunionId + "/comentarios",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                loadComentarios(reunionId, appI18N);
                $.modal.close();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest.responseJSON.message);
            },
            complete: function () {
                element.target.disabled = false;
            }
        });
    });

    $(document).on("click", "a.delete-comentario", function (event) {
        event.preventDefault();
        var comentarioId = $(this).attr('data-id');

        if (confirm(appI18N.reuniones.deseaBorrarComentario)) {
            $.ajax({
                type: "DELETE",
                url: "/goc/rest/reuniones/" + reunionId + "/comentarios/" + comentarioId,
                success: function () {
                    loadComentarios(reunionId, appI18N);
                }
            });
        }
    });

    $(document).on("click", "a.delete-comentario-punto", function (event) {
        event.preventDefault();
        var comentarioId = $(this).attr('data-comentario-id');
        var puntoOrdenDiaId = $(this).attr('data-punto-id');

        if (confirm(appI18N.reuniones.deseaBorrarComentario)) {
            $.ajax({
                type: "DELETE",
                url: "/goc/rest/reuniones/" + reunionId + "/puntosOrdenDia/" + puntoOrdenDiaId + "/comentarios/" + comentarioId,
                success: function (respuesta) {
                    eliminarComentarioPunto(respuesta);
                }
            });
        }
    });

    function eliminarComentarioPunto(comentarioId) {
        var divComentario = $('#comentario-' + comentarioId);
        var divsComentarioWhereDeletedIsFather = $('[data-comentario-padre-id=' + comentarioId + ']');

        divComentario.remove();
        if (divsComentarioWhereDeletedIsFather.length > 0) {
            divsComentarioWhereDeletedIsFather.remove();
        }
    }

    $('button[name=confirmar-ausencia]').on('click', function () {
        var organoMiembroId = $(this).attr('data-miembroid');
        var justificacion = $('#motivo-ausencia').val();

        updateConfirmacion(false, organoMiembroId, justificacion);
    });

    $('div.confirmacion button[name=confirmar], div.confirmacion button[name=denegar]').on('click', function () {
        var confirmacion = $(this).attr("name") === 'confirmar' ? true : false;
        var organoMiembroId = $(this).attr('data-miembroid');
        $('button[name=confirmar-ausencia]').attr('data-miembroid', organoMiembroId);
        if (!confirmacion) {
            $('#form-ausencia').modal({clickClose: false});
        } else {
            updateConfirmacion(confirmacion, organoMiembroId);
        }
    })

    $('textarea[name=motivo-ausencia]').on('keyup', function () {
        var total = document.getElementById("motivo-ausencia").value;
        var strContador = document.getElementById("total");
        strContador.innerHTML = strContador.innerHTML.replace(/: \d+\/(\d+)/, ": " + total.length + "/$1");
    })


    function updateConfirmacion(confirmacion, organoMiembroId, justificacion) {
        var data;
        if (justificacion != null && justificacion != '') {
            data = JSON.stringify({confirmacion: confirmacion, justificacion: justificacion});
        } else {
            data = JSON.stringify({confirmacion: confirmacion});
        }
        $.ajax({
            type: "POST",
            url: "/goc/rest/reuniones/" + reunionId + "/confirmar/" + organoMiembroId,
            data: data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                window.location.reload();
            },
            error: function (e) {
                const res = JSON.parse(e.responseText);
                alert(res.message);
            },
            scope: this
        });
    }

	$("button[name='delete-doc-reunion']").on('click', function(ev) {
		var reunionId = $(this).attr("data-reunionid");
		var documentoId = $(this).attr("data-documentoid");
		var reunionAbierta = $(this).attr("data-reunionabierta");
		
		$("#form-borrado").attr("data-documentoid", documentoId);
		$("#form-borrado").attr("data-reunionid", reunionId);
		$("#form-borrado").attr("data-reunionabierta", reunionAbierta);

		if (confirm(appI18N.reuniones.deseaBorrarDocumento)) {
			if (reunionAbierta == "true") {
				$.ajax({
					type: "PUT",
					url: '/goc/rest/reuniones/' + reunionId + '/documentos/' + documentoId + '/desactivar',
					success: function() {
						$('#form-borrado').modal({ clickClose: false, escapeClose: false});
					}
				});
			} else {
				$.ajax({
					type: "DELETE",
					url: '/goc/rest/reuniones/' + reunionId + '/documentos/' + documentoId,
					success: function() {
						window.location.reload();
					}
				});
			}
		}
	});
	
    $("button[name='delete-doc-ordendia']").on('click', function (ev) {
        var reunionId = $(this).attr("data-reunionid");
        var documentoId = $(this).attr("data-documentoid");
		var reunionAbierta = $(this).attr("data-reunionabierta");
		var tipoAdjunto = "ADJUNTO";
		var puntoOrdenDiaId = $(this).attr("data-puntoordendiaid"); 
		
		$("#form-borrado").attr("data-documentoid", documentoId);
		$("#form-borrado").attr("data-reunionid", reunionId);
		$("#form-borrado").attr("data-puntoordendiaid", puntoOrdenDiaId);
		$("#form-borrado").attr("data-tipoadjunto", tipoAdjunto);
		

		if (confirm(appI18N.reuniones.deseaBorrarDocumento)) {
			if (reunionAbierta == "true") {
				$.ajax({
					type: "DELETE",
					url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + documentoId + '/inhabilitar',
					success: function() {
						$('#form-borrado').modal({clickClose: false, escapeClose: false});
					}
				});
			} else {
				$.ajax({
					type: "DELETE",
					url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + documentoId,
					success: function() {
						window.location.reload();
					}
				});
			}
		}
    });
	
	$("button[name='delete-doc-acuerdo-ordendia']").on('click', function (ev) {
	        var reunionId = $(this).attr("data-reunionid");
	        var acuerdoId = $(this).attr("data-acuerdoid");
			var reunionAbierta = $(this).attr("data-reunionabierta");
			var tipoAdjunto = "ACUERDO";
			var puntoOrdenDiaId = $(this).attr("data-puntoordendiaid"); 
			
			$("#form-borrado").attr("data-documentoid", acuerdoId);
			$("#form-borrado").attr("data-reunionid", reunionId);
			$("#form-borrado").attr("data-puntoordendiaid", puntoOrdenDiaId);
			$("#form-borrado").attr("data-tipoadjunto", tipoAdjunto);
			

			if (confirm(appI18N.reuniones.deseaBorrarDocumento)) {
				if (reunionAbierta == "true") {
					$.ajax({
						type: "DELETE",
						url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + acuerdoId + '/inhabilitar',
						success: function() {
							$('#form-borrado').modal({clickClose: false, escapeClose: false});
						}
					});
				} else {
					$.ajax({
						type: "DELETE",
						url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + acuerdoId,
						success: function() {
							window.location.reload();
						}
					});
				}
			}
	    });
		
	$("button[name='confirmar-borrado-doc']").on('click', function(ev) {
		var documentoId = $("#form-borrado").attr("data-documentoid");
		var reunionId = $("#form-borrado").attr("data-reunionid");
		var puntoOrdenDiaId = $("#form-borrado").attr("data-puntoordendiaid");
		var tipoAdjunto = $("#form-borrado").attr("data-tipoadjunto");
		var motivoBorrado = $('textarea[name=motivo-borrado]').val();
		
		if (!motivoBorrado) {
			alert("Debes ingresar un motivo antes de borrar el documento.");
			return;
		}

		var jsonData = JSON.stringify({ motivoEdicion: motivoBorrado });

		if (puntoOrdenDiaId) {
			$.ajax({
				type: "POST",
				url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + documentoId + '/guardarModificacionDocumento',
				data: jsonData,
				contentType: "application/json",
				success: function() {
					$.ajax({
						type: "PUT",
						url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + documentoId + '/motivoEdicion',
						data: jsonData,
						contentType: "application/json",
						success: function() {
							window.location.reload();
						}
					});
				}
			});
		} else {
			$.ajax({
				type: "POST",
				url: '/goc/rest/reuniones/' + reunionId + '/documentos/' + documentoId + '/guardarModificacionDocumento',
				data: jsonData,
				contentType: "application/json",
				success: function() {
					$.ajax({
						type: "PUT",
						url: '/goc/rest/reuniones/' + reunionId + '/documentos/' + documentoId + '/motivoEdicion',
						data: jsonData,
						contentType: "application/json",
						success: function() {
							window.location.reload();
						}
					});
				}
			});
		}
	});
	

    function cargaReunionesHistorico(target, pagina, contenedor) {
        let organoId = target.data('organo-id'),
            organoExterno = target.data('organo-externo'),
            lang = target.data('lang');

        CMPLoading.show();

        $.ajax({
            type: "GET",
            url: '/goc/rest/publicacion/reuniones-cerradas',
            data: {
                organoId: organoId,
                organoExterno: organoExterno,
                pagina: pagina,
                lang: lang
            },
            success: function (data) {
                contenedor[0].innerHTML = data;
                CMPLoading.hide();
            },
            error: function (e) {
                CMPLoading.hide();
                alert(e.responseText);
            }
        });
    }

    $(".lista-reuniones.historico .accordion-item a.accordion-title").click(function () {

        let contenedor = $(this).siblings(".accordion-content");

        cargaReunionesHistorico($(this), 0, contenedor);
    });

    $('.lista-reuniones.historico .accordion-item').on('click', 'a.nextPage', function (event) {
        event.preventDefault();

        if ($(this).hasClass("disabled")) return;

        let contenedor = $(this).closest('.accordion-content'),
            pagina = $(this).data('pagina');

        pagina++;

        cargaReunionesHistorico($(this), pagina, contenedor);

        return false;
    });

    $('.lista-reuniones.historico .accordion-item').on('click', 'a.prevPage', function (event) {
        event.preventDefault();

        if ($(this).hasClass("disabled")) return;

        let contenedor = $(this).closest('.accordion-content'),
            pagina = $(this).data('pagina');

        pagina--;

        cargaReunionesHistorico($(this), pagina, contenedor);

        return false;
    });
});

function subirDocumentoPuntoOrdenDia(button) {
    var form = document.getElementById(button.parentNode.id);
    var errors = validarFormularioDocumentoPuntosOrdenDia(form);
    if (errors.length == 0) {
        var data = new FormData(form);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: button.parentNode.action,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                location.reload();
            },
            error: function (e) {
                alert(e.responseText);
            }
        });
    } else {
        var labels = document.getElementsByTagName('label');
        var label;
        for (var i = 0; i < labels.length; i++) {
            if (labels[i].htmlFor == form[i].id) {
                label = labels[i];
            }
        }
        for (var i in errors) {
            if (label) {
                label.className += "form-invalido";
                label = null;
            } else {
                form[i].className += "form-invalido";
            }
        }
    }
}

function mostrarFomularioSubirDocumento(button) {
    document.getElementById(button.getAttribute('data-target')).style.display = 'block';
    button.style.display = 'none';
}

function validarFormularioDocumentoPuntosOrdenDia(form) {
    var errors = [];
    if (!form["archivo"].value) {
        errors.push("archivo");
    }
    if (!form["descripcion"].value) {
        errors.push("descripcion");
    }
    return errors;
}

function eliminarError(item, label) {
    if (item.value && label) {
        label.classList.remove("form-invalido");
    } else {
        if (item.value) {
            item.classList.remove("form-invalido");
        }
    }
}

function mostrarNombre(input) {
    var fileName = '';
    var labels = document.getElementsByTagName('label');
    var label;
    for (var i = 0; i < labels.length; i++) {
        if (labels[i].htmlFor == input.id) {
            label = labels[i];
        }
    }
    if (input.value) {
        fileName = input.value.split('\\').pop();
        eliminarError(input, label);
    }

    if (fileName) {
        label.querySelector('span').innerHTML = fileName;
    }
}

function getPuntoIdByDataAttribute(element) {
    return $(element).attr('data-punto-id');
}

function getIdComentarioByDataAttribute(element) {
    return $(element).attr('data-comentario-id');
}

function getDivComentariosByPuntoId(puntoId) {
    return $('#comentarios-' + puntoId);
}

function getBotonVerRespuestas(puntoId, idComentarioInicial) {
    return $('#comentarios-' + puntoId + ' .enlace-ver-respuestas[data-comentario-id=' + idComentarioInicial + ']');
}


function getBotonVerComentarios(puntoId) {
    return $('.enlace-ver-comentarios[data-punto-id=' + puntoId + ']');
}


function getBotonOcultarComentarios(puntoId) {
    return $('.enlace-ocultar-comentarios[data-punto-id=' + puntoId + ']');
}

function getBotonOcultarRespuestas(puntoId, idComentario) {
    return $('#comentarios-' + puntoId + ' .enlace-ocultar-respuestas[data-comentario-id=' + idComentario + ']');
}


function verComentarios(element) {

    var puntoId = getPuntoIdByDataAttribute(element);
    var a = $('#comentarios-' + puntoId);
    a.show();

    var boton_ocultar = getBotonOcultarComentarios(puntoId);
    boton_ocultar.show();
    $(element).hide();
}

function cerrarComentarios(element) {
    var puntoId = getPuntoIdByDataAttribute(element);
    var a = $('#comentarios-' + puntoId);
    a.hide();

    var boton_ver = getBotonVerComentarios(puntoId);
    boton_ver.show();
    $(element).hide();
}

function verRespuestas(element) {
    var puntoId = getPuntoIdByDataAttribute(element);
    var idComentario = getIdComentarioByDataAttribute(element);
    var respuestas = $('#comentarios-' + puntoId + ' [data-comentario-padre-id=' + idComentario + ']');
    respuestas.show();

    var boton_ocultar = getBotonOcultarRespuestas(puntoId, idComentario);
    boton_ocultar.show();
    $(element).hide();
}

function cerrarRespuestas(element) {
    var puntoId = getPuntoIdByDataAttribute(element);
    var idComentario = getIdComentarioByDataAttribute(element);


    cerrarRespuestasRecursiva($('#comentarios-' + puntoId + ' [data-comentario-padre-id=' + idComentario + ']'), puntoId, idComentario);
}

function cerrarRespuestasRecursiva(respuestas, puntoId, idComentarioInicial) {
    for (var i = 0; i < respuestas.length; i++) {
        var idComentario = getIdComentarioByDataAttribute(respuestas[i]);
        cerrarRespuestasRecursiva($('#comentarios-' + puntoId + ' [data-comentario-padre-id=' + idComentario + ']'), puntoId, idComentario);
    }
    if ($('#comentarios-' + puntoId + ' [data-comentario-padre-id=' + idComentarioInicial + ']').length > 0) {
        respuestas.hide();
        var boton_ver = getBotonVerRespuestas(puntoId, idComentarioInicial);
        var boton_ocultar = getBotonOcultarRespuestas(puntoId, idComentarioInicial);
        boton_ver.show();
        boton_ocultar.hide();
    }
}

function abrirModalComentarioPunto(element, event) {
    $('div.nuevo-comentario-punto').modal({clickClose: false});
    $('div.nuevo-comentario-punto textarea').val('');
    $('div.nuevo-comentario-punto [name=puntoId]').val(getPuntoIdByDataAttribute(element));
    var comentarioPadre = getIdComentarioByDataAttribute(element);
    if (comentarioPadre) {
        $('div.nuevo-comentario-punto [name=comentarioId]').val(comentarioPadre);
    } else {
        $('div.nuevo-comentario-punto [name=comentarioId]').val('');
    }

    event.preventDefault();
    return false;
}

function abrirResultadosVotacionDetalle(puntoId) {
    var deplegableResultadosDetalle = document.getElementById('votantes_punto-' + puntoId)
    if (deplegableResultadosDetalle.style.display === "block") {
        deplegableResultadosDetalle.style.display = 'none';
        document.getElementById('icon-' + puntoId).className = "fa fa-angle-down";
    } else {
        deplegableResultadosDetalle.style.display = 'block';
        document.getElementById('icon-' + puntoId).className = "fa fa-angle-up";
    }

}

function enviarComentarioPunto(element) {
    element.disabled = true;
    var data = {
        comentario: $('div.nuevo-comentario-punto textarea').val(),
        reunionId: $('div.nuevo-comentario-punto [name=reunionId]').val(),
        puntoOrdenDiaId: $('div.nuevo-comentario-punto [name=puntoId]').val(),
        comentarioSuperiorId: $('div.nuevo-comentario-punto [name=comentarioId]').val()
    };

    $.ajax({
        type: "POST",
        url: "/goc/rest/reuniones/" + data.reunionId + "/puntosOrdenDia/" + data.puntoOrdenDiaId + '/comentarios',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (comentarioCreado) {
            anyadirComentarioInsertado(comentarioCreado.data);
        },
        complete: function () {
            element.disabled = false;
        }
    });
}

function getMargenIndentado(margin, marginNum) {
    if (margin != null) {
        marginNum = parseInt(margin.substring(0, margin.indexOf('px')));
    }
    marginNum += 25;
    return marginNum;
}

function anyadirComentarioInsertado(respuesta) {
    var marginNum = 0;
    if (respuesta.comentarioSuperiorId) {
        var divComentarioPadre = $('#comentario-' + respuesta.comentarioSuperiorId);
        var respuestasComentarioPadre = $('#comentarios-' + respuesta.puntoOrdenDiaId + ' [data-comentario-padre-id=' + respuesta.comentarioSuperiorId + ']');
        var margin = divComentarioPadre.css('margin-left');

        marginNum = getMargenIndentado(margin, marginNum);

        var html = procesarPlantilla(respuesta, marginNum);
        $('#comentario-' + respuesta.comentarioSuperiorId + ' .separador-enlaces-comentarios').show();
        $('#comentario-' + respuesta.comentarioSuperiorId + ' .enlace-ocultar-respuestas').show();
        if (respuestasComentarioPadre.length > 0) {
            $(html).insertAfter(respuestasComentarioPadre[respuestasComentarioPadre.length - 1]);
        } else {
            $(html).insertAfter(divComentarioPadre);
        }
        $.modal.close();
    } else {
        var divComentarios = getDivComentariosByPuntoId(respuesta.puntoOrdenDiaId);
        var html = procesarPlantilla(respuesta, marginNum);

        divComentarios.append(html);
        divComentarios.show();
        var boton_ocultar = getBotonOcultarComentarios(respuesta.puntoOrdenDiaId);
        boton_ocultar.show();
        $.modal.close();
    }

    function procesarPlantilla(respuesta, marginNum) {
        var html =
            '  <div class="comentario" id="comentario-' + respuesta.id + '" data-comentario-id="' + respuesta.id + '" data-comentario-padre-id="' + respuesta.comentarioSuperiorId + '" style="margin-left: ' + marginNum + 'px;">' +
            '    <a href="#" class="delete-comentario-punto"  data-punto-id="' + respuesta.puntoOrdenDiaId + '" data-comentario-id="' + respuesta.id + '"><i class="fa fa-times"></i></a>' +
            '   <div class="comentario-cabecera">' + '    <p class="autor"><strong>' + appI18N.comentarios.autor + '</strong>: ' + respuesta.creadorNombre + ' </p>';

        html += '   </div>' +
            '   <div class="comentario-cuerpo">' +
            respuesta.comentario +
            '   <div> ' +
            '    <p class="fecha"><strong>' + appI18N.comentarios.fecha + '</strong>: [' + respuesta.fecha + ']</p>' +
            '  </div>' +
            '  <div>' +
            '<a href="#comentario-' + respuesta.id + '" style="display: none;" onclick="verRespuestas(this)" class="enlace-ver-respuestas" data-comentario-id="' + respuesta.id + '" data-punto-id="' + respuesta.puntoOrdenDiaId + '">' + appI18N.comentarios.verRespuestas + '</a>' +
            '<a href="#comentario-' + respuesta.id + '" style="display: none;" onclick="cerrarRespuestas(this)" class="enlace-ocultar-respuestas" data-comentario-id="' + respuesta.id + '" data-punto-id="' + respuesta.puntoOrdenDiaId + '">' + appI18N.comentarios.ocultarRespuestas + '</a>' +
            '<span style="display: none" class="separador-enlaces-comentarios"> | </span>' +
            '<a href="#" onclick="abrirModalComentarioPunto(this)" class="enlace-responder-comentario" data-comentario-id="' + respuesta.id + '" data-punto-id="' + respuesta.puntoOrdenDiaId + '">' + appI18N.comentarios.responder + '</a>' +
            '  </div>' +
            '</div>';
        return html;
    }

}
