$(function () {
    var form, pagina, paginaActual;

    function setValues(item) {
        form = item.closest('form');

        if (form.length === 0) form = $('form[name=form-busqueda]');

        pagina = form.find('input[name=pagina]');
        paginaActual = parseInt(pagina.val());
    }

    $('a.nextPage').click(function (event) {
        event.preventDefault();

        if ($(this).hasClass("disabled")) return;

        CMPLoading.show();

        setValues($(this));

        pagina.val(paginaActual + 1);
        form.submit();

        return false;
    });

    $('a.prevPage').click(function (event) {
        event.preventDefault();

        if ($(this).hasClass("disabled")) return;

        CMPLoading.show();

        setValues($(this));

        pagina.val(paginaActual - 1);
        form.submit();

        return false;
    });
});