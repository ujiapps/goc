var CMPLoading = (function () {
    function change(value) {
        var loading = document.querySelector("#cmpcomponent-loading");

        if (!loading) return;

        loading.style.display = value;
    }

    return {
        show: function () {
            change('block');
        },

        hide: function () {
            change('none');
        }
    }
})();