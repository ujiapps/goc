Ext.define('Ext.ux.uji.form.ChangedField', {
    alias: 'plugin.changedfield',
    singleton: true,

    config: {
        key: 'changedfield'
    },

    enable: function () {
        sessionStorage.setItem(this.config.key, true);
    },

    disable: function () {
        sessionStorage.removeItem(this.config.key);
    },

    isEnabled: function () {
        return sessionStorage.getItem(this.config.key);
    }
});