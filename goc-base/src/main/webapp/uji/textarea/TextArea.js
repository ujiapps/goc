Ext.define('Ext.ux.uji.textarea.TextArea',
{
    extend : 'Ext.form.field.TextArea',
    alias : 'widget.ujitextareafield',

    enableKeyEvents: true,

    listeners : {
        keypress : function()
        {
            Ext.ux.uji.form.ChangedField.enable();
        }
    }
});