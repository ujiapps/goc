<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="es.uji.apps.goc.auth.LanguageConfig" %>
<%@ page import="es.uji.apps.goc.auth.PersonalizationConfig" %>
<%@ page import="es.uji.commons.sso.User" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%
        User user = (User) session.getAttribute(User.SESSION_USER);
        List<String> roles = (List<String>) session.getAttribute("roles");

        String rolesString = "[";
        for(String rol : roles) {
            rolesString += "'" + rol + "',";
        }
        rolesString = rolesString.substring(0, rolesString.length() - 1);
        rolesString += "]";

        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        LanguageConfig languageConfig = context.getBean(LanguageConfig.class);
        PersonalizationConfig personalizationConfig = context.getBean(PersonalizationConfig.class);
        String lang = request.getParameter("lang");
        if(lang == null)
        {
            lang = languageConfig.mainLanguage;
        }
    %>
    <title>Gestión de órganos colegiados</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/build/classic/theme-triton/resources/theme-triton-all.css">
    <link rel="stylesheet" type="text/css" href="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/build/packages/font-awesome/resources/font-awesome-all.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script type="text/javascript" src="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/build/ext-all.js"></script>
    <script type="text/javascript" src="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/build/classic/locale/locale-<%= lang %>.js"></script>
    <script type="text/javascript" src="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/packages/ux/classic/src/TabCloseMenu.js"></script>
    <script type="text/javascript" src="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/packages/ux/classic/src/form/SearchField.js"></script>
    <script type="text/javascript" src="app/i18n/<%= lang %>.js"></script>

    <title>Portal</title>
    <script type="text/javascript">
        var login = '<%= user.getName() %>';
        var perId = <%= user.getId() %>;
        var roles = eval(<%= rolesString %>);
        var mainLanguage = '<%= languageConfig.mainLanguage %>';
        var mainLanguageDescription = '<%= languageConfig.mainLanguageDescription %>';
        var alternativeLanguage = '<%= languageConfig.alternativeLanguage %>';
        var alternativeLanguageDescription = '<%= languageConfig.alternativeLanguageDescription %>';
        var logo = '<%= personalizationConfig.logo %>';
        var staticsurl = '<%= personalizationConfig.staticsUrl %>';
        var exprMeetingTitle = '<%= personalizationConfig.tituloAutomatico %>';
        var exprMeetingTitleAlternative = '<%= personalizationConfig.tituloAutomaticoAlternativo %>';
        var mostrarInformarOtrosOrganos = <%= personalizationConfig.mostrarInformarOtrosOrganos %>;
        var enlaceManual= '<%= personalizationConfig.enlaceManual %>';
        var enlaceMasOpciones = '<%= personalizationConfig.enlaceMasOpciones %>';
        var activarOpcionesPorDefecto = <%= personalizationConfig.activarOpcionesPorDefecto %>;
        var admiteCambioVoto = <%= personalizationConfig.admiteCambioVoto %>;
        var rolAdmin = '<%= personalizationConfig.rolAdministrador %>';
        var mostrarPublicarAcuerdos = <%= personalizationConfig.mostrarPublicarAcuerdos %>;
        var habilitarVotaciones = <%= personalizationConfig.habilitarVotaciones %>;
        var miembrosNatos = <%= personalizationConfig.miembrosNatos %>;
        var permiteAbstencionVoto = <%= personalizationConfig.permiteAbstencionVoto %>;
        var presidenteVotoDoble = <%= personalizationConfig.presidenteVotoDoble %>;
		var verDeliberaciones = <%= personalizationConfig.verDeliberaciones %>;
		var verAcuerdos = <%= personalizationConfig.verAcuerdos %>;

        
        function getMultiLangLabel(value, lang) {
            if (isMultilanguageApplication() && lang === mainLanguage)
                return value + ' (' + mainLanguageDescription + ')';

            if (isMultilanguageApplication() && lang === alternativeLanguage)
                return value + ' (' + alternativeLanguageDescription + ')';

            return value;
        }

        function isMultilanguageApplication() {
            return (mainLanguage && mainLanguageDescription && alternativeLanguage && alternativeLanguageDescription);
        }

        function isAdmin() {
            var isAdmin = false;
            if(roles) {
                for (var i = 0; i < roles.length; i++) {
                    if(roles[i] === rolAdmin) {
                        isAdmin = true;
                    }
                }
            }
            return isAdmin;
        }
    </script>

    <script type="text/javascript">

        function getValidLang(lang) {
            if (!lang || (lang.toLowerCase() !== mainLanguage && lang.toLowerCase() !== alternativeLanguage)) {
                return mainLanguage;
            }

            return lang;
        }

        var requestLang = '<%= request.getParameter("lang")%>';
        var appLang = getValidLang(requestLang);
    </script>

    <%
        if(!personalizationConfig.debug)
        {
    %>
        <script type="text/javascript" src="goc-all.js"></script>
    <%
        } else {
    %>
        <script type="text/javascript" src="app/Application.js"></script>
    <%
        }
    %>
</head>
<body>
    <div id="landing-loading">
        <img src="img/gears.gif"/>
        <p>Loading...</p>
    </div>
</body>
</html>
