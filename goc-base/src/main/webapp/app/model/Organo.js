Ext.define('goc.model.Organo', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'string' },
        { name: 'nombre', type: 'string' },
        { name: 'nombreAlternativo', type: 'string' },
        { name: 'inactivo', type: 'boolean' },
        { name: 'convocarSinOrdenDia', type: 'boolean' },
        { name: 'externo', type: 'boolean' },
        { name: 'tipoOrganoId', type: 'number' },
        { name: 'email', type: 'string' },
        { name: 'actaProvisionalActiva', type: 'boolean', defaultValue: true },
        { name: 'delegacionVotoMultiple', type: 'boolean', defaultValue: true },
        { name: 'tipoProcedimientoVotacionId', type: 'string', defaultValue : 'ORDINARIO' },
        { name: 'permiteAbstencionVoto', type: 'boolean' },
        { name: 'presidenteVotoDoble', type: 'boolean'},
        { name: 'mostrarAsistencia',type:'boolean',defaultValue:true},
        { name: 'verDelegaciones', type:'boolean',defaultValue:true}
    ]
});
