Ext.define('goc.model.PuntosOrdenDiaTipoAdjuntosCombo', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'string'},
        {
            name: 'name', type: 'string',
            calculate: function (data) {
                if (data && data.id)
                    return appI18N.adjuntos.tipos[data.id.toLowerCase()];
                return '';
            }
        }

    ]
});