Ext.define('goc.model.OrganoReunionInvitado', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'number' },
        { name: 'organoReunionId', type: 'number' },
        { name: 'organoExterno', type: 'boolean' },
        { name: 'reunionId', type: 'number' },
        { name: 'organoId', type: 'string' },
        { name: 'nombre', type: 'string' },
        { name: 'email', type: 'string' },
        { name: 'personaId', type: 'number' },
        { name: 'urlAsistencia', type: 'string' },
        { name: 'urlAsistenciaAlt', type: 'string' },
        { name: 'soloConsulta', type: 'boolean' },
        { name: 'asistencia', type: 'boolean', defaultValue: true }
    ]
});
