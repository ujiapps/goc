Ext.define('goc.model.Reunion', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'number' },
        { name: 'asunto', type: 'string' },
        { name: 'asuntoAlternativo', type: 'string' },
        { name: 'descripcion', type: 'string' },
        { name: 'descripcionAlternativa', type: 'string' },
        { name: 'ubicacion', type: 'string' },
        { name: 'ubicacionAlternativa', type: 'string' },
        { name: 'urlGrabacion', type: 'string' },
        { name: 'numeroDocumentos', type: 'number' },
        { name: 'fecha', type: 'date', dateFormat: 'd/m/Y H:i:s' },
        { name: 'fechaSegundaConvocatoria', type: 'date', dateFormat: 'd/m/Y H:i:s' },
        { name: 'fechaFinVotacion', type: 'date', dateFormat: 'd/m/Y H:i:s' },
        { name: 'completada', type: 'boolean' },
        { name: 'telematica', type: 'boolean' },
        { name: 'admiteSuplencia', type: 'boolean' },
        { name: 'admiteDelegacionVoto', type: 'boolean' },
        { name: 'admiteComentarios', type: 'boolean' },
        { name: 'emailsEnNuevosComentarios', type: 'boolean' },
        { name: 'telematicaDescripcion', type: 'string' },
        { name: 'telematicaDescripcionAlternativa', type: 'string' },
        { name: 'publica', type: 'boolean' },
        { name: 'avisoPrimeraReunion', type: 'boolean'},
        { name: 'avisoPrimeraReunionUser', type: 'string'},
        { name: 'avisoPrimeraReunionFecha', type: 'date', dateFormat: 'd/m/Y H:i:s'},
        { name: 'numeroSesion', type: 'string' },
        { name: 'verDeliberaciones',type:'boolean'},
        { name: 'verAcuerdos',type:'boolean'},
        {
            name: 'duracion',
            type: 'number'
        },
        {
            name: 'hora',
            type: 'string',
            calculate: function(data) {
                if (data && data.fecha)
                    return Ext.Date.format(new Date(data.fecha), 'H:i');
                return '';
            }
        },
        {
            name: 'horaSegundaConvocatoria',
            type: 'string',
            calculate: function(data) {
                if (data && data.fechaSegundaConvocatoria)
                    return Ext.Date.format(new Date(data.fechaSegundaConvocatoria), 'H:i');
                return '';
            }
        },
        {
            name: 'horaFinVotacion',
            type: 'string',
            calculate: function(data) {
                if (data && data.fechaFinVotacion)
                    return Ext.Date.format(new Date(data.fechaFinVotacion), 'H:i');
                return '';
            }
        },
        { name: 'urlActa', type: 'string'},
        { name: 'urlActaAlternativa', type: 'string'},
        { name: 'horaFin', type: 'string' },
        { name: 'convocatoriaComienzo', type: 'number'},
        { name: 'votacionTelematica', type: 'boolean'},
        { name: 'admiteCambioVoto', type: 'boolean'},
        { name: 'responsableVotoId', type: 'number'},
        { name: 'responsableVotoNom', type: 'string'},
    ]
});