Ext.define('goc.model.PuntoOrdenDia', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'int', persist: false },
        { name: 'titulo', type: 'string' },
        { name: 'tituloAlternativo', type: 'string' },
        { name: 'descripcion', type: 'string' },
        { name: 'descripcionAlternativa', type: 'string' },
        { name: 'deliberaciones', type: 'string' },
        { name: 'deliberacionesAlternativas', type: 'string' },
        { name: 'acuerdos', type: 'string' },
        { name: 'acuerdosAlternativos', type: 'string' },
        { name: 'orden', type: 'number' },
        { name: 'numeroDocumentos', type: 'number' },
        { name: 'reunionId', type: 'number' },
        { name: 'publico', type: 'boolean' },
        { name: 'puntoSuperior' },
        { name: 'tipoVoto', type: 'string'},
        { name: 'tipoProcedimientoVotacion', type: 'string'},
        { name: 'tipoRecuentoVoto', type: 'string'},
        { name: 'votableEnContra', type: 'boolean', defaultValue: true},
        { name: 'tipoVotacion', type: 'string'}
    ]
});