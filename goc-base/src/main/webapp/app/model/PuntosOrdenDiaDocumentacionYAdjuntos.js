Ext.define('goc.model.PuntosOrdenDiaDocumentacionYAdjuntos', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'number' },
        { name: 'descripcion', type: 'string' },
        { name: 'tipo', type: 'string'},
        { name: 'descripcionAlternativa', type: 'string' },
        { name: 'mostrarEnFicha', type: 'boolean' },
        { name: 'publico', type: 'boolean'},
        { name: 'editorId', type:'number'},
        { name: 'fechaEdicion', type:'date'},
        { name: 'motivoEdicion', type:'string'},
        { name: 'activo', type:'boolean', defaultValue: true},
        { name: 'orden', type:'number'}
    ]
});