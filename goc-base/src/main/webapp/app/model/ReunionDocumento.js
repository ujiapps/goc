Ext.define('goc.model.ReunionDocumento', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'number' },
        { name: 'descripcion', type: 'string' },
        { name: 'descripcionAlternativa', type: 'string' },
        { name: 'editorId', type:'number'},
        { name: 'fechaEdicion', type:'date'},
        { name: 'motivoEdicion', type:'string'},
        { name: 'activo', type:'boolean', defaultValue:true},
        { name: 'orden',type:'number'}
    ]
});