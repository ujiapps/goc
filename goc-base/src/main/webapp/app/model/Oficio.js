Ext.define('goc.model.Oficio', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'number' },
        { name: 'tituloReunion', type: 'string' },
        { name: 'tituloReunionAlternativo', type: 'string' },
        { name: 'tituloPuntoOrdenDia', type: 'string' },
        { name: 'tituloReunionAlternativo', type: 'string' },
        { name: 'destinatario', type: 'string' },
        { name: 'fechaEnvio', type: 'date', dateFormat: 'd/m/Y H:i:s' },
        { name: 'puntoOrdenDiaId', type: 'int'},
        { name: 'email', type: 'string' },
        { name: 'registroSalida', type: 'string' },
        { name: 'nombre', type: 'string' }
    ]
});
