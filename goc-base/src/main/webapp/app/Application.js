Ext.Loader.setPath('Ext.ux', staticsurl + '/js/extjs/ext-6.2.1/packages/ux/classic/src');
Ext.Loader.setPath('Ext.ux.uji', 'uji');
Ext.Loader.setPath('goc', 'app');

if (appI18N != null) {
    Ext.require('Ext.ux.uji.data.Store');
    Ext.require('Ext.ux.TabCloseMenu');
    Ext.require('Ext.ux.form.SearchField');
    Ext.require('Ext.ux.uji.ApplicationViewport');
    Ext.require('goc.view.tipoOrgano.Main');
    Ext.require('goc.view.organo.Main');
    Ext.require('goc.view.cargo.Main');
    Ext.require('goc.view.miembro.Main');
    Ext.require('goc.view.reunion.Main');
    Ext.require('goc.view.historicoReunion.Main');
    Ext.require('goc.view.common.LookupWindowPersonas');
    Ext.require('goc.view.descriptor.Main');
    Ext.require('goc.view.oficio.Main');

    Ext.ariaWarn = Ext.emptyFn;

    Ext.define('Transforms', {
        statics: {
            _hasExternalPrefix: function (organo) {
                return organo.externo === true && organo.id.startsWith('E');
            },

            getOrganoId: function (id, externo) {
                if (this._hasExternalPrefix({id: id, externo: externo})) {
                    return id.substring(1);
                } else {
                    return id;
                }
            },

            transformIdExterno: function (organo) {
                if (organo.externo === "true")
                    organo.id = 'E' + organo.id;
                else
                    organo.id = organo.id;
                return organo;
            }
        }
    });

    Ext.define('Overrides.form.field.Base',
        {
            override: 'Ext.form.field.Base',

            getLabelableRenderData: function () {
                var me = this, data = me.callParent(), labelSeparator = me.labelSeparator;

                if (!me.allowBlank) {
                    data.labelSeparator = labelSeparator + ' <span style="color:red">*</span>';
                }

                return data;
            }
        });

    Ext.define('Overrides.form.field.Text',
        {
            override: 'Ext.form.field.Text',

            onFocus: function (e) {
                var me = this;
                me.addCls(me.fieldFocusCls);
                me.triggerWrap.addCls(me.triggerWrapFocusCls);
                me.inputWrap.addCls(me.inputWrapFocusCls);
            }
        }
    );

    Ext.define('overrides.selection.CheckboxModel', {
        override: 'Ext.selection.CheckboxModel',
        privates: {
            onBeforeNavigate: function (metaEvent) {
                var e = metaEvent.keyEvent;
                e.ctrlKey = true;
            }
        }
    });

    /*Ext.Ajax.timeout = 600000;
    Ext.Ajax.setTimeout(600000);
    Ext.override(Ext.data.proxy.Ajax, {timeout: 600000});
    Ext.override(Ext.form.action.Action, {timeout: 600000});
    Ext.override(Ext.data.Connection, {timeout: 600000});*/

    Ext.application(
        {
            extend: 'Ext.app.Application',

            name: 'goc',
            title: 'goc',

            // requires : [ 'goc.model.Organo' ],

            launch: function () {
                Ext.create('Ext.ux.uji.ApplicationViewport',
                    {
                        codigoAplicacion: 'GOC',
                        tituloAplicacion: appI18N.header.titulo,
                        dashboard: false
                    });

                document.getElementById('landing-loading').style.display = 'none';
            }
        });
}