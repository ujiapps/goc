var cargoGridColumns = [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        text : getMultiLangLabel(appI18N.cargos.codigo, mainLanguage),
        dataIndex : 'codigo',
        editor : {
            field : {
                allowBlank : false
            }
        }
    },
    {
        text : getMultiLangLabel(appI18N.cargos.nombre, mainLanguage),
        dataIndex : 'nombre',
        flex : 1,
        editor : {
            field : {
                allowBlank : false
            }
        }
    }
];

if (isMultilanguageApplication())
{
    cargoGridColumns.push({
        text : getMultiLangLabel(appI18N.cargos.nombre, alternativeLanguage),
        dataIndex : 'nombreAlternativo',
        flex : 1,
        editor : {
            field : {
                allowBlank : false
            }
        }
    });
}

cargoGridColumns.push({
        text : appI18N.cargos.responsableActa,
        dataIndex : 'responsableActa',
        flex : 1,
        renderer : function(value, meta, rec)
        {
            return value ? 'Sí' : 'No';
        },
        editor : {
            xtype : 'checkbox',
            allowBlank : false
        }
    });

cargoGridColumns.push({
    text : appI18N.cargos.suplente,
    dataIndex : 'suplente',
    flex : 1,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

Ext.define('goc.view.cargo.Grid', {
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.cargoGrid',

    requires : [
        'goc.store.Cargos'
    ],

    store : {
        type : 'cargos'
    },

    title : appI18N.cargos.titulo,
    scrollable : true,

    columns : cargoGridColumns
});
