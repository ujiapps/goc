var items = [];

items.push({
    name: 'puntoOrdenDia',
    xtype: 'combo',
    emptyText: getMultiLangLabel(appI18N.reuniones.seleccionarPunto, mainLanguage),
    triggerAction: 'all',
    queryMode: 'local',
    displayField: 'tituloCompuesto',
    valueField: 'id',
    bind: {
        store: '{puntosOrdenDiaStore}'
    },
    listeners: {
        change: 'onPuntoChanged'
    }
})
;

if(isMultilanguageApplication()) {
    items.push(
        {
            xtype: 'tabpanel',
            height: '100%',
            bodyPadding: '10 20 10 20',
            listeners: {
                tabchange: 'onTabChange'
            },
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    title: mainLanguageDescription,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'ujitextareafield',
                            name: 'deliberaciones',
                            flex: 4,
                            emptyText: getMultiLangLabel(appI18N.reuniones.deliberaciones, mainLanguage),
                            bind: {
                                value: '{deliberaciones}',
                                disabled: '{completada}'
                            }
                        },
                        {
                            xtype: 'ujitextareafield',
                            name: 'acuerdos',
                            flex: 2,
                            emptyText: getMultiLangLabel(appI18N.reuniones.acuerdos, mainLanguage),
                            bind: {
                                value: '{acuerdos}',
                                disabled: '{completada}'
                            }
                        }
                    ]
                },
                {
                    title: alternativeLanguageDescription,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'ujitextareafield',
                            name: 'deliberacionesAlternativas',
                            flex: 4,
                            emptyText: getMultiLangLabel(appI18N.reuniones.deliberaciones, alternativeLanguage),
                            bind: {
                                value: '{deliberacionesAlternativas}',
                                disabled: '{completada}'
                            }
                        },
                        {
                            xtype: 'ujitextareafield',
                            name: 'acuerdosAlternativos',
                            flex: 2,
                            emptyText: getMultiLangLabel(appI18N.reuniones.acuerdos, alternativeLanguage),
                            bind: {
                                value: '{acuerdosAlternativos}',
                                disabled: '{completada}'
                            }
                        }
                    ]
                }
            ]
        }
    );
}
else
{
    items.push({
        xtype: 'ujitextareafield',
        name: 'deliberaciones',
        flex: 4,
        emptyText: getMultiLangLabel(appI18N.reuniones.deliberaciones, mainLanguage),
        bind: {
            value: '{deliberaciones}',
            disabled: '{completada}'
        }
    });

    items.push({
        xtype: 'ujitextareafield',
        name: 'acuerdos',
        flex: 2,
        emptyText: getMultiLangLabel(appI18N.reuniones.acuerdos, mainLanguage),
        bind: {
            value: '{acuerdos}',
            disabled: '{completada}'
        }
    });

}


Ext.define('goc.view.reunion.FormOrdenDiaConjuntos', {
    extend: 'Ext.window.Window',
    xtype: 'formOrdenDiaConjuntos',

    width: '90%',
    height: '90%',
    modal: true,
    bodyPadding: 10,
    autoScroll: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    requires: [
        'goc.view.reunion.FormOrdenDiaConjuntosController', 'goc.store.PuntosOrdenDia'
    ],
    controller: 'formOrdenDiaConjuntosController',

    bbar: {
        defaultButtonUI: 'default',
        items: [
            '->', {
                xtype: 'button',
                text: appI18N.reuniones.guardar,
                handler: 'onSaveRecord',
                bind: {
                    disabled: '{puntosOrdenDia.length <= 0}'
                }
            }, {
                xtype: 'button',
                text: appI18N.reuniones.guardarYContinuar,
                handler: 'onOnlySaveRecord',
                bind: {
                    disabled: '{puntosOrdenDia.length <= 0}'
                }
            }, {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cancelar + '</a>',
                listeners: {
                    render: function (component) {
                        component.getEl().on('click', 'onCancel');
                    }
                }
            }
        ]
    },

    bind: {
        title: '{title}'
    },

    items: items,

    listeners: {
        render: 'onLoad'
    }
});
