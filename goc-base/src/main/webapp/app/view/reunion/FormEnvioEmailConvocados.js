Ext.define('goc.view.reunion.FormEnvioEmailConvocados',
    {
        extend : 'Ext.window.Window',
        xtype : 'formEnvioEmailConvocados',
        title: appI18N.reuniones.enviarEmail,
        modal : true,
        bodyPadding : 10,
        layout : 'fit',

        requires : ['goc.view.reunion.FormEnvioEmailConvocadosController'],
        controller : 'formEnvioEmailConvocadosController',

        bbar : {
            defaultButtonUI : 'default',
            items: [
                '->',
                {
                    xtype : 'button',
                    text : appI18N.reuniones.enviar,
                    handler : 'onEnviarEmail'
                }, {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'onClose');
                        }
                    }
                }
            ]
        },

        items : [
            {
                xtype: 'textarea',
                fieldLabel : appI18N.reuniones.mensaje,
                width: 500,
                allowBlank: false,
                height: 200,
                name : 'cuerpoEmail'
            }
        ]
    });