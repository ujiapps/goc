Ext.define('goc.view.reunion.FormMotivoEdicionController',{
	
	extend:'Ext.app.ViewController',
	alias : 'controller.formMotivoEdicionController',
	
	  onClose : function()
        {
            var win = Ext.WindowManager.getActive();
            if (win)
            {
                win.destroy();
            }
        },
        
        onEnviarMotivoEdicion:function(button,event){		
			var vm = this.getViewModel()
			var motivo = vm.get('motivoEdicion');
			
			if(motivo != null && motivo != '') {
				button.disable()
				var reunionId = vm.get('reunionId')
				var ref = this;
				var record = vm.get('record')
				
				Ext.Ajax.request({
					timeout: 180000,
					url:'rest/reuniones/'+ reunionId+'/documentos/'+record.get('id')+'/motivoEdicion',
					method:'PUT',
					jsonData:{motivoEdicion:motivo},
					success:function(){
						ref.onClose()
						
						Ext.Ajax.request({
							timeout: 180000,
							url:'rest/reuniones/'+ reunionId + '/documentos/' + record.get('id') + '/guardarModificacionDocumento',
							method:'POST',
							jsonData:{motivoEdicion:motivo}
						})
					}
				})
			} else {
				return Ext.Msg.alert(appI18N.common.edicionRegistro, appI18N.reuniones.faltaMotivoEdicion);
			}
		}
})
