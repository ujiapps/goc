Ext.define('goc.view.reunion.FormOrdenDiaConjuntosController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.formOrdenDiaConjuntosController',

    idSeleccionado: null,

    init : function() {
        Ext.ux.uji.form.ChangedField.disable();
    },

    onLoad: function () {
        var store = this.getViewModel().getStore('puntosOrdenDiaStore');
        store.getProxy().url = '/goc/rest/reuniones/' + this.getViewModel().get('reunionId') + '/puntosOrdenDia';
        store.load();
    },

    onClose: function () {
        var win = Ext.WindowManager.getActive();
        var grid = Ext.ComponentQuery.query('treepanel[name=ordenDia]')[0];
        grid.getStore().reload();

        Ext.ux.uji.form.ChangedField.disable();

        if (win) {
            win.destroy();
        }
    },

    onCancel: function () {
        this.onClose();
    },

    inicializaPuntosGuardadosCheck: function () {
        var arrChecks = {};
        var store = this.getViewModel().getStore('puntosOrdenDiaStore');
        store.getData().each(function (punto) {
           arrChecks[punto.id] = false;
        });
        return arrChecks;
    },

    allPuntosGuardados: function (arrChecks) {
        for (var i=0; i < arrChecks.length; i++) {
            if (!arrChecks[i])
                return false;
        }
        return true;
    },

    onSaveRecord: function (closeForm) {
        var vm = this.getViewModel(),
            view = this.getView(),
            ref = this;

        this.saveTemporalData(this.idSeleccionado);
        view.setLoading(appI18N.common.loading);

        var store = vm.getStore('puntosOrdenDiaStore');
        var puntosGuardadosCheck = this.inicializaPuntosGuardadosCheck();

        store.getData().each(function (punto) {
            Ext.Ajax.request({
                url: '/goc/rest/reuniones/' + vm.get('reunionId') + '/puntosOrdenDia/' + punto.id + '/acuerdosydeliberaciones',
                method: 'PUT',
                jsonData: punto.data,
                success: function (data) {
                    puntosGuardadosCheck[punto.id] = true;
                    if (ref.allPuntosGuardados(puntosGuardadosCheck)) {
                        view.setLoading(false);
                        Ext.ux.uji.form.ChangedField.disable();
                        if (closeForm == null || closeForm !== false)
                            ref.onClose();
                    }
                },
                failure: function (err) {
                    view.setLoading(false);
                }
            });
        });
    },

    onOnlySaveRecord: function () {
        this.onSaveRecord(false);
    },

    onPuntoChanged: function (object, newId, oldId) {
        this.idSeleccionado = newId;
        this.saveTemporalData(oldId);
        var vm = this.getViewModel();
        var store = vm.getStore('puntosOrdenDiaStore');
        var punto = store.getById(newId);
        vm.set('acuerdos', punto.get('acuerdos'));
        vm.set('deliberaciones', punto.get('deliberaciones'));
        vm.set('acuerdosAlternativos', punto.get('acuerdosAlternativos'));
        vm.set('deliberacionesAlternativas', punto.get('deliberacionesAlternativas'));
    },

    saveTemporalData: function (id) {
        if (id != null) {
            var vm = this.getViewModel();
            var store = vm.getStore('puntosOrdenDiaStore');
            var punto = store.getById(id);
            punto.set('acuerdos', vm.get('acuerdos'));
            punto.set('deliberaciones', vm.get('deliberaciones'));
            punto.set('acuerdosAlternativos', vm.get('acuerdosAlternativos'));
            punto.set('deliberacionesAlternativas', vm.get('deliberacionesAlternativas'));
        }
    }
});