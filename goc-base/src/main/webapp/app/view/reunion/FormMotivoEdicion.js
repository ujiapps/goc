Ext.define('goc.view.reunion.FormMotivoEdicion',
{
	extend:'Ext.window.Window',
	xtype:'formMotivoEdicion',
	title:appI18N.reuniones.motivoEdicion,
	bodyPadding:10,
	modal:true,
	layout:'fit',
	closable: false,
	
	requires:['goc.view.reunion.FormMotivoEdicionController'],
	controller:'formMotivoEdicionController',
	
	   bbar : {
            defaultButtonUI : 'default',
            items: [
                '->',
                {
                    xtype : 'button',
                    text : appI18N.reuniones.enviarMotivo,
                    handler : 'onEnviarMotivoEdicion'
                }
            ]
        },
	items : [
            {
                xtype: 'textarea',
                allowBlank:false,
                enforceMaxLength:true,
                maxLength:250,
                fieldLabel : appI18N.reuniones.textoAdicional,
                width: 500,
                height: 200,
                name : 'motivoEdicion',
                bind : {
                    value : '{motivoEdicion}'
                }
            }
        ]
})