Ext.define('goc.view.reunion.FormTextoInformarOtrosOrganosController',
	{
		extend: 'Ext.app.ViewController',
		alias: 'controller.formTextoInformarOtrosOrganosController',


		onLoad:function(){
			viewModel.get('organosStore').load({
			url: '/goc/rest/organos/activos/usuario'
			});
		},
		onClose: function() {
			var win = Ext.WindowManager.getActive();
			if (win) {
				win.destroy();
			}

		},


		onEnviarTexto: function() {
		  	var viewModel = this.getViewModel();
            var record = viewModel.get('record');
       
            var ref = this;
            var formData = viewModel.get("formData")
           
                var organo = viewModel.get('organo')
                var organoExterno = organo.get('externo');
                var organoId = Transforms.getOrganoId(organo.get('id'), organoExterno);
                Ext.Ajax.request(
                    {
                        url : '/goc/rest/reuniones/' + record.get('id') + '/informarOrgano?externo=' + organoExterno,
                        method : 'PUT',
                        jsonData: {organoId: organoId, enviarOrden: formData.enviarOrden || false,textoInformar:viewModel.get('textoInformar'),
                        textoInformarAlternativo:viewModel.get('textoInformarAlternativo')},
                        success : function(response)
                        {
                            var data = Ext.decode(response.responseText);
                            var mensajeRespuesta = (data.message && data.message.indexOf("appI18N") != -1) ? eval(data.message) : data.message;
                            ref.onClose();
                            Ext.Msg.alert(appI18N.reuniones.resultadoEnvioConvocatoriaTitle, mensajeRespuesta);
                        },
                        scope : this
                    }
                );
            
		}

	});
