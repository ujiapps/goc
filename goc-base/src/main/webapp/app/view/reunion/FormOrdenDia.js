var formItems = [
    {
        name : 'tipoVoto',
        bodyPadding : 10,
        xtype : 'radiogroup',
        fieldLabel : appI18N.reuniones.ambitoVotacion,
        columns : 1,
        simpleValue : true,
        vertical : true,
        items : [
            {
                boxLabel : appI18N.reuniones.ambitoVotacionAbierta,
                inputValue : 'true',
                id : 'checkVotacionAbiertaTrue'
            },
            {
                boxLabel : appI18N.reuniones.ambitoVotacionSecreta,
                inputValue : 'false',
                id : 'checkVotacionAbiertaFalse'
            },
            {
                boxLabel : appI18N.reuniones.ambitoVotacionIndefinida,
                inputValue : null,
                id : 'checkVotacionAbiertaNull'
            },
        ],
        bind : {
            value : '{puntoOrdenDia.tipoVoto}',
            disabled : '{reunionCompletada}',
            hidden : '{!reunion.votacionTelematica}'
        },
        listeners : {
            change : 'onSetAmbitoVotacion'
        }
    }
];

formItems.push({
    name : 'tipoVotacion',
    bodyPadding : 10,
    xtype : 'radiogroup',
    fieldLabel : appI18N.reuniones.tipoVotacion,
    columns : 1,
    simpleValue : true,
    vertical : true,
    items : [
        {
            boxLabel : appI18N.reuniones.tipoVotacionTelematica,
            inputValue : 'TELEMATICA',
            id : 'checkTipoVotacionTelematica',
            checked: true
        },
        {
            boxLabel : appI18N.reuniones.tipoVotacionPresencial,
            inputValue : 'PRESENCIAL',
            id : 'checkTipoVotacionPresencial',
        }
    ],
    bind : {
        value : '{puntoOrdenDia.tipoVotacion}',
        disabled : '{reunionCompletada}',
        hidden : '{!reunion.votacionTelematica || puntoOrdenDia.tipoVoto == null}'
    }
});

formItems.push({
    name : 'tipoProcedimientoVotacion',
    bodyPadding : 10,
    xtype : 'radiogroup',
    fieldLabel : appI18N.procedimientosVotacion.tipoProcedimientoVotacion,
    columns : 1,
    simpleValue : true,
    vertical : true,
    items : [
        {
            boxLabel : appI18N.procedimientosVotacion.abreviado,
            inputValue : 'ABREVIADO',
            id : 'checkProcedimientoAbreviado',
            boxLabelAttrTpl : "data-qtip=" + appI18N.procedimientosVotacion.abreviadoTooltip
        },
        {
            boxLabel : appI18N.procedimientosVotacion.ordinario,
            inputValue : 'ORDINARIO',
            id : 'checkProcedimientoOrdinario',
            boxLabelAttrTpl : "data-qtip=" + appI18N.procedimientosVotacion.ordinarioTooltip
        }
    ],
    bind : {
        value : '{puntoOrdenDia.tipoProcedimientoVotacion}',
        disabled : '{reunionCompletada}',
        hidden : '{!reunion.votacionTelematica || puntoOrdenDia.tipoVoto == null}'
    }
});

formItems.push({
    name : 'tipoRecuentoVoto',
    bodyPadding : 10,
    xtype : 'radiogroup',
    fieldLabel : appI18N.procedimientosVotacion.tipoRecuentoVotacion,
    columns : 1,
    simpleValue : true,
    vertical : true,
    items : [
        {
            boxLabel : appI18N.procedimientosVotacion.mayoriaSimple,
            inputValue : 'MAYORIA_SIMPLE',
            id : 'checkMayoriaSimple'
        },
        {
            boxLabel : appI18N.procedimientosVotacion.mayoriaAbsolutaPresentes,
            inputValue : 'MAYORIA_ABSOLUTA_PRESENTES',
            id : 'checkMayoriaAbsolutaPresentes'
        },
       {
           boxLabel : appI18N.procedimientosVotacion.mayoriaAbsolutaOrgano,
           inputValue : 'MAYORIA_ABSOLUTA_ORGANO',
           id : 'checkMayoriaAbsolutaOrgano'
       },
       {
		   boxLabel : appI18N.procedimientosVotacion.mayoriaDosTercios,
           inputValue : 'MAYORIA_DOS_TERCIOS',
		   id : 'checkMayoriaDosTercios'
	   }
    ],
    bind : {
        value : '{puntoOrdenDia.tipoRecuentoVoto}',
        disabled : '{reunionCompletada}',
        hidden : '{!reunion.votacionTelematica || puntoOrdenDia.tipoVoto == null}'
    }
});

formItems.push({
    xtype : 'fieldcontainer',
    fieldLabel : appI18N.reuniones.opciones,
    layout : 'hbox',
    bodyPadding : 10,
    items : [
        {
            boxLabel : appI18N.reuniones.votoEnContra,
            name : 'votableEnContra',
            padding : '0 0 0 4',
            bind : {
                value : '{puntoOrdenDia.votableEnContra}',
            },
            checked : true,
            xtype : 'checkbox'
        }
    ],
    bind : {
        disabled : '{reunionCompletada}',
        hidden : '{!reunion.votacionTelematica || puntoOrdenDia.tipoVoto == null}'
    }
});

formItems.push(
{
    name : 'id',
    xtype : 'hidden',
    bind : '{puntoOrdenDia.id}'
},
{
    allowBlank : false,
    fieldLabel : getMultiLangLabel(appI18N.reuniones.titulo, mainLanguage),
    labelWidth : 130,
    xtype : 'ujitextareafield',
    grow : true,
    growMax : 20,
    minHeight : 20,
    maxHeight : 20,
    height : 20,
    labelAlign : 'top',
    name : 'titulo',
    emptyText : getMultiLangLabel(appI18N.reuniones.titulo, mainLanguage),
    bind : {
        value : '{puntoOrdenDia.titulo}',
        disabled : '{reunionCompletada}'
    }
}
);

if (isMultilanguageApplication())
{
    formItems.push({
        allowBlank : false,
        fieldLabel : getMultiLangLabel(appI18N.reuniones.titulo, alternativeLanguage),
        labelWidth : 130,
        xtype : 'ujitextareafield',
        grow : true,
        growMax : 20,
        minHeight : 20,
        maxHeight : 20,
        height : 20,
        labelAlign : 'top',
        name : 'tituloAlternativo',
        emptyText : getMultiLangLabel(appI18N.reuniones.titulo, alternativeLanguage),
        bind : {
            value : '{puntoOrdenDia.tituloAlternativo}',
            disabled : '{reunionCompletada}'
        }
    });
}

formItems.push({
    xtype : 'container',
    layout : 'hbox',
    items : [
        {
            xtype : 'tbfill',
            flex : 1
        },
        {
            boxLabel : appI18N.reuniones.publicarAcuerdos,
            name : 'publico',
            bind : {
                disabled : '{reunionCompletada}',
                value : '{puntoOrdenDia.publico}'
            },
            xtype : 'checkbox',
            inputValue : '1'
        }
    ]
});

formItems.push({
    xtype : 'ujitextareafield',
    name : 'descripcion',
    fieldLabel : getMultiLangLabel(appI18N.reuniones.descripcion, mainLanguage),
    labelAlign : 'top',
    flex : 1,
    emptyText : getMultiLangLabel(appI18N.reuniones.descripcion, mainLanguage),
    bind : {
        value : '{puntoOrdenDia.descripcion}',
        disabled : '{reunionCompletada}'
    }
});

if (isMultilanguageApplication())
{
    formItems.push({
        xtype : 'ujitextareafield',
        name : 'descripcionAlternativa',
        fieldLabel : getMultiLangLabel(appI18N.reuniones.descripcion, alternativeLanguage),
        labelAlign : 'top',
        flex : 1,
        emptyText : getMultiLangLabel(appI18N.reuniones.descripcion, alternativeLanguage),
        bind : {
            value : '{puntoOrdenDia.descripcionAlternativa}',
            disabled : '{reunionCompletada}'
        }
    });
}

formItems.push({
    xtype : 'ujitextareafield',
    name : 'deliberaciones',
    fieldLabel : getMultiLangLabel(appI18N.reuniones.deliberaciones, mainLanguage),
    labelAlign : 'top',
    flex : 1,
    emptyText : getMultiLangLabel(appI18N.reuniones.deliberaciones, mainLanguage),
    bind : {
        value : '{puntoOrdenDia.deliberaciones}',
        disabled : '{reunionCompletada}'
    }
});

if (isMultilanguageApplication())
{
    formItems.push({
        xtype : 'ujitextareafield',
        name : 'deliberacionesAlternativas',
        fieldLabel : getMultiLangLabel(appI18N.reuniones.deliberaciones, alternativeLanguage),
        labelAlign : 'top',
        flex : 1,
        emptyText : getMultiLangLabel(appI18N.reuniones.deliberaciones, alternativeLanguage),
        bind : {
            value : '{puntoOrdenDia.deliberacionesAlternativas}',
            disabled : '{reunionCompletada}'
        }
    });
}

formItems.push({
    xtype : 'ujitextareafield',
    name : 'acuerdos',
    fieldLabel : getMultiLangLabel(appI18N.reuniones.acuerdos, mainLanguage),
    labelAlign : 'top',
    flex : 1,
    emptyText : getMultiLangLabel(appI18N.reuniones.acuerdos, mainLanguage),
    bind : {
        value : '{puntoOrdenDia.acuerdos}',
        disabled : '{reunionCompletada}'
    }
});

if (isMultilanguageApplication())
{
    formItems.push({
        xtype : 'ujitextareafield',
        name : 'acuerdosAlternativos',
        fieldLabel : getMultiLangLabel(appI18N.reuniones.acuerdos, alternativeLanguage),
        labelAlign : 'top',
        flex : 1,
        emptyText : getMultiLangLabel(appI18N.reuniones.acuerdos, alternativeLanguage),
        bind : {
            value : '{puntoOrdenDia.acuerdosAlternativos}',
            disabled : '{reunionCompletada}'
        }
    });
}

Ext.define('goc.view.reunion.FormOrdenDia', {
    extend : 'Ext.window.Window',
    xtype : 'formOrdenDia',

    width : '90%',
    maxHeight : 1000,
    modal : true,
    bodyPadding : 10,
    autoScroll : true,

    layout : {
        type : 'vbox',
        align : 'stretch'
    },

    requires : [
        'goc.view.reunion.FormOrdenDiaController'
    ],
    controller : 'formOrdenDiaController',

    bbar : {
        defaultButtonUI : 'default',
        items : [
            '->', {
                xtype : 'button',
                bind : {
                    disabled : '{reunionCompletada}'
                },
                text : appI18N.reuniones.guardar,
                handler : 'onSaveRecord'
            }, {
                xtype : 'panel',
                html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cancelar + '</a>',
                listeners : {
                    render : function(component)
                    {
                        component.getEl().on('click', 'onCancel');
                    }
                }
            }
        ]
    },

    bind : {
        title : '{title}'
    },

    items : [
        {
            xtype : 'form',
            name : 'puntoOrdenDia',
            border : 0,
            layout : 'anchor',
            items : [
                {
                    xtype : 'fieldset',
                    title : appI18N.reuniones.informacion,
                    defaultType : 'textfield',
                    defaults : {
                        anchor : '100%'
                    },

                    items : formItems
                }
            ]
        }
    ],

    listeners : {
        afterLayout : 'afterRenderFormOrdenDia',
        close : 'onClose'
    }
});
