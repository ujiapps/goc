Ext.define('goc.view.reunion.FormOrdenDiaDocumentacionYAdjuntosController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.formOrdenDiaDocumentacionYAdjuntosController',

        onLoad: function () {
            var viewModel = this.getViewModel();
            var reunionId = viewModel.get('reunionId');
            var puntoOrdenDiaId = viewModel.get('puntoOrdenDiaId');

            if (reunionId) {
                viewModel.get('store').load({
                    url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/documentacionyadjuntos'
                });
            }
        },

        onClose: function () {
            var win = Ext.WindowManager.getActive();
            var grid = Ext.ComponentQuery.query('treepanel[name=ordenDia]')[0];
            grid.getStore().reload();

            if (win) {
                win.destroy();
            }
        },

        onSelect: function () {
            var record = this.getView().down('grid').getSelection()[0];
            if (record != null) {
                var inputDescripcion = this.getView().lookupReference('descripcion');
                var inputDescripcionAlternativa = this.getView().lookupReference('descripcionAlternativa');
                inputDescripcion.setValue(record.get('descripcion'));
                inputDescripcion.disable();
                if (isMultilanguageApplication()) {
                    inputDescripcionAlternativa.setValue(record.get('descripcionAlternativa'));
                    inputDescripcionAlternativa.disable();
                }
                var inputTipo = this.getView().lookupReference('tipo');
                inputTipo.setValue(record.get('tipo'));
                inputTipo.disable();
                var form = this.getView().down('form');
                form.setTitle(appI18N.reuniones.subirNuevoDocumentoTituloFormulario);
                var btnSubirArchivo = this.getView().lookupReference('btnSubirArchivo');
                btnSubirArchivo.setText(appI18N.reuniones.subirNuevaVersion);
                var inputIdDoc = form.down('hiddenfield');
                inputIdDoc.setValue(record.get('id'));
            }
        },

        borraPuntoOrdenDiaDocumento: function (adjuntoId, tipoAdjunto) {
            var viewModel = this.getViewModel();
            var reunionId = viewModel.get('reunionId');
            var puntoOrdenDiaId = viewModel.get('puntoOrdenDiaId');

            Ext.Msg.confirm(appI18N.common.borrar, appI18N.reuniones.preguntaBorrarDocumento, function (result) {
                if (result === 'yes') {
                    Ext.Ajax.request({
                        url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + adjuntoId,
                        headers: {'Content-Type': 'application/json'},
                        method: 'DELETE',
                        success: function () {
                            viewModel.get('store').reload();
                        }
                    });
                }
            });
        },

        inhabilitaPuntoOrdenDiaDocumento: function (adjuntoId, tipoAdjunto) {
            var viewModel = this.getViewModel();
            var view = this.getView();
            var reunionId = viewModel.get('reunionId');
            var puntoOrdenDiaId = viewModel.get('puntoOrdenDiaId');

            var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
            var record = reunionGrid.getView().getSelectionModel().getSelection()[0];
            var enviada = record.get('avisoPrimeraReunion');

            Ext.Msg.confirm(appI18N.common.borrar, appI18N.reuniones.preguntaBorrarDocumento, function (result) {
                if (result === 'yes') {
                    Ext.Ajax.request({
                        url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + adjuntoId + '/inhabilitar',
                        headers: {'Content-Type': 'application/json'},
                        method: 'DELETE',
                        success: function () {
                            viewModel.get('store').reload();

                            if (enviada) {
                                var viewport = view.up('viewport')
                                this.modal = viewport.add({
                                    xtype: 'formMotivoEdicionAdjuntosYAcuerdos',
                                    viewModel: {
                                        data: {
                                            reunionId: reunionId,
                                            puntoOrdenDiaId: puntoOrdenDiaId,
                                            tipoAdjunto: tipoAdjunto,
                                            adjuntoId: adjuntoId
                                        }
                                    }
                                })
                                this.modal.show()
                                return;
                            }

                            Ext.Ajax.request({
                                timeout: 180000,
                                url: 'rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + adjuntoId + '/guardarModificacionDocumento',
                                method: 'POST',
                                jsonData: {motivoEdicion: "No disponible (convocatoria no enviada)"}
                            })
                        }
                    });
                }
            });
        },

        descargaPuntoOrdenDiaDocumento: function (documentoId, tipoAdjunto) {
            var viewModel = this.getViewModel();
            var reunionId = viewModel.get('reunionId');
            var puntoOrdenDiaId = viewModel.get('puntoOrdenDiaId');
            var url = '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipoAdjunto + '/' + documentoId + '/descargar';

            var body = Ext.getBody();
            var frame = body.createChild({
                tag: 'iframe',
                cls: 'x-hidden',
                name: 'iframe'
            });

            var form = body.createChild({
                tag: 'form',
                cls: 'x-hidden',
                action: url,
                target: 'iframe'
            });
            form.dom.submit();
        },

        subirPuntoOrdenDiaAdjunto: function () {
            var viewModel = this.getViewModel();
            var reunionId = viewModel.get('reunionId');
            var puntoOrdenDiaId = viewModel.get('puntoOrdenDiaId');
            var view = this.getView();
            var form = view.down('form[name=subirDocumento]');
            var grid = view.down('grid');
            var descripcionForm = form.down('textfield[name=descripcion]');
            var descripcionAlternativaForm = form.down('textfield[name=descripcionAlternativa]');
            var nombreDocumento = form.down('displayfield[name=nombreDocumento]').getValue();
            var combo = Ext.getCmp('tipoAdjuntoSubida');
            var tipo = combo.getValue();
            var inputIdDoc = form.down('hiddenfield');

            var url = '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipo + '/';
            var showConfirm = false;
            var confirmado = false;

            var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
            var record = reunionGrid.getView().getSelectionModel().getSelection()[0];
            var enviada = record.get('avisoPrimeraReunion');

            var idDoc = parseInt(inputIdDoc.getValue());
            if (inputIdDoc.getValue() != null && typeof idDoc === 'number' && idDoc > 0) {
                url = '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipo + '/' + idDoc;
                showConfirm = true;
            }

            if (form.down('filefield[name=documento]').getValue() !== "") {
                if (form.getForm().isValid()) {
                    if (showConfirm) {
                        if (confirm(appI18N.reuniones.subirNuevaVersionConfirm)) {
                            confirmado = true;
                        }
                    }

                    if ((showConfirm && confirmado) || !showConfirm) {
                        if (descripcionForm.getValue() === "") {
                            descripcionForm.setValue(nombreDocumento.split(".")[0])
                        }

                        if (isMultilanguageApplication() && descripcionAlternativaForm.getValue() === "") {
                            descripcionAlternativaForm.setValue(nombreDocumento.split(".")[0])
                        }


                        form.submit({
                            url: url,
                            scope: this,
                            submitEmptyText: false,
                            success: function () {
                                viewModel.get('store').reload();
                                grid.setSelection(null);
                                this.limpiarFormulario();
                            }
                        });
                    }

                    if (confirmado) {
                        viewModel.get('store').reload();

                        if (enviada) {
                            var viewport = view.up('viewport')
                            this.modal = viewport.add({
                                xtype: 'formMotivoEdicionAdjuntosYAcuerdos',
                                viewModel: {
                                    data: {
                                        reunionId: reunionId,
                                        puntoOrdenDiaId: puntoOrdenDiaId,
                                        tipoAdjunto: tipo,
                                        adjuntoId: idDoc
                                    }
                                }
                            })
                            this.modal.show();
                            return;
                        }

                        Ext.Ajax.request({
                            timeout: 180000,
                            url: 'rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + tipo + '/' + idDoc + '/guardarModificacionDocumento',
                            method: 'POST',
                            jsonData: {motivoEdicion: "No disponible (convocatoria no enviada)"}
                        })
                    }
                }
            } else {
                return Ext.Msg.alert(appI18N.reuniones.subirNuevoDocumento, appI18N.adjuntos.seleccionarDocumentoParaSubir);
            }
        },

        limpiarFormulario: function () {
            this.getView().down('grid').setSelection(null);
            this.getView().down('form').reset();
            var inputDescripcion = this.getView().lookupReference('descripcion');
            inputDescripcion.enable();
            if (isMultilanguageApplication()) {
                var inputDescripcionAlternativa = this.getView().lookupReference('descripcionAlternativa');
                inputDescripcionAlternativa.enable();
            }
            var inputTipo = this.getView().lookupReference('tipo');
            inputTipo.enable();
            var form = this.getView().down('form');
            form.setTitle(appI18N.reuniones.subirNuevoDocumento);
            var btnSubirArchivo = this.getView().lookupReference('btnSubirArchivo');
            btnSubirArchivo.setText(appI18N.reuniones.subirDocumento);
        },

//        onEdit : function(elem, rowIndex, checked, record)
//        {
//            var viewModel = this.getViewModel();
//            var reunionId = viewModel.get('reunionId');
//            var puntoOrdenDiaId = viewModel.get('puntoOrdenDiaId');
//            var documentoId = record.get("id");
//            var tipo = record.get("tipo");
//			var grid = this.getView().down('grid');
////            Ext.Ajax.request({
////                url : '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/'+ tipo +'/' + documentoId,
////                method : 'PUT',
////                jsonData : {
////                   publico: checked
////                },
////                success : function()
////                {
////                    viewModel.get('store').reload();
////                }
////            });
//			
//     },

        onBeforeEditComplete: function (editor, context) {
            var inputDescripcion = this.getView().lookupReference('descripcion');
            var inputDescripcionAlternativa = this.getView().lookupReference('descripcionAlternativa');
            var viewModel = this.getViewModel();
            var reunionId = viewModel.get('reunionId');
            var puntoOrdenDiaId = viewModel.get('puntoOrdenDiaId');

            var grid = this.getView().down('grid');
            var record = grid.getSelectedRow();

            var store = grid.getStore();
            store.proxy.url = '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/' + record.data.tipo;
            store.sync();
            inputDescripcion.setValue(record.get("descripcion"));
            if (isMultilanguageApplication()) {
                inputDescripcionAlternativa.setValue(record.get("descripcionAlternativa"));
            }
        },

        cancelAcuerdoCheckBox: function (editor, context, eOpts) {
            var record = context.record;
            var grid = this.getView().down('grid');
            var columns = grid.getColumns();

            var i;
            for (i = 0; i < columns.length; i++) {
                var column = columns[i];
                if (column.dataIndex == "mostrarEnFicha" || column.dataIndex == "publico") {
                    if (record.data.tipo == "ACUERDO") {
                        column.setEditor({
                            xtype: 'label'
                        });
                    } else {
                        column.setEditor({
                            xtype: 'checkbox'
                        });
                    }
                }
            }
        },
        onFileChangeOrdenDia: function (obj, value) {
            var filename = value.split(/(\\|\/)/g).pop(),
                labelNombreDocumento = this.getView().lookupReference('nombreDocumento');
            labelNombreDocumento.setValue(filename);
        },


        subeOrdenDocumento: function (documentoId, tipoDoc) {
            var viewModel = this.getViewModel();
            var reunionId = viewModel.get('reunionId');
            var puntoId = viewModel.get('puntoOrdenDiaId')
            Ext.Ajax.request({
                url: 'rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoId + '/documentacionyadjuntos/' + tipoDoc + '/subir/' + documentoId,
                method: 'PUT',
                success: function () {
                    viewModel.get('store').reload();
                }
            })
        },
        bajaOrdenDocumento: function (documentoId, tipoDoc) {
            var viewModel = this.getViewModel();
            var reunionId = viewModel.get('reunionId');
            var puntoId = viewModel.get('puntoOrdenDiaId')
            Ext.Ajax.request({
                url: 'rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoId + '/documentacionyadjuntos/' + tipoDoc + '/bajar/' + documentoId,
                method: 'PUT',
                success: function () {
                    viewModel.get('store').reload();
                }
            })
        },


    });
