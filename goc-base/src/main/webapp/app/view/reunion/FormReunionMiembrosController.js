Ext.define('goc.view.reunion.FormReunionMiembrosController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formReunionMiembrosController',
    onClose: function () {
        var win = this.getView();

        if (win) {
            win.destroy();
        }
    },

    onAddSuplente: function () {
        var grid = this.getView().down('grid');
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.addSuplente, appI18N.reuniones.seleccionarParaAnyadirSuplente);
        }

        var window = Ext.create('goc.view.common.LookupWindowPersonas', {
            appPrefix: 'goc',
            title: appI18N.reuniones.seleccionaSuplente
        });

        window.show();

        window.on('LookoupWindowClickSeleccion', function (res) {
            record.set('suplenteId', res.get('id'));
            record.set('suplenteNombre', res.get('nombre'));
            record.set('suplenteEmail', res.get('email'));
            record.set('asistencia', false);

            if (record.get('suplenteId') != null) {

                Ext.Ajax.request({
                    url: '/goc/rest/reuniones/' + record.get('reunionId') + '/organos/' + record.get('organoId') + '/miembros/' + record.get('id'),
                    method: 'PUT',
                    jsonData: {miembro: record.data}
                })
            }
        });
    },

    onRemoveSuplente: function () {
        var grid = this.getView().down('grid');
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.borrarSuplente, appI18N.reuniones.seleccionarParaBorrarSuplente);
        }
        record.set('suplenteId', null);
        record.set('suplenteNombre', null);
        record.set('suplenteEmail', null);
        record.set('asistencia', true);

        if (record.get('suplenteId') == null || record.get('suplenteId') == 0) {

            Ext.Ajax.request({
                url: '/goc/rest/reuniones/' + record.get('reunionId') + '/organos/' + record.get('organoId') + '/miembros/' + record.get('id'),
                method: 'PUT',
                jsonData: {miembro: record.data}
            })
        }
    },

    onLoad: function () {
        var view = this.getView()

        var modalDefinition = {
            xtype: 'organoReunionInvitadoGrid',
            title: appI18N.organos.invitadosOrganos,
            tbar: null,
            bbar: null,
            width: '50%',
            padding: '10 0 0 0',
        }
        var modalReunionInvitado = {
            xtype: 'invitadoGrid',
            title: appI18N.organos.invitadosReunion,
            width: '50%',
            padding: '10 0 0 0'
        }
        var gridMiembros = view.down('grid')
        gridMiembros.setTitle(appI18N.miembros.tituloGrid)
        this.modal = view.add(modalDefinition)
        this.modal = view.add(modalReunionInvitado)
        this.modal.show()

        var viewModel = this.getViewModel()

        var invitadoGrid = view.down('invitadoGrid')
        invitadoGrid.columns[2].setHidden(false)
        invitadoGrid.columns[3].destroy()
        invitadoGrid.columns[4].setHidden(false)

        var reunionId = viewModel.get('reunionId')
        viewModel.get('organosStore').load({
            params: {reunionId: reunionId},
            callback: function (records, options, success) {
                var controller = view.getController()
                var primerOrgano = records[0].data
                var organoId = Transforms.getOrganoId(primerOrgano.id, primerOrgano.externo);
                var externo = primerOrgano.externo
                var combo = view.down('combo')
                var nombre = (appLang === alternativeLanguage ? primerOrgano.nombreAlternativo : primerOrgano.nombre)
                combo.setValue(nombre)
                if (!controller.getStoreDeMiembros(organoId, externo)) {
                    controller.creaStoreDeMiembros(organoId, externo);
                }

                var delegadosResponse = Ext.Ajax.request({
                    url: '/goc/rest/votos/resultado/votacion/reunion/' + reunionId,
                    async: false,
                    method: 'GET'
                });

                var delegados = Ext.decode(delegadosResponse.responseText).data

                var store = controller.getStoreDeMiembros(organoId, externo)
                viewModel.set('organoId', organoId)
                viewModel.set('externo', externo)
                viewModel.set('store', store)
                viewModel.get('store').load({
                    url: '/goc/rest/reuniones/' + reunionId + '/miembros',
                    params: {organoId: Transforms.getOrganoId(organoId, externo), externo: externo},
                    callback: function (records) {
                        records.forEach(function (rec) {

                            for (var index in delegados) {
                                var nombreTitular = delegados[index].nombreTitular;
                                if (nombreTitular === rec.get('nombre')) {
                                    rec.set('asistenciaBloqueada', true);
                                } else {
                                    rec.set('asistenciaBloqueada', false);
                                }
                            }
                        });
                    }
                })

                var organoReunionInvitadosStore = viewModel.get('organoReunionInvitadosStore').load({
                    url: '/goc/rest/reuniones/' + reunionId + '/organos/' + organoId + '/invitados',
                    params: {externo: externo}
                })

                organoReunionInvitadosStore.filterBy(function (record, id) {
                    if (record.get('soloConsulta') == false) {
                        return true;
                    } else {
                        return false;
                    }
                })

            }
        })
        viewModel.get('reunionInvitadosStore').load({
            url: 'rest/reuniones/' + reunionId + '/invitados',
            method: 'GET'
        })
    },
    buildDatosInvitados: function (invitados) {
        var datosInvitados = [];

        invitados.each(function (record) {
            datosInvitados.push({
                personaId: record.get('personaId'),
                personaNombre: record.get('personaNombre'),
                personaEmail: record.get('personaEmail'),
                motivoInvitacion: record.get('motivoInvitacion'),
                asistencia: record.get('asistencia')
            });
        });

        return datosInvitados;
    },

    /*  buildDatosOrgano: function (organo)
    {
        var datosOrganos = [];
        var self = this;
        //organos.each(function (record)
        //{
            var miembros = [];
            if (!this.getStoreDeMiembros(organo.get('id'), organo.get('externo'))) {
				this.creaStoreDeMiembros(organo.get('id'), organo.get('externo'));
			}
			
            var miembrosStore = self.getStoreDeMiembros(organo.get('id'), organo.get('externo'));
            if (miembrosStore)
            {
		
                miembrosStore.getData().each(function (record)
                {
                    miembros.push({
                        id: record.get('id'),
                        email: record.get('email'),
                        nombre: record.get('nombre'),
                        cargo: record.get('cargo'),
                        asistencia: record.get('asistencia'),
                        suplenteId: record.get('suplenteId'),
                        suplenteNombre: record.get('suplenteNombre'),
                        suplenteEmail: record.get('suplenteEmail'),
                        justificaAusencia: record.get('justificaAusencia'),
                        motivoAusencia: record.get('motivoAusencia')
                    });
                });
            }
             var data = {
             	id: organo.get('id'),
            	externo: organo.get('externo'),
                miembros: miembros
            };
            datosOrganos.push(data);
        //});

        return miembros;

    },*/


    onSave: function () {
        /*var viewModel = this.getViewModel();
        var view = this.getView()
        var reunionId = viewModel.get('reunionId')
        var store = viewModel.get('reunionInvitadosStore').load()
        var storeData = store.data;

        var combo = view.down('combo')

        var selectedOrgano = combo.getSelectedRecord();
        var organoId = selectedOrgano.get('id');

        var datosInvitados = this.buildDatosInvitados(storeData)

        var datosOrgano = this.buildDatosOrgano(selectedOrgano)

        Ext.Ajax.request({
            url:'/goc/rest/reuniones/' + reunionId + '/organos/' + organoId,
            method:'PUT',
            jsonData:{miembrosOrgano:datosOrgano},
        })

        Ext.Ajax.request({
            url:'rest/reuniones/'+reunionId+'/invitados',
            method:'PUT',
            jsonData:{invitados:datosInvitados},
        })*/
        this.onClose();
    },

    onSearchMiembro: function (field, searchString) {
        var viewModel = this.getViewModel();
        var store = viewModel.get('store');

        if (!searchString) {
            store.clearFilter();
            return;
        }

        var filter = new Ext.util.Filter(
            {
                filterFn: function (item) {
                    var propertyNormalized = item.data.nombre.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
                    var valueNormalized = searchString.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
                    ;

                    return propertyNormalized.includes(valueNormalized);
                },
                anyMatch: true
            });

        store.addFilter(filter);
    },
    getStoreDeMiembros: function (organoId, externo) {
        var vm = this.getViewModel();
        var miembrosStores = vm.get('miembrosStore');
        return miembrosStores[organoId + '_' + externo];
    },

    creaStoreDeMiembros: function (organoId, externo) {
        var vm = this.getViewModel();
        var miembrosStores = vm.get('miembrosStore');
        miembrosStores[organoId + '_' + externo] = Ext.create('goc.store.OrganoReunionMiembros');
    },

    onSelectOrganos: function (_, organo) {
        var organoId = organo.get('id')
        var externo = organo.get('externo')
        var viewModel = this.getViewModel()

        var reunionId = viewModel.get('reunionId')
        if (!this.getStoreDeMiembros(organoId, externo)) {
            this.creaStoreDeMiembros(organoId, externo);
            remoteLoad = true;
        }
        var organoIdTra = Transforms.getOrganoId(organoId, externo)
        viewModel.get('store').load({
            url: '/goc/rest/reuniones/' + reunionId + '/miembros',
            params: {
                organoId: organoIdTra,
                externo: externo
            }
        })
        var organoReunionInvitadosStore = viewModel.get('organoReunionInvitadosStore')
        organoReunionInvitadosStore.load({
            url: '/goc/rest/reuniones/' + reunionId + '/organos/' + organoId + '/invitados',
            params: {externo: externo}
        })
        organoReunionInvitadosStore.filterBy(function (record, id) {
            if (record.get('soloConsulta') == false) {
                return true;
            } else {
                return false;
            }
        })
    },

    onChangeAsistencia: function (elem, rowIndex, checked, record) {

        if (checked && record.data.delegadoVotoId != null) {
            record.set("delegadoVotoNombre", "");
            record.set("delegadoVotoEmail", "");
            record.set("delegadoVotoId", null);

            Ext.Msg.alert(appI18N.alertas.alertaTitlBorrarDelegadoVoto, appI18N.alertas.alertaBorradoDelegadoVoto);
        }

        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + record.get('reunionId') + '/organos/' + record.get('organoId') + '/miembros/' + record.get('id'),
            method: 'PUT',
            jsonData: {miembro: record.data}
        })
    },

    onChangeJustificaAusencia: function (elem, rowIndex, checked, record) {

        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + record.get('reunionId') + '/organos/' + record.get('organoId') + '/miembros/' + record.get('id'),
            method: 'PUT',
            jsonData: {miembro: record.data}
        })
    }


});
