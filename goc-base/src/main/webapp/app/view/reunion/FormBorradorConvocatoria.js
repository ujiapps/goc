Ext.define('goc.view.reunion.FormBorradorConvocatoria',
{
	extend:'Ext.window.Window',
	xtype:'formBorradorConvocatoria',
	title:'Enviar borrador',
	modal:true,
	bodyPadding : 10,
    layout : 'vbox',
    requires : ['goc.view.reunion.FormBorradorConvocatoriaController'],
    controller : 'formBorradorConvocatoriaController',
    bbar:{
		defaultButtonUI : 'default',
		items:[
			'->',
			{
			xtype:'button',
			text:'Enviar',
			handler:'guardarCorreos'
			}]
    },
    items:[
	{
	xtype:'fieldset',
	defaultType:'textfield',
	},
	{
	 xtype : 'button',
     iconCls : 'fa fa-plus',
	 text : appI18N.common.anadir,
	handler:'anyadirCampos'	
	}
	],
	listeners:{
		render:'onLoad'
	}
	
});