Ext.define('goc.view.reunion.FormInformarOtrosOrganos',
    {
        extend : 'Ext.window.Window',
        xtype : 'formInformarOtrosOrganos',
        title: appI18N.reuniones.informarOtrosOrganos,
        modal : true,
        bodyPadding : 10,
        layout : 'fit',

        requires : ['goc.view.reunion.FormInformarOtrosOrganosController'],
        controller : 'formInformarOtrosOrganosController',

        bbar : {
            defaultButtonUI : 'default',
            items: [
                '->',
                {
                    xtype : 'button',
                    text : 'Informar',
                    handler : 'onInformarOrgano'
                }, {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'onClose');
                        }
                    }
                }
            ]
        },

        items : [
            {
                xtype : 'form',
                name : 'informarOtroOrgano',
                border : 0,
                layout : 'anchor',
                items : [
                    {
                        xtype : 'fieldset',
                        title : appI18N.reuniones.informacionBasica,
                        defaultType : 'textfield',
                        defaults : {
                            anchor : '100%'
                        },

                        items : [
                            {
                                xtype: 'combobox',
                                fieldLabel: appI18N.reuniones.organo,
                                displayField: 'nombre',
                                valueField: 'id',
                                name: 'organo',
                                editable: true,
                                forceSelection: true,
                                queryMode: 'local',
                                triggerAction: 'all',
                                bind: {
                                    store: '{organosStore}'
                                },
                                allowBlank: false,
                                listeners: {
                                    expand: function () {
                                        this.getStore().reload();
                                    }
                                },
                                width: 400
                            },
                            {
                                xtype:'checkbox',
                                name: 'enviarOrden',
                                boxLabel: appI18N.reuniones.enviarOrdenDia,
                                checked: true
                            }
                        ]
                    }
                ]
            }
        ],

        listeners: {
            render: 'onLoad'
        }
    });