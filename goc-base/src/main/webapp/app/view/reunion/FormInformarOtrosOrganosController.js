Ext.define('goc.view.reunion.FormInformarOtrosOrganosController',
    {
        extend : 'Ext.app.ViewController',
        alias : 'controller.formInformarOtrosOrganosController',

        onLoad: function() {
            var viewModel = this.getViewModel();
            viewModel.getStore('organosStore').load({
                url: '/goc/rest/organos/activos/usuario'
            });
        },

        onClose : function()
        {
            var win = Ext.WindowManager.getActive();
            if (win)
            {
                win.destroy();
            }
        },


        onInformarOrgano: function ()
        {
            var viewModel = this.getViewModel();
            var viewport = this.getView().up('viewport')  
            var record = viewModel.get('record');
            var organosStroe = viewModel.getStore('organosStore');
            var ref = this;
            var form = this.getView().down('form');
            var formData = form.getValues();
            if(form.isValid())
            {
                var organo = organosStroe.getById(formData.organo);
                var organoExterno = organo.get('externo');
                var organoId = Transforms.getOrganoId(organo.get('id'), organoExterno);
//                Ext.Ajax.request(
//                    {
//                        url : '/goc/rest/reuniones/' + record.get('id') + '/informarOrgano?externo=' + organoExterno,
//                        method : 'PUT',
//                        jsonData: {organoId: organoId, enviarOrden: formData.enviarOrden || false},
//                        success : function(response)
//                        {
//                            var data = Ext.decode(response.responseText);
//                            var mensajeRespuesta = (data.message && data.message.indexOf("appI18N") != -1) ? eval(data.message) : data.message;
//                            ref.onClose();
//                            Ext.Msg.alert(appI18N.reuniones.resultadoEnvioConvocatoriaTitle, mensajeRespuesta);
//                        },
//                        scope : this
//                    }
//                );
				ref.onClose()	
				this.modal = viewport.add({
					xtype:'formTextoInformarOtrosOrganos',
					viewModel:{
						data:{
							record:record,
							organo:organo,
							formData:formData
						}
					}
				})
				this.modal.show()
            }

        }
    });
