Ext.define('goc.view.reunion.ViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.reunionViewModel',
    requires: [
		'goc.store.ReunionInvitados',
		'goc.store.OrganoInvitados',
		'goc.store.Miembros',
        'goc.store.PuntosOrdenDiaTree',
        'goc.store.Reuniones',
        'goc.store.ReunionDocumentos',
        'goc.store.PuntosOrdenDia',
        'goc.store.PuntosOrdenDiaDocumentos',
        'goc.store.PuntosOrdenDiaAcuerdos',
        'goc.store.OrganoReunionMiembros',
        'goc.store.TipoOrganos',
        'goc.store.Organos',
        'goc.store.Personas',
        'goc.store.OrganoReunionInvitados'
    ],
    stores: {
		organoInvitadosStore:{
			type:'organoInvitados'
		},
		miembrosStore:{
			type:'miembros'
		},
        reunionesStore: {
            type: 'reuniones',
            remoteFilter: true
        },
        reunionDocumentosStore: {
            type: 'reunionDocumentos'
        },
        puntosOrdenDiaStore: {
            type: 'puntosOrdenDia'
        },
        puntosOrdenDiaDocumentosStore: {
            type: 'puntosOrdenDiaDocumentos'
        },
        puntosOrdenDiaAcuerdosStore: {
            type: 'puntosOrdenDiaAcuerdos'
        },
        tipoOrganosStore: {
            type: 'tipoOrganos',
            autoLoad: true
        },
        organosStore: {
            type: 'organos',
            autoLoad: true
        },
        reunionInvitadosStore: {
            type: 'reunionInvitados'
        },
        puntosOrdenDiaTreeStore: {
            type: 'puntosOrdenDiaTree'
        },        
        organoReunionInvitadosStore : {
            type : 'organoReunionInvitados'
        }
    }
});
