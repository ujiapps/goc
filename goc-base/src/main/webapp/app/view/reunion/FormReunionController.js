Ext.define('goc.view.reunion.FormReunionController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formReunionController',

    init : function() {
        Ext.ux.uji.form.ChangedField.disable();
    },

    onLoad: function ()
    {
		
		var view = this.getView();
		var labelResponsableVotacionId = view.down("displayfield[name=labelIdResponsableVotacion]");
		var inputResponsableVotacionNombre = view.down('textfield[name=inputResponsableVotacion]');
		
		var vm = this.getViewModel(),
            organosAsistentesStore = vm.get('organosStore'),
            me = this,
            reunion = vm.get('reunion');
            
        
        if(reunion.data != null) {
		
			var responsableVotoId = reunion.data.responsableVotoId;
			var responsableVotoNombre = reunion.data.responsableVotoNom;
		
			if(responsableVotoId != "" && labelResponsableVotacionId.rawValue == "") {
				
				if(labelResponsableVotacionId.rawValue == "") {
					
					labelResponsableVotacionId.setValue(responsableVotoId);
				}
				
				if(inputResponsableVotacionNombre.getValue() == "") {
					inputResponsableVotacionNombre.setValue(responsableVotoNombre);
				}
				
			}
	
		} 
		
        const TEMPLATE_ORGANO = '{organo}';

        if (exprMeetingTitle && reunion.create)
        {
            var fieldAsunto = this.getFieldAsunto();
            var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();

            if (fieldAsunto !== null && fieldAsunto.value === "" )
                vm.set('reunion.asunto', exprMeetingTitle);
            if (fieldAsuntoAlt !== null && fieldAsuntoAlt.value  === "")
                vm.set('reunion.asuntoAlternativo', (exprMeetingTitleAlternative) ? exprMeetingTitleAlternative : '');
        }

        var onOrganoAdded = function (store, records, index)
        {
            var organo = store.getAt(index).get('nombre'),
                fieldAsunto = me.getFieldAsunto(),
                fieldAsuntoAlt = me.getFieldAsuntoAlternativo(),
                nuevoNombre = organo,
                nombreAnterior = null;

            if (index > 0)
            {
                nombreAnterior = store.getAt(index - 1).get('nombre');
                nuevoNombre = nombreAnterior + ' | ' + organo;
            }
            me.replaceTemplateInField(fieldAsunto, TEMPLATE_ORGANO, nuevoNombre, nombreAnterior);
            me.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE_ORGANO, nuevoNombre, nombreAnterior);
        };

        for (var index = 0; index < organosAsistentesStore.getData().length; index++) {
            organosAsistentesStore = vm.get('organosStore');

            onOrganoAdded(organosAsistentesStore, organosAsistentesStore.getData(), index);
        }

        organosAsistentesStore.on("add", onOrganoAdded);

        organosAsistentesStore.on("remove", function (store, records, index)
        {
            var organo = records[0].get('nombre'),
                fieldAsunto = me.getFieldAsunto(),
                fieldAsuntoAlt = me.getFieldAsuntoAlternativo(),
                nuevoNombre = '',
                nombreAnterior = organo;

            if (me.esElUltimoOrganoParaBorrar(store))
            {
                nuevoNombre = TEMPLATE_ORGANO;
            } else if (me.hayAlmenosUnOrgano(store) && me.noEsElPrimerOrganoDeLaListaParaBorrar(index))
            {
                nombreAnterior = ' | ' + organo;
            } else
            {
                nombreAnterior =  organo + ' | ';
            }
            me.replaceTemplateInField(fieldAsunto, TEMPLATE_ORGANO, nuevoNombre, nombreAnterior);
            me.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE_ORGANO, nuevoNombre, nombreAnterior);

        });

        if (!reunion.create)
        {
            this.ocultarCheckRevisarActa();
        }
    },

    ocultarCheckRevisarActa: function () {
          this.getView().lookupReference('revisarActa').setVisible(false);
    },

    esElUltimoOrganoParaBorrar: function(store){
        return store.count() === 0;
    },

    hayAlmenosUnOrgano: function (store) {
        return store.count() > 0;
    },

    noEsElPrimerOrganoDeLaListaParaBorrar: function (index) {
        return index > 0;
    },
    
    rendererLabelIdResponsableVotacion: function(value) {
		if(value != "") {
			return value;
		} else {
			return "";
		}
	},
	
	rendererInputResponsableVotacion: function(value) {
		if(value!= "") {
			return value;
		} else {
			return "";
		}
	},
    
    addResponsable: function() {
		
		var window = Ext.create('goc.view.common.LookupWindowPersonas',
        {
            appPrefix : 'goc',
            title : appI18N.reuniones.seleccionaResponsable
        });
        
        
        var gridBusquedaLookUp = window.items.items[1];
        
        
        window.show();
          
		var view = this.getView();
        
        
        window.on('LookoupWindowClickSeleccion', function(res)
        {
	
			
			var inputResponsableVotacionNombre = view.down('textfield[name=inputResponsableVotacion]');
			var labelResponsableVotacionId = view.down("displayfield[name=labelIdResponsableVotacion]");
	
            var persona = Ext.create('goc.model.ReunionInvitado',
            {
                personaId : res.get('id'),
                personaNombre : res.get('nombre'),
                personaEmail : res.get('email')
            });
			

            var existePersona = gridBusquedaLookUp.store.find('personaId', persona.get('personaId'));
            
            
            if (existePersona === -1)
            {
               
                
                labelResponsableVotacionId.setValue(persona.get('personaId'));
                labelResponsableVotacionId.setHidden(true);
                inputResponsableVotacionNombre.setValue(persona.get('personaNombre'));
            }
        });
        
	},
	
	removeResponsable: function() {
		var view = this.getView();
		var labelResponsableVotacionId = view.down("displayfield[name=labelIdResponsableVotacion]");
		var inputResponsableVotacionNombre = view.down('textfield[name=inputResponsableVotacion]');
		
		labelResponsableVotacionId.setValue("");
		inputResponsableVotacionNombre.setValue("");
	},

    onClose: function ()
    {
        var win = Ext.WindowManager.getActive();
        var grid = Ext.ComponentQuery.query('grid[name=reunion]')[0];
        grid.getStore().reload();

        Ext.ux.uji.form.ChangedField.disable();

        if (win)
        {
            win.destroy();
        }
    },

    onBorrarAsistenteReunion: function (record)
    {
        var vm = this.getViewModel();
        var store = vm.get('organosStore');
        store.remove(record);
    },

    getStoreDeMiembros: function (organoId, externo)
    {
        var vm = this.getViewModel();
        var miembrosStores = vm.get('miembrosStores');
        return miembrosStores[organoId + '_' + externo];
    },

    creaStoreDeMiembros: function (organoId, externo)
    {
        var vm = this.getViewModel();
        var miembrosStores = vm.get('miembrosStores');
        miembrosStores[organoId + '_' + externo] = Ext.create('goc.store.OrganoReunionMiembros');
    },

    onDetalleAsistentesReunion: function (organo, reunionId)
    {
        var vm = this.getViewModel();
        var reunion = vm.get('reunion');
        var view = this.getView();
        var remoteLoad = false;
        if (!this.getStoreDeMiembros(organo.get('id'), organo.get('externo')))
        {
            this.creaStoreDeMiembros(organo.get('id'), organo.get('externo'));
            remoteLoad = true;
        }

        var store = this.getStoreDeMiembros(organo.get('id'), organo.get('externo'));

        var formData = view.down('form').getValues();

        var modalDefinition = {
            xtype: 'formReunionMiembros',
            viewModel: {
                data: {
                    reunionId: reunionId,
                    reunionCompletada: reunionId ? reunion.get('completada') : false,
                    admiteSuplencia: formData.admiteSuplencia ? true : false,
                    admiteDelegacionVoto: formData.admiteDelegacionVoto ? true : false,
                    admiteComentarios: formData.admiteComentarios ? true : false,
                    organoId: organo.get('id'),
                    externo: organo.get('externo'),
                    store: store,
                    remoteLoad: remoteLoad,
                }
            }
        };
        this.modal = view.add(modalDefinition);
        this.modal.show();
    },

    saveOrganosYInvitados: function (reunionId, organos, invitados, onClose, puntoActaAnterior, reunionOriginalId, duplicando)
    {
        var me = this;
        var view = this.getView();

        var datosOrganos = this.buildDatosOrganos(organos);
        for(var i = 0; i< datosOrganos.length; i++){
            if (datosOrganos[i].id !== undefined && isNaN(datosOrganos[i].id))
                datosOrganos[i].id = Transforms.getOrganoId(datosOrganos[i].id, datosOrganos[i].externo);
        }
        var datosInvitados = this.buildDatosInvitados(invitados);
        var nombreYEmailInvitados = null;

        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + reunionId + '/organos',
            method: 'PUT',
            jsonData: {organos: datosOrganos},
            success: function ()
            {
                if(puntoActaAnterior)
                {
                    me.anyadirPuntoRevisionActaAnterior(reunionId, reunionOriginalId, duplicando, onClose);
                }
                var invitadosNuevosConvocados = null;
                Ext.Ajax.request({
                    url: '/goc/rest/reuniones/' + reunionId + '/invitados',
                    method: 'PUT',
                    jsonData: {invitados: datosInvitados},
                    success: function (response)
                    {
                        invitadosNuevosConvocados = Ext.decode(response.responseText).data;
                        if(invitadosNuevosConvocados !== null && invitadosNuevosConvocados.length > 0) {
                            var i;
                            for (i = 0; i < invitadosNuevosConvocados.length; i++) {
                                if (nombreYEmailInvitados === null)
                                    nombreYEmailInvitados = invitadosNuevosConvocados[i].personaNombre + ' (' + invitadosNuevosConvocados[i].personaEmail + ') ' + ", \n";
                                else if (i+1 === invitadosNuevosConvocados.length)
                                    nombreYEmailInvitados = nombreYEmailInvitados + ' ' + invitadosNuevosConvocados[i].personaNombre  + ' (' + invitadosNuevosConvocados[i].personaEmail + ') ';
                                else
                                    nombreYEmailInvitados = nombreYEmailInvitados + ' ' + invitadosNuevosConvocados[i].personaNombre  + ' (' + invitadosNuevosConvocados[i].personaEmail + ') ' + ", \n";
                            }
                        }
                        onClose();
                        if (nombreYEmailInvitados !== null) {
                            Ext.Msg.confirm(appI18N.reuniones.invitadoNotificado, nombreYEmailInvitados,
                                function (result) {
                                if(result === 'yes' ){
                                    Ext.Ajax.request({
                                        url: '/goc/rest/reuniones/' + reunionId + '/notificainvitados',
                                        method: 'PUT',
                                        jsonData: {invitados: invitadosNuevosConvocados},
                                    });
                                }
                                }
                                );
                        }
                    },
                    failure: function ()
                    {
                        view.setLoading(false);
                    }
                });
            },
            failure: function ()
            {
                view.setLoading(false);
            }
        });
    },

    anyadirPuntoRevisionActaAnterior: function (reunionId, reunionOriginalId, duplicando, onClose)
    {
        var me = this;
        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/revisionactaanterior',
            method: 'POST',
            success: function ()
            {
                if (duplicando)
                {
                    me.duplicaPuntosOrdenDia(reunionOriginalId, reunionId, onClose);
                }
                onClose();
            },
            failure: function ()
            {
                view.setLoading(false);
            }
        });
    },

    buildDatosInvitados: function (invitados)
    {
        var datosInvitados = [];

        invitados.each(function (record)
        {
            datosInvitados.push({
                personaId: record.get('personaId'),
                personaNombre: record.get('personaNombre'),
                personaEmail: record.get('personaEmail'),
                motivoInvitacion: record.get('motivoInvitacion'),
                asistencia: record.get('asistencia')
            });
        });

        return datosInvitados;
    },

    buildDatosOrganos: function (organos)
    {
        var datosOrganos = [];
        var self = this;
        organos.each(function (record)
        {
            var miembros = [];
            var miembrosStore = self.getStoreDeMiembros(record.get('id'), record.get('externo'));
            if (miembrosStore)
            {
                miembrosStore.getData().each(function (record)
                {
                    miembros.push({
                        id: record.get('id'),
                        email: record.get('email'),
                        nombre: record.get('nombre'),
                        cargo: record.get('cargo'),
                        asistencia: record.get('asistencia'),
                        suplenteId: record.get('suplenteId'),
                        suplenteNombre: record.get('suplenteNombre'),
                        suplenteEmail: record.get('suplenteEmail'),
                        justificaAusencia: record.get('justificaAusencia'),
                        motivoAusencia: record.get('motivoAusencia'),
                        delegadoVotoNombre: record.get('delegadoVotoNombre'),
                        delegadoVotoId: record.get('delegadoVotoId'),
                        delegadoVotoEmail: record.get('delegadoVotoEmail')
                    });
                });
            }

            var data = {
                id: record.get('id'),
                externo: record.get('externo'),
                miembros: miembros
            };

            datosOrganos.push(data);
        });

        return datosOrganos;
    },
    
    seEliminaDelegadoVoto: function() {
		eliminaDelegadoVoto = false;
		
		vm = this.getViewModel();
		organosStore = vm.get('organosStore');
		
		datosOrganos = this.buildDatosOrganos(organosStore.getData());
		
		datosOrganos[0].miembros.forEach(function(element, index){
			if(element.delegadoVotoId == 0) {
				eliminaDelegadoVoto = true;
			}
		});
		
		return eliminaDelegadoVoto;
	},

    onSaveRecord: function (button, context)
    {
        var form = Ext.ComponentQuery.query('form[name=reunion]')[0];
        var gridStore = Ext.ComponentQuery.query('grid[name=reunion]')[0].getStore();
        if (form.isValid())
        {
            var data = form.getValues();
            var ref = this;

            if (Ext.Date.parse(data.fecha + ' ' + data.hora, 'd/m/Y H:i') < new Date())
            {
				
				Ext.Msg.confirm(appI18N.reuniones.fechaAnterior, appI18N.reuniones.fechaAnteriorAActual, function(result){
                    if (result === 'yes') {
                        ref.checkDelegacionVotoAndSaveReunion();
                    }
				});
				
			} else {
				ref.checkDelegacionVotoAndSaveReunion();
				gridStore.reload();
			}
        }
    },

    areWeDuplicatingReunion: function(record)
    {
        return record && record.data && record.get('idReunionOriginal') != null && record.get('idReunionOriginal') !== '';
    },

    duplicaReunion: function(record, organosStore, invitadosStore)
    {
        var me = this;
        var view = this.getView();
        var duplicando = true;
        var reunionOriginalId = record.get('idReunionOriginal');
        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + reunionOriginalId + '/duplica',
            method: 'POST',
            jsonData: record.data,
            success: function (response)
            {
                response = Ext.decode(response.responseText);
                if (response && response.success && response.data && response.data.id)
                {
                    me.saveOrganosYInvitados(response.data.id, organosStore.getData(), invitadosStore.getData(), me.onClose, true, reunionOriginalId, duplicando);
                } else
                {
                    me.onClose();
                }
            },
            failure: function()
            {
                view.setLoading(false);
            }
        });
    },

    duplicaPuntosOrdenDia: function (idreunionOriginal, recordData, onClose)
    {
        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + idreunionOriginal + '/duplicapuntos',
            method: 'POST',
            jsonData: recordData,
            success: function ()
            {
                onClose();
            },
            failure: function ()
            {
                view.setLoading(false);
            }
        });
    },

    saveReunion: function (view, record, data, me, organosStore, invitadosStore, store)
    {
        view.setLoading(true);
     	var displayFieldIdResponsableVot = view.down('displayfield[name=labelIdResponsableVotacion]');
        var inputResponsableVotacionNombre = view.down('textfield[name=inputResponsableVotacion]');

        if (record.create !== true)
        {
            record.set('fecha', Ext.Date.parseDate(data.fecha + ' ' + data.hora, 'd/m/Y H:i'));
            record.set('fechaSegundaConvocatoria', Ext.Date.parseDate(data.fecha + ' ' + data.horaSegundaConvocatoria, 'd/m/Y H:i'));
            record.set('fechaFinVotacion', Ext.Date.parseDate(data.fechaFinVotacion + ' '+ data.horaFinVotacion, 'd/m/Y H:i'));
            
            if(displayFieldIdResponsableVot.rawValue != "" && displayFieldIdResponsableVot.value != "") {
				record.set('responsableVotoId', displayFieldIdResponsableVot.rawValue);
			} else {
				record.set('responsableVotoId', "");
			}
			
			if(inputResponsableVotacionNombre.getValue()!= "" && inputResponsableVotacionNombre.getValue() != "") {
				record.set('responsableVotoNom', inputResponsableVotacionNombre.getValue());
			} else {
				record.set('responsableVotoNom', "");
			}
			

            if (me.areWeDuplicatingReunion(record))
            {
                return me.duplicaReunion(record, organosStore, invitadosStore);
            } else
            {
                return record.save({
                    success: function ()
                    {
                        me.saveOrganosYInvitados(data.id, organosStore.getData(), invitadosStore.getData(), me.onClose);
                    },
                    failure: function ()
                    {
                        view.setLoading(false);
                    },
                    scope: me
                });
            }
        }

        record.fecha = Ext.Date.parseDate(data.fecha + ' ' + data.hora, 'd/m/Y H:i');
        record.fechaSegundaConvocatoria = Ext.Date.parseDate(data.fecha + ' ' + data.horaSegundaConvocatoria, 'd/m/Y H:i');
        record.fechaFinVotacion = Ext.Date.parseDate(data.fechaFinVotacion + ' '+ data.horaFinVotacion, 'd/m/Y H:i');
        record.tipoVisualizacionResultados = data.tipoVisualizacionResultados;
        
        if(data.admiteSuplencia !== undefined) {
			record.admiteSuplencia = data.admiteSuplencia;
		} else {
			record.admiteSuplencia = false;
		}
		
		if(data.admiteDelegacionVoto !== undefined) {
        	record.admiteDelegacionVoto = data.admiteDelegacionVoto;
		} else {
			record.admiteDelegacionVoto = false;
		}
        
        if(data.revisarActa !== undefined) {
        	record.revisarActa = data.revisarActa;
        } else {
			record.revisarActa = false;
		}
        
        if(data.admiteComentarios !== undefined) {
        	record.admiteComentarios = data.admiteComentarios;
        } else {
			record.admiteComentarios = false;
		}
        
        if(data.emailsEnNuevosComentarios !== undefined) {
        	record.emailsEnNuevosComentarios = data.emailsEnNuevosComentarios;
        } else {
			record.emailsEnNuevosComentarios = false;
		}
        
        
        record.publica = data.publica;
        record.telematica = data.telematica;
        
        if(data.verDeliberaciones !== undefined) {
        	record.verDeliberaciones = data.verDeliberaciones;
        } else {
			record.verDeliberaciones = false;
		}
		
		if(data.verAcuerdos !== undefined) {
        	record.verAcuerdos = data.verAcuerdos;
        } else {
			record.verAcuerdos = false;
		}
        
        if(displayFieldIdResponsableVot.rawValue != "" && displayFieldIdResponsableVot.value != "") {
				record.responsableVotoId = displayFieldIdResponsableVot.rawValue;
			} else {
				record.responsableVotoId = "";
			}
			
			if(inputResponsableVotacionNombre.getValue()!= "" && inputResponsableVotacionNombre.getValue() != "") {
				record.responsableVotoNom = inputResponsableVotacionNombre.getValue();
			} else {
				record.responsableVotoNom = "";
			}
        

        store.add(record);
        store.sync({
            success: function ()
            {
                me.saveOrganosYInvitados(record.id, organosStore.getData(), invitadosStore.getData(), me.onClose, record.revisarActa);
            },
            failure: function ()
            {
                view.setLoading(false);
            },
            scope: me
        });

        Ext.ux.uji.form.ChangedField.disable();
    },

    checkDelegacionVotoAndSaveReunion: function ()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var form = Ext.ComponentQuery.query('form[name=reunion]')[0];

        var organosStore = vm.get('organosStore');
        var invitadosStore = vm.get('reunionInvitadosStore');

        var record = vm.get('reunion');
        var data = form.getValues();
        var store = vm.get('store');
        var me = this;
        var reunionId;
        if (record.data !== undefined){
            reunionId = record.data.id;
        }

        if (record.data !== undefined && !record.data.admiteDelegacionVoto)
        {
            var delegados;
            Ext.Ajax.request({
                url: '/goc/rest/reuniones/' + reunionId + '/existendelegados',
                method: 'GET',
                success: function (response)
                {
                    delegados = Ext.decode(response.responseText).data;
                    if (delegados !== undefined && delegados !== null && delegados.length > 0)
                    {
                        var nombres = "";
                        for (var i = 0; i < delegados.length; i++)
                        {
                            nombres += delegados[i].nombre + "\n";
                        }

                        return Ext.Msg.confirm(appI18N.reuniones.deshabilitarDelegacionVoto, appI18N.reuniones.deshabilitarDelegacionVotoListaNombres + "\n" + nombres, function (btn, text)
                        {
                            if (btn == 'yes')
                            {
                                return me.saveReunion(view, record, data, me, organosStore, invitadosStore, store);
                            }
                        });
                    }else {
                        me.saveReunion(view, record, data, me, organosStore, invitadosStore, store);
                    }
                }
            });
        } else {
            me.saveReunion(view, record, data, me, organosStore, invitadosStore, store);
        }
    },

    afterRenderFormReunion: function (windowFormReunion)
    {
        var height = Ext.getBody().getViewSize().height;
        if (windowFormReunion.getHeight() > height)
        {
            windowFormReunion.setHeight(height - 30);
            windowFormReunion.setPosition(windowFormReunion.x, 15);
        }
    },

    onDateChanged: function (obj, newValue, oldValue)
    {
        var fieldAsunto = this.getFieldAsunto();
        var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();
        const TEMPLATE = '{fecha}';
        var strFecha = this.formatDate(newValue, 'd/m/Y', TEMPLATE);
        var strFechaOld = this.formatDate(oldValue, 'd/m/Y', TEMPLATE);

        if (fieldAsunto)
        {
            this.replaceTemplateInField(fieldAsunto, TEMPLATE, strFecha, strFechaOld);
            this.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE, strFecha, strFechaOld);
        }
    },

    onVotacionTelematicaChange: function(obj, newValue, oldValue, eOpts)
    {
	
		var view = this.getView();
		var labelResponsableVotacionId = view.down("displayfield[name=labelIdResponsableVotacion]");
		var inputResponsableVotacionNombre = view.down('textfield[name=inputResponsableVotacion]');
	
	
        if (!newValue) {
            Ext.ComponentQuery.query('checkbox[name=admiteCambioVoto]')[0].setValue(false);
            Ext.ComponentQuery.query('checkbox[name=tipoVisualizacionResultados]')[0].setValue(true);
            Ext.ComponentQuery.query('datefield[name=fechaFinVotacion]')[0].reset();
            Ext.ComponentQuery.query('timefield[name=horaFinVotacion]')[0].reset();
            
			if(!inputResponsableVotacionNombre.isHidden()) {
				Ext.ComponentQuery.query('button[name=botonResponsableVotacion]')[0].setDisabled(!newValue);
				
				labelResponsableVotacionId.setValue("");
				inputResponsableVotacionNombre.setValue("");
			}
        }

        if (newValue) {
            Ext.ComponentQuery.query('checkbox[name=admiteCambioVoto]')[0].setValue(admiteCambioVoto);
        }

        Ext.ComponentQuery.query('checkbox[name=admiteCambioVoto]')[0].setDisabled(!newValue);
        Ext.ComponentQuery.query('checkbox[name=tipoVisualizacionResultados]')[0].setDisabled(!newValue);
        Ext.ComponentQuery.query('datefield[name=fechaFinVotacion]')[0].setDisabled(!newValue);
        Ext.ComponentQuery.query('timefield[name=horaFinVotacion]')[0].setDisabled(!newValue);
        
        Ext.ComponentQuery.query('button[name=botonResponsableVotacion]')[0].setDisabled(!newValue);
        Ext.ComponentQuery.query('textfield[name=inputResponsableVotacion]')[0].setDisabled(!newValue);
        Ext.ComponentQuery.query("button[name=btnEliminaResponsable]")[0].setDisabled(!newValue);
    },

    onTimeChanged: function (obj, newValue, oldValue)
    {
        const TEMPLATE = '{hora}';
        var fieldAsunto = this.getFieldAsunto();
        var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();
        var strHora = this.formatDate(newValue, 'H:i', TEMPLATE);
        var strHoraOld = this.formatDate(oldValue, 'H:i', TEMPLATE);


        if (fieldAsunto)
        {
            this.replaceTemplateInField(fieldAsunto, TEMPLATE, strHora, strHoraOld);
            this.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE, strHora, strHoraOld);
        }
    },

    onDuracionChanged: function (obj, newValue, oldValue)
    {
        const TEMPLATE = '{duracion}';
        var fieldAsunto = this.getFieldAsunto();
        var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();
        newValue = (!newValue || newValue === '') ? TEMPLATE : newValue;

        if (fieldAsunto)
        {
            this.replaceTemplateInField(fieldAsunto, TEMPLATE, newValue, oldValue);
            this.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE, newValue, oldValue);
        }
    },

    onNumSessionChanged: function (obj, newValue, oldValue)
    {
        const TEMPLATE = '{numSesion}';
        var fieldAsunto = this.getFieldAsunto();
        var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();
        newValue = (!newValue || newValue === '') ? TEMPLATE : newValue;

        if (fieldAsunto)
        {
            this.replaceTemplateInField(fieldAsunto, TEMPLATE, newValue, oldValue);
            this.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE, newValue, oldValue);
        }
    },

    getFieldAsunto: function ()
    {
        return Ext.ComponentQuery.query('textfield[name=asunto]')[0];
    },

    getFieldAsuntoAlternativo: function ()
    {
        return (isMultilanguageApplication()) ? Ext.ComponentQuery.query('textfield[name=asuntoAlternativo]')[0] : null;
    },

    formatDate: function (date, format, returnIfError)
    {
        var strDate = (date && format) ? Ext.Date.format(date, format) : '';
        return (strDate && strDate !== '') ? strDate : returnIfError;
    },

    replaceTemplateInField: function (field, template, newValue, oldValue)
    {
        if (field)
        {
            if (field.value.indexOf(template) != -1)
                field.setValue(field.value.replace(template, newValue));
            else if (field.value.indexOf(oldValue) != -1)
                field.setValue(field.value.replace(oldValue, newValue));
        }
    },

    reposiciona: function(obj) {
        var left = obj.getXY()[0] - 500;
        obj.setStyle('left', left + 'px');
    },

    onSearchGridShow: function(obj) {
        var me = this;
        var searchGrid = obj.down('grid');
        if (!searchGrid.hasListeners.itemclick)
        {
            searchGrid.on("itemclick", function ()
            {
                me.reposiciona(obj);
            });
        }
        this.reposiciona(obj);
    },
    onSetOpcionResultadoVotacion: function (obj, newValue, oldValue){
        Ext.apply(Ext.getCmp('horaFinVotacion'), {allowBlank: (newValue.toLowerCase() === 'true')}, {});
        Ext.apply(Ext.getCmp('fechaFinVotacion'), {allowBlank: (newValue.toLowerCase() === 'true')}, {});
    }
});
