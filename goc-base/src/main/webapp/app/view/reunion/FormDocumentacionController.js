Ext.define('goc.view.reunion.FormDocumentacionController', {
    extend: 'Ext.app.ViewController', alias: 'controller.formDocumentacionController',

    onLoad: function () {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');

        if (reunionId) {
            viewModel.get('store').load({
                url: '/goc/rest/reuniones/' + reunionId + '/documentos'
            });
        }

    },

    onClose: function () {
        var win = Ext.WindowManager.getActive();
        var grid = Ext.ComponentQuery.query('grid[name=reunion]')[0];
        grid.getStore().reload();

        if (win) {
            win.destroy();
        }
    },

    borraDocumento: function (documentoId) {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');

        Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar el document?', function (result) {
            if (result === 'yes') {
                Ext.Ajax.request({
                    url: '/goc/rest/reuniones/' + reunionId + '/documentos/' + documentoId,
                    method: 'DELETE',
                    success: function () {
                        viewModel.get('store').reload();
                    }
                });
            }
        });
    },

    inhabilitaDocumento: function (documento) {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');
        var view = this.getView();
        var grid = view.down('grid');
        var id = documento.get('id');

        var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
        var record = reunionGrid.getView().getSelectionModel().getSelection()[0];
        var enviada = record.get('avisoPrimeraReunion');

        console.log(record);

        Ext.Ajax.request({
            url: 'rest/reuniones/' + reunionId + '/documentos/' + id + '/desactivar',
            method: 'PUT',
            success: function () {
                grid.getStore().reload()

                if (enviada) {
                    var viewport = view.up('viewport')
                    this.modal = viewport.add({
                        xtype: 'formMotivoEdicion', viewModel: {
                            data: {
                                reunionId: reunionId, record: documento
                            }
                        }
                    })
                    this.modal.show()
                    return;
                }

                Ext.Ajax.request({
                    timeout: 180000,
                    url: 'rest/reuniones/' + reunionId + '/documentos/' + id + '/guardarModificacionDocumento',
                    method: 'POST',
                    jsonData: {motivoEdicion: "No disponible (convocatoria no enviada)"}
                })
            }
        })
    },

    descargaDocumento: function (documentoId) {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');
        var url = '/goc/rest/reuniones/' + reunionId + '/documentos/' + documentoId + '/descargar';

        var body = Ext.getBody();
        var frame = body.createChild({
            tag: 'iframe', cls: 'x-hidden', name: 'iframe'
        });

        var form = body.createChild({
            tag: 'form', cls: 'x-hidden', action: url, target: 'iframe'
        });
        form.dom.submit();
    },

    subirDocumento: function () {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');
        var view = this.getView();

        var form = view.down('form[name=subirDocumento]');
        var grid = view.down('grid');

        var inputIdDoc = form.down('hiddenfield');
        var url = '/goc/rest/reuniones/' + reunionId + '/documentos/';
        var showConfirm = false;
        var confirmado = false;
        var selectedRecord = grid.getSelection()[0]
        var descripcionForm = form.down('textfield[name=descripcion]');
        var descripcionAlternativaForm = form.down('textfield[name=descripcionAlternativa]');
        var nombreDocumento = form.down('displayfield[name=nombreDocumento]').getValue();

        var idDoc = parseInt(inputIdDoc.getValue());
        if (inputIdDoc.getValue() != null && typeof idDoc === 'number' && idDoc > 0) {
            url = '/goc/rest/reuniones/' + reunionId + '/documentos/' + idDoc;
            showConfirm = true;
        }

        if (form.getForm().isValid() && form.down('filefield[name=documento]').getValue() !== "") {

            if (showConfirm) {
                if (confirm(appI18N.reuniones.subirNuevaVersionConfirm)) {
                    confirmado = true;
                }
            }

            if ((showConfirm && confirmado) || !showConfirm) {

                if (descripcionForm.getValue() === "") {
                    descripcionForm.setValue(nombreDocumento.split(".")[0])
                }

                if (isMultilanguageApplication() && descripcionAlternativaForm.getValue() === "") {
                    descripcionAlternativaForm.setValue(nombreDocumento.split(".")[0])
                }

                form.submit({
                    url: url, scope: this, submitEmptyText: false, success: function () {
                        viewModel.get('store').load({
                            url: '/goc/rest/reuniones/' + reunionId + '/documentos'
                        });
                        grid.setSelection(null);
                        this.limpiarFormulario();
                    }
                });
                if (confirmado) {

                    var store = viewModel.get('store')
                    var record = store.findRecord('id', selectedRecord.get('id'));
                    var viewport = view.up('viewport');

                    var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
                    var enviada = reunionGrid.getView().getSelectionModel().getSelection()[0].get('avisoPrimeraReunion');

                    if (enviada) {
                        this.modal = viewport.add({
                            xtype: 'formMotivoEdicion', viewModel: {
                                data: {
                                    reunionId: reunionId, record: record
                                }
                            }
                        })
                        this.modal.show();
                        return;
                    }

                    Ext.Ajax.request({
                        timeout: 180000,
                        url: 'rest/reuniones/' + reunionId + '/documentos/' + idDoc + '/guardarModificacionDocumento',
                        method: 'POST',
                        jsonData: {motivoEdicion: "No disponible (convocatoria no enviada)"}
                    })
                }
            }
        } else {
            return Ext.Msg.alert(appI18N.reuniones.subirNuevoDocumento, appI18N.adjuntos.seleccionarDocumentoParaSubir);
        }
    },

    subeOrdenDocumento: function (documentoId) {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');
        var grid = this.getView().down('grid');
        Ext.Ajax.request({
            url: 'rest/reuniones/' + reunionId + '/documentos/' + documentoId + '/subir',
            method: 'PUT',
            success: function () {
                grid.getStore().reload()
            }
        })
    },

    bajaOrdenDocumento: function (documentoId) {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');
        var grid = this.getView().down('grid');
        Ext.Ajax.request({
            url: 'rest/reuniones/' + reunionId + '/documentos/' + documentoId + '/bajar',
            method: 'PUT',
            success: function () {
                grid.getStore().reload()
            }
        })
    },

    onFileChange: function (obj, value) {
        var filename = value.split(/(\\|\/)/g).pop(),
            labelNombreDocumento = this.getView().lookupReference('nombreDocumento');
        labelNombreDocumento.setValue(filename);
    },

    onSelect: function () {
        var record = this.getView().down('grid').getSelection()[0];
        if (record != null) {
            var inputDescripcion = this.getView().lookupReference('descripcion');
            var inputDescripcionAlternativa = this.getView().lookupReference('descripcionAlternativa');
            inputDescripcion.setValue(record.get('descripcion'));
            inputDescripcion.disable();
            if (isMultilanguageApplication()) {
                inputDescripcionAlternativa.setValue(record.get('descripcionAlternativa'));
                inputDescripcionAlternativa.disable();
            }
            var form = this.getView().down('form');
            form.setTitle(appI18N.reuniones.subirNuevoDocumentoTituloFormulario);
            var btnSubirArchivo = this.getView().lookupReference('btnSubirArchivo');
            btnSubirArchivo.setText(appI18N.reuniones.subirNuevaVersion);
            var inputIdDoc = form.down('hiddenfield');
            inputIdDoc.setValue(record.get('id'));
        }
    },

    limpiarFormulario: function () {
        this.getView().down('grid').setSelection(null);
        this.getView().down('form').reset();
        var inputDescripcion = this.getView().lookupReference('descripcion');
        inputDescripcion.enable();
        if (isMultilanguageApplication()) {
            var inputDescripcionAlternativa = this.getView().lookupReference('descripcionAlternativa');
            inputDescripcionAlternativa.enable();
        }
        var form = this.getView().down('form');
        form.setTitle(appI18N.reuniones.subirNuevoDocumento);
        var btnSubirArchivo = this.getView().lookupReference('btnSubirArchivo');
        btnSubirArchivo.setText(appI18N.reuniones.subirDocumento);
    }
});
