Ext.define('goc.view.reunion.FormTextoConvocatoriaController',
    {
        extend : 'Ext.app.ViewController',
        alias : 'controller.formTextoConvocatoriaController',


        onClose : function()
        {
            var win = Ext.WindowManager.getActive();
            if (win)
            {
                win.destroy();
            }
        },
        
        datosTextosConvocatoria:function(vm){
			var textosConvocatoria=[]
			textosConvocatoria.push({
				textoConvocatoria:vm.get('textoConvocatoria'),
				textoConvocatoriaAlternativo:vm.get('textoConvocatoriaAlternativo')
			})
			return textosConvocatoria;
		},
        onEnviarConvocatoria: function (button, event)
        {
            button.disable();
            var vm = this.getViewModel();
            var record = vm.get('record');
            var ref = this;
            var textosConvocatoria = ref.datosTextosConvocatoria(vm)
            Ext.Ajax.request(
                {
                    timeout: 180000,
                    url : '/goc/rest/reuniones/' + record.get('id') + '/enviarconvocatoria',
                    method : 'PUT',
                    jsonData: {textoConvocatoria:vm.get('textoConvocatoria'), textoConvocatoriaAlternativo:vm.get('textoConvocatoriaAlternativo')},
                    success : function(response)
                    {
                        var data = Ext.decode(response.responseText);
                        var mensajeRespuesta = (data.message && data.message.indexOf("appI18N") != -1) ? eval(data.message) : data.message;

                        vm.get('reunionesStore').reload();
                        ref.onClose();

                        Ext.Msg.alert(appI18N.reuniones.resultadoEnvioConvocatoriaTitle, mensajeRespuesta);
                        button.enable();
                    },
                    failure :function(response) {
                        var data = Ext.decode(response.responseText);
                        var mensajeRespuesta = (data.message && data.message.indexOf("appI18N") != -1) ? eval(data.message) : data.message;

                        Ext.Msg.alert(appI18N.reuniones.resultadoEnvioConvocatoriaTitle, mensajeRespuesta);
                        button.enable();
                    },
                    scope : this
                }
            );
        }
});
