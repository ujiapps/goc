var items= [
            {
                xtype: 'textarea',
                fieldLabel : appI18N.reuniones.textoAdicional + '\n(Valenciá)',
                width: 500,
                height: 200,
                name : 'convocatoria',
                bind : {
                    value : '{textoConvocatoria}'
                }
            }
        ]
        
if(isMultilanguageApplication()){
	items.push({ 
                xtype: 'textarea',
                fieldLabel : appI18N.reuniones.textoAdicional+'\n(Castellano)',
                width: 500,
                height: 200,
                name : 'convocatoriaAlternativa',
                bind : {
                    value : '{textoConvocatoriaAlternativo}'
                }
            })
}        

Ext.define('goc.view.reunion.FormTextoConvocatoria',
    {
        extend : 'Ext.window.Window',
        xtype : 'formTextoConvocatoria',
        title: appI18N.reuniones.enviarConvocatoriaTexto,
        modal : true,
        bodyPadding : 10,
        layout : 'vbox',

        requires : ['goc.view.reunion.FormTextoConvocatoriaController'],
        controller : 'formTextoConvocatoriaController',

        bbar : {
            defaultButtonUI : 'default',
            items: [
                '->',
                {
                    xtype : 'button',
                    text : appI18N.reuniones.enviarConvocatoria,
                    handler : 'onEnviarConvocatoria'
                }, {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'onClose');
                        }
                    }
                }
            ]
        },

        items :items
    });