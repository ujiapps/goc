Ext.define('goc.view.reunion.FormTextoInformarOtrosOrganos',
    {
        extend : 'Ext.window.Window',
        xtype : 'formTextoInformarOtrosOrganos',
        title: appI18N.reuniones.enviarTextoInformarOtrosOrganos,
        modal : true,
        bodyPadding : 10,
        layout : 'vbox',

        requires : ['goc.view.reunion.FormTextoInformarOtrosOrganosController'],
        controller : 'formTextoInformarOtrosOrganosController',

        bbar : {
            defaultButtonUI : 'default',
            items: [
                '->',
                {
                    xtype : 'button',
                    text : appI18N.reuniones.enviarTextoOtrosOrganos,
                    handler : 'onEnviarTexto' 
                }, {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'onClose');
                        }
                    }
                }
            ]
        },

        items : [
            {
                xtype: 'textarea',
                fieldLabel : getMultiLangLabel(appI18N.reuniones.textoAdicional, mainLanguage),
                width: 500,
                height: 200,
                name : 'textoInformar',
                bind : {
                    value : '{textoInformar}'
                }
            },
            {
				xtype: 'textarea',
                fieldLabel : getMultiLangLabel(appI18N.reuniones.textoAdicional, alternativeLanguage),
                width: 500,
                height: 200,
                name : 'textoInformarAlternativo',
                bind : {
                    value : '{textoInformarAlternativo}'
                },
                hidden:!isMultilanguageApplication()
			}
        ]
    });