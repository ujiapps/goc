Ext.define('goc.view.reunion.FormBorradorConvocatoriaController',
	{
		extend: 'Ext.app.ViewController',
		alias: 'controller.formBorradorConvocatoriaController',


onLoad: function(){
	var view = this.getView()
	var vm = view.getViewModel()
	var record = vm.get('record')
	var ref = this;
    var organosStore = vm.get('organosStore')
	organosStore.load({
			params: { reunionId:record.get('id') },
			callback: function(records, options, success) {
				ref.anyadirCamposDefault(records)
			}
		})
	},
	onClose : function()
        {
            var win = Ext.WindowManager.getActive();

 			if(win)
			{
			win.destroy();
			}
 },

anyadirCamposDefault:function(records){
	var view = this.getView()
	var vm = view.getViewModel()
	var organosStore = vm.get('organosStore')
	organosStore.load({
		callback: function() {
			records.forEach(function(record){
			var organo = organosStore.findRecord('id', record.get('id'))
			var fieldset = view.down('fieldset')
			fieldset.add({
				value: organo.get('email'),
				allowBlank: false,
				fieldLabel: 'Email',
				vtype: 'email'
				})
			})

		}
	})

},

anyadirCampos: function() {
	var view = this.getView()
	var fieldset = view.down('fieldset')

	fieldset.add({
		allowBlank:false,
		fieldLabel: 'Email',
		vtype: 'email'
	})
},



listaEmails: function(){
	var view = this.getView()
	var items = view.down('fieldset').items.items;
	var listaEmails = []
	items.forEach(function(item) {
		listaEmails.push(item.value)
		})
	return listaEmails;
	},
	
	guardarCorreos:function(){
	var view = this.getView()
	var vm = view.getViewModel()
	var record = vm.get('record')
	var ref = this;
	var listaEmails = this.listaEmails()
	Ext.Ajax.request({
		url:'rest/reuniones/'+record.get('id') + '/enviarborrador',
		method: 'PUT',
		jsonData: { emails: listaEmails},
		success: function() {
			ref.onClose()
		}
	})
},
	
	
})