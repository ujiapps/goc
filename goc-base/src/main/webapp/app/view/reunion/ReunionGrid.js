var reunionesGridColumns = [
    {
        text: 'Id',
        width: 80,
        dataIndex: 'id',
        hidden: true
    },
    {
        text: getMultiLangLabel(appI18N.reuniones.asunto, mainLanguage),
        dataIndex: 'asunto',
        flex: 1,
        renderer: function (asunto, celda, record) {
            return asunto + '&nbsp;&nbsp;<a class="link" target="_blank" href="/goc/rest/publicacion/reuniones/' + record.get('id') + '?lang=' + mainLanguage + '"><i class="fa fa-external-link"></i></a>';
        }
    }
];

if (isMultilanguageApplication()) {
    reunionesGridColumns.push({
        text: getMultiLangLabel(appI18N.reuniones.asunto, alternativeLanguage),
        dataIndex: 'asuntoAlternativo',
        flex: 1,
        renderer: function (asunto, celda, record) {
            return asunto + '&nbsp;&nbsp;<a class="link" target="_blank" href="/goc/rest/publicacion/reuniones/' + record.get('id') + '?lang=' + alternativeLanguage + '"><i class="fa fa-external-link"></i></a>';
        }
    });
}

reunionesGridColumns.push({
    text: appI18N.reuniones.fecha,
    dataIndex: 'fecha',
    width: 150,
    format: 'd/m/Y H:i',
    xtype: 'datecolumn'
});

reunionesGridColumns.push({
    text: appI18N.reuniones.duracion,
    dataIndex: 'duracion',
    renderer: function (value) {
        var duracionTexto =
            {
                30: '0,5 ' + appI18N.reuniones.horas,
                60: '1 ' + appI18N.reuniones.hora,
                90: '1,5 ' + appI18N.reuniones.horas,
                120: '2 ' + appI18N.reuniones.horas,
                150: '2,5 ' + appI18N.reuniones.horas,
                180: '3 ' + appI18N.reuniones.horas
            };

        return duracionTexto[value];
    }
});

reunionesGridColumns.push({
    dataIndex: 'avisoPrimeraReunion',
    text: appI18N.reuniones.enviada,
    align: 'center',
    reference: 'avisoPrimeraReunion',
    renderer: function (value, metadata, record) {
        if (value) {
            return '<span data-qwidth="200" ' +
                'data-qtip="' + record.get("avisoPrimeraReunionUser") + ' ' + Ext.util.Format.date(record.get("avisoPrimeraReunionFecha"), 'd/m/Y H:i') + '">Sí</span>';
        }

        return 'No';
    }
});

reunionesGridColumns.push({
    dataIndex: 'numeroDocumentos',
    text: appI18N.reuniones.documentos,
    align: 'center',
    reference: 'documentos',
    renderer: function (value) {
        if (value > 0) {
            return '<span class="fa fa-file-text-o"></span>';
        }
        return '';
    }
});

Ext.define('goc.view.reunion.ReunionGrid',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.reunionGrid',
        requires: ['goc.view.reunion.ReunionGridController'],
        controller: 'reunionGridController',
        bind: {
            store: '{reunionesStore}'
        },
        name: 'reunion',
        reference: 'reunionGrid',
        multiSelect: false,
        scrollable: true,
        title: appI18N.reuniones.tituloGridReuniones,
        border: 0,
        columns: reunionesGridColumns,
        tbar: [],
        bbar: {
            xtype: 'pagingtoolbar'
        },

        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            title: 'searchReunion',
            items: [{
                xtype: 'textfield',
                emptyText: appI18N.reuniones.buscarReunion,
                flex: 8,
                name: 'searchReunion',
                listeners: {
                    change: 'debouncedOnSearchReunion'
                }
            }, '->',
                {
                    xtype: 'comboReunionTipoOrgano',
                    margin: '0 10 10 0',
                    listeners: {
                        tipoOrganoSelected: 'tipoOrganoSelected'
                    }
                },
                {
                    xtype: 'comboOrgano',
                    margin: '0 0 10 0',
                    listeners: {
                        organoSelected: 'organoSelected'
                    }
                }]

        }, {
            xtype: "toolbar",
            dock: "top",
            title: "buttonToolbar",
            items: [
                {
                    xtype: 'button',
                    iconCls: 'fa fa-plus',
                    text: appI18N.common.anadir,
                    handler: 'onAdd'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-edit',
                    name: 'botonEditar',
                    text: appI18N.common.editar,
                    handler: 'onEdit'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-remove',
                    name: 'botonBorrar',
                    text: appI18N.common.borrar,
                    handler: 'onDelete',
                    bind: {
                        disabled: '{reunionGrid.selection.completada}'
                    }
                }, ' | ',
                {
                    xtype: 'button',
                    iconCls: 'fa fa-list-ol',
                    name: 'botonOrdenDia',
                    text: appI18N.reuniones.ordenDia,
                    handler: 'onOpenPuntos'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-check',
                    name: 'botonDocumentacion',
                    text: appI18N.reuniones.documentacionAdjunta,
                    handler: 'onAttachmentEdit'
                },
                {
                    xtype: 'button',
                    iconCls: 'x-fa fa-user',
                    name: 'botonAsistencia',
                    text: appI18N.reuniones.asistencia,
                    handler: 'onDetalleAsistentesReunion'

                }, ' | ', {
                    xtype: 'splitbutton',
                    text: appI18N.reuniones.acciones,
                    iconCls: 'fa fa-gears',
                    menu: [
                        {
                            iconCls: 'fa fa-envelope-o',
                            name: 'botonEnviarConvocatoria',
                            text: appI18N.reuniones.enviarConvocatoria,
                            handler: 'onEnviarConvocatoria',
                            style: {
                                fontSize: '1.4em'
                            }
                        },

                        {
                            iconCls: 'fa fa-envelope-o',
                            name: 'botonEnviarBorradorConvocatoria',
                            text: appI18N.reuniones.enviarBorradorConvocatoria,
                            handler: 'onEnviarBorradorConvocatoria',
                            style: {
                                fontSize: '1.4em'
                            }
                        },

                        {
                            iconCls: 'fa fa-comments',
                            name: 'botonAcuerdosDeliberaciones',
                            text: appI18N.reuniones.editarAcuerdosDeliberaciones,
                            handler: 'onEditarAcuerdosDeliberaciones',
                            style: {
                                fontSize: '1.4em'
                            }
                        },
                        {
                            iconCls: 'fa fa-pencil-square-o',
                            name: 'fullaFirmes',
                            text: appI18N.reuniones.hojaFirmas,
                            handler: 'onHojaFirmas',
                            style: {
                                fontSize: '1.4em'
                            }
                        },
                        {
                            iconCls: 'fa fa-lock',
                            name: 'botonCerrar',
                            text: appI18N.reuniones.cerrarActa,
                            handler: 'onCompleted',
                            style: {
                                fontSize: '1.4em'
                            }
                        },
                        {
                            iconCls: 'fa fa-clone',
                            name: 'botonDuplicarReunion',
                            text: getMultiLangLabel(appI18N.reuniones.duplicarReunion),
                            handler: 'onDuplicarReunion',
                            style: {
                                fontSize: '1.4em'
                            }
                        },
                        {
                            iconCls: 'fa fa-cloud-upload',
                            name: 'botonSubirHojaFirmas',
                            text: getMultiLangLabel(appI18N.reuniones.subirHojaFirmas),
                            handler: 'onSubirHojaFirmas',
                            style: {
                                fontSize: '1.4em'
                            }
                        },
                        {
                            iconCls: 'fa fa-pencil',
                            name: 'botonEnviarEmail',
                            text: getMultiLangLabel(appI18N.reuniones.enviarEmailConvocados),
                            handler: 'onEnviarEmailConvocados',
                            style: {
                                fontSize: '1.4em'
                            }
                        },
                        {
                            iconCls: 'fa fa-refresh',
                            name: 'botonBotonSincronizar',
                            text: appI18N.reuniones.sincronizarMiembrosYCargos,
                            handler: 'sincronizarMiembrosYCargosReunion',
                            style: {
                                fontSize: '1.4em'
                            }
                        },
                        {
                            iconCls: 'fa fa-send',
                            name: 'botonInformarOtrosOrganos',
                            text: getMultiLangLabel(appI18N.reuniones.informarOtrosOrganos),
                            handler: 'onInformarOtrosOrganos',
                            style: {
                                fontSize: '1.4em'
                            },
                            hidden: !mostrarInformarOtrosOrganos
                        },
                        {
                            iconCls: 'fa fa-arrow-right',
                            name: 'masOpciones',
                            text: appI18N.reuniones.masOpciones,
                            handler: 'onMasOpciones',
                            hidden: enlaceMasOpciones == null || enlaceMasOpciones === '' || enlaceMasOpciones == 'null',
                            style: {
                                fontSize: '1.4em'
                            }
                        },
                        {
                            iconCls: 'fa fa-upload',
                            name: 'masOpciones',
                            text: appI18N.reuniones.publicarAcuerdos,
                            handler: 'onPublicarAcuerdos',
                            hidden: mostrarPublicarAcuerdos == null || mostrarPublicarAcuerdos === '' || !mostrarPublicarAcuerdos,
                            style: {
                                fontSize: '1.4em'
                            }
                        }
                    ]
                }
            ],
        }],
        listeners: {
            render: 'onLoad',
            celldblclick: 'onOpenPuntos',
            afterEdit: 'onAfterEdit',
            organoSelected: 'organoSelected',
            tipoOrganoSelected: 'tipoOrganoSelected'
        }
    });
