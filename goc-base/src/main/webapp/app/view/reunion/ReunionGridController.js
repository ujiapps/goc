Ext.define('goc.view.reunion.ReunionGridController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.reunionGridController',

    init: function () {
        this.debouncedOnSearchReunion = this.debounce(this.onSearchReunion, 300);
    },

    onLoad: function () {
        var viewModel = this.getViewModel();
        viewModel.getStore('reunionesStore').load();
    },

    onAdd: function () {
        this.createModalReunion(null);
    },

    onEdit: function (grid, td, cellindex) {
        if (grid && grid.getHeaderCt) {
            var cell = grid.getHeaderCt().getHeaderAtIndex(cellindex);

            if (cell.getReference() === 'documentos') {
                return this.onAttachmentEdit();
            }
        }

        grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record) {
            return Ext.Msg.alert(appI18N.common.edicionRegistro, appI18N.common.seleccionarParaEditarRegistro);
        }

        var ref = this;

        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + record.id,
            method: 'GET',
            success: function (data) {
                record.set(Ext.decode(data.responseText).data);

                ref.createModalReunion(record);
            }
        });
    },

    onDuplicarReunion: function (grid, td, cellindex) {
        grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record) {
            return Ext.Msg.alert(appI18N.common.edicionRegistro, appI18N.common.seleccionarParaDuplicarRegistro);
        }

        var ref = this;

        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + record.id,
            method: 'GET',
            success: function (data) {
                record.set(Ext.decode(data.responseText).data);
                record.set('fecha', "");
                record.set('fechaFinVotacion', "");
                record.set('horaFinVotacion', "");
                record.set('numeroSesion', "");
                record.set('idReunionOriginal', record.id);

                if (exprMeetingTitle)
                {
                    var exprMeetingTitleRegExp = new RegExp(exprMeetingTitle.replace(/\{[^}]+\}/g, ".+"));
                    if (exprMeetingTitleRegExp.test(record.data.asunto))
                    {
                        record.set('asunto', exprMeetingTitle);
                        record.set('asuntoAlternativo', (exprMeetingTitleAlternative) ? exprMeetingTitleAlternative : '');
                    }
                }
                ref.createModalReunion(record);
            }
        });
        var viewModel = this.getViewModel();
        viewModel.getStore('reunionesStore').load();
    },

    onEnviarConvocatoria: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewport = this.getView().up('viewport');

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.enviarConvocatoria, appI18N.reuniones.seleccionarParaEnviarConvocatoria);
        }

        var mensaje = appI18N.reuniones.confirmacionEnvioMensaje;

        if (record.get('avisoPrimeraReunion')) {
            mensaje = appI18N.reuniones.confirmacionEnvioMensajeYaEnviado;
        }

        Ext.Msg.confirm(appI18N.reuniones.confirmacionEnvioTitulo, mensaje, function (result) {
            if (result === 'yes') {

                this.modal = viewport.add({
                    xtype: 'formTextoConvocatoria',
                    viewModel: {
                        data: {
                            record: record,
                            reunionesStore: grid.getStore()
                        }
                    }
                });
                this.modal.show();

                // Ext.Ajax.request(
                // {
                //     url : '/goc/rest/reuniones/' + record.get('id') + '/enviarconvocatoria',
                //     method : 'PUT',
                //     success : function(response)
                //     {
                //         var data = Ext.decode(response.responseText);
                //         var mensajeRespuesta = (data.message && data.message.indexOf("appI18N") != -1) ? eval(data.message) : data.message;
                //
                //         Ext.Msg.alert(appI18N.reuniones.resultadoEnvioConvocatoriaTitle, mensajeRespuesta);
                //
                //         grid.getStore().reload();
                //     },
                //     scope : this
                // }
                // );
            }
        });
    },
	
	onEnviarBorradorConvocatoria:function(){
		var grid = this.getView();
		var record = grid.getView().getSelectionModel().getSelection()[0];
		var viewport = this.getView().up('viewport');
		
		if(!record){
          return Ext.Msg.alert(appI18N.reuniones.enviarConvocatoria, appI18N.reuniones.seleccionarParaEnviarConvocatoria);
		}
		var vm = this.getViewModel();
		var store = vm.getStore('organosStore');
		this.modal = viewport.add({
                    xtype: 'formBorradorConvocatoria',
                    viewModel:{
							data:{
								record:record,
								reunionesStore:grid.getStore(),
								organosStore:store
							}
						}
                });
                this.modal.show();

	},
    onCompleted: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewport = this.getView().up('viewport');

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.cerrarActa, appI18N.reuniones.seleccionarParaCerrarActa);
        }

        Ext.Ajax.request(
            {
                url: '/goc/rest/reuniones/' + record.get('id') + '/checktoclose',
                method: 'GET',
                success: function (response) {
                    var data = Ext.decode(response.responseText);

                    if (data.message) {
                        var mensajeRespuesta = (data.message && data.message.indexOf("appI18N") != -1) ? eval(data.message) : data.message;
                        Ext.Msg.alert(appI18N.reuniones.resultadoEnvioConvocatoriaTitle, mensajeRespuesta);
                        return;
                    }
                    Ext.Ajax.request(
                        {
                            url: '/goc/rest/reuniones/' + record.get('id') + '/acuerdos',
                            method: 'GET',
                            success: function (response) {
                                var asistentesStore = Ext.create('goc.store.OrganoReunionMiembros', {
                                    proxy: {
                                        url: '/goc/rest/reuniones/' + record.get('id') + '/miembros'
                                    },
                                    autoLoad: false
                                });

                                var data = Ext.decode(response.responseText).data;

                                this.modal = viewport.add({
                                    xtype: 'formReunionAcuerdos',
                                    viewModel: {
                                        data: {
                                            title: appI18N.reuniones.titleSingular + ': ' + record.get('asunto'),
                                            reunionId: record.get('id'),
                                            responsableId: data.responsableActaId,
                                            completada: record.get('completada'),
                                            acuerdos: data.acuerdos,
                                            acuerdosAlternativos: record.get('acuerdosAlternativos'),
                                            asistentesStore: asistentesStore
                                        }
                                    }
                                });
                                this.modal.show();
                            }

                        });
                },
                scope: this
            }
        );
    },

    onHojaFirmas: function () {
        var id = this.getView().getSelectedId();

        if (!id) {
            return Ext.Msg.alert(appI18N.reuniones.hojaFirmas, appI18N.reuniones.seleccionarParaHojaFirmas);
        }

        window.open('/goc/rest/reuniones/' + id + '/asistentes?lang=' + appLang, '_blank');
    },

    onEditarAcuerdosDeliberaciones: function () {
        var id = this.getView().getSelectedId();

        if (!id) {
            return Ext.Msg.alert(appI18N.reuniones.editarAcuerdosDeliberaciones, appI18N.reuniones.seleccionarParaEditarAcuerdosDeliberaciones);
        }

        var ref = this;
        var reunionSeleccionada = this.getView().getSelectedRow();

        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + id + '/puntosOrdenDia',
            method: 'GET',
            success: function (data) {
                var arrPuntos = Ext.decode(data.responseText).data;
                if (!arrPuntos || arrPuntos.length === 0)
                    return Ext.Msg.alert(appI18N.reuniones.editarAcuerdosDeliberaciones, appI18N.reuniones.sinPuntosOrdenDia);

                var puntosOrdenDiaModel = [];
                puntosOrdenDiaModel = ref.getPuntosOrdenDiaSinNivel(puntosOrdenDiaModel, arrPuntos);
                ref.createModalPuntosOrdenDia(puntosOrdenDiaModel, reunionSeleccionada.get('asunto'), id);
            }
        });
    },

    getPuntosOrdenDiaSinNivel: function (puntosOrdenDiaModel, arrPuntos) {
        var ref = this;
        var ids = [];
        arrPuntos.forEach(function (arrPunto) {
            if (!ids.includes(arrPunto.id)) {
                var subpuntos = arrPunto.data;
                var puntoOrdenDia = Ext.create('goc.model.PuntoOrdenDiaAcuerdo',
                    {
                        id: arrPunto.id,
                        titulo: arrPunto.titulo,
                        tituloAlternativo: arrPunto.tituloAlternativo,
                        descripcion: arrPunto.descripcion,
                        descripcionAlternativa: arrPunto.descripcionAlternativa,
                        deliberaciones: arrPunto.deliberaciones,
                        deliberacionesAlternativas: arrPunto.deliberacionesAlternativas,
                        acuerdos: arrPunto.acuerdos,
                        acuerdosAlternativos: arrPunto.acuerdosAlternativos,
                        tieneSubpuntos: subpuntos != null
                    });
                puntosOrdenDiaModel.push(puntoOrdenDia);
                ids.push(puntoOrdenDia.id);
                if (subpuntos != null && Array.isArray(subpuntos)) {
                    ref.getPuntosOrdenDiaSinNivel(puntosOrdenDiaModel, subpuntos);
                }
                else if(subpuntos != null) {
                    var puntos = [subpuntos];
                    ref.getPuntosOrdenDiaSinNivel(puntosOrdenDiaModel, puntos);
                }
            }
        });
        return puntosOrdenDiaModel;
    },

    organoSelected: function (id, externo) {
        var grid = this.getView();

        if (id) {
            grid.getStore().load({
                params: {
                    organoId: Transforms.getOrganoId(id, externo),
                    externo: externo
                }
            });
        }
        else {
            var comboTipoOrgano = grid.down('comboReunionTipoOrgano');
            grid.getStore().load({
                params: {
                    tipoOrganoId: comboTipoOrgano.getValue()
                }
            });
        }
    },

    filtraComboOrgano: function (tipoOrganoId) {
        var vm = this.getViewModel();
        var store = vm.getStore('organosStore');
        var filter = new Ext.util.Filter(
            {
                id: 'tipoOrganoId',
                property: 'tipoOrganoId',
                value: tipoOrganoId
            });

        store.addFilter(filter);
    },

    limpiaFiltrosComboOrgano: function () {
        var vm = this.getViewModel();
        var store = vm.getStore('organosStore');
        store.clearFilter();

        var grid = this.getView();

        var comboOrganos = grid.down('comboOrgano');
        comboOrganos.clearValue();
    },

    tipoOrganoSelected: function (id, externo) {
        var grid = this.getView();

        if (id) {
            grid.getStore().load({
                params: {
                    tipoOrganoId: id
                }
            });
            var comboOrganos = grid.down('comboOrgano');
            comboOrganos.clearValue();
            this.filtraComboOrgano(id);
        }
        else {
            grid.getStore().load();
            this.limpiaFiltrosComboOrgano();
        }
    },

    onAttachmentEdit: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var store = Ext.create('goc.store.ReunionDocumentos');
        var viewport = this.getView().up('viewport');

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.documentacion, appI18N.reuniones.seleccionarParaDocumentacion);
        }

        this.modal = viewport.add({
            xtype: 'formDocumentacion',
            viewModel: {
                data: {
                    title: appI18N.reuniones.tituloDocumentacion + ': ' + record.get('asunto'),
                    id: record.get('id'),
                    completada: record.get('completada'),
                    store: store
                }
            }
        });
        this.modal.show();
    },

	  getStoreDeMiembros: function (organoId, externo)
    {
        var vm = this.getViewModel();
        var miembrosStores = vm.get('miembrosStore');
        return miembrosStores[organoId + '_' + externo];
    },

    creaStoreDeMiembros: function (organoId, externo)
    {
        var vm = this.getViewModel();
        var miembrosStores = vm.get('miembrosStore');
        miembrosStores[organoId + '_' + externo] = Ext.create('goc.store.OrganoReunionMiembros');
    },



	onDetalleAsistentesReunion: function() {
		var grid = this.getView();
		var vm = this.getViewModel();
		var reunion = grid.getView().getSelectionModel().getSelection()[0];
		if (!reunion) {
			return Ext.Msg.alert(appI18N.reuniones.verAsistentes, appI18N.reuniones.asistenciaReunionNoSeleccionada);
		}
		var idReunion = reunion.get('id')

		var store = vm.get('organosStore')
		var records = store.load({
			params: { reunionId: idReunion },
			callback: function() {
				if (records.data.length == 0) {
					return Ext.Msg.alert(appI18N.reuniones.reunionSinOrganosTitle, appI18N.reuniones.reunionSinOrganosBody);
				} else {
					var viewport = grid.up('viewport');
					this.modal = viewport.add({
						xtype: 'formReunionMiembros',
						height: '700px',
						width: '1200px',
						viewModel: {
							data: {
								reunionId: idReunion,
								reunionCompletada: idReunion ? reunion.get('completada') : false,
								admiteSuplencia: reunion.get('admiteSuplencia') ? true : false,
								admiteDelegacionVoto: reunion.get('admiteDelegacionVoto') ? true : false,
								admiteComentarios: reunion.get('admiteComentarios') ? true : false,
								organoId: null,
								externo: null,
								organoReuniones: store,
								store: null,
								remoteLoad: true
							}
						}
					})
					store.sync();

					this.modal.show();
				}
			}
		});
	},

    getReunionModalDefinition: function (reunion, reunionesStore, organosStore, invitadosStore) {
        var telematica = false;
        var votacionTelematica = telematica && activarOpcionesPorDefecto;

        return {
            xtype: 'formReunion',
            viewModel: {
                data: {
                    title: reunion ? appI18N.reuniones.edicion + ': ' + reunion.get('asunto') : appI18N.reuniones.nuevaReunion,
                    id: reunion ? reunion.get('id') : undefined,
                    reunion: reunion || {
                        telematica: telematica,
                        votacionTelematica: votacionTelematica,
                        admiteCambioVoto: votacionTelematica && admiteCambioVoto,
                        type: 'goc.model.Reunion',
                        create: true,
                        completada: false
                    },
                    store: reunionesStore,
                    organosStore: organosStore,
                    reunionInvitadosStore: invitadosStore,
                    organosMiembros: {},
                    miembrosStores: {}
                }
            }
        };
    },

    createModalReunion: function (record) {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('reunionesStore');
        var organosStore = Ext.create('goc.store.Organos');
        var invitadosStore = Ext.create('goc.store.ReunionInvitados');
        var viewport = this.getView().up('viewport');
        var ref = this;

        if (record) {
            organosStore.load({
                params: {
                    reunionId: record ? record.get('id') : null
                }
            });

            invitadosStore.proxy.url = '/goc/rest/reuniones/' + record.get('id') + '/invitados';
            
            organosStore.on("load", function () {
                invitadosStore.load();
            });
            

            invitadosStore.on("load", function () {
                var modalDefinition = ref.getReunionModalDefinition(record, store, organosStore, invitadosStore);
                ref.modal = viewport.add(modalDefinition);
                ref.modal.down('textfield[name=urlGrabacion]').setVisible(true);
                ref.modal.show();
            });
        } else {
	       
            var modalDefinition = this.getReunionModalDefinition(record, store, organosStore, invitadosStore);
            this.modal = viewport.add(modalDefinition);
            this.modal.show();
        }
    },

    getPuntosOrdenDiaModalDefinition: function (puntosOrdenDia, reunionTitulo, reunionId) {
        return {
            xtype: 'formOrdenDiaConjuntos',
            viewModel: {
                data: {
                    title: appI18N.reuniones.edicion + ': ' + reunionTitulo,
                    puntosOrdenDia: puntosOrdenDia,
                    reunionId: reunionId
                },
                stores: {
                    puntosOrdenDiaStore: this.getViewModel().getStore('puntosOrdenDiaTreeStore')
                }
            }
        };
    },

    createModalPuntosOrdenDia: function (puntosOrdenDia, reunionTitulo, reunionId) {
        if (puntosOrdenDia && puntosOrdenDia.length > 0) {
            var modalDefinition = this.getPuntosOrdenDiaModalDefinition(puntosOrdenDia, reunionTitulo, reunionId);
            var viewport = this.getView().up('viewport');

            this.modal = viewport.add(modalDefinition);
            this.modal.show();
        }
    },

    reunionSelected: function () {
        var vm = this.getViewModel();
        var grid = this.getView();
        var record = grid.getSelectedRow();
        vm.set('tituloReunionSeleccionada', record.get('asunto'));

        var gridPuntos = Ext.ComponentQuery.query('treepanel[name=ordenDia]')[0];

        if (!record) {
            gridPuntos.clearStore();
            gridPuntos.disable();
            return;
        }

        gridPuntos.fireEvent('reunionSelected', record.get('id'));
    },

    onDelete: function () {
        var grid = this.getView();
        var records = grid.getView().getSelectionModel().getSelection();
        var record = records[0];

        if (records.length === 0) {
            return Ext.Msg.alert(appI18N.common.borradoRegistro || "Esborrar registre", appI18N.common.seleccionarParaBorrarRegistro || "Cal seleccionar un registre per a poder esborrar-lo");
        }

        if (records.length === 1 && records[0].phantom === true) {
            return grid.getStore().remove(records);
        }

        if (records.length > 0) {
            var titulo, mensaje;
            if (record.get('avisoPrimeraReunion')) {
                titulo = appI18N.reuniones.anular;
                mensaje = appI18N.reuniones.anularMensaje;
            }
            else {
                titulo = appI18N.common.borradoRegistro || "Esborrar registre";
                mensaje = appI18N.common.confirmarBorrado || 'Esteu segur/a de voler esborrar el registre ?';
            }
            Ext.Msg.confirm(titulo, mensaje, function (btn, text) {
                if (btn == 'yes') {
                    grid.getStore().remove(records);
                    grid.getStore().sync({
                        failure: function() {
                            grid.getStore().reload();
                        }
                    });
                }
            });
        }
    },

    onSubirHojaFirmas: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewport = this.getView().up('viewport');

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.documentacion, appI18N.reuniones.seleccionarParaDocumentacion);
        }

        this.modal = viewport.add({
            xtype: 'formHojaFirmas',
            viewModel: {
                data: {
                    title: record.get('asunto'),
                    id: record.get('id'),
                    completada: record.get('completada'),
                    record: record,
                    existeHojaFirmas: false,
                    store: Ext.create('Ext.data.Store', {
                        fields: ['nombreFichero'],
                        proxy: {
                            type: 'memory'
                        }
                    })
                }
            }
        });
        this.modal.show();
    },

    onEnviarEmailConvocados: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewport = this.getView().up('viewport');

        if (!record)
        {
            return Ext.Msg.alert(appI18N.reuniones.enviarEmail, appI18N.reuniones.seleccionarParaEnviarEmail);
        }

        this.modal = viewport.add({
            xtype : 'formEnvioEmailConvocados',
            viewModel : {
                data : {
                    record: record,
                    reunionesStore: grid.getStore()
                }
            }
        });
        this.modal.show();
    },

    onInformarOtrosOrganos: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewport = this.getView().up('viewport');

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.informarConvocatoriaAlert, appI18N.reuniones.informarConvocatoriaAlertMessage);
        }

        if (!record.get('avisoPrimeraReunion')) {
            return Ext.Msg.alert(appI18N.reuniones.informarConvocatoriaNoEnviadaAlert, appI18N.reuniones.informarConvocatoriaNoEnviadaAlertMessage);
        }

        this.modal = viewport.add({
            xtype: 'formInformarOtrosOrganos',
            viewModel: {
                data: {
                    record: record,
                    reunionesStore: grid.getStore()
                },
                stores: {
                    organosStore: {
                        type: 'organos'
                    }
                }
            }
        });
        this.modal.show();
    },

    onMasOpciones: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.masOpciones, appI18N.reuniones.seleccionarParaMasOpciones);
        }

        window.open(enlaceMasOpciones + record.get('id'), '_blank');
    },

    onOpenPuntos: function () {
        this.reunionSelected();
        this.getView().up('panel').setActiveItem(1);
    },

    onSearchReunion: function(field, searchString, oldValue, eOpts, me) {
        var viewModel = me.getViewModel();
        var store = viewModel.getStore('reunionesStore');

        if (!searchString || searchString.length <= 3) {
            store.clearFilter();
            return;
        }

        if(searchString.length > 3) {
			searchString = searchString.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            
            var filter = new Ext.util.Filter(
                {
                    id: 'search',
                    property: 'query',
                    value: searchString
                });

            store.addFilter(filter);
        }
    },

    debounce: function (fn, delay) {
        var timerId;
        return function () {
            if (timerId) {
                clearTimeout(timerId);
            }
            var argumentsChange = arguments;
            var me = this;
            timerId = setTimeout(function() {
                fn(argumentsChange[0], argumentsChange[1], argumentsChange[2], argumentsChange[3], me);
                timerId = null;
            }, delay);
        };
    },

    onPublicarAcuerdos: function() {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.acuerdosPublicadosTitle, appI18N.reuniones.seleccionarParaPublicarAcuerdos);
        }

        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + record.get('id') + '/publicaracuerdos',
            method: 'POST',
            success: function (data) {
                Ext.Msg.alert(appI18N ? appI18N.reuniones.acuerdosPublicadosTitle : 'Publicació d\'acords.', appI18N ? appI18N.reuniones.acuerdosPublicados : 'Els acords s\'han publicat correctament.');
            },
            failure: function (err) {
                view.setLoading(false);
            }
        });
    },

    sincronizarMiembrosYCargosReunion: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.sincronizarMiembrosYCargos, appI18N.reuniones.seleccionarParaSincronizarMiembros);
        }

        Ext.Ajax.request({
           url: '/goc/rest/reuniones/' + record.get('id') + '/sincronizar',
            method: 'PUT',
            success: function(){
               grid.getStore().reload();
               Ext.Msg.alert(appI18N.reuniones.sincronizarMiembrosYCargos, appI18N.reuniones.SeHaPodidoSincronizar);
            },
            failure: function(){
                Ext.Msg.alert(appI18N.reuniones.sincronizarMiembrosYCargos, appI18N.reuniones.noSeHaPodidoSincronizar);
            }
        });
    }

});
