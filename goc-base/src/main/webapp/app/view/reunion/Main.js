Ext.define('goc.view.reunion.Main',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.reunionMain',
    name : 'reuniones',
    viewModel : {
        type : 'reunionViewModel'
    },
    requires : [
        'goc.view.reunion.ViewModel', 'goc.view.reunion.ReunionGrid', 'goc.view.reunion.OrdenDiaGrid',
        'goc.view.reunion.FormReunion', 'goc.view.reunion.FormOrdenDia',
        'goc.view.reunion.FormReunionAcuerdos', 'goc.view.reunion.FormDocumentacion',
        'goc.view.reunion.FormReunionMiembros', 'goc.view.common.ComboTipoOrgano', 'goc.view.common.ComboOrgano',
        'goc.view.reunion.GridDescriptoresOrdenDia', 'goc.store.DescriptoresOrdenDia', 'goc.store.Descriptores',
        'goc.store.Claves',
        'goc.view.reunion.FormDescriptoresOrdenDia','goc.view.reunion.InvitadoGrid', 'goc.store.ReunionInvitados',
        'goc.view.reunion.FormOrdenDiaConjuntos', 'goc.view.reunion.FormHojaFirmas', 'goc.view.reunion.FormTextoConvocatoria',
        'goc.view.reunion.FormEnvioEmailConvocados',
        'goc.view.reunion.FormInformarOtrosOrganos',
        'goc.view.reunion.FormOrdenDiaDocumentacionYAdjuntos','goc.store.PuntosOrdenDiaAdjuntosCombo'
    ],
    title : appI18N.reuniones.tituloPrincipal,
    layout : 'fit',

    items : [
        {
            xtype : 'panel',
            layout : 'card',
            items : [
                {
                    xtype : 'reunionGrid',
                    width : '100%'
                },
                {
                    xtype : 'ordenDiaGrid',
                    width : '100%'
                }
            ]
        }
    ]
});
