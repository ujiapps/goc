Ext.define('goc.view.reunion.FormEnvioEmailConvocadosController',
    {
        extend : 'Ext.app.ViewController',
        alias : 'controller.formEnvioEmailConvocadosController',


        onClose : function()
        {
            var win = Ext.WindowManager.getActive();
            if (win)
            {
                win.destroy();
            }
        },
        
        
        onEnviarEmail: function (button)
        {
            button.disable();
            var textareaMensaje = this.getView().down('textarea');
            textareaMensaje.validate();
            if(textareaMensaje.getValue() !== '')
            {
                var vm = this.getViewModel();
                var record = vm.get('record');
                var ref = this;
                Ext.Ajax.request(
                    {
                        url : '/goc/rest/reuniones/' + record.get('id') + '/enviaremail',
                        method : 'PUT',
                        jsonData: {cuerpoEmail: textareaMensaje.getValue() },
                        success : function(response)
                        {
                            var data = Ext.decode(response.responseText);
                            var mensajeRespuesta = (data.message && data.message.indexOf("appI18N") != -1) ? eval(data.message) : data.message;

                            vm.get('reunionesStore').reload();
                            ref.onClose();

                            Ext.Msg.alert(appI18N.reuniones.enviarEmail, mensajeRespuesta);
                            button.enable();
                        },
                        failure: function() {
                            button.enable();
                        },
                        scope : this
                    }
                );
            }
            else {
                button.enable();
            }
        }
});
