var gridColumns = [
    {
        xtype: 'actioncolumn',
        title: appI18N.reuniones.posicion,
        align: 'right',
        width: 30,
        items: [{
            iconCls: 'x-fa fa-arrow-up',
            tooltip: appI18N.reuniones.subir,
            isDisabled: function (grid) {
                var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
                var record = reunionGrid.getView().getSelectionModel().getSelection()[0];
                return record.get('completada');
            },
            handler: function (grid, index) {
                var rec = grid.getStore().getAt(index);
                var docId = rec.get('id');
                var tipo = rec.get('tipo');
                grid.up('formOrdenDiaDocumentacionYAdjuntos').fireEvent('subeOrdenDocumento', docId, tipo);
            }
        }]
    }
];

gridColumns.push({
    xtype: 'actioncolumn',
    title: appI18N.reuniones.posicion,
    align: 'right',
    width: 30,
    items: [{
        iconCls: 'x-fa fa-arrow-down',
        tooltip: appI18N.reuniones.bajar,
        isDisabled: function (grid) {
            var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
            var record = reunionGrid.getView().getSelectionModel().getSelection()[0];
            return record.get('completada');
        },

        handler: function (grid, index) {
            var rec = grid.getStore().getAt(index);
            var docId = rec.get('id');
            var tipo = rec.get('tipo');
            grid.up('formOrdenDiaDocumentacionYAdjuntos').fireEvent('bajaOrdenDocumento', docId, tipo);
        }
    }]
})
gridColumns.push({
    text: appI18N.adjuntos.nombre,
    dataIndex: 'nombre',
    flex: 1
})

gridColumns.push({
    text: getMultiLangLabel(appI18N.reuniones.descripcion, mainLanguage),
    dataIndex: 'descripcion',
    flex: 1,
    editor: {
        field: {
            allowBlank: false
        }
    }
});

if (isMultilanguageApplication()) {
    gridColumns.push({
        text: getMultiLangLabel(appI18N.reuniones.descripcion, alternativeLanguage),
        dataIndex: 'descripcionAlternativa',
        flex: 1,
        editor: {
            field: {
                allowBlank: false
            }
        }
    });
}

gridColumns.push({
    text: appI18N.adjuntos.tipo,
    dataIndex: 'tipo',
    flex: 1,
    renderer: function (value, record) {
        return appI18N.adjuntos.tipos[value.toLowerCase()];
    }

});

gridColumns.push({
    text: appI18N.adjuntos.publicarEnBuscador,
    dataIndex: 'publico',
    flex: 1,
    renderer: function (value, meta, rec) {
        if (rec.data.tipo == "ACUERDO") {
            return '';
        }
        return value ? 'Sí' : 'No';
    },
    editor: {
        xtype: 'checkbox'
    }
});

gridColumns.push({
    text: appI18N.reuniones.mostrarEnFicha,
    dataIndex: 'mostrarEnFicha',
    flex: 1,
    renderer: function (value, meta, rec) {
        if (rec.data.tipo == "ACUERDO") {
            return '';
        }
        return value ? 'Sí' : 'No';
    },
    editor: {
        xtype: 'checkbox'
    }
});

gridColumns.push({
    xtype: 'actioncolumn',
    align: 'right',
    width: 30,
    items: [
        {
            iconCls: 'x-fa fa-download',
            tooltip: appI18N.reuniones.descargar,
            handler: function (grid, index) {
                var rec = grid.getStore().getAt(index);
                var documentoId = rec.get('id');
                var adjuntoTipo = rec.get('tipo');
                grid.up('formOrdenDiaDocumentacionYAdjuntos').fireEvent('descargaPuntoOrdenDiaDocumento', documentoId, adjuntoTipo);
            }
        }
    ]
});

gridColumns.push({
    xtype: 'actioncolumn',
    bind: {
        disabled: '{reunionCompletada}'
    },
    width: 30,
    align: 'right',
    items: [
        {
            iconCls: 'x-fa fa-remove',
            tooltip: appI18N.common.borrar,
            isDisabled: function (grid) {
                return this.disabled;
            },
            handler: function (grid, index) {
                var rec = grid.getStore().getAt(index);
                var adjuntoId = rec.get('id');
                var adjuntoTipo = rec.get('tipo');
                grid.up('formOrdenDiaDocumentacionYAdjuntos').fireEvent('inhabilitaPuntoOrdenDiaDocumento', adjuntoId, adjuntoTipo);
            }
        }
    ]
});

var formItems = [
    {
        xtype: 'textfield',
        fieldLabel: getMultiLangLabel(appI18N.reuniones.descripcion, mainLanguage),
        allowBlank: true,
        emptyText: appI18N.reuniones.descripcion,
        width: '100%',
        flex: 1,
        itemId: 'descripcion',
        name: 'descripcion',
        reference: 'descripcion'
    }
];

if (isMultilanguageApplication()) {
    formItems.push({
        xtype: 'textfield',
        fieldLabel: getMultiLangLabel(appI18N.reuniones.descripcion, alternativeLanguage),
        allowBlank: true,
        emptyText: appI18N.reuniones.descripcion,
        width: '100%',
        flex: 1,
        name: 'descripcionAlternativa',
        reference: 'descripcionAlternativa'
    });
}

formItems.push({
    xtype: 'combobox',
    fieldLabel: getMultiLangLabel(appI18N.reuniones.tipo, alternativeLanguage),
    bind: {
        store: '{storeCombo}'
    },
    id: 'tipoAdjuntoSubida',
    displayField: 'name',
    valueField: 'id',
    allowBlank: true,
    editable: false,
    name: 'tipo',
    value: 'ADJUNTO',
    reference: 'tipo',
    renderer: function (value, record) {
        return appI18N.adjuntos.tipos[value.toLowerCase()];
    },

});


formItems.push({
    xtype: 'filefield',
    buttonOnly: true,
    width: 200,
    name: 'documento',
    buttonConfig: {
        text: appI18N.common.seleccionarFichero
    },
    listeners: {
        change: 'onFileChangeOrdenDia'
    }
});

formItems.push({
    xtype: 'displayfield',
    width: '100%',
    name: 'nombreDocumento',
    reference: 'nombreDocumento'
});

Ext.define('goc.view.reunion.FormOrdenDiaDocumentacionYAdjuntos',
    {
        extend: 'Ext.window.Window',
        xtype: 'formOrdenDiaDocumentacionYAdjuntos',
        autoScroll: true,
        bind: {
            title: '{title}'
        },
        width: '80%',
        modal: true,
        bodyPadding: 10,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        requires: ['goc.view.reunion.FormOrdenDiaDocumentacionYAdjuntosController'],
        controller: 'formOrdenDiaDocumentacionYAdjuntosController',

        bbar: [
            '->',
            {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                listeners: {
                    render: function (component) {
                        component.getEl().on('click', 'onClose');
                    }
                }
            }
        ],

        items: [
            {
                xtype: 'ujigridpanel',
                scrollable: true,
                flex: 1,
                margin: '5 0 5 0',
                controller: null,
                allowEdit: true,
                viewConfig: {
                    emptyText: appI18N.reuniones.documentacionAdjuntaVacia
                },
                bind: {
                    store: '{store}'
                },
                columns: gridColumns,
                listeners: {
                    select: 'onSelect',
                    edit: 'onBeforeEditComplete',
                    beforeedit: 'cancelAcuerdoCheckBox'
                },
                tbar: undefined,
                bbar: undefined,
                autoScroll: true,
                minHeight: 150
            },
            {
                xtype: 'form',
                autoScroll: true,
                bind: {
                    disabled: '{reunionCompletada}'
                },
                frame: true,
                title: appI18N.reuniones.subirNuevoDocumento,
                layout: 'anchor',
                border: false,
                padding: 5,
                name: 'subirDocumento',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idDoc',
                        reference: 'idDoc'
                    },
                    {
                        xtype: 'fieldcontainer',
                        anchor: '100%',
                        name: 'fieldContainer',
                        reference: 'fieldContainer',
                        layout: 'vbox',
                        items: formItems
                    }
                ],
                buttons: [
                    {
                        text: appI18N.reuniones.subirDocumento,
                        reference: 'btnSubirArchivo',
                        handler: 'subirPuntoOrdenDiaAdjunto'
                    },
                    {
                        xtype: 'panel',
                        html: '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.limpiarFormulario + '</a>',
                        listeners: {
                            render: function (component) {
                                component.getEl().on('click', 'limpiarFormulario');
                            }
                        }
                    }
                ]
            }
        ],
        listeners: {
            render: 'onLoad',
            subirPuntoOrdenDiaAdjunto: 'subirPuntoOrdenDiaAdjunto',
            descargaPuntoOrdenDiaDocumento: 'descargaPuntoOrdenDiaDocumento',
            borraPuntoOrdenDiaDocumento: 'borraPuntoOrdenDiaDocumento',
            updatePuntoOrdenDiaDocumento: 'updatePuntoOrdenDiaDocumento',
            inhabilitaPuntoOrdenDiaDocumento: 'inhabilitaPuntoOrdenDiaDocumento',
            subeOrdenDocumento: 'subeOrdenDocumento',
            bajaOrdenDocumento: 'bajaOrdenDocumento'
        }
    });
