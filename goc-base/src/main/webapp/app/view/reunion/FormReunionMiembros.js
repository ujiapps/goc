Ext.define('goc.view.reunion.FormReunionMiembros', {
    extend: 'Ext.window.Window',
    xtype: 'formReunionMiembros',
    width: 850,
    modal: true,
    name: 'formReunionMiembros',
    bodyPadding: 10,
    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'
    },
    title: appI18N.reuniones.gestionAsistentes,

    requires: [
        'goc.view.reunion.FormReunionMiembrosController'
    ],
    controller: 'formReunionMiembrosController',
    viewModel: {
        type: 'reunionViewModel'
    },

    items: [

        {
            xtype: 'combobox',
            id: 'comboOrganos',
            valuefield: 'id',
            displayField: (appLang === alternativeLanguage ? 'nombreAlternativo' : 'nombre'),
            bind: {
                store: '{organosStore}'
            },
            queryMode: 'local',
            triggerAction: 'all',
            fieldLabel: appI18N.miembros.organo,
            emptyText: 'Selecciona un organo...',
            editable: false,
            listeners: {
                select: 'onSelectOrganos'
            }
        },
        {
            xtype: 'grid',
            flex: 1,
            height: 300,
            scrollable: true,
            margin: '5 0 5 0',
            viewConfig: {
                emptyText: appI18N.reuniones.sinAsistentes,
                markDirty: false
            },
            tbar: [
                {
                    xtype: 'button',
                    iconCls: 'fa fa-plus',
                    text: appI18N.reuniones.anadirSuplente,
                    handler: 'onAddSuplente',
                    bind: {
                        disabled: '{!admiteSuplencia || selectedMiembro.delegadoVotoId}'
                    }
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-edit',
                    text: appI18N.reuniones.borrarSuplente,
                    handler: 'onRemoveSuplente',
                    bind: {
                        disabled: '{!admiteSuplencia}'
                    }
                },
                {
                    xtype: 'textfield',
                    emptyText: appI18N.reuniones.buscaAsistente,
                    name: 'searchMiembro',
                    listeners: {
                        change: 'onSearchMiembro'
                    }
                },
            ],
            bind: {
                store: '{store}',
                selection: '{selectedMiembro}'
            },
            columns: [
                {
                    text: appI18N.common.nombre,
                    dataIndex: 'nombre',
                    flex: 2
                },
                {
                    text: appI18N.miembros.cargo,
                    dataIndex: 'cargoNombre',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.correo,
                    dataIndex: 'email',
                    flex: 1
                },
                {
                    dataIndex: 'suplenteId',
                    hidden: true
                },
                {
                    text: appI18N.reuniones.suplente,
                    dataIndex: 'suplenteNombre',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.suplenteEmail,
                    dataIndex: 'suplenteEmail',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.delegacionVoto,
                    dataIndex: 'delegadoVotoNombre',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.delegacionVotoEmail,
                    dataIndex: 'delegadoVotoEmail',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.asistencia,
                    xtype: 'checkcolumn',
                    dataIndex: 'asistencia',
                    bind: {
                        disabled: '{reunionCompletada}'
                    },
                    listeners: {
                        checkchange: 'onChangeAsistencia'
                    },
                    renderer: function (value, metadata, rec) {
                        if (rec.get('asistenciaBloqueada')) {
                            metadata.tdCls = 'x-item-disabled';
                        }

                        return this.defaultRenderer(value, metadata);
                    }
                },
                {
                    text: appI18N.reuniones.justificaAusencia,
                    xtype: 'checkcolumn',
                    dataIndex: 'justificaAusencia',
                    listeners: {
                        checkchange: 'onChangeJustificaAusencia',
                    }
                },
                {
                    text: appI18N.reuniones.motivoAusencia,
                    dataIndex: 'motivoAusencia',
                    cls: 'motivoAusenciaCls',
                    tdCls: 'motivoAusenciaTdCls',
                    flex: 1,
                    renderer: function (value, metaData, record, rowIdx, colIdx, store) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    }
                }
            ]
        }
    ],
    bbar: {
        defaultButtonUI: 'default',
        items: [
            '->', {
                xtype: 'button',
                text: appI18N.common.aceptar,
                handler: 'onSave',
                bind: {
                    disabled: '{reunionCompletada}'
                }
            }, {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cerrar + '</a>',
                listeners: {
                    render: function (component) {
                        component.getEl().on('click', 'onClose');
                    }
                }
            }
        ]
    },

    listeners: {
        onSelectOrganos: 'onSelectOrganos',
        show: 'onLoad',
        onSave: 'onSave',
        onAddSuplente: 'onAddSuplente',
        onRemoveSuplente: 'onRemoveSuplente'
    }
});
