Ext.define('goc.view.miembro.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.miembroMainController',
    
    onLoad: function() {
        var viewModel = this.getViewModel();
        viewModel.getStore('organosStore').load({
            url: '/goc/rest/organos/activos/usuario'
        });
    },

    onOrganoSelected: function(combo, record) {
        if (record !== null) {
            var view = this.getView();

            var grid = view.down('grid');
            grid.setHidden(false);

            var buttons = grid.getDockedItems('toolbar')[0].items.items;
            var i;
            var externo = record.data.externo;
            if (externo) {
                for (i = 0; i < buttons.length; i++) {
                    buttons[i].hidden = true;
                }
            } else {
                for (i = 0; i < buttons.length; i++) {
                    buttons[i].hidden = false;
                }
            }

            grid.getStore().load({
                params: {
                    organoId: Transforms.getOrganoId(record.data.id, externo),
                    externo: externo
                }
            });

            var toolbar = grid.down("toolbar");
            toolbar.items.each(function (button) {
                button.setDisabled(externo === "true");
            });
        }
    }
});
