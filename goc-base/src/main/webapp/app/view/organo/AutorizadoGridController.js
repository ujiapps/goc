Ext.define('goc.view.organo.AutorizadoGridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.autorizadoGridController',

    organoSelected : function(record)
    {
        if (record !== null) {
            var externo = record.data.externo;
            this.getViewModel().getStore('organoAutorizadosStore').load(
                {
                    params: {
                        organoId: Transforms.getOrganoId(record.data.id, externo),
                        externo: externo
                    }
                });
        }
    },

    onAdd : function()
    {
        var organosGrid = this.getView().up('panel[alias=widget.organoMainPanel]').down('grid[name=organosGrid]');
        var record = organosGrid.getView().getSelectionModel().getSelection()[0];
        var vm = this.getViewModel();

        if (!record)
            return;

        var window = Ext.create('goc.view.common.LookupWindowPersonas',
        {
            appPrefix : 'goc',
            title : appI18N.organos.seleccionaAutorizado
        });

        window.show();

        var store = vm.getStore('organoAutorizadosStore');
        window.on('LookoupWindowClickSeleccion', function(res)
        {
            var externo = record.data.externo;
            var autorizado = Ext.create('goc.model.OrganoAutorizado',
            {
                personaId : res.get('id'),
                personaNombre : res.get('nombre'),
                personaEmail : res.get('email'),
                organoId : Transforms.getOrganoId(record.data.id, externo),
                organoExterno : externo
            });

            var existeAutorizado = store.find('personaId', autorizado.get('personaId'));
            if (existeAutorizado === -1)
            {
                store.add(autorizado);
                store.getModel().getField('id').persist = false;
                store.sync();
                store.getModel().getField('id').persist = true;
            }
        });
    },

    onDelete : function(grid, td, cellindex)
    {
        grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record)
        {
            return Ext.Msg.alert(appI18N.organos.borrarAutorizacion, appI18N.organos.seleccionarParaBorrarAutorizacion);
        }

        var vm = this.getViewModel();
        var store = vm.getStore('organoAutorizadosStore');

        Ext.Msg.confirm(appI18N.common.borrar, appI18N.organos.borrarAutorizacion, function(result)
        {
            if (result === 'yes')
            {
                store.remove(record);
                store.sync();
            }
        });
    },

    onSubirDocumentosChange : function( checkColumn, rowIndex, checked, record, e, eOpts) {
        var vm = this.getViewModel();
        var store = vm.getStore('organoAutorizadosStore');
        store.sync();
    }
});
