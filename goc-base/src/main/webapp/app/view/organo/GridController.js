Ext.define('goc.view.organo.GridController', {
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.organoGridController',
    onLoad : function()
    {
        var viewModel = this.getViewModel();
        viewModel.getStore('organosStore').load();
        viewModel.getStore('tipoOrganosStore').load({
            callback : function()
            {
                var grid = this.getView();
                grid.getView().refresh();
            },
            scope : this
        });
    },

    afterLoad : function()
    {
        var comboEstado = this.getView().up('organoMainPanel').down('comboEstadoOrgano');
        comboEstado.setValue(false);
    },

    decideRowIsEditable : function(editor, context)
    {
        if(context != null && !context.record.phantom)
        {
            var grid = this.getView();
            var selection = grid.getView().getSelectionModel().getSelection()[0];
            if (!selection) {
                return Ext.Msg.alert(appI18N.common.edicionRegistro || "Edició de registre", appI18N.common.seleccionarParaEditarRegistro || "Cal seleccionar un registre per a poder fer l'edició");
            }

            if (selection.get('externo')) {
                this.deshabilitarCamposFormulario();
            } else {
                this.habilitarCamposFormulario();
            }
        }
        return true;
    },

    deshabilitarCamposFormulario: function() {
        var grid = this.getView();
        var editor = grid.plugins[0];
        editor.editor.items.items[0].disabled = true;
        var input1 = document.getElementById(editor.editor.items.items[0].id + '-inputEl');

        if(input1)
        {
            input1.disabled = true;
        }

        if(isMultilanguageApplication())
        {
            editor.editor.items.items[1].disabled = true;
            var input2 = document.getElementById(editor.editor.items.items[1].id + '-inputEl');
            if(input2)
            {
                input2.disabled = true;
            }
            editor.editor.items.items[2].disabled = true;
            editor.editor.items.items[2].allowBlank = true;
        }
        else
        {
            editor.editor.items.items[1].disabled = true;
            editor.editor.items.items[1].allowBlank = true;
        }
    },

    habilitarCamposFormulario: function() {
        var grid = this.getView();
        var editor = grid.plugins[0];
        editor.editor.items.items[0].disabled = false;
        var input1 = document.getElementById(editor.editor.items.items[0].id + '-inputEl');
        if(input1)
        {
            input1.disabled = false;
        }
        if(isMultilanguageApplication())
        {
            editor.editor.items.items[1].disabled = false;
            var input2 = document.getElementById(editor.editor.items.items[1].id + '-inputEl');
            if(input2)
            {
                input2.disabled = false;
            }
            editor.editor.items.items[2].disabled = false;
            editor.editor.items.items[2].allowBlank = false;
        }
        else
        {
            editor.editor.items.items[1].disabled = false;
            editor.editor.items.items[1].allowBlank = false;
        }
    },

    organoSelected : function(controller, record)
    {
        var grid = this.getView();
        var toolbar = grid.down("toolbar");

        var recordModel = grid.getSelectedRow();
        var gridAutorizados = grid.up('panel').down('grid[name=autorizadoGrid]');
        var gridInvitados = grid.up('panel').down('grid[name=organoInvitadoGrid]');
        var tab = this.getView().up('panel').down('tabpanel');

        if (!recordModel)
        {
            gridAutorizados.clearStore();
            gridInvitados.clearStore();
            tab.disable();

            toolbar.items.each(function(button)
            {
                if (button.name !== 'add')
                {
                    button.setDisabled();
                }
            });

            return;
        }

        toolbar.items.each(function(button)
        {
            if (button.name === 'edit')
            {
                button.setDisabled(false);
            }
        });

        var selection = grid.getView().getSelectionModel().getSelection();

        record = selection[selection.length - 1];
        gridAutorizados.fireEvent('organoSelected', record);
        gridInvitados.fireEvent('organoSelected', record);

        if (record.get('inactivo'))
        {
            return tab.disable();
        }

        tab.enable();
    },

    initFilters : function()
    {
        var grid = this.getView();
        var comboEstado = grid.up('organoMainPanel').down('comboEstadoOrgano');
        var comboTipoOrgano = grid.up('organoMainPanel').down('comboTipoOrgano');
        var organoSearch = grid.up('organoMainPanel').down('[reference=organoSearch]');

        organoSearch.setValue("");
        comboTipoOrgano.clearValue();
        comboEstado.setValue(false);

        var store = this.getStore('organosStore');
        store.clearFilter();

        grid.up('organoMainPanel').fireEvent('filtrarOrganos', false);
    },

    onAdd : function()
    {
        this.habilitarCamposFormulario();
        this.initFilters();

        var grid = this.getView();
        var editor = grid.plugins[0];
        var rec = Ext.create('goc.model.Organo', {
            id: null,
            permiteAbstencionVoto: permiteAbstencionVoto,
            presidenteVotoDoble: presidenteVotoDoble
        });

        grid.getStore().insert(0, rec);
        editor.cancelEdit();
        editor.startEdit(rec, 0);
    },

    cancelEdit : function()
    {
        var editor = this.getView().plugins[0];
        editor.cancelEdit();
    },

    onToggleEstado : function()
    {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (record.phantom === true)
        {
            return grid.getStore().remove(record);
        }

        this.cancelEdit();

        if (record.get('inactivo') === false)
        {
            return Ext.Msg.confirm(appI18N.organos.inhabilitar, appI18N.organos.inhabilitarOrgano, function(btn, text)
            {
                if (btn == 'yes')
                {
                    record.set('inactivo', true);
                    grid.getStore().sync({
                        success : function()
                        {
                            grid.getSelectionModel().clearSelections();
                            grid.getView().refresh();
                            grid.up('panel').down('grid[name=autorizadoGrid]').clearStore();
                            grid.up('panel').down('grid[name=organoInvitadoGrid]').clearStore();
                        }
                    });
                }
            });
        }

        record.set('inactivo', false);
        grid.getStore().sync({
            success : function()
            {
                grid.getSelectionModel().clearSelections();
                grid.getView().refresh();
                grid.up('panel').down('grid[name=autorizadoGrid]').clearStore();
                grid.up('panel').down('grid[name=organoInvitadoGrid]').clearStore();
            }
        });
    },

    onSearchOrgano: function(field, searchString)
    {
        this.getView().up('organoMainPanel').fireEvent('searchOrgano', searchString);
    },

    onExport: function () {
        var grid = this.getView();
        var selection = grid.getView().getSelectionModel().getSelection()[0];
        if (!selection)
        {
            return Ext.Msg.alert(appI18N.organos.exportarOrgano || "Exportar membres", appI18N.organos.exportarOrganoMensaje || "Cal seleccionar un registre per a poder exportar els membres.");
        }
        var url = '/goc/rest/organos/' + Transforms.getOrganoId(selection.data.id, selection.data.externo) + "/export";
        window.open(url,'_blank');
    },

    onEdit : function() {
        var grid = this.getView();
        var selection = grid.getView().getSelectionModel().getSelection()[0];
        if (!selection)
        {
            return Ext.Msg.alert(appI18N.common.edicionRegistro || "Edició de registre", appI18N.common.seleccionarParaEditarRegistro || "Cal seleccionar un registre per a poder fer l'edició");
        }
        var editor = grid.plugins[0];
        editor.cancelEdit();
        editor.startEdit(selection);
    },

    onEditComplete: function (editor, context) {
        var record = context.record;
        record.id = Transforms.getOrganoId(record.id, record.data.externo);
        record.data.id = Transforms.getOrganoId(record.data.id, record.data.externo);
        this.callParent(arguments);
    },

    afterEdit: function() {
        var grid = this.getView();
        var gridAutorizados = grid.up('panel').down('grid[name=autorizadoGrid]');

        setTimeout(function () {
            var selection = grid.getView().getSelectionModel().getSelection()[0];
            gridAutorizados.fireEvent('organoSelected', selection);
        }, 500);
    }
});
