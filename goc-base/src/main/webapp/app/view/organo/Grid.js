var organoGridColumns = [
    {
        text : getMultiLangLabel(appI18N.organos.nombre, mainLanguage),
        dataIndex : 'nombre',
        flex : 1,
        editor : {
            field : {
                allowBlank : false
            }
        }
    }
];

if (isMultilanguageApplication())
{
    organoGridColumns.push({
        text : getMultiLangLabel(appI18N.organos.nombre, alternativeLanguage),
        dataIndex : 'nombreAlternativo',
        flex : 1,
        editor : {
            field : {
                allowBlank : false
            }
        }
    });
}

organoGridColumns.push({
    text : appI18N.organos.tipoOrgano,
    dataIndex : 'tipoOrganoId',
    flex : 1,
    renderer : function(id, meta, rec)
    {
        var store = this.up('organoMainPanel').getViewModel().getStore('tipoOrganosStore');
        var tipoOrganoRecord = store.getById(id);
        return tipoOrganoRecord ? tipoOrganoRecord.get('nombre') : '';
    },
    editor : {
        xtype : 'combobox',
        bind : {
            store : '{tipoOrganosStore}'
        },
        displayField : 'nombre',
        valueField : 'id',
        allowBlank : false,
        editable : false,
        listeners : {
            expand : function()
            {
                this.getStore().reload();
            }
        }
    }
});

organoGridColumns.push({
    text : appI18N.organos.ordinario,
    width : 80,
    dataIndex:'externo',
    renderer : function(id, meta, rec)
    {
        return rec.get('externo') === true ? 'Sí' : 'No';
    }
});

organoGridColumns.push({
    text : appI18N.organos.activo,
    width : 80,
    renderer : function(id, meta, rec)
    {
        return rec.get('inactivo') ? 'No' : 'Sí';
    }
});


organoGridColumns.push({
    text : appI18N.organos.convocarSinOrdenDia,
    dataIndex : 'convocarSinOrdenDia',
    flex : 1,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

if(isAdmin()) {
    organoGridColumns.push({
        text: appI18N.organos.email,
        dataIndex: 'email',
        flex: 1,
        editor: {
            field: {
                allowBlank: true
            }
        }
    });
}

organoGridColumns.push({
    text : appI18N.organos.actaProvisionalActiva,
    dataIndex : 'actaProvisionalActiva',
    flex : 1,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

organoGridColumns.push({
    text : appI18N.organos.delegacionVotoMultiple,
    dataIndex : 'delegacionVotoMultiple',
    flex : 1,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

organoGridColumns.push({
    text: appI18N.organos.permiteAbstencionVoto,
    dataIndex: 'permiteAbstencionVoto',
    flex: 1,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor: {
        xtype: 'checkbox',
        allowBlank: false,
        editable: true
    }
});

organoGridColumns.push({
    text : appI18N.organos.procedimientoVotacion,
    dataIndex : 'tipoProcedimientoVotacionId',
    flex : 1,
    renderer : function(value)
    {
        return value === "ABREVIADO" ? appI18N.procedimientosVotacion.abreviado : appI18N.procedimientosVotacion.ordinario;
    },
    editor : {
        xtype : 'combobox',
        allowBlank: true,
        width: 120,
        triggerAction : 'all',
        queryMode : 'local',
        displayField : 'text',
        valueField : 'value',
        store : Ext.create('Ext.data.Store', {
            fields: [
                'value',
                'text'
            ],
            data: [
                {
                    value: "ABREVIADO",
                    text: appI18N.procedimientosVotacion.abreviado
                },
                {
                    value: "ORDINARIO",
                    text: appI18N.procedimientosVotacion.ordinario
                }
            ]
        })
    }
});

organoGridColumns.push({
    text : appI18N.organos.presidenteVotoDoble,
    dataIndex : 'presidenteVotoDoble',
    flex : 1,
    renderer : function(value)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

organoGridColumns.push({
	text:appI18N.organos.mostrarAsistentes,
	dataIndex: 'mostrarAsistencia',
	flex:1,
	  renderer : function(value)
    {
        return value ? 'Sí' : 'No';
    },
     editor : {
        xtype : 'checkbox',
        allowBlank : true
    }
});

organoGridColumns.push({
	text:appI18N.organos.verDelegaciones,
	dataIndex:'verDelegaciones',
	flex:1,
	renderer:function(value){
		return value ? 'Sí' : 'No'
	},
	editor:{
		xtype: 'checkbox',
		allowBlank:true
	}
});

Ext.define('goc.view.organo.Grid', {
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.organoGrid',
    requires : [
        'goc.view.organo.GridController'
    ],
    controller : 'organoGridController',
    reference : 'organoGrid',
    bind : {
        store : '{organosStore}',
        selection : '{selectedOrgano}'
    },
    name : 'organosGrid',
    title : appI18N ? appI18N.organos.titulo : 'Òrgans',
    scrollable : true,
    selModel: {
        mode: 'SINGLE'
    },
    collapsible : true,
    tbar : [
        {
            xtype : 'button',
            name : 'add',
            iconCls : 'fa fa-plus',
            text : appI18N ? appI18N.common.anadir : 'Afegir',
            handler : 'onAdd'
        },
        {
            xtype : 'button',
            name : 'edit',
            iconCls : 'fa fa-edit',
            text : appI18N ? appI18N.common.editar : 'Editar',
            handler : 'onEdit'
        },
        {
            xtype : 'button',
            iconCls : 'fa fa-remove',
            text : 'Inhabilita',
            handler : 'onToggleEstado',
            hidden : true,
            bind : {
                hidden : '{ocultaBotonInhabilita}'
            }
        },
        {
            xtype : 'button',
            iconCls : 'fa fa-check-circle',
            text : 'Habilita',
            handler : 'onToggleEstado',
            bind : {
                hidden : '{ocultaBotonHabilita}'
            }
        }, ' | ',
        {
            xtype : 'button',
            name : 'export',
            iconCls : 'fa fa-file-o',
            text : appI18N ? appI18N.common.exportarMiembros : 'Exportar miembros',
            handler : 'onExport'
        }, '->',
        {
            xtype : 'comboEstadoOrgano'
        },
        {
            xtype : 'comboTipoOrgano'
        },
        {
            xtype : 'textfield',
            emptyText : appI18N.organos.buscarOrgano,
            width : 380,
            reference: 'organoSearch',
            listeners : {
                change : 'onSearchOrgano'
            }
        }
    ],

    columns : organoGridColumns,

    listeners : {
        render : 'onLoad',
        afterrender : 'afterLoad',
        beforeedit : 'decideRowIsEditable',
        selectionChange : 'organoSelected',
        edit: 'afterEdit'
    }
});
