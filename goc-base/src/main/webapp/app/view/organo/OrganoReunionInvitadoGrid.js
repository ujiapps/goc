Ext.define('goc.view.organo.OrganoReunionInvitadoGrid',
{
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.organoReunionInvitadoGrid',
    plugins : [],
    requires : ['goc.view.organo.OrganoReunionInvitadoGridController'],
    controller : 'organoReunionInvitadoGridController',
    name : 'organoReunionInvitadoGrid',
    title : appI18N.organos.invitados,
    multiSelect : true,
    scrollable : true,
    height: 160,
    bind : {
        store : '{organoReunionInvitadosStore}'
    },
    columns : [
        {
            text : 'Id',
            width : 80,
            dataIndex : 'id',
            hidden : true
        },
        {
            text : appI18N.invitados.nombre,
            dataIndex : 'nombre',
            flex : 10
        },
        {
	 		text: appI18N.reuniones.asistencia,
	 		xtype:'checkcolumn',
	 		dataIndex:'asistencia',
	 		listeners:{
				checkchange:'onCheckChange'
			}
	    }
       
    ],
    viewConfig : {
        emptyText : appI18N.organos.sinInvitadosDisponibles
    },
    listeners : {
        organoSelected : 'organoSelected'
    }
});
