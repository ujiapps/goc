Ext.define('goc.view.organo.OrganoReunionInvitadoGridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.organoReunionInvitadoGridController',
    
    onLoad : function()
    {
    },

    onCheckChange : function(elem, rowIndex, checked, record)
    {
		Ext.Ajax.request({
			url:'/goc/rest/reuniones/' + record.get('reunionId') + '/organos/' + record.get('organoId') + '/invitados/' + record.get('id'),
			method:'PUT',
			jsonData: {invitado:record.data}
		})
    },
    
    organoSelected : function(record)
    {
        if (record !== null) {
            var externo = record.data.externo;
            this.getViewModel().getStore('organoReunionInvitadosStore').load(
                {
					url: '/goc/rest/reuniones/' + record.get('reunionId') + '/organos/' + record.get('organoId') + '/invitados',	
                    params: {
                        externo: externo
                    }
                });
        }
    },

});
