Ext.define('goc.view.historicoReunion.FormRectificarVisibilidadController', {

    extend : 'Ext.app.ViewController',
    alias : 'controller.formRectificarVisibilidadController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();

        if (win)
        {
            win.destroy();
        }
    }
});