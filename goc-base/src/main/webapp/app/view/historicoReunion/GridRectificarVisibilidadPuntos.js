var gridRectificarVisibilidadPuntosColumns = [
    {
        text: 'Id',
        width: 80,
        dataIndex: 'id',
        hidden: true
    }
];

gridRectificarVisibilidadPuntosColumns.push({
    text: getMultiLangLabel(appI18N.reuniones.tituloOrdenDia, mainLanguage),
    dataIndex: 'titulo',
    flex: 1
});

if (isMultilanguageApplication()) {
    gridRectificarVisibilidadPuntosColumns.push({
        text: getMultiLangLabel(appI18N.reuniones.titulo, alternativeLanguage),
        dataIndex: 'tituloAlternativo',
        flex: 1,
    });
}

gridRectificarVisibilidadPuntosColumns.push({
    text: appI18N.reuniones.publico,
    dataIndex: 'publico',

    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

Ext.define('goc.view.historicoReunion.GridRectificarVisibilidadPuntos',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.gridRectificarVisibilidadPuntos',
        requires: ['goc.view.historicoReunion.GridRectificarVisibilidadPuntosController'],
        controller: 'gridRectificarVisibilidadPuntosController',
        name: 'gridRectificarVisibilidadPuntos',
        multiSelect: false,
        scrollable: true,

        flex: 1,
        columns: gridRectificarVisibilidadPuntosColumns,
        bbar: null,
        tbar: null,
        listeners: {
            beforeedit: 'onEdit'
        }
    }
);