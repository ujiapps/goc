var gridRectificarVisibilidadReunionColumns = [
    {
        text: 'Id',
        width: 80,
        dataIndex: 'id',
        hidden: true
    }
];

gridRectificarVisibilidadReunionColumns.push({
    text: getMultiLangLabel(appI18N.oficios.tituloReunion, mainLanguage),
    dataIndex: 'asunto',
    flex: 1
});

if (isMultilanguageApplication()) {
    gridRectificarVisibilidadReunionColumns.push({
        text: getMultiLangLabel(appI18N.reuniones.titulo, alternativeLanguage),
        dataIndex: 'asuntoAlternativo',
        flex: 1
    });
}

gridRectificarVisibilidadReunionColumns.push({
    text: appI18N.reuniones.publico,
    dataIndex: 'publica',
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

Ext.define('goc.view.historicoReunion.GridRectificarVisibilidadReunion',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.gridRectificarVisibilidadReunion',
        requires: ['goc.view.historicoReunion.GridRectificarVisibilidadReunionController'],
        controller: 'gridRectificarVisibilidadReunionController',
        name: 'gridRectificarVisibilidadReunion',
        multiSelect: false,
        scrollable: true,

        columns: gridRectificarVisibilidadReunionColumns,
        bbar: null,
        tbar: null,
        listeners: {
            beforeedit: 'onEdit'

        }
    }
);