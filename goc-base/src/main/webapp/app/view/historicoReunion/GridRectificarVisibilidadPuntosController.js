Ext.define('goc.view.historicoReunion.GridRectificarVisibilidadPuntosController', {

    extend : 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.gridRectificarVisibilidadPuntosController',

    onLoad: function () {
        var reunionId = this.getViewModel().data.id;
        var store = this.getViewModel().data.storePunto;
        store.getProxy().url = '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia';

        var ref = this;

        Ext.Ajax.request({
            url: '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia',
            method: 'GET',
            success: function (data) {
                var arrPuntos = Ext.decode(data.responseText).data;
                if (!arrPuntos || arrPuntos.length === 0)
                    return Ext.Msg.alert(appI18N.reuniones.editarAcuerdosDeliberaciones, appI18N.reuniones.sinPuntosOrdenDia);

                var puntosOrdenDia = [];
                puntosOrdenDia = ref.getPuntosOrdenDiaSinNivel(puntosOrdenDia, arrPuntos, reunionId);

                store.loadData(puntosOrdenDia);
            }
        });

    },

    getPuntosOrdenDiaSinNivel: function (puntosOrdenDiaModel, arrPuntos, reunionId) {
        var ref = this;
        var ids = [];
        arrPuntos.forEach(function (arrPunto) {
            if (!ids.includes(arrPunto.id)) {
                var subpuntos = arrPunto.data;
                var puntoOrdenDia = Ext.create('goc.model.PuntoOrdenDiaAcuerdo',
                {
                    id: arrPunto.id,
                    titulo: arrPunto.numeracionMultinivel + ' '+ arrPunto.titulo,
                    tituloAlternativo: arrPunto.numeracionMultinivel + ' '+ arrPunto.tituloAlternativo,
                    descripcion: arrPunto.descripcion,
                    descripcionAlternativa: arrPunto.descripcionAlternativa,
                    deliberaciones: arrPunto.deliberaciones,
                    deliberacionesAlternativas: arrPunto.deliberacionesAlternativas,
                    acuerdos: arrPunto.acuerdos,
                    acuerdosAlternativos: arrPunto.acuerdosAlternativos,
                    tieneSubpuntos: subpuntos != null,
                    reunionId: reunionId,
                    publico: arrPunto.publico
                });
                puntosOrdenDiaModel.push(puntoOrdenDia);
                ids.push(puntoOrdenDia.id);
                if (subpuntos != null && Array.isArray(subpuntos)) {
                    ref.getPuntosOrdenDiaSinNivel(puntosOrdenDiaModel, subpuntos, reunionId);
                }
                else if(subpuntos != null) {
                    var puntos = [subpuntos];
                    ref.getPuntosOrdenDiaSinNivel(puntosOrdenDiaModel, puntos, reunionId);
                }
            }
        });
        return puntosOrdenDiaModel;
    },
    
    onEdit: function () {
        var reunionId = this.getViewModel().data.id;
        this.getView().getStore().proxy.url = '/goc/rest/reuniones/' + reunionId + '/puntosOrdenDia/publico';
    }
});