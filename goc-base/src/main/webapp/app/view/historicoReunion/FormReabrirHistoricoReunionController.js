Ext.define('goc.view.historicoReunion.FormReabrirHistoricoReunionController', {

        extend: 'Ext.app.ViewController',
        alias: 'controller.formReabrirHistoricoReunionController',


    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        var gridHistorico = Ext.ComponentQuery.query('[name=reunionHistorico]')[0];
        var gridReunion = Ext.ComponentQuery.query('[reference=reunionGrid]')[0];
        gridHistorico.getStore().reload();
        if(gridReunion !== undefined)
            gridReunion.getStore().reload();
        if (win)
        {
            win.destroy();
        }
    },

    onReabrirReunion : function(){
        var textareaMensaje = this.getView().down('textarea');

        if (textareaMensaje.validate()) {
            var vm = this.getViewModel();
            var record = vm.get('record');
            var ref = this;
            Ext.Ajax.request(
                {
                    url: '/goc/rest/reuniones/' + record.get('id') + '/reabrir',
                    method: 'PUT',
                    jsonData: {motivoReapertura: textareaMensaje.getValue()},
                    scope: this,
                    success: function() {
                        ref.onClose();
                    }
                }
            );
        }
    }

    });