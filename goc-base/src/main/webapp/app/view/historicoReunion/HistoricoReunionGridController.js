Ext.define('goc.view.historicoReunion.HistoricoReunionGridController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.historicoReunionGridController',
    name: 'historicoReunionGridController',

    init: function () {
        this.debouncedOnSearchReunion = this.debounce(this.onSearchReunion, 300);
    },

    onLoad: function () {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('reunionesStore');
        store.getProxy().url = '/goc/rest/reuniones/completadas';
        store.load();
    },

    organoSelected: function (id, externo) {
        var grid = this.getView();

        if (id) {
            grid.getStore().load({
                params: {
                    organoId: Transforms.getOrganoId(id, externo),
                    externo: externo
                }
            });
        } else {
            var comboTipoOrgano = grid.down('comboReunionTipoOrgano');
            grid.getStore().load({
                params: {
                    tipoOrganoId: comboTipoOrgano.getValue()
                }
            });
        }
    },

    filtraComboOrgano: function(tipoOrganoId) {
        var vm = this.getViewModel();
        var store = vm.getStore('organosStore');

        var filter  = new Ext.util.Filter(
            {
                id : 'tipoOrganoId',
                property : 'tipoOrganoId',
                value : tipoOrganoId
            });

        store.addFilter(filter);
    },

    createModalReunion: function (record) {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('reunionesStore');
        var organosStore = Ext.create('goc.store.Organos');
        var invitadosStore = Ext.create('goc.store.ReunionInvitados');
        var viewport = this.getView().up('viewport');
        var ref = this;

        if (record) {
            organosStore.load({
                params: {
                    reunionId: record ? record.get('id') : null
                }
            });

            invitadosStore.proxy.url = '/goc/rest/reuniones/' + record.get('id') + '/invitados';

            organosStore.on("load", function () {
                invitadosStore.load();
            });

            invitadosStore.on("load", function () {
                var modalDefinition = ref.getReunionModalDefinition(record, store, organosStore, invitadosStore);
                ref.modal = viewport.add(modalDefinition);
                ref.modal.down('textfield[name=urlGrabacion]').setVisible(true);
                ref.modal.show();
            });
        } else {
            var modalDefinition = this.getReunionModalDefinition(record, store, organosStore, invitadosStore);
            this.modal = viewport.add(modalDefinition);
            this.modal.show();
        }
    },
    getReunionModalDefinition: function (reunion, reunionesStore, organosStore, invitadosStore) {
        return {
            xtype: 'formReunion',
            viewModel: {
                data: {
                    title: reunion ? appI18N.reuniones.edicion + ': ' + reunion.get('asunto') : appI18N.reuniones.nuevaReunion,
                    id: reunion ? reunion.get('id') : undefined,
                    reunion: reunion || {
                        type: 'goc.model.Reunion',
                        create: true
                    },
                    store: reunionesStore,
                    organosStore: organosStore,
                    reunionInvitadosStore: invitadosStore,
                    organosMiembros: {},
                    miembrosStores: {}
                }
            }
        };
    },

    limpiaFiltrosComboOrgano: function() {
        var vm = this.getViewModel();
        var store = vm.getStore('organosStore');
        store.clearFilter();

        var grid = this.getView();

        var comboOrganos = grid.down('comboOrgano');
        comboOrganos.clearValue();
    },

    tipoOrganoSelected: function (id, externo) {
        var grid = this.getView();

        if (id) {
            grid.getStore().load({
                params: {
                    tipoOrganoId: id
                }
            });
            var comboOrganos = grid.down('comboOrgano');
            comboOrganos.clearValue();
            this.filtraComboOrgano(id);
        } else {
            grid.getStore().load();
            this.limpiaFiltrosComboOrgano();
        }
    },

    onSearchReunion: function(field, searchString, oldValue, eOpts, me) {
        var viewModel = me.getViewModel();
        var store = viewModel.getStore('reunionesStore');

        if (!searchString || searchString.length <= 3) {
            store.clearFilter();
            return;
        }

        if(searchString.length > 3) {
			searchString = searchString.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
			
            var filter = new Ext.util.Filter(
                {
                    id: 'search',
                    property: 'query',
                    value: searchString
                });

            store.addFilter(filter);
        }
    },

    debounce: function (fn, delay) {
        var timerId;
        return function () {
            if (timerId) {
                clearTimeout(timerId);
            }
            var argumentsChange = arguments;
            var me = this;
            timerId = setTimeout(function() {
                fn(argumentsChange[0], argumentsChange[1], argumentsChange[2], argumentsChange[3], me);
                timerId = null;
            }, delay);
        };
    },

    onReabrirReunion: function (){
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewport = this.getView().up('viewport');

        if (!record)
        {
            return Ext.Msg.alert(appI18N.reuniones.reabrir,appI18N.reuniones.noReabrereunion );
        }
        this.modal = viewport.add({
            xtype : 'formReabrirHistoricoReunion',
            viewModel : {
                data : {
                    title : record.get('asunto'),
                    id : record.get('id'),
                    completada : record.get('completada'),
                    reabierta : record.get('reabierta'),
                    record: record,
                }
            }
        });
        this.modal.show();
    },

    onSubirHojaFirmas: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewport = this.getView().up('viewport');

        if (!record)
        {
            return Ext.Msg.alert(appI18N.reuniones.documentacion, appI18N.reuniones.seleccionarParaDocumentacion);
        }

        this.modal = viewport.add({
            xtype : 'formHojaFirmas',
            viewModel : {
                data : {
                    title : record.get('asunto'),
                    id : record.get('id'),
                    completada : record.get('completada'),
                    record: record,
                    existeHojaFirmas: false,
                    store: Ext.create('Ext.data.Store', {
                        fields: ['nombreFichero'],
                        proxy: {
                            type: 'memory'
                        }
                    })
                }
            }
        });
        this.modal.show();
    },

    onRectificarVisibilidad: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewport = this.getView().up('viewport');
        var storePuntoOrdenDia = Ext.create('goc.store.PuntosOrdenDia');
        var storeReunion = Ext.create('goc.store.Reuniones');

        if (!record) {
            return Ext.Msg.alert(appI18N.reuniones.tituloPrincipal, appI18N.reuniones.seleccionarParaVisbilidad);
        }

        this.modal = viewport.add({
            xtype: 'formRectificarVisibilidad',
            title: record.get('asunto'),
            viewModel: {
                data: {
                    id: record.get('id'),
                    record: record,
                    reunionPublica: record.data.publica,
                    storePunto: storePuntoOrdenDia,
                    storeReunion: storeReunion
                }
            }

        });
        this.modal.show();
    }
});
