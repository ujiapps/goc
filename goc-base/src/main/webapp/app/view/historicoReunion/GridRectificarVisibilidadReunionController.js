Ext.define('goc.view.historicoReunion.GridRectificarVisibilidadReunionController', {

    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.gridRectificarVisibilidadReunionController',

    onLoad: function () {
        var reunionId = this.getViewModel().data.id;
        var store = this.getViewModel().data.storeReunion;
        store.getProxy().url = '/goc/rest/reuniones/' + reunionId;
        store.load();
    },

    onEdit: function () {
        this.getView().getStore().proxy.url = '/goc/rest/reuniones/publica';
    }
});