Ext.define('goc.view.historicoReunion.FormRectificarVisibilidad',
    {
        extend: 'Ext.window.Window',
        alias: 'widget.formRectificarVisibilidad',
        xtype: 'formRectificarVisibilidad',
        modal: true,
        width: '60%',
        height: '70%',
        bodyPadding: 10,
        layout : {
            type : 'vbox',
            align : 'stretch'
        },
        requires: [
            'goc.view.historicoReunion.GridRectificarVisibilidadPuntos',
            'goc.view.historicoReunion.GridRectificarVisibilidadReunion',
            'goc.view.historicoReunion.FormRectificarVisibilidadController'
        ],

        controller : 'formRectificarVisibilidadController',

        items: [
            {
                xtype: 'gridRectificarVisibilidadReunion',
                bind : {
                    store : '{storeReunion}'
                },
                height:118
            },
            {
                xtype: 'tbspacer',
                height: 35
            },
            {
                xtype: 'gridRectificarVisibilidadPuntos',
                bind : {
                    store : '{storePunto}'
                },
            }
        ],

        bbar : {
            defaultButtonUI : 'default',
            items : [
                '->',
            {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'onClose');
                        }
                    }
                }
            ]
        }
    });