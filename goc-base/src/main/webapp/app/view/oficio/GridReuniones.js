Ext.define('goc.view.oficio.GridReuniones', {
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.gridReuniones',

    title: 'Selecciona una reunión',
    name: 'gridReuniones',
    reference: 'gridReuniones',
    requires: ['goc.view.oficio.GridReunionesController'],
    controller: 'gridReunionesController',
    autoHeight: true,
    maxHeight: 250,
    bind: {
        store: '{reunionesStore}'
    },
    tbar: [
        {
            xtype : 'textfield',
            emptyText : appI18N.oficios.asunto,
            flex : 1,
            name : 'searchReunion',
            listeners : {
                change : 'onSearchReunion'
            }
        },
        {
            xtype : 'comboOrgano',
            width: 150
        },
        {
            xtype: 'inputFecha',
            emptyText: appI18N.oficios.filtroFechaDesde,
            showClearIcon : true,
            name: 'searchFechaInicio',
            reference: 'searchFechaInicio',
            width: 150,
            listeners: {
                change: 'onSearchFechaInicio'
            }
        },
        {
            xtype: 'inputFecha',
            emptyText: appI18N.oficios.filtroFechaHasta,
            showClearIcon : true,
            name: 'searchFechaFin',
            reference: 'searchFechaFin',
            width: 150,
            listeners: {
                change: 'onSearchFechaFin'
            }
        }
    ],
    columns: [
        {
            text : 'ID',
            dataIndex : 'id',
            hidden : true
        },
        {
            text : getMultiLangLabel(appI18N.oficios.tituloReunion, mainLanguage),
            dataIndex : 'asunto',
            flex : 1
        },
        {
            text : appI18N.oficios.fecha,
            dataIndex : 'fecha',
            flex : 1,
            format : 'd/m/Y',
            xtype : 'datecolumn'
        }
    ],
    listeners: {
        organoSelected : 'organoSelected',
        selectionChange : 'reunionSelected'
    }
});