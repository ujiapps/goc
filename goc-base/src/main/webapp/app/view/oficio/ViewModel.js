Ext.define('goc.view.oficio.ViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.oficioViewModel',
    requires: [
        'goc.store.Reuniones',
        'goc.store.Organos',
        'goc.store.Oficios'
    ],
    stores: {
        reunionesStore: {
            type: 'reuniones'
        },
        organosStore: {
            type: 'organos',
            autoLoad: true
        },
        oficiosStore: {
            type: 'oficios',
            autoLoad: true
        },
        puntosOrdenDiaTreeStore: {
            type: 'puntosOrdenDiaTree'
        }
    }
});
