var oficioGridColumns = [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        text : getMultiLangLabel(appI18N.oficios.tituloReunion, mainLanguage),
        dataIndex : 'tituloReunion',
        flex : 1
    }
];

if (isMultilanguageApplication())
{
    oficioGridColumns.push({
        text : getMultiLangLabel(appI18N.oficios.tituloReunion, alternativeLanguage),
        dataIndex : 'tituloReunionAlternativo',
        flex : 1
    });
}

oficioGridColumns.push({
    text: getMultiLangLabel(appI18N.oficios.tituloPuntoOrdenDia, mainLanguage),
    dataIndex: 'tituloPuntoOrdenDia',
    flex: 1
});

if (isMultilanguageApplication())
{
    oficioGridColumns.push({
        text: getMultiLangLabel(appI18N.oficios.tituloPuntoOrdenDia, alternativeLanguage),
        dataIndex: 'tituloPuntoOrdenDiaAlternativo',
        flex: 1
    });
}


oficioGridColumns.push(
    {
        text: appI18N.oficios.destinatario,
        dataIndex: 'nombre',
        flex: 1
    },
    {
        text: appI18N.oficios.fecha,
        dataIndex: 'fechaEnvio',
        flex: 1,
        format : 'd/m/Y H:i',
        xtype : 'datecolumn'
    },
    {
        text: appI18N.oficios.registro,
        dataIndex: 'registroSalida',
        flex: 1,
        editor: {
            field: {
                allowBlank: true
            }
        }
    });


Ext.define('goc.view.oficio.GridOficios', {
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.oficioGrid',

    requires : [
        'goc.store.Oficios',
        'goc.view.oficio.GridOficiosController'
    ],
    controller : 'gridOficiosController',

    bind: {
        store: '{oficiosStore}'
    },

    tbar : [
        {
            xtype : 'button',
            name : 'add',
            iconCls : 'fa fa-plus',
            text : appI18N ? appI18N.common.anadir : 'Afegir',
            handler : 'onAdd'
        },
        {
            xtype : 'textfield',
            emptyText : appI18N.reuniones.buscarReunion,
            flex : 1,
            name : 'searchReunion',
            listeners : {
                change : 'onSearchReunion'
            }
        },
        {
            xtype : 'textfield',
            emptyText : appI18N.oficios.destinatario,
            flex : 1,
            name : 'searchDestinatario',
            listeners : {
                change : 'onSearchDestinatario'
            }
        },
        {
            xtype: 'inputFecha',
            emptyText: appI18N ? appI18N.oficios.fechaEnvioInicio : 'Data des de',
            showClearIcon : true,
            name: 'searchFechaEnvioInicio',
            reference: 'searchFechaEnvioInicio',
            listeners: {
                change: 'onSearchFechaEnvioInicio'
            }
        },
        {
            xtype: 'inputFecha',
            emptyText: appI18N ? appI18N.oficios.fechaEnvioFin : 'Data fins',
            showClearIcon : true,
            name: 'searchFechaEnvioFin',
            reference: 'searchFechaEnvioFin',
            listeners: {
                change: 'onSearchFechaEnvioFin'
            }
        }
    ],
    listeners : {
        render : 'onLoad',
        organoSelected : 'organoSelected',
        tipoOrganoSelected : 'tipoOrganoSelected'
    },

    title : appI18N.oficios.titulo,
    scrollable : true,

    columns : oficioGridColumns
});
