Ext.define('goc.view.oficio.Main', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.oficioMain',
    title: appI18N.oficios.titulo,
    viewModel: {
        type: 'oficioViewModel'
    },
    requires : [ 'goc.view.oficio.GridOficios',
        'goc.view.oficio.ViewModel',
        'goc.view.oficio.GridOficiosController',
        'goc.view.common.ComboOrgano',
        'goc.view.common.InputFecha',
        'goc.view.oficio.FormSeleccionaReunionPuntoController'],
    layout: 'fit',

    items: [{
        xtype: 'oficioGrid'
    }]
});
