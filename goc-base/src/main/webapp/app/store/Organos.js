Ext.define('goc.store.Organos', {
    extend: 'Ext.data.Store',
    alias: 'store.organos',
    model: 'goc.model.Organo',

    proxy: {
        type: 'rest',
        url: '/goc/rest/organos',
        reader: {
            type: 'json',
            rootProperty: 'data',
            transform: {
                fn: function (response) {
                    if (Array.isArray(response.data)) {
                        Ext.Array.each(response.data, function(record) {
                            Transforms.transformIdExterno(record);
                        });
                    } else {
                        response.data = Transforms.transformIdExterno(response.data);
                    }
                    return response;
                },
                scope: this
            }
        },
        writer: {
            type: 'json',
            writeAllFields : true,
            transform: {
                fn: function (organo) {
                    organo.id = Transforms.getOrganoId(organo.id, organo.externo);
                    return organo;
                },
                scope: this
            }
        }
    },
    pageSize: 10000
});
