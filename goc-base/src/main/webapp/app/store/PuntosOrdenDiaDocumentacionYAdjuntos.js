Ext.define('goc.store.PuntosOrdenDiaDocumentacionYAdjuntos', {
    extend: 'Ext.data.Store',
    alias: 'store.PuntosOrdenDiaDocumentacionYAdjuntos',
    model: 'goc.model.PuntosOrdenDiaDocumentacionYAdjuntos',

    proxy: {
        type: 'rest',
        url: '/goc/rest/reuniones',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    },
    filters:[{
		property: 'activo',
		value: true
	}]
});
