Ext.define('goc.store.PuntosOrdenDia', {
    extend: 'Ext.data.Store',
    alias: 'store.puntosOrdenDia',
    model: 'goc.model.PuntoOrdenDia',

    proxy: {
        type: 'rest',
        url: '/goc/rest/reuniones',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    }
});
