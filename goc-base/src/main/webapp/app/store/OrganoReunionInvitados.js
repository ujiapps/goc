Ext.define('goc.store.OrganoReunionInvitados', {
    extend: 'Ext.data.Store',
    alias: 'store.organoReunionInvitados',
    model: 'goc.model.OrganoReunionInvitado',

    proxy: {
        type: 'rest',
        url: '/goc/rest/reuniones',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    },

    autoLoad: false,
    autoSync: false
});
