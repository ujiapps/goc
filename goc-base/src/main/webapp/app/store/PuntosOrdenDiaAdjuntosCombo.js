Ext.define('goc.store.PuntosOrdenDiaAdjuntosCombo', {
    extend: 'Ext.data.Store',
    alias: 'store.PuntosOrdenDiaAdjuntosCombo',
    model: 'goc.model.PuntosOrdenDiaTipoAdjuntosCombo',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: '/goc/rest/puntosordendia/tipos',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    }
});
