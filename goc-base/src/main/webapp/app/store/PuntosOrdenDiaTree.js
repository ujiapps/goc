Ext.define('goc.store.PuntosOrdenDiaTree', {
    extend: 'Ext.data.TreeStore',
    alias: 'store.puntosOrdenDiaTree',
    fields: ['titulo',
        {
            name: 'tituloCompuesto', type: 'string',
            convert: function (value, rec) {
                return rec.data.numeracionMultinivel + ' ' + rec.data.titulo;
            }
        }, 'tituloAlternativo', 'publico', 'numeroDocumentos', 'numeroAcuerdos', 'urlActaAnterior', 'urlActaAnteriorAlt', 'numeracion', 'votacionAbierta'],
    root: {
        expanded: true
    },

    proxy: {
        type: 'rest',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        },
        appendId: false
    },
    autoLoad: false
});
